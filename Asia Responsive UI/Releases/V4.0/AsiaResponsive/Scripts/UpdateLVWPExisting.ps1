Add-Type -TypeDefinition @"
	public enum ToolbarType
	{
		Standard,
		FreeForm,
		None,
		ShowToolbar
	}   
"@



# =================================================================================
#
#Main Function to Add web Parts to publishing page
#
# =================================================================================
function GetWebpartDetails([string]$ConfigPath = "")
{
	$Error.Clear();

	$cfg = [xml](get-content $ConfigPath)

	if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
	{    
		Add-PsSnapin Microsoft.SharePoint.PowerShell
	}

	# Exit if config file is invalid
	if( $? -eq $false ) 
	{
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
		Write-Output "Could not read config file. Exiting ..."
	}

	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
	Write-Output "Sucessfully read config file $($ConfigPath) file"
	
	if($Error.Count -eq 0)
	{
		try
		{
			$Error.Clear()  

##Remove WebPartPages

			foreach($webpart in $cfg.Webparts.RemoveWebpart)
			{ 
				
				$siteUrl=$webpart.SiteURL
				$oSite = Get-SPSite $siteUrl   
				$projWebUrl=$siteUrl +"/offices/"
				foreach ($web in $oSite.AllWebs)
				{	
					$webUrl=$web.Url
					if($webUrl.StartsWith($projWebUrl,"CurrentCultureIgnoreCase"))
					{
						$webpart.SiteURL=$webUrl                     
						
						$result = CheckWebPartOnPage $webpart
						if($result -eq $false)
						{
							Write-Host "CheckWebPartOnPage " $result -ForegroundColor Green
							Write-Host "Url " $web.Url -ForegroundColor Green
							RemoveWebPartToPage $webpart
						}
						else
						{
							
							Write-Host "Failed Url " $web.Url -ForegroundColor Red
						}
						
					}

				}
				

			}  


#####
			
			foreach($webpart in $cfg.Webparts.Webpart)
			{ 
				if($webpart.Title -eq "Completed Campaigns")
				{
					$result = CheckWebPartOnPage $webpart
					if($result -eq $true)
					{
						Write-Host "CheckWebPartOnPage " $result -ForegroundColor Green
						Write-Host "Url " $web.Url -ForegroundColor Green
						AddListViewWebPartToPage $webpart
					}
					else
					{
						
						Write-Host "Failed Url " $web.Url -ForegroundColor Red
					}
				}
				
				else
				{
					$siteUrl=$webpart.SiteURL 
					$oSite = Get-SPSite $siteUrl   
					$projWebUrl=$siteUrl +"/Services/offices/"
					foreach ($web in $oSite.AllWebs)
					{	
						$webUrl=$web.Url
						
						Write-Host $webUrl " | " $projWebUrl
						if($webUrl.StartsWith($projWebUrl,"CurrentCultureIgnoreCase"))
						{
							$webpart.SiteURL=$webUrl
							
							$result = CheckWebPartOnPage $webpart
							if($result -eq $true)
							{
								Write-Host "CheckWebPartOnPage " $result -ForegroundColor Green
								Write-Host "Url " $web.Url -ForegroundColor Green
								#AddListViewWebPartToPage $webpart
							}
							else
							{
								#updateListView $webpart
								updateListViewWebPart $webpart
								Write-Host "Updated Url " $web.Url -ForegroundColor Green
							}
						}
						

					}
				}				

			}            
		}
		catch
		{
			Write-Host "`nException :" $Error -ForegroundColor Red
			Write-Output "`nException :" $Error
		}
	}
	else
	{
		Write-Host $Error -ForegroundColor Red
		Write-Output $Error
	}

} 
#===============================================================================
#
#
#
#===============================================================================


function updateListViewWebPart([object] $webpart)
{

	$siteUrl=$webpart.SiteURL
	$itemCount=$webpart.ItemCount
	$pagePath=$webpart.PagePath
	$webpartTitle=$webpart.Title
	$Query=[System.Web.HttpUtility]::HtmlDecode($webpart.Query)
	$pageUrl = $siteUrl + $pagePath
	$spWeb = Get-SPWeb $siteUrl -ErrorAction Stop
	$site=$spWeb.Site

	[Microsoft.SharePoint.Publishing.PublishingWeb]$pubWeb = [Microsoft.SharePoint.Publishing.PublishingWeb]::GetPublishingWeb($spWeb);
	$allowunsafeupdates = $spWeb.AllowUnsafeUpdates
	$spWeb.AllowUnsafeUpdates = $true
	try
	{
		$page = $spWeb.GetFile($pageUrl);
		if($page)
		{
			if ($page.Level -eq [Microsoft.SharePoint.SPFileLevel]::Checkout)
			{ 
				if ($page.CheckedOutBy.UserLogin -eq $spWeb.CurrentUser.UserLogin) 
				{
					Write-Host "Page has already been checked out " -ForegroundColor Yellow
					Write-Output "Page has already been checked out "
					
				} 
				else 
				{
					$SPWeb.CurrentUser.LoginName
					$page.UndoCheckOut()
					$page.CheckOut()
					Write-Host "Check out the page override" -ForegroundColor Yellow
					Write-Output "Check out the page override"
				}  
				
			}
			else
			{
				$page.CheckOut() 
				Write-Host "Check out the page" -ForegroundColor Green
				Write-Output "Check out the page"
			}
			
			try
			{
				#Initialise the Web part manager for the specified profile page.
				$spWebPartManager = $spWeb.GetLimitedWebPartManager($pageUrl, [System.Web.UI.WebControls.WebParts.PersonalizationScope]::Shared)
				$allWebparts=$spWebPartManager.Webparts
				for($i=0;$i -lt $allWebparts.Count;$i++)
				{
					$removeWP=$null
					$removeWP=$allWebparts[$i]
					if($removeWP.Title -eq $webpartTitle)
					{
						if($Query)
						{
							$removeWP.View.Query=$Query
						}
						if($itemCount)
						{
							$removeWP.View.RowLimit=$itemCount
						}
						if($webpart.JSLink)
						{
							$removeWP.JSLink=$webpart.JSLink
						}
						$removeWP.View.Update();
						$spWebPartManager.SaveChanges($removeWP);
						Write-Host "PagWP Saved Successfully" -ForegroundColor Green
						
					}
				}

				#Check to ensure the page is checked out by you, and if so, check it in    
				if ($page.CheckedOutBy.UserLogin -eq $spWeb.CurrentUser.UserLogin)    
				{        
					$page.CheckIn("Page checked in automatically by PowerShell script") 
					Write-Host "Page has been checked in" -ForegroundColor Green
					Write-Output "Page has been checked in"    
				}
				$error.Clear()
				$ErrorActionPreference = "SilentlyContinue"
				$page.Publish("Published")
				$ErrorActionPreference="Continue"
				if($error.Count > 0)
				{
					Write-Host "Page need not required to be published because of following reason : $(Error)" -ForegroundColor Yellow
					Write-Output "Page need not required to be published because of following reason : $(Error)"
				}
				Write-Host "Page has been published success" -ForegroundColor Green
				Write-Output "Page has been published success"
				
				$pubWeb.Close()
				$spWeb.Update()                                                                                                 
				$spWeb.AllowUnsafeUpdates = $allowunsafeupdates 
				$spWeb.Dispose() 
			}
			catch
			{
				Write-Host "(ERROR : "$_.Exception.Message")" -ForegroundColor Red
				Write-Output "(ERROR : "$_.Exception.Message")"
				$Error.clear()
			}
		}
	}
	catch
	{
		Write-Host "(ERROR : "$_.Exception.Message")" -ForegroundColor Red
		Write-Output "(ERROR : "$_.Exception.Message")"
		$Error.clear()
	}
	
} 
#===============================================================================
#
#
#
#===============================================================================


function updateListView([object] $webpart)
{

	$siteUrl=$webpart.SiteURL
	$viewQuery=[System.Web.HttpUtility]::HtmlDecode($webpart.Query)
	$spWeb = Get-SPWeb $siteUrl -ErrorAction Stop
	$site=$spWeb.Site

	[Microsoft.SharePoint.Publishing.PublishingWeb]$pubWeb = [Microsoft.SharePoint.Publishing.PublishingWeb]::GetPublishingWeb($spWeb);
	$allowunsafeupdates = $spWeb.AllowUnsafeUpdates
	$spWeb.AllowUnsafeUpdates = $true
	try
	{
		# Get list
		$list=$spWeb.Lists[$webpart.ListName]
		if(!$list)
		{
			Write-Host "List not Exist" -ForegroundColor Red
		}
		else
		{
			$existingView=$list.Views[$webpart.ViewName]
			if(!$existingView)
			{	
				Write-Host $webpart.ViewName "ViewName not Exist" -ForegroundColor Red
			}
			else
			{
				if(![string]::IsNullOrEmpty($viewQuery))
				{
					$existingView.Query=$viewQuery
					$existingView.Update()
					Write-Output "View has been updated successfully" -ForegroundColor Green
				}
			}
		}
		$spWeb.Update()                                                                                                 
		$spWeb.AllowUnsafeUpdates = $allowunsafeupdates 
		$spWeb.Dispose() 
	}
	catch
	{
		Write-Host "(ERROR : "$_.Exception.Message")" -ForegroundColor Red
		Write-Output "(ERROR : "$_.Exception.Message")"
	}
} 




# =================================================================================
#
# FUNC: RemoveWebPartToPage
# DESC: Remove WebPart from  given  publishing page .#
# =================================================================================
function  RemoveWebPartToPage([object] $webpart)
{                           
	$siteUrl=$webpart.SiteURL
	$pagePath=$webpart.PagePath
	$webpartTitle=$webpart.Title
	
	$pageUrl = $siteUrl + $pagePath
	$spWeb = Get-SPWeb $siteUrl -ErrorAction Stop
	$site=$spWeb.Site

	[Microsoft.SharePoint.Publishing.PublishingWeb]$pubWeb = [Microsoft.SharePoint.Publishing.PublishingWeb]::GetPublishingWeb($spWeb);
	$allowunsafeupdates = $spWeb.AllowUnsafeUpdates
	$spWeb.AllowUnsafeUpdates = $true

	$page = $spWeb.GetFile($pageUrl);
	if ($page.Level -eq [Microsoft.SharePoint.SPFileLevel]::Checkout)
	{ 
		if ($page.CheckedOutBy.UserLogin -eq $spWeb.CurrentUser.UserLogin) 
		{
			Write-Host "Page has already been checked out " -ForegroundColor Yellow
			Write-Output "Page has already been checked out "
			
		} 
		else 
		{
			$SPWeb.CurrentUser.LoginName
			$page.UndoCheckOut()
			$page.CheckOut()
			Write-Host "Check out the page override" -ForegroundColor Yellow
			Write-Output "Check out the page override"
		}  
		
	}
	else
	{
		$page.CheckOut() 
		Write-Host "Check out the page" -ForegroundColor Green
		Write-Output "Check out the page"
	}
	
	try
	{
		#Initialise the Web part manager for the specified profile page.
		$spWebPartManager = $spWeb.GetLimitedWebPartManager($pageUrl, [System.Web.UI.WebControls.WebParts.PersonalizationScope]::Shared)
		$allWebparts=$spWebPartManager.Webparts
		for($i=0;$i -lt $allWebparts.Count;$i++)
		{
			$removeWP=$null
			$removeWP=$allWebparts[$i]
			if($removeWP.Title -eq $webpartTitle)
			{
				$spWebPartManager.DeleteWebPart($removeWP)
			}
		}

		#Check to ensure the page is checked out by you, and if so, check it in    
		if ($page.CheckedOutBy.UserLogin -eq $spWeb.CurrentUser.UserLogin)    
		{        
			$page.CheckIn("Page checked in automatically by PowerShell script") 
			Write-Host "Page has been checked in" -ForegroundColor Green
			Write-Output "Page has been checked in"    
		}
		$error.Clear()
		$ErrorActionPreference = "SilentlyContinue"
		$page.Publish("Published")
		$ErrorActionPreference="Continue"
		if($error.Count > 0)
		{
			Write-Host "Page need not required to be published because of following reason : $(Error)" -ForegroundColor Yellow
			Write-Output "Page need not required to be published because of following reason : $(Error)"
		}
		Write-Host "Page has been published success" -ForegroundColor Green
		Write-Output "Page has been published success"
		
		$pubWeb.Close()
		$spWeb.Update()                                                                                                 
		$spWeb.AllowUnsafeUpdates = $allowunsafeupdates 
		$spWeb.Dispose() 
	}
	catch
	{
		Write-Host "(ERROR : "$_.Exception.Message")" -ForegroundColor Red
		Write-Output "(ERROR : "$_.Exception.Message")"
	} 
}



###This is the method that does the magic. It is invoked by the main script below.
Function SetViewToolbar([Microsoft.SharePoint.SPView] $view, [ToolbarType] $toolbarType)
{
	$setToolbarTypeParameterTypes = [uint32]
	$setToolbarTypeMethod = $view.GetType().GetMethod("SetToolbarType", [System.Reflection.BindingFlags]::Instance -bor [System.Reflection.BindingFlags]::NonPublic, $null, $setToolbarTypeParameterTypes, $null)
	$setToolbarParameters = [uint32] 2
	
	$setToolbarTypeMethod.Invoke($view, $setToolbarParameters)
	$view.Update()
}

# =================================================================================
#
# FUNC: AddWebPartPublish
# DESC: Add search query ,list view and Members Web Part to given publishing page .
# =================================================================================
function  AddListViewWebPartToPage([object] $webpart)
{                           
	$siteUrl=$webpart.SiteURL
	$pagePath=$webpart.PagePath
	$calendarWP=[System.Convert]::ToBoolean(@{$true="false";$false=$webpart.CalendarWP}[$webpart.CalendarWP -eq ""])
	$webpartZone=$webpart.WebpartZone
	$index= $webpart.Index
	$webpartTitle=$webpart.Title
	[boolean]$SiteUserWP=[System.Convert]::ToBoolean(@{$true="false";$false=$webpart.IsSiteUserWP}[$webpart.IsSiteUserWP -eq ""])
	[boolean]$isContentSearchWebpart=[System.Convert]::ToBoolean(@{$true="false";$false=$webpart.IsContentSearchWebpart}[$webpart.IsContentSearchWebpart -eq ""])
	[boolean]$isContentEditorWebpart=[System.Convert]::ToBoolean(@{$true="false";$false=$webpart.isContentEditorWebpart}[$webpart.isContentEditorWebpart -eq ""])
	$DisplayType=$webpart.DisplayType

	$pageUrl = $siteUrl + $pagePath
	$spWeb = Get-SPWeb $siteUrl -ErrorAction Stop
	$site=$spWeb.Site

	[Microsoft.SharePoint.Publishing.PublishingWeb]$pubWeb = [Microsoft.SharePoint.Publishing.PublishingWeb]::GetPublishingWeb($spWeb);
	$allowunsafeupdates = $spWeb.AllowUnsafeUpdates
	$spWeb.AllowUnsafeUpdates = $true

	$page = $spWeb.GetFile($pageUrl);
	if([Microsoft.SharePoint.Publishing.PublishingWeb]::IsPublishingWeb($spWeb)){
		if ($page.Level -eq [Microsoft.SharePoint.SPFileLevel]::Checkout)
		{ 
			if ($page.CheckedOutBy.UserLogin -eq $spWeb.CurrentUser.UserLogin) 
			{
				Write-Host "Page has already been checked out " -ForegroundColor Yellow
				Write-Output "Page has already been checked out "

			} 
			else 
			{
				$SPWeb.CurrentUser.LoginName
				$page.UndoCheckOut()
				$page.CheckOut()
				Write-Host "Check out the page override" -ForegroundColor Yellow
				Write-Output "Check out the page override"
			}  

		}
		else
		{
			$page.CheckOut() 
			Write-Host "Check out the page" -ForegroundColor Green
			Write-Output "Check out the page"
		}
	}

	try
	{
		#Initialise the Web part manager for the specified profile page.
		$spWebPartManager = $spWeb.GetLimitedWebPartManager($pageUrl, [System.Web.UI.WebControls.WebParts.PersonalizationScope]::Shared)

		$list= $spWeb.Lists[$webpart.ListName];
		If($list -ne $null )
		{
			Write-Host "The list named $list is existing" -ForegroundColor Green
			Write-Output "The list named $list is existing"
			$listViewWebPart=$null
			$listViewWebPart=$spWebPartManager.Webparts | where {$_.DisplayTitle.ToLower() -eq $webpart.Title.ToLower()}
			$newWebpart=$false
			if($listViewWebPart -eq $null)
			{
				if($calendarWP)
				{
					$listViewWebPart = New-Object Microsoft.SharePoint.WebPartPages.ListViewWebPart
					$newWebpart=$true
				}
				else
				{
					$listViewWebPart = New-Object Microsoft.SharePoint.WebPartPages.XsltListViewWebPart
					$newWebpart=$true
				}
			}
			$listViewWebPart.Title = $webpartTitle
			$listViewWebPart.ListName = ($list.ID).ToString("B").ToUpper()
			$view = $list.Views[$webpart.ViewName];
			$listViewWebPart.ViewGuid = $view.ID.ToString("B").ToUpper();
			$listViewWebPart.JSLink=$webpart.JSLink
			$listViewWebPart.ViewId=37
			$listViewWebPart.TitleUrl=$webpart.TitleURL

			if(![string]::IsNullOrEmpty($webpart.ToolBarType))
			{
				#$view = $listViewWebPart.View
				SetViewToolbar $view $webpart.ToolBarType
#                                                              $listViewWebPart.Toolbar = $webpart.ToolBarType
			}
			if($newWebpart)
			{
				$spWebPartManager.AddWebPart($listViewWebPart, $webpartZone, $index)
				Write-Host "Webpart $webpartTitle added successfully" -ForegroundColor Green
				Write-Output "Webpart $webpartTitle added successfully"
			}
			$spWebPartManager.SaveChanges($listViewWebPart)
			Write-Host "Webpart $webpartTitle updated successfully" -ForegroundColor Green
			Write-Output "Webpart $webpartTitle updated successfully" 
		}
		else
		{
			Write-Host "Error: The list named $list is not existing" -ForegroundColor Red
			Write-Output "Error: The list named $list is not existing"
		}



		if([Microsoft.SharePoint.Publishing.PublishingWeb]::IsPublishingWeb($spWeb)){
			#Check to ensure the page is checked out by you, and if so, check it in    
			if ($page.CheckedOutBy.UserLogin -eq $spWeb.CurrentUser.UserLogin)    
			{        
				$page.CheckIn("Page checked in automatically by PowerShell script") 
				Write-Host "Page has been checked in" -ForegroundColor Green
				Write-Output "Page has been checked in"   
				$error.Clear()
				$ErrorActionPreference = "SilentlyContinue"
				$page.Publish("Published")
				$ErrorActionPreference="Continue"
				if($error.Count > 0)
				{
					Write-Host "Page need not required to be published because of following reason : $(Error)" -ForegroundColor Yellow
					Write-Output "Page need not required to be published because of following reason : $(Error)"
				}
				Write-Host "Page has been published success" -ForegroundColor Green
				Write-Output "Page has been published success" 
			}
		}

		$pubWeb.Close()
		$spWeb.Update()
		$spWeb.AllowUnsafeUpdates = $allowunsafeupdates 
		$spWeb.Dispose() 

	}
	catch
	{
		Write-Host "(ERROR : "$_.Exception.Message")" -ForegroundColor Red
		Write-Output "(ERROR : "$_.Exception.Message")"
	} 
}


function  CheckWebPartOnPage([object] $webpart)
{	
	try
	{	
		$isValid=$false
		$siteUrl=$webpart.SiteURL
		$pagePath=$webpart.PagePath
		$webpartTitle=$webpart.Title
		$WebpartZone=$webpart.WebpartZone
		$pageUrl = $siteUrl + $pagePath
		$spWeb = Get-SPWeb $siteUrl -ErrorAction Stop
		$site=$spWeb.Site
		[Microsoft.SharePoint.Publishing.PublishingWeb]$pubWeb = [Microsoft.SharePoint.Publishing.PublishingWeb]::GetPublishingWeb($spWeb);
		$allowunsafeupdates = $spWeb.AllowUnsafeUpdates
		$page = $spWeb.GetFile($pageUrl);
		if ($page.Level -eq [Microsoft.SharePoint.SPFileLevel]::Checkout)
		{ 
			if ($page.CheckedOutBy.UserLogin -eq $spWeb.CurrentUser.UserLogin) 
			{
				#Write-Host "Page has already been checked out " -ForegroundColor Yellow
				Write-Output "Page has already been checked out "

			} 
			else 
			{
				$page.UndoCheckOut()
				$page.CheckOut()
			}  

		}
		else
		{
			$page.CheckOut() 
		}	
		$isValid=$true
		#Initialise the Web part manager for the specified profile page.
		$spWebPartManager = $spWeb.GetLimitedWebPartManager($pageUrl, [System.Web.UI.WebControls.WebParts.PersonalizationScope]::Shared)
		$allWebparts=$spWebPartManager.Webparts
		for($i=0;$i -lt $allWebparts.Count;$i++)
		{
			$removeWP=$null
			$removeWP=$allWebparts[$i]
			if($removeWP.Title -eq $webpartTitle)
			{
				$isValid= $false
				

			}
		}

		$page.UndoCheckOut()

		$pubWeb.Close()				
		$spWeb.Dispose() 
	}
	catch
	{
		Write-Host "(ERROR : "$_.Exception.Message")" -ForegroundColor Red
		Write-Output "(ERROR : "$_.Exception.Message")"
		return $false
	} 
	return $isValid
}
