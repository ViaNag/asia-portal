var editLinks = {
    clientContext: null,
    web: null,
    oListLinks: null,
    currentUser: null,
    LoginName: null,
    quickLinksItems: null,
    searchProperties: { Title: "Title", ID: "ListItemID", LinkLoc: "LinkLocationOWSURLH", BackGrndImgLoc: "BackgroundImageLocationOWSURLH", DefaultQuickLink: "DefaultQuickLinksOWSBOOL", SiteUrl: "SPSiteURL" },
    getArticle: function () {
        if (confirm('This will revert all the quick links to default?')) {
            editLinks.clientContext = new SP.ClientContext.get_current();
            editLinks.web = editLinks.clientContext.get_web();
            editLinks.currentUser = editLinks.web.get_currentUser();
            editLinks.LoginName = editLinks.currentUser.get_loginName();
            //Code update for ADFS
            if (editLinks.LoginName.split('|').length == 2) {
                editLinks.LoginName = editLinks.LoginName.split('|')[1];
            }
            editLinks.clientContext.load(editLinks.currentUser);
            editLinks.clientContext.executeQueryAsync(editLinks.successgetArticle, editLinks.failureGetArticle);
        }

    },
    successgetArticle: function () {

        var linkID = "";
        var linkUrl = "";
        var linkName = "";
        var linkLocation = "";
        var propertyValue = "";
        var count = 0;

        //new implementation

        $('#QuickLinksContainer ul').html("");
        var quickLinkString = quickLinks.webApplicationQuickLinks.replace(/\|/g, '');
        var delim = _spPageContextInfo.webServerRelativeUrl.substring(_spPageContextInfo.webServerRelativeUrl.lastIndexOf("/") + 1);
        if (localStorage.getItem("searchResults") == null) {
            editLinks.getQuickLinksResults();
        }
        $.each(JSON.parse(localStorage.getItem("searchResults")), function (i, val) {
            if (editLinks.getResultItem(i, editLinks.searchProperties.DefaultQuickLink) == "1" && editLinks.getResultItem(i, editLinks.searchProperties.SiteUrl).toLowerCase() == _spPageContextInfo.siteAbsoluteUrl.toLowerCase()) {
                linkID = editLinks.getResultItem(i, editLinks.searchProperties.ID);
                linkName = editLinks.getResultItem(i, editLinks.searchProperties.Title);
                linkLocation = editLinks.getResultItem(i, editLinks.searchProperties.LinkLoc);
                linkUrl = editLinks.getResultItem(i, editLinks.searchProperties.BackGrndImgLoc);
                var qucikString = '<li style="float: left;margin-right: 10px;"><a href="' + linkLocation + '"><img src="' + linkUrl + '" style="width:40px;height:40px;display:block"><span>' + linkName + '</span></a></li>';
                // display quicklinks in mobile, Ipad or Desktop
                if ($(window).width() < 480) {
                    if (count >= 4) {
                        $('#QuickLinksDropdown ul.QuickLinksDropdown').append(qucikString);
                    }
                    else {
                        $('#QuickLinksContainer ul.QuickLinksContainer').append(qucikString);
                    }
                }
                else if ($(window).width() < 768) {
                    if (count >= 6) {
                        $('#QuickLinksDropdown ul.QuickLinksDropdown').append(qucikString);
                    }
                    else {
                        $('#QuickLinksContainer ul.QuickLinksContainer').append(qucikString);
                    }
                }
                else if ($(window).width() < 1200) {
                    if (count >= 7) {
                        $('#QuickLinksDropdown ul.QuickLinksDropdown').append(qucikString);
                    }
                    else {
                        $('#QuickLinksContainer ul.QuickLinksContainer').append(qucikString);
                    }
                }
                else {
                    $('#QuickLinksContainer ul.QuickLinksContainer').append(qucikString);
                }
                if (delim != "") {
                    propertyValue += linkID + "$$" + delim + '@#@';
                }
                else {
                    propertyValue += linkID + '@#@';
                }
                count++;

            }
        });
        var propertyName = 'IntranetQuickLinks';
        SiteQuickLinks.updateUserProfileValue(editLinks.LoginName, propertyName, propertyValue, false);
        SiteQuickLinks.updateUserProfileValue(editLinks.LoginName, "IntranetCustomQuickLinks", "", true);


    },
    getQuickLinks: function () {
        var linkID = '';
        editLinks.clientContext = new SP.ClientContext.get_current();
        editLinks.web = editLinks.clientContext.get_web();
        editLinks.currentUser = editLinks.web.get_currentUser();
        editLinks.LoginName = editLinks.currentUser.get_loginName();
        //Code update for ADFS
        if (editLinks.LoginName.split('|').length == 2) {
            editLinks.LoginName = editLinks.LoginName.split('|')[1];
        }
        editLinks.clientContext.load(editLinks.currentUser);
        editLinks.clientContext.executeQueryAsync(editLinks.successgetQuickLinks, editLinks.failure);
    },
    successgetQuickLinks: function () {
        // clear existing quicklinks HTML so that it will create again when window has resized
        $('#AddQuickLinksContainer ul').html('');
        var temp = quickLinks.webApplicationQuickLinks.replace(/\|/g, '');
        var selectedLinks = temp.split('@#@');
        var finalLinks = selectedLinks;
        if (localStorage.getItem("searchResults") == null) {
            editLinks.getQuickLinksResults();
        }
        if (JSON.parse(localStorage.getItem("searchResults")) != null && JSON.parse(localStorage.getItem("searchResults")) != undefined) {
            var resultsCount = JSON.parse(localStorage.getItem("searchResults")).length;

            for (var i = 0; i < resultsCount; i++) {

                if (editLinks.getResultItem(i, editLinks.searchProperties.Title) === 'DefaultQuickLinksImagePlaceholder') {
                    imgUrl = editLinks.getResultItem(i, quickLinks.searchProperties.BackGrndImgLoc);
                    break;
                }
            }
            for (var i = 0; i < resultsCount; i++) {

                if (editLinks.getResultItem(i, editLinks.searchProperties.DefaultQuickLink) == "1" && editLinks.getResultItem(i, editLinks.searchProperties.SiteUrl).toUpperCase() == _spPageContextInfo.siteAbsoluteUrl.toUpperCase()) {
                    var isRecommendedItemInQuickLinks = false;
                    linkID = editLinks.getResultItem(i, editLinks.searchProperties.ID);
                    linkName = editLinks.getResultItem(i, editLinks.searchProperties.Title);
                    linkLocation = editLinks.getResultItem(i, editLinks.searchProperties.LinkLoc);
                    linkUrl = editLinks.getResultItem(i, editLinks.searchProperties.BackGrndImgLoc);
                    var delim = _spPageContextInfo.webServerRelativeUrl.substring(_spPageContextInfo.webServerRelativeUrl.lastIndexOf("/") + 1);
                    var finalLinkId = "";
                    if (delim != "") {
                        finalLinkId = linkID + "$$" + delim;
                    }
                    else {
                        finalLinkId = linkID;
                    }
                    var link = finalLinkId + '@#@';

                    /* Check to bind the correct html if the same item is already added from Recommended links section */
                    if (linkLocation != null && linkLocation != undefined && linkLocation != "" && quickLinks.webApplicationQuickLinks.toLowerCase().indexOf(linkLocation.toLowerCase()) > -1) {
                        var qucikString = '<li class="quick-link-pages quick-link-pages-remove" style="padding:0 5px;margin-bottom: 5px; height:20px;width:100%"><span><a href="' + linkLocation + '" style="display: block;float: left;margin-right: 10px;font-weight:bold;"><span>' + linkName + '</span></a><img src="../SiteCollectionImages/icons/greenroom-panel-icon-circle-check.png" style="height:10px;width:10px;"></span><a href="#" onclick="SiteQuickLinks.fetchUserProfile(\'' + linkName + '\',\'' + linkLocation + '\',\'remove\',\'IntranetQuickLinks\', \'false\')" class="btnRemove"><span style="padding:0 5px;float:right;">Remove</span></a></li>';
                        isRecommendedItemInQuickLinks = true;
                    }
                    else if (quickLinks.webApplicationQuickLinks.indexOf(link) == -1 && !isRecommendedItemInQuickLinks) {
                        var qucikString = '<li class="quick-link-pages" style="padding:0 5px; background-color: white; margin-bottom: 5px; height:20px;width:100%"><a href="' + linkLocation + '" style="display: block;float: left;margin-right: 10px;font-weight:bold;"><span>' + linkName + '</span></a>' +
                            '<a href="#" id="btn' + finalLinkId + '" class="btnAdd"><span class="hover" style="padding:0 5px; display: none"><img src="/SiteCollectionImages/icons/greenroom-panel-icon-plus.png">Add to quick links</span></a>' +
                            '<span class="quick-links-full" style="padding:0 5px; display: none"><img src="../SiteCollectionImages/icons/greenroom-panel-icon-delete.png" style="width:10px;height:10px"><span style="padding:0 5px;float:right">Quick links full</span></span>' + '</li>';
                    } else if (!isRecommendedItemInQuickLinks) {
                        var qucikString = '<li class="quick-link-pages quick-link-pages-remove" style="padding:0 5px;margin-bottom: 5px; height:20px;width:100%"><span><a href="' + linkLocation + '" style="display: block;float: left;margin-right: 10px;font-weight:bold;"><span>' + linkName + '</span></a><img src="../SiteCollectionImages/icons/greenroom-panel-icon-circle-check.png" style="height:10px;width:10px;"></span>' +
                        '<a href="#" id="btn' + finalLinkId + '" class="btnRemove"><span style="padding:0 5px;float:right;">Remove</span></a>' + '</li>';
                    }
                    //}
                    $('#AddQuickLinksContainer ul').append(qucikString);
                }
                //break;							
                //}
                //}

            }

            editLinks.setEditQuickLinkPanel();
            setTimeout(function () {
                $(".btnRemove").click(function () {
                    var linkType = "";
                    var selectedLinksFinal;
                    var isMultiValue;
                    var propertyName;
                    linkType = $(this).attr('type');
                    var id = $(this).attr('id');
                    id = id.split('btn');
                    var removeID = id[1];
                    var delim = _spPageContextInfo.webServerRelativeUrl.substring(_spPageContextInfo.webServerRelativeUrl.lastIndexOf("/") + 1);
                    if (linkType == "customLink") {
                        selectedLinksFinal = quickLinks.intranetCustomQuickLinks.replace(removeID + '@#@', '');
                        propertyName = 'IntranetCustomQuickLinks';
                        isMultiValue = true;
                    }
                    else {

                        if (quickLinks.intranetQuickLinks.slice(-1) == removeID) {
                            selectedLinksFinal = quickLinks.webApplicationQuickLinks.replace(removeID, '');
                        }
                        else {

                            selectedLinksFinal = quickLinks.webApplicationQuickLinks.replace(removeID + '@#@', '');

                        }

                        propertyName = 'IntranetQuickLinks';
                        isMultiValue = false;
                    }

                    selectedLinksFinal = clearQuickLinksValue(selectedLinksFinal);

                    SiteQuickLinks.updateUserProfileValue(editLinks.LoginName, propertyName, selectedLinksFinal, isMultiValue);
                    //  location.reload();
                });
            }, 500);

            setTimeout(function () {
                $(".btnAdd").click(function () {
                    var id = $(this).attr('id');
                    id = id.split('btn');
                    var addID = id[1];
                    var delim = _spPageContextInfo.webServerRelativeUrl.substring(_spPageContextInfo.webServerRelativeUrl.lastIndexOf("/") + 1);
                    var isDelemeterContains = addID.split("$$");
                    if (isDelemeterContains.length == 1 && delim != "") {
                        addID = id[1] + "$$" + delim;
                    }

                    var selectedLinksFinal = quickLinks.webApplicationQuickLinks.replace(/\|/g, '');
                    selectedLinksFinal = clearQuickLinksValue(selectedLinksFinal);
                    selectedLinksFinal = selectedLinksFinal + addID + '@#@';
                    var propertyName = 'IntranetQuickLinks';
                    selectedLinksFinal = clearQuickLinksValue(selectedLinksFinal);
                    SiteQuickLinks.updateUserProfileValue(editLinks.LoginName, propertyName, selectedLinksFinal, false);
                    // location.reload();
                });
            }, 500);
            var propertyName = 'IntranetQuickLinks';
            SiteQuickLinks.fetchUserProfile("formattedname", "unknown", "load", propertyName, false);

            /* Code to bind the final html for the categorized links web part on edit quick links page. */
            $(".category-links-edit-li").each(function () {
                $(this).find('img').remove();
                if ($(this).find('a')[1] != undefined || $(this).find('a')[1] != null) {
                    $(this).find('a')[1].remove();
                }
                $(this).find('span.quick-links-full').remove();
                $(this).removeClass('category-links-edit-li-remove');
                var isRecommendedItemInQuickLinks = false;
                var recommendedLinksAnchor = $(this).find("a");
                var recommendedLinksSpan = $(this).find("span");
                var recommendedLinkUrl;
                var recommendedLinkTitle;
                var quickString;
                var linkID;
                if (recommendedLinksAnchor != undefined && recommendedLinksAnchor != null) {
                    recommendedLinkUrl = recommendedLinksAnchor.attr("href");
                    linkID = recommendedLinksAnchor.attr("id");
                }
                if (recommendedLinksSpan != undefined && recommendedLinksSpan != null) {
                    recommendedLinkTitle = recommendedLinksSpan.text().trim();
                }

                var delim = _spPageContextInfo.webServerRelativeUrl.substring(_spPageContextInfo.webServerRelativeUrl.lastIndexOf("/") + 1);
                var finalLinkId = "";
                if (delim != "") {
                    finalLinkId = linkID + "$$" + delim;
                }
                else {
                    finalLinkId = linkID;
                }
                var link = finalLinkId + '@#@';

                for (var i = 0; i < quickLinks.webApplicationQuickLinks.split("@#@").length; i++) {

                    if (recommendedLinkUrl != null && recommendedLinkUrl != undefined && recommendedLinkUrl != "" && quickLinks.webApplicationQuickLinks.split("@#@")[i].toLowerCase().indexOf(recommendedLinkUrl.toLowerCase()) > -1) {
                        quickString = '<img src="../SiteCollectionImages/icons/greenroom-panel-icon-circle-check.png" style="height:10px;width:10px;"><a href="#" onclick="SiteQuickLinks.fetchUserProfile(\'' + recommendedLinkTitle + '\',\'' + quickLinks.webApplicationQuickLinks.split("@#@")[i].split(",")[1] + '\',\'remove\',\'IntranetQuickLinks\',\'false\')"><span style="padding:0 5px;float:right;">Remove</span></a>';
                        isRecommendedItemInQuickLinks = true;
                        $(this).addClass("category-links-edit-li-remove");
                        break;
                    }
                }
                if (quickLinks.webApplicationQuickLinks.indexOf(link) == -1 && !isRecommendedItemInQuickLinks) {
                    quickString = '<a href="#" id="btn' + finalLinkId + '" class="btnAdd"><span class="hover" style="padding:0 5px; display: none"><img src="/SiteCollectionImages/icons/greenroom-panel-icon-plus.png">Add to quick links</span></a>' +
                        '<span class="quick-links-full" style="padding:0 5px; display: none"><img src="../SiteCollectionImages/icons/greenroom-panel-icon-delete.png" style="width:10px;height:10px"><span style="padding:0 5px;float:right">Quick links full</span></span>';
                } else if (!isRecommendedItemInQuickLinks) {
                    quickString = '<img src="../SiteCollectionImages/icons/greenroom-panel-icon-circle-check.png" style="height:10px;width:10px;"></span>' +
                    '<a href="#" id="btn' + finalLinkId + '" class="btnRemove"><span style="padding:0 5px;float:right;">Remove</span></a>';
                    $(this).addClass("category-links-edit-li-remove");
                }
                $(this).append(quickString);
            });

            /* Code to add links in the recommended links section */
            $('.category-links-edit-li').hover(function () {
                var myQuickLinksLength = $('#EditQuickLinksContainer').find('li').length;
                var myCustomLinksLength = $('#CustomQuickLinksContainer').find('li').length;
                var totalLinksLength = myQuickLinksLength + myCustomLinksLength;
                if (totalLinksLength < 13) {
                    $(this).find("span.hover").toggle();
                }
                else {
                    $(this).find("span.quick-links-full").toggle();

                }
            });
            /* Code to apply hover functionality in Enterprise links section */
            $('.quick-link-pages').hover(function () {
                var myQuickLinksLength = $('#EditQuickLinksContainer').find('li').length;
                var myCustomLinksLength = $('#CustomQuickLinksContainer').find('li').length;
                var totalLinksLength = myQuickLinksLength + myCustomLinksLength;
                if (totalLinksLength < 13) {
                    $(this).find("span.hover").toggle();
                } else {
                    $(this).find("span.quick-links-full").toggle();

                }
            });
        }
    },



    setEditQuickLinkPanel: function () {
        $('#EditQuickLinksContainer ul').html('');
        var temp = quickLinks.webApplicationQuickLinks.replace(/\|/g, '');
        var selectedLinks = temp.split('@#@');

        var imgUrl = "";
        if (localStorage.getItem("searchResults") == null) {
            editLinks.getQuickLinksResults();
        }
        if (JSON.parse(localStorage.getItem("searchResults")) != null && JSON.parse(localStorage.getItem("searchResults")) != undefined) {
            var resultsCount = JSON.parse(localStorage.getItem("searchResults")).length;

            for (var i = 0; i < selectedLinks.length; i++) {

                var selectedId = selectedLinks[i].split("$$")[0];

                if (!isNaN(selectedId)) {
                    for (var j = 0; j < resultsCount; j++) {

                        if (selectedLinks[i].split("$$").length == 1) {
                            var crntSiteUrl = _spPageContextInfo.siteAbsoluteUrl.indexOf("/sites") > -1 ? _spPageContextInfo.siteAbsoluteUrl.substring(0, _spPageContextInfo.siteAbsoluteUrl.indexOf("/sites")).toUpperCase() : _spPageContextInfo.siteAbsoluteUrl.toUpperCase();
                            if (editLinks.getResultItem(j, editLinks.searchProperties.ID).toString() == selectedLinks[i] && crntSiteUrl) {
                                var linkName = editLinks.getResultItem(j, editLinks.searchProperties.Title);
                                var linkUrl = editLinks.getResultItem(j, editLinks.searchProperties.BackGrndImgLoc) != null ? editLinks.getResultItem(j, editLinks.searchProperties.BackGrndImgLoc) : "";
                                var linkLocation = editLinks.getResultItem(j, editLinks.searchProperties.LinkLoc) != null ? editLinks.getResultItem(j, editLinks.searchProperties.LinkLoc) : "";
                                var qucikString = '<li style="float: left; padding:0 5px;"><img src="' + linkUrl + '" style="height:40px;width:40px;"><a href="' + linkLocation + '" style="display:block;font-weight:bold;padding-top:10px;min-height:90px;"><span>' + linkName + '</span></a><a href="#" style="display:block;" id="btn' + selectedLinks[i] + '" class="btnRemove"><span>(remove)</span></a></li>';
                                $('#EditQuickLinksContainer ul').append(qucikString);
                                break;
                            }
                        }
                        else {
                            if (editLinks.getResultItem(j, editLinks.searchProperties.ID).toString() == selectedId && editLinks.getResultItem(j, editLinks.searchProperties.SiteUrl).indexOf(selectedLinks[i].split("$$")[1]) > -1) {
                                var linkName = editLinks.getResultItem(j, editLinks.searchProperties.Title);
                                var linkUrl = editLinks.getResultItem(j, editLinks.searchProperties.BackGrndImgLoc) != null ? editLinks.getResultItem(j, editLinks.searchProperties.BackGrndImgLoc) : "";
                                var linkLocation = editLinks.getResultItem(j, editLinks.searchProperties.LinkLoc) != null ? editLinks.getResultItem(j, editLinks.searchProperties.LinkLoc) : "";
                                var qucikString = '<li style="float: left; padding:0 5px;"><img src="' + linkUrl + '" style="height:40px;width:40px;"><a href="' + linkLocation + '" style="display:block;font-weight:bold;padding-top:10px;min-height:90px;"><span>' + linkName + '</span></a><a href="#" style="display:block;" id="btn' + selectedLinks[i] + '" class="btnRemove"><span>(remove)</span></a></li>';
                                $('#EditQuickLinksContainer ul').append(qucikString);
                                break;
                            }
                        }
                    }
                }

                else {
                    var LinkImgUrl = "";
                    var sites;
                    var title = selectedId.split(',');
                    var linkLocation = title[1];
                    var nestedSite = title[1].split(_spPageContextInfo.siteAbsoluteUrl);
                    if (nestedSite.length > 1) {
                        sites = nestedSite[1].split('/');
                    }
                    else if (nestedSite.length == 1) {
                        sites = nestedSite[0].split('/');
                    }
                    for (var k = 0; k < sites.length; k++) {
                        if (sites[k] != "") {
                            if (LinkImgUrl == "") {
                                for (var j = 0; j < resultsCount; j++) {
                                    if (editLinks.getResultItem(j, editLinks.searchProperties.Title) === sites[k]) {
                                        LinkImgUrl = editLinks.getResultItem(j, editLinks.BackGrndImgLoc) != null ? editLinks.getResultItem(j, editLinks.BackGrndImgLoc) : "";
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (LinkImgUrl == "")
                        LinkImgUrl = imgUrl;
                    var linkName = title[0];
                    var qucikString = '<li style="float: left; padding:0 5px;"><img src="' + LinkImgUrl + '" style="height:40px;width:40px;"><a href="' + linkLocation + '" style="display:block;font-weight:bold;padding-top:10px;min-height:90px;"><span>' + linkName + '</span></a><a href="#" style="display:block" id="btn' + title[0] + ',' + title[1] + '" class="btnRemove"><span>(remove)</span></a></li>';
                    $('#EditQuickLinksContainer ul').append(qucikString);
                }
            }
        }
    },
    getQuickLinksResults: function () {
        $.ajax({

            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/search/query?querytext='ContentType:CTQuickLinks'&selectproperties='" + editLinks.searchProperties.Title + "%2c" + editLinks.searchProperties.ID + "%2c" + editLinks.searchProperties.LinkLoc + "%2c" + editLinks.searchProperties.BackGrndImgLoc + "%2c" + editLinks.searchProperties.SiteUrl + "%2c" + quickLinks.searchProperties.DefaultQuickLink + "'&rowlimit=500&clienttype='ContentSearchRegular'",
            type: "GET",
            headers: {
                "Accept": "application/json;odata=verbose"
            },
            async: false,
            success: function (data, textStatus, xhr) {
                var query = data.d.query;
                editLinks.insertResultItem(query.PrimaryQueryResult.RelevantResults.Table.Rows.results);
                editLinks.getListQuickLinksResults();
            },
            error: function (data) {
                editLinks.getListQuickLinksResults();
                //If any errors occurred - detail them here
                if (console != undefined && console != null) {
                    //console.log(data);
                }
            }
        });
    },
    getListQuickLinksResults: function () {
        $.ajax({
            url: _spPageContextInfo.siteAbsoluteUrl.indexOf("/sites") > -1 ? _spPageContextInfo.siteAbsoluteUrl.substring(0, _spPageContextInfo.siteAbsoluteUrl.indexOf("/sites")) + "/_api/web/lists/getbytitle('QuickLinks')/items?$top=5000" : _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('QuickLinks')/items?$top=5000",
            type: "GET",
            async: false,
            headers: {
                "Accept": "application/json;odata=verbose"
            },
            success: function (data, textStatus, xhr) {
                var resultItemLength = quickLinks.resultItem.length;
                for (var i = 0; i < data.d.results.length; i++) {
                    var resultItemIndex = resultItemLength + i;
                    quickLinks.resultItem[resultItemIndex] = {};
                    quickLinks.resultItem[resultItemIndex]["Item"] = [];
                    quickLinks.resultItem[resultItemIndex]["Item"].push({ Key: quickLinks.searchProperties.ID, Value: data.d.results[i].ID });
                    quickLinks.resultItem[resultItemIndex]["Item"].push({ Key: quickLinks.searchProperties.Title, Value: data.d.results[i].Title });
                    quickLinks.resultItem[resultItemIndex]["Item"].push({ Key: quickLinks.searchProperties.LinkLoc, Value: data.d.results[i].LinkLocation != null ? data.d.results[i].LinkLocation.Url : "" });
                    quickLinks.resultItem[resultItemIndex]["Item"].push({ Key: quickLinks.searchProperties.BackGrndImgLoc, Value: data.d.results[i].BackgroundImageLocation != null ? data.d.results[i].BackgroundImageLocation.Url : "" });
                    quickLinks.resultItem[resultItemIndex]["Item"].push({ Key: quickLinks.searchProperties.SiteUrl, Value: _spPageContextInfo.siteAbsoluteUrl.indexOf("/sites") > -1 ? _spPageContextInfo.siteAbsoluteUrl.substring(0, _spPageContextInfo.siteAbsoluteUrl.indexOf("/sites")) : _spPageContextInfo.siteAbsoluteUrl });
                    if (data.d.results[i].DefaultQuickLinks == true || data.d.results[i].DefaultQuickLinks == false) {
                        quickLinks.resultItem[resultItemIndex]["Item"].push({ Key: quickLinks.searchProperties.DefaultQuickLink, Value: data.d.results[i].DefaultQuickLinks ? "1" : "0" });
                    }
                    else {
                        quickLinks.resultItem[resultItemIndex]["Item"].push({ Key: quickLinks.searchProperties.DefaultQuickLink, Value: data.d.results[i].DefaultQuickLinks });
                    }
                }

                // VC: Remove this from the for loop
                localStorage.setItem("searchResults", JSON.stringify(quickLinks.resultItem));
            },
            error: function (data) {
                //If any errors occurred - detail them here
                if (console != undefined && console != null) {

                }
            }
        });
    },
    insertResultItem: function (data) {
        $.each(data, function (i, val) {
            quickLinks.resultItem[i] = {};
            quickLinks.resultItem[i]["Item"] = [];
            $.each(this.Cells.results, function () {
                quickLinks.resultItem[i]["Item"].push({ Key: this.Key, Value: this.Value })
            });
            localStorage.setItem("searchResults", JSON.stringify(quickLinks.resultItem))
        });
    },
    getResultItem: function (index, key) {
        var result = JSON.parse(localStorage.getItem("searchResults"));
        var item = ''
        for (var i = 0; i < result[index]["Item"].length; i++) {
            if (result[index]["Item"][i].Key == key) {
                var item = result[index]["Item"][i].Value;
                break;
            }
        }

        return item;
    },
    editLinks: function () {
        location.href = 'EditQuickLinks.aspx';
    },

    failure: function (sender, args) {
    },
    failureGetArticle: function (sender, args) {
        location.reload();
    },
    CreateQuickLink: function () {
        editLinks.ResetMyLinkExists();
        if (editLinks.ValidateCreateQuickLink()) {
            var linkName = $("#txtCreateQuickLinkTitle").val().trim();
            var linkUrl = $("#txtCreateQuickLinkUrl").val().trim();
            $("#createQuickLinkRow-compliment-loader").show();
            var action = "create";
            SiteQuickLinks.fetchUserProfile(linkName, linkUrl, action, "IntranetCustomQuickLinks", true);
            $("#txtCreateQuickLinkTitle").val("");
            $("#txtCreateQuickLinkUrl").val("");
        }

    },
    ValidateCreateQuickLink: function () {
        var myQuickLinksLength = $('#EditQuickLinksContainer').find('li').length;
        var myCustomLinksLength = $('#CustomQuickLinksContainer').find('li').length;
        var totalLinksLength = myQuickLinksLength + myCustomLinksLength;
        if (totalLinksLength >= 13) {
            $("#createQuickLinkErrorMsgRow").html("Your Quick Links are full, please remove some links.");
            return false;
        }
        else if ($("#txtCreateQuickLinkTitle").val() == null || $("#txtCreateQuickLinkTitle").val() == undefined || $("#txtCreateQuickLinkTitle").val().trim() == "" || $("#txtCreateQuickLinkUrl").val() == null || $("#txtCreateQuickLinkUrl").val() == undefined || $("#txtCreateQuickLinkUrl").val().trim() == "") {
            $("#createQuickLinkErrorMsgRow").html("Please fill in required fields indicated by *");
            return false;
        }
        else if ($("#txtCreateQuickLinkTitle").val().indexOf(',') > -1 || $("#txtCreateQuickLinkTitle").val().indexOf('$$') > -1 || $("#txtCreateQuickLinkTitle").val().indexOf('@#@') > -1 || $("#txtCreateQuickLinkUrl").val().indexOf(',') > -1 || $("#txtCreateQuickLinkUrl").val().indexOf('$$') > -1 || $("#txtCreateQuickLinkUrl").val().indexOf('@#@') > -1) {
            $("#createQuickLinkErrorMsgRow").html("$$, @#@, , is not allowed");
            return false;
        }
        else {
            return !editLinks.CheckIfMyUrlExists();
        }

    },
    CheckIfMyUrlExists: function () {
        var isMyLinkExists = false;
        //Check if URL exists in the Default links section
        $("#AddQuickLinksContainer").each(function () {
            $(this).find("li.quick-link-pages").each(function () {
                var anchor = $(this).find("a");
                if (anchor != null && anchor != undefined && anchor.length > 0) {
                    var anchorHref = anchor[0].href;
                    if (anchorHref != null && anchorHref != undefined && anchorHref != "") {
                        var myQuickLinkUrl = $("#txtCreateQuickLinkUrl").val().trim();
                        if (myQuickLinkUrl.toLowerCase().indexOf(anchorHref.toLowerCase()) > -1 || anchorHref.toLowerCase().indexOf(myQuickLinkUrl.toLowerCase()) > -1) {
                            $("#createQuickLinkErrorMsgRow").html("This link already exists on this page, please scroll down to add the link.");
                            $(this).addClass("myLinkExists");
                            isMyLinkExists = true;
                        }
                    }
                }
            });
        });
        //Check if URL exists in the Quick links list section
        $(".category-links-edit-ul").each(function () {
            $(this).find("li.category-links-edit-li").each(function () {
                var anchor = $(this).find("a.category-links-edit");
                if (anchor != null && anchor != undefined && anchor.length > 0) {
                    var anchorHref = anchor.attr("href");
                    if (anchorHref != null && anchorHref != undefined && anchorHref != "") {
                        var myQuickLinkUrl = $("#txtCreateQuickLinkUrl").val().trim();
                        if (myQuickLinkUrl.toLowerCase().indexOf(anchorHref.toLowerCase()) > -1 || anchorHref.toLowerCase().indexOf(myQuickLinkUrl.toLowerCase()) > -1) {
                            $("#createQuickLinkErrorMsgRow").html("This link already exists on this page, please scroll down to add the link.");
                            $(this).addClass("myLinkExists");
                            isMyLinkExists = true;
                        }
                    }
                }
            });
        });

        //Check if URL exists in the Custom Quick Links Section
        //Check if URL exists in the Quick links list section
        $("#CustomQuickLinksContainer li").each(function () {
            var anchor = $(this).find("a.custom-quick-link");
            if (anchor != null && anchor != undefined && anchor.length > 0) {
                var anchorHref = anchor.attr("href");
                if (anchorHref != null && anchorHref != undefined && anchorHref != "") {
                    var myQuickLinkUrl = $("#txtCreateQuickLinkUrl").val().trim();
                    if (myQuickLinkUrl.toLowerCase().indexOf(anchorHref.toLowerCase()) > -1 || anchorHref.toLowerCase().indexOf(myQuickLinkUrl.toLowerCase()) > -1) {
                        $("#createQuickLinkErrorMsgRow").html("This link already exists on this page, please scroll down to add the link.");
                        $($(this).children()[0]).addClass("myLinkExists");
                        anchor.addClass("myAnchorExists");
                        isMyLinkExists = true;
                    }
                }
            }
        });
        return isMyLinkExists;
    },
    ResetMyLinkExists: function () {
        $(".myLinkExists").each(function () {
            $(this).removeClass("myLinkExists");
        });
        $(".myAnchorExists").each(function () {
            $(this).removeClass("myAnchorExists");
        });
    },
    getCustomQuickLinks: function () {
        var intranetCustomQuickLinks = quickLinks.intranetCustomQuickLinks;
        $('#CustomQuickLinksContainer ul').html('');
        var customIntranetQuickLinksHtml = "";
        intranetCustomQuickLinks = intranetCustomQuickLinks.replace(/\|/g, '');
        intranetCustomQuickLinks = intranetCustomQuickLinks.split("@#@");
        for (var iCount = 0; iCount < intranetCustomQuickLinks.length; iCount++) {
            if (intranetCustomQuickLinks[iCount] != "" && intranetCustomQuickLinks[iCount] != undefined && intranetCustomQuickLinks[iCount] != null) {
                var linkDescription = intranetCustomQuickLinks[iCount].split(",")[0];
                var linkUrl = intranetCustomQuickLinks[iCount].split(",")[1];
                customIntranetQuickLinksHtml = '<li style="float: left; padding:0 5px;"><img src="/SiteCollectionImages/QLs/greenroom-default-ql-icon.png" style="height:40px;width:40px;"><a class="custom-quick-link" href="' + linkUrl + '" style="display:block;font-weight:bold;padding-top:10px;min-height:50px;"><span>' + linkDescription + '</span></a><a href="#" style="display:block" id="btn' + linkDescription + ',' + linkUrl + '" class="btnRemove" type="customLink"><span>(remove)</span></a></li>';
                $('#CustomQuickLinksContainer ul').append(customIntranetQuickLinksHtml);
            }
        }
    }
}
$(document).ready(function () {
    $("#txtCreateQuickLinkUrl").val("http://");
    $('#RecommendedlinksSection').hide();
    $('#QuickLinks').hide();
    $('#Revertquicklinks').click(function () {
        var reply = confirm('Are you sure of reverting quick links to default?');
        if (reply) {
            editLinks.getArticle();
        }
    });
    $("#btnCreateQuickLink").click(function () {
        editLinks.CreateQuickLink();
    });
});




/**************************************************Modified By Robin Start*************************************************************************/
var SiteQuickLinks = {
    clientContext: null,
    web: null,
    LoginName: null,
    followedSiteName: null,
    followedSiteUri: null,
    userProfile: null,
    fetchUserProfile: function (name, url, action, propertyName, isMultiValue) {
        SiteQuickLinks.clientContext = new SP.ClientContext.get_current();
        SiteQuickLinks.web = SiteQuickLinks.clientContext.get_site().get_rootWeb();
        currentUser = SiteQuickLinks.web.get_currentUser();
        oList = SiteQuickLinks.web.get_lists().getByTitle('Config');
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml('<View><Query><Where><Eq><FieldRef Name=\'Title\'/><Value Type=\'Text\'>Test</Value></Eq></Where></Query><ViewFields><FieldRef Name=\'FetchProfileProperties\'/><FieldRef Name=\'Title\'/></ViewFields></View>');
        collListItem = oList.getItems(camlQuery);
        SiteQuickLinks.clientContext.load(collListItem);
        SiteQuickLinks.clientContext.load(currentUser);
        SiteQuickLinks.clientContext.executeQueryAsync(function () {
            SiteQuickLinks.fetchUserProfileSuccess(name, url, action, propertyName, isMultiValue)
        }, SiteQuickLinks.failure);
    },
    fetchUserProfileSuccess: function (name, url, action, propertyName, isMultiValue) {
        var listItemInfo = '';
        var listItemEnumerator = collListItem.getEnumerator();
        while (listItemEnumerator.moveNext()) {
            var oListItem = listItemEnumerator.get_current();
            listItemInfo = oListItem.get_item('FetchProfileProperties');
        }
        userProfile = listItemInfo;
        SiteQuickLinks.getUserPropertiesFromProfile(name, url, action, propertyName, isMultiValue);
    },
    getUserPropertiesFromProfile: function (name, url, action, propertyName, isMultiValue) {
        var userProfileInfo = userProfile.split(",");
        LoginName = currentUser.get_loginName();
        //Code update for ADFS
        if (LoginName.split('|').length == 2) {
            LoginName = LoginName.split('|')[1];
        }
        var IntranetQuickLinksValue = null;
        $.ajax({
            url: "/_api/SP.UserProfiles.PeopleManager/GetPropertiesFor(accountName=@v)?@v='" + LoginName + "'",
            async: false,
            headers: {
                Accept: "application/json;odata=verbose"
            },
            success: function (data) {
                for (var i = 0; i < data.d.UserProfileProperties.results.length; i++) {
                    if (data.d.UserProfileProperties.results[i].Key == propertyName)
                        IntranetQuickLinksValue = data.d.UserProfileProperties.results[i].Value;

                }
            },
            error: function (jQxhr, errorCode, errorThrown) {
            }
        });
        if (action == "create")
            SiteQuickLinks.AddUserProfileValue(name, url, IntranetQuickLinksValue, propertyName, isMultiValue);
        else if (action == "remove")
            SiteQuickLinks.RemoveUserProfileValue(name, url, IntranetQuickLinksValue, propertyName, isMultiValue);
        else
            SiteQuickLinks.Load(IntranetQuickLinksValue);

    },
    failure: function (sender, args) {
        $("#createQuickLinkRow-compliment-loader").hide();
    },
    AddUserProfileValue: function (name, url, IntranetQuickLinksValue, propertyName, isMultiValue) {
        var formattedValue = name + ',' + url;
        if (IntranetQuickLinksValue == null || IntranetQuickLinksValue == "" || IntranetQuickLinksValue == "undefined") {
            formattedValue = formattedValue + '@#@';
            SiteQuickLinks.updateUserProfileValue(LoginName, propertyName, formattedValue, isMultiValue);
        } else {
            if (IntranetQuickLinksValue.indexOf(formattedValue) == -1) {
                formattedValue = IntranetQuickLinksValue + formattedValue + '@#@';
                SiteQuickLinks.updateUserProfileValue(LoginName, propertyName, formattedValue, isMultiValue);
            }
            else {
                location.reload();
            }
        }
        //location.reload();
    },
    setQuickLinkHtml: function (qucikString) {
        quickLinks.quicklinkhtml1 = "";
        quickLinks.quicklinkhtml2 = "";
        var propertiesDetails = [];
        if (qucikString) {
            var propertyName = 'quicklinkhtml1';
            if (qucikString.length < 3600) {
                quickLinks.quicklinkhtml1 = qucikString;
                quickLinks.quicklinkhtml2 = "";
            }
            else if (qucikString.length > 3600 && qucikString.length < 7200) {
                quickLinks.quicklinkhtml1 = qucikString.substr(0, 3600);
                quickLinks.quicklinkhtml2 = qucikString.substr(3600);

            }
        }
        propertiesDetails.push({
            "IsMultiValued": false,
            "Name": "quicklinkhtml1",
            "Value": quickLinks.quicklinkhtml1
        });
        propertiesDetails.push({
            "IsMultiValued": false,
            "Name": "quicklinkhtml2",
            "Value": quickLinks.quicklinkhtml2
        });
        var domain, userName;

        if (quickLinks.LoginName != undefined && quickLinks.LoginName != null && quickLinks.LoginName != "") {
            // code Updated for ADFS
            if (quickLinks.LoginName.split('\\').length == 2) {
                domain = quickLinks.LoginName.split("\\")[0];
                userName = quickLinks.LoginName.split("\\")[1];
            }
            else {
                domain = "";
                userName = quickLinks.LoginName;
            }
        }
        var siteUrl = GetMySiteUrlFromLocalStarageJson();
        var profileProperties = {
            "UserName": userName,
            "Domain": domain,
            "SiteUrl": siteUrl,
            "PropertiesDetails": propertiesDetails
        };
        SiteQuickLinks.saveProfileProperties(profileProperties);
    },

    /* Post Function to update user profile custom property*/

    saveProfileProperties: function (profileProperties) {
        var result;
        $.ajax({
            url: _spPageContextInfo.siteAbsoluteUrl + "/_vti_bin/Viacom.Intranet.WCFService/IntranetService.svc/SaveProfileProperties",
            type: "POST",
            data: JSON.stringify(profileProperties),
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                localStorage.clear();
                location.reload();
            },
            error: function (data) {
                localStorage.clear();
                location.reload();
            }
        });

    },
    updateUserProfileValue: function (LoginName, propertyName, propertyValue, isMultiValue) {
        propertyValue = propertyValue.replace(/\|/g, '');
        propertyValue = propertyValue.replace(/\&/g, '&amp;');

        var propertyValues = propertyValue.split('@#@');
        var myLinks = "";
        var quickLinks = "";
        for (var iMyLink = 0; iMyLink < propertyValues.length; iMyLink++) {
            if (propertyValues[iMyLink] != "" && propertyValues[iMyLink] != null && propertyValues[iMyLink] != undefined) {

                if (!isNaN(propertyValues[iMyLink].split("$$")[0])) {
                    quickLinks += propertyValues[iMyLink] + "@#@";
                }
                else {
                    quickLinks += propertyValues[iMyLink] + "@#@#*#";
                }
            }
        }
        var domain, userName;

        if (LoginName != undefined && LoginName != null && LoginName != "") {
            // code Updated for ADFS
            if (LoginName.split('\\').length == 2) {
                domain = LoginName.split("\\")[0];
                userName = LoginName.split("\\")[1];
            }
            else {
                domain = "";
                userName = LoginName;
            }
        }
        var userprofileServiceURL = GetUserProfileUrlFromLocalStarageJson();
        var siteUrl = GetMySiteUrlFromLocalStarageJson();
        $.support.cors = true;
        var jsonDataType = "json";
        if (isMultiValue) {
            $.ajax({
                url: userprofileServiceURL,
                dataType: jsonDataType,
                type: "GET",
                data: { domain: domain, userName: userName, propertyName: propertyName, value: quickLinks, siteUrl: siteUrl, isMultiValued: "true" },
                success: function (data) {
                    //Grab our data from Ground Control
                    if (data.indexOf("Commit Success") > -1) {
                        SiteQuickLinks.setQuickLinkHtml();

                    }
                    else {
                        if (console != undefined && console != null) {

                        }
                    }
                },
                error: function (data) {
                    /* Added code to check the status code and status text in case of IE9
                    as IE couldn't parse the response XML and falls into the error function
                    with all the successful processing at the back end and statusText=success and status=200 */
                    if (data.statusText.toLowerCase() == "success" && data.status == 200) {
                        location.reload();
                    }
                        //If any errors occurred - detail them here
                    else if (console != undefined && console != null) {

                    }
                }
            });
        }
        else {
            $.ajax({
                url: userprofileServiceURL,
                dataType: jsonDataType,
                type: "GET",
                data: { domain: domain, userName: userName, propertyName: propertyName, value: quickLinks, siteUrl: siteUrl },
                success: function (data) {
                    //Grab our data from Ground Control
                    if (data.indexOf("Commit Success") > -1) {
                        SiteQuickLinks.setQuickLinkHtml();
                    }
                    else {
                        if (console != undefined && console != null) {
                        }
                    }
                },
                error: function (data) {
                    /* Added code to check the status code and status text in case of IE9
                    as IE couldn't parse the response XML and falls into the error function
                    with all the successful processing at the back end and statusText=success and status=200 */
                    if (data.statusText.toLowerCase() == "success" && data.status == 200) {
                        location.reload();
                    }
                        //If any errors occurred - detail them here
                    else if (console != undefined && console != null) {
                    }
                }
            });
        }
    },
    RemoveUserProfileValue: function (name, url, IntranetQuickLinksValue, propertyName, isMultiValue) {
        var formattedValue = name + ',' + url + '@#@';
        if (IntranetQuickLinksValue.indexOf(formattedValue) > -1) {
            IntranetQuickLinksValue = IntranetQuickLinksValue.replace(formattedValue, "");
        }
        SiteQuickLinks.updateUserProfileValue(LoginName, propertyName, IntranetQuickLinksValue, isMultiValue);
        // location.reload();
    },
    Load: function (IntranetQuickLinksValue) {
        var size;
        var formattedName;
        var strHtml = "";
        var siteName = [];
        var siteUrl = [];
        var url = window.location.protocol + "//" + window.location.host + _spPageContextInfo.siteServerRelativeUrl;
        $.ajax({
            url: "/_api/social.following/my/followed(types=4)",
            type: "GET",
            headers: {
                "Accept": "application/json;odata=verbose"
            },
            async: false,
            success: function (data, textStatus, xhr) {
                for (i = 0; i < data.d.Followed.results.length; i++) {

                    var followedSiteName = data.d.Followed.results[i].Name;
                    var followedSiteUri = data.d.Followed.results[i].Uri;
                    formattedName = followedSiteName + "," + followedSiteUri + "@#@";
                    //var site = followedSiteUri.split(url);
                    var sitebase = followedSiteUri.split('://');
                    site = sitebase[1].split('/');
                    var baseurl = sitebase[0] + "://" + site[0];

                    followedSiteUri = baseurl;
                    for (var partindex = 1; partindex < site.length; partindex++) {
                        followedSiteUri = followedSiteUri + "/" + site[partindex];
                    }

                    siteName.push(site[1]);
                    siteUrl.push([followedSiteUri, followedSiteName]);
                    siteName = siteName.filter(onlyUnique);

                }

            }

        });
        /* Start Uncomment the below section if need to show the followed site in Edit Quick Links section  */
        //var AddQuickLinksForPages = $("#AddQuickLinksForPages");
        //AddQuickLinksForPages.append("<h2>Recommended Links</h2>");
        //debugger;
        //SiteQuickLinks.buildHtml(siteName, siteUrl, IntranetQuickLinksValue);
        //Add message in case user is not following the site.

        //if(AddQuickLinksForPages.children().length <= 1)
        //{
        //	AddQuickLinksForPages.append("<div>Don't see any options? Start following sites</div>");
        //}
        /* End Uncomment the below section if need to show the followed site in Edit Quick Links section  */
    },

    buildHtml: function (siteName, siteUrl, IntranetQuickLinksValue) {
        var html = ""
        var url = window.location.protocol + "//" + window.location.host + _spPageContextInfo.siteServerRelativeUrl;
        for (var i = 0; i < siteName.length; i++) {
            if (siteName[i] != "") {
                html = '<div style=";margin-right:30px;float:left;display:block;"><div><span class="greenroom-quicklinks-sitename">' + siteName[i] + '</span></div><ul>';
                for (var j = 0; j < siteUrl.length; j++) {
                    if ((siteUrl[j][0]).indexOf(url + siteName[i]) > -1) {
                        var link = siteUrl[j][1] + ',' + siteUrl[j][0] + '@#@';
                        IntranetQuickLinksValue = IntranetQuickLinksValue.replace(/\ï¼†/g, '&');
                        if (IntranetQuickLinksValue.indexOf(link) == -1) {
                            html += '<li class="quick-link-pages" style="padding:0 5px;overflow:auto; margin-bottom: 5px;min-height: 20px; height:auto;width:100%"><a href="' + (siteUrl[j])[0] + '" style="display: block;float: left;margin-right: 10px;"><span>' + (siteUrl[j])[1] + '</span></a>' +
                                '<a href="#" onclick="SiteQuickLinks.fetchUserProfile(\'' + siteUrl[j][1] + '\',\'' + siteUrl[j][0] + '\',\'create\');"><span class="hover" style="padding:0 5px; display: none"><img src="../SiteCollectionImages/icons/greenroom-panel-icon-plus.png">Add to quick links</span></a>' +
                                '<span class="quick-links-full" style="padding:0 5px; display: none"><img src="../SiteCollectionImages/icons/greenroom-panel-icon-delete.png" style="width:10px;height:10px"><span style="padding:0 5px;float:right">Quick links full</span></span>' + '</li>';
                            //Changes made here                       
                        } else {
                            html += '<li class="quick-link-pages" style="padding:0 5px;margin-bottom:5px; overflow:auto;min-height: 20px; height:auto;width:100%"><a href="#" style="display: block;float: left;margin-right: 10px;background-color: white;"><span >' + (siteUrl[j])[1] + '</span></a><img src="../SiteCollectionImages/icons/greenroom-panel-icon-circle-check.png" style="height:10px;width:10px;">' +
                              '<a href="#" onclick="SiteQuickLinks.fetchUserProfile(\'' + siteUrl[j][1] + '\',\'' + siteUrl[j][0] + '\',\'remove\')"><span style="padding:0 5px;float:right;">Remove</span></a>'
                          + '</li>';
                        }

                    }
                }
                html += '</ul></div>';
                $('#AddQuickLinksForPages').append(html);
            }

        }
        $('.quick-link-pages').hover(function () {
            var myQuickLinksLength = $('#EditQuickLinksContainer').find('li').length;
            var myCustomLinksLength = $('#CustomQuickLinksContainer').find('li').length;
            var totalLinksLength = myQuickLinksLength + myCustomLinksLength;
            if (totalLinksLength < 13) {
                $(this).find("span.hover").toggle();
            } else {
                $(this).find("span.quick-links-full").toggle();

            }
        });
    },
}


function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

function clearQuickLinksValue(quickLinksValue) {
    var finalQuickLinksValue = "";
    for (var i = 0; i < quickLinksValue.split('@#@').length; i++) {
        if (quickLinksValue.split('@#@')[i] != "") {
            finalQuickLinksValue += quickLinksValue.split('@#@')[i] + '@#@';
        }
    }
    return finalQuickLinksValue;
}


/**************************************************Modified By Robin END*************************************************************************/