var OfficesEventsCalendar = {
    ListTitle: 'cal',
    ColumnNames: {
        Title: 'Title',
        ID: 'ID',
        StartDate: 'EventDate',
        EndDate: 'EndDate'
    },
    HeaderOverride: function (ctx) {
        if (ctx.ListTitle.toLowerCase() === OfficesEventsCalendar.ListTitle) {
            return '<div class="event-calendar-container">';
        }
        else {
            return RenderHeaderTemplate(ctx);

        }
    },
    FooterOverride: function (ctx) {
        if (ctx.ListTitle.toLowerCase() === OfficesEventsCalendar.ListTitle) {
            return '</div>';
        }
        else {
            return RenderFooterTemplate(ctx);
        }
    },
    ItemOverride: function (ctx) {
        if (ctx.ListTitle.toLowerCase() === OfficesEventsCalendar.ListTitle) {
            var rowItemHtml = '';
            var fileRef = ctx.displayFormUrl + "&ID=" + ctx.CurrentItem[OfficesEventsCalendar.ColumnNames.ID];
            var title = ctx.CurrentItem[OfficesEventsCalendar.ColumnNames.Title];
            var timeString = "";
            var startDate = ctx.CurrentItem[OfficesEventsCalendar.ColumnNames.StartDate];
            if (startDate) {
                timeString = startDate.split(" ")[0];
            }
            var endDate = ctx.CurrentItem[OfficesEventsCalendar.ColumnNames.EndDate];
            if (endDate) {
                if (timeString) {
                    timeString += " - ";
                }
                timeString += endDate.split(" ")[0];
            }
            rowItemHtml = '<div class="event-calendar-item"><div class="event-calendar-title"><a href="' + fileRef + '">' + title + '</a></div><div class="event-calendar-time">' + timeString + '</div> </div>';

            return rowItemHtml;
        }
        else {
            return RenderItemTemplate(ctx);
        }
    }
};
(function () {
    var itemCtx = {};
    itemCtx.Templates = {};
    itemCtx.listTemplate = 106;
    itemCtx.Templates.Header = OfficesEventsCalendar.HeaderOverride;
    itemCtx.Templates.Footer = OfficesEventsCalendar.FooterOverride;;
    itemCtx.Templates.Item = OfficesEventsCalendar.ItemOverride;
    SPClientTemplates.TemplateManager.RegisterTemplateOverrides(itemCtx);

})();