if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
{    
      Add-PSSnapin Microsoft.SharePoint.PowerShell
}

function UpdateCSWP()
{
$spFile.CheckOut()
#Initialise the Web part manager for the specified profile page.
$spWebPartManager = $spWeb.GetLimitedWebPartManager($spFile.URL, [System.Web.UI.WebControls.WebParts.PersonalizationScope]::Shared)

#Remove the Web part from that page
foreach ($webpart in ($spWebPartManager.WebParts | Where-Object {$_.Title -eq $webpartTitle}))
{
    write-host "Web is:" + $spWeb +": Existing Web part - " + $webpart.Title + " : " + $webpart.ID
	#$webpart.NumberOfItems =2;  
    $dataProviderJSON = $webpart.DataProviderJSON
    $x = $dataProviderJSON | ConvertFrom-Json
    #$x.SourceName = $webpartResultSourceName
    $x.QueryTemplate = $webpartQuery
    $webpart.DataProviderJSON  = $x | ConvertTo-Json
	write-host "Updated " + $webpartTitle
	$spWebPartManager.SaveChanges($webpart)
}
   
$spFile.Update()
$spFile.CheckIn("Update page via PowerShell",[Microsoft.SharePoint.SPCheckinType]::MajorCheckIn)
$spFile.Publish("");
$spWeb.Update()
$spWeb.Dispose()
}

function CallUpdateCSWP([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)
	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
	if($Error.Count -eq 0)
	{
		$webAppFile = $cfg
		$error.clear()
		$sites = $webAppFile.Sites
		foreach($site in $Sites.Site)	
		{
		# Get the list for operation to perform
		$siteUrl= $site.SiteUrl
		$pageName = $site.PageName
		$webpartTitle = $site.WebPartName
		$webpartQuery = $site.Query
		#$webpartResultSourceName = $site.ResultSourceName

		#****Give the site url where one want to update page properties***** 

		$spWeb = get-spWeb $siteUrl
		$spFile = $spWeb.GetFile($pageName)

		###Calling function
		UpdateCSWP
		}
	}
}




