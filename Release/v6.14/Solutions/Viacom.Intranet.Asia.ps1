echo ""
echo  "Start Deploying Viacom.Intranet.Asia..."
echo ""
echo  "1 Removing wsp" 
$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'}
if ($snapin -eq $null) 
{
    Write-Host "Loading SharePoint Powershell Snapin" -ForegroundColor Green
    Add-PSSnapin "Microsoft.SharePoint.Powershell"
}

$SolutionName = "Viacom.Intranet.Asia.wsp"
Write-Host -f Yellow 'Uninstalling solution '$SolutionName
$Solution = Get-SPSolution | ? {($_.Name -eq $SolutionName) -and ($_.Deployed -eq $true)}

if($Solution.ContainsWebApplicationResource)
{
	Uninstall-SPSolution $SolutionName -AllWebApplications -Confirm:$false
}
else
{
	Uninstall-SPSolution $SolutionName -Confirm:$false
}

while ($Solution.JobExists)
 {
		Start-Sleep 2
 }
#  WaitForJobToFinish($SolutionName)
if($Error.Count -eq 0)
{
  Write-Host -f Green 'Solution '$SolutionName' has been uninstalled successfully.'
}
else
{
  Write-Host -f Red 'Error in uninstalling solution '$SolutionName
  $error.clear()
}

Write-Host -f Yellow 'Removing solution '$SolutionName
$Solution = Get-SPSolution | ? {($_.Name -eq $SolutionName) -and ($_.Deployed -eq $false)}

if($Solution -ne $null)
{
	Remove-SPSolution $SolutionName -Confirm:$false   
}

while ($Solution.JobExists)
{
  Start-Sleep 2
}
# WaitForJobToFinish($SolutionName)
if($Error.Count -eq 0)
{
	Write-Host -f Green 'Solution '$SolutionName' has been removed successfully.'
}
else
{
	Write-Host -f Red 'Error in removing solution '$SolutionName
	$error.clear()
}
echo ""
echo  "2 Adding Viacom.Intranet.Asia.wsp ..."

$CurrentPath=Split-Path ((Get-Variable MyInvocation -Scope 0).Value).MyCommand.Path
Add-SPSolution -LiteralPath $CurrentPath"\Viacom.Intranet.Asia.wsp"

Start-Sleep -s 10

echo ""
echo  "3 Insalling Viacom.Intranet.Asia.wsp ..."

Install-SPSolution -Identity Viacom.Intranet.Asia.wsp -GACDeployment -Force -Confirm:$false


	$JobName = "*solution-deployment*$SolutionName*"
	$job = Get-SPTimerJob | ?{ $_.Name -like $JobName }
	if ($job -eq $null) 
	{
		Write-Host 'Timer job not found'
	}
	else
	{
		$JobFullName = $job.Name
		Write-Host -NoNewLine "Waiting to finish job $JobFullName"
		while ((Get-SPTimerJob $JobFullName) -ne $null) 
		{
			Write-Host -NoNewLine .
			Start-Sleep -Seconds 2
		}
		Write-Host  "Finished waiting for job.."
	}

echo "Done"
echo ""

