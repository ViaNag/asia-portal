﻿$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

function ApplyJSLink([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$lookUpXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green      

    $sitecollectionUrl =  $lookUpXml.SiteCollection.SiteUrl

    # Get Site Collection URL
    $siteCollection = Get-SPSite $sitecollectionUrl
    
    
    
    $rootWeb = $siteCollection.RootWeb 	
    
	Write-Host "In the Site Collection"  $web.url  

    $lookUpXml.SiteCollection.Subsite |
	ForEach-Object {  

        $Subsite = $_.SubSiteUrl
        
        $web = Get-SPWeb -Identity $Subsite	

        $_.mapping |
	    ForEach-Object {
	    		
           $ListName = $_.ListURLWithView

           $JSLink = $_.JSLink

           try
           {
                $file = $web.GetFile($ListName)
                $file.CheckOut()
                
                $webPartManager = $web.GetLimitedWebPartManager($ListName, [System.Web.UI.WebControls.WebParts.PersonalizationScope]::Shared)
                $webpart = $webPartManager.WebParts[0]
                $webpart.JSLink = $JSLink
                
                $webPartManager.SaveChanges($webpart)
                $file.CheckIn("")
                $file.update() 

                Write-Host "JSLink added successfully" -ForegroundColor Green
           }
           catch
           {
                Write-Host $file "not found" in $web -ForegroundColor Red
           }            
        }
    }
        
	$rootWeb.Dispose()				
}
