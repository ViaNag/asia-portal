﻿$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

function UpdateDescription([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$lookUpXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green  
	
    $lookUpXml.Sites.Site |
	ForEach-Object {
        $SiteURL = $_.SiteURL
        
        $siteWeb = Get-SPWeb $SiteURL        
        
        $ListCollection = $siteWeb.Lists
        Foreach ($listName in $ListCollection)
        {
                #$listName.Description = $listName.Title
                #$list.Update()

                ForEach($culture in $siteWeb.SupportedUICultures)
                {
                    [System.Threading.Thread]::CurrentThread.CurrentUICulture=$culture;
                    $listName.Description=$listName.Title;
                    $listName.Update();
                }
        }
        Write-Host "Description updaed for all lists " $siteWeb.Url -ForegroundColor Green
        $siteWeb.Dispose()
    }     
    		
}
