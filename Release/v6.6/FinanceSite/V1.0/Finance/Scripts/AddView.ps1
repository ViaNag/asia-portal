﻿$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

function AddViewInList([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$lookUpXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green      

    
    # List name
    $listName = "Externally shared Content"
    
    #My Shared Content view
	$mySharedContentViewName = "My Shared Content"
    
    #Add the column names from the ViewField property to a string collection
	$viewFields = New-Object System.Collections.Specialized.StringCollection
	$viewFields.Add("AdSpaceName") > $null
	$viewFields.Add("SharedBy") > $null 
    $viewFields.Add("SharedOn") > $null
    $viewFields.Add("SharedWith") > $null
    $viewFields.Add("AllowDownload") > $null
    $viewFields.Add("ExternallySharedContentStatus") > $nul
    $viewFields.Add("ExpiresOn") > $null
    $viewFields.Add("Message") > $null
	$viewFields.Add("ListName") > $null
    $viewFields.Add("FilePath") > $null
    $viewFields.Add("ItemID") > $null
	$viewFields.Add("SiteURL") > $null
    $viewFields.Add("LiveShare") > $null  
     
    $viewQuery = "<Where><Eq><FieldRef Name='SharedBy' /><Value Type='Integer'><UserID Type='Integer' /></Value></Eq></Where><GroupBy><FieldRef Name='ContentGroup' /></GroupBy>"


	#DefaultView property
	$mySharedContentDefaultView = $false

	#RowLimit property
	$viewRowLimit = 100
	#Paged property
	$viewPaged = $true

    # ByContent view
	$ByContentViewName = "ByContentGroup"
    
    #Add the column names from the ViewField property to a string collection
	$viewFieldsByContent = New-Object System.Collections.Specialized.StringCollection
	$viewFieldsByContent.Add("AdSpaceName") > $null
    $viewFieldsByContent.Add("SharedBy") > $null 
    $viewFieldsByContent.Add("SharedOn") > $null
    $viewFieldsByContent.Add("SharedWith") > $null
    $viewFieldsByContent.Add("AllowDownload") > $null
    $viewFieldsByContent.Add("ExternallySharedContentStatus") > $null
    $viewFieldsByContent.Add("ExpiresOn") > $null
    $viewFieldsByContent.Add("Message") > $null
    $viewFieldsByContent.Add("ListName") > $null
    $viewFieldsByContent.Add("FilePath") > $null
    $viewFieldsByContent.Add("LiveShare") > $null   
    

    $viewQueryByContent = "<Where></Where><GroupBy><FieldRef Name='ContentGroup' /></GroupBy>"


	#DefaultView property
	$ByContentDefaultView = $false

	#RowLimit property
	$viewRowLimitByContent = 100
	#Paged property
	$viewPagedByContent = $true


    # ByUser view
	$ByUserViewName = "ByUser"
    
    #Add the column names from the ViewField property to a string collection
	$viewFieldsByUser = New-Object System.Collections.Specialized.StringCollection
	$viewFieldsByUser.Add("AdSpaceName") > $null
    $viewFieldsByUser.Add("SharedBy") > $null 
    $viewFieldsByUser.Add("SharedOn") > $null
    $viewFieldsByUser.Add("SharedWith") > $null
    $viewFieldsByUser.Add("AllowDownload") > $null
    $viewFieldsByUser.Add("ExternallySharedContentStatus") > $null
    $viewFieldsByUser.Add("ExpiresOn") > $null
    $viewFieldsByUser.Add("Message") > $null
    $viewFieldsByUser.Add("ListName") > $null
    $viewFieldsByUser.Add("FilePath") > $null
    $viewFieldsByUser.Add("LiveShare") > $null   
    

    $viewQueryByUser = "<Where></Where><GroupBy><FieldRef Name='SharedBy' /><FieldRef Name='ContentGroup' /></GroupBy>"


	#DefaultView property
	$ByUserDefaultView = $false

	#RowLimit property
	$viewRowLimitByUser = 100
	#Paged property
	$viewPagedByUser = $true

    $lookUpXml.Sites.Site |
	ForEach-Object {  

        $siteURL = $_.SiteUrl
        
        $web = Get-SPWeb -Identity $siteURL	

        $list = $web.Lists[$ListName]
        $viewMyShared = $list.Views[$mySharedContentViewName]

        
			
        if($viewMyShared -ne $null)
        {
        	#Delete this view from the list
        	$list.Views.Delete($viewMyShared.ID)
        }

        $mySharedContentView = $list.Views.Add($mySharedContentViewName, $viewFields, $viewQuery, $viewRowLimit, $viewPaged, $mySharedContentDefaultView)

        Write-Host ("View '" + $mySharedContentView.Title + "' created in list '" + $list.Title + "' on site " + $web.Url) -ForegroundColor Green

        
        $viewByContent = $list.Views[$ByContentViewName]

        if($viewByContent -ne $null)
        {
        	#Delete this view from the list
        	$list.Views.Delete($viewByContent.ID)
        }

        $ByContentView = $list.Views.Add($ByContentViewName, $viewFieldsByContent, $viewQueryByContent, $viewRowLimitByContent, $viewPagedByContent, $ByContentDefaultView)
        

        Write-Host ("View '" + $ByContentView.Title + "' created in list '" + $list.Title + "' on site " + $web.Url) -ForegroundColor Green

        $viewByUser = $list.Views[$ByUserViewName]

        if($viewByUser -ne $null)
        {
        	#Delete this view from the list
        	$list.Views.Delete($viewByUser.ID)
        }

        $ByUserView = $list.Views.Add($ByUserViewName, $viewFieldsByUser, $viewQueryByUser, $viewRowLimitByUser, $viewPagedByUser, $ByUserDefaultView)

        Write-Host ("View '" + $ByUserView.Title + "' created in list '" + $list.Title + "' on site " + $web.Url) -ForegroundColor Green

        $web.Dispose()

    }        			
}
