﻿$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

function ChangeDisplayNames([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$lookUpXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green      
    
	# Get Site Collection URL
	$sitecollectionUrl =  $lookUpXml.Sites.SiteCollection
    $siteCollection = Get-SPSite $sitecollectionUrl
    $rootWeb = $siteCollection.RootWeb 		
	
	# Change Display Name of Site Column
	$lookUpXml.Sites.Column |
	ForEach-Object {
        $internalName = $_.InternalName
		$displayName = $_.DisplayName
        $siteColumn = $rootWeb.Fields.GetFieldByInternalName($internalName)
		if($siteColumn -ne $null)
		{
			$siteColumn.Title=$displayName       
			$siteColumn.Update($true)

			Write-Host "Site Column" $internalName "display name has been changed." -ForegroundColor Green
			$siteColumn = $null
		}
        
	}
	$rootWeb.Dispose()				
}
