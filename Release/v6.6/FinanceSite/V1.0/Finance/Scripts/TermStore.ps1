. .\scripts\SP2010Functions.ps1

$CmdletName="Amsurg.CorporateConnect.Utilities"

function CreateTermStore([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$TermStoreData =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}

	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green
	 
	Write-Host "Accessing Site : Process Starting ...." -ForegroundColor Green
	$site = Get-SPSite $Configuration.Configuration.Farm.CentralAdminURL
   
	 if($Error.Count -gt 0) 
	 {     
		Write-Host "Error: Accessing Site. Exiting Script... " -ForegroundColor Red
		Stop-Transcript
		Stop-SPAssignment -Global
		exit 1
	 } 
	 
	 Write-Host "Success: Accessing Site." -ForegroundColor Green
	 
	 Write-Host "Accessing Taxonomy Session : Process Starting ....." -ForegroundColor Green
	 $session = new-object Microsoft.SharePoint.Taxonomy.TaxonomySession($site) 
	 
	 if($Error.Count -gt 0) 
	 {     
		Write-Host "Error: Accessing Taxonomy Session. Exiting Script... " -ForegroundColor Red
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	 }
	 
	 Write-Host "Success: Accessing Taxonomy Session : Process Completed .....`n" -ForegroundColor Green

	 Write-Host "Creating Meta Store  : Process Starting ..." -ForegroundColor Green
	 
	 try
	 {
	 	$termstore = $session.TermStores[$TermStoreData.termstore.name]	
		$TermStoreData.termstore.group |
		 ForEach-Object {
		 	# create the group
		 	if ($termstore.Groups[$_.name.Replace("&", "＆")] -eq $null)
		 	{
				$group = $termstore.CreateGroup($_.name);
		   		Write-Host -ForegroundColor Green "Added group : " $_.name "`n"		
		 	}
			else
			{
				$group = $termstore.Groups[$_.name.Replace("&", "＆")]
			}
			
		 	$_.termset |
		 	ForEach-Object {
		 		## create the termset
				if($group.TermSets[$_.name.Replace("&", "＆")] -eq $null)
				{
		    		$termset = $group.CreateTermSet($_.name);
		  			if($_.isAvailableForTagging -eq "false")
	  				{
						$termset.IsAvailableForTagging = $false;	
	  				}
	  				else
	  				{	
						$termset.IsAvailableForTagging = $true;	
	  				}	
				
	  				
		    		Write-Host -ForegroundColor Green "Added termset : " $_.name; "`n"		
				}
				else
				{
					$termset = $group.TermSets[$_.name.Replace("&", "＆")]
					if($_.Action -eq "Rename")
					{
						$termset.Name = $_.Name
					}
				}

		    	SetTerms -termsetitem $termset -parentnode $_;
				if($_.Action -eq "Rename")
				{
					$termset.Name = $_.NewName
					Write-Host "Rename Term " $_.name " to " $_.NewName -ForegroundColor Green
				}
	 $termstore.CommitAll()

		 	}
		 }
		 
	}
	catch
	{
		$termstore.RollbackAll();
		Write-Host "Exception while creating Meta store. Exiting script..." -ForegroundColor Red
		Stop-Transcript
		Stop-SPAssignment -Global
		
		Exit -1
	}
}


function AddNavigation([String]$ConfigFileName = "")
{
    Write-Host "Navigation Script ....." -ForegroundColor Green
	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string] $Path = Get-Location
	$Path = $Path + "\dll\Amsurg.CorporateConnect.Cmdlets.dll"
	installutil $Path
    Add-PSSnapin $CmdletName

	[string]$xmlpath = $ConfigFileName
	 
	$TermStoreData =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}

	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green
	 
	Write-Host "Accessing Site : Process Starting ...." -ForegroundColor Green
	$site = Get-SPSite $Configuration.Configuration.Farm.CentralAdminURL
   
	 if($Error.Count -gt 0) 
	 {     
		Write-Host "Error: Accessing Site. Exiting Script... " -ForegroundColor Red
		Stop-Transcript
		Stop-SPAssignment -Global
		exit 1
	 } 
	 
	 Write-Host "Success: Accessing Site." -ForegroundColor Green
	 
	 Write-Host "Accessing Taxonomy Session : Process Starting ....." -ForegroundColor Green
	 $session = new-object Microsoft.SharePoint.Taxonomy.TaxonomySession($site) 
	 
	 if($Error.Count -gt 0) 
	 {     
		Write-Host "Error: Accessing Taxonomy Session. Exiting Script... " -ForegroundColor Red
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	 }
	 
	 Write-Host "Success: Accessing Taxonomy Session : Process Completed .....`n" -ForegroundColor Green

	 Write-Host "Creating Meta Store  : Process Starting ..." -ForegroundColor Green
	 
	 try
	 {
	    
	 	$termstore = $session.TermStores[$TermStoreData.termstore.name]			
		$TermStoreData.termstore.group |
		 ForEach-Object {		 	
            $group = $termstore.Groups[$_.name.Replace("&", "＆")]        			     
		 	$_.termset |
		 	ForEach-Object {
							if($_.isAvailableForNavigation)
	  						{ 
									$weburl= $_.isAvailableForNavigation

									$web= Get-SPWeb $_.isAvailableForNavigation
				 					$spsite = $web.Site

 									$navSettings = New-Object  Microsoft.SharePoint.Publishing.Navigation.WebNavigationSettings($web)
									$taxSession = Get-SPTaxonomySession -Site $spsite                      

									#Quick Launch
									$navSettings.CurrentNavigation.Source = 2
									$navSettings.CurrentNavigation.TermStoreId = $termStore.Id                         
									$termSet = $group.TermSets[$_.name]
									$navSettings.CurrentNavigation.TermSetId = $termSet.Id
						
									#Global Navigation
									$navSettings.GlobalNavigation.Source = 2
									$navSettings.GlobalNavigation.TermStoreId = $termStore.Id
									$navSettings.GlobalNavigation.TermSetId = $termSet.Id

									$navSettings.AddNewPagesToNavigation = $false
									$navSettings.CreateFriendlyUrlsForNewPages = $false
									$navSettings.Update()
	  						}

							$termstore.CommitAll()

                            $customSortOrder=""
                            $termSet = $group.TermSets[$_.name]								
                            # Gets each term from the termset
                   $_.term |ForEach-Object{                                                                                 
                        $term=$termSet.Terms[$_.Name]  						
                        #Gets the term id and adds to the custom sortorder                    
                        $customSortOrder=$customSortOrder+$term.id.ToString()+":"                                                   
						if($_.simpleLink)
                        { 
                            $Link=$_.simpleLink 													                                                                        
                            Set-TopNavigation -url $weburl -simpleLink $Link -guid $term.id.ToString()
                            write-host "Navigation page added successfully to termset-" $term.Name 
                        }                                                                
                    }                   	  
                   #Sets the custom sort order of the termset
		           $termSet.CustomSortOrder = $customSortOrder 
                   write-host "Termset order of termset-"$termSet.name"changed to custom order successfully."
		 		   ## set the navigation
				   
				$termstore.CommitAll()
		 	}
			Write-Host "Navigation updated succesfully for site "	-ForegroundColor Green				 
		 }
		
	}
	catch
	{
		Write-Host "Exception while creating Meta store. Exiting script..." -ForegroundColor Red
		Stop-Transcript
		Stop-SPAssignment -Global
		
		Exit -1
	}
}

# adds the pages url in the existing navigation.
function AddUrlToNavigation([String]$ConfigFileName = "")
{
    Write-Host "Navigation Script ....." -ForegroundColor Green
	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string] $Path = Get-Location
	$Path = $Path + "\dll\Amsurg.CorporateConnect.Cmdlets.dll"
	installutil $Path
    Add-PSSnapin $CmdletName

	[string]$xmlpath = $ConfigFileName
	 
	$TermStoreData =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}

	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green
	 
	Write-Host "Accessing Site : Process Starting ...." -ForegroundColor Green
	$site = Get-SPSite $Configuration.Configuration.Farm.CentralAdminURL
   
	 if($Error.Count -gt 0) 
	 {     
		Write-Host "Error: Accessing Site. Exiting Script... " -ForegroundColor Red
		Stop-Transcript
		Stop-SPAssignment -Global
		exit 1
	 } 
	 
	 Write-Host "Success: Accessing Site." -ForegroundColor Green
	 
	 Write-Host "Accessing Taxonomy Session : Process Starting ....." -ForegroundColor Green
	 $session = new-object Microsoft.SharePoint.Taxonomy.TaxonomySession($site) 
	 
	 if($Error.Count -gt 0) 
	 {     
		Write-Host "Error: Accessing Taxonomy Session. Exiting Script... " -ForegroundColor Red
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	 }
	 
	 Write-Host "Success: Accessing Taxonomy Session : Process Completed .....`n" -ForegroundColor Green

	 Write-Host "Adding Url to Meta Store  : Process Starting ..." -ForegroundColor Green
	 
	 try
	 {
	    
	 	$termstore = $session.TermStores[$TermStoreData.termstore.name]			
		$TermStoreData.termstore.group |
		 ForEach-Object {		 	
            $group = $termstore.Groups[$_.name.Replace("&", "＆")]        			     
		 	$_.termset |
		 	ForEach-Object {
							if($_.NavigationUrl)
	  						{ 
							$weburl= $_.NavigationUrl																                          
                            $termSet = $group.TermSets[$_.name]								
                            # Gets each term from the termset
                   $_.term |ForEach-Object{                                                                                 
                        $term=$termSet.Terms[$_.Name]  						
                        #Gets the term id and adds to the custom sortorder                                                                                           
						if($_.simpleLink)
                        { 
                            $Link=$_.simpleLink 													                                                                        
                            Set-TopNavigation -url $weburl -simpleLink $Link -guid $term.id.ToString()
                            write-host "Navigation page added successfully to termset-" $term.Name 
                        }                                                                
                    }   
					}                	  
                   
				   
				$termstore.CommitAll()
		 	}
			Write-Host "Navigation updated succesfully for site "	-ForegroundColor Green				 
		 }
		
	}
	catch
	{
		Write-Host "Exception while creating Meta store. Exiting script..." -ForegroundColor Red
		Stop-Transcript
		Stop-SPAssignment -Global
		
		Exit -1
	}
}



function DeleteTermStore([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$TermStoreData =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}

	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green
	 
	Write-Host "Accessing Site : Process Starting ...." -ForegroundColor Green
	$site = Get-SPSite $Configuration.Configuration.Farm.CentralAdminURL
   
	 if($Error.Count -gt 0) 
	 {     
		Write-Host "Error: Accessing Site. Exiting Script... " -ForegroundColor Red
		Stop-Transcript
		Stop-SPAssignment -Global
		exit 1
	 } 
	 
	 Write-Host "Success: Accessing Site." -ForegroundColor Green
	 
	 Write-Host "Accessing Taxonomy Session : Process Starting ....." -ForegroundColor Green
	 $session = new-object Microsoft.SharePoint.Taxonomy.TaxonomySession($site) 
	 
	 if($Error.Count -gt 0) 
	 {     
		Write-Host "Error: Accessing Taxonomy Session. Exiting Script... " -ForegroundColor Red
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	 }
	 
	 Write-Host "Success: Accessing Taxonomy Session : Process Completed .....`n" -ForegroundColor Green

	 Write-Host "Delete Meta Store  : Process Starting ..." -ForegroundColor Green
	 
	 try
	 {
	 	$termstore = $session.TermStores[$TermStoreData.termstore.name]	
		$TermStoreData.termstore.group |
		 ForEach-Object {
		 	# create the group
		 	if ($termstore.Groups[$_.name.Replace("&", "＆")] -eq $null)
		 	{
		   		Write-Host -ForegroundColor Green "No Term store found"		
		 	}
			else
			{
				$group = $termstore.Groups[$_.name.Replace("&", "＆")]
				$group.TermSets|foreach{
					$_.Delete()
					$termstore.CommitAll()
				}

				if($_.name.Replace("&", "＆") -ne "Update")
				{
					$group.Delete()
				}

				$termstore.CommitAll()

				Write-Output "Deleted " $_.name
			}
			}
	}
	catch
	{
		Write-Host "Exception while deleting Meta store. Exiting script..." -ForegroundColor Red
		Stop-Transcript
		Stop-SPAssignment -Global
		
		Exit -1
	}
}

function installutil
{
    param([string]$dllPath=$(throw "Please Enter the DLL path to Install!"))
    set-alias installutil $env:windir\Microsoft.NET\Framework64\v4.0.30319\installutil
    if(test-path $dllPath)
    {
        installutil /u $dllpath
        installutil $dllpath
        write-host "snap-in installed. now, you can add it to your shell instance"
    }
    else{
        write-error "The file does not exist"
    }
   
}
