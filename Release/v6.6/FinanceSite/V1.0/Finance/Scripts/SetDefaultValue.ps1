﻿$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

function SetDefaultValue([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$lookUpXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green      

    $sitecollectionUrl =  $lookUpXml.SiteCollection.SiteUrl

    # Get Site Collection URL
    $siteCollection = Get-SPSite $sitecollectionUrl
    
    
    
    $rootWeb = $siteCollection.RootWeb 	
    
	Write-Host "In the Site Collection"  $web.url 

    $lookUpXml.SiteCollection.SiteColumn.Mapping |
	ForEach-Object {
        $ColumnName = $_.Column
        $DefaultValue= $_.DefaultValue

        $siteColumn = $rootWeb.Fields.GetFieldByInternalName($ColumnName)
        if($siteColumn -ne $null)
        {
            $siteColumn.DefaultValue = $DefaultValue
            $siteColumn.ShowInDisplayForm = 0
            $siteColumn.ShowInNewForm = 0
            $siteColumn.ShowInEditForm = 0
            $siteColumn.Update()

            Write-Host "Site Column" $ColumnName "Default value set to" $DefaultValue -ForegroundColor Green

            $siteColumn = $null
        }

    } 

    $lookUpXml.SiteCollection.Subsite |
	ForEach-Object {  

        $Subsite = $_.SubSiteUrl
        
        $web = Get-SPWeb -Identity $Subsite	

        $_.mapping |
	    ForEach-Object {
	    		
           $ListName = $_.ListName

           $ColumnName = $_.Column

           $DefaultValue= $_.DefaultValue

           try
           {
                $list = $web.Lists[$ListName]
                $column = $list.Fields.GetFieldByInternalName($ColumnName)

                if($column -ne $null)
                {
                    $column.DefaultValue = $DefaultValue
                    $column.ShowInDisplayForm = 0
                    $column.ShowInNewForm = 0
                    $column.ShowInEditForm = 0
                    $column.Update()
                }

                Write-Host "Column" $ColumnName "Default value set to" $DefaultValue in $web -ForegroundColor Green
           }
           catch
           {
                Write-Host $ListName "not found" in $web -ForegroundColor Red
           }
           $column = $null     
        }
    }
        
	$rootWeb.Dispose()				
}
