﻿$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

function AddIconItemConfig([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$lookUpXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green      

    $sitecollectionUrl =  $lookUpXml.Sites.SiteCollection

    $listTitle = $lookUpXml.Sites.ListTitle
	
    # Get Site Collection URL
    $siteCollection = Get-SPSite $sitecollectionUrl
    
    $rootWeb = $siteCollection.RootWeb 	
	
	#Get Configuration List
	$configList = $rootWeb.Lists[$listTitle];
	
	# Add Icon items to Connfig list if not already present
	$lookUpXml.Sites.Item |
	ForEach-Object {
			# Get title , key and value from XMl
			$title = $_.Title
			$key = $_.Key
			$value = $_.Value
			$configItems = $configList.Items | where {$_['Key'] -eq $key}
			if($configItems)
			{
				Write-Host "Item with Title : " $title " Key :" $key" and Value : "$value "already exists in the list" -ForegroundColor Green
			}
			else
			{
				#Create a new item
				$newItem = $configList.Items.Add()
				 
				#Add properties to this list item
				$newItem["Title"] = $title
				$newItem["Key"] = $key
				$newItem["Value1"] = $value
				 
				#Update the object so it gets saved to the list
				$newItem.Update()
			}
		}
        Write-Host "Items added to configuration list"-ForegroundColor Green
           
        
	$rootWeb.Dispose()				
}
