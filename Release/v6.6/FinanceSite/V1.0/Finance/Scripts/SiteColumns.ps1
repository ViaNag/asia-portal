﻿# =================================================================================
#
#Main Function
#
# =================================================================================
function CreateSiteColumns([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file"
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
            foreach($column in $cfg.SiteColumns.Column)
            {
                SiteColumnToAdd $column
            }
        }
        catch
        {
            Write-Output "`nException :" $Error
            Write-Host "`nException :" $Error -ForegroundColor Red
        }
	}
	else
	{
		Write-Host $Error -ForegroundColor Red
        Write-Output $Error
	}
}

# =================================================================================
#
# FUNC: SiteCOlumnToAdd
# DESC: Create site column in existing site collection
#
# =================================================================================
function SiteColumnToAdd([object] $column)
{
    try
    {
        $error.Clear()
        $site = new-object Microsoft.SharePoint.SPSite $column.SiteUrl
        $web = $site.RootWeb
        [boolean]$isExist=[System.Convert]::ToBoolean(@{$true="false";$false=$column.IsExist}[$column.IsExist -eq ""])
        # Check if the field does not exist already
        if($web.Fields[$column.ColumnName] -eq $null -and !$isExist)
        {
            # Get the type of the field
            $type = $column.Type
            if(($type -eq "TaxonomyFieldType") -or ($type -eq "TaxonomyFieldTypeMulti"))
            {
                # Get the Taxonomy session of your site collection
                $session = new-object Microsoft.SharePoint.Taxonomy.TaxonomySession($site)
                $termStore = $session.TermStores[$column.TermStore]
                # Get the term store group which stores the term sets you want to retrieve.
                $taxGroup=$column.TaxonomyGroup
                if($column.TaxonomyGroup -eq "M&E Ad Sales")
                {
                    $taxGroup="M＆E Ad Sales"
                }
                $termStoreGroup = $termStore.Groups[$taxGroup]

                      # Get the term set you want to associate with this field. 
                      $termSet = $termStoreGroup.TermSets[$column.TermSet]
                      # In most cases, the anchor on the managed metadata field is set to a lower level under the root term of the term set. In such cases, specify the term in the spreadsheet and do the following
                      $termID=""
                      if(($column.TermSet -ne $column.Term) -and ($column.Term -ne "") -and ($column.Term -ne $null))
                      {
                            #Get all terms under term set
                            $terms = $termSet.GetAllTerms()

                            #Get the term to map the column to
                            $term = $terms | Where-Object {$_.Name -eq $column.Term}

                            #Get the GUID of the term to map the metadata column anchor to
                            $termID = $term.Id
                        }
                     else # In cases when you want to set the anchor at the root of the term set, leave the  value as blank. Empty guids will error out when you run the script but will accomplish what you need to do i.e. set the anchor at the root of the termset
                       {                                
                            $termID = [System.GUID]::empty
                       } 
                     # Create the new managed metadata field
                     $newSiteColumn=$null
                     if([string]::IsNullOrEmpty($column.Guid))
                     {
                        $newSiteColumn = $web.Fields.CreateNewField("TaxonomyFieldType", $column.InternalName)
                        $web.Fields.Add($newSiteColumn)
                        $web.Update()
                     }
                     else
                     {
                        $fieldXML=$null
                        $fieldXML = '<Field Type="TaxonomyFieldType" SourceID="http://schemas.microsoft.com/sharepoint/v3" Name="'+$column.InternalName+
                                     '" ID="{' + $column.Guid+'}'+
                                     '" DisplayName="'+$column.ColumnName+
                                     '"></Field>' 
                        $web.Fields.AddFieldAsXml($fieldXML)
                        $web.update()
                        $newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)
                     }

                     # Update the properties of the new field.
                     $newSiteColumn.SspId = $termSet.TermStore.ID
                     $newSiteColumn.TermSetId = $termSet.Id 
                     $newSiteColumn.AnchorId = $termID
                     if($type -eq "TaxonomyFieldTypeMulti")
                     {
                        $newSiteColumn.AllowMultipleValues = $true
                     }
                     # Add the the new column to the Site collection's Root web's Fields Collection
                     $newSiteColumn.Group = $column.GroupName
                     $newSiteColumn.Update()
                     $web.Update() 
            }
             elseif($type -eq "Choice")
             {
                    # Build a string array with the choice values separating the values at ","
                    $choiceFieldChoices = @($column.Choices.choice)

                    # Declare a new empty String collection
                    $stringColl = new-Object System.Collections.Specialized.StringCollection

                    # Add the choice fields from array to the string collection
                    $stringColl.AddRange($choiceFieldChoices)

                    # Create a new choice field and add it to the web using overload method
                    $newSiteColumn=$null
                    if([string]::IsNullOrEmpty($column.Guid))
                    {
                        $newSiteColumn = $web.Fields.Add($column.InternalName,[Microsoft.SharePoint.SPFieldType]::$type, $siteColumn.Required, $false, $stringColl)
                        $web.Update()
                    }
                    else
                    {
                       $fieldXML=$null
                       $fieldXML = '<Field Type="' + [Microsoft.SharePoint.SPFieldType]::$type + '" SourceID="http://schemas.microsoft.com/sharepoint/v3" FillInChoice="FALSE" Name="'+$column.InternalName+
                                     '" DisplayName="'+$column.ColumnName+
                                     '" ID="{' + $column.Guid+'}'+
                                     '" Format="Dropdown"'+
                                     '>'
                        if($column.Choices.choice.Count -gt 0)
                        {
                           $fieldXML=$fieldXML+"<CHOICES>"
                           foreach($choice in $choiceFieldChoices)
                           {
                                $fieldXML=$fieldXML+'<CHOICE>'+ $choice.Trim() +'</CHOICE>'
                           }
                           $fieldXML=$fieldXML+"</CHOICES>"
                        }
                       $fieldXML=$fieldXML+ '</Field>'
                       $web.Fields.AddFieldAsXml($fieldXML)
                       $web.update()
                       $newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)
                    }
             }
			 elseif($type -eq "Number")
             {
					$newSiteColumn=$null
					
                    if([string]::IsNullOrEmpty($column.Guid))
                    {
                        $newSiteColumn = $web.Fields.CreateNewField([Microsoft.SharePoint.SPFieldType]::$type, $column.InternalName)                        
						$web.Fields.Add($newSiteColumn)
                        $web.update()
                    }
                    else
                    {
                     $fieldXML=$null
                        $fieldXML = '<Field Type="' + [Microsoft.SharePoint.SPFieldType]::$type + '"'+' SourceID="http://schemas.microsoft.com/sharepoint/v3" Name="'+$column.InternalName+
                                     '" ID="{' + $column.Guid+'}'+
                                     '" DisplayName="'+$column.ColumnName + '"></Field>'
									 
                        $web.Fields.AddFieldAsXml($fieldXML)
                        $web.update()
                        $newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)
                    }			 
             }
             elseif($type -eq "Image")
             {
				   $newSiteColumn=$null
                    if([string]::IsNullOrEmpty($column.Guid))
                    {
                        $newSiteColumn = $web.Fields.CreateNewField("Image", $column.InternalName)
                        $web.Fields.Add($newSiteColumn)
                        $web.update()
                    }
                    else
                    {
                        $fieldXML=$null
                        $fieldXML = '<Field Type="' + "Image" + '"'+' SourceID="http://schemas.microsoft.com/sharepoint/v3" Name="'+$column.InternalName+
                                     '" ID="{' + $column.Guid+'}'+
                                     '" DisplayName="'+$column.ColumnName + '"></Field>'
                        $web.Fields.AddFieldAsXml($fieldXML)
                        $web.update()
                        $newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)
                    }
             }
             elseif($type -eq "Calculated")
             {		  
					$newSiteColumn=$null
                     $choiceFieldChoices = @($column.CalculatedFields.Field)
					 $formula = $column.Formula
                    if([string]::IsNullOrEmpty($column.Guid))
                    {                      
                      # Add the the new column to the Site collection's Root web's Fields Collection  
                      #$newSiteColumn = $web.Fields.CreateNewField([Microsoft.SharePoint.SPFieldType]::"Calculated", $column.InternalName)
                      $web.Fields.Add($column.InternalName,$type,$false)
                      $newSiteColumn=$web.Fields.GetField($column.InternalName)             
                      $newSiteColumn.Formula = $formula
                      $newSiteColumn.Update()
                      $web.Update()
                    }
                    else
                    {
                        $fieldXML=$null
                        $fieldXML = '<Field Type="' + [Microsoft.SharePoint.SPFieldType]::$type + '"'+' SourceID="http://schemas.microsoft.com/sharepoint/v3" Name="'+$column.InternalName+
                                     '" ID="{' + $column.Guid+'}'+
                                     '" DisplayName="'+$column.ColumnName + '">'
                        
						 $fieldXML=$fieldXML+"<Formula>="                           
                         $fieldXML=$fieldXML+ $formula
                         $fieldXML=$fieldXML+"</Formula>"

                         if($column.CalculatedFields.Field.Count -gt 0)
                        {
                           $fieldXML=$fieldXML+"<FieldRefs>"
                           foreach($choice in $choiceFieldChoices)
                           {
                                $fieldXML=$fieldXML+ "<FieldRef Name='" + $choice + "'/>"
                           }
                           $fieldXML=$fieldXML+"</FieldRefs>"
                        }
                        $fieldXML=$fieldXML+ '</Field>'   						 

						$web.Fields.AddFieldAsXml($fieldXML)
                        $web.update()
                        $newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)						

                    }
                      
             }
             else
             {
                    # Create the new field and add it to the web
                    $newSiteColumn=$null
                    if($type -eq "UserMulti")
                    {
                        $type="User"
                    }
                    $newSiteColumn=$null
                    if([string]::IsNullOrEmpty($column.Guid))
                    {
                        $newSiteColumn = $web.Fields.CreateNewField([Microsoft.SharePoint.SPFieldType]::$type, $column.InternalName)
                        $web.Fields.Add($newSiteColumn)
                        $web.update()
                    }
                    else
                    {
                        $fieldXML=$null
                        $fieldXML = '<Field Type="' + [Microsoft.SharePoint.SPFieldType]::$type + '"'+' SourceID="http://schemas.microsoft.com/sharepoint/v3" Name="'+$column.InternalName+
                                     '" ID="{' + $column.Guid+'}'+
                                     '" DisplayName="'+$column.ColumnName + '"></Field>'
                        $web.Fields.AddFieldAsXml($fieldXML)
                        $web.update()
                        $newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)
                    }
             }
              Write-Host "Site column has been created: " $column.ColumnName " with internal name "$column.InternalName "at web:- "$column.SiteUrl -ForegroundColor Green
              Write-Output "Site column has been created: " $column.ColumnName " with internal name "$column.InternalName "at web:- "$column.SiteUrl
              UpdateColumnProperites $column $web
        }
        else
        {
            Write-Host "The following site column already exists: " $column.ColumnName " with internal name " $column.InternalName -ForegroundColor Yellow
            Write-Output "The following site column already exists: " $column.ColumnName " with internal name " $column.InternalName
            Write-Host "Updating site column properties :" $column.ColumnName " with internal name " $column.InternalName -ForegroundColor Yellow
            Write-Output "Updating site column properties :" $column.ColumnName " with internal name " $column.InternalName
            UpdateColumnProperites $column $web
        }
    }
    catch
    {
        Write-Host "`nException for site column :" $column.ColumnName "`n" $Error -ForegroundColor Red
        Write-Output "`nException for site column :" $column.ColumnName "`n" $Error
    }
}

# =================================================================================
#
# FUNC: UpdateColumnProperites
# DESC: Update site column properties in existing site collection
#
# =================================================================================
function UpdateColumnProperites([object] $column, $web)
{
    try
    {
        $error.Clear()
        SetDefaultValue $column
        $web=Get-SPWeb $column.SiteUrl
        $clm = $null
        $clm = $web.Fields.GetFieldByInternalName($column.InternalName)

        # Add or remove any properties here
        $clm.Title = $column.ColumnName
        $clm.Description = $column.Description
        $clm.Group = $column.GroupName
              
        # Boolean values must be converted before they are assigned in PowerShell.
        if(![string]::IsNullOrEmpty($column.ShowInNewForm))
        {
            [boolean]$clm.ShowInNewForm = [System.Convert]::ToBoolean($column.ShowInNewForm)
        }
        if(![string]::IsNullOrEmpty($column.ShowInDisplayForm))
        {
            [boolean]$clm.ShowInDisplayForm = [System.Convert]::ToBoolean($column.ShowInDisplayForm)
        }
        if(![string]::IsNullOrEmpty($column.ShowInEditForm))
        {
            [boolean]$clm.ShowInEditForm = [System.Convert]::ToBoolean($column.ShowInEditForm)
        }
        if(![string]::IsNullOrEmpty($column.Hidden))
        {
            [boolean]$clm.Hidden = [System.Convert]::ToBoolean($column.Hidden)
        }
        if(![string]::IsNullOrEmpty($column.Required))
        {
            [boolean]$clm.Required = [System.Convert]::ToBoolean($column.Required)
        }
         if(![string]::IsNullOrEmpty($column.ReadOnlyField))
        {
            [boolean]$clm.ReadOnlyField = [System.Convert]::ToBoolean($column.ReadOnlyField)
        }
        if($type -eq "Note")
        {
        if($column.MLTType -eq "PlainText")
        {
            [boolean]$clm.RichText = $false
        }
        else
        {
            [boolean]$clm.RichText = $true
        }
        }
        if($column.Type -eq "UserMulti")
        {
            $clm.AllowMultipleValues = $true
        }
        if(![string]::IsNullOrEmpty($column.JSLink))
        {
            $clm.JSLink = $column.JSLink
        }
		$IsCalculated = $column.IsCalculated
        
         # Update the properties of the new field.
         if(($IsCalculated -eq "True") -and ($IsCalculated -ne ""))
         {
            $formula = $column.Formula
            $clm.DefaultFormula = $formula
         }
		 $maxValue = $column.Max
		 $minValue = $column.Min
		  if(($maxValue -ne $null) -and ($maxValue -ne "") -and ($minValue -ne $null) -and ($minValue -ne ""))
						 {
							$clm.MaximumValue = $maxValue
							$clm.MinimumValue = $minValue
						 }

        # Update the site column
        $clm.Update()
        $web.Update()
    }
    catch
    {
         Write-Host "`nException for site column :" $column.ColumnName "`n" $Error -ForegroundColor Red
         Write-Output "`nException for site column :" $column.ColumnName "`n" $Error 
    }
}

# =================================================================================
#
# FUNC: SetDeafultValue
# DESC: Set default value for column at site collection level
#
# =================================================================================
function SetDefaultValue([object] $column)
{
    try
    {
        $error.Clear()
        $type=$column.Type
        if(($type -eq "TaxonomyFieldType") -or ($type -eq "TaxonomyFieldTypeMulti"))
            {
                # Get the Taxonomy session of your site collection
                $site = new-object Microsoft.SharePoint.SPSite $column.SiteUrl
                $session = new-object Microsoft.SharePoint.Taxonomy.TaxonomySession($site)
                $termStore = $session.TermStores[$column.TermStore]
                $taxGroup=$column.TaxonomyGroup
                if($column.TaxonomyGroup -eq "M&E Ad Sales")
                {
                    $taxGroup="M＆E Ad Sales"
                }
                $termStoreGroup = $termStore.Groups[$taxGroup]
                $termSet = $termStoreGroup.TermSets[$column.TermSet]
				$web=$site.RootWeb
                $field=$web.Fields.GetFieldByInternalName($column.InternalName)
                $setDefVal=""
                if($type -eq "TaxonomyFieldTypeMulti")
                {
                    $defaultValues = @($column.DefaultValues.Split(","))
                    foreach($value in $defaultValues)
                    {
                        if(($value -ne $null) -and ($value -ne ""))
                        {
                            $term=$termSet.Terms[$value]
							$wssIDToSet = AddTaxonomyHiddenListItem $web $term 
                            $setDefVal=[string]$setDefVal+$wssIDToSet + ";#" + $term.GetPath() + [Microsoft.SharePoint.Taxonomy.TaxonomyField]::TaxonomyGuidLabelDelimiter + $term.Id.ToString()+";#"
                        }
                    }
                    $setDefVal=$setDefVal.TrimEnd(";#")
                }
                elseif($type -eq "TaxonomyFieldType")
                {
                    $defaultValues = $column.DefaultValues
                    if(($defaultValues -ne $null) -and ($defaultValues -ne ""))
                    {
                        $term=$termSet.Terms[$defaultValues]
						$wssIDToSet = AddTaxonomyHiddenListItem $web $term 
                        $setDefVal=[string]$wssIDToSet + ";#" + $term.GetPath() + [Microsoft.SharePoint.Taxonomy.TaxonomyField]::TaxonomyGuidLabelDelimiter + $term.Id.ToString()
                    }
                }
                $field.DefaultValue=$setDefVal
                $field.Update()
            }
            elseif(($type -ne "UserMulti") -and ($type -ne "User") -and ($type -ne "Note"))
            {
                $defaultValues = $column.DefaultValues
                if(($defaultValues -ne $null) -and ($defaultValues -ne ""))
                {
                    $field=$web.Fields.GetFieldByInternalName($column.InternalName)
                    $field.DefaultValue=$defaultValues
                    $field.Update()
                }
            }
    }
    catch
    {
        Write-Host "`nException for site column :" $column.ColumnName "`n" $Error -ForegroundColor Red
        Write-Output "`nException for site column :" $column.ColumnName "`n" $Error 
    }
}

# =================================================================================
#
# FUNC: AddTaxonomyHiddenListItem
# DESC: Add term to taxonomy hidden list.
#
# =================================================================================
function AddTaxonomyHiddenListItem($w,$termToAdd)
{
      $wssid = $null; #return value
      $count = 0;     
      $l = $w.Lists["TaxonomyHiddenList"]; 
      #check if Hidden List Item already exists
      foreach($item in $l.Items){
        $xml = [xml]$item.xml;#cast the xml TaxonomyHiddenList item values
        $temID = $xml.row.ows_IdForTerm #get the IdForTerm, this is the key that unlocks all the doors
        if($temID -eq $termToAdd.ID){ #compare the IdForTerm in the TaxonomyHiddenList item to the term in the termstore
            Write-Host $item.Name "Taxonomy Hidden List Item already exists" -ForegroundColor Red
            $wssid =  $item.ID; #get and return the WSSID needed to set the default clumn value
            return $wssid;
        } 
      }
      $newListItem = $l.Items.ADD();
      $newListItem["Title"] = $termToAdd.Name;
      $newListItem["IdForTermStore"] = $termToAdd.TermStore.ID;
      $newListItem["IdForTerm"] = $termToAdd.ID;
      $newListItem["IdForTermSet"] = $termToAdd.TermSet.ID;
      $newListItem["Term"] = $termToAdd.Name;
      $newListItem["Path"] = $divTerm.Name + ":" + $termToAdd.Name;
      $newListItem["CatchAllDataLabel"] = $termToAdd.Name + "#Љ|";  #"Љ" special char
      $newListItem["Term1033"] = $termToAdd.Name;
      $newListItem["Path1033"] = $divTerm.Name + ":" + $termToAdd.Name;
      $newListItem.Update();
      foreach($item in $l.Items){
        $xml = [xml]$item.xml;
        $temID = $xml.row.ows_IdForTerm
        if($temID -eq $termToAdd.ID){
            $wssid =  $item.ID; #get and return the WSSID needed to set the default clumn value
        } 
      }     
	  return $wssid;      
}