Pre-requisite to run the "IAAutomation.bat" file.
 1) Run the gac util command to install DocumentFormat.OpenXml.dll in gac.
	a) Open command prompt as administartor.
	b) Write command ==>cd "C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\bin\x64" and press enter.
	   Note:- Above is the path to gacutil. It is valid for Window 7 and path for gacutil may vary on OS version like Window 8 or Window Server.
	c) Write command ==> gacutil.exe /i "[DLL location]\DocumentFormat.OpenXml.dll". Replace [DLL Location] parameter with value where dll is stored and press enter.


To run the IA Automation replace the parameters given in "IAAutomation.bat" file.

1) Replace "FileUrl" with file url where we need to populate GUID and Internal name of columns. Eg:- "D:\RM_TFS_Code\Common\SiteCreationUtility\Code\SiteCreationScripts\Information Architecture Design Template.xlsm"

2) Replace "InventoryListSiteUrl" with url of site where inventory list is present. Eg:- "http://sp2013-ddev-3:7777"

3) Replace "Domain" with domain value of user who has access to inventory list. Eg:- "viacom_corp"

4) Replace "Username" with username of user who has access to inventory list. Eg:- "testone"

5) Replace "Password" with password of user who has access to inventory list. Eg:-"newuser1"