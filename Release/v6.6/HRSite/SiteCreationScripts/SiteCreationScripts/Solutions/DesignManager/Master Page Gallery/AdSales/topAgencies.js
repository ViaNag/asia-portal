﻿// function to process an accordion item..
var Viacom = Viacom || {};
Viacom.AdSales = Viacom.AdSales || {};
Viacom.AdSales.Count = 0;
Viacom.AdSales.getTopItems = function () {
        Viacom.AdSales.Count++;
		var Items="";
		var agencyName = ctx.CurrentItem.AgencyName;
		var agencyNameLabel = agencyName.Label;
		var agencySiteURL = ctx.CurrentItem.AgencySiteURL;
		Items += "<tr><td><a href='" + agencySiteURL + "'>" + agencyNameLabel + "</a></td></tr>";
		return Items
};
 
// anonymous self-executing function to setup JSLink templates on page load..
(function () {
var overrideCtx = {};
overrideCtx.Templates = {};
 
overrideCtx.Templates.Header = '<table border="0" class="ms-content-listing"><tbody><tr><th align="left" valign="top" class="ms-header">Team Top Agencies</th><th align="right" valign="top" class="ms-edit"><a href="AllAgencies.aspx">[All Agencies]</th></a></tr><tr><td colspan="2" class="ms-content"><table border="0" class="ms-content-rows"><tbody>';
overrideCtx.Templates.Item = Viacom.AdSales.getTopItems;

//if (Viacom.AdSales.Count == 0) {
 //   overrideCtx.Templates.Item = "There are no items to show in this view";
//}

overrideCtx.Templates.Footer = "</tbody></table></td></tr></tr></tbody></table>";
 
overrideCtx.BaseViewID = 1;
overrideCtx.ListTemplateType = 10008;
 
SPClientTemplates.TemplateManager.RegisterTemplateOverrides(overrideCtx);
})();

