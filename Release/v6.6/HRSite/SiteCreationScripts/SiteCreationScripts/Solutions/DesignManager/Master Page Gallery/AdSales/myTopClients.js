// function to process an accordion item..
var Viacom = Viacom || {};
Viacom.AdSales = Viacom.AdSales || {};
Viacom.AdSales.getTopItems = function () {

		var Items="";
		var clientName = ctx.CurrentItem.ClientName;
		var clientNameLabel = clientName.Label;
		var clientSiteURL = ctx.CurrentItem.ClientSiteURL;
		Items += "<tr><td><a href='" + clientSiteURL + "'>" + clientNameLabel + "</a></td></tr>";
		return Items
};
 
// anonymous self-executing function to setup JSLink templates on page load..
(function () {
var overrideCtx = {};
overrideCtx.Templates = {};
 
overrideCtx.Templates.Header = '<table border="0" class="ms-content-listing"><tbody><tr><th align="left" valign="top" class="ms-header">My Top Clients</th><th align="right" valign="top" class="ms-edit"><a href="javascript:void(0)" onclick="Viacom.AdSales.OpenDialog(&quot;../Pages/SVPClientPage.aspx&quot;)">[Edit]</th></a></tr><tr><td colspan="2" class="ms-content"><table border="0" class="ms-content-rows"><tbody>';
overrideCtx.Templates.Item = Viacom.AdSales.getTopItems;
overrideCtx.Templates.Footer = "</tbody></table></td></tr></tr></tbody></table>";
 
overrideCtx.BaseViewID = 1;
overrideCtx.ListTemplateType = 10023;
 
SPClientTemplates.TemplateManager.RegisterTemplateOverrides(overrideCtx);
})();

Viacom.AdSales.Constants=function(){
 return{
 accountName:null,
 ClientsCtx:null
 }
};

Viacom.AdSales.OpenDialog=function(URL) {
		Viacom.AdSales.Constants.ClientsCtx = SP.ClientContext.get_current();
		var user = Viacom.AdSales.Constants.ClientsCtx.get_web().get_currentUser();
		Viacom.AdSales.Constants.ClientsCtx.load(user);
		Viacom.AdSales.Constants.ClientsCtx.executeQueryAsync(function(){
		Viacom.AdSales.Constants.accountName=user.get_loginName();
		var svpAccNmClnt=Viacom.AdSales.Constants.accountName;
		if(Viacom.AdSales.Constants.accountName.indexOf("|")>=0)
		  svpAccNmClnt=svpAccNmClnt.split("|")[1];
		//alert(svpAccNmClnt+'--'+Viacom.AdSales.Constants.accountName);
		URL=URL+"?lstName=Site Mapping&filterField=SVPSiteURL&propertyName=MyTopClients&userId="+svpAccNmClnt+"&sourceUrl="+_spPageContextInfo.webServerRelativeUrl;
    var options = {  
	url:URL, 
	width: 700, 
	height: 516, 
	showClose: true,
	title:"Set Top Clients", 
	allowMaximize: true ,
	dialogReturnValueCallback: function (dialogResult, returnVlue) {
                if (returnVlue) {
                    RefreshPage(dialogResult);
                }
            }   
	};
    SP.UI.ModalDialog.showModalDialog(options);
			}, 
		function(){});
 
};


