<%-- SPG:

This HTML file has been associated with a SharePoint Page Layout (.aspx file) carrying the same name.  While the files remain associated, you will not be allowed to edit the .aspx file, and any rename, move, or deletion operations will be reciprocated.

To build the page layout directly from this HTML file, simply fill in the contents of content placeholders.  Use the Snippet Generator at http://teamsitesqa.viacom.com/_layouts/15/ComponentHome.aspx?Url=http%3A%2F%2Fteamsitesqa%2Eviacom%2Ecom%2F%5Fcatalogs%2Fmasterpage%2FAdSales%2FAdSalesSiteCreationPageLayout%2Easpx to create and customize additional content placeholders and other useful SharePoint entities, then copy and paste them as HTML snippets into your HTML code.   All updates to this file within content placeholders will automatically sync to the associated page layout.

 --%>
<%@Page language="C#" Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage, Microsoft.SharePoint.Publishing, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="PageFieldFieldValue" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@Register TagPrefix="PageFieldTaxonomyFieldControl" Namespace="Microsoft.SharePoint.Taxonomy" Assembly="Microsoft.SharePoint.Taxonomy, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<asp:Content runat="server" ContentPlaceHolderID="PlaceHolderMain">
            <script>//<![CDATA[
			  function toggleView(opt) {
				  if (opt == "SVP") {
					  //$("#div_SVPSection").show();
					  //$("#div_AgencySection").hide();
					  // $("#div_ClientSection").hide();
					  Viacom.appTaxonomy.loadJSBeforeDDLPopulate("SVPSite");
					  //Viacom.CreateWeb.setDropdownValue("SVPSite");
				  }
				  if (opt == "Agency") {
					  // $("#div_SVPSection").hide();
					  // $("#div_AgencySection").show();
					  // $("#div_ClientSection").hide();
					  Viacom.CreateWeb.setDropdownValue("AgencySite");
				  }
				  if (opt == "Client") {
					  // $("#div_SVPSection").hide();
					  // $("#div_AgencySection").hide();
					  // $("#div_ClientSection").show();
				  }
				  if (opt == "") {
					  //  $("#div_SVPSection").hide();
					  // $("#div_AgencySection").hide();
					  // $("#div_ClientSection").hide();
				  }
			  }
			  $(document).ready(function () {
				  toggleView("");


				  $('.tab-items li:first-child').addClass('active');
				  $('.tab-content:first-child').show();
				  $('.tab-items li a').click(function (event) {
					  var open_tab = $(this).attr('href');
					  $('.tab-items li').removeClass('active');
					  $(this).parent('li').addClass('active');
					  $('.tab-content').hide();
					  $(open_tab).show();
					  switch (open_tab) {
					      case "#div_SVPSection": {
					          window.location.hash = '';
					          break;
					      }
					      case "#div_AgencySection": {
					          window.location.hash = 'createAgency';
					          break;
					      }
					      case "#div_ClientSection": {
					          window.location.hash = 'createClient';
					          break;
					      }
					      case "#edit_ClientSection": {
					          window.location.hash = 'editClient';
					          break;
					      }
					      case "#edit_AgencySection": {
					          window.location.hash = 'editAgency';
					          break;
					      }
					      case "#delete_siteSection": {
					          window.location.hash = 'deleteSite';
					          break;
					      }
					  }
					  event.preventDefault();
				  });
				  ShowHideTab();
				  $(function () {
					  $("#svpTeamNm,#agencyNm,#agencyId,#clientNm,#rdmCLientId,#gabrielClientId,#wideOrbitClientId,#adFrontClientId").on('input', function (event) {
						  var posCaret = this.selectionStart;
						  this.value = this.value.replace(/^\s+/, '');
						  setCaretPosition(this, posCaret);
					  });
				  });

			  });
			  function ShowHideTab() {
			      var tabToSelected = window.location.hash;
			      var showTab = "";
			      switch (tabToSelected) {
			          case "#": {
			              showTab = "#div_SVPSection";
			              break;
			          }
			          case "": {
			              showTab = "#div_SVPSection";
			              break;
			          }
			          case "#createAgency": {
			              showTab = "#div_AgencySection";
			              break;
			          }
			          case "#createClient": {
			              showTab = "#div_ClientSection";
			              break;
			          }
			          case "#editClient": {
			              showTab = "#edit_ClientSection";
			              break;
			          }
			          case "#editAgency": {
			              showTab = "#edit_AgencySection";
			              break;
			          }
			          case "#deleteSite": {
			              showTab = "#delete_siteSection";
			              break;
			          }
			      }
			      var element = "a[href='" + showTab + "']";
			      $(element).click();
			  }
			  function blockSpecialChar(e, includeSpace) {
				  var k;
				  var cc = (e.charCode) ? e.charCode : ((e.which) ? e.which : e.keyCode);
				  document.all ? k = e.keyCode : k = e.which;
				  if (includeSpace) {
					  return (((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57)) && (cc != 32));
				  }
				  else {
					  return (((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57)));
				  }
			  }
			  function setCaretPosition(elem, caretPos) {
				  if (elem != null) {
					  if (elem.createTextRange) {
						  var range = elem.createTextRange();
						  range.move('character', caretPos);
						  range.select();
					  }
					  else {
						  if (elem.selectionStart) {
							  elem.focus();
							  elem.setSelectionRange(caretPos, caretPos);
						  }
						  else
							  elem.focus();
					  }
				  }
			  }

			  function setSiteUrl(nmId, urlId) {
				  nmId = '#' + nmId;
				  var siteNm = $(nmId).val();
				  siteNm = siteNm.replace(/[^a-zA-Z0-9]/g, '');
				  urlId = '#' + urlId;
				  $(urlId).val(siteNm);
			  }
		
            
            
            
            
            //]]></script>
            <div class="welcome welcome-splash">
                <div class="ms-table ms-fullWidth">
                    <div class="tableCol-100 ms-content-listing activity-listing">
                        <div class="middle-column-area">
                            <div id="tblMain">
                                <div style="width:23%; float: left; padding: 0 1%;">
                                    <ul class="tab-items">
                                        <li>
                                            <a href="#div_AgencySection">Create New Agency Site
                                            
                                            
                                            
                                            
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#div_ClientSection">Create New Client Site
                                            
                                            
                                            
                                            
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#edit_AgencySection">Edit Agency
                                            
                                            
                                            
                                            
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#edit_ClientSection">Edit Client
                                            
                                            
                                            
                                            
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div style="width:75%; float: left;">
                                    <div id="div_AgencySection" class="tab-content">
                                        <ul>
                                            <li>
                                                <span class="lblText">Agency Name
                                                    
                                                    
                                                    
                                                    
                                                    <em style="color:red">*
                                                    
                                                    
                                                    
                                                    
                                                    </em>
                                                </span>
                                                <input type="text" id="agencyNm" />
                                            </li>
                                            <li>
                                                <span class="lblText">Agency ID
                                                
                                                
                                                
                                                
                                                </span>
                                                <input type="text" id="agencyId" onkeypress="return blockSpecialChar(event,false)" />
                                            </li>
                                            <li>
                                                <span class="lblText">
                                                    <p>
                                                    </p>
                                                </span>
                                                <input type="button" value="Create Site" onclick="Viacom.createAgencyWeb.chkAgencyTerm($('#agencyNm').val(), 'Agency', 'MÃ¯Â¼â€ E Ad Sales', 'Managed Metadata Service')" style="margin-left: 0;" />
                                                <input type="button" value="Cancel" onclick="Viacom.CreateWeb.CancelPage()" style="margin-left: 5%;" />
                                            </li>
                                        </ul>
                                    </div>
                                    <div id="div_ClientSection" class="tab-content">
                                        <ul>
                                            <li>
                                                <span class="lblText">Client Name
                                                    
                                                    
                                                    
                                                    
                                                    <em style="color:red">*
                                                    
                                                    
                                                    
                                                    
                                                    </em>
                                                </span>
                                                <input type="text" id="clientNm" />
                                            </li>
                                            <li>
                                                <span class="lblText">Client Category
                                                    
                                                    
                                                    
                                                    
                                                    <em style="color:red">*
                                                    
                                                    
                                                    
                                                    
                                                    </em>
                                                </span>
                                                
                                                <PageFieldTaxonomyFieldControl:TaxonomyFieldControl ID="ClientCategory" FieldName="ClientCategory" runat="server" ControlMode="New">
                                                </PageFieldTaxonomyFieldControl:TaxonomyFieldControl>
                                            </li>
                                            <li>
                                                <span class="lblText">Client Segment
                                                    
                                                    
                                                    
                                                    
                                                    <em style="color:red">*
                                                    
                                                    
                                                    
                                                    
                                                    </em>
                                                </span>
                                                <PageFieldTaxonomyFieldControl:TaxonomyFieldControl ID="ClientSegment" FieldName="ClientSegment" runat="server" ControlMode="New">
                                                </PageFieldTaxonomyFieldControl:TaxonomyFieldControl>
                                            </li>
                                            <li>
                                                <span id="agencyTypeNew1" value="Assigned Primary Linear Buying Agency" taxFieldNameProvisioning="AssignedPrimaryLinearBuyingAgenc0" class="lblText">Linear Buying Agency
                                                
                                                
                                                
                                                
                                                </span>
                                                <PageFieldTaxonomyFieldControl:TaxonomyFieldControl ID="clientAgencyNmNew1" FieldName="AgencyNameMulti" runat="server" ControlMode="New">
                                                </PageFieldTaxonomyFieldControl:TaxonomyFieldControl>
                                            </li>
                                            <li>
                                                <span id="agencyTypeNew2" value="Assigned Primary Digital Buying Agency" taxFieldNameProvisioning="AssignedPrimaryDigitalBuyingAgen0" class="lblText">Digital Buying Agency
                                                
                                                
                                                
                                                
                                                </span>
                                                <PageFieldTaxonomyFieldControl:TaxonomyFieldControl ID="clientAgencyNmNew2" FieldName="AgencyNameMulti" runat="server" ControlMode="New">
                                                </PageFieldTaxonomyFieldControl:TaxonomyFieldControl>
                                            </li>
                                            <li>
                                                <span id="agencyTypeNew3" value="Assigned Planning Agency" taxFieldNameProvisioning="AssignedPlanningAgency" class="lblText">Planning Agency
                                                
                                                
                                                
                                                
                                                </span>
                                                <PageFieldTaxonomyFieldControl:TaxonomyFieldControl ID="clientAgencyNmNew3" FieldName="AgencyNameMulti" runat="server" ControlMode="New">
                                                </PageFieldTaxonomyFieldControl:TaxonomyFieldControl>
                                            </li>
                                            <li>
                                                <span id="agencyTypeNew4" value="Direct Response Agency" taxFieldNameProvisioning="DirectResponseAgency" class="lblText">Direct Response Agency
                                                
                                                
                                                
                                                
                                                </span>
                                                <PageFieldTaxonomyFieldControl:TaxonomyFieldControl ID="clientAgencyNmNew4" FieldName="AgencyNameMulti" runat="server" ControlMode="New">
                                                </PageFieldTaxonomyFieldControl:TaxonomyFieldControl>
                                            </li>
                                            <li>
                                                <span id="agencyTypeNew5" value="Barter Agency" taxFieldNameProvisioning="BarterAgency" class="lblText">Barter Agency
                                                
                                                
                                                
                                                
                                                </span>
                                                <PageFieldTaxonomyFieldControl:TaxonomyFieldControl ID="clientAgencyNmNew5" FieldName="AgencyNameMulti" runat="server" ControlMode="New">
                                                </PageFieldTaxonomyFieldControl:TaxonomyFieldControl>
                                            </li>
                                            <li>
                                                <span class="lblText">RDM Client ID
                                                
                                                
                                                
                                                
                                                </span>
                                                <input type="text" id="rdmCLientId" onkeypress="return blockSpecialChar(event,false)" />
                                            </li>
                                            <li>
                                                <span class="lblText">Gabriel Client ID
                                                
                                                
                                                
                                                
                                                </span>
                                                <input type="text" id="gabrielClientId" onkeypress="return blockSpecialChar(event,false)" />
                                            </li>
                                            <li>
                                                <span class="lblText">Wide Orbit Client ID
                                                
                                                
                                                
                                                
                                                </span>
                                                <input type="text" id="wideOrbitClientId" onkeypress="return blockSpecialChar(event,false)" />
                                            </li>
                                            <li>
                                                <span class="lblText">AdFront Client ID
                                                
                                                
                                                
                                                
                                                </span>
                                                <input type="text" id="adFrontClientId" onkeypress="return blockSpecialChar(event,false)" />
                                            </li>
                                            <li>
                                                <span class="lblText">
                                                    <p>
                                                    </p>
                                                </span>
                                                <input type="button" value="Create Site" onclick="Viacom.createClientWeb.chkClientTerm($('#clientNm').val(), 'Client', 'MÃ¯Â¼â€ E Ad Sales', 'Managed Metadata Service')" />
                                                <input type="button" value="Cancel" onclick="    Viacom.CreateWeb.CancelPage()" style="margin-left: 5%;" />
                                            </li>
                                        </ul>
                                    </div>
                                    <div id="edit_AgencySection" class="tab-content">
                                        <ul>
                                            <li>
                                                <span class="lblText">Agency Name
                                                    
                                                    
                                                    
                                                    
                                                    <em style="color:red">*
                                                    
                                                    
                                                    
                                                    
                                                    </em>
                                                </span>
                                                <PageFieldTaxonomyFieldControl:TaxonomyFieldControl ID="TaxonomyAgencyNameEdit" FieldName="AgencyName" runat="server" ControlMode="New">
                                                </PageFieldTaxonomyFieldControl:TaxonomyFieldControl>
                                                <a href="javascript:void(0)" onclick="Viacom.createAgencyWeb.populateData()" style="float:right; margin-top:-25px; margin-right:25px">
                                                    <img title="ClientDetails" style="height: 20px" alt="Client Details" src="/_layouts/15/images/search32x32.png" />
                                                </a>
                                                <span>
                                                    <em>Click on search icon to fetch the agency details
                                                    
                                                    
                                                    
                                                    
                                                    </em>
                                                </span>
                                                <br />
                                                <br />
                                            </li>
                                            <li>
                                                <span class="lblText">Site URL
                                                
                                                
                                                
                                                
                                                </span>
                                                <div>
                                                    <input type="text" id="preAgencyUrlEdit" disabled="disabled" />
                                                </div>
                                            </li>
                                            <li>
                                                <span class="lblText">Agency ID
                                                
                                                
                                                
                                                
                                                </span>
                                                <input type="text" id="agencyIdEdit" onkeypress="return blockSpecialChar(event,false)" />
                                            </li>
                                            <li>
                                                <span class="lblText">
                                                    <p>
                                                    </p>
                                                </span>
                                                <input type="button" id="updateAgency" value="Update Site" onclick="Viacom.createAgencyWeb.addAgencyIDTerm()" style="margin-left: 0;" />
                                                <input type="button" value="Cancel" onclick="    Viacom.CreateWeb.CancelPage()" style="margin-left: 5%;" />
                                            </li>
                                        </ul>
                                    </div>
                                    <div id="edit_ClientSection" class="tab-content">
                                        <a href="/Lists/SiteMapping/ClientAgencyAssociation.aspx" style="float:right; margin: 10px 11% 0px 10px; ">View existing mapping
                                        
                                        
                                        
                                        
                                        </a>
                                        <ul>
                                            <li>
                                                <span class="lblText">Client Name
                                                    
                                                    
                                                    
                                                    
                                                    <em style="color:red">*
                                                    
                                                    
                                                    
                                                    
                                                    </em>
                                                </span>
                                                <PageFieldTaxonomyFieldControl:TaxonomyFieldControl ID="TaxonomyClientNameEdit" FieldName="ClientName" runat="server" ControlMode="New">
                                                </PageFieldTaxonomyFieldControl:TaxonomyFieldControl>
                                                <a href="javascript:void(0)" onclick="Viacom.createClientWeb.populateData()" style="float:right; margin-top:-25px; margin-right:25px">
                                                    <img title="ClientDetails" style="height: 20px" alt="Client Details" src="/_layouts/15/images/search32x32.png" />
                                                </a>
                                                <span>
                                                    <em>Click on search icon to fetch the client details
                                                    
                                                    
                                                    
                                                    
                                                    </em>
                                                </span>
                                                <br />
                                                <br />
                                            </li>
                                            <li>
                                                <span class="lblText">Site URL
                                                    
                                                    
                                                    
                                                    
                                                    <em style="color:red">*
                                                    
                                                    
                                                    
                                                    
                                                    </em>
                                                </span>
                                                <div>
                                                    <input type="text" id="preClientUrlEdit" disabled="disabled" />
                                                </div>
                                            </li>
                                            <li>
                                                <span class="lblText">Client Category
                                                    
                                                    
                                                    
                                                    
                                                    <em style="color:red">*
                                                    
                                                    
                                                    
                                                    
                                                    </em>
                                                </span>
                                                <PageFieldTaxonomyFieldControl:TaxonomyFieldControl ID="ClientCategoryEdit" FieldName="ClientCategory" runat="server" ControlMode="New">
                                                </PageFieldTaxonomyFieldControl:TaxonomyFieldControl>
                                            </li>
                                            <li>
                                                <span class="lblText">Client Segment
                                                    
                                                    
                                                    
                                                    
                                                    <em style="color:red">*
                                                    
                                                    
                                                    
                                                    
                                                    </em>
                                                </span>
                                                <PageFieldTaxonomyFieldControl:TaxonomyFieldControl ID="ClientSegmentEdit" FieldName="ClientSegment" runat="server" ControlMode="New">
                                                </PageFieldTaxonomyFieldControl:TaxonomyFieldControl>
                                            </li>
                                            <li>
                                                <span id="agencyTypeEdit1" value="Assigned Primary Linear Buying Agency" taxFieldName="Assigned Primary Linear Buying Agency" taxFieldNameProvisioning="AssignedPrimaryLinearBuyingAgenc0" class="lblText">Linear Buying Agency
                                                
                                                
                                                
                                                
                                                </span>
                                                <PageFieldTaxonomyFieldControl:TaxonomyFieldControl ID="clientAgencyNmEdit1" FieldName="AgencyNameMulti" runat="server" ControlMode="New">
                                                </PageFieldTaxonomyFieldControl:TaxonomyFieldControl>
                                            </li>
                                            <li>
                                                <span id="agencyTypeEdit2" value="Assigned Primary Digital Buying Agency" taxFieldName="Assigned Primary Digital Buying Agency" taxFieldNameProvisioning="AssignedPrimaryDigitalBuyingAgen0" class="lblText">Digital Buying Agency
                                                
                                                
                                                
                                                
                                                </span>
                                                <PageFieldTaxonomyFieldControl:TaxonomyFieldControl ID="clientAgencyNmEdit2" FieldName="AgencyNameMulti" runat="server" ControlMode="New">
                                                </PageFieldTaxonomyFieldControl:TaxonomyFieldControl>
                                            </li>
                                            <li>
                                                <span id="agencyTypeEdit3" value="Assigned Planning Agency" taxFieldName="Assigned Planning Agency" taxFieldNameProvisioning="AssignedPlanningAgency" class="lblText">Planning Agency
                                                
                                                
                                                
                                                
                                                </span>
                                                <PageFieldTaxonomyFieldControl:TaxonomyFieldControl ID="clientAgencyNmEdit3" FieldName="AgencyNameMulti" runat="server" ControlMode="New">
                                                </PageFieldTaxonomyFieldControl:TaxonomyFieldControl>
                                            </li>
                                            <li>
                                                <span id="agencyTypeEdit4" value="Direct Response Agency" taxFieldName="DirectResponseAgency" taxFieldNameProvisioning="DirectResponseAgency" class="lblText">Direct Response Agency
                                                
                                                
                                                
                                                
                                                </span>
                                                <PageFieldTaxonomyFieldControl:TaxonomyFieldControl ID="clientAgencyNmEdit4" FieldName="AgencyNameMulti" runat="server" ControlMode="New">
                                                </PageFieldTaxonomyFieldControl:TaxonomyFieldControl>
                                            </li>
                                            <li>
                                                <span id="agencyTypeEdit5" value="Barter Agency" taxFieldName="BarterAgency" taxFieldNameProvisioning="BarterAgency" class="lblText">Barter Agency
                                                
                                                
                                                
                                                
                                                </span>
                                                <PageFieldTaxonomyFieldControl:TaxonomyFieldControl ID="clientAgencyNmEdit5" FieldName="AgencyNameMulti" runat="server" ControlMode="New">
                                                </PageFieldTaxonomyFieldControl:TaxonomyFieldControl>
                                            </li>
                                            <li>
                                                <span class="lblText">RDM Client ID
                                                
                                                
                                                
                                                
                                                </span>
                                                <input type="text" id="rdmCLientIdEdit" onkeypress="return blockSpecialChar(event,false)" />
                                            </li>
                                            <li>
                                                <span class="lblText">Gabriel Client ID
                                                
                                                
                                                
                                                
                                                </span>
                                                <input type="text" id="gabrielClientIdEdit" onkeypress="return blockSpecialChar(event,false)" />
                                            </li>
                                            <li>
                                                <span class="lblText">Wide Orbit Client ID
                                                
                                                
                                                
                                                
                                                </span>
                                                <input type="text" id="wideOrbitClientIdEdit" onkeypress="return blockSpecialChar(event,false)" />
                                            </li>
                                            <li>
                                                <span class="lblText">AdFront Client ID
                                                
                                                
                                                
                                                
                                                </span>
                                                <input type="text" id="adFrontClientIdEdit" onkeypress="return blockSpecialChar(event,false)" />
                                            </li>
                                            <li>
                                                <span class="lblText">
                                                    <p>
                                                    </p>
                                                </span>
                                                <input type="button" value="Update Site" onclick="Viacom.clientGroups.UpdateSite()" />
                                                <input type="button" value="Cancel" onclick="    Viacom.CreateWeb.CancelPage()" style="margin-left: 5%;" />
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            
                            <WebPartPages:WebPartZone runat="server" AllowPersonalization="false" ID="BottomLeftZone" Title="&lt;%$Resources:cms,WebPartZoneTitle_BottomLeft%&gt;" FrameType="TitleBarOnly" Orientation="Vertical">
                                <ZoneTemplate>
                                    
                                </ZoneTemplate>
                            </WebPartPages:WebPartZone>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Content><asp:Content runat="server" ContentPlaceHolderID="PlaceHolderFormDigest">
            <SharePoint:FormDigest runat="server" />
        </asp:Content><asp:Content runat="server" ContentPlaceHolderID="PlaceHolderPageTitle">
            
            
            <SharePointWebControls:FieldValue id="PageTitle" FieldName="Title" runat="server" />
            
        </asp:Content><asp:Content runat="server" ContentPlaceHolderID="PlaceHolderAdditionalPageHead">
            
            
            <SharePoint:ScriptLink ID="ScriptLink1" Name="~sitecollection/_layouts/15/sp.runtime.js" runat="server" LoadAfterUI="true" />
            <SharePoint:ScriptLink ID="ScriptLink2" Name="~sitecollection/_layouts/15/sp.js" runat="server" LoadAfterUI="true" />
            <SharePoint:ScriptLink ID="ScriptLink3" Name="~sitecollection/_layouts/15/sp.ui.dialog.js" runat="server" LoadAfterUI="true" />
            <SharePoint:ScriptLink ID="ScriptLink4" Name="~sitecollection/_layouts/15/sp.publishing.js" runat="server" LoadAfterUI="true" />
            <SharePoint:ScriptLink ID="ScriptLink5" Name="~sitecollection/_layouts/15/sp.taxonomy.js" runat="server" LoadAfterUI="true" />
            
            <SharePointWebControls:CssRegistration ID="CssRegistration1" name="&lt;% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/pagelayouts15.css %&gt;" runat="server" />
            <SharePointWebControls:CssRegistration ID="CssRegistration2" name="&lt;% $SPUrl:~sitecollection/Style Library/CSS/AdSalesStyle.css %&gt;" runat="server" />
            
        </asp:Content><asp:Content runat="server" ContentPlaceHolderID="PlaceHolderTitleBreadcrumb">
            <SharePointWebControls:ListSiteMapPath ID="ListSiteMapPath1" runat="server" SiteMapProviders="CurrentNavigationSwitchableProvider" RenderCurrentNodeAsLink="false" PathSeparator="" CssClass="s4-breadcrumb" NodeStyle-CssClass="s4-breadcrumbNode" CurrentNodeStyle-CssClass="s4-breadcrumbCurrentNode" RootNodeStyle-CssClass="s4-breadcrumbRootNode" NodeImageOffsetX="0" NodeImageOffsetY="289" NodeImageWidth="16" NodeImageHeight="16" NodeImageUrl="/_layouts/15/images/fgimg.png?rev=23" HideInteriorRootNodes="true" SkipLinkText="" />
        </asp:Content><asp:Content runat="server" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea">
            
            
            <PageFieldFieldValue:FieldValue FieldName="fa564e0f-0c70-4ab9-b863-0177e6ddd247" runat="server">
            </PageFieldFieldValue:FieldValue>
            
        </asp:Content>