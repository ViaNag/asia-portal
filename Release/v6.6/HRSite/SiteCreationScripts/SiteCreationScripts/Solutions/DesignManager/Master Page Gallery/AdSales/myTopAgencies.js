﻿// function to process an accordion item..
var Viacom = Viacom || {};
Viacom.AdSales = Viacom.AdSales || {};
Viacom.AdSales.Count = 0;
Viacom.AdSales.getTopItems = function () {
        Viacom.AdSales.Count++;
		var Items="";
		var agencyName = ctx.CurrentItem.AgencyName;
		var agencyNameLabel = agencyName.Label;
		var agencySiteURL = ctx.CurrentItem.AgencySiteURL;
		Items += "<tr><td><a href='" + agencySiteURL + "'>" + agencyNameLabel + "</a></td></tr>";
		return Items
};
 
// anonymous self-executing function to setup JSLink templates on page load..
(function () {
var overrideCtx = {};
overrideCtx.Templates = {};
 
overrideCtx.Templates.Header = '<table border="0" class="ms-content-listing"><tbody><tr><th align="left" valign="top" class="ms-header">My Top Agencies</th><th align="right" valign="top" class="ms-edit"><a href="javascript:void(0)" onclick="Viacom.SVPAgenciesAdSales.AgencyOpenDialog(&quot;../Pages/SVPAgencyPage.aspx&quot;)">[Edit]</th></a></tr><tr><td colspan="2" class="ms-content"><table border="0" class="ms-content-rows"><tbody>';
overrideCtx.Templates.Item = Viacom.AdSales.getTopItems;

//if (Viacom.AdSales.Count == 0) {
 //   overrideCtx.Templates.Item = "There are no items to show in this view";
//}

overrideCtx.Templates.Footer = "</tbody></table></td></tr></tr></tbody></table>";
 
overrideCtx.BaseViewID = 1;
overrideCtx.ListTemplateType = 10020;
 
SPClientTemplates.TemplateManager.RegisterTemplateOverrides(overrideCtx);
})();


Viacom.SVPAgenciesAdSales={};
Viacom.SVPAgenciesAdSales.AgencyConstants=function(){
 return{
 accountName:null,
 AgenciesCtx:null
 }
};

Viacom.SVPAgenciesAdSales.AgencyOpenDialog=function(URL) {
		Viacom.SVPAgenciesAdSales.AgencyConstants.AgenciesCtx = SP.ClientContext.get_current();
		var user = Viacom.SVPAgenciesAdSales.AgencyConstants.AgenciesCtx.get_web().get_currentUser();
		Viacom.SVPAgenciesAdSales.AgencyConstants.AgenciesCtx.load(user);
		Viacom.SVPAgenciesAdSales.AgencyConstants.AgenciesCtx.executeQueryAsync(function(){
		Viacom.SVPAgenciesAdSales.AgencyConstants.accountName=user.get_loginName();
		var svpAccNmAgency=Viacom.SVPAgenciesAdSales.AgencyConstants.accountName;
				if(Viacom.SVPAgenciesAdSales.AgencyConstants.accountName.indexOf("|")>=0)
					svpAccNmAgency=svpAccNmAgency.split("|")[1];

		URL=URL+"?lstName=Site Mapping&filterField=SVPSiteURL&propertyName=MyTopAgencies&userId="+svpAccNmAgency+"&sourceUrl="+_spPageContextInfo.webServerRelativeUrl;
    var options = {  
	url:URL, 
	width: 700, 
	height: 516, 
	showClose: true,
	title:"Set Top Agencies", 
	allowMaximize: true ,
	dialogReturnValueCallback: function (dialogResult, returnVlue) {
                if (returnVlue) {
                    RefreshPage(dialogResult);
                }
            }   
	};
    SP.UI.ModalDialog.showModalDialog(options);
			}, 
		function(sender, args){console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());});
 
};

