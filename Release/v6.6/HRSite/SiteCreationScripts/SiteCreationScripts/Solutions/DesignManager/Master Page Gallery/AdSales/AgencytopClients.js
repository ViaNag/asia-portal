// function to process an accordion item..
var Viacom = Viacom || {};
Viacom.AdSales = Viacom.AdSales || {};
Viacom.AdSales.getTopItems = function () {

		var Items="";
		var clientName = ctx.CurrentItem.ClientName;
		var clientNameLabel = clientName.Label;
		var clientSiteURL = ctx.CurrentItem.ClientSiteURL;
		Items += "<tr><td><a href='" + clientSiteURL + "'>" + clientNameLabel + "</a></td></tr>";
		return Items
};
 
// anonymous self-executing function to setup JSLink templates on page load..
(function () {
var overrideCtx = {};
overrideCtx.Templates = {};
 
overrideCtx.Templates.Header = '<table border="0" class="ms-content-listing"><tbody><tr><th align="left" valign="top" class="ms-header">Agency Top Clients</th><th align="right" valign="top" class="ms-edit"><a href="AllClients.aspx">[All Clients]</th></a></tr><tr><td colspan="2" class="ms-content"><table border="0" class="ms-content-rows"><tbody>';
overrideCtx.Templates.Item = Viacom.AdSales.getTopItems;
overrideCtx.Templates.Footer = "</tbody></table></td></tr></tr></tbody></table>";
 
overrideCtx.BaseViewID = 1;
overrideCtx.ListTemplateType = 10009;
 
SPClientTemplates.TemplateManager.RegisterTemplateOverrides(overrideCtx);
})();

