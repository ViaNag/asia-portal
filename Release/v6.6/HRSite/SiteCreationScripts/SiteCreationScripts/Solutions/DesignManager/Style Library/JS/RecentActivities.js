﻿function getCallOutFilePreviewBodyContent(urlWOPIFrameSrc, pxWidth, pxHeight) {
    var callOutContenBodySection = '<div class="js-callout-bodySection">';
    callOutContenBodySection += '<div class="js-filePreview-containingElement">';
    callOutContenBodySection += '<div class="js-frame-wrapper" style="line-height: 0">';
    callOutContenBodySection += '<iframe style="width: ' + pxWidth + 'px; height: ' + pxHeight + 'px;" src="' + urlWOPIFrameSrc + '" frameborder="0" scrolling="no" ></iframe>';
    callOutContenBodySection += '</div></div></div>';

    return callOutContenBodySection;
}

function FilePreviewCallOut(sender, siteURL, LinkURL, FileExtention) {
    LinkURL = htmlUnescape(LinkURL);
    $(".ms-wpContentDivSpace").css("position", "static");
    if (siteURL != "") {
        OpenItemPreviewCalloutMSDOC(sender, siteURL, LinkURL, FileExtention);
    }
    else {
        OpenItemPreviewCallout(sender, siteURL, LinkURL, FileExtention);
    }

}
function OpenItemPreviewCalloutMSDOC(sender, siteURL, LinkURL, FileExtention) {
    var elementHref = $(sender).attr("href");
    var elementText = $(sender).text();
    var targetlement = sender;
    var link = siteURL;
    RemoveAllItemCallouts();
    var openNewWindow = true; //set this to false to open in current window
    var callOutContenBodySection = getCallOutFilePreviewBodyContent(link, 370, 322);
    var c = CalloutManager.getFromLaunchPointIfExists(sender);
    if (c == null) {
        c = CalloutManager.createNewIfNecessary({
            ID: 'CalloutId_' + sender.id,
            launchPoint: targetlement,
            beakOrientation: 'leftRight',
            title: elementText,
            contentWidth: 410,
            content: callOutContenBodySection,
        });

        var customAction = new CalloutActionOptions();
        customAction.text = 'EDIT';
        customAction.onClickCallback = function (event, action) {
            Viacom.OpenInClientAppCommon.OpenInClientApp(LinkURL, FileExtention);
        };
        var _newCustomAction = new CalloutAction(customAction);
        c.addAction(_newCustomAction);
    }
    c.open();

    var leftWidth = $(sender).width() + $(sender).offset().left;
    var windowwidth = $(window).width();
    var rightWidth = windowwidth - leftWidth;
    if (rightWidth < 410) {
        function alwaysGoUpAndRight(calloutPositioningProxy) {
            calloutPositioningProxy.moveUpAndLeft();
        }
        c.set({ positionAlgorithm: alwaysGoUpAndRight });
    }
    $(".accountplans-table, .js-callout-mainElement,.ms-cellstyle-name, .ms-cellstyle-modified").mouseleave(function () {
        RemoveAllItemCallouts();
    });
}

function OpenItemPreviewCallout(sender, siteURL, LinkURL, FileExtention) {
    var elementHref = $(sender).attr("href");
    var elementText = $(sender).text();
    var targetlement = sender;
    var link = siteURL;
    RemoveAllItemCallouts();
    var openNewWindow = true; //set this to false to open in current window
    var callOutContenBodySection = getCallOutFilePreviewBodyContent(link, 370, 322);
    var c = CalloutManager.getFromLaunchPointIfExists(sender);
    if (c == null) {
        c = CalloutManager.createNewIfNecessary({
            ID: 'CalloutId_' + sender.id,
            launchPoint: targetlement,
            beakOrientation: 'leftRight',
            title: elementText,
            content: ''
        });
        var customAction = new CalloutActionOptions();
        customAction.text = 'OPEN';
        customAction.onClickCallback = function (event, action) {
            Viacom.OpenInClientAppCommon.OpenInClientApp(LinkURL, FileExtention);
        };
        var _newCustomAction = new CalloutAction(customAction);
        c.addAction(_newCustomAction);
    }
    c.open();
    var leftWidth = $(sender).width() + $(sender).offset().left;
    var windowwidth = $(window).width();
    var rightWidth = windowwidth - leftWidth;
    if (rightWidth < 410) {
        function alwaysGoUpAndRight(calloutPositioningProxy) {
            calloutPositioningProxy.moveUpAndLeft();
        }
        c.set({ positionAlgorithm: alwaysGoUpAndRight });
    }
    $(".accountplans-table, .js-callout-mainElement, .ms-cellstyle-name, .ms-cellstyle-modified").mouseleave(function () {
        RemoveAllItemCallouts();
    });
}


function RemoveAllItemCallouts(popUpOpenInRightSide) {
    CalloutManager.forEach(function (callout) {
        // remove the current callout
        CalloutManager.remove(callout);
    });
}

function RemoveItemCallout(sender) {
    var callout = CalloutManager.getFromLaunchPointIfExists(sender);
    if (callout != null) {
        // remove
        CalloutManager.remove(callout);
    }
}


function CloseItemCallout(sender) {
    var callout = CalloutManager.getFromLaunchPointIfExists(sender);
    if (callout != null) {
        // close
        callout.close();
    }
}

function htmlUnescape(value) {
    return String(value)
        .replace(/&quot;/g, '"')
        .replace(/&#39;/g, "'")
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>')
        .replace(/&amp;/g, '&');
}

