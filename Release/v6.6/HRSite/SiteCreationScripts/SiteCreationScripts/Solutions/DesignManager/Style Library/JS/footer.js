$(document).ready(function() {
	var screenHeight = screen.height,
		footerHeight = $('#footer').outerHeight(),
		headerHeight = $('#vi-header-bar').outerHeight(),
		contentHeight = $('#s4-bodyContainer').outerHeight();

	if( contentHeight < (screenHeight - headerHeight - footerHeight) ) {
		$('#footer').addClass('sticky-footer');
	}
});