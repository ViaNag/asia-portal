var Viacom = Viacom || {};
Viacom.NewDocCustomCalloutJSLink = {};

Viacom.NewDocCustomCalloutJSLink.registerTemplateOverridesOnPostRender = function() {
	SPClientTemplates.TemplateManager.RegisterTemplateOverrides({
		OnPreRender: function(ctx) 
		{	
			if(ctx.ContentTypesEnabled)	
			{				
				SP.SOD.executeFunc('callout.js', 'callout', function () { Viacom.NewDocCustomCalloutJSLink.createViacomNewDocumentCallOut(ctx); });
			}
		}
	});	
};

Viacom.NewDocCustomCalloutJSLink.createViacomNewDocumentCallOut = function(ctx)
{

	var popUpPageParameter='&quot;'+_spPageContextInfo.webAbsoluteUrl+'/_layouts/15/CreateNewDocument.aspx?SaveLocation='+ctx.listUrlDir+'&DefaultItemOpen=1&Source='+ 
							window.location.href+ '&TemplateType=';
							
	var callOutContent = '<div id="js-newdocWOPI-divMain-WPQ2" class="ms-newdoc-callout-main" style="margin-left: 20px; margin-right: 20px;">' +
		'<a id="js-newdocWOPI-divWord-WPQ2" class="ms-newdoc-callout-item ms-displayBlock" onclick="CalloutManager.closeAll(); OpenPopUpPageWithTitle(' + 
			popUpPageParameter + '1&quot;, OnCloseDialogNavigate); return false;" href="#">' +
			'<img id="js-newdocWOPI-divWord-img-WPQ2" src="/_layouts/15/images/lg_icdocx.png?rev=23" alt="Create a new Word document" class="ms-verticalAlignMiddle ms-newdoc-callout-img"/>'+
			'<h3 id="js-newdocWOPI-divWord-txt-WPQ2" class="ms-displayInline ms-newdoc-callout-text ms-verticalAlignMiddle ms-soften">Word document</h3>' +
		'</a>' +
		'<a id="js-newdocWOPI-divExcel-WPQ2" class="ms-newdoc-callout-item ms-displayBlock" onclick="CalloutManager.closeAll(); OpenPopUpPageWithTitle(' + 
			popUpPageParameter + '2&quot;, OnCloseDialogNavigate); return false;" href="#">' +
			'<img id="js-newdocWOPI-divExcel-img-WPQ2" src="/_layouts/15/images/lg_icxlsx.png?rev=23" alt="Create a new Excel workbook" class="ms-verticalAlignMiddle ms-newdoc-callout-img"/>' +
			'<h3 id="js-newdocWOPI-divExcel-txt-WPQ2" class="ms-displayInline ms-newdoc-callout-text ms-verticalAlignMiddle ms-soften">Excel workbook</h3>' +
		'</a>' +
		'<a id="js-newdocWOPI-divPowerPoint-WPQ2" class="ms-newdoc-callout-item ms-displayBlock" onclick="CalloutManager.closeAll(); OpenPopUpPageWithTitle(' + 
			popUpPageParameter + '3&quot;, OnCloseDialogNavigate); return false;" href="#">' +
			'<img id="js-newdocWOPI-divPowerPoint-img-WPQ2" src="/_layouts/15/images/lg_icpptx.png?rev=23" alt="Create a new PowerPoint presentation" class="ms-verticalAlignMiddle ms-newdoc-callout-img"/>' +
			'<h3 id="js-newdocWOPI-divPowerPoint-txt-WPQ2" class="ms-displayInline ms-newdoc-callout-text ms-verticalAlignMiddle ms-soften">PowerPoint presentation</h3>' +
		'</a>' +
		'<a id="js-newdocWOPI-divOneNote-WPQ2" class="ms-newdoc-callout-item ms-displayBlock" onclick="CalloutManager.closeAll(); OpenPopUpPageWithTitle(' + 
			popUpPageParameter + '4&quot;, OnCloseDialogNavigate); return false;" href="#">' +
			'<img id="js-newdocWOPI-divOneNote-img-WPQ2" src="/_layouts/15/images/lg_icont.png?rev=23" alt="Create a new OneNote notebook" class="ms-verticalAlignMiddle ms-newdoc-callout-img"/>' +
			'<h3 id="js-newdocWOPI-divOneNote-txt-WPQ2" class="ms-displayInline ms-newdoc-callout-text ms-verticalAlignMiddle ms-soften">OneNote notebook</h3>' +
		'</a>' ;
		
	if(typeof ctx.AllowCreateFolder != 'undefined')
	{
		var newFolderParameter='&quot;' + _spPageContextInfo.webAbsoluteUrl+'/_layouts/15/listform.aspx?ListId='+ctx.listName+'&amp;PageType=8&amp;RootFolder='+
							ctx.rootFolder+'&amp;Type=1&quot;' 
							
		callOutContent = callOutContent + '<hr></hr>' + 
			'<a class="ms-newdoc-callout-item ms-displayBlock" id="js-newdocWOPI-divFolder-WPQ2" onclick="CalloutManager.closeAll(); NewItem2(event,' 
				+ newFolderParameter + ');return false;" href="#"><img class="ms-verticalAlignMiddle ms-newdoc-callout-img" id="js-newdocWOPI-divFolder-img-WPQ2" '+
				'alt="Create a new folder" src="/_layouts/15/images/mb_folder.png?rev=23"><h3 class="ms-displayInline ms-newdoc-callout-text ms-verticalAlignMiddle ms-soften" ' + 
				'id="js-newdocWOPI-divFolder-txt-WPQ2">New folder</h3>'+
			'</a>';				
	}
	callOutContent =callOutContent + '</div>';
	
	var targetElement = document.getElementById("idHomePageNewDocument-WPQ2");
	if(targetElement==null)
	{
		targetElement=document.getElementById("idHomePageNewItem");
	}
	targetElement.onclick=null;

	// configure options
	var createNewFilecalloutOptions = new CalloutOptions();
	createNewFilecalloutOptions.ID = 'js-newdocWOPI-WPQ2-custom';
	createNewFilecalloutOptions.launchPoint = targetElement;
	createNewFilecalloutOptions.contentWidth = 280;
	createNewFilecalloutOptions.beakOrientation = 'leftRight';
	createNewFilecalloutOptions.content = callOutContent;
	createNewFilecalloutOptions.title = 'Create a new file';
	var createNewFileCallout = CalloutManager.createNew(createNewFilecalloutOptions);

	var uploadFileActionOptions = new CalloutActionOptions();
	uploadFileActionOptions.text = 'UPLOAD EXISTING FILE';
	uploadFileActionOptions.onClickCallback = function(event, action) {
		CalloutManager.closeAll();
		
		var dialogOptions = SP.UI.$create_DialogOptions();
		dialogOptions.url = _spPageContextInfo.webAbsoluteUrl+"/_layouts/15/Upload.aspx?List=" + ctx.listName +"&Source=" +window.location.href + "&RootFolder="+ ctx.rootFolder ;
		
		dialogOptions.autoSize=true,
        dialogOptions.resizable= true,
        dialogOptions.scroll= false
		dialogOptions.title = "Upload a document";
		dialogOptions.dialogReturnValueCallback = Function.createDelegate(null, Viacom.NewDocCustomCalloutJSLink.closeCallBack);
		SP.UI.ModalDialog.showModalDialog(dialogOptions);
	};
	var uploadFileAction = new CalloutAction(uploadFileActionOptions);

	createNewFileCallout.addAction(uploadFileAction);
	
};

Viacom.NewDocCustomCalloutJSLink.closeCallBack = function (result, returnValue) 
{
	
	if (result == SP.UI.DialogResult.OK) 
	{
		  SP.UI.ModalDialog.RefreshPage(SP.UI.DialogResult.OK);
	} else 
	{
		// logic 
	}
};

//CSR-override for MDS disabled site
Viacom.NewDocCustomCalloutJSLink.registerTemplateOverridesOnPostRender();

if (typeof _spPageContextInfo != "undefined" && _spPageContextInfo != null) {          

    // CSR-override for MDS enabled site
    RegisterModuleInit(_spPageContextInfo.webAbsoluteUrl + '/Style Library/CustomCalloutJSLink.js', Viacom.NewDocCustomCalloutJSLink.registerTemplateOverridesOnPostRender)
}