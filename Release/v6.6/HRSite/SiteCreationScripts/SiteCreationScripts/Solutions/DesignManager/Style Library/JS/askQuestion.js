var Viacom=Viacom || {};
Viacom.AdSalesAskQuestion={}
Viacom.AdSalesAskQuestion.Constants=function(){
	return{
		isKaleoInjctorLoaded:false,
		kwDataSitemapValue:null,
		kwDataSitemapKey:null
	}
};

Viacom.AdSalesAskQuestion.getConfigData=function(key){
	try
	{
		var configListSiteUrl=null;
		if(_spPageContextInfo.siteAbsoluteUrl.indexOf('/sites') >0)
		{
			configListSiteUrl=_spPageContextInfo.siteAbsoluteUrl.substring(0,_spPageContextInfo.siteAbsoluteUrl.indexOf('/sites'));
		}
		else{
			configListSiteUrl=_spPageContextInfo.siteAbsoluteUrl;
		}
		$.ajax({
			url: configListSiteUrl + "/_api/web/lists/getbytitle('Configurations')/items?$select=Title,Value1&$filter=(Title eq '"+key+"')",
			method: "GET",				
			async: false,
			headers: { "Accept": "application/json; odata=verbose" },
			success: function (data) {
				
				if(data.d.results.length>0)
				{
					Viacom.AdSalesAskQuestion.Constants.kwDataSitemapValue = data.d.results[0].Value1;								  
				}
			},
			error: function (jqXHR, textStatus, error) {
			  
			}
		});
	}
	catch (error)
	{
		
	}
};	


Viacom.AdSalesAskQuestion.askQuestion=function(){
	var kwWidget = '<div id="kw-widget" class="kw-container"><div class="kw-button"></div><div class="kw-popover"><div class="kw-popover-content"><div class="kw-iframe-container"><!-- iframe will be injected here when icon is clicked --></div></div></div>';
	var parentLi = $("span:contains('Ask A Question')")[0].parentNode;
	parentLi.className = "static askQuestion";
	parentLi.href="#";
	parentLi.id="ask-Question"
	$("span:contains('Ask A Question')")[0].innerHTML='<span class="menu-item-text">Ask A Question<small class="adsales-topnav-icon-kaleo askQuestion sprite-new"></small></span>';
	$("#kw-widget").insertAfter("#ask-Question");
	/*InjectIFrame(window,document,'https://viacom.kaleosoftware.com/assets/v4/widgets/injector.js');*/
};

/*This is created to wait for injector.js loading */
Viacom.AdSalesAskQuestion.askQuestionAfterInjectorLoad=function(){
	$(".kw-iframe-container").html("");		
	$("#kw-widget").attr("data-sitemap",Viacom.AdSalesAskQuestion.Constants.kwDataSitemapValue);
	$("#kw-widget").insertAfter("#ask-Question");
	$(".kw-button").hide();
	$("#kw-widget").show();
	$(".kw-popover").show();
	$(".kw-button").click();
};

Viacom.AdSalesAskQuestion.Initialize=function(){
	Viacom.AdSalesAskQuestion.Constants();
	Viacom.AdSalesAskQuestion.Constants.kwDataSitemapKey='KW_WidgetSiteMap';
	Viacom.AdSalesAskQuestion.Constants.isKaleoInjctorLoaded=false;
	Viacom.AdSalesAskQuestion.Constants.DataSitemapValue=null;
};

$(document).ready(function() {	
	if($("span:contains('Ask A Question')").length > 0)
	{
		Viacom.AdSalesAskQuestion.Initialize();
		Viacom.AdSalesAskQuestion.getConfigData(Viacom.AdSalesAskQuestion.Constants.kwDataSitemapKey);	
		KaleoWidget = {};
		KaleoWidget.config = [
			//[DOM ID, Host URL, Widget Token]
			["kw-widget", "https://viacom.kaleosoftware.com", "026df204-3b38-4c54-88d6-c7e5b55981f8"]
		];
		if(Viacom.AdSalesAskQuestion.Constants.kwDataSitemapValue!=null && Viacom.AdSalesAskQuestion.Constants.kwDataSitemapValue!='')
		{
			Viacom.AdSalesAskQuestion.askQuestion();	

			/*Show KaleoWidget on ask a question hover */
			$('.askQuestion').on('click', function(event){
					
				if(Viacom.AdSalesAskQuestion.Constants.isKaleoInjctorLoaded==false)
				{
					var kwWidgetIframe= '<iframe width="1" height="1" style="visibility:hidden" src="https://viacom.kaleosoftware.com/users/auth/saml?redirect_to=/widgets/saml_status"> </iframe>';
					$("#iFrameForAsk-a-Question").html(kwWidgetIframe);
					var head= document.getElementsByTagName('head')[0];
					var script= document.createElement('script');
					script.type= 'text/javascript';
					script.onload= Viacom.AdSalesAskQuestion.askQuestionAfterInjectorLoad;
					script.src= 'https://viacom.kaleosoftware.com/assets/v4/widgets/injector.js';
					head.appendChild(script);
					Viacom.AdSalesAskQuestion.Constants.isKaleoInjctorLoaded=true;
				}
				else{
					Viacom.AdSalesAskQuestion.askQuestionAfterInjectorLoad();
				}
			
			});
			
			$('.kw-button').on('click', function(event){
				$(".kw-popover").attr("style","display:block");
			});
			
			/*Hide KaleoWidget on click out side KaleoWidget */
			$(document).click(function (e)
			{
				var container = $('#kw-widget');
				var askQuestionContainer = $('#ask-Question');
				var askKaleoSidebarContainer = $('#btn-greenroom-ask-kaleo');

			    if (!container.is(e.target) // if the target of the click isn't the container...
			        && container.has(e.target).length === 0 && !askQuestionContainer.is(e.target) && askQuestionContainer.has(e.target).length === 0
					&& !askKaleoSidebarContainer.is(e.target) && askKaleoSidebarContainer.has(e.target).length === 0) // ... nor a descendant of the container
			        {
						$("#kw-widget").removeClass("kw-open");
						$(".kw-iframe-container").html("");
						$("#kw-widget").attr("classname","kw-container");
			        	container.hide();
			    	}
			});
		}
		else{
			$($("span:contains('Ask A Question')")[0]).closest('li').remove();
		}
	}
});

