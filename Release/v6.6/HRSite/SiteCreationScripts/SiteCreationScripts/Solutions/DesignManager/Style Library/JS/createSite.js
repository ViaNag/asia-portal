﻿if (!window.console) {
    console = { log: function() { } };
}

var Viacom=Viacom || {};
Viacom.CreateGroup = {};
Viacom.CreateWeb = Viacom.CreateWeb || {};
Viacom.appTaxonomy = Viacom.appTaxonomy || {};
Viacom.CreateGroup.Constants=function(){
 return{
 lstGrp:null,
 siteLevel:null,
 objTerm: null,
 objTaxSVP: {},
 objPersonSVP: {},
 regionCounter: null,
 newSiteUrl: null,
 customTemplateNm: null,
 mmsName: null,
 svpWebTitle:null,
 svpEVPUrl:null
 }
};

  $(document).ready(function () {
    //For browser which don't support toISoString method
    if (!Date.prototype.toISOString) {
        (function () {
            function pad(number) {
                var r = String(number);
                if (r.length === 1) {
                    r = '0' + r;
                }
                return r;
            }
            Date.prototype.toISOString = function () {
                return this.getUTCFullYear()
                    + '-' + pad(this.getUTCMonth() + 1)
                    + '-' + pad(this.getUTCDate())
                    + 'T' + pad(this.getUTCHours())
                    + ':' + pad(this.getUTCMinutes())
                    + ':' + pad(this.getUTCSeconds())
                    + '.' + String((this.getUTCMilliseconds() / 1000).toFixed(3)).slice(2, 5)
                    + 'Z';
            };
        }());
    }
    //Viacom.CreateGroup.Initialize();
    var scriptbase = _spPageContextInfo.webServerRelativeUrl + "_layouts/15/";
    //$.getScript(scriptbase + "SP.Runtime.js",
     //   function () {
           // $.getScript(scriptbase + "SP.js", function () {
              //  $.getScript(scriptbase + "SP.Taxonomy.js", Viacom.CreateGroup.Initialize());
        //    });
      //  }
      //  );
    ExecuteOrDelayUntilScriptLoaded(Viacom.CreateGroup.Initialize, "sp.js");
    });

  Viacom.CreateGroup.Initialize = function () {
              Viacom.agencyGroups.Initialize();
              Viacom.clientGroups.Initialize();
              Viacom.CreateGroup.Constants.siteLevel = "SVP";
              Viacom.CreateGroup.Constants.lstGrp = "Site Groups Mapping";
              Viacom.CreateGroup.Constants.mmsName = "Managed Metadata Service";
			  Viacom.CreateGroup.Constants.svpEVPUrl=null;
              Viacom.CreateGroup.Constants.svpWebTitle = null;
              Viacom.CreateGroup.Constants.customTemplateNm = null;
              Viacom.CreateGroup.Constants.regionCounter = null;
              Viacom.CreateGroup.Constants.newSiteUrl = null;
              Viacom.CreateGroup.Constants.objTaxSVP = {};
              Viacom.CreateGroup.Constants.objTaxSVP.taxSVPRegValue = [];
              Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTypeValue = [];
              Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTmNameValue = [];
              Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTmEVPValue = [];

              Viacom.CreateGroup.Constants.objPersonSVP = {};
              Viacom.CreateGroup.Constants.objPersonSVP.svpNmValue = [];
              Viacom.CreateGroup.Constants.objPersonSVP.svpMgrValue = [];
              Viacom.CreateGroup.Constants.objPersonSVP.svpTmValue = [];
              Viacom.CreateGroup.Constants.objPersonSVP.svpCPDValue = [];
              Viacom.CreateGroup.Constants.objPersonSVP.svpEVPValue = [];
              Viacom.CreateGroup.Constants.objTerm = null;
              Viacom.CreateGroup.SetDefaultConstantValues();
  };

  Viacom.CreateGroup.SetDefaultConstantValues = function () {
      Viacom.CreateGroup.Constants.objTaxSVP.taxSVPRegValue.length = 0;
      Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTypeValue.length = 0;
      Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTmNameValue.length = 0;
      Viacom.CreateGroup.Constants.objPersonSVP.svpNmValue.length = 0;
      Viacom.CreateGroup.Constants.objPersonSVP.svpMgrValue.length = 0;
      Viacom.CreateGroup.Constants.objPersonSVP.svpTmValue.length = 0;
      Viacom.CreateGroup.Constants.objPersonSVP.svpCPDValue.length = 0;
      Viacom.CreateGroup.Constants.objPersonSVP.svpEVPValue.length = 0;
  };

  Viacom.appTaxonomy.loadTaxonomyJS = function (termName, termSetName, groupName, mmsName) {
		groupName = 'M＆E Ad Sales';
      SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
          SP.SOD.executeFunc('sp.taxonomy.js', 'SP.Taxonomy.TaxonomySession', function (){
              Viacom.appTaxonomy.getTaxonomyTermByName(termName, termSetName, groupName, mmsName);
          });
      });
     // Viacom.appTaxonomy.getTaxonomyTermByName(termName, termSetName, groupName, mmsName);
  }

  Viacom.appTaxonomy.validationForExistingterms = function (lstName, query, svpTerm, termName) {
      try {
          var ctxUrl = window.location.protocol+"//" + window.location.host + _spPageContextInfo.siteServerRelativeUrl;
          var clientContext = new SP.ClientContext(ctxUrl);
          var root = clientContext.get_site().get_rootWeb();
          var oList = root.get_lists().getByTitle(lstName);
          var camlQuery = new SP.CamlQuery();
          camlQuery.set_viewXml(query);
          var collListItem = oList.getItems(camlQuery);
          clientContext.load(collListItem);
          var tempSVPArr = [];
          clientContext.executeQueryAsync(
                function (sender, args) {
                    if (collListItem.get_count() != 0) {
                        var listItemInfo = '';
                        var listItemEnumerator = collListItem.getEnumerator();
                        try {
                            while (listItemEnumerator.moveNext()) {
                                var oListItem = listItemEnumerator.get_current();
                                var svpNm = oListItem.get_item('TeamName').$0_1;
                                var svpUrl = oListItem.get_item('SVPSiteUrl').$2_1;
                                tempSVPArr.push(svpNm);
                            }
                            var svpExist = false;
                            for (var m = 0; m < tempSVPArr.length; m++) {
                                if (tempSVPArr[m].toLowerCase() == termName.toLowerCase()) {
                                    svpExist = true;
                                    SP.UI.ModalDialog.commonModalDialogClose();
                                    alert("Site with svp name: " + termName + " already exists. Please choose another name.");
                                    break;
                                }
                            }
                            if (!svpExist) {
                                Viacom.CreateWeb.CreateItemInList();
                            }
                        }
                        catch (err) {
                            SP.UI.ModalDialog.commonModalDialogClose();
                            alert("Error in iterating list items which are fetched");
                            return false;
                        }
                    }
                    else {
                        Viacom.CreateWeb.CreateItemInList();
                    }
                }, function (sender, args) {
                    SP.UI.ModalDialog.commonModalDialogClose();
                    alert("Error while fetching data from SVP Data list for validation");
                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace())
                });
      }
      catch (e) {
          SP.UI.ModalDialog.commonModalDialogClose();
          alert("Error while doing validation for svp name already exist or not");
          console.log('Request failed. ' + e.message);
      }
  };

  Viacom.appTaxonomy.getTaxonomyTermByName = function (termName, termSetName, groupName, mmsName) {
      SP.UI.ModalDialog.showWaitScreenWithNoClose('Please wait...','Populating data in master list...');
      Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTmNameValue.length = 0;
      Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTmEVPValue.length = 0;
      Viacom.CreateGroup.Constants.objPersonSVP.svpNmValue.length = 0;
      Viacom.CreateGroup.Constants.objPersonSVP.svpMgrValue.length = 0;
      Viacom.CreateGroup.Constants.objPersonSVP.svpTmValue.length = 0;
      Viacom.CreateGroup.Constants.objPersonSVP.svpCPDValue.length = 0;
      Viacom.CreateGroup.Constants.objPersonSVP.svpEVPValue.length = 0;
      Viacom.CreateGroup.Constants.svpWebTitle = "Team " + termName;
      termName = "SVP Team " + termName;
      var termExist = false;
      try{
          Viacom.appTaxonomy.getTaxWebTaggingValue();
          //Viacom.appTaxonomy.getPeopleEditorValue();
          var IsValidate = Viacom.CreateWeb.ValidateFields();
          var validateEVP = false;
          if (Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTmEVPValue[0].toLowerCase().indexOf('evp team') >= 0)
              validateEVP = true;
          if (IsValidate && validateEVP) {
              var taxonomyContext = new SP.ClientContext.get_current();
              //var session = SP.Taxonomy.TaxonomySession.getTaxonomySession(taxonomyContext);
              //var termStores = session.get_termStores();
              //var termStore = termStores.getByName(mmsName);
              var session = SP.Taxonomy.TaxonomySession.getTaxonomySession(taxonomyContext);
              var termStore = session.getDefaultSiteCollectionTermStore();
              var group = termStore.get_groups().getByName(groupName);
              var termSet = group.get_termSets().getByName(termSetName);
              var termObj = termSet.get_terms().getByName(termName);
              taxonomyContext.load(termObj);
              taxonomyContext.executeQueryAsync(function () {
                  if (termObj.get_name().toLowerCase() == termName.toLowerCase()) {
                      termExist = true;
                      Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTmNameValue.push(termObj.get_name());
                      Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTmNameValue.push(termObj.get_id()._m_guidString$p$0);
                  }
                  //Viacom.CreateWeb.CreateItemInList();
                  var query = "<View><Query><Where><Contains><FieldRef Name='TeamName'/><Value Type='TaxonomyFieldType'>";
                  query = query + termName + "</Value></Contains></Where>";
                  query = query + "</Query></View>";
                  Viacom.appTaxonomy.validationForExistingterms("SVP Data", query, true, termName);
              }, function (sender, args) {
                  console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                  if (!termExist && args.get_errorCode() == -2146233086) {
                      var errorCtx = new SP.ClientContext.get_current();
                      var errorSession = SP.Taxonomy.TaxonomySession.getTaxonomySession(errorCtx);
                      //var errorTermStore = errorSession.get_termStores();
                      //var errorTermStore = errorTermStore.getByName(mmsName);
                      var errorTermStore = errorSession.getDefaultSiteCollectionTermStore();
                      var errorgroup = errorTermStore.get_groups().getByName(groupName);
                      var errorTermSet = errorgroup.get_termSets().getByName(termSetName);
                      var errornewTerm = errorTermSet.createTerm(termName, 1033, SP.Guid.newGuid()._m_guidString$p$0);
                      errorCtx.load(errornewTerm);
                      errorCtx.executeQueryAsync(function () {
                        //  alert("Term Created: " + errornewTerm.get_name());
                          Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTmNameValue.push(errornewTerm.get_name());
                          Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTmNameValue.push(errornewTerm.get_id()._m_guidString$p$0);
                          //Viacom.CreateWeb.CreateItemInList();
                          var query = "<View><Query><Where><Contains><FieldRef Name='TeamName'/><Value Type='TaxonomyFieldType'>";
                          query = query + termName + "</Value></Contains></Where>";
                          query = query + "</Query></View>";
                          Viacom.appTaxonomy.validationForExistingterms("SVP Data", query, true, termName);
                      }, function (sender, args) {
                          console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                          SP.UI.ModalDialog.commonModalDialogClose();
                          alert("Error in creating new svp term in managed metadata service");
                      });
                  }
              });
          }
          else {
              SP.UI.ModalDialog.commonModalDialogClose();
              if (!validateEVP) {
                  alert("EVP Team name is not correct. It must start with EVP Team <Team Name>");
              }
              else {
                  if ($('#svpTeamNm').val().indexOf(' ') >= 0) {
                      alert("SVP Last Name cannot have spaces");
                  }
                  else {
                      alert("Please fill all the values");
                  }
              }
          }
      }
      catch (e) {
          SP.UI.ModalDialog.commonModalDialogClose();
          alert("Error while creating or getting metadata terms");
          console.log('Request failed. ' + e.message);
      }
  };

  Viacom.appTaxonomy.getTaxWebTaggingValue = function () {
      try {
          
          var taxSVPRegId = $("span[id*='taxSVPTmRegion']")[0].children[0];
          if (taxSVPRegId.hasAttribute('value')) {
              Viacom.CreateGroup.Constants.objTaxSVP.taxSVPRegValue = taxSVPRegId.getAttribute('value').split('|');
          }
          var taxSVPTypeId = $("span[id*='taxSVPTmType']")[0].children[0];
          if (taxSVPTypeId.hasAttribute('value')) {
              Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTypeValue = taxSVPTypeId.getAttribute('value').split('|');
          }
          var taxSVPEVP = $("span[id*='taxSVPTeamEVP']")[0].children[0];
          if (taxSVPEVP.hasAttribute('value')) {
              Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTmEVPValue = taxSVPEVP.getAttribute('value').split('|');
          }
      }
      catch (e) {
          SP.UI.ModalDialog.commonModalDialogClose();
          alert("Error in reading value of Team Region and Team type");
          console.log('Request failed. ' + e.message);
      }
  };

  Viacom.appTaxonomy.getPeopleEditorValue = function () {
      try{
          var peopleArr = ["span[id*='svpNm']", "span[id*='svpMgr']", "span[id*='svpTm']", "span[id*='svpCPD']", "span[id*='svpEVP']"];
          var peopleObjArr = [Viacom.CreateGroup.Constants.objPersonSVP.svpNmValue, Viacom.CreateGroup.Constants.objPersonSVP.svpMgrValue, Viacom.CreateGroup.Constants.objPersonSVP.svpTmValue, Viacom.CreateGroup.Constants.objPersonSVP.svpCPDValue, Viacom.CreateGroup.Constants.objPersonSVP.svpEVPValue];
          for (var i = 0; i < peopleArr.length; i++) {
              Viacom.appTaxonomy.getGenericPeopleEditor(peopleArr[i], peopleObjArr[i]);
          }
      }
      catch (e) {
          SP.UI.ModalDialog.commonModalDialogClose();
          alert("Error in reading people picker control values");
          console.log('Request failed. ' + e.message);
      }
  };

  Viacom.appTaxonomy.getGenericPeopleEditor = function (spanId, peopleObj) {
      try{
          var svpNmVal = $(spanId)[0].children[0].getAttribute('Value');
          if (svpNmVal.length != 0) {
              svpNmVal=svpNmVal.replace(/"/g, "'");
              peopleObj.push(svpNmVal.split("displaytext='")[1].split("' ")[0]);
              peopleObj.push(svpNmVal.split("description='")[1].split("'")[0]);
              peopleObj.push(svpNmVal.split("key='")[1].split("'")[0]);
          }
          else {
              peopleObj.length = 0;
          }
      }
      catch (e) {
          SP.UI.ModalDialog.commonModalDialogClose();
          alert("Error in reading people picker control values");
          console.log('Request failed. ' + e.message);
      }
  };

  Viacom.appTaxonomy.getTermObject = function (termName, termSetName, groupName, mmsName) {
      try{
          var termCtx = new SP.ClientContext.get_current();
          var session = SP.Taxonomy.TaxonomySession.getTaxonomySession(termCtx);
          //var session = SP.Taxonomy.TaxonomySession.getTaxonomySession(termCtx);
          //var termStore = session.getDefaultSiteCollectionTermStore();
          var termStores = session.get_termStores();
          var termStore = termStores.getByName(mmsName);
          var group = termStore.get_groups().getByName(groupName);
          if(termName != null && termName != undefined){
              var networkSet = group.get_termSets().getByName(termSetName);
              var termObj = networkSet.get_terms().getByName(termName);
              termCtx.load(termObj);
              termCtx.executeQueryAsync(function(){
                  return termObj;
              },function (sender, args) {
                  console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                  SP.UI.ModalDialog.commonModalDialogClose();
              });
          }
      }
      catch (e) {
          SP.UI.ModalDialog.commonModalDialogClose();
          console.log('Request failed. ' + e.message);
      }
  };

  Viacom.CreateWeb.getItemFromConfigLists = function (lstName, query, beforeEntryInSVPList) {
      try{
          var clientContext = SP.ClientContext.get_current();
          var root = clientContext.get_site().get_rootWeb();
          var oList = root.get_lists().getByTitle(lstName);
          var camlQuery = new SP.CamlQuery();
          camlQuery.set_viewXml(query);
          var collListItem = oList.getItems(camlQuery);
          clientContext.load(collListItem);
          clientContext.executeQueryAsync(
                function (sender, args) {
                    if(collListItem.get_count() !=0){
                        var listItemInfo = '';
                        var listItemEnumerator = collListItem.getEnumerator();
                        try {
                            while (listItemEnumerator.moveNext()) {
                                var oListItem = listItemEnumerator.get_current();
                                Viacom.CreateGroup.Constants.regionCounter = oListItem.get_item('Counter');
                                Viacom.CreateGroup.Constants.regionCounter += 1;
                                break;
                            }
                        }
                        catch (err) {
                            SP.UI.ModalDialog.commonModalDialogClose();
                            alert("Error when iterating data fetched from SVP Data list"); 
                            return false;
                        }
                    }
                    else{
                        Viacom.CreateGroup.Constants.regionCounter = 1;
                    }
                    Viacom.CreateWeb.CallToCreateItemInLists(lstName, beforeEntryInSVPList);
                }, function (sender, args) {
                    SP.UI.ModalDialog.commonModalDialogClose();
                    alert("Error when getting data from SVP Data list to get counter for url");
                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace())
                });
      }
      catch (e) {
          SP.UI.ModalDialog.commonModalDialogClose();
          alert("Error while making query to SVP Data list to get data");
          console.log('Request failed. ' + e.message);
      }
  };
  
  Viacom.CreateWeb.CreateItemInList = function () {
      try{
          Viacom.CreateWeb.GetItemFromEVPDataList(); 
      }
      catch (e) {
          SP.UI.ModalDialog.commonModalDialogClose();
          alert("Error when it start populating data in master list");
          console.log('Request failed. ' + e.message);
      }
  };

  Viacom.CreateWeb.CalltoOtherFunctionToMakeEntry = function () {
      try{
          var query = "<View><Query><Where><Contains><FieldRef Name='TeamRegion'/><Value Type='TaxonomyFieldType'>";
          query = query + Viacom.CreateGroup.Constants.objTaxSVP.taxSVPRegValue[0] + "</Value></Contains></Where>";
          query = query + "<OrderBy><FieldRef Name='Counter' Ascending='FALSE' /></OrderBy></Query></View>";
          Viacom.CreateWeb.getItemFromConfigLists("SVP Data", query, true);  
      }
      catch (e) {
          SP.UI.ModalDialog.commonModalDialogClose();
          alert("Error when it start populating data in master list");
          console.log('Request failed. ' + e.message);
      }
  };
  
   Viacom.CreateWeb.GetItemFromEVPDataList = function () {
      try{
          var clientContext = SP.ClientContext.get_current();
          var root = clientContext.get_site().get_rootWeb();
          var oList = root.get_lists().getByTitle('EVP Data');
		  var query = "<View><Query><Where><Contains><FieldRef Name='EVPName'/><Value Type='TaxonomyFieldType'>"+Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTmEVPValue[0] +"</Value></Contains></Where></Query></View>";
          var camlQuery = new SP.CamlQuery();
          camlQuery.set_viewXml(query);
          var collListItem = oList.getItems(camlQuery);
          clientContext.load(collListItem);
          clientContext.executeQueryAsync(
                function (sender, args) {
                    if(collListItem.get_count() !=0){
                        var listItemInfo = '';
                        var listItemEnumerator = collListItem.getEnumerator();
                        try {
                            while (listItemEnumerator.moveNext()) {
                                var oListItem = listItemEnumerator.get_current();
								var evpName=oListItem.get_item('EVPName').$0_1;
								if(evpName.toLowerCase() == Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTmEVPValue[0].toLowerCase()){
                                Viacom.CreateGroup.Constants.svpEVPUrl = oListItem.get_item('EVPUrl');
								Viacom.CreateWeb.CalltoOtherFunctionToMakeEntry();
                                break;
								}
                            }
                        }
                        catch (err) {
                            SP.UI.ModalDialog.commonModalDialogClose();
                            alert("Error when iterating data fetched from EVP Data list"); 
                            return false;
                        }
                    }
                    else{
                        SP.UI.ModalDialog.commonModalDialogClose();
						alert("Error as EVP entry is not present in EVP Data list.");
						console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace())
                    }
                }, function (sender, args) {
                    SP.UI.ModalDialog.commonModalDialogClose();
                    alert("Error while fetching data from EVP Data list.");
                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                });
      }
      catch (e) {
          SP.UI.ModalDialog.commonModalDialogClose();
          alert("Error while making query to SVP Data list to get data");
          console.log('Request failed. ' + e.message);
      }
  };

  Viacom.CreateWeb.trimSVPRegValue = function (regValue) {
      regValue = regValue.trim();
      regValue = regValue.replace(/[^a-zA-Z0-9]/g, '');
      return regValue;
  }

  Viacom.CreateWeb.CallToCreateItemInLists = function (lstName, beforeEntryInSVPList) {
      try{
          if (lstName == "SVP Data" && beforeEntryInSVPList) {
              var arrObj = [[]];
              Viacom.CreateGroup.Constants.newSiteUrl = _spPageContextInfo.siteAbsoluteUrl + "/SVP/SVP" + Viacom.CreateWeb.trimSVPRegValue(Viacom.CreateGroup.Constants.objTaxSVP.taxSVPRegValue[0]) + Viacom.CreateGroup.Constants.regionCounter.toString();
              arrObj[0] = ["MMD", "TeamName", Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTmNameValue[1]];
              arrObj[1] = ["MMD", "TeamRegion", Viacom.CreateGroup.Constants.objTaxSVP.taxSVPRegValue[1]];
              arrObj[2] = ["MMD", "TeamType", Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTypeValue[1]];
              //arrObj[3] = ["User", "SVP", Viacom.CreateGroup.Constants.objPersonSVP.svpNmValue[1]];
              //arrObj[4] = ["User", "Managers", Viacom.CreateGroup.Constants.objPersonSVP.svpMgrValue[1]];
              //arrObj[5] = ["User", "Team", Viacom.CreateGroup.Constants.objPersonSVP.svpTmValue[1]];
              //arrObj[6] = ["User", "CPD", Viacom.CreateGroup.Constants.objPersonSVP.svpCPDValue[1]];
              //arrObj[7] = ["User", "EVP", Viacom.CreateGroup.Constants.objPersonSVP.svpEVPValue[1]];
              arrObj[3] = ["Number", "Counter", Viacom.CreateGroup.Constants.regionCounter];
              arrObj[4] = ["HyperLink", "SVPSiteUrl", Viacom.CreateGroup.Constants.newSiteUrl];
              Viacom.CreateWeb.MakeEntryInList("SVP Data", arrObj);
          }
          else if (lstName == "SVP Data" && (!beforeEntryInSVPList)) {
              var sitesArrObj = [[]];
              Viacom.CreateGroup.Constants.newSiteUrl = _spPageContextInfo.siteAbsoluteUrl + "/SVP/SVP" + Viacom.CreateWeb.trimSVPRegValue(Viacom.CreateGroup.Constants.objTaxSVP.taxSVPRegValue[0]) + Viacom.CreateGroup.Constants.regionCounter.toString();
              sitesArrObj[0] = ["Choice", "SiteLevel", "SVP"];
              sitesArrObj[1] = ["HyperLink", "RootSiteCollectionURL", _spPageContextInfo.siteAbsoluteUrl];
              sitesArrObj[2] = ["SLT", "SiteValue", Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTmNameValue[0]];
              sitesArrObj[3] = ["SLT", "SiteURL", Viacom.CreateGroup.Constants.newSiteUrl];
              sitesArrObj[4] = ["Date", "SiteCreationDate", new Date()];
              Viacom.CreateWeb.MakeEntryInList("Sites", sitesArrObj);
             // Viacom.CreateWeb.openDialog(Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTmNameValue[0], Viacom.CreateGroup.Constants.newSiteUrl, Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTmNameValue[0]);
              //Viacom.CreateWeb.GetWebTemplates();
          }
          else if (lstName == "EVP SVP Mapping" && (!beforeEntryInSVPList)) {
              var evpArrObj = [[]];
              Viacom.CreateGroup.Constants.newSiteUrl = _spPageContextInfo.siteAbsoluteUrl + "/SVP/SVP" + Viacom.CreateWeb.trimSVPRegValue(Viacom.CreateGroup.Constants.objTaxSVP.taxSVPRegValue[0]) + Viacom.CreateGroup.Constants.regionCounter.toString();
              evpArrObj[0] = ["MMD", "SVPTeamName", Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTmNameValue[1]];
              evpArrObj[1] = ["MMD", "EVPName", Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTmEVPValue[1]];
			  evpArrObj[2] = ["HyperLink", "EVPSiteURL", Viacom.CreateGroup.Constants.svpEVPUrl];
              Viacom.CreateWeb.MakeEntryInList("EVP SVP Mapping", evpArrObj);
              Viacom.CreateWeb.openDialog(Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTmNameValue[0], Viacom.CreateGroup.Constants.newSiteUrl, Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTmNameValue[0]);
              //Viacom.CreateWeb.GetWebTemplates();
          }
      }
      catch (e) {
          SP.UI.ModalDialog.commonModalDialogClose();
          alert("Error while preparing data to make entry in master lists");
          console.log('Request failed. ' + e.message);
      }
  };

  Viacom.CreateWeb.MakeEntryInList = function (lstName, insertItems) {
      try{
          var clientContext = SP.ClientContext.get_current();
          var root = clientContext.get_site().get_rootWeb();
          var oList = root.get_lists().getByTitle(lstName);
          var item = oList.addItem();
          for (var i = 0; i < insertItems.length; i++) {
              switch (insertItems[i][0]) {
                  case "User": {
                      if (insertItems[i][2] != undefined && insertItems[i][2] != null)
                          item.set_item(insertItems[i][1], SP.FieldUserValue.fromUser(insertItems[i][2]));
                      break;
                  }
                  case "HyperLink": {
                      if (insertItems[i][2] != undefined && insertItems[i][2] != null) {
                          var rootSiteUrl = new SP.FieldUrlValue();
                          rootSiteUrl.set_url(insertItems[i][2]);
                          rootSiteUrl.set_description(insertItems[i][2]);
                          item.set_item(insertItems[i][1], rootSiteUrl);
                      }
                      break;
                  }
                  case "Date": {
                      if (insertItems[i][2] != undefined && insertItems[i][2] != null) {
                          var iDt = insertItems[i][2].toISOString();
                          item.set_item(insertItems[i][1], iDt);
                      }
                      break;
                  }
                  default: {
                      if (insertItems[i][2] != undefined && insertItems[i][2] != null)
                          item.set_item(insertItems[i][1], insertItems[i][2]);
                  }
              }
          }
          item.update();
          clientContext.executeQueryAsync(function () {
              if (lstName == "SVP Data") {
                  Viacom.CreateWeb.CallToCreateItemInLists("SVP Data", false);
              }
              if (lstName == "Sites") {
                  Viacom.CreateWeb.CallToCreateItemInLists("EVP SVP Mapping", false);
              }
          }, function (sender, args) {
              SP.UI.ModalDialog.commonModalDialogClose();
              alert("Error while making call to funtion to make entry iin Sites list");
              console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
          });
      }
      catch (e) {
          SP.UI.ModalDialog.commonModalDialogClose();
          alert("Error while making entry in master list");
          console.log('Request failed. ' + e.message);
      }
  };

  Viacom.CreateWeb.ValidateFields = function () {
      try{
          var taxReg= Viacom.CreateGroup.Constants.objTaxSVP.taxSVPRegValue.length;
          var taxType= Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTypeValue.length;
          var taxName = $('#svpTeamNm').val();
          var taxEVP = Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTmEVPValue.length;
          var personSVP= Viacom.CreateGroup.Constants.objPersonSVP.svpNmValue.length;
          var personMgr= Viacom.CreateGroup.Constants.objPersonSVP.svpMgrValue.length;
          var personTeam= Viacom.CreateGroup.Constants.objPersonSVP.svpTmValue.length;
          var personCPD= Viacom.CreateGroup.Constants.objPersonSVP.svpCPDValue.length;
          var personEVP= Viacom.CreateGroup.Constants.objPersonSVP.svpEVPValue.length;
          //if (taxReg > 0 && taxType > 0 && (taxName != null && taxName != undefined && taxName.length !=0) && personSVP > 0 && personMgr > 0 && personTeam > 0 && personCPD > 0 && personEVP > 0) {
          //    return true;
          //}
          if (taxReg > 0 && taxType > 0 && taxEVP > 0 && (taxName != null && taxName != undefined && taxName.length != 0) && taxName.indexOf(' ') == -1) {
              return true;
          }
          else {
              return false;
          }
      }
      catch (e) {
          SP.UI.ModalDialog.commonModalDialogClose();
          alert("Error while doing validation for svp fields");
          console.log('Request failed. ' + e.message);
      }
  };

  Viacom.CreateWeb.createOptions = function(value, text, ddlId){
          var opt = document.createElement("option"); 
          opt.text = text;
          opt.value = value;
          ddlId.options.add(opt);
  };

  Viacom.appTaxonomy.loadJSBeforeDDLPopulate = function (siteName) {
     // SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
      //    SP.SOD.executeFunc('sp.taxonomy.js', 'SP.Taxonomy.TaxonomySession', Viacom.CreateWeb.setDropdownValue(siteName))});
      //var scriptbase = _spPageContextInfo.webServerRelativeUrl + "_layouts/15/";
      //$.getScript(scriptbase + "SP.Runtime.js",
      //    function () {
      //       $.getScript(scriptbase + "SP.js", function () {
      //            $.getScript(scriptbase + "SP.Taxonomy.js", Viacom.CreateWeb.setDropdownValue(siteName));
      //       });
      //    }
      //);
      Viacom.CreateWeb.setDropdownValue(siteName);
  }

  Viacom.CreateWeb.setDropdownValue = function (siteName) {
      //var mmsName = "Managed Metadata Service";
      Viacom.CreateGroup.Constants.mmsName = "Managed Metadata Service";
      var groupName = "M＆E Ad Sales";
      try {
          if (siteName == "SVPSite") {
              var svpRegTermSetNm = "SVP Team Regions";
              var svpTypeTermSetNm = "SVP Team Types";
              var ddlTaxSvpReg = document.getElementById('taxSVPTmRegion');
              var ddlTaxSvpType = document.getElementById('taxSVPTmType');
              ddlTaxSvpReg.options.length = 0;
              ddlTaxSvpType.options.length = 0;
              Viacom.CreateWeb.createOptions('', '--Select--', ddlTaxSvpReg);
              Viacom.CreateWeb.createOptions('', '--Select--', ddlTaxSvpType);
              var ddlTaxCtx = new SP.ClientContext.get_current();
              var ddlTaxSession = SP.Taxonomy.TaxonomySession.getTaxonomySession(ddlTaxCtx);
              var ddlTaxStores = ddlTaxSession.get_termStores();
              var ddlTaxtermStore = ddlTaxStores.getByName(Viacom.CreateGroup.Constants.mmsName);
              //var ddlTaxSession = SP.Taxonomy.TaxonomySession.getTaxonomySession(ddlTaxCtx);
              //var ddlTaxStores = ddlTaxSession.getDefaultSiteCollectionTermStore();
              var ddlTaxgroup = ddlTaxtermStore.get_groups().getByName(groupName);
              var svpRegTermSet = ddlTaxgroup.get_termSets().getByName(svpRegTermSetNm);
              var svpTypeTermSet = ddlTaxgroup.get_termSets().getByName(svpTypeTermSetNm);
              var svpRegTerms = svpRegTermSet.getAllTerms();
              var svpTypeTerms = svpTypeTermSet.getAllTerms();
              ddlTaxCtx.load(svpRegTerms);
              ddlTaxCtx.load(svpTypeTerms);
              ddlTaxCtx.executeQueryAsync(function () {
                  var svpRegEnumerator = svpRegTerms.getEnumerator();
                  while (svpRegEnumerator.moveNext()) {
                      var currentTerm = svpRegEnumerator.get_current();
                      Viacom.CreateWeb.createOptions(currentTerm.get_id()._m_guidString$p$0, currentTerm.get_name(), ddlTaxSvpReg);
                  }
                  var svpTypeEnumerator = svpTypeTerms.getEnumerator();
                  while (svpTypeEnumerator.moveNext()) {
                      var currentTerm = svpTypeEnumerator.get_current();
                      Viacom.CreateWeb.createOptions(currentTerm.get_id()._m_guidString$p$0, currentTerm.get_name(), ddlTaxSvpType);
                  }
              }, function (sender, args) {
                  console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace())
              });
          }
          else if (siteName == "AgencySite") {
              var svpTermSetNm = "SVP Team Names";
              var ddlTaxAgencySVP = document.getElementById('ddlTaxAgencySVPTm');
              ddlTaxAgencySVP.options.length = 0;
              Viacom.CreateWeb.createOptions('', '--Select--', ddlTaxAgencySVP);
              var ddlTaxCtx = new SP.ClientContext.get_current();
              var ddlTaxSession = SP.Taxonomy.TaxonomySession.getTaxonomySession(ddlTaxCtx);
              var ddlTaxStores = ddlTaxSession.get_termStores();
              var ddlTaxtermStore = ddlTaxStores.getByName(Viacom.CreateGroup.Constants.mmsName);
              //var ddlTaxSession = SP.Taxonomy.TaxonomySession.getTaxonomySession(ddlTaxCtx);
              //var ddlTaxStores = ddlTaxSession.getDefaultSiteCollectionTermStore();
              var ddlTaxgroup = ddlTaxtermStore.get_groups().getByName(groupName);
              var agencySVpTermSet = ddlTaxgroup.get_termSets().getByName(svpTermSetNm);
              var agencySVPTerms = agencySVpTermSet.getAllTerms();
              ddlTaxCtx.load(agencySVPTerms);
              ddlTaxCtx.executeQueryAsync(function () {
                  var agencySVPEnumerator = agencySVPTerms.getEnumerator();
                  while (agencySVPEnumerator.moveNext()) {
                      var currentTerm = agencySVPEnumerator.get_current();
                      Viacom.CreateWeb.createOptions(currentTerm.get_id()._m_guidString$p$0, currentTerm.get_name(), ddlTaxAgencySVP);
                  }
              }, function (sender, args) {
                  console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace())
              });
          }
      }
      catch (e) {
          SP.UI.ModalDialog.commonModalDialogClose();
          console.log('Request failed. ' + e.message);
      }
  };

  Viacom.CreateWeb.ddlChangeEvent = function (ddlSelected) {
      try {
          if (ddlSelected == "SVPRegion") {
              var ddlRegtext = $("#taxSVPTmRegion option:selected").text();
              var ddlRegvalue = $("#taxSVPTmRegion option:selected").val();
              if (ddlRegvalue.length != 0) {
                  Viacom.CreateGroup.Constants.objTaxSVP.taxSVPRegValue[0] = ddlRegtext;
                  Viacom.CreateGroup.Constants.objTaxSVP.taxSVPRegValue[1] = ddlRegvalue;
              }
          }
          if (ddlSelected == "SVPType") {
              var ddlTypetext = $("#taxSVPTmType option:selected").text();
              var ddlTypevalue = $("#taxSVPTmType option:selected").val();
              if (ddlTypevalue.length != 0) {
                  Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTypeValue[0] = ddlTypetext;
                  Viacom.CreateGroup.Constants.objTaxSVP.taxSVPTypeValue[1] = ddlTypevalue;
              }
          }
      }
      catch (e) {
          SP.UI.ModalDialog.commonModalDialogClose();
          console.log('Request failed. ' + e.message);
      }
  };

  Viacom.CreateWeb.openDialog = function (parameterObj, newSiteUrl, svpName) {
      try{
          var diaOptions = SP.UI.$create_DialogOptions();
          diaOptions.url = _spPageContextInfo.siteAbsoluteUrl + '/Pages/WebCreationPopup.aspx?termobj=' + parameterObj + '&newSiteUrl=' + newSiteUrl + '&svpName=' + svpName + '&siteType=SVP&webName=' + Viacom.CreateGroup.Constants.svpWebTitle+'&evpUrl='+Viacom.CreateGroup.Constants.svpEVPUrl;
          diaOptions.width = 300;
          diaOptions.height = 215;
          diaOptions.showClose = false;
          diaOptions.allowMaximize = false;
          diaOptions.title = "Please wait..";
          diaOptions.dialogReturnValueCallback = Function.createDelegate(null, Viacom.CreateWeb.CloseCallBack);
          SP.UI.ModalDialog.commonModalDialogClose();
          SP.UI.ModalDialog.showModalDialog(diaOptions);
      }
      catch (e) {
          SP.UI.ModalDialog.commonModalDialogClose();
          alert("Error while opening popup to create svp web");
          console.log('Request failed. ' + e.message);
      }
  };

  Viacom.CreateWeb.CloseCallBack = function (result, returnValue) {
      if (result == SP.UI.DialogResult.OK) {
          alert('Site created successfully!');
          location.reload();
      }
      else {
          if (returnValue != 'Fail!') {
              alert('Site created successfully!');
              location.reload();
          }
          else {
              alert('Site creation failed!');
          }
      }
  };

  Viacom.CreateWeb.CancelPage = function () {
      var rootSiteUrl = _spPageContextInfo.siteAbsoluteUrl;
      window.location.assign(rootSiteUrl);
  };