﻿var Viacom = Viacom || {};
Viacom.ShareDocumentPopUp = {}
//WebName == Site name
//WebTitle == Site name from site Url e.g "http://localhost/ACS0001" then WebTitle=ACS0001

Viacom.ShareDocumentPopUp.Constants = function () {
    return {
        fieldToFilter: 'Title',
        itemCounter: 0,
        rootSite: '/',
        listID: '',
        ItemID: '',
        buttonType: '',
        WebURL: '',
        CurrentUser: '',
        TargetSiteURL: '',
        TreeStrPermission: '',
        CheckPermission: '',
        SiteURL: '',
        WebTitle: '',
        WebName: ''       //Sprint 8-Issue name Should come in tree structure for client space
    }
};

$(document).ready(function () {
    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
        SP.UI.ModalDialog.showWaitScreenWithNoClose('Please wait...');
        Viacom.ShareDocumentPopUp.Initialize();
    });
});

//function to get query string value
Viacom.ShareDocumentPopUp.PopupgetParameterByName = function (name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
};


//variable initialize functionality 
Viacom.ShareDocumentPopUp.Initialize = function () {
    Viacom.ShareDocumentPopUp.Constants.listID = Viacom.ShareDocumentPopUp.PopupgetParameterByName('listID');
    Viacom.ShareDocumentPopUp.Constants.ItemID = Viacom.ShareDocumentPopUp.PopupgetParameterByName('ItemID');
    Viacom.ShareDocumentPopUp.Constants.buttonType = Viacom.ShareDocumentPopUp.PopupgetParameterByName('buttonType');
    Viacom.ShareDocumentPopUp.Constants.WebURL = Viacom.ShareDocumentPopUp.PopupgetParameterByName('WebURL');
    Viacom.ShareDocumentPopUp.Constants.SiteURL = Viacom.ShareDocumentPopUp.PopupgetParameterByName('SiteURL');
    Viacom.ShareDocumentPopUp.Constants.WebTitle = Viacom.ShareDocumentPopUp.PopupgetParameterByName('WebTitle');
    Viacom.ShareDocumentPopUp.Constants.TreeStrPermission = '';
    Viacom.ShareDocumentPopUp.Constants.CheckPermission = '';
    Viacom.ShareDocumentPopUp.Constants.rootSite = '/';
    Viacom.ShareDocumentPopUp.Constants.itemCounter = 0;
    Viacom.ShareDocumentPopUp.Constants.fieldToFilter = 'Title';  //Key->Title because of change of configuration list
    Viacom.ShareDocumentPopUp.Constants.CurrentUser = '';
    Viacom.ShareDocumentPopUp.Constants.TargetSiteURL = '';
    Viacom.ShareDocumentPopUp.Constants.WebName = Viacom.ShareDocumentPopUp.PopupgetParameterByName('WebName'); ////Sprint 8-Issue name Should come in tree structure for client space

    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', Viacom.ShareDocumentPopUp.GetCurrentUser);
};


//function to get current user
Viacom.ShareDocumentPopUp.GetCurrentUser = function () {
    var context = new SP.ClientContext.get_current();
    this.website = context.get_web();
    this.currentUser = website.get_currentUser();
    context.load(currentUser);
    context.executeQueryAsync(
function (sender, args) {
    Viacom.ShareDocumentPopUp.Constants.CurrentUser = currentUser.get_loginName();
    Viacom.ShareDocumentPopUp.GetConfigListData();
},
        function (sender, args) {
            SP.UI.ModalDialog.commonModalDialogClose();
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
}

Viacom.ShareDocumentPopUp.GetConfigListData = function () {

    var filterValue = '';
    var treeStructureName = '';//Sprint 8-Issue name Should come in tree structure for client space
    var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl.split('/sites/')[0]);
    var oConfigList = clientContext.get_web().get_lists().getByTitle('Configurations'); // //sharetoadspaceconfig->Configurations because of change of configuration list 
    if (Viacom.ShareDocumentPopUp.Constants.buttonType == 'AdSpace') {
        filterValue = 'AdSpace';
        treeStructureName = Viacom.ShareDocumentPopUp.Constants.WebTitle;  //Sprint 8-Issue name Should come in tree structure for client space
    }
    else {
        filterValue = 'ClientSpace';
        treeStructureName = Viacom.ShareDocumentPopUp.Constants.WebName;  //Sprint 8-Issue name Should come in tree structure for client space
    }
    var camlQuery = new SP.CamlQuery();
    var treeStrpermissionQuery = new SP.CamlQuery();
    var chkpermissionQuery = new SP.CamlQuery();
    treeStrpermissionQuery.set_viewXml("<View><Query><Where><Eq><FieldRef Name='" + Viacom.ShareDocumentPopUp.Constants.fieldToFilter + "'/><Value Type='Text'>" + 'TreeStrPermission' + "</Value></Eq></Where></Query></View>");
    chkpermissionQuery.set_viewXml("<View><Query><Where><Eq><FieldRef Name='" + Viacom.ShareDocumentPopUp.Constants.fieldToFilter + "'/><Value Type='Text'>" + 'CheckPermission' + "</Value></Eq></Where></Query></View>");
    camlQuery.set_viewXml("<View><Query><Where><Eq><FieldRef Name='" + Viacom.ShareDocumentPopUp.Constants.fieldToFilter + "'/><Value Type='Text'>" + filterValue + "</Value></Eq></Where></Query></View>");
    var collListItemtreeStrPermission = oConfigList.getItems(treeStrpermissionQuery);
    var collListItemChkrPermission = oConfigList.getItems(chkpermissionQuery);
    var collListItem = oConfigList.getItems(camlQuery);
    clientContext.load(collListItem, 'Include(Title, Value1)');   //Key->Title because of change of configuration list
    clientContext.load(collListItemChkrPermission, 'Include(Title, Value1)');  //Key->Title because of change of configuration list
    clientContext.load(collListItemtreeStrPermission, 'Include(Title, Value1)')  //Key->Title because of change of configuration list
    clientContext.executeQueryAsync(
        function (sender, args) {
            var listItemPermissionEnumerator = collListItemtreeStrPermission.getEnumerator();
            if (collListItemtreeStrPermission.get_count() > 0) {
                while (listItemPermissionEnumerator.moveNext()) {
                    var oListItem = listItemPermissionEnumerator.get_current();
                    if (oListItem.get_item('Value1') != null) {
                        Viacom.ShareDocumentPopUp.Constants.TreeStrPermission = oListItem.get_item('Value1');
                    }
                }
            }
            var listItemChkPermissionEnumerator = collListItemChkrPermission.getEnumerator();
            if (collListItemChkrPermission.get_count() > 0) {
                while (listItemChkPermissionEnumerator.moveNext()) {
                    var oListItem = listItemChkPermissionEnumerator.get_current();
                    if (oListItem.get_item('Value1') != null) {
                        Viacom.ShareDocumentPopUp.Constants.CheckPermission = oListItem.get_item('Value1');
                    }
                }
            }



            var listItemInfo = '';
            var listItemEnumerator = collListItem.getEnumerator();
            if (collListItem.get_count() > 0) {
                while (listItemEnumerator.moveNext()) {
                    var oListItem = listItemEnumerator.get_current();
                    var ValuePair = oListItem.get_item('Value1').split('~');
                    var TargetSiteName = ValuePair[0];
                    Viacom.ShareDocumentPopUp.Constants.TargetSiteURL = ValuePair[1];

                }
            }
            var targetWebURL = ValuePair[1];
            if (targetWebURL.indexOf("##SiteURL##") > -1) {
                targetWebURL = targetWebURL.replace("##SiteURL##", Viacom.ShareDocumentPopUp.Constants.WebTitle);
                Viacom.ShareDocumentPopUp.Constants.TargetSiteURL = Viacom.ShareDocumentPopUp.Constants.TargetSiteURL.replace("##SiteURL##", "");
                TargetSiteName = treeStructureName;//Viacom.ShareDocumentPopUp.Constants.WebTitle; //Sprint 8-Issue name Should come in tree structure for client space
            }
            $('#SpanSitecollection').attr('url', targetWebURL);
            $('#SpanSitecollection').text(TargetSiteName);
            Viacom.ShareDocumentPopUp.plusIconOnClick('SiteCollection');
            SP.UI.ModalDialog.commonModalDialogClose();
        },
        function (sender, args) {
            SP.UI.ModalDialog.commonModalDialogClose();
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
    );
};


//Plus icon click functionality
Viacom.ShareDocumentPopUp.plusIconOnClick = function (parm) {
    var Spanctr = $('#' + parm).next();
    var classattr = $('#' + parm).attr('class');
    var Type = Spanctr.attr('type');
    var ID = Spanctr.attr('Id');
    var Name = $("#" + ID).text()
    var SelectedItemURL = Spanctr.attr('URL');;
    var URL = '';
    var id = $('#' + parm).parent().attr('id');
    if (Type == 'Lib' || Type == 'Folder') {
        URL = $('#' + parm).parents("li[type='SubSite']").children("span").attr('URL');
    }
    else {
        URL = Spanctr.attr('URL');
    }
    if (classattr.indexOf('plus-icon') >= 0) {
        Viacom.ShareDocumentPopUp.GetTargetLocationsWebMethod(Name, URL, Type, id, SelectedItemURL);
        $('#' + parm).removeClass('tree-toggle plus-icon');
        $('#' + parm).addClass('tree-toggle minus-icon');

    }
    else {
        $('#' + parm).removeClass('tree-toggle minus-icon');
        $('#' + parm).addClass('tree-toggle plus-icon');
        $('#' + parm).parent().find('ul').remove();
    }
}

Viacom.ShareDocumentPopUp.OnLibClick = function (parm) {
    var contrl = $(parm);
    var Type = $(parm).attr('type');
    TargetwebURL = contrl.parents("li[type='SubSite']").children("span").attr('URL');
    //alert(TargetwebURL);
    TargetLibName = contrl.text();
    //alert(TargetLibName);
    //  Viacom.ShareDocumentPopUp.CheckPermission(TargetLibName, TargetwebURL, Type);
}

Viacom.ShareDocumentPopUp.OnFolderClick = function (parm) {
    var contrl = $(parm);
    var Type = $(parm).attr('type');
    TargetwebURL = contrl.attr('URL');
    //alert(TargetwebURL);
    TargetLibName = contrl.text();
    //alert(TargetLibName);
    // Viacom.ShareDocumentPopUp.CheckPermission(TargetLibName, TargetwebURL, Type);
}

Viacom.ShareDocumentPopUp.GetHTMLStr = function (xmlstr, ControlID) {
    var data = "";
    if (xmlstr != null) {
        var xmlDoc = jQuery.parseXML(xmlstr);

        if (xmlDoc) {

            var XMlType = $(xmlDoc).find('Site').attr('Type');
            if (XMlType == 'SubSite') {
                $(xmlDoc).find('Web').each(function () {
                    var $url = $(this);
                    var name = $url.text();
                    var URL = $url.attr('Url');
                    var Type = $url.attr('Type');
                    var nameID = name.replace(/ /g, '');
                    data += '<li Type="' + Type + '" id="lisubsite' + nameID + Viacom.ShareDocumentPopUp.Constants.itemCounter + '"><a href="javascript:void(0)" id="subsite' + nameID + Viacom.ShareDocumentPopUp.Constants.itemCounter + '" Type="' + Type + '" class="tree-toggle plus-icon" onClick="Viacom.ShareDocumentPopUp.plusIconOnClick(\'subsite' + nameID + Viacom.ShareDocumentPopUp.Constants.itemCounter + '\');"></a><span id="subsiteSpan' + nameID + Viacom.ShareDocumentPopUp.Constants.itemCounter + '" class="subSites" Type="' + Type + '" url="' + URL + '">' + name + '</span></li>';
                    Viacom.ShareDocumentPopUp.Constants.itemCounter++;
                });
                $(xmlDoc).find('List').each(function () {
                    var $url = $(this);
                    var name = $url.text();
                    var nameID = name.replace(/ /g, '');
                    var URL = $url.attr('Url');
                    var Type = $url.attr('Type');
                    data += '<li Type="' + Type + '" id="lidoc' + nameID + Viacom.ShareDocumentPopUp.Constants.itemCounter + '"><a href="javascript:void(0)" id="doc' + nameID + Viacom.ShareDocumentPopUp.Constants.itemCounter + '" Type="' + Type + '" class="tree-toggle plus-icon" onClick="Viacom.ShareDocumentPopUp.plusIconOnClick(\'doc' + nameID + Viacom.ShareDocumentPopUp.Constants.itemCounter + '\');"></a><span onClick="Viacom.ShareDocumentPopUp.OnLibClick(this);" id="docSpan' + nameID + Viacom.ShareDocumentPopUp.Constants.itemCounter + '" class="documents" Type="' + Type + '" url="' + URL + '">' + name + '</span></li>';
                    Viacom.ShareDocumentPopUp.Constants.itemCounter++;
                });
            }

            else if (XMlType == "Lib") {
                $(xmlDoc).find('Folder').each(function () {
                    var $url = $(this);
                    var name = $url.text();
                    var nameID = name.replace(/ /g, '');
                    var URL = $url.attr('Url');
                    var Type = $url.attr('Type');
                    data += '<li Type="' + Type + '" id="lidoc' + nameID + Viacom.ShareDocumentPopUp.Constants.itemCounter + '"><a href="javascript:void(0)" id="Folder' + nameID + Viacom.ShareDocumentPopUp.Constants.itemCounter + '" Type="' + Type + '" class="tree-toggle plus-icon" onClick="Viacom.ShareDocumentPopUp.plusIconOnClick(\'Folder' + nameID + Viacom.ShareDocumentPopUp.Constants.itemCounter + '\');"></a><span onClick="Viacom.ShareDocumentPopUp.OnFolderClick(this);" id="FolderSpan' + nameID + Viacom.ShareDocumentPopUp.Constants.itemCounter + '" class="folders" Type="' + Type + '" url="' + URL + '">' + name + '</span></li>';
                    Viacom.ShareDocumentPopUp.Constants.itemCounter++;
                });
            }
            else if (XMlType == "SubFolder") {
                $(xmlDoc).find('Folder').each(function () {
                    var $url = $(this);
                    var name = $url.text();
                    var nameID = name.replace(/ /g, '');
                    var URL = $url.attr('Url');
                    var Type = $url.attr('Type');
                    data += '<li Type="' + Type + '" id="liFolder' + nameID + Viacom.ShareDocumentPopUp.Constants.itemCounter + '"><a href="javascript:void(0)" id="Folder' + nameID + Viacom.ShareDocumentPopUp.Constants.itemCounter + '" Type="' + Type + '" class="tree-toggle plus-icon" onClick="Viacom.ShareDocumentPopUp.plusIconOnClick(\'Folder' + nameID + Viacom.ShareDocumentPopUp.Constants.itemCounter + '\');"></a><span onClick="Viacom.ShareDocumentPopUp.OnFolderClick(this);" id="Folder' + nameID + Viacom.ShareDocumentPopUp.Constants.itemCounter + '" class="folders" Type="' + Type + '" url="' + URL + '">' + name + '</span></li>';
                    Viacom.ShareDocumentPopUp.Constants.itemCounter++;
                });
            }
            else if (XMlType == "Message") {
                $(xmlDoc).find('Message').each(function () {
                    SP.UI.ModalDialog.commonModalDialogClose();
                    alert($(this).text());
                    SP.UI.ModalDialog.commonModalDialogClose(1, "Close");
                });
            }
            data = '<ul>' + data + '</ul>'
            $('#' + ControlID).append(data);
            $(".documents, .folders").click(function () {
                $('.documents, .folders').removeClass('selected');
                $(this).addClass('selected');
            });
            SP.UI.ModalDialog.commonModalDialogClose();
        }
    }
    else {
        $('#' + ControlID).children('ul').addClass('tree-first');
        $(".documents, .folders").click(function () {
            //$(this).toggleClass('selected');
            $("#btnShare").attr('disabled', false);
            $('.documents, .folders').removeClass('selected');
            $(this).addClass('selected');
        });
    }

};

Viacom.ShareDocumentPopUp.GetTargetLocationsWebMethod = function (Name, URL, Type, id, SelectedItemURL) {
    SP.UI.ModalDialog.showWaitScreenWithNoClose('Please wait...');
    var jsonData = '{Name:"' + Name + '",URL:"' + URL + '",TargetSiteURL:"' + Viacom.ShareDocumentPopUp.Constants.TargetSiteURL + '",CurrentUser:"' + Viacom.ShareDocumentPopUp.Constants.CurrentUser + '",Permission:"' + Viacom.ShareDocumentPopUp.Constants.TreeStrPermission + '",Type:"' + Type + '",oFolderURL:"' + SelectedItemURL + '"}';
    jsonData = jsonData.replace('\\', '\\\\');
    var URLValue = _spPageContextInfo.siteAbsoluteUrl + "/_catalogs/masterpage/adsales/SourceWSShareDocumentsPageLayout.aspx/SourceWSGetTargetLocations";

    $.ajax({
        type: "POST",
        url: URLValue,
        data: jsonData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: true,
        success: function (data) {
            if (data) {
                Viacom.ShareDocumentPopUp.GetHTMLStr(data.d, id);
            }
            else {
                SP.UI.ModalDialog.commonModalDialogClose();
                SP.UI.ModalDialog.RefreshPage(SP.UI.DialogResult.OK);
                return '';
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //SP.UI.ModalDialog.commonModalDialogClose();
            SP.UI.ModalDialog.commonModalDialogClose(1, 'Something went wrong!')
            // alert('Something went wrong!');
        }
    });

};

Viacom.ShareDocumentPopUp.CheckPermission = function (Name, URL, Type) {
    SP.UI.ModalDialog.showWaitScreenWithNoClose('Please wait...');
    var jsonData = '{Name:"' + Name + '",URL:"' + URL + '",TargetSiteURL:"' + Viacom.ShareDocumentPopUp.Constants.TargetSiteURL + '",CurrentUser:"' + Viacom.ShareDocumentPopUp.Constants.CurrentUser + '",Permission:"' + Viacom.ShareDocumentPopUp.Constants.CheckPermission + '",Type:"' + Type + '"}';
    jsonData = jsonData.replace('\\', '\\\\');
    var URLValue = _spPageContextInfo.siteAbsoluteUrl + "/_catalogs/masterpage/adsales/SourceWSShareDocumentsPageLayout.aspx/SourceWSCheckPermission";

    $.ajax({
        type: "POST",
        url: URLValue,
        data: jsonData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: true,
        success: function (data) {
            if (data.d == 'TRUE') {
                $("#btnShare").attr('disabled', false).removeClass("disabledButton");;
                SP.UI.ModalDialog.commonModalDialogClose();
            }
            else {
                $("#btnShare").attr('disabled', true).addClass("disabledButton");
                SP.UI.ModalDialog.commonModalDialogClose();
                return '';
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // SP.UI.ModalDialog.commonModalDialogClose();
            SP.UI.ModalDialog.commonModalDialogClose(1, 'Something went wrong!')
            //alert('Something went wrong!');
            console.log(jqXHR + '---' + textStatus + '---' + errorThrown);
        }
    });

};

Viacom.ShareDocumentPopUp.ShareButtonOnClick = function () {

    var contrl = $(".treeStrCont li .selected");
    if (contrl != null) {
        var targetFolderURL = '';
        var TargetwebURL = '';
        var TargetLibName = '';
        var Type = contrl.attr('type');
        if (Type == "Folder") {
            TargetwebURL = contrl.parents("li[type='SubSite']").children("span").attr('URL');
            targetFolderURL = contrl.parent().children("span").attr('URL');
            TargetLibName = contrl.parents("li[type='Lib']").children("span").text();

        }
        else {
            TargetwebURL = contrl.parents("li[type='SubSite']").children("span").attr('URL');
            //alert(TargetwebURL);
            TargetLibName = contrl.text();
        }
        Viacom.ShareDocumentPopUp.ShareDocument(Viacom.ShareDocumentPopUp.Constants.WebURL, Viacom.ShareDocumentPopUp.Constants.listID, Viacom.ShareDocumentPopUp.Constants.ItemID, TargetwebURL, TargetLibName, targetFolderURL, Type);

    }
    else {
        //alert('Please Select any Library or Folder');
        SP.UI.ModalDialog.commonModalDialogClose(1, 'Please Select any Library or Folder')
    }


};

Viacom.ShareDocumentPopUp.ShareDocument = function (WebURL, listID, id, TargetwebURL, TargetLibName, targetFolderURL, Type) {

    SP.UI.ModalDialog.showWaitScreenWithNoClose('Please wait...');
    listID = listID.replace("{", '');
    listID = listID.replace("}", '');

    var jsonData = '{SourceItemSiteURL:"' + WebURL + '",SourceItemLibID:"' + listID + '",SourceItemId:"' + id + '",TargetSiteURL:"' + TargetwebURL + '",TargetSiteCollURL:"' + Viacom.ShareDocumentPopUp.Constants.TargetSiteURL + '",TargetLibName:"' + TargetLibName + '",targetFolderURL:"' + targetFolderURL + '",Type:"' + Type + '",CurrentUser:"' + Viacom.ShareDocumentPopUp.Constants.CurrentUser + '"}';
    jsonData = jsonData.replace('\\', '\\\\');
    var URLValue = _spPageContextInfo.siteAbsoluteUrl + "/_catalogs/masterpage/adsales/SourceWSShareDocumentsPageLayout.aspx/SourceWSShareDocument";

    $.ajax({
        type: "POST",
        url: URLValue,
        data: jsonData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: true,
        success: function (data) {

            SP.UI.ModalDialog.commonModalDialogClose();
            SP.UI.ModalDialog.commonModalDialogClose(1, data);

        },
        error: function (jqXHR, textStatus, errorThrown) {
            SP.UI.ModalDialog.commonModalDialogClose();
            SP.UI.ModalDialog.commonModalDialogClose(1, 'Something went wrong!');
            // alert('Something went wrong!');
            console.log(jqXHR + '---' + textStatus + '---' + errorThrown);
        }
    });

};

