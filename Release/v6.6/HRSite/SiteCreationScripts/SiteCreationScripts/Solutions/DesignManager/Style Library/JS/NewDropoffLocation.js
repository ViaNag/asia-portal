﻿var Viacom = Viacom || {};
Viacom.NewDropoffLocationCustomAction = {};
Viacom.NewDropoffLocationCustomAction.enableButton = null;
Viacom.NewDropoffLocationCustomAction.selectedCT = null;
Viacom.NewDropoffLocationCustomAction.folderRelativeUrl = null;
Viacom.NewDropoffLocationCustomAction.constants = function () {
    return {
        rootFolder: null,
        dialogBoxTitle: null

    }
};
Viacom.NewDropoffLocationCustomAction.initialize = function () {
    Viacom.NewDropoffLocationCustomAction.constants();
    Viacom.NewDropoffLocationCustomAction.constants.rootFolder = 'RootFolder';
    Viacom.NewDropoffLocationCustomAction.constants.dialogBoxTitle = "Create Dropoff Location";
};

Viacom.NewDropoffLocationCustomAction.enableDisable = function () {
    Viacom.NewDropoffLocationCustomAction.initialize();
    var spContext = SP.ClientContext.get_current();
    var currentListId = SP.ListOperation.Selection.getSelectedList(spContext).replace(/[{}]/g, "");

    Viacom.NewDropoffLocationCustomAction.folderRelativeUrl = Viacom.NewDropoffLocationCustomAction.getParameterByName(Viacom.NewDropoffLocationCustomAction.constants.rootFolder);

    if (Viacom.NewDropoffLocationCustomAction.enableButton == null) {
        if (Viacom.NewDropoffLocationCustomAction.folderRelativeUrl.length > 0) {

            if (Viacom.NewDropoffLocationCustomAction.folderRelativeUrl.indexOf("'") >= 0) {
                Viacom.NewDropoffLocationCustomAction.folderRelativeUrl =encodeURIComponent(Viacom.NewDropoffLocationCustomAction.folderRelativeUrl.replace(/'/g, "''"));
            }

            var restApiCurrFoldCTOrderUrl =_spPageContextInfo.webAbsoluteUrl + "/_api/web/GetFolderByServerRelativeUrl('" + Viacom.NewDropoffLocationCustomAction.folderRelativeUrl + "')/ContentTypeOrder";
            
            $.ajax({
                url: restApiCurrFoldCTOrderUrl,
                method: "GET",
                headers: { "Accept": "application/json; odata=verbose" },
                success: function (data) {
                    Viacom.NewDropoffLocationCustomAction.enableCustomAction(data.d.ContentTypeOrder.results);
                },
                error: function (jqXHR, textStatus, error) {
                    console.log(error)
                }
            });
        }
        else {
            $.ajax({
                url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists(guid'" + currentListId + "')/RootFolder/ContentTypeOrder",
                method: "GET",
                headers: { "Accept": "application/json; odata=verbose" },
                success: function (data) {
                    Viacom.NewDropoffLocationCustomAction.enableCustomAction(data.d.ContentTypeOrder.results);
                },
                error: function (jqXHR, textStatus, error) {
                    console.log(error)
                }
            });
        }
    }
    else {
        return Viacom.NewDropoffLocationCustomAction.enableButton;
    }
};
Viacom.NewDropoffLocationCustomAction.enableCustomAction = function (contentTypeOrders) {
    for (var index = 0; index < contentTypeOrders.length; index++) {
        if (contentTypeOrders[index].StringValue.indexOf("0x0120D520") != 0) {
            Viacom.NewDropoffLocationCustomAction.enableButton = true;
            Viacom.NewDropoffLocationCustomAction.selectedCT = contentTypeOrders[index].StringValue;
            RefreshCommandUI();
            break;
        }
    }
};

Viacom.NewDropoffLocationCustomAction.openDialog = function (listID) {
    try {
        var diaOptions = SP.UI.$create_DialogOptions();
        diaOptions.url = _spPageContextInfo.webServerRelativeUrl + '/_layouts/15/Viacom.DropOff/NewDropoffLocation.aspx?List=' + listID + '&ContentTypeId=' + Viacom.NewDropoffLocationCustomAction.selectedCT + '&FolderRelativeUrl=' + Viacom.NewDropoffLocationCustomAction.folderRelativeUrl;
        diaOptions.width = 700;
        diaOptions.height = 600;
        diaOptions.showClose = true;
        diaOptions.allowMaximize = false;
        diaOptions.title = Viacom.NewDropoffLocationCustomAction.constants.dialogBoxTitle;
        diaOptions.dialogReturnValueCallback = Function.createDelegate(null, Viacom.NewDropoffLocationCustomAction.closeCallBack);
        SP.UI.ModalDialog.commonModalDialogClose();
        SP.UI.ModalDialog.showModalDialog(diaOptions);
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error while opening popup For Dropoff Location");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.NewDropoffLocationCustomAction.closeCallBack = function (result, returnValue) {
    if (result == SP.UI.DialogResult.OK) {
        window.location.href = returnValue;
    }
};
Viacom.NewDropoffLocationCustomAction.getParameterByName = function (name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};