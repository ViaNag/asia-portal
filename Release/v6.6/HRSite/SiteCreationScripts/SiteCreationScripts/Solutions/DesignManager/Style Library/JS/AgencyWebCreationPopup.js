﻿var Viacom = Viacom || {};
Viacom.agencyGroupsPopup = {};
Viacom.createAgencyWebPopup = Viacom.createAgencyWebPopup || {};
Viacom.agencySiteTaxonomyPopup = Viacom.agencySiteTaxonomyPopup || {};
Viacom.agencyGroupsPopup.Constants = function () {
    return {
        mmsName: null,
        agencySvpName: null,
        agencyCounter: null,
        newSiteUrl: null,
        customTemplateNm: null,
        templateTitle:null,
        agencyName: null,
        grpLstData: null,
        siteOwnerGrp: null,
        siteMemberGrp: null,
        siteVisitorGrp: null,
        domainValue: null,
        agencydefaultGrps:null,
		agencyPresentationName:null
    }
};

Viacom.agencyGroupsPopup.PopupgetParameterByName = function (name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
};

Viacom.agencyGroupsPopup.Initialize = function () {
    Viacom.agencyGroupsPopup.Constants.siteLevel = "Agency";
    Viacom.agencyGroupsPopup.Constants.lstGrp = "Site Groups Mapping";
    Viacom.agencyGroupsPopup.Constants.mmsName = "Managed Metadata Service";
	Viacom.agencyGroupsPopup.Constants.agencyPresentationName='Agency Presentations';
    Viacom.agencyGroupsPopup.Constants.agencyCounter = null;
    Viacom.agencyGroupsPopup.Constants.newSiteUrl = Viacom.agencyGroupsPopup.PopupgetParameterByName('newSiteUrl');
    Viacom.agencyGroupsPopup.Constants.agencySvpName = Viacom.agencyGroupsPopup.PopupgetParameterByName('svpName');
    Viacom.agencyGroupsPopup.Constants.agencyName = Viacom.agencyGroupsPopup.PopupgetParameterByName('termobj');  
    Viacom.agencyGroupsPopup.Constants.agencydefaultGrps = Viacom.agencyGroupsPopup.Constants.agencyName.replace(/[\/\\'|"@＂,:;*?<>+=]/g, ' ');
    var siteType = Viacom.agencyGroupsPopup.PopupgetParameterByName('siteType');
    Viacom.agencyGroupsPopup.Constants.customTemplateNm = null;
    Viacom.agencyGroupsPopup.Constants.templateTitle = 'Agency Web Template';
    Viacom.agencyGroupsPopup.Constants.grpLstData = {};
    Viacom.agencyGroupsPopup.Constants.grpLstData.grpName = [];
    Viacom.agencyGroupsPopup.Constants.grpLstData.grpPermission = [];
    Viacom.agencyGroupsPopup.Constants.grpLstData.grpComment = [];
    Viacom.agencyGroupsPopup.Constants.grpLstData.currentWebGroups = [];
    Viacom.agencyGroupsPopup.Constants.grpLstData.agencySVpGrp = [];
    Viacom.agencyGroupsPopup.Constants.siteOwnerGrp = null;
    Viacom.agencyGroupsPopup.Constants.siteMemberGrp = null;
    Viacom.agencyGroupsPopup.Constants.siteVisitorGrp = null;
    Viacom.agencyGroupsPopup.Constants.domainValue = null;
    //alert(siteType);
    $('#lblStatus').html('');
    if (siteType == "Agency") {
       // alert(siteType);
        $('#lblStatus').html('Getting web Templates..');
        //alert("Getting web Templates..");
        Viacom.agencyGroupsPopup.GetWebTemplates(_spPageContextInfo.siteAbsoluteUrl + "/sites/agencies/");
    }
};

Viacom.agencyGroupsPopup.GetDataSiteGrp = function () {
    try {
        Viacom.agencyGroupsPopup.Constants.grpLstData.currentWebGroups.length = 0;
        var lstCtx = SP.ClientContext.get_current();
        var root = lstCtx.get_site().get_rootWeb();
        var oList = root.get_lists().getByTitle(Viacom.agencyGroupsPopup.Constants.lstGrp);
        var filterUrl = "Agency";
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml("<View><Query><Where><Eq><FieldRef Name='SiteLevel'/><Value Type='Choice'>" + filterUrl + "</Value></Eq></Where></Query></View>");
        var collListItem = oList.getItems(camlQuery);
        lstCtx.load(collListItem);
        lstCtx.executeQueryAsync(
                            function (sender, args) {
                                var listItemInfo = '';
                                var listItemEnumerator = collListItem.getEnumerator();
                                try {
                                    while (listItemEnumerator.moveNext()) {
                                        var oListItem = listItemEnumerator.get_current();
                                        var oListItem = listItemEnumerator.get_current();
                                        Viacom.agencyGroupsPopup.Constants.grpLstData.grpName.push(oListItem.get_item('GroupName'));
                                        Viacom.agencyGroupsPopup.Constants.grpLstData.grpPermission.push(oListItem.get_item('PermissionLevel'));
                                        Viacom.agencyGroupsPopup.Constants.grpLstData.grpComment.push(oListItem.get_item('Comment'));
                                        var svpTmName = oListItem.get_item('GroupName').replace("<SVP Last Name>", Viacom.agencyGroupsPopup.Constants.agencySvpName);
                                        Viacom.agencyGroupsPopup.Constants.grpLstData.agencySVpGrp.push(svpTmName);
                                    }
                                    $('#lblStatus').html('Groups data fetched successfully for agency..');
                                    //alert('Groups data fetched successfully');
                                    Viacom.agencyGroupsPopup.RootSiteGroups();
                                    
                                }
                                catch (err) {
                                    SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
                                    return false;
                                }
                            }, function (sender, args) {
                                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                                SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
                            });
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
    }
};

Viacom.agencyGroupsPopup.getDefaultSiteGroups = function () {
    try{
        var defualtGrpCtx = new SP.ClientContext(Viacom.agencyGroupsPopup.Constants.newSiteUrl);
        var defaultGrpWeb = defualtGrpCtx.get_site().get_rootWeb();
        var ownerGroup = defaultGrpWeb.get_associatedOwnerGroup();
        var memberGroup = defaultGrpWeb.get_associatedMemberGroup();
        var visitorGroup = defaultGrpWeb.get_associatedVisitorGroup();
        defualtGrpCtx.load(ownerGroup);
        defualtGrpCtx.load(memberGroup);
        defualtGrpCtx.load(visitorGroup);
        defualtGrpCtx.executeQueryAsync(function () {
            Viacom.agencyGroupsPopup.Constants.siteOwnerGrp = ownerGroup.get_title();
            Viacom.agencyGroupsPopup.Constants.siteMemberGrp = memberGroup.get_title();
            Viacom.agencyGroupsPopup.Constants.siteVisitorGrp = visitorGroup.get_title();
            Viacom.agencyGroupsPopup.GetDataSiteGrp();
        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
    }
};

Viacom.agencyGroupsPopup.RootSiteGroups = function () {
    var grpName = [];
    var grpPermission = [];
    var grpComment = [];
    var prefixValue = Viacom.agencyGroupsPopup.Constants.agencydefaultGrps;
    grpName.push(prefixValue + " Owners");
    grpComment.push(prefixValue + " Owners");
    grpPermission.push("Full Control");
    grpName.push(prefixValue + " Members");
    grpComment.push(prefixValue + " Members");
    grpPermission.push("Contribute");
    grpName.push(prefixValue + " Visitors");
    grpComment.push(prefixValue + " Visitors");
    grpPermission.push("Read");

    try {
        var grpCtx = new SP.ClientContext(Viacom.agencyGroupsPopup.Constants.newSiteUrl);
        var grpWeb = grpCtx.get_site().get_rootWeb();
        var groupCollection = grpWeb.get_siteGroups();
        var membersGRP = new SP.GroupCreationInformation();
        for (var m = 0; m < grpName.length; m++) {
            membersGRP.set_title(grpName[m]);
            membersGRP.set_description(grpComment[m]);
            var oMembersGRP = groupCollection.add(membersGRP);
            var gpPermission = grpWeb.get_roleDefinitions().getByName(grpPermission[m]);
            var collPerm = SP.RoleDefinitionBindingCollection.newObject(grpCtx);
            collPerm.add(gpPermission);
            var assignments = grpWeb.get_roleAssignments();
            var roleAssignmentContribute = assignments.add(oMembersGRP, collPerm);
            grpCtx.load(oMembersGRP);
        }
        grpCtx.executeQueryAsync(function () {
            //alert('groupCreated');   
            $('#lblStatus').html('Groups created for agency new web..');
            Viacom.agencyGroupsPopup.Constants.grpLstData.agencySVpGrp.push(Viacom.agencyGroupsPopup.Constants.agencySvpName + " - Super Users");
			Viacom.agencyGroupsPopup.Constants.grpLstData.agencySVpGrp.push('RECORDS MANAGEMENT PROJECT GROUP');
			Viacom.agencyGroupsPopup.Constants.grpLstData.agencySVpGrp.push(prefixValue + " Owners");
			Viacom.agencyGroupsPopup.Constants.grpLstData.agencySVpGrp.push(prefixValue + " Members");
			Viacom.agencyGroupsPopup.Constants.grpLstData.agencySVpGrp.push(prefixValue + " Visitors");
            Viacom.agencyGroupsPopup.breakSecurityInheritance();
        }, function (sender, args) {
            alert('fail');
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        });
    }
    catch (e) {
        alert('fail');
        console.log('Request failed. ' + e.message);
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
    }
};

Viacom.agencyGroupsPopup.breakSecurityInheritance = function () {
    try {
        var webCtx = new SP.ClientContext(Viacom.agencyGroupsPopup.Constants.newSiteUrl);
        var web = webCtx.get_web();
        web.breakRoleInheritance(false, false);
        webCtx.load(web);
        webCtx.executeQueryAsync(function () {
            //alert("Web Permission break");
            $('#lblStatus').html('Permission break for agency web..');
            Viacom.agencyGroupsPopup.breakListsSecurityInheritance(Viacom.agencyGroupsPopup.Constants.newSiteUrl);
        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        });
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
    }
};

Viacom.agencyGroupsPopup.breakListsSecurityInheritance = function (siteUrl) {
    try {
        var clientContext = new SP.ClientContext(siteUrl);
        var oListAgPresent = clientContext.get_web().get_lists().getByTitle(Viacom.agencyGroupsPopup.Constants.agencyPresentationName);
        var oListDoc = clientContext.get_web().get_lists().getByTitle('Documents');
        var oListEvents = clientContext.get_web().get_lists().getByTitle('Events');
        var oListRest = clientContext.get_web().get_lists().getByTitle('Restricted Agency Documents');
        var oListSalesTeam = clientContext.get_web().get_lists().getByTitle('Sales Team Agency Documents');
        var oListScatter = clientContext.get_web().get_lists().getByTitle('Scatter');
        var oListUpfront= clientContext.get_web().get_lists().getByTitle('Upfront');
		
        oListAgPresent.breakRoleInheritance(false, false);
        clientContext.load(oListAgPresent);
        oListDoc.breakRoleInheritance(false, false);
        clientContext.load(oListDoc);
        oListEvents.breakRoleInheritance(false, false);
        clientContext.load(oListEvents);
        oListRest.breakRoleInheritance(false, false);
        clientContext.load(oListRest);
        oListSalesTeam.breakRoleInheritance(false, false);
        clientContext.load(oListSalesTeam);
        oListScatter.breakRoleInheritance(false, false);
        clientContext.load(oListScatter);
        oListUpfront.breakRoleInheritance(false, false);
        clientContext.load(oListUpfront);

        clientContext.executeQueryAsync(function () {
            $('#lblStatus').html('List permission break for Agency..');
            Viacom.agencyGroupsPopup.DeleteGroupsInNewWeb();
        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
            //SP.UI.ModalDialog.commonModalDialogClose();
        });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        //SP.UI.ModalDialog.commonModalDialogClose();
        console.log('Request failed. ' + e.message);
    }
};

Viacom.agencyGroupsPopup.DeleteGroupsInNewWeb = function () {
    try {
        var prefixValue = Viacom.agencyGroupsPopup.Constants.agencydefaultGrps;
        Viacom.agencyGroupsPopup.Constants.grpLstData.currentWebGroups.push("Approvers");
        Viacom.agencyGroupsPopup.Constants.grpLstData.currentWebGroups.push("Designers");
        Viacom.agencyGroupsPopup.Constants.grpLstData.currentWebGroups.push("Excel Services Viewers");
        Viacom.agencyGroupsPopup.Constants.grpLstData.currentWebGroups.push("Hierarchy Managers");
        Viacom.agencyGroupsPopup.Constants.grpLstData.currentWebGroups.push("Restricted Readers");
        Viacom.agencyGroupsPopup.Constants.grpLstData.currentWebGroups.push("Translation Managers");
        Viacom.agencyGroupsPopup.Constants.grpLstData.currentWebGroups.push("NT AUTHORITY\authenticated users");
        Viacom.agencyGroupsPopup.Constants.grpLstData.currentWebGroups.push("Quick Deploy Users");
        Viacom.agencyGroupsPopup.Constants.grpLstData.currentWebGroups.push(prefixValue + " Owners");
        Viacom.agencyGroupsPopup.Constants.grpLstData.currentWebGroups.push(prefixValue + " Members");
        Viacom.agencyGroupsPopup.Constants.grpLstData.currentWebGroups.push(prefixValue + " Visitors");
        Viacom.agencyGroupsPopup.Constants.grpLstData.currentWebGroups.push(Viacom.agencyGroupsPopup.Constants.siteOwnerGrp);
        Viacom.agencyGroupsPopup.Constants.grpLstData.currentWebGroups.push(Viacom.agencyGroupsPopup.Constants.siteMemberGrp);
        Viacom.agencyGroupsPopup.Constants.grpLstData.currentWebGroups.push(Viacom.agencyGroupsPopup.Constants.siteVisitorGrp);
        var delGrpCtx = new SP.ClientContext(Viacom.agencyGroupsPopup.Constants.newSiteUrl);
        var delGrpweb = delGrpCtx.get_web();
        var groupCollection = delGrpCtx.get_web().get_siteGroups();
        var groupsArr = [];
        groupsArr.length = Viacom.agencyGroupsPopup.Constants.grpLstData.agencySVpGrp.length;
        for (var m = 0; m < Viacom.agencyGroupsPopup.Constants.grpLstData.agencySVpGrp.length; m++) {
            groupsArr[m] = groupCollection.getByName(Viacom.agencyGroupsPopup.Constants.grpLstData.agencySVpGrp[m]);
            delGrpCtx.load(groupsArr[m]);
        }
        delGrpCtx.executeQueryAsync(function () {
            Viacom.agencyGroupsPopup.EditGroupsPermissionInNewWeb(delGrpCtx, delGrpweb, groupsArr);
        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        })
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
    }
};

Viacom.agencyGroupsPopup.EditGroupsPermissionInNewWeb = function (editGrpCtx, editGrpweb, groupsArr) {
    try {
		var oListAgPresent = editGrpweb.get_lists().getByTitle(Viacom.agencyGroupsPopup.Constants.agencyPresentationName);
        var oListEvents = editGrpweb.get_lists().getByTitle('Events');
        var oListRest = editGrpweb.get_lists().getByTitle('Restricted Agency Documents');
        var oListSalesTeam = editGrpweb.get_lists().getByTitle('Sales Team Agency Documents');
        var oListScatter = editGrpweb.get_lists().getByTitle('Scatter');
        var oListUpfront = editGrpweb.get_lists().getByTitle('Upfront');
		var prefixValue = Viacom.agencyGroupsPopup.Constants.agencydefaultGrps;
        for (var m = 0; m < Viacom.agencyGroupsPopup.Constants.grpLstData.agencySVpGrp.length; m++) {
		if(!(Viacom.agencyGroupsPopup.Constants.grpLstData.agencySVpGrp[m]=='Approvers' || Viacom.agencyGroupsPopup.Constants.grpLstData.agencySVpGrp[m]=='Designers' || Viacom.agencyGroupsPopup.Constants.grpLstData.agencySVpGrp[m]=='Hierarchy Managers' || Viacom.agencyGroupsPopup.Constants.grpLstData.agencySVpGrp[m]=='NT AUTHORITY\authenticated users' || Viacom.agencyGroupsPopup.Constants.grpLstData.agencySVpGrp[m]=='Quick Deploy Users')){
			var index=null;
		  for(var q=0;q<groupsArr.length;q++){
		   if(groupsArr[q].get_title().toLowerCase() == Viacom.agencyGroupsPopup.Constants.grpLstData.agencySVpGrp[m].toLowerCase()){
		    index=q;
		    }
		  }
            var newPerm = null;
			var listCollPerm = null;
			var collListPerm = SP.RoleDefinitionBindingCollection.newObject(editGrpCtx);
            if (Viacom.agencyGroupsPopup.Constants.grpLstData.agencySVpGrp[m].toLowerCase() == Viacom.agencyGroupsPopup.Constants.agencySvpName.toLowerCase() + " - super users" || Viacom.agencyGroupsPopup.Constants.grpLstData.agencySVpGrp[m] == 'RECORDS MANAGEMENT PROJECT GROUP') {
                newPerm = editGrpweb.get_roleDefinitions().getByName('TeamSuperUser');
				listCollPerm = editGrpweb.get_roleDefinitions().getByName("TeamSuperUser");
				collListPerm.add(listCollPerm);
				oListAgPresent.get_roleAssignments().add(groupsArr[index], collListPerm);
				oListRest.get_roleAssignments().add(groupsArr[index], collListPerm);
				oListEvents.get_roleAssignments().add(groupsArr[index], collListPerm);
				oListSalesTeam.get_roleAssignments().add(groupsArr[index], collListPerm);
				oListScatter.get_roleAssignments().add(groupsArr[index], collListPerm);
				oListUpfront.get_roleAssignments().add(groupsArr[index], collListPerm);
            }
            else if(!((groupsArr[index].get_title().toLowerCase() == (prefixValue + " Owners").toLowerCase()) || (groupsArr[index].get_title().toLowerCase() == (prefixValue + " Members").toLowerCase()) || (groupsArr[index].get_title().toLowerCase() == (prefixValue + " Visitors").toLowerCase()))) {
                newPerm = editGrpweb.get_roleDefinitions().getByName('TeamRead');
				listCollPerm = editGrpweb.get_roleDefinitions().getByName('TeamEdit');
				collListPerm.add(listCollPerm);
				oListAgPresent.get_roleAssignments().add(groupsArr[index], collListPerm);
				oListRest.get_roleAssignments().add(groupsArr[index], collListPerm);
				oListEvents.get_roleAssignments().add(groupsArr[index], collListPerm);
				oListSalesTeam.get_roleAssignments().add(groupsArr[index], collListPerm);
				oListScatter.get_roleAssignments().add(groupsArr[index], collListPerm);
				oListUpfront.get_roleAssignments().add(groupsArr[index], collListPerm);
            }
			else if(groupsArr[index].get_title().toLowerCase() == (prefixValue + " Owners").toLowerCase()){
			    newPerm = editGrpweb.get_roleDefinitions().getByName('Full Control');
			}
			else if(groupsArr[index].get_title().toLowerCase() == (prefixValue + " Members").toLowerCase()){
			    newPerm = editGrpweb.get_roleDefinitions().getByName('Contribute');
			}
			else if(groupsArr[index].get_title().toLowerCase() == (prefixValue + " Visitors").toLowerCase()){
			    newPerm = editGrpweb.get_roleDefinitions().getByName('Read');
			}
            var newcollPerm = SP.RoleDefinitionBindingCollection.newObject(editGrpCtx);
            newcollPerm.add(newPerm);
            var newWebAssgn = editGrpweb.get_roleAssignments();
            var roleAssignmentContribute = newWebAssgn.add(groupsArr[index], newcollPerm);
		 }
        }
        editGrpCtx.executeQueryAsync(function () {
            $('#lblStatus').html('Group permission edited for agency web..');
            //alert('edit group permissions');
            //Viacom.agencyGroupsPopup.GetDomainValue();
			Viacom.agencyGroupsPopup.ActivateFeature("58705871-67dc-44c0-ac97-318d1c25c9e3");
        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        });
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
    }
};

Viacom.agencyGroupsPopup.ActivateFeature= function(featureGuid)
{
    try {
        var clientContext = new SP.ClientContext(Viacom.agencyGroupsPopup.Constants.newSiteUrl);
        var web = clientContext.get_web();
        var guid = new SP.Guid(featureGuid);
        var featDef = web.get_features().add(guid, true, SP.FeatureDefinitionScope.None);
        clientContext.executeQueryAsync(function () {
            //alert('Success!');
            $('#lblStatus').html('Default value feature activated for agency web..');
            if (featureGuid == '58705871-67dc-44c0-ac97-318d1c25c9e3') {
                Viacom.agencyGroupsPopup.ActivateFeature('75bfd766-4a7c-4e3e-82f0-2cc4d4f9e8e6')
            }
            else if (featureGuid == '75bfd766-4a7c-4e3e-82f0-2cc4d4f9e8e6') {
                Viacom.agencyGroupsPopup.ActivateFeature('842ae8aa-f9b7-47d3-be14-928c54363049')
            }
            else {
                $('#lblStatus').html('Navigation feature activated for agency web..');
				Viacom.agencyGroupsPopup.GetUserToDeleteForAgencyWeb();
                //SP.UI.ModalDialog.commonModalDialogClose(1, 'Success!');
            }
        }, function (sender, args) {
            alert('feature activation problem');
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(1, 'Success!');
        });
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
    }
};

Viacom.agencyGroupsPopup.GetUserToDeleteForAgencyWeb = function (){
try {
        var getRootGrp = new SP.ClientContext(Viacom.agencyGroupsPopup.Constants.newSiteUrl);
        var getAgencyweb = getRootGrp.get_web();
        var currentUser = getRootGrp.get_web().get_currentUser();		
		getRootGrp.load(getAgencyweb);
        getRootGrp.load(currentUser);
        getRootGrp.executeQueryAsync(function () {
			 Viacom.agencyGroupsPopup.DeleteCurrentUserFromAgency(getRootGrp, getAgencyweb,currentUser);
			}, function (sender, args) {
						alert('Error while getting current user from agency Web');
						console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
						SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
			   });
	}
catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        console.log('Request failed. ' + e.message);
		}
};

Viacom.agencyGroupsPopup.DeleteCurrentUserFromAgency = function(getRootGrp, getAgencyweb,currentUser){
try {					
			var userId=currentUser.get_id();
			var delUserFromWeb = getAgencyweb.get_roleAssignments();
                delUserFromWeb = delUserFromWeb.getByPrincipalId(userId);
                delUserFromWeb.deleteObject();
                getRootGrp.load(delUserFromWeb);
				
			var oListAgPresent = getAgencyweb.get_lists().getByTitle(Viacom.agencyGroupsPopup.Constants.agencyPresentationName);
			var oListDoc = getAgencyweb.get_lists().getByTitle('Documents');
			var oListEvents = getAgencyweb.get_lists().getByTitle('Events');
			var oListRest = getAgencyweb.get_lists().getByTitle('Restricted Agency Documents');
			var oListSalesTeam = getAgencyweb.get_lists().getByTitle('Sales Team Agency Documents');
			var oListScatter = getAgencyweb.get_lists().getByTitle('Scatter');
			var oListUpfront = getAgencyweb.get_lists().getByTitle('Upfront');
			
				    var delUserFromAgPresent = oListAgPresent.get_roleAssignments();
				     delUserFromAgPresent = delUserFromAgPresent.getByPrincipalId(userId);
					 delUserFromAgPresent.deleteObject();										
					var delUserFromEvents = oListEvents.get_roleAssignments();
                     delUserFromEvents = delUserFromEvents.getByPrincipalId(userId);
                     delUserFromEvents.deleteObject();
					var delUserFromRestricted= oListRest.get_roleAssignments();
						delUserFromRestricted = delUserFromRestricted.getByPrincipalId(userId);
						delUserFromRestricted.deleteObject();
					var delUserSalesTeam = oListSalesTeam.get_roleAssignments();
						delUserSalesTeam = delUserSalesTeam.getByPrincipalId(userId);
						delUserSalesTeam.deleteObject();	
					var delUserFromDoc = oListDoc.get_roleAssignments();
						delUserFromDoc = delUserFromDoc.getByPrincipalId(userId);
						delUserFromDoc.deleteObject();
					var delUserFromScatter = oListScatter.get_roleAssignments();
					    delUserFromScatter = delUserFromScatter.getByPrincipalId(userId);
					    delUserFromScatter.deleteObject();
					var delUserFromUpfront = oListUpfront.get_roleAssignments();
					    delUserFromUpfront = delUserFromUpfront.getByPrincipalId(userId);
					    delUserFromUpfront.deleteObject();
					 
				getRootGrp.load(delUserFromAgPresent);;
				getRootGrp.load(delUserFromEvents);
				getRootGrp.load(delUserFromRestricted);
				getRootGrp.load(delUserSalesTeam);
				getRootGrp.load(delUserFromDoc);
				getRootGrp.load(delUserFromScatter);
				getRootGrp.load(delUserFromUpfront);
				getRootGrp.executeQueryAsync(function () {
				  SP.UI.ModalDialog.commonModalDialogClose(SP.UI.Dialog.OK, 'Success!');
                  }, function (sender, args) {
                        console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
							//alert('Error while deleting user from created web and its list');
							SP.UI.ModalDialog.commonModalDialogClose(SP.UI.Dialog.OK, 'Success!');
                 });
	}
catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        console.log('Request failed. ' + e.message);
		}
};

Viacom.agencyGroupsPopup.createWebsite = function (subsiteType) {
    try {
        //alert("Web creation started");
        $('#lblStatus').html('Web creation started..');
        var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl + "/" + subsiteType + "/");
        var collWeb = clientContext.get_web().get_webs();
        var webCreationInfo = new SP.WebCreationInformation();
        webCreationInfo.set_title(Viacom.agencyGroupsPopup.Constants.agencyName);
        webCreationInfo.set_description("New web for " + Viacom.agencyGroupsPopup.Constants.agencyName);
        webCreationInfo.set_language(1033);
        var url = Viacom.agencyGroupsPopup.Constants.newSiteUrl.substr(Viacom.agencyGroupsPopup.Constants.newSiteUrl.lastIndexOf("/") + 1, Viacom.agencyGroupsPopup.Constants.newSiteUrl.length);
        webCreationInfo.set_url(url);
        webCreationInfo.set_useSamePermissionsAsParentSite(true);
        webCreationInfo.set_webTemplate(Viacom.agencyGroupsPopup.Constants.customTemplateNm);
        var oNewWebsite = collWeb.add(webCreationInfo);
        clientContext.executeQueryAsync(function () {
            //alert("Created Web site.");
            $('#lblStatus').html('Created web site..');
            Viacom.agencyGroupsPopup.getDefaultSiteGroups();
        }, function (sender, args) {
            alert(args.get_message());
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        })
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
    }
};

Viacom.agencyGroupsPopup.GetWebTemplates = function (siteurl) {
    try {
        var context = new SP.ClientContext(siteurl);
        var web = context.get_web();
        var templateCollection = web.getAvailableWebTemplates(1033, false);
        context.load(templateCollection);
        context.executeQueryAsync(function () {
            var siteTemplatesEnum = templateCollection.getEnumerator();
            while (siteTemplatesEnum.moveNext()) {
                var siteTemplate = siteTemplatesEnum.get_current();
                if (siteTemplate.get_title().toLowerCase() == Viacom.agencyGroupsPopup.Constants.templateTitle.toLowerCase()) {
                    $('#lblStatus').html('Web template found..');
                    //alert("Web Template Found");
                    Viacom.agencyGroupsPopup.Constants.customTemplateNm = siteTemplate.get_name();
                    Viacom.agencyGroupsPopup.createWebsite("sites/agencies");
                    break;
                }
            }
        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace())
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        });
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
    }
};