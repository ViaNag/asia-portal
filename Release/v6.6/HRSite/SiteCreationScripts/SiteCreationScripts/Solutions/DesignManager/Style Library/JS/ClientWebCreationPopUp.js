﻿var Viacom = Viacom || {};
Viacom.clientGroupsPopup = {};
Viacom.createClientWebPopup = Viacom.createAgencyWebPopup || {};
Viacom.clientSiteTaxonomyPopup = Viacom.agencySiteTaxonomyPopup || {};
Viacom.clientGroupsPopup.Constants = function () {
    return {
        mmsName: null,
        newSiteUrl: null,
        customTemplateNm: null,
        templateTitle: null,
        clientName: null,
        grpLstData: null,
        clientGroups: null,
        recordMgmtGrp: null,
        grpRecordMgmtObj: null,
        domainValue: null,
        clientGroupsCount: null
    }
};

Viacom.clientGroupsPopup.PopupgetParameterByName = function (name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
};

Viacom.clientGroupsPopup.Initialize = function () {
    Viacom.clientGroupsPopup.Constants.siteLevel = "Client";
    Viacom.clientGroupsPopup.Constants.lstGrp = "Site Groups Mapping";
    Viacom.clientGroupsPopup.Constants.mmsName = "Managed Metadata Service";
    Viacom.clientGroupsPopup.Constants.newSiteUrl = Viacom.clientGroupsPopup.PopupgetParameterByName('newSiteUrl');
    Viacom.clientGroupsPopup.Constants.clientName = Viacom.clientGroupsPopup.PopupgetParameterByName('termobj');
    Viacom.clientGroupsPopup.Constants.agSvpName = Viacom.clientGroupsPopup.PopupgetParameterByName('svpName');
    var siteType = Viacom.clientGroupsPopup.PopupgetParameterByName('siteType');
    Viacom.clientGroupsPopup.Constants.customTemplateNm = null;
    Viacom.clientGroupsPopup.Constants.templateTitle = 'Client Web Template';
    Viacom.clientGroupsPopup.Constants.recordMgmtGrp = 'RECORDS MANAGEMENT PROJECT GROUP';
    Viacom.clientGroupsPopup.Constants.grpRecordMgmtObj = null;
    Viacom.clientGroupsPopup.Constants.domainValue = null;
    Viacom.clientGroupsPopup.Constants.clientGroupsCount = 0;
    Viacom.clientGroupsPopup.Constants.grpLstData = {};
    Viacom.clientGroupsPopup.Constants.grpLstData.grpName = [];
    Viacom.clientGroupsPopup.Constants.grpLstData.grpPermission = [];
    Viacom.clientGroupsPopup.Constants.clientGroups = [];
    Viacom.clientGroupsPopup.Constants.grpLstData.grpComment = [];
    Viacom.clientGroupsPopup.Constants.grpLstData.currentWebGroups = [];
    Viacom.clientGroupsPopup.Constants.grpLstData.clientSVpGrp = [];
    $('#lblStatus').html('');
    if (siteType == "Client") {
        $('#lblStatus').html('Processing for client web..');
        Viacom.clientGroupsPopup.GetWebTemplates(window.location.protocol+"//" + window.location.host + _spPageContextInfo.siteServerRelativeUrl + "/sites/clients/");
    }
};

Viacom.clientGroupsPopup.GetDataSiteGrp = function () {
    try {
        Viacom.clientGroupsPopup.Constants.grpLstData.currentWebGroups.length = 0;
        var lstCtx = SP.ClientContext.get_current();
        var root = lstCtx.get_site().get_rootWeb();
        var oList = root.get_lists().getByTitle(Viacom.clientGroupsPopup.Constants.lstGrp);
        var filterUrl = "Client";
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml("<View><Query><Where><Eq><FieldRef Name='SiteLevel'/><Value Type='Choice'>" + filterUrl + "</Value></Eq></Where></Query></View>");
        var collListItem = oList.getItems(camlQuery);
        lstCtx.load(collListItem);
        lstCtx.executeQueryAsync(
                            function (sender, args) {
                                var listItemInfo = '';
                                var listItemEnumerator = collListItem.getEnumerator();
                                try {
                                    while (listItemEnumerator.moveNext()) {
                                        var oListItem = listItemEnumerator.get_current();
                                        var oListItem = listItemEnumerator.get_current();
                                        Viacom.clientGroupsPopup.Constants.grpLstData.grpName.push(oListItem.get_item('GroupName'));
                                        Viacom.clientGroupsPopup.Constants.grpLstData.grpPermission.push(oListItem.get_item('PermissionLevel'));
                                        Viacom.clientGroupsPopup.Constants.grpLstData.grpComment.push(oListItem.get_item('Comment'));
                                        switch (oListItem.get_item('GroupName').toLowerCase()) {
                                            case '<svp last name> - svp': {
                                                Viacom.clientGroupsPopup.Constants.clientGroups.push('SVP - SVPs');
                                                break;
                                            }
                                            case '<svp last name> - team': {
                                                Viacom.clientGroupsPopup.Constants.clientGroups.push('SVP - Team');
                                                break;
                                            }
                                            case '<svp last name> - managers': {
                                                Viacom.clientGroupsPopup.Constants.clientGroups.push('SVP - Managers');
                                                break;
                                            }
                                            case '<svp last name> - evp': {
                                                Viacom.clientGroupsPopup.Constants.clientGroups.push('SVP - EVP');
                                                break;
                                            }
                                            case '<svp last name> - client planning': {
                                                Viacom.clientGroupsPopup.Constants.clientGroups.push('SVP - Client Planning');
                                                break;
                                            }
                                        }
                                        var svpTmName = oListItem.get_item('GroupName').replace("<SVP Last Name>", Viacom.clientGroupsPopup.Constants.agSvpName);
                                        if (svpTmName.indexOf('CPD') == -1) {
                                            Viacom.clientGroupsPopup.Constants.grpLstData.clientSVpGrp.push(svpTmName);
                                        }
                                    }
                                    Viacom.clientGroupsPopup.Constants.clientGroups.push('SVP - SuperUsers');
                                    Viacom.clientGroupsPopup.Constants.grpLstData.clientSVpGrp.push(Viacom.clientGroupsPopup.Constants.agSvpName + ' - Super Users');
                                    //alert('data');
                                    $('#lblStatus').html('Groups for client web fetched from configuration list');
                                    //Viacom.clientGroupsPopup.breakSecurityInheritance();
                                    Viacom.clientGroupsPopup.breakListsSecurityInheritance(Viacom.clientGroupsPopup.Constants.newSiteUrl);
                                }
                                catch (err) {
                                    return false;
                                }
                            }, function (sender, args) {
                                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                                SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
                            });
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
    }
};

Viacom.clientGroupsPopup.breakListsSecurityInheritance = function (siteUrl) {
    try {
        $('#lblStatus').html('Libraries permission breaking..');
        var clientContext = new SP.ClientContext(siteUrl);
        var oListClientPresent = clientContext.get_web().get_lists().getByTitle('Client Presentations');
        var oListDoc = clientContext.get_web().get_lists().getByTitle('Documents');
        var oListEvents = clientContext.get_web().get_lists().getByTitle('Events');
        var oListRest = clientContext.get_web().get_lists().getByTitle('Restricted Client Documents');
        var oListSalesTeam = clientContext.get_web().get_lists().getByTitle('Sales Team Client Documents');
        var oListCPD = clientContext.get_web().get_lists().getByTitle('CPD In Progress');
        var oListScatter = clientContext.get_web().get_lists().getByTitle('Scatter');
        var oListUpfront = clientContext.get_web().get_lists().getByTitle('Upfront');

        oListClientPresent.breakRoleInheritance(false, false);
        clientContext.load(oListClientPresent);
        oListDoc.breakRoleInheritance(false, false);
        clientContext.load(oListDoc);
        oListEvents.breakRoleInheritance(false, false);
        clientContext.load(oListEvents);
        oListRest.breakRoleInheritance(false, false);
        clientContext.load(oListRest);
        oListSalesTeam.breakRoleInheritance(false, false);
        clientContext.load(oListSalesTeam);
        oListCPD.breakRoleInheritance(false, false);
        clientContext.load(oListCPD);
        oListScatter.breakRoleInheritance(false, false);
        clientContext.load(oListScatter);
        oListUpfront.breakRoleInheritance(false, false);
        clientContext.load(oListUpfront);

        clientContext.executeQueryAsync(function () {
            $('#lblStatus').html('List permission break for Clients..');
            Viacom.clientGroupsPopup.DeleteGroupsInNewWeb();
        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
            //SP.UI.ModalDialog.commonModalDialogClose();
        });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        //SP.UI.ModalDialog.commonModalDialogClose();
        console.log('Request failed. ' + e.message);
    }
};

Viacom.clientGroupsPopup.DeleteGroupsInNewWeb = function () {
    try {
        Viacom.clientGroupsPopup.Constants.grpLstData.clientSVpGrp.push("Approvers");
        Viacom.clientGroupsPopup.Constants.grpLstData.clientSVpGrp.push("Designers");
        Viacom.clientGroupsPopup.Constants.grpLstData.clientSVpGrp.push("Excel Services Viewers");
        Viacom.clientGroupsPopup.Constants.grpLstData.clientSVpGrp.push("Hierarchy Managers");
        Viacom.clientGroupsPopup.Constants.grpLstData.clientSVpGrp.push("Restricted Readers");
        Viacom.clientGroupsPopup.Constants.grpLstData.clientSVpGrp.push("Clients Members");
        Viacom.clientGroupsPopup.Constants.grpLstData.clientSVpGrp.push("Clients Owners");
        Viacom.clientGroupsPopup.Constants.grpLstData.clientSVpGrp.push("Clients Visitors");
        Viacom.clientGroupsPopup.Constants.grpLstData.clientSVpGrp.push("Translation Managers");
        Viacom.clientGroupsPopup.Constants.grpLstData.clientSVpGrp.push("NT AUTHORITY\authenticated users");
        Viacom.clientGroupsPopup.Constants.grpLstData.clientSVpGrp.push("Quick Deploy Users");
        
        var groupsArr = [];
        groupsArr.length = 0;
        var delGrpCtx = new SP.ClientContext(Viacom.clientGroupsPopup.Constants.newSiteUrl);
        var delGrpweb = delGrpCtx.get_web();
        var groupCollection = delGrpCtx.get_web().get_siteGroups();
        Viacom.clientGroupsPopup.Constants.grpRecordMgmtObj = groupCollection.getByName(Viacom.clientGroupsPopup.Constants.recordMgmtGrp.toLowerCase());
        delGrpCtx.load(Viacom.clientGroupsPopup.Constants.grpRecordMgmtObj);
        var groupsArr = [];
        groupsArr.length = Viacom.clientGroupsPopup.Constants.clientGroups.length;
        for (var m = 0; m < Viacom.clientGroupsPopup.Constants.clientGroups.length; m++) {
            groupsArr[m] = groupCollection.getByName(Viacom.clientGroupsPopup.Constants.clientGroups[m]);
            delGrpCtx.load(groupsArr[m]);
        }
        delGrpCtx.load(groupCollection);
        delGrpCtx.executeQueryAsync(function () {
            $('#lblStatus').html('Groups which have to associate are added in array..');
            Viacom.clientGroupsPopup.EditGroupsPermissionInNewWeb(delGrpCtx, delGrpweb, groupsArr)
        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        })
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
    }
};

Viacom.clientGroupsPopup.EditGroupsPermissionInNewWeb = function (editGrpCtx, editGrpweb, groupsArr) {
    try {
        var oListClientPresent = editGrpweb.get_lists().getByTitle('Client Presentations');
        var oListEvents = editGrpweb.get_lists().getByTitle('Events');
        var oListRest = editGrpweb.get_lists().getByTitle('Restricted Client Documents');
        var oListSalesTeam = editGrpweb.get_lists().getByTitle('Sales Team Client Documents');
        var oListCPD = editGrpweb.get_lists().getByTitle('CPD In Progress');
        var oListScatter = editGrpweb.get_lists().getByTitle('Scatter');
        var oListUpfront = editGrpweb.get_lists().getByTitle('Upfront');
        for (var m = 0; m < Viacom.clientGroupsPopup.Constants.clientGroups.length; m++) {
            if (!(Viacom.clientGroupsPopup.Constants.clientGroups[m] == 'Approvers' || Viacom.clientGroupsPopup.Constants.clientGroups[m] == 'Designers' || Viacom.clientGroupsPopup.Constants.clientGroups[m] == 'Hierarchy Managers' || Viacom.clientGroupsPopup.Constants.clientGroups[m] == 'NT AUTHORITY\authenticated users' || Viacom.clientGroupsPopup.Constants.clientGroups[m] == 'Quick Deploy Users')) {
                var index = null;
                for (var q = 0; q < groupsArr.length; q++) {
                    if (groupsArr[q].get_title().toLowerCase() == Viacom.clientGroupsPopup.Constants.clientGroups[m].toLowerCase()) {
                        index = q;
                    }
                }
                var listCollPerm = null;
                var collListPerm = SP.RoleDefinitionBindingCollection.newObject(editGrpCtx);
                if (Viacom.clientGroupsPopup.Constants.clientGroups[m].toLowerCase() == ('SVP - SuperUsers').toLowerCase()) {
                    newPerm = editGrpweb.get_roleDefinitions().getByName('TeamSuperUser');
                    listCollPerm = editGrpweb.get_roleDefinitions().getByName("TeamSuperUser");
                    collListPerm.add(listCollPerm);
                    oListClientPresent.get_roleAssignments().add(groupsArr[m], collListPerm);
                    oListRest.get_roleAssignments().add(groupsArr[m], collListPerm);
                    oListEvents.get_roleAssignments().add(groupsArr[m], collListPerm);
                    oListSalesTeam.get_roleAssignments().add(groupsArr[m], collListPerm);
                    oListCPD.get_roleAssignments().add(groupsArr[m], collListPerm);
                    oListScatter.get_roleAssignments().add(groupsArr[index], collListPerm);
                    oListUpfront.get_roleAssignments().add(groupsArr[index], collListPerm);

                    // code to add record management group to libraries where permission is break
                    oListClientPresent.get_roleAssignments().add(Viacom.clientGroupsPopup.Constants.grpRecordMgmtObj, collListPerm);
                    oListRest.get_roleAssignments().add(Viacom.clientGroupsPopup.Constants.grpRecordMgmtObj, collListPerm);
                    oListEvents.get_roleAssignments().add(Viacom.clientGroupsPopup.Constants.grpRecordMgmtObj, collListPerm);
                    oListSalesTeam.get_roleAssignments().add(Viacom.clientGroupsPopup.Constants.grpRecordMgmtObj, collListPerm);
                    oListCPD.get_roleAssignments().add(Viacom.clientGroupsPopup.Constants.grpRecordMgmtObj, collListPerm);
                    oListScatter.get_roleAssignments().add(Viacom.clientGroupsPopup.Constants.grpRecordMgmtObj, collListPerm);
                    oListUpfront.get_roleAssignments().add(Viacom.clientGroupsPopup.Constants.grpRecordMgmtObj, collListPerm);
                }
                else {
                    listCollPerm = editGrpweb.get_roleDefinitions().getByName('TeamEdit');
                    collListPerm.add(listCollPerm);
                    oListClientPresent.get_roleAssignments().add(groupsArr[m], collListPerm);
                    oListRest.get_roleAssignments().add(groupsArr[m], collListPerm);
                    oListEvents.get_roleAssignments().add(groupsArr[m], collListPerm);
                    oListSalesTeam.get_roleAssignments().add(groupsArr[m], collListPerm);
                    oListCPD.get_roleAssignments().add(groupsArr[m], collListPerm);
                    oListScatter.get_roleAssignments().add(groupsArr[index], collListPerm);
                    oListUpfront.get_roleAssignments().add(groupsArr[index], collListPerm);
                }
            }
        }
        editGrpCtx.executeQueryAsync(function () {
            $('#lblStatus').html('Group permission edited for agency web..');
            //alert('edit group permissions');
            Viacom.clientGroupsPopup.ActivateFeature('0e7e0863-72ba-4a9a-aa19-b99031ebf607');
        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        });
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
    }
};

Viacom.clientGroupsPopup.ActivateFeature = function (featureGuid) {
    var clientContext = new SP.ClientContext(Viacom.clientGroupsPopup.Constants.newSiteUrl);
    var web = clientContext.get_web();
    var guid = new SP.Guid(featureGuid);
    var featDef = web.get_features().add(guid, true, SP.FeatureDefinitionScope.None);
    clientContext.executeQueryAsync(function () {
        // alert('Success!');
        $('#lblStatus').html('Default value feature for client activated successfully');
        if (featureGuid == '0e7e0863-72ba-4a9a-aa19-b99031ebf607') {
            Viacom.clientGroupsPopup.ActivateFeature('5e70b69d-4909-497c-850b-8e7654d4cc3e')
        }
        else if (featureGuid == '5e70b69d-4909-497c-850b-8e7654d4cc3e') {
            Viacom.clientGroupsPopup.ActivateFeature('842ae8aa-f9b7-47d3-be14-928c54363049')
        }
        else {
            $('#lblStatus').html('Navigation feature activated for client web..');
            //SP.UI.ModalDialog.commonModalDialogClose(1, 'Success
            Viacom.clientGroupsPopup.GetUserToDeleteForClientWeb();
        }
    }, function (sender, args) {
        console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        SP.UI.ModalDialog.commonModalDialogClose(1, 'Success!');
    })
}

Viacom.clientGroupsPopup.GetUserToDeleteForClientWeb = function () {
    try {
        var getRootGrp = new SP.ClientContext(Viacom.clientGroupsPopup.Constants.newSiteUrl);
        var getClientweb = getRootGrp.get_web();
        var currentUser = getRootGrp.get_web().get_currentUser();
        getRootGrp.load(getClientweb);
        getRootGrp.load(currentUser);
        getRootGrp.executeQueryAsync(function () {
            Viacom.clientGroupsPopup.DeleteCurrentUserFromClient(getRootGrp, getClientweb, currentUser);
        }, function (sender, args) {
            alert('Error while getting current user from agency Web');
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        console.log('Request failed. ' + e.message);
    }
};

Viacom.clientGroupsPopup.DeleteCurrentUserFromClient = function (getRootGrp, getClientweb, currentUser) {
    try {
        var userId = currentUser.get_id();

        var oListClientPresent = getClientweb.get_lists().getByTitle('Client Presentations');
        var oListDoc = getClientweb.get_lists().getByTitle('Documents');
        var oListEvents = getClientweb.get_lists().getByTitle('Events');
        var oListRest = getClientweb.get_lists().getByTitle('Restricted Client Documents');
        var oListSalesTeam = getClientweb.get_lists().getByTitle('Sales Team Client Documents');
        var oListCPD = getClientweb.get_lists().getByTitle('CPD In Progress');
        var oListScatter = getClientweb.get_lists().getByTitle('Scatter');
        var oListUpfront = getClientweb.get_lists().getByTitle('Upfront');

        var delUserFromClientPresent = oListClientPresent.get_roleAssignments();
        delUserFromClientPresent = delUserFromClientPresent.getByPrincipalId(userId);
        delUserFromClientPresent.deleteObject();
        var delUserFromEvents = oListEvents.get_roleAssignments();
        delUserFromEvents = delUserFromEvents.getByPrincipalId(userId);
        delUserFromEvents.deleteObject();
        var delUserFromRestricted = oListRest.get_roleAssignments();
        delUserFromRestricted = delUserFromRestricted.getByPrincipalId(userId);
        delUserFromRestricted.deleteObject();
        var delUserSalesTeam = oListSalesTeam.get_roleAssignments();
        delUserSalesTeam = delUserSalesTeam.getByPrincipalId(userId);
        delUserSalesTeam.deleteObject();
        var delUserFromCPD = oListCPD.get_roleAssignments();
        delUserFromCPD = delUserFromCPD.getByPrincipalId(userId);
        delUserFromCPD.deleteObject();
        var delUserFromDoc = oListDoc.get_roleAssignments();
        delUserFromDoc = delUserFromDoc.getByPrincipalId(userId);
        delUserFromDoc.deleteObject();
        var delUserFromScatter = oListScatter.get_roleAssignments();
        delUserFromScatter = delUserFromScatter.getByPrincipalId(userId);
        delUserFromScatter.deleteObject();
        var delUserFromUpfront = oListUpfront.get_roleAssignments();
        delUserFromUpfront = delUserFromUpfront.getByPrincipalId(userId);
        delUserFromUpfront.deleteObject();

        getRootGrp.load(delUserFromClientPresent);;
        getRootGrp.load(delUserFromEvents);
        getRootGrp.load(delUserFromRestricted);
        getRootGrp.load(delUserSalesTeam);
        getRootGrp.load(delUserFromDoc);
        getRootGrp.load(delUserFromCPD);
        getRootGrp.load(delUserFromScatter);
        getRootGrp.load(delUserFromUpfront);
        getRootGrp.executeQueryAsync(function () {
            SP.UI.ModalDialog.commonModalDialogClose(SP.UI.Dialog.OK, 'Success!');
        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            //alert('Error while deleting user from created web and its list');
            SP.UI.ModalDialog.commonModalDialogClose(SP.UI.Dialog.OK, 'Success!');
        });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        console.log('Request failed. ' + e.message);
    }
};

Viacom.clientGroupsPopup.createWebsite = function (subsiteType) {
    try {
        // alert("started");
        $('#lblStatus').html('Client web creation started.');
        var clientContext = new SP.ClientContext(window.location.protocol+"//" + window.location.host + _spPageContextInfo.siteServerRelativeUrl + "/" + subsiteType + "/");
        var collWeb = clientContext.get_web().get_webs();
        var webCreationInfo = new SP.WebCreationInformation();
        webCreationInfo.set_title(Viacom.clientGroupsPopup.Constants.clientName);
        webCreationInfo.set_description("New web for " + Viacom.clientGroupsPopup.Constants.clientName);
        webCreationInfo.set_language(1033);
        var url = Viacom.clientGroupsPopup.Constants.newSiteUrl.substr(Viacom.clientGroupsPopup.Constants.newSiteUrl.lastIndexOf("/") + 1, Viacom.clientGroupsPopup.Constants.newSiteUrl.length);
        webCreationInfo.set_url(url);
        webCreationInfo.set_useSamePermissionsAsParentSite(true);
        webCreationInfo.set_webTemplate(Viacom.clientGroupsPopup.Constants.customTemplateNm);
        var oNewWebsite = collWeb.add(webCreationInfo);
        clientContext.executeQueryAsync(function () {
            //alert("Created Web site.");
            $('#lblStatus').html('Client web created.');
            Viacom.clientGroupsPopup.GetDataSiteGrp();
        }, function (sender, args) {
            alert(args.get_message());
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        })
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
    }
};

Viacom.clientGroupsPopup.GetWebTemplates = function (siteurl) {
    try {
        var context = new SP.ClientContext(siteurl);
        var web = context.get_web();
        var templateCollection = web.getAvailableWebTemplates(1033, false);
        context.load(templateCollection);
        context.executeQueryAsync(function () {
            var siteTemplatesEnum = templateCollection.getEnumerator();
            while (siteTemplatesEnum.moveNext()) {
                var siteTemplate = siteTemplatesEnum.get_current();
                if (siteTemplate.get_title().toLowerCase() == Viacom.clientGroupsPopup.Constants.templateTitle.toLowerCase()) {
                    $('#lblStatus').html('Client web template found.');
                    //alert(siteTemplate.get_name());
                    Viacom.clientGroupsPopup.Constants.customTemplateNm = siteTemplate.get_name();
                    Viacom.clientGroupsPopup.createWebsite("sites/clients");
                    break;
                }
            }
        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace())
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        });
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
    }
};