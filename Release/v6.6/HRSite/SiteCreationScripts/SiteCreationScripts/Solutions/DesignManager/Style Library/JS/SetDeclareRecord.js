var Viacom = Viacom || {};
Viacom.AdSalesCustomAction = {};
Viacom.AdSalesCustomAction.Constants = function () {
    return {
        termNm: null,
        totalItem: 0,
        arrItemId: [],
        itemCounter: 0,
        Created: null,
        Author: null,
        Modified: null,
        Editor: null,
        itemChk: null,
        confirmClosingDate: 'false',
        IsPresentDocManagerField: "No",
        DialogBoxTitle: null,
        documentSetHtml: null,
        listID: null
    }
};
Viacom.AdSalesCustomAction.execDecRecord = function (listID, itemID) {
    Viacom.AdSalesCustomAction.Constants.termNm = null;
    Viacom.AdSalesCustomAction.Constants.itemChk = "OneItem";
    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
        Viacom.AdSalesCustomAction.decRecImplementation(listID, itemID);
    });
};


Viacom.AdSalesCustomAction.execMultiDecRecord = function (listID, itemID) {
    if (itemID == "null" && Viacom.AdSalesCustomAction.getQueryStringValue('FolderCTID').startsWith('0x0120D520')) {
        itemID = Viacom.AdSalesCustomAction.getQueryStringValue('ID');
    }
    Viacom.AdSalesCustomAction.Constants.itemChk = "MultiItem";

    Viacom.AdSalesCustomAction.Constants.termNm = null;
    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
        Viacom.AdSalesCustomAction.decRecImplementation(listID, itemID);
    });
};

Viacom.AdSalesCustomAction.getQueryStringValue = function (key) {
    return unescape(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + escape(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}

Viacom.AdSalesCustomAction.decRecImplementation = function (listID, itemID) {
    confirmClosingDate = 'false';
    SP.SOD.executeFunc('SP.Taxonomy.js', 'SP.ClientContext', function () {
        Viacom.AdSalesValidateBusinessRecords(listID, itemID)
        //Viacom.AdSalesCustomAction.UpdateListItems(listID, itemID);
    });
};

Viacom.AdSalesValidateBusinessRecords = function (listID, itemID) {
    Viacom.AdSalesCustomAction.Constants.arrItemId = itemID.split(',');
    Viacom.AdSalesCheckForBusinessRecord(Viacom.AdSalesCustomAction.Constants.arrItemId, listID, itemID);
};
Viacom.AdSalesCustomAction.UpdateListItems = function (listID, id) {
    SP.UI.ModalDialog.showWaitScreenWithNoClose('Please wait...');
    Viacom.AdSalesCustomAction.Constants.itemCounter = 0;
    if (id.indexOf(',') >= 0) {
        Viacom.AdSalesCustomAction.Constants.totalItem = id.split(',').length;
        Viacom.AdSalesCustomAction.Constants.arrItemId = id.split(',');
    }
    else {
        Viacom.AdSalesCustomAction.Constants.totalItem = 1;
        Viacom.AdSalesCustomAction.Constants.arrItemId = [id];
    }
    Viacom.AdSalesCustomAction.getTaxonomyTermByName('Business Record', 'Lifecycle State', 'Viacom Enterprise', listID, Viacom.AdSalesCustomAction.Constants.arrItemId[Viacom.AdSalesCustomAction.Constants.itemCounter]);
};

Viacom.AdSalesCustomAction.getTaxonomyTermByName = function (termName, termSetName, groupName, listID, id) {
    var taxonomyContext = new SP.ClientContext.get_current();
    //var session = SP.Taxonomy.TaxonomySession.getTaxonomySession(taxonomyContext);
    var session = SP.Taxonomy.TaxonomySession.getTaxonomySession(taxonomyContext);
    var termStore = session.getDefaultSiteCollectionTermStore();
    //var store = session.get_termStores().getByName(mmsName);
    var group = termStore.get_groups().getByName(groupName);
    if (termName != null && termName != undefined) {
        var networkSet = group.get_termSets().getByName(termSetName);
        Viacom.AdSalesCustomAction.Constants.termNm = networkSet.get_terms().getByName(termName);
        if (Viacom.AdSalesCustomAction.Constants.termNm != null && Viacom.AdSalesCustomAction.Constants.termNm != undefined) {
            taxonomyContext.load(Viacom.AdSalesCustomAction.Constants.termNm);
            taxonomyContext.executeQueryAsync(function () {


                if (Viacom.AdSalesCustomAction.Constants.itemChk == "OneItem") {
                    Viacom.AdSalesCustomAction.UpdateItems(listID, id);
                }
                else {
                    listID = listID.replace("{", '');
                    listID = listID.replace("}", '');
                    Viacom.AdSalesCustomAction.UpdateMultiItems(listID, id)


                }
            }, function (sender, args) {
                SP.UI.ModalDialog.commonModalDialogClose();
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            });

        }
    }
};

Viacom.AdSalesCustomAction.UpdateMultiItems = function (listID, id) {
    if (Viacom.AdSalesCustomAction.Constants.itemCounter < Viacom.AdSalesCustomAction.Constants.totalItem) {
        Viacom.AdSalesCustomAction.Constants.itemCounter++;
        Viacom.AdSalesCustomAction.UpdateItems(listID, Viacom.AdSalesCustomAction.Constants.arrItemId[Viacom.AdSalesCustomAction.Constants.itemCounter - 1]);
    }
    else {
        SP.UI.ModalDialog.commonModalDialogClose();
        if (confirmClosingDate == 'true') {
            Viacom.AdSalesCustomAction.openDialog();
        }
        else {
            SP.UI.ModalDialog.RefreshPage(SP.UI.DialogResult.OK);
        }
    }
};

Viacom.AdSalesCustomAction.checkFieldName = function (oUpdateList) {

}

Viacom.AdSalesCustomAction.singleEnable = function () {

    var items = SP.ListOperation.Selection.getSelectedItems();
    var ci = CountDictionary(items);
    if (ci >= 1 && Viacom.AdSalesCustomAction.getQueryStringValue('FolderCTID').startsWith('0x0120D52000B7658193EC5C4E6F908523E8CAF50974')) {
        return false;
    }
    else {
        return true;
    }
};


Viacom.AdSalesCustomAction.UpdateItems = function (listID, id) {
    var oListContext = new SP.ClientContext.get_current();
    var oList = oListContext.get_web().get_lists().getById(listID);
    this.oFields = oList.get_fields();
    this.oListItem = oList.getItemById(id);
    this.contentType = this.oListItem.get_contentType();
    var listContentTypes = oList.get_contentTypes();
    oListContext.load(this.contentType);

    this.file = this.oListItem.get_file();

    oListContext.load(this.file)

    //oListContext.load(this.file.checkOutType)

    oListContext.load(oList, 'Title', 'Id', 'RootFolder'); //rootfolder property added//DOCSOURCELIBRARY updated with rootfolder name not title
    oListContext.load(listContentTypes);
    oListContext.load(this.oFields);
    oListContext.load(this.oListItem);


    oListContext.executeQueryAsync(Function.createDelegate(this,
        function () {
            if (oList.get_title() == "Uncategorized") {
                SP.UI.ModalDialog.commonModalDialogClose();
                alert("Uncategorized document can not set to Business Record");
            }
            else {

                var fieldEnumerator = this.oFields.getEnumerator();
                while (fieldEnumerator.moveNext()) {
                    var oField = fieldEnumerator.get_current();
                    if (oField.get_internalName() == "DocManager") {
                        Viacom.AdSalesCustomAction.Constants.IsPresentDocManagerField = "Yes";
                    }

                }
                if (!this.contentType.get_id().toString().startsWith("0x0120D520")) {
                    if (this.file.get_checkOutType() == SP.CheckOutType.none) {
                        this.file.checkOut();
                    }
                }

                var oItemContext = new SP.ClientContext.get_current();
                var oUpdateList = oItemContext.get_web().get_lists().getById(listID);
                var oUpdateItem = oUpdateList.getItemById(id);
                var lcid = _spPageContextInfo.currentLanguage;
                var field = oUpdateList.get_fields().getByInternalNameOrTitle("LifecycleState");
                var taxfield = oItemContext.castTo(field, SP.Taxonomy.TaxonomyField);
                taxfield.setFieldValueByTerm(oUpdateItem, Viacom.AdSalesCustomAction.Constants.termNm, lcid);

                oUpdateItem.set_item('DocCreated', this.oListItem.get_item('Created'));
                oUpdateItem.set_item('DocCreatedBy', this.oListItem.get_item('Author'));
                oUpdateItem.set_item('DocModified', this.oListItem.get_item('Modified'));
                oUpdateItem.set_item('DocModifiedBy', this.oListItem.get_item('Editor'));

                if (this.contentType.get_id().toString().startsWith("0x0120D520")) {
                    oUpdateItem.set_item('CaseName', this.oListItem.get_item('FileLeafRef'));
                    if (this.oListItem.get_item('ClosedDate') == null) {
                        confirmClosingDate = 'true';
                        var setClosedDate = new Date();
                        setClosedDate.setHours(0);
                        setClosedDate.setMinutes(0);
                        setClosedDate.setSeconds(0);
                        setClosedDate.setDate(setClosedDate.getDate());
                        oUpdateItem.set_item('ClosedDate', setClosedDate);
                        //Viacom.AdSalesCustomAction.openDialog();
                    }
                }

                var ctid = this.oListItem.get_item("ContentTypeId").toString();

                var ct_enumerator = listContentTypes.getEnumerator();
                while (ct_enumerator.moveNext()) {
                    var ct = ct_enumerator.get_current();

                    if (ct.get_id().toString() == ctid) {
                        //we've got our content type, now let's get its name
                        var contentTypeName = ct.get_name();
                        oUpdateItem.set_item('DocContentType', contentTypeName);
                    }
                }


                var today = new Date();
                var numberOfDaysToAdd = -1;
                today.setDate(today.getDate() + numberOfDaysToAdd);

                var itemlistname = oList.get_rootFolder().get_name();//get_title(); //DOCSOURCELIBRARY updated with rootfolder name not title 
                oUpdateItem.set_item('DocSourceLibrary', itemlistname); //rootfolder

                //var itemfoldername = '';
                //if (Viacom.AdSalesCustomAction.Constants.IsPresentDocManagerField == "Yes") {
                //  oUpdateItem.set_item('DocManager', itemfoldername);
                //}
                oUpdateItem.set_item('RecordDeclarationDate', today);
                oUpdateItem.update();
                if (!this.contentType.get_id().toString().startsWith("0x012")) {
                    var Fileval = oUpdateItem.get_file();
                    Fileval.checkIn();
                    oItemContext.load(Fileval);
                }
                oItemContext.load(oUpdateItem);

                oItemContext.executeQueryAsync(AddListItemSucceeded, AddListItemFailed);
                function AddListItemSucceeded() {
                    if (Viacom.AdSalesCustomAction.Constants.itemChk == "MultiItem") {
                        Viacom.AdSalesCustomAction.UpdateMultiItems(listID, id);
                    }
                    else {
                        SP.UI.ModalDialog.commonModalDialogClose();
                        if (confirmClosingDate == 'true') {
                            Viacom.AdSalesCustomAction.openDialog();
                        }
                        else {
                            SP.UI.ModalDialog.RefreshPage(SP.UI.DialogResult.OK);
                        }
                    }
                }

                function AddListItemFailed(sender, args) {
                    SP.UI.ModalDialog.commonModalDialogClose();
                    alert('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                }

            }
        }),
                function (sender, args) {
                    SP.UI.ModalDialog.commonModalDialogClose();
                    alert('Error occured' + args.get_message());
                });
};

Viacom.AdSalesCheckForBusinessRecord = function (arrItemIDs, listID, itemID) {
    var flag = true;
    if (arrItemIDs.length > 0) {
        var oListContext = new SP.ClientContext.get_current();
        var oList = oListContext.get_web().get_lists().getById(listID);
        var arrListItems = new Array();

        for (var i = 0; i < arrItemIDs.length; i++) {
            arrListItems[i] = oList.getItemById(arrItemIDs[i]);
            oListContext.load(arrListItems[i]);
        }

        oListContext.executeQueryAsync(function () {
            for (var i = 0; i < arrItemIDs.length; i++) {
                var lifeStateLabel = arrListItems[i].get_item('LifecycleState').Label;
                if (lifeStateLabel == null) {
                    lifeStateLabel = arrListItems[i].get_item('LifecycleState').get_label();
                }
                if (lifeStateLabel == 'Business Record') {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                //process further 
                Viacom.AdSalesCustomAction.UpdateListItems(listID, itemID);
            }
            else {
                alert("Some of the records are already marked as Business Records.\nPlease unselect them to proceed.");
            }
        },
			function (sender, args) {
			    SP.UI.ModalDialog.commonModalDialogClose();
			    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
			}
		);
    }
};


Viacom.AdSalesCustomAction.openDialog = function () {
    Viacom.AdSalesCustomAction.Constants();
    Viacom.AdSalesCustomAction.Constants.DialogBoxTitle = "Confirmation";
    documentSetHtml = "<div id='AdSalesCustomAction'><span>Please update closed date, if applicable.</span>&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/><input type='button' value='OK' onclick='Viacom.AdSalesCustomAction.OK()' />&nbsp;&nbsp;</div>";
    $('body').append(documentSetHtml);
    try {

        var diaOptions = SP.UI.$create_DialogOptions();
        diaOptions.html = document.getElementById('AdSalesCustomAction');
        diaOptions.width = 300;
        diaOptions.height = 100;
        diaOptions.showClose = true;
        diaOptions.allowMaximize = false;
        diaOptions.title = Viacom.AdSalesCustomAction.Constants.DialogBoxTitle;
        diaOptions.dialogReturnValueCallback = Function.createDelegate(null, Viacom.AdSalesCustomAction.CloseCallBack);
        SP.UI.ModalDialog.commonModalDialogClose();
        SP.UI.ModalDialog.showModalDialog(diaOptions);
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error while opening popup For DocumentSet");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.AdSalesCustomAction.OK = function () {
    try {
        SP.UI.ModalDialog.commonModalDialogClose();
        SP.UI.ModalDialog.RefreshPage(SP.UI.DialogResult.OK);
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        console.log('Request failed. ' + e.message);
    }
}

Viacom.AdSalesCustomAction.Cancel = function () {
    try {
        SP.UI.ModalDialog.commonModalDialogClose();
        SP.UI.ModalDialog.RefreshPage(SP.UI.DialogResult.OK);
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        console.log('Request failed. ' + e.message);
    }
}

Viacom.AdSalesCustomAction.CloseCallBack = function (result, returnValue) {
    SP.UI.ModalDialog.RefreshPage(SP.UI.DialogResult.OK);
};

