﻿var Viacom = Viacom || {};
Viacom.Mapping = {};
Viacom.editClientAgencyMapping = Viacom.editClientAgencyMapping || {};
Viacom.editClientSiteTaxonomy = Viacom.editClientSiteTaxonomy || {};
Viacom.Mapping.Constants = function () {
    return {
        objTaxClient: {},
        agencyTypeCount: null
    }
};

Viacom.Mapping.Initialize = function () {
    Viacom.Mapping.Constants.objTaxClient = {};
    Viacom.Mapping.Constants.objClientPopup = {};
    Viacom.Mapping.Constants.mmsName = "Managed Metadata Service";
    Viacom.Mapping.Constants.objTaxClient.taxClientCategoryValue = [];
    Viacom.Mapping.Constants.objTaxClient.taxClientNameValue = [];
    Viacom.Mapping.Constants.objTaxClient.taxClientSegmentValue = [];
    Viacom.Mapping.Constants.objTaxClient.taxClientName = [];
    Viacom.Mapping.Constants.AgencySiteUrl = null;
    Viacom.Mapping.Constants.objTaxClient.allAgencyType = [[]];
    Viacom.Mapping.Constants.agencyTypeCount = null;

    Viacom.Mapping.Constants.ClientSiteUrl = null;
    Viacom.Mapping.Constants.clientNameMsg = null;
    Viacom.Mapping.Constants.agencyNameMsg = null;
    Viacom.Mapping.Constants.agencyTypeMsg = null;
    Viacom.Mapping.Constants.agencyTypeConfirmationCount = null;

    Viacom.Mapping.Constants.AgencyDataListAgencyName = [];
    Viacom.Mapping.Constants.AgencyDataListAgencyID = [];
    Viacom.Mapping.Constants.AgencyDataListAgencyURL = [];
    Viacom.Mapping.Constants.AgencyDataListSVPTeamName = [];
    Viacom.Mapping.Constants.AgencyDataListCount = 0;

    Viacom.Mapping.Constants.SVPDataListCount = 0;
    Viacom.Mapping.Constants.SVPDataListSVPTeamRegion = [];
    Viacom.Mapping.Constants.SVPDataListSVPTeamType = [];
    Viacom.Mapping.Constants.SVPDataListSVPSiteUrl = [];

    Viacom.Mapping.Constants.SiteMappingListCount = 0;
    Viacom.Mapping.Constants.SiteMappingClientName = [];
    Viacom.Mapping.Constants.SiteMappingAgencyTypeGUID = [];
    Viacom.Mapping.Constants.SiteMappingClientCategory = [];
    Viacom.Mapping.Constants.SiteMappingClientSegment = [];
    Viacom.Mapping.Constants.SiteMappingClientSiteUrl = [];
    Viacom.Mapping.Constants.SiteMappingRMDClientID = [];
    Viacom.Mapping.Constants.SiteMappingGabrielClientID = [];
    Viacom.Mapping.Constants.SiteMappingWideorbitClientID = [];
    Viacom.Mapping.Constants.SiteMappingAdfrontClientID = [];
    Viacom.Mapping.Constants.SiteMappingClientDataFlag = "True";


};


Viacom.editClientAgencyMapping.getTaxWebTaggingValue = function () {
    try {
        var taxClientClientName3 = $("span[id*='EditClientNameNm1']")[0].children[0];
        if (taxClientClientName3.hasAttribute('value')) {
            Viacom.Mapping.Constants.objTaxClient.taxClientNameValue.push(taxClientClientName3.getAttribute('value').split('|'));
        }

        var taxClientAgencyName1 = $("span[id*='EditclientAgencyNm1']")[0].children[0];
        if (taxClientAgencyName1.hasAttribute('value')) {
            var taxClientCategoryValue = taxClientAgencyName1.getAttribute('value').split('|');
            Viacom.Mapping.Constants.objTaxClient.taxClientCategoryValue = taxClientAgencyName1.getAttribute('value').split('|');

        }
        var taxClientAgencyName2 = $("span[id*='EditclientAgencyNm2']")[0].children[0];
        if (taxClientAgencyName2.hasAttribute('value')) {
            Viacom.Mapping.Constants.objTaxClient.taxClientCategoryValue = taxClientAgencyName2.getAttribute('value').split('|');
        }
        var taxClientAgencyName3 = $("span[id*='EditclientAgencyNm3']")[0].children[0];
        if (taxClientAgencyName3.hasAttribute('value')) {
            Viacom.Mapping.Constants.objTaxClient.taxClientCategoryValue = taxClientAgencyName3.getAttribute('value').split('|');
        }
        var agencyTypeContext = new SP.ClientContext.get_current();
        //  var session = SP.Taxonomy.TaxonomySession.getTaxonomySession(agencyTypeContext);
        // var termStores = session.get_termStores();
        //var termStore = termStores.getByName(Viacom.Mapping.Constants.mmsName);
        var session = SP.Taxonomy.TaxonomySession.getTaxonomySession(agencyTypeContext);
        var termStore = session.getDefaultSiteCollectionTermStore();
        var group = termStore.get_groups().getByName('M＆E Ad Sales');
        var termSet = group.get_termSets().getByName('Agency Types');
        var terms = termSet.getAllTerms();
        agencyTypeContext.load(terms);
        agencyTypeContext.executeQueryAsync(function () {
            var agTyEnumerator = terms.getEnumerator();
            var termAgTypeArr = [[]];
            termAgTypeArr.length = 0;
            while (agTyEnumerator.moveNext()) {
                var currentTerm = agTyEnumerator.get_current();
                var termName = currentTerm.get_name();
                var termGuid = currentTerm.get_id()._m_guidString$p$0;
                var arr = [];
                arr.length = 0;
                arr[0] = termName;
                arr[1] = termGuid;
                termAgTypeArr.push(arr);
            }
            var allAgencytype = $("span[id*='EditAgencyType']");
            var k = 0;
            for (var i = 0; i < allAgencytype.length; i++) {
                var id = $("span[id*='EditAgencyType']")[i].getAttribute('id');
                var agencyTypeValue = $("span[id*='EditAgencyType']")[0].getAttribute('value');
                var no = id.replace("EditAgencyType", "");
                var taxid = "EditclientAgencyNm" + no;
                var spanHtml = "span[id*='" + taxid + "']";
                var spanText = null;
                var spanID = null;
                for (var m = 0; m < termAgTypeArr.length; m++) {
                    var typeId = '#' + id;
                    var typeValue = $(typeId).attr('value').toLowerCase();
                    if (taxid == "EditclientAgencyNm1") {
                        if (termAgTypeArr[m][0].indexOf('Linear') >= 0) {
                            spanText = termAgTypeArr[m][0];
                            spanID = termAgTypeArr[m][1];
                            termAgTypeArr.splice(m, 1);
                            break;
                        }
                    }

                    else if (termAgTypeArr[m][0].toLowerCase().indexOf(typeValue) >= 0 && taxid != "EditclientAgencyNm1") {
                        spanText = termAgTypeArr[m][0];
                        spanID = termAgTypeArr[m][1];
                        termAgTypeArr.splice(m, 1);
                        break;
                    }
                    else if (termAgTypeArr[m][0].toLowerCase().indexOf(typeValue) >= 0 && taxid != "EditclientAgencyNm1") {
                        spanText = termAgTypeArr[m][0];
                        spanID = termAgTypeArr[m][1];
                        termAgTypeArr.splice(m, 1);
                        break;
                    }
                }
                var spanId = '#' + id;
                $(spanId).attr('value', spanText);
                var taxClientAgencyType = $(spanHtml)[0].children[0];
                if (taxClientAgencyType.hasAttribute('value')) {
                    Viacom.Mapping.Constants.objTaxClient.allAgencyType[k] = taxClientAgencyType.getAttribute('value').split('|');
                    Viacom.Mapping.Constants.objTaxClient.allAgencyType[k].push(spanText);
                    Viacom.Mapping.Constants.objTaxClient.allAgencyType[k].push(spanID);
                    k++;
                }
            }
            Viacom.editClientAgencyMapping.GetItemRow("Site Mapping");
        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        });
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
    }
};

Viacom.editClientAgencyMapping.ValidateFields = function () {
    try {
        var clientNameValue = Viacom.Mapping.Constants.objTaxClient.taxClientNameValue.length;
        var clientCatValue = Viacom.Mapping.Constants.objTaxClient.taxClientCategoryValue.length;
        if (clientNameValue > 0 && clientCatValue > 0) {
            return true;
        }
        else {
            return false;
        }
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
    }
};

Viacom.editClientAgencyMapping.chkEditClientTerm = function (termName, termSetName, groupName, mmsName) {
    var termExist = false;
    try {
        Viacom.Mapping.Initialize();
        Viacom.editClientAgencyMapping.getTaxWebTaggingValue();

    }
    catch (e) {
        console.log('Request failed. ' + e.message);
    }
};


Viacom.editClientAgencyMapping.GetItemRow = function (lstName) {
    try {
        var IsValidate = Viacom.editClientAgencyMapping.ValidateFields();
        if (IsValidate) {
            Viacom.Mapping.Constants.agencyTypeConfirmationCount = 0;
            if (Viacom.Mapping.Constants.agencyTypeConfirmationCount <= Viacom.Mapping.Constants.objTaxClient.allAgencyType.length - 1) {
                var ClientNameAgencyTypeQuery = null;
                var AgencyNamequery = null;
                Viacom.Mapping.Constants.agencyTypeConfirmationCount++;
                var m = Viacom.Mapping.Constants.agencyTypeConfirmationCount - 1;
                ClientNameAgencyTypeQuery = "<View><Query><Where><And><Contains><FieldRef Name='ClientName'/><Value Type='TaxonomyFieldType'>" + Viacom.Mapping.Constants.objTaxClient.taxClientNameValue[0][0] + "</Value></Contains><Contains><FieldRef Name='AgencyType'/><Value Type='TaxonomyFieldType'>" + Viacom.Mapping.Constants.objTaxClient.allAgencyType[m][2] + "</Value></Contains></And></Where></Query></View>";
                Viacom.editClientAgencyMapping.GetConfirmationMassage("Site Mapping", ClientNameAgencyTypeQuery);
            }
        }
        else {
            alert("Please fill all the values");
        }
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
    }
};

Viacom.editClientAgencyMapping.GetConfirmationMassage = function (lstName, ClientNameAgencyTypeQuery) {
    try {
        var context = new SP.ClientContext.get_current();
        var web = context.get_web();
        var list = web.get_lists().getByTitle(lstName);

        var ClientNameAgencyTypeCaml = new SP.CamlQuery();
        ClientNameAgencyTypeCaml.set_viewXml(ClientNameAgencyTypeQuery);
        var ClientNameItems = list.getItems(ClientNameAgencyTypeCaml);


        context.load(ClientNameItems);
        context.executeQueryAsync(function () {

            var enumeratorClientNameType = ClientNameItems.getEnumerator();
            while (enumeratorClientNameType.moveNext()) {
                var ClientNameTypeItem = enumeratorClientNameType.get_current();
                //oListItem.set_item('ClientName', Viacom.Mapping.Constants.objTaxClient.taxClientNameValue[0][1])
                Viacom.Mapping.Constants.clientNameMsg = ClientNameTypeItem.get_item('ClientName').$0_1;
                if (ClientNameTypeItem.get_item('ClientName').$0_1.toString().toLowerCase() == Viacom.Mapping.Constants.objTaxClient.taxClientNameValue[0][0].toString().toLowerCase()) {
                    if (Viacom.Mapping.Constants.agencyNameMsg != null) {
                        Viacom.Mapping.Constants.agencyNameMsg = Viacom.Mapping.Constants.agencyNameMsg + "," + ClientNameTypeItem.get_item('AgencyName').$0_1;

                    }
                    else {
                        Viacom.Mapping.Constants.agencyNameMsg = ClientNameTypeItem.get_item('AgencyName').$0_1;
                    }
                    if (Viacom.Mapping.Constants.agencyTypeMsg != null) {
                        Viacom.Mapping.Constants.agencyTypeMsg = Viacom.Mapping.Constants.agencyTypeMsg + "," + ClientNameTypeItem.get_item('AgencyType').$0_1;

                    }
                    else {
                        Viacom.Mapping.Constants.agencyTypeMsg = ClientNameTypeItem.get_item('AgencyType').$0_1;
                    }
                    if (Viacom.Mapping.Constants.AgencySiteUrl != null) {
                        Viacom.Mapping.Constants.AgencySiteUrl = Viacom.Mapping.Constants.AgencySiteUrl + "," + ClientNameTypeItem.get_item('AgencyURL').$2_1;

                    }
                    else {
                        Viacom.Mapping.Constants.AgencySiteUrl = ClientNameTypeItem.get_item('AgencyURL').$2_1;
                    }

                    Viacom.Mapping.Constants.SiteMappingClientName.push([ClientNameTypeItem.get_item('ClientName').$0_1, ClientNameTypeItem.get_item('ClientName').$1_1]);
                    Viacom.Mapping.Constants.SiteMappingClientCategory.push([ClientNameTypeItem.get_item('ClientCategory').$0_1, ClientNameTypeItem.get_item('ClientCategory').$1_1]);
                    Viacom.Mapping.Constants.SiteMappingClientSegment.push([ClientNameTypeItem.get_item('ClientSegment').$0_1, ClientNameTypeItem.get_item('ClientSegment').$1_1]);
                    Viacom.Mapping.Constants.SiteMappingClientSiteUrl.push(ClientNameTypeItem.get_item('ClientSiteUrl').$2_1);
                    Viacom.Mapping.Constants.SiteMappingRMDClientID.push(ClientNameTypeItem.get_item('RDMClientID'));
                    Viacom.Mapping.Constants.SiteMappingGabrielClientID.push(ClientNameTypeItem.get_item('GabrielClientID'));
                    Viacom.Mapping.Constants.SiteMappingWideorbitClientID.push(ClientNameTypeItem.get_item('WideOrbitClientID'));
                    Viacom.Mapping.Constants.SiteMappingAdfrontClientID.push(ClientNameTypeItem.get_item('AdFrontClientID'));
                }
            }
            if (Viacom.Mapping.Constants.agencyTypeConfirmationCount <= Viacom.Mapping.Constants.objTaxClient.allAgencyType.length - 1) {
                var ClientNameAgencyTypeQuery = null;
                var AgencyNamequery = null;
                Viacom.Mapping.Constants.agencyTypeConfirmationCount++;
                var m = Viacom.Mapping.Constants.agencyTypeConfirmationCount - 1;
                ClientNameAgencyTypeQuery = "<View><Query><Where><And><Contains><FieldRef Name='ClientName'/><Value Type='TaxonomyFieldType'>" + Viacom.Mapping.Constants.objTaxClient.taxClientNameValue[0][0] + "</Value></Contains><Contains><FieldRef Name='AgencyType'/><Value Type='TaxonomyFieldType'>" + Viacom.Mapping.Constants.objTaxClient.allAgencyType[m][2] + "</Value></Contains></And></Where></Query></View>";
                Viacom.editClientAgencyMapping.GetConfirmationMassage("Site Mapping", ClientNameAgencyTypeQuery);
            }
            else {
                var messageboxstring = Viacom.Mapping.Constants.clientNameMsg + " is associated with " + Viacom.Mapping.Constants.agencyNameMsg + " with given " + Viacom.Mapping.Constants.agencyTypeMsg +
				". Are you sure you want to change existing association?";
                var boolResult = Viacom.editClientAgencyMapping.ConfirmMessagebox(messageboxstring);
                if (boolResult) {
                    if (Viacom.Mapping.Constants.AgencyDataListCount < Viacom.Mapping.Constants.objTaxClient.allAgencyType.length) {
                        AgencyDataListQuery = "<View><Query><Where><Contains><FieldRef Name='AgencyName'/><Value Type='TaxonomyFieldType'>" + Viacom.Mapping.Constants.objTaxClient.allAgencyType[Viacom.Mapping.Constants.AgencyDataListCount][0] + "</Value></Contains></Where></Query></View>";
                        Viacom.Mapping.Constants.AgencyDataListCount++;
                        Viacom.editClientAgencyMapping.GetAgencyListData("Agency Data", AgencyDataListQuery);
                    }
                }
            }

        },
            function (sender, args) {
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            }

        );
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
    }
}

Viacom.editClientAgencyMapping.GetAgencyListData = function (lstName, AgencyDataListQuery) {
    try {
        var context = new SP.ClientContext.get_current();
        var web = context.get_web();
        var list = web.get_lists().getByTitle(lstName);

        var AgencyDataCaml = new SP.CamlQuery();
        AgencyDataCaml.set_viewXml(AgencyDataListQuery);
        var AgencyDataListInfo = list.getItems(AgencyDataCaml);


        context.load(AgencyDataListInfo);
        context.executeQueryAsync(function () {

            var enumeratorAgencyDataInfo = AgencyDataListInfo.getEnumerator();
            while (enumeratorAgencyDataInfo.moveNext()) {
                var AgencyDataListItem = enumeratorAgencyDataInfo.get_current();
                if (AgencyDataListItem.get_item('AgencyName').$0_1.toString().toLowerCase() == Viacom.Mapping.Constants.objTaxClient.allAgencyType[Viacom.Mapping.Constants.AgencyDataListCount - 1][0].toString().toLowerCase()) {
                    Viacom.Mapping.Constants.AgencyDataListAgencyName.push([AgencyDataListItem.get_item('AgencyName').$0_1, AgencyDataListItem.get_item('AgencyName').$1_1]);
                    if (AgencyDataListItem.get_item('AgencyID') != null) {
                        Viacom.Mapping.Constants.AgencyDataListAgencyID.push([AgencyDataListItem.get_item('AgencyID').$0_1, AgencyDataListItem.get_item('AgencyID').$1_1]);
                    }
                    else {
                        Viacom.Mapping.Constants.AgencyDataListAgencyID.push(['', '']);
                    }
                    Viacom.Mapping.Constants.AgencyDataListAgencyURL.push(AgencyDataListItem.get_item('AgencyUrl'));
                    Viacom.Mapping.Constants.AgencyDataListSVPTeamName.push([AgencyDataListItem.get_item('TeamName').$0_1, AgencyDataListItem.get_item('TeamName').$1_1]);
                }
            }
            if (Viacom.Mapping.Constants.AgencyDataListCount < Viacom.Mapping.Constants.objTaxClient.allAgencyType.length) {
                AgencyDataListQuery = "<View><Query><Where><Contains><FieldRef Name='AgencyName'/><Value Type='TaxonomyFieldType'>" + Viacom.Mapping.Constants.objTaxClient.allAgencyType[Viacom.Mapping.Constants.AgencyDataListCount][0] + "</Value></Contains></Where></Query></View>";
                Viacom.Mapping.Constants.AgencyDataListCount++;
                Viacom.editClientAgencyMapping.GetAgencyListData("Agency Data", AgencyDataListQuery);
            }
            else {
                if (Viacom.Mapping.Constants.SVPDataListCount < Viacom.Mapping.Constants.AgencyDataListSVPTeamName.length) {
                    SVPDataListQuery = "<View><Query><Where><Contains><FieldRef Name='TeamName'/><Value Type='TaxonomyFieldType'>" + Viacom.Mapping.Constants.AgencyDataListSVPTeamName[Viacom.Mapping.Constants.SVPDataListCount][0] + "</Value></Contains></Where></Query></View>";
                    Viacom.Mapping.Constants.SVPDataListCount++;
                    Viacom.editClientAgencyMapping.GetSVPListData("SVP Data", SVPDataListQuery);
                }
            }
        },
            function (sender, args) {
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            }

        );
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
    }
}

Viacom.editClientAgencyMapping.GetSVPListData = function (lstName, SVPListQuery) {
    try {
        var context = new SP.ClientContext.get_current();
        var web = context.get_web();
        var list = web.get_lists().getByTitle(lstName);

        var SVPDataCaml = new SP.CamlQuery();
        SVPDataCaml.set_viewXml(SVPListQuery);
        var SVPDataListInfo = list.getItems(SVPDataCaml);


        context.load(SVPDataListInfo);
        context.executeQueryAsync(function () {

            var enumeratorSVPListData = SVPDataListInfo.getEnumerator();
            while (enumeratorSVPListData.moveNext()) {
                var SVPListItem = enumeratorSVPListData.get_current();
                if (SVPListItem.get_item('TeamName').$0_1.toString().toLowerCase() == Viacom.Mapping.Constants.AgencyDataListSVPTeamName[Viacom.Mapping.Constants.SVPDataListCount - 1][0].toString().toLowerCase()) {
                    Viacom.Mapping.Constants.SVPDataListSVPTeamRegion.push([SVPListItem.get_item('TeamRegion').$0_1, SVPListItem.get_item('TeamRegion').$1_1]);
                    Viacom.Mapping.Constants.SVPDataListSVPTeamType.push([SVPListItem.get_item('TeamType').$0_1, SVPListItem.get_item('TeamType').$1_1]);
                    Viacom.Mapping.Constants.SVPDataListSVPSiteUrl.push(SVPListItem.get_item('SVPSiteUrl').$2_1);
                }
            }
            if (Viacom.Mapping.Constants.SVPDataListCount < Viacom.Mapping.Constants.AgencyDataListSVPTeamName.length) {
                SVPDataListQuery = "<View><Query><Where><Contains><FieldRef Name='TeamName'/><Value Type='TaxonomyFieldType'>" + Viacom.Mapping.Constants.AgencyDataListSVPTeamName[Viacom.Mapping.Constants.SVPDataListCount][0] + "</Value></Contains></Where></Query></View>";
                Viacom.Mapping.Constants.SVPDataListCount++;
                Viacom.editClientAgencyMapping.GetSVPListData("SVP Data", SVPDataListQuery);
            }
            else {
                if (Viacom.Mapping.Constants.SiteMappingListCount <= Viacom.Mapping.Constants.objTaxClient.allAgencyType.length - 1) {
                    var SiteMappingListQuery = null;
                    // var AgencyNamequery = null;
                    var m = Viacom.Mapping.Constants.SiteMappingListCount;
                    SiteMappingListQuery = "<View><Query><Where><And><Contains><FieldRef Name='ClientName'/><Value Type='TaxonomyFieldType'>" + Viacom.Mapping.Constants.objTaxClient.taxClientNameValue[0][0] + "</Value></Contains><Contains><FieldRef Name='AgencyType'/><Value Type='TaxonomyFieldType'>" + Viacom.Mapping.Constants.objTaxClient.allAgencyType[m][2] + "</Value></Contains></And></Where></Query></View>";
                    // AgencyNamequery = "<View><Query><Where><Contains><FieldRef Name='AgencyName'/><Value Type='TaxonomyFieldType'>" + Viacom.Mapping.Constants.objTaxClient.allAgencyType[m][0] + "</Value></Contains></Where></Query></View>";
                    Viacom.editClientAgencyMapping.UpdateSiteMappingList("Site Mapping", SiteMappingListQuery);
                }
            }
        },
            function (sender, args) {
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            }

        );
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
    }
}

Viacom.editClientAgencyMapping.UpdateSiteMappingList = function (lstName, SiteMappingListQuery) {

    try {
        if (Viacom.Mapping.Constants.SiteMappingListCount < Viacom.Mapping.Constants.objTaxClient.allAgencyType.length) {
            var SiteMappingcontext = new SP.ClientContext.get_current();
            var web = SiteMappingcontext.get_web();
            var list = web.get_lists().getByTitle(lstName);
            var oSiteMappingListQuery = new SP.CamlQuery();
            oSiteMappingListQuery.set_viewXml(SiteMappingListQuery);
            var oSiteMappingItems = list.getItems(oSiteMappingListQuery);
            SiteMappingcontext.load(oSiteMappingItems);
            SiteMappingcontext.load(list);
            SiteMappingcontext.executeQueryAsync(function () {
                if (oSiteMappingItems.get_count() > 0) {

                    var enumeratorSiteMapping = oSiteMappingItems.getEnumerator();
                    while (enumeratorSiteMapping.moveNext()) {
                        var SiteMappingListItem = enumeratorSiteMapping.get_current();
                        if (SiteMappingListItem.get_item('ClientName').$0_1.toString().toLowerCase() == Viacom.Mapping.Constants.objTaxClient.taxClientNameValue[0][0].toString().toLowerCase()) {
                            Viacom.Mapping.Constants.ClientSiteUrl = SiteMappingListItem.get_item('ClientSiteUrl').$2_1;
                            SiteMappingListItem.set_item('AgencyName', Viacom.Mapping.Constants.AgencyDataListAgencyName[Viacom.Mapping.Constants.SiteMappingListCount][1]);
                            SiteMappingListItem.set_item('AgencyID', Viacom.Mapping.Constants.AgencyDataListAgencyID[Viacom.Mapping.Constants.SiteMappingListCount][1]);
                            var AgencyUrl = new SP.FieldUrlValue();
                            AgencyUrl.set_url(Viacom.Mapping.Constants.AgencyDataListAgencyURL[Viacom.Mapping.Constants.SiteMappingListCount]);
                            AgencyUrl.set_description(Viacom.Mapping.Constants.AgencyDataListAgencyURL[Viacom.Mapping.Constants.SiteMappingListCount]);
                            SiteMappingListItem.set_item('AgencyURL', AgencyUrl);
                            SiteMappingListItem.set_item('SVPTeamName', Viacom.Mapping.Constants.AgencyDataListSVPTeamName[Viacom.Mapping.Constants.SiteMappingListCount][1]);
                            SiteMappingListItem.set_item('SVPTeamRegion', Viacom.Mapping.Constants.SVPDataListSVPTeamRegion[Viacom.Mapping.Constants.SiteMappingListCount][1]);
                            SiteMappingListItem.set_item('SVPTeamType', Viacom.Mapping.Constants.SVPDataListSVPTeamType[Viacom.Mapping.Constants.SiteMappingListCount][1]);
                            var SVPSiteUrl = new SP.FieldUrlValue();
                            SVPSiteUrl.set_url(Viacom.Mapping.Constants.SVPDataListSVPSiteUrl[Viacom.Mapping.Constants.SiteMappingListCount]);
                            SVPSiteUrl.set_description(Viacom.Mapping.Constants.SVPDataListSVPSiteUrl[Viacom.Mapping.Constants.SiteMappingListCount]);
                            SiteMappingListItem.set_item('SVPSiteURL', SVPSiteUrl);
                            SiteMappingListItem.update();
                        }
                    }
                }
                else {
                    if (Viacom.Mapping.Constants.SiteMappingClientDataFlag == "True") {
                        var SiteMappingClientDataQuery = "<View><Query><Where><Contains><FieldRef Name='ClientName'/><Value Type='TaxonomyFieldType'>" + Viacom.Mapping.Constants.objTaxClient.taxClientNameValue[0][0] + "</Value></Contains></Where></Query></View>";
                        Viacom.editClientAgencyMapping.GetSiteMappingClientData("Site Mapping", SiteMappingClientDataQuery);
                    }
                    else {
                        var SiteMappingListNewItem = list.addItem();
                        SiteMappingListNewItem.set_item('AgencyName', Viacom.Mapping.Constants.AgencyDataListAgencyName[Viacom.Mapping.Constants.SiteMappingListCount][1]);
                        SiteMappingListNewItem.set_item('AgencyID', Viacom.Mapping.Constants.AgencyDataListAgencyID[Viacom.Mapping.Constants.SiteMappingListCount][1]);
                        SiteMappingListNewItem.set_item('AgencyType', Viacom.Mapping.Constants.objTaxClient.allAgencyType[Viacom.Mapping.Constants.SiteMappingListCount][3]);
                        var AgencyUrl = new SP.FieldUrlValue();
                        AgencyUrl.set_url(Viacom.Mapping.Constants.AgencyDataListAgencyURL[Viacom.Mapping.Constants.SiteMappingListCount]);
                        AgencyUrl.set_description(Viacom.Mapping.Constants.AgencyDataListAgencyURL[Viacom.Mapping.Constants.SiteMappingListCount]);
                        SiteMappingListNewItem.set_item('AgencyURL', AgencyUrl);
                        SiteMappingListNewItem.set_item('SVPTeamName', Viacom.Mapping.Constants.AgencyDataListSVPTeamName[Viacom.Mapping.Constants.SiteMappingListCount][1]);
                        SiteMappingListNewItem.set_item('SVPTeamRegion', Viacom.Mapping.Constants.SVPDataListSVPTeamRegion[Viacom.Mapping.Constants.SiteMappingListCount][1]);
                        SiteMappingListNewItem.set_item('SVPTeamType', Viacom.Mapping.Constants.SVPDataListSVPTeamType[Viacom.Mapping.Constants.SiteMappingListCount][1]);
                        var SVPSiteUrl = new SP.FieldUrlValue();
                        SVPSiteUrl.set_url(Viacom.Mapping.Constants.SVPDataListSVPSiteUrl[Viacom.Mapping.Constants.SiteMappingListCount]);
                        SVPSiteUrl.set_description(Viacom.Mapping.Constants.SVPDataListSVPSiteUrl[Viacom.Mapping.Constants.SiteMappingListCount]);
                        SiteMappingListNewItem.set_item('SVPSiteURL', SVPSiteUrl);
                        SiteMappingListNewItem.set_item('ClientName', Viacom.Mapping.Constants.SiteMappingClientName[Viacom.Mapping.Constants.SiteMappingListCount][1]);
                        SiteMappingListNewItem.set_item('ClientCategory', Viacom.Mapping.Constants.SiteMappingClientCategory[Viacom.Mapping.Constants.SiteMappingListCount][1]);
                        SiteMappingListNewItem.set_item('ClientSegment', Viacom.Mapping.Constants.SiteMappingClientSegment[Viacom.Mapping.Constants.SiteMappingListCount][1]);
                        var ClientUrl = new SP.FieldUrlValue();
                        ClientUrl.set_url(Viacom.Mapping.Constants.SiteMappingClientSiteUrl[Viacom.Mapping.Constants.SiteMappingListCount]);
                        ClientUrl.set_description(Viacom.Mapping.Constants.SiteMappingClientSiteUrl[Viacom.Mapping.Constants.SiteMappingListCount]);
                        SiteMappingListNewItem.set_item('ClientSiteUrl', ClientUrl);
                        SiteMappingListNewItem.set_item('RDMClientID', Viacom.Mapping.Constants.SiteMappingRMDClientID[Viacom.Mapping.Constants.SiteMappingListCount]);
                        SiteMappingListNewItem.set_item('GabrielClientID', Viacom.Mapping.Constants.SiteMappingGabrielClientID[Viacom.Mapping.Constants.SiteMappingListCount]);
                        SiteMappingListNewItem.set_item('WideOrbitClientID', Viacom.Mapping.Constants.SiteMappingWideorbitClientID[Viacom.Mapping.Constants.SiteMappingListCount]);
                        SiteMappingListNewItem.set_item('AdFrontClientID', Viacom.Mapping.Constants.SiteMappingAdfrontClientID[Viacom.Mapping.Constants.SiteMappingListCount]);
                        SiteMappingListNewItem.update();
                        Viacom.Mapping.Constants.SiteMappingClientDataFlag = "False";
                    }
                }
                SiteMappingcontext.executeQueryAsync(function () {
                    if (Viacom.Mapping.Constants.SiteMappingListCount < Viacom.Mapping.Constants.objTaxClient.allAgencyType.length - 1) {
                        Viacom.Mapping.Constants.SiteMappingListCount++;
                        var SiteMappingListQuery = null;
                        var m = Viacom.Mapping.Constants.SiteMappingListCount;
                        SiteMappingListQuery = "<View><Query><Where><And><Contains><FieldRef Name='ClientName'/><Value Type='TaxonomyFieldType'>" + Viacom.Mapping.Constants.objTaxClient.taxClientNameValue[0][0] + "</Value></Contains><Contains><FieldRef Name='AgencyType'/><Value Type='TaxonomyFieldType'>" + Viacom.Mapping.Constants.objTaxClient.allAgencyType[m][2] + "</Value></Contains></And></Where></Query></View>";
                        Viacom.editClientAgencyMapping.UpdateSiteMappingList("Site Mapping", SiteMappingListQuery);
                    }
                    else {
                        Viacom.editClientAgencyMapping.openDialog(Viacom.Mapping.Constants.ClientSiteUrl);
                    }
                },
			function (sender, args) {
			    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
			});
            },
                function (sender, args) {
                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                }

            );
        }
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
    }
};

Viacom.editClientAgencyMapping.GetSiteMappingClientData = function (lstName, SiteMappingClientDataQuery) {
    try {
        Viacom.Mapping.Constants.SiteMappingClientDataFlag = "False";
        var context = new SP.ClientContext.get_current();
        var web = context.get_web();
        var list = web.get_lists().getByTitle(lstName);

        var SiteMappingClientDataCaml = new SP.CamlQuery();
        SiteMappingClientDataCaml.set_viewXml(SiteMappingClientDataQuery);
        var SiteMappingClientDataItems = list.getItems(SiteMappingClientDataCaml);


        context.load(SiteMappingClientDataItems);
        context.executeQueryAsync(function () {

            var enumeratorSiteMappingClientData = SiteMappingClientDataItems.getEnumerator();
            while (enumeratorSiteMappingClientData.moveNext()) {
                var SiteMappingClientDataItem = enumeratorSiteMappingClientData.get_current();
                if (SiteMappingClientDataItem.get_item('ClientName').$0_1.toString().toLowerCase() == Viacom.Mapping.Constants.objTaxClient.taxClientNameValue[0][0].toString().toLowerCase()) {
                    Viacom.Mapping.Constants.clientNameMsg = SiteMappingClientDataItem.get_item('ClientName').$0_1;
                    Viacom.Mapping.Constants.SiteMappingClientName.push([SiteMappingClientDataItem.get_item('ClientName').$0_1, SiteMappingClientDataItem.get_item('ClientName').$1_1]);
                    Viacom.Mapping.Constants.SiteMappingClientCategory.push([SiteMappingClientDataItem.get_item('ClientCategory').$0_1, SiteMappingClientDataItem.get_item('ClientCategory').$1_1]);
                    Viacom.Mapping.Constants.SiteMappingClientSegment.push([SiteMappingClientDataItem.get_item('ClientSegment').$0_1, SiteMappingClientDataItem.get_item('ClientSegment').$1_1]);
                    Viacom.Mapping.Constants.SiteMappingClientSiteUrl.push(SiteMappingClientDataItem.get_item('ClientSiteUrl').$2_1);
                    Viacom.Mapping.Constants.SiteMappingRMDClientID.push(SiteMappingClientDataItem.get_item('RDMClientID'));
                    Viacom.Mapping.Constants.SiteMappingGabrielClientID.push(SiteMappingClientDataItem.get_item('GabrielClientID'));
                    Viacom.Mapping.Constants.SiteMappingWideorbitClientID.push(SiteMappingClientDataItem.get_item('WideOrbitClientID'));
                    Viacom.Mapping.Constants.SiteMappingAdfrontClientID.push(SiteMappingClientDataItem.get_item('AdFrontClientID'));
                }
            }

            var SiteMappingListQuery = null;
            var m = Viacom.Mapping.Constants.SiteMappingListCount;
            SiteMappingListQuery = "<View><Query><Where><And><Contains><FieldRef Name='ClientName'/><Value Type='TaxonomyFieldType'>" + Viacom.Mapping.Constants.objTaxClient.taxClientNameValue[0][0] + "</Value></Contains><Contains><FieldRef Name='AgencyType'/><Value Type='TaxonomyFieldType'>" + Viacom.Mapping.Constants.objTaxClient.allAgencyType[m][2] + "</Value></Contains></And></Where></Query></View>";
            Viacom.editClientAgencyMapping.UpdateSiteMappingList("Site Mapping", SiteMappingListQuery);
        },
            function (sender, args) {
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            }

        );
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
    }
}


Viacom.editClientAgencyMapping.CancelPage = function () {
    var rootSiteUrl = _spPageContextInfo.siteAbsoluteUrl;
    window.location.assign(rootSiteUrl);
};

Viacom.editClientAgencyMapping.ConfirmMessagebox = function (ConfirmMessage) {
    return confirm(ConfirmMessage);

};

Viacom.editClientAgencyMapping.openDialog = function (newSiteUrl) {
    var diaOptions = SP.UI.$create_DialogOptions();
    diaOptions.url = _spPageContextInfo.siteAbsoluteUrl + '/Pages/WebCreationPopup.aspx?termobj=' + '' + '&newSiteUrl=' + newSiteUrl + '&svpName=' + '' + '&siteType=FeatureActivation' + '&agencyUrl=' + Viacom.Mapping.Constants.AgencySiteUrl;
    diaOptions.width = 300;
    diaOptions.height = 215;
    diaOptions.showClose = false;
    diaOptions.allowMaximize = false;
    diaOptions.title = "Please wait..";
    diaOptions.dialogReturnValueCallback = Function.createDelegate(null, Viacom.editClientAgencyMapping.CloseCallBack);
    SP.UI.ModalDialog.showModalDialog(diaOptions);
};

Viacom.editClientAgencyMapping.CloseCallBack = function (result, returnValue) {
    if (result == SP.UI.DialogResult.OK) {
        alert('Mapping updated successfully!');
        location.reload();
    }
    else {
        if (returnValue != 'Fail!') {
            alert('Mapping updation successfully!');
            location.reload();
        }
        else {
            alert('Mapping updation failed!');
        }
    }
};

