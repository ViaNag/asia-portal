
var Viacom = Viacom || {};
Viacom.AdSalesCustomActionsBreakPermission = {}
Viacom.AdSalesCustomActionsBreakPermission.Constants = function () {
    return {
        termNm: null,
        totalItem: 0,
        arrItemId: [],
        itemCounter: 0,
        itemChk: null
    }
};
Viacom.AdSalesCustomActionsBreakPermission.breakPermission = function (itemID, listID) {
    //alert('item iD:' + itemID)
    $('input[value="Ok"]').val('Please wait..');
    Viacom.AdSalesCustomActionsBreakPermission.breakSecurityInheritance(itemID, listID);




};
Viacom.AdSalesCustomActionsBreakPermission.ValidateItems = function (ids) {
    if (ids.indexOf(',') > -1) {
        return false;
    }
    else {
        return true;
    }

};
Viacom.AdSalesCustomActionsBreakPermission.getContentTypeOfCurrentItem = function (id, listId) {


    var clientContext = new SP.ClientContext.get_current();
    var oList = clientContext.get_web().get_lists().getById(listId);

    if (!Viacom.AdSalesCustomActionsBreakPermission.ValidateItems(id)) {
        alert("Please select a single folder.");
        return;
    }
    
    listItem = oList.getItemById(id);
    clientContext.load(listItem);

    
    clientContext.executeQueryAsync(
		function () {
		    var ctid = listItem.get_item("ContentTypeId").toString();

		    if (ctid.indexOf("0x0120") > -1) {
		        Viacom.AdSalesCustomActionsBreakPermission.openDialog(id, listId);

		    }
		},
		function (sender, args) {
		    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
		    SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail');
		    //SP.UI.ModalDialog.commonModalDialogClose();
		});
};

Viacom.AdSalesCustomActionsBreakPermission.breakSecurityInheritance = function (itemID, listId) {
    try {
        var webCtx = new SP.ClientContext.get_current();
        var web = webCtx.get_web();
        var oList = web.get_lists().getById(listId);

        theObject = oList.getItemById(itemID);
        theObject.breakRoleInheritance(false, true);
        webCtx.load(web);
        webCtx.load(theObject);



        webCtx.executeQueryAsync(function () {
            //alert("Permission broken");
            Viacom.AdSalesCustomActionsBreakPermission.AssignGroups(theObject);

        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail');
            
        });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail');
        console.log('Request failed. ' + e.message);
    }
};

Viacom.AdSalesCustomActionsBreakPermission.AssignGroups = function (theObject) {
    try {

        var webCtx = new SP.ClientContext.get_current();
        var web = webCtx.get_web();
        var currentUser = webCtx.get_web().get_currentUser();

        var newPerm = web.get_roleDefinitions().getByName("TeamEdit");
        var newcollPerm = SP.RoleDefinitionBindingCollection.newObject(webCtx);
        newcollPerm.add(newPerm);
        var newWebAssgn = theObject.get_roleAssignments();

        var collUsers = $("span.ms-entity-resolved");
        if (collUsers.length > 0) {
            for (var i = 0; i < collUsers.length; i++) {

                newWebAssgn.add(web.ensureUser(collUsers[i].title), newcollPerm);
            }
        }
        else {
            alert('please select atleast one user');
            return false;

        }

       

        webCtx.executeQueryAsync(function () {
            //alert("Permission granted");
            SP.UI.ModalDialog.commonModalDialogClose(1, 'success');
            
        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail');
            
        });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail');
        //SP.UI.ModalDialog.commonModalDialogClose();
        console.log('Request failed. ' + e.message);
    }
};


Viacom.AdSalesCustomActionsBreakPermission.openDialog = function (itemId, listId) {
    try {
        var diaOptions = SP.UI.$create_DialogOptions();
        diaOptions.url = _spPageContextInfo.siteAbsoluteUrl + '/Pages/breakpermission.aspx?itemId=' + itemId + '&listId=' + listId;
        diaOptions.width = 600;
        diaOptions.height = 200;
        diaOptions.showClose = true;
        diaOptions.allowMaximize = false;
        diaOptions.title = "Set Permissions";
        diaOptions.dialogReturnValueCallback = Function.createDelegate(null, Viacom.AdSalesCustomActionsBreakPermission.CloseCallBack);
        SP.UI.ModalDialog.commonModalDialogClose();
        SP.UI.ModalDialog.showModalDialog(diaOptions);
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error while opening popup to break permissions");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.AdSalesCustomActionsBreakPermission.CloseCallBack = function (result, returnValue) {
    if (result == SP.UI.DialogResult.OK) {
        if (returnValue == 'success') {
            alert('Permissions updated successfully!');
        }
        //location.reload();
    }
    else {
        if (returnValue == 'Fail') {
            alert('There was some error in processing!');
            //location.reload();
        }
        else {
            //alert('failed!');
        }
    }
};

