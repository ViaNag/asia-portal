﻿var Viacom = Viacom || {};
Viacom.svpGroupsPopup = {};
Viacom.createSVPWebPopup = Viacom.createSVPWebPopup || {};
Viacom.svpSiteTaxonomyPopup = Viacom.svpSiteTaxonomyPopup || {};
Viacom.svpGroupsPopup.Constants = function () {
    return {
        mmsName: null,
        svpName: null,
        newSiteUrl: null,
        customTemplateNm: null,
        templateTitle: null,
        grpLstData: null,
        grpNmToSetInLst: null,
        adGroupsNm: null,
        svpGrpCount: null,
        clientGrpCount: null,
        agencyGrpCount: null,
		svpGrpCountwithShortHyphen:null,
		clientGrpCountwithShortHyphen:null,
		agencyGrpCountwithShortHyphen:null,
        domainValue: null,
        siteOwnerGrp: null,
        siteMemberGrp: null,
        siteVisitorGrp: null,
        svpsiteOwnerGrp: null,
        svpsiteMemberGrp: null,
        svpsiteVisitorGrp: null,
        svpdomainValue: null,
        clientsiteOwnerGrp: null,
        agencysiteOwnerGrp: null,
        svpWebTitle:null,
		svpEvpUrl:null,
		svpuniqueGrpForClientSite:null,
		svpGrpForClientSite:null,	
		svpGrpForClientSiteWdShortHyphen: null,
		svpGrpForAgencySite: null,
		svpGrpForAgencySiteWdShortHyphen: null,
		editClientAgencyCount: null,
		EditClientAgencySiteUrl:null,
		EditClientAgencySiteUrlArr: null,
		LibNmInEVpWeb: null,
        cpdUrl:null
    }
};

$(document).ready(function () {
    //For browser which don't support toISoString method
    //var scriptbase = _spPageContextInfo.webServerRelativeUrl + "_layouts/15/";
    //$.getScript(scriptbase + "SP.Runtime.js",
    //    function () {
    //        $.getScript(scriptbase + "SP.js", function () {
    //            $.getScript(scriptbase + "SP.UI.Dialog.js", function () {
    //                $.getScript(scriptbase + "SP.Taxonomy.js", Viacom.svpGroupsPopup.Initialize())
    //            });
    //        });
    //    }
    //);
    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
        SP.SOD.executeFunc('SP.UI.Dialog.js', 'SP.UI.ModalDialog', function () {
            Viacom.svpGroupsPopup.Initialize();
        });
    });

});

Viacom.svpGroupsPopup.PopupgetParameterByName = function (name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
};

Viacom.svpGroupsPopup.Initialize = function () {
    Viacom.agencyGroupsPopup.Initialize();
    Viacom.clientGroupsPopup.Initialize();
    Viacom.svpGroupsPopup.Constants.siteLevel = "SVP";
    Viacom.svpGroupsPopup.Constants.lstGrp = "Site Groups Mapping";
    Viacom.svpGroupsPopup.Constants.mmsName = "Managed Metadata Service";	
    Viacom.svpGroupsPopup.Constants.svpWebTitle = Viacom.svpGroupsPopup.PopupgetParameterByName('webName');
    Viacom.svpGroupsPopup.Constants.newSiteUrl = Viacom.svpGroupsPopup.PopupgetParameterByName('newSiteUrl');
    Viacom.svpGroupsPopup.Constants.svpName = Viacom.agencyGroupsPopup.PopupgetParameterByName('termobj');
	Viacom.svpGroupsPopup.Constants.svpEvpUrl = Viacom.agencyGroupsPopup.PopupgetParameterByName('evpUrl');
    var siteType = Viacom.svpGroupsPopup.PopupgetParameterByName('siteType');
    Viacom.svpGroupsPopup.Constants.customTemplateNm = null;
    Viacom.svpGroupsPopup.Constants.templateTitle = 'SVP Web Template';
    Viacom.svpGroupsPopup.Constants.grpLstData = {};
    Viacom.svpGroupsPopup.Constants.grpLstData.grpName = [];
    Viacom.svpGroupsPopup.Constants.grpLstData.grpPermission = [];
    Viacom.svpGroupsPopup.Constants.grpLstData.grpComment = [];
    Viacom.svpGroupsPopup.Constants.grpLstData.newGroups = [];
    Viacom.svpGroupsPopup.Constants.grpLstData.grpInOtherSites = [];
    Viacom.svpGroupsPopup.Constants.grpLstData.currentWebGroups = [];
    Viacom.svpGroupsPopup.Constants.grpNmToSetInLst = [];
    Viacom.svpGroupsPopup.Constants.adGroupsNm = [];
    Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite = [];
    Viacom.svpGroupsPopup.Constants.LibNmInEVpWeb = "Administrative";
    Viacom.svpGroupsPopup.Constants.cpdUrl = _spPageContextInfo.siteAbsoluteUrl + "/cpd";
	Viacom.svpGroupsPopup.Constants.svpGrpForClientSite=0;
	Viacom.svpGroupsPopup.Constants.svpGrpForClientSiteWdShortHyphen = 0;
	Viacom.svpGroupsPopup.Constants.svpGrpForAgencySite = 0;
	Viacom.svpGroupsPopup.Constants.svpGrpForAgencySiteWdShortHyphen = 0;
    Viacom.svpGroupsPopup.Constants.svpGrpCount = 0;
    Viacom.svpGroupsPopup.Constants.clientGrpCount = 0;
    Viacom.svpGroupsPopup.Constants.agencyGrpCount = 0;
	Viacom.svpGroupsPopup.Constants.svpGrpCountwithShortHyphen = 0;
    Viacom.svpGroupsPopup.Constants.clientGrpCountwithShortHyphen = 0;
    Viacom.svpGroupsPopup.Constants.agencyGrpCountwithShortHyphen = 0;
    Viacom.svpGroupsPopup.Constants.domainValue = null;
    Viacom.svpGroupsPopup.Constants.siteOwnerGrp = null;
    Viacom.svpGroupsPopup.Constants.siteMemberGrp = null;
    Viacom.svpGroupsPopup.Constants.siteVisitorGrp = null;
    Viacom.svpGroupsPopup.Constants.svpsiteOwnerGrp = null;
    Viacom.svpGroupsPopup.Constants.svpsiteMemberGrp = null;
    Viacom.svpGroupsPopup.Constants.svpsiteVisitorGrp = null;
    Viacom.svpGroupsPopup.Constants.svpdomainValue = null;
    Viacom.svpGroupsPopup.Constants.clientsiteOwnerGrp = null;
    Viacom.svpGroupsPopup.Constants.agencysiteOwnerGrp = null;
	
	Viacom.svpGroupsPopup.Constants.EditClientAgencySiteUrl = Viacom.agencyGroupsPopup.PopupgetParameterByName('agencyUrl');
	Viacom.svpGroupsPopup.Constants.EditClientAgencySiteUrlArr = [];
    Viacom.svpGroupsPopup.Constants.editClientAgencyCount = 0;
	
	var clientSvpGrp1=['SVP - EVP',Viacom.svpGroupsPopup.Constants.svpName+ ' - evp'];
	Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite.push(clientSvpGrp1);
	var clientSvpGrp2=[];
	clientSvpGrp2.push('SVP - SuperUsers');clientSvpGrp2.push(Viacom.svpGroupsPopup.Constants.svpName+ ' - super users');
	Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite.push(clientSvpGrp2);
	var clientSvpGrp3=[];
	clientSvpGrp3.push('SVP - Client Planning');clientSvpGrp3.push(Viacom.svpGroupsPopup.Constants.svpName+ ' - client planning');
	Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite.push(clientSvpGrp3);
	var clientSvpGrp4=[];
	clientSvpGrp4.push('SVP - Managers');clientSvpGrp4.push(Viacom.svpGroupsPopup.Constants.svpName+ ' - managers');
	Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite.push(clientSvpGrp4);
	var clientSvpGrp5=[];
	clientSvpGrp5.push('SVP - SVPs');clientSvpGrp5.push(Viacom.svpGroupsPopup.Constants.svpName+ ' - svp');
	Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite.push(clientSvpGrp5);
	var clientSvpGrp6=[];
	clientSvpGrp6.push('SVP - Team');clientSvpGrp6.push(Viacom.svpGroupsPopup.Constants.svpName+ ' - team');
	Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite.push(clientSvpGrp6);
    //alert(siteType);
    $('#lblStatus').html('');
    if (siteType == "SVP") {
        // alert(siteType);
        //$('#lblStatus').html('Getting web Templates..');
        $('#lblStatus').html('Getting root, agency and client site collection owner group..');
        Viacom.createSVPWebPopup.GetWebTemplates(_spPageContextInfo.siteAbsoluteUrl);
    }
     if (siteType == "FeatureActivation") {
	Viacom.createSVPWebPopup.EditDeactivateFeature();
    }
};

Viacom.createSVPWebPopup.GetWebTemplates = function (siteUrl) {
    try {
        $('#lblStatus').html('Getting web Templates..');
        var context = new SP.ClientContext(siteUrl);
        var web = context.get_web();
        var templateCollection = web.getAvailableWebTemplates(1033, false);
        context.load(templateCollection);
        context.executeQueryAsync(function () {
            var siteTemplatesEnum = templateCollection.getEnumerator();
            while (siteTemplatesEnum.moveNext()) {
                var siteTemplate = siteTemplatesEnum.get_current();
                if (siteTemplate.get_title().toLowerCase() == Viacom.svpGroupsPopup.Constants.templateTitle.toLowerCase()) {
                    $('#lblStatus').html('SVP web template found..');
                    Viacom.svpGroupsPopup.Constants.customTemplateNm = siteTemplate.get_name();
                    Viacom.createSVPWebPopup.createWebsite("SVP");
                    break;
                }
            }

        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace())
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
    }
};

Viacom.createSVPWebPopup.createWebsite = function (subsiteType) {
    try {
        $('#lblStatus').html('Web creation for SVP started..');
        var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl + "/" + subsiteType + "/");
        var collWeb = clientContext.get_web().get_webs();
        var webCreationInfo = new SP.WebCreationInformation();
        webCreationInfo.set_title(Viacom.svpGroupsPopup.Constants.svpWebTitle);
        webCreationInfo.set_description("New web for " + Viacom.svpGroupsPopup.Constants.svpName);
        webCreationInfo.set_language(1033);
        var url = Viacom.svpGroupsPopup.Constants.newSiteUrl.substr(Viacom.svpGroupsPopup.Constants.newSiteUrl.lastIndexOf("/") + 1, Viacom.svpGroupsPopup.Constants.newSiteUrl.length);
        webCreationInfo.set_url(url);
        webCreationInfo.set_useSamePermissionsAsParentSite(true);
        webCreationInfo.set_webTemplate(Viacom.svpGroupsPopup.Constants.customTemplateNm);
        var oNewWebsite = collWeb.add(webCreationInfo);
        clientContext.executeQueryAsync(function () {
            //  alert("Created Web site.");
            $('#lblStatus').html('Web created for SVP successfully..');
            Viacom.createSVPWebPopup.getDefaultSiteGroups();
        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        })
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
    }
};

Viacom.createSVPWebPopup.getDefaultSiteGroups = function () {
    try{
        var defualtGrpCtx = new SP.ClientContext(Viacom.svpGroupsPopup.Constants.newSiteUrl);
        var defaultGrpWeb = defualtGrpCtx.get_site().get_rootWeb();
        var ownerGroup = defaultGrpWeb.get_associatedOwnerGroup();
        var memberGroup = defaultGrpWeb.get_associatedMemberGroup();
        var visitorGroup = defaultGrpWeb.get_associatedVisitorGroup();
        defualtGrpCtx.load(ownerGroup);
        defualtGrpCtx.load(memberGroup);
        defualtGrpCtx.load(visitorGroup);
        defualtGrpCtx.executeQueryAsync(function () {
            Viacom.svpGroupsPopup.Constants.siteOwnerGrp = ownerGroup.get_title();
            Viacom.svpGroupsPopup.Constants.siteMemberGrp = memberGroup.get_title();
            Viacom.svpGroupsPopup.Constants.siteVisitorGrp = visitorGroup.get_title();
            Viacom.createSVPWebPopup.GetDataSiteGrp();
        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
    }
};

Viacom.createSVPWebPopup.GetDataSiteGrp = function () {
    try {
        Viacom.svpGroupsPopup.Constants.grpLstData.grpInOtherSites.length = 0;
        var lstCtx = SP.ClientContext.get_current();
        var root = lstCtx.get_site().get_rootWeb();
        var oList = root.get_lists().getByTitle(Viacom.svpGroupsPopup.Constants.lstGrp);
        var filterUrl = Viacom.svpGroupsPopup.Constants.siteLevel;
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml("<View><Query><Where><Eq><FieldRef Name='SiteLevel'/><Value Type='Choice'>" + filterUrl + "</Value></Eq></Where></Query></View>");
        var collListItem = oList.getItems(camlQuery);
        lstCtx.load(collListItem);
        lstCtx.executeQueryAsync(
                            function (sender, args) {
                                var listItemInfo = '';
                                var listItemEnumerator = collListItem.getEnumerator();
                                try {
                                    while (listItemEnumerator.moveNext()) {
                                        var oListItem = listItemEnumerator.get_current();
                                        var oListItem = listItemEnumerator.get_current();
                                        Viacom.svpGroupsPopup.Constants.grpLstData.grpName.push(oListItem.get_item('GroupName'));
                                        Viacom.svpGroupsPopup.Constants.grpLstData.grpPermission.push(oListItem.get_item('PermissionLevel'));
                                        Viacom.svpGroupsPopup.Constants.grpLstData.grpComment.push(oListItem.get_item('Comment'));
                                        var svpTmName = oListItem.get_item('GroupName').replace("<SVP Last Name>", Viacom.svpGroupsPopup.Constants.svpName);
                                        Viacom.svpGroupsPopup.Constants.grpLstData.grpInOtherSites.push(svpTmName);
                                        Viacom.svpGroupsPopup.Constants.grpNmToSetInLst.push(svpTmName);
                                        Viacom.svpGroupsPopup.Constants.adGroupsNm.push(svpTmName);
                                    }
                                    $('#lblStatus').html('Groups data fetached for SVP from configuration list..');
                                    Viacom.createSVPWebPopup.RootSiteGroups();
                                }
                                catch (err) {
                                    SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
                                    return false;
                                }
                            }, function (sender, args) {
                                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                                SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
                            });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        console.log('Request failed. ' + e.message);
    }
}

Viacom.createSVPWebPopup.RootSiteGroups = function () {
    Viacom.svpGroupsPopup.Constants.grpLstData.currentWebGroups.length = 0;
    Viacom.svpGroupsPopup.Constants.grpLstData.newGroups.length = 0;
    Viacom.svpGroupsPopup.Constants.grpLstData.currentWebGroups.push("Approvers");
    Viacom.svpGroupsPopup.Constants.grpLstData.currentWebGroups.push("Designers");
    Viacom.svpGroupsPopup.Constants.grpLstData.currentWebGroups.push("Excel Services Viewers");
    Viacom.svpGroupsPopup.Constants.grpLstData.currentWebGroups.push("Hierarchy Managers");
    Viacom.svpGroupsPopup.Constants.grpLstData.currentWebGroups.push("Restricted Readers");
    Viacom.svpGroupsPopup.Constants.grpLstData.currentWebGroups.push("Translation Managers");
    Viacom.svpGroupsPopup.Constants.grpLstData.currentWebGroups.push("NT AUTHORITY\authenticated users");
    Viacom.svpGroupsPopup.Constants.grpLstData.currentWebGroups.push("Quick Deploy Users");
    var prefixValue = Viacom.svpGroupsPopup.Constants.svpName;
	var webGroupsPreFix=Viacom.svpGroupsPopup.Constants.svpWebTitle;

    Viacom.svpGroupsPopup.Constants.grpLstData.grpName.push(webGroupsPreFix + " Owners");
    Viacom.svpGroupsPopup.Constants.grpLstData.grpComment.push(webGroupsPreFix + " Owners");
    Viacom.svpGroupsPopup.Constants.grpLstData.grpPermission.push("Full Control");
    Viacom.svpGroupsPopup.Constants.grpLstData.grpName.push(webGroupsPreFix + " Members");
    Viacom.svpGroupsPopup.Constants.grpLstData.grpComment.push(webGroupsPreFix + " Members");
    Viacom.svpGroupsPopup.Constants.grpLstData.grpPermission.push("Contribute");
    Viacom.svpGroupsPopup.Constants.grpLstData.grpName.push(webGroupsPreFix + " Visitors");
    Viacom.svpGroupsPopup.Constants.grpLstData.grpComment.push(webGroupsPreFix + " Visitors");
    Viacom.svpGroupsPopup.Constants.grpLstData.grpPermission.push("Read");
    Viacom.svpGroupsPopup.Constants.grpLstData.grpName.push(prefixValue + " - Super Users");
    Viacom.svpGroupsPopup.Constants.grpLstData.grpComment.push(prefixValue + " Super Users");
    Viacom.svpGroupsPopup.Constants.grpLstData.grpPermission.push("TeamSuperUser");

    try {
        var grpCtx = SP.ClientContext.get_current();
        var grpWeb = grpCtx.get_site().get_rootWeb();
        var groupCollection = grpWeb.get_siteGroups();
        var membersGRP = new SP.GroupCreationInformation();
        
        for (var m = 0; m < Viacom.svpGroupsPopup.Constants.grpLstData.grpName.length; m++) {
            Viacom.svpGroupsPopup.Constants.grpLstData.grpName[m] = Viacom.svpGroupsPopup.Constants.grpLstData.grpName[m].replace("<SVP Last Name>", prefixValue);
            Viacom.svpGroupsPopup.Constants.grpLstData.newGroups.push(Viacom.svpGroupsPopup.Constants.grpLstData.grpName[m]);
            membersGRP.set_title(Viacom.svpGroupsPopup.Constants.grpLstData.grpName[m]);
            membersGRP.set_description(Viacom.svpGroupsPopup.Constants.grpLstData.grpComment[m]);
            var oMembersGRP = groupCollection.add(membersGRP);
            var gpPermission = null;
            if (Viacom.svpGroupsPopup.Constants.grpLstData.grpName[m] != prefixValue + " - Super Users" && (Viacom.svpGroupsPopup.Constants.grpLstData.grpName[m] != webGroupsPreFix + " Owners") && (Viacom.svpGroupsPopup.Constants.grpLstData.grpName[m] != webGroupsPreFix + " Members") && (Viacom.svpGroupsPopup.Constants.grpLstData.grpName[m] != webGroupsPreFix + " Visitors")) {
                gpPermission = grpWeb.get_roleDefinitions().getByName("TeamRead");
            }
            else if(Viacom.svpGroupsPopup.Constants.grpLstData.grpName[m] != webGroupsPreFix + " Owners") {
               gpPermission = grpWeb.get_roleDefinitions().getByName("Full Control");
            }
			else if(Viacom.svpGroupsPopup.Constants.grpLstData.grpName[m] != prefixValue + " - Super Users") {               
				gpPermission = grpWeb.get_roleDefinitions().getByName("TeamSuperUser");
            }
			else if(Viacom.svpGroupsPopup.Constants.grpLstData.grpName[m] != webGroupsPreFix + " Members") {
                gpPermission = grpWeb.get_roleDefinitions().getByName("Contribute");
            }
			else if(Viacom.svpGroupsPopup.Constants.grpLstData.grpName[m] != webGroupsPreFix + " Visitors") {
                gpPermission = grpWeb.get_roleDefinitions().getByName("Read");
            }
            var collPerm = SP.RoleDefinitionBindingCollection.newObject(grpCtx);
            collPerm.add(gpPermission);
            var assignments = grpWeb.get_roleAssignments();
            var roleAssignmentContribute = assignments.add(oMembersGRP, collPerm);
            grpCtx.load(oMembersGRP);
           }
        grpCtx.executeQueryAsync(function () {
            // alert('groupCreated');     
            $('#lblStatus').html('Groups created for SVP..');
            Viacom.svpGroupsPopup.Constants.grpLstData.currentWebGroups.push(Viacom.svpGroupsPopup.Constants.siteOwnerGrp);
            Viacom.svpGroupsPopup.Constants.grpLstData.currentWebGroups.push(Viacom.svpGroupsPopup.Constants.siteMemberGrp);
            Viacom.svpGroupsPopup.Constants.grpLstData.currentWebGroups.push(Viacom.svpGroupsPopup.Constants.siteVisitorGrp);
			
			Viacom.svpGroupsPopup.Constants.grpLstData.newGroups.push('RECORDS MANAGEMENT PROJECT GROUP');
			Viacom.svpGroupsPopup.Constants.grpLstData.grpPermission.push("TeamSuperUser");
            Viacom.createSVPWebPopup.breakSecurityInheritance();
        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createSVPWebPopup.GetDomainValue = function () {
    var domainCtx = SP.ClientContext.get_current();
    var root = domainCtx.get_site().get_rootWeb();
    var oList = root.get_lists().getByTitle('Configurations');
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml("<View><Query><Where><Eq><FieldRef Name='Title'/><Value Type='Text'>DomainName</Value></Eq></Where></Query></View>");
    var collListItem = oList.getItems(camlQuery);
    domainCtx.load(collListItem);
    domainCtx.executeQueryAsync(
                        function (sender, args) {
                            var listItemInfo = '';
                            var listItemEnumerator = collListItem.getEnumerator();
                            try {
                                while (listItemEnumerator.moveNext()) {
                                    var oListItem = listItemEnumerator.get_current();
                                    Viacom.svpGroupsPopup.Constants.domainValue = oListItem.get_item('Value1');
                                    Viacom.createSVPWebPopup.AssociateAdGroups(_spPageContextInfo.siteAbsoluteUrl, Viacom.svpGroupsPopup.Constants.adGroupsNm[0].toLowerCase(), "svp",false);
                                    break;
                                }
                            }
                           catch (err) {
                                    SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
                                    return false;
                                }
                            }, function (sender, args) {
                                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                                SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
                            });
};

Viacom.createSVPWebPopup.GroupsAssociationToClientSite = function (siteUrl, grpNm,userForGrp, siteType, shortHyphen) {
try{
var grpCtx = new SP.ClientContext(siteUrl);
        var grpWeb = grpCtx.get_site().get_rootWeb();
        var groupCollection = grpWeb.get_siteGroups();
        grpCtx.load(grpWeb);
        var group = groupCollection.getByName(grpNm.toLowerCase());
        grpCtx.load(group);
		var grpExist=false;
        grpCtx.executeQueryAsync(function () {
                if (group.get_title().toLowerCase() == grpNm.toLowerCase()) {
					grpExist=true;
                    var adGNm = userForGrp;    
					var adGNmWithShortHyphen=userForGrp;
					var adGrpNm=null;
					var adGrpNmWithShortHyphen=null;
					var user=null;
					var userWithShortHyphen=null;
					var oUser=null;
					var oUserWithShortHyphen=null;
					if(!shortHyphen){
                    adGNm = adGNm.replace('-', '–');
					adGrpNm = Viacom.svpGroupsPopup.Constants.domainValue + "\\" + adGNm;
					user = grpWeb.ensureUser(adGrpNm);
					oUser = group.get_users().addUser(user);
					grpCtx.load(oUser);
					}					
                    if(shortHyphen){
					adGrpNmWithShortHyphen= Viacom.svpGroupsPopup.Constants.domainValue + "\\" + adGNmWithShortHyphen;
					userWithShortHyphen = grpWeb.ensureUser(adGrpNmWithShortHyphen);
					oUserWithShortHyphen = group.get_users().addUser(userWithShortHyphen);
					grpCtx.load(oUserWithShortHyphen);
					}					
                    grpCtx.executeQueryAsync(function () {
                        $('#lblStatus').html('Active directory group - ' + adGrpNm + ' added to SharePoint group - ' + grpNm + ' for site - '+siteType+'..');
                         if (siteType == "clientSVPGrp") {
							switch(shortHyphen){
							case false:{
							Viacom.svpGroupsPopup.Constants.svpGrpForClientSite++;
							break;
							}
							case true:{
							Viacom.svpGroupsPopup.Constants.svpGrpForClientSiteWdShortHyphen++;
							break;
							}
							}
                            if (Viacom.svpGroupsPopup.Constants.svpGrpForClientSite < Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite.length) {
                                Viacom.createSVPWebPopup.GroupsAssociationToClientSite(_spPageContextInfo.siteAbsoluteUrl + "/sites/clients", Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite[Viacom.svpGroupsPopup.Constants.svpGrpForClientSite][0].toLowerCase(),Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite[Viacom.svpGroupsPopup.Constants.svpGrpForClientSite][1].toLowerCase(), "clientSVPGrp",false);
                            }
							else if (Viacom.svpGroupsPopup.Constants.svpGrpForClientSiteWdShortHyphen < Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite.length) {
                                Viacom.createSVPWebPopup.GroupsAssociationToClientSite(_spPageContextInfo.siteAbsoluteUrl + "/sites/clients", Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite[Viacom.svpGroupsPopup.Constants.svpGrpForClientSiteWdShortHyphen][0].toLowerCase(),Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite[Viacom.svpGroupsPopup.Constants.svpGrpForClientSiteWdShortHyphen][1].toLowerCase(), "clientSVPGrp",true);
                            }
                            else {
							    Viacom.createSVPWebPopup.GroupsAssociationToClientSite(_spPageContextInfo.siteAbsoluteUrl + "/sites/agencies", Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite[0][0].toLowerCase(), Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite[0][1].toLowerCase(), "agencySVPGrp", false);
                            }
                         }
                         else if (siteType == "agencySVPGrp") {
                             switch (shortHyphen) {
                                 case false: {
                                     Viacom.svpGroupsPopup.Constants.svpGrpForAgencySite++;
                                     break;
                                 }
                                 case true: {
                                     Viacom.svpGroupsPopup.Constants.svpGrpForAgencySiteWdShortHyphen++;
                                     break;
                                 }
                             }
                             if (Viacom.svpGroupsPopup.Constants.svpGrpForAgencySite < Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite.length) {
                                 Viacom.createSVPWebPopup.GroupsAssociationToClientSite(_spPageContextInfo.siteAbsoluteUrl + "/sites/agencies", Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite[Viacom.svpGroupsPopup.Constants.svpGrpForAgencySite][0].toLowerCase(), Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite[Viacom.svpGroupsPopup.Constants.svpGrpForAgencySite][1].toLowerCase(), "agencySVPGrp", false);
                             }
                             else if (Viacom.svpGroupsPopup.Constants.svpGrpForAgencySiteWdShortHyphen < Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite.length) {
                                 Viacom.createSVPWebPopup.GroupsAssociationToClientSite(_spPageContextInfo.siteAbsoluteUrl + "/sites/agencies", Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite[Viacom.svpGroupsPopup.Constants.svpGrpForAgencySiteWdShortHyphen][0].toLowerCase(), Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite[Viacom.svpGroupsPopup.Constants.svpGrpForAgencySiteWdShortHyphen][1].toLowerCase(), "agencySVPGrp", true);
                             }
                             else {
                                 Viacom.createSVPWebPopup.EditGroupsPermissionInEVPLibrary();
                             }
                         }
						}, function (sender, args) {
                        console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                        $('#lblStatus').html('Active directory group - ' + adGrpNm + ' not exist');
						if (siteType == "clientSVPGrp") {
                            switch(shortHyphen){
							case false:{
							Viacom.svpGroupsPopup.Constants.svpGrpForClientSite++;
							break;
							}
							case true:{
							Viacom.svpGroupsPopup.Constants.svpGrpForClientSiteWdShortHyphen++;
							break;
							}
							}
                            if (Viacom.svpGroupsPopup.Constants.svpGrpForClientSite < Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite.length) {
                                Viacom.createSVPWebPopup.GroupsAssociationToClientSite(_spPageContextInfo.siteAbsoluteUrl + "/sites/clients", Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite[Viacom.svpGroupsPopup.Constants.svpGrpForClientSite][0].toLowerCase(),Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite[Viacom.svpGroupsPopup.Constants.svpGrpForClientSite][1].toLowerCase(), "clientSVPGrp",false);
                            }
							else if (Viacom.svpGroupsPopup.Constants.svpGrpForClientSiteWdShortHyphen < Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite.length) {
                                Viacom.createSVPWebPopup.GroupsAssociationToClientSite(_spPageContextInfo.siteAbsoluteUrl + "/sites/clients", Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite[Viacom.svpGroupsPopup.Constants.svpGrpForClientSiteWdShortHyphen][0].toLowerCase(),Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite[Viacom.svpGroupsPopup.Constants.svpGrpForClientSiteWdShortHyphen][1].toLowerCase(), "clientSVPGrp",true);
                            }
                            else {
							    Viacom.createSVPWebPopup.GroupsAssociationToClientSite(_spPageContextInfo.siteAbsoluteUrl + "/sites/agencies", Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite[0][0].toLowerCase(), Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite[0][1].toLowerCase(), "agencySVPGrp", false);
							}
						}
						else if (siteType == "agencySVPGrp") {
						    switch (shortHyphen) {
						        case false: {
						            Viacom.svpGroupsPopup.Constants.svpGrpForAgencySite++;
						            break;
						        }
						        case true: {
						            Viacom.svpGroupsPopup.Constants.svpGrpForAgencySiteWdShortHyphen++;
						            break;
						        }
						    }
						    if (Viacom.svpGroupsPopup.Constants.svpGrpForAgencySite < Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite.length) {
						        Viacom.createSVPWebPopup.GroupsAssociationToClientSite(_spPageContextInfo.siteAbsoluteUrl + "/sites/agencies", Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite[Viacom.svpGroupsPopup.Constants.svpGrpForAgencySite][0].toLowerCase(), Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite[Viacom.svpGroupsPopup.Constants.svpGrpForAgencySite][1].toLowerCase(), "agencySVPGrp", false);
						    }
						    else if (Viacom.svpGroupsPopup.Constants.svpGrpForAgencySiteWdShortHyphen < Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite.length) {
						        Viacom.createSVPWebPopup.GroupsAssociationToClientSite(_spPageContextInfo.siteAbsoluteUrl + "/sites/agencies", Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite[Viacom.svpGroupsPopup.Constants.svpGrpForAgencySiteWdShortHyphen][0].toLowerCase(), Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite[Viacom.svpGroupsPopup.Constants.svpGrpForAgencySiteWdShortHyphen][1].toLowerCase(), "agencySVPGrp", true);
						    }
						    else {
						        Viacom.createSVPWebPopup.EditGroupsPermissionInEVPLibrary();
						    }
						}
						});
                }
			if(!grpExist){
			 //Viacom.createSVPWebPopup.ActivateFeature('b10adbab-e67b-4734-afa0-e7675d12eff1');
			 Viacom.createSVPWebPopup.EditGroupsPermissionInEVPLibrary();
			}
        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
     }
};

Viacom.createSVPWebPopup.AssociateAdGroups = function (siteUrl, grpNm, siteType, shortHyphen) {
    try{
        var grpCtx = new SP.ClientContext(siteUrl);
        var grpWeb = grpCtx.get_site().get_rootWeb();
        var groupCollection = grpWeb.get_siteGroups();
        grpCtx.load(grpWeb);
        var group = groupCollection.getByName(grpNm.toLowerCase());
        grpCtx.load(group);
        grpCtx.executeQueryAsync(function () {
                if (group.get_title().toLowerCase() == grpNm.toLowerCase()) {
					var adGNmWithShortHyphen=null;
                    var adGNm = null;
					var adGrpNm=null;
					var adGrpNmWithShortHyphen=null;
					var user = null;					
					var userWithShortHyphen = null;
					var oUser = null;
					var oUserWithShortHyphen = null;
					if(!shortHyphen){
                        adGNm = group.get_title().toLowerCase();
                        adGNm = adGNm.replace('-', '–');
						adGrpNm = Viacom.svpGroupsPopup.Constants.domainValue + "\\" + adGNm;
						user=grpWeb.ensureUser(adGrpNm);
						oUser=group.get_users().addUser(user);
						grpCtx.load(oUser);
						}
					if(shortHyphen){
						adGNmWithShortHyphen= group.get_title().toLowerCase();
						adGrpNmWithShortHyphen= Viacom.svpGroupsPopup.Constants.domainValue + "\\" + adGNmWithShortHyphen;
						userWithShortHyphen=grpWeb.ensureUser(adGrpNmWithShortHyphen);
						oUserWithShortHyphen=group.get_users().addUser(userWithShortHyphen);
						grpCtx.load(oUserWithShortHyphen);
						} 
                    grpCtx.executeQueryAsync(function () {
                        $('#lblStatus').html('Active directory group - ' + adGrpNm + ' added to SharePoint group - ' + grpNm + ' for site - '+siteType+'..');
                        if (siteType == "svp") {
							switch (shortHyphen){
								case false:{
								Viacom.svpGroupsPopup.Constants.svpGrpCount++;
								break;
								}
								case true:{
								Viacom.svpGroupsPopup.Constants.svpGrpCountwithShortHyphen++;
								break;
								}
							}								
                            if (Viacom.svpGroupsPopup.Constants.svpGrpCount < Viacom.svpGroupsPopup.Constants.adGroupsNm.length) {
                                Viacom.createSVPWebPopup.AssociateAdGroups(_spPageContextInfo.siteAbsoluteUrl, Viacom.svpGroupsPopup.Constants.adGroupsNm[Viacom.svpGroupsPopup.Constants.svpGrpCount].toLowerCase(), "svp",false);
                            }
                            else if (Viacom.svpGroupsPopup.Constants.svpGrpCountwithShortHyphen < Viacom.svpGroupsPopup.Constants.adGroupsNm.length) {
								
                                Viacom.createSVPWebPopup.AssociateAdGroups(_spPageContextInfo.siteAbsoluteUrl, Viacom.svpGroupsPopup.Constants.adGroupsNm[Viacom.svpGroupsPopup.Constants.svpGrpCountwithShortHyphen].toLowerCase(), "svp",true);
                            }
                            else {
                                Viacom.createSVPWebPopup.AssociateAdGroups(_spPageContextInfo.siteAbsoluteUrl + "/sites/clients", Viacom.svpGroupsPopup.Constants.adGroupsNm[0], "client",false);
                            }
                        }
                        else if (siteType == "client") {
						 switch (shortHyphen){
								case false:{
								Viacom.svpGroupsPopup.Constants.clientGrpCount++;
								break;
								}
								case true:{
								Viacom.svpGroupsPopup.Constants.clientGrpCountwithShortHyphen++;
								break;
								}
							}
                            if (Viacom.svpGroupsPopup.Constants.clientGrpCount < Viacom.svpGroupsPopup.Constants.adGroupsNm.length) {
                                Viacom.createSVPWebPopup.AssociateAdGroups(_spPageContextInfo.siteAbsoluteUrl + "/sites/clients", Viacom.svpGroupsPopup.Constants.adGroupsNm[Viacom.svpGroupsPopup.Constants.clientGrpCount].toLowerCase(), "client",false);
                            }
							else if (Viacom.svpGroupsPopup.Constants.clientGrpCountwithShortHyphen < Viacom.svpGroupsPopup.Constants.adGroupsNm.length) {
                                Viacom.createSVPWebPopup.AssociateAdGroups(_spPageContextInfo.siteAbsoluteUrl + "/sites/clients", Viacom.svpGroupsPopup.Constants.adGroupsNm[Viacom.svpGroupsPopup.Constants.clientGrpCountwithShortHyphen].toLowerCase(), "client",true);
                            }
                            else {
                                Viacom.createSVPWebPopup.AssociateAdGroups(_spPageContextInfo.siteAbsoluteUrl + "/sites/agencies", Viacom.svpGroupsPopup.Constants.adGroupsNm[0], "agencies",false);
                            }
                        }						
                        else if (siteType == "agencies") {
							switch (shortHyphen){
								case false:{
								Viacom.svpGroupsPopup.Constants.agencyGrpCount++;
								break;
								}
								case true:{
								Viacom.svpGroupsPopup.Constants.agencyGrpCountwithShortHyphen++;
								break;
								}
							}
                            if (Viacom.svpGroupsPopup.Constants.agencyGrpCount < Viacom.svpGroupsPopup.Constants.adGroupsNm.length) {
                                Viacom.createSVPWebPopup.AssociateAdGroups(_spPageContextInfo.siteAbsoluteUrl + "/sites/agencies", Viacom.svpGroupsPopup.Constants.adGroupsNm[Viacom.svpGroupsPopup.Constants.agencyGrpCount].toLowerCase(), "agencies",false);
                            }
							else if (Viacom.svpGroupsPopup.Constants.agencyGrpCountwithShortHyphen < Viacom.svpGroupsPopup.Constants.adGroupsNm.length) {
                                Viacom.createSVPWebPopup.AssociateAdGroups(_spPageContextInfo.siteAbsoluteUrl + "/sites/agencies", Viacom.svpGroupsPopup.Constants.adGroupsNm[Viacom.svpGroupsPopup.Constants.agencyGrpCountwithShortHyphen].toLowerCase(), "agencies",true);
                            }
                            else {
                                //Viacom.createSVPWebPopup.AssociateAdGroups(_spPageContextInfo.siteAbsoluteUrl, Viacom.svpGroupsPopup.Constants.svpWebTitle + " Owners", "OwnerGroup");
								Viacom.createSVPWebPopup.GroupsAssociationToClientSite(_spPageContextInfo.siteAbsoluteUrl + "/sites/clients", Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite[0][0].toLowerCase(),Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite[0][1].toLowerCase(), "clientSVPGrp",false);
                            }
                        }
                    }, function (sender, args) {
                        console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                        $('#lblStatus').html('Active directory group - ' + adGrpNm + ' not exist');
                        if (siteType == "svp") {						
							switch (shortHyphen){
								case false:{
								Viacom.svpGroupsPopup.Constants.svpGrpCount++;
								break;
								}
								case true:{
								Viacom.svpGroupsPopup.Constants.svpGrpCountwithShortHyphen++;
								break;
								}
							}
                            if (Viacom.svpGroupsPopup.Constants.svpGrpCount < Viacom.svpGroupsPopup.Constants.adGroupsNm.length) {
                                Viacom.createSVPWebPopup.AssociateAdGroups(_spPageContextInfo.siteAbsoluteUrl, Viacom.svpGroupsPopup.Constants.adGroupsNm[Viacom.svpGroupsPopup.Constants.svpGrpCount].toLowerCase(), "svp",false);
                            }
                            else if (Viacom.svpGroupsPopup.Constants.svpGrpCountwithShortHyphen < Viacom.svpGroupsPopup.Constants.adGroupsNm.length) {
								
                                Viacom.createSVPWebPopup.AssociateAdGroups(_spPageContextInfo.siteAbsoluteUrl, Viacom.svpGroupsPopup.Constants.adGroupsNm[Viacom.svpGroupsPopup.Constants.svpGrpCountwithShortHyphen].toLowerCase(), "svp",true);
                            }
                            else {
                                Viacom.createSVPWebPopup.AssociateAdGroups(_spPageContextInfo.siteAbsoluteUrl + "/sites/clients", Viacom.svpGroupsPopup.Constants.adGroupsNm[0], "client",false);
                            }
                        }
                        else if (siteType == "client") {
							switch (shortHyphen){
								case false:{
								Viacom.svpGroupsPopup.Constants.clientGrpCount++;
								break;
								}
								case true:{
								Viacom.svpGroupsPopup.Constants.clientGrpCountwithShortHyphen++;
								break;
								}
							}
                            if (Viacom.svpGroupsPopup.Constants.clientGrpCount < Viacom.svpGroupsPopup.Constants.adGroupsNm.length) {
                                Viacom.createSVPWebPopup.AssociateAdGroups(_spPageContextInfo.siteAbsoluteUrl + "/sites/clients", Viacom.svpGroupsPopup.Constants.adGroupsNm[Viacom.svpGroupsPopup.Constants.clientGrpCount].toLowerCase(), "client",false);
                            }
							else if (Viacom.svpGroupsPopup.Constants.clientGrpCountwithShortHyphen < Viacom.svpGroupsPopup.Constants.adGroupsNm.length) {
                                Viacom.createSVPWebPopup.AssociateAdGroups(_spPageContextInfo.siteAbsoluteUrl + "/sites/clients", Viacom.svpGroupsPopup.Constants.adGroupsNm[Viacom.svpGroupsPopup.Constants.clientGrpCountwithShortHyphen].toLowerCase(), "client",true);
                            }
                            else {
                                Viacom.createSVPWebPopup.AssociateAdGroups(_spPageContextInfo.siteAbsoluteUrl + "/sites/agencies", Viacom.svpGroupsPopup.Constants.adGroupsNm[0], "agencies",false);
                            }
                        }
                        else if (siteType == "agencies") {
							switch (shortHyphen){
								case false:{
								Viacom.svpGroupsPopup.Constants.agencyGrpCount++;
								break;
								}
								case true:{
								Viacom.svpGroupsPopup.Constants.agencyGrpCountwithShortHyphen++;
								break;
								}
							}
                            if (Viacom.svpGroupsPopup.Constants.agencyGrpCount < Viacom.svpGroupsPopup.Constants.adGroupsNm.length) {
                                Viacom.createSVPWebPopup.AssociateAdGroups(_spPageContextInfo.siteAbsoluteUrl + "/sites/agencies", Viacom.svpGroupsPopup.Constants.adGroupsNm[Viacom.svpGroupsPopup.Constants.agencyGrpCount].toLowerCase(), "agencies",false);
                            }
							else if (Viacom.svpGroupsPopup.Constants.agencyGrpCountwithShortHyphen < Viacom.svpGroupsPopup.Constants.adGroupsNm.length) {
                                Viacom.createSVPWebPopup.AssociateAdGroups(_spPageContextInfo.siteAbsoluteUrl + "/sites/agencies", Viacom.svpGroupsPopup.Constants.adGroupsNm[Viacom.svpGroupsPopup.Constants.agencyGrpCountwithShortHyphen].toLowerCase(), "agencies",true);
                            }
                            else {
                                //Viacom.createSVPWebPopup.AssociateAdGroups(_spPageContextInfo.siteAbsoluteUrl, Viacom.svpGroupsPopup.Constants.svpWebTitle + " Owners", "OwnerGroup");
								Viacom.createSVPWebPopup.GroupsAssociationToClientSite(_spPageContextInfo.siteAbsoluteUrl + "/sites/clients", Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite[0][0].toLowerCase(),Viacom.svpGroupsPopup.Constants.svpuniqueGrpForClientSite[0][1].toLowerCase(), "clientSVPGrp",false);
                            }
                        }                   
                    });
                }
        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
    }
}

Viacom.createSVPWebPopup.breakSecurityInheritance = function () {
    try {
        var webCtx = new SP.ClientContext(Viacom.svpGroupsPopup.Constants.newSiteUrl);
        var web = webCtx.get_web();
        web.breakRoleInheritance(false, false);
        webCtx.load(web);
        webCtx.executeQueryAsync(function () {
            //   alert("Web Permission break");
            $('#lblStatus').html('Web permission break for SVP..');
            Viacom.createSVPWebPopup.breakListsSecurityInheritance(Viacom.svpGroupsPopup.Constants.newSiteUrl, "Management");
        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
            //SP.UI.ModalDialog.commonModalDialogClose();
        });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        //SP.UI.ModalDialog.commonModalDialogClose();
        console.log('Request failed. ' + e.message);
    }
};
Viacom.createSVPWebPopup.breakListsSecurityInheritance = function (siteUrl, lstTitle) {
    try {
        var clientContext = new SP.ClientContext(siteUrl);
        var oList = clientContext.get_web().get_lists().getByTitle(lstTitle);
        //break all lists permission
        var oListUpFront = clientContext.get_web().get_lists().getByTitle('Upfront');
        var oListScatter = clientContext.get_web().get_lists().getByTitle('Scatter');
        var oListEvents = clientContext.get_web().get_lists().getByTitle('Events');
        var oListAdmin = clientContext.get_web().get_lists().getByTitle('Administrative');
        var oListUncategory = clientContext.get_web().get_lists().getByTitle('Uncategorized');
        var oListWeeklyHighlights = clientContext.get_web().get_lists().getByTitle('Weekly Highlights');
		var oListMyTopAgencies = clientContext.get_web().get_lists().getByTitle('My Top Agencies');
		var oListMyTopClients= clientContext.get_web().get_lists().getByTitle('My Top Clients');
		var oListDocuments= clientContext.get_web().get_lists().getByTitle('Documents');
        oList.breakRoleInheritance(false, false);
        clientContext.load(oList);

        oListUpFront.breakRoleInheritance(false, false);
        clientContext.load(oListUpFront);
        oListScatter.breakRoleInheritance(false, false);
        clientContext.load(oListScatter);
        oListEvents.breakRoleInheritance(false, false);
        clientContext.load(oListEvents);
        oListAdmin.breakRoleInheritance(false, false);
        clientContext.load(oListAdmin);
        oListUncategory.breakRoleInheritance(false, false);
        clientContext.load(oListUncategory);
        oListWeeklyHighlights.breakRoleInheritance(false, false);
        clientContext.load(oListWeeklyHighlights);
		oListMyTopAgencies.breakRoleInheritance(false, false);
        clientContext.load(oListMyTopAgencies);
		oListMyTopClients.breakRoleInheritance(false, false);
        clientContext.load(oListMyTopClients);
		oListDocuments.breakRoleInheritance(false, false);
        clientContext.load(oListDocuments);

        clientContext.executeQueryAsync(function () {
            $('#lblStatus').html('List permission break for SVP..');
            Viacom.createSVPWebPopup.DeleteGroupsInNewWeb();
        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
            //SP.UI.ModalDialog.commonModalDialogClose();
        });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        //SP.UI.ModalDialog.commonModalDialogClose();
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createSVPWebPopup.DeleteGroupsInNewWeb = function () {
    try {
        var listGrpToDel = [];
        listGrpToDel.push(Viacom.svpGroupsPopup.Constants.siteOwnerGrp);
        listGrpToDel.push(Viacom.svpGroupsPopup.Constants.siteMemberGrp);
        listGrpToDel.push(Viacom.svpGroupsPopup.Constants.siteVisitorGrp);
        var delGrpCtx = new SP.ClientContext(Viacom.svpGroupsPopup.Constants.newSiteUrl);
        var delGrpweb = delGrpCtx.get_web();
        var groupCollection = delGrpCtx.get_web().get_siteGroups();
        var groupsArr = [];
        groupsArr.length = Viacom.svpGroupsPopup.Constants.grpLstData.newGroups.length;
        for (var m = 0; m < Viacom.svpGroupsPopup.Constants.grpLstData.newGroups.length; m++) {
            groupsArr[m] = groupCollection.getByName(Viacom.svpGroupsPopup.Constants.grpLstData.newGroups[m]);
            delGrpCtx.load(groupsArr[m]);
        }
        delGrpCtx.executeQueryAsync(function () {
           Viacom.createSVPWebPopup.EditGroupsPermissionInNewWeb(delGrpCtx, delGrpweb, groupsArr);
			//Viacom.createSVPWebPopup.EditGroupsPermissionInEVPLibrary(delGrpCtx, delGrpweb, groupsArr);
        }, function (sender, args) {
            console.log('Request failed at deassociate group function. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
            //SP.UI.ModalDialog.commonModalDialogClose();
        })
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        //SP.UI.ModalDialog.commonModalDialogClose();
        console.log('Exception at at deassociate group function. ' + e.message);
    }
};

Viacom.createSVPWebPopup.EditGroupsPermissionInEVPLibrary = function () {
	try {
		var evpGrpCtx = new SP.ClientContext(Viacom.svpGroupsPopup.Constants.svpEvpUrl);
		var evpWeb=evpGrpCtx.get_web();
        var evpGrpRootSiteWeb = evpGrpCtx.get_site().get_rootWeb();
        var groupCollection = evpGrpRootSiteWeb.get_siteGroups();
        var group = groupCollection.getByName(Viacom.svpGroupsPopup.Constants.svpName.toLowerCase() + " - evp");
        evpGrpCtx.load(group);
		evpGrpCtx.load(evpWeb);
		var evpGroupAssociate=false;
        evpGrpCtx.executeQueryAsync(function () {
				if(group.get_title().toLowerCase() == Viacom.svpGroupsPopup.Constants.svpName.toLowerCase() + " - evp"){
					evpGroupAssociate=true;
					Viacom.createSVPWebPopup.AddGroupsInEVPLibrary(evpGrpCtx,evpWeb,group);
				}
			 if(!evpGroupAssociate){
			  //Viacom.createSVPWebPopup.ActivateFeature('b10adbab-e67b-4734-afa0-e7675d12eff1');
			  Viacom.createSVPWebPopup.editGroupPermissionCustomLists();
			 }
			}, function (sender, args) {
				alert('Error while associating groups to EVP Library');
				console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
				SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
		});
		}
		catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        //SP.UI.ModalDialog.commonModalDialogClose();
        console.log('Request failed. ' + e.message);
		}		
};

Viacom.createSVPWebPopup.editGroupPermissionCustomLists = function () {
try {
        var editListPermCtx = new SP.ClientContext(Viacom.svpGroupsPopup.Constants.newSiteUrl);
        var editListPermweb = editListPermCtx.get_web();
		var editListtRootWeb = editListPermCtx.get_site().get_rootWeb();
		var groupCollection = editListtRootWeb.get_siteGroups();
		var groupName = [Viacom.svpGroupsPopup.Constants.svpName.toLowerCase() + " - svp", Viacom.svpGroupsPopup.Constants.svpName.toLowerCase() + " - evp", Viacom.svpGroupsPopup.Constants.svpName.toLowerCase() + " - managers", Viacom.svpGroupsPopup.Constants.svpName.toLowerCase() + " - super users", Viacom.svpGroupsPopup.Constants.svpName.toLowerCase() + " - team", "records management project group", Viacom.svpGroupsPopup.Constants.svpName.toLowerCase() + " - client planning"];
		var groupsArr = [];
		groupsArr.length = groupName.length;
		for (var m = 0; m < groupName.length; m++) {
		    groupsArr[m] = groupCollection.getByName(groupName[m]);
		    editListPermCtx.load(groupsArr[m]);
		}
        var oListMyTopAgencies = editListPermweb.get_lists().getByTitle('My Top Agencies');	
		var oListMyTopClients = editListPermweb.get_lists().getByTitle('My Top Clients');			
		var listExist=false;
        editListPermCtx.load(groupCollection);
		editListPermCtx.load(editListPermweb);
        editListPermCtx.executeQueryAsync(function () {
            if (!listExist) {
                for (var k = 0; k < groupsArr.length;k++){
				 listExist=true;
				 var listPerm = editListPermweb.get_roleDefinitions().getByName("TeamEdit");
				 var collListPerm = SP.RoleDefinitionBindingCollection.newObject(editListPermCtx);
                collListPerm.add(listPerm);
                oListMyTopAgencies.get_roleAssignments().add(groupsArr[k], collListPerm);
                oListMyTopClients.get_roleAssignments().add(groupsArr[k], collListPerm);
                 }
			 }
				if(!listExist){
				    //Viacom.createSVPWebPopup.ActivateFeature('b10adbab-e67b-4734-afa0-e7675d12eff1');
				    Viacom.createSVPWebPopup.AddGroupToCPDSite();
				}
					editListPermCtx.executeQueryAsync(function () {
					$('#lblStatus').html('Groups permission edited for custon lists corresponding to SVP..');
					    //Viacom.createSVPWebPopup.ActivateFeature('b10adbab-e67b-4734-afa0-e7675d12eff1');
					Viacom.createSVPWebPopup.AddGroupToCPDSite();
					}, function (sender, args) {
						alert('Error while associating groups to EVP Library');
						console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
						SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
					});
			}, function (sender, args) {
				alert('Error while associating groups to custom lists in svp web');
				console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
				SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
			});
	}
catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        console.log('Request failed. ' + e.message);
		}
};

Viacom.createSVPWebPopup.AddGroupToCPDSite = function () {
    try {
        $('#lblStatus').html('Client Planning group adding to CPD web');
        var cpdGrpCtx = new SP.ClientContext(Viacom.svpGroupsPopup.Constants.cpdUrl);
        var cpdWeb = cpdGrpCtx.get_web();
        var cpdGrpRootSiteWeb = cpdGrpCtx.get_site().get_rootWeb();
        var groupCollection = cpdGrpRootSiteWeb.get_siteGroups();
        var group = groupCollection.getByName(Viacom.svpGroupsPopup.Constants.svpName.toLowerCase() + " - client planning");
        cpdGrpCtx.load(group);
        cpdGrpCtx.load(cpdWeb);
        var cpdGroupAssociate = false;
        cpdGrpCtx.executeQueryAsync(function () {
            if (group.get_title().toLowerCase() == Viacom.svpGroupsPopup.Constants.svpName.toLowerCase() + " - client planning") {
                cpdGroupAssociate = true;
                var readPermission = cpdWeb.get_roleDefinitions().getByName("TeamRead");
                var PermColl = SP.RoleDefinitionBindingCollection.newObject(cpdGrpCtx);
                PermColl.add(readPermission);
                cpdWeb.get_roleAssignments().add(group, PermColl);
            }
            if (!cpdGroupAssociate) {
                Viacom.createSVPWebPopup.ActivateFeature('b10adbab-e67b-4734-afa0-e7675d12eff1');
            }
            else {
                    cpdGrpCtx.executeQueryAsync(function () {
                    $('#lblStatus').html('Client Planning group added to CPD web');
                    Viacom.createSVPWebPopup.ActivateFeature('b10adbab-e67b-4734-afa0-e7675d12eff1');
                }, function (sender, args) {
                    alert('Error while associating groups to CPD web');
                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                    Viacom.createSVPWebPopup.ActivateFeature('b10adbab-e67b-4734-afa0-e7675d12eff1');
                });
            }
        }, function (sender, args) {
            alert('Error while associating groups to CPD web');
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createSVPWebPopup.AddGroupsInEVPLibrary = function (evpGrpCtx,evpWeb,group) {
		try {
		    var evpLibrary = evpWeb.get_lists().getByTitle(Viacom.svpGroupsPopup.Constants.LibNmInEVpWeb);
					var listCollPerm = evpWeb.get_roleDefinitions().getByName("TeamEdit");
					var collListPerm = SP.RoleDefinitionBindingCollection.newObject(evpGrpCtx);
					collListPerm.add(listCollPerm);
					evpLibrary.get_roleAssignments().add(group, collListPerm);
					evpGrpCtx.executeQueryAsync(function () {
					$('#lblStatus').html('Groups permission edited for EVP library corresponding to SVP..');
					 Viacom.createSVPWebPopup.editGroupPermissionCustomLists();
					}, function (sender, args) {
						alert('Error while associating groups to EVP Library');
						console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
						SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
					});
			}
			catch (e) {
				SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
				//SP.UI.ModalDialog.commonModalDialogClose();
				console.log('Request failed. ' + e.message);
					}
};

Viacom.createSVPWebPopup.EditGroupsPermissionInNewWeb = function (editGrpCtx, editGrpweb, groupsArr) {
    try {
        var webTitle = Viacom.svpGroupsPopup.Constants.svpName;
		var webPrefixVal=Viacom.svpGroupsPopup.Constants.svpWebTitle;
        var oList = editGrpCtx.get_web().get_lists().getByTitle('Management');
        var oListUpFront = editGrpCtx.get_web().get_lists().getByTitle('Upfront');
        var oListScatter = editGrpCtx.get_web().get_lists().getByTitle('Scatter');
        var oListEvents = editGrpCtx.get_web().get_lists().getByTitle('Events');
        var oListAdmin = editGrpCtx.get_web().get_lists().getByTitle('Administrative');
        var oListUncategory = editGrpCtx.get_web().get_lists().getByTitle('Uncategorized');
        var oListWeeklyHighlights = editGrpCtx.get_web().get_lists().getByTitle('Weekly Highlights');
        for (var m = 0; m < Viacom.svpGroupsPopup.Constants.grpLstData.newGroups.length; m++) {
		if(!(Viacom.svpGroupsPopup.Constants.grpLstData.newGroups[m]=='Approvers' || Viacom.svpGroupsPopup.Constants.grpLstData.newGroups[m]=='Designers' || Viacom.svpGroupsPopup.Constants.grpLstData.newGroups[m]=='Hierarchy Managers' || Viacom.svpGroupsPopup.Constants.grpLstData.newGroups[m]=='NT AUTHORITY\authenticated users' || Viacom.svpGroupsPopup.Constants.grpLstData.newGroups[m]=='Quick Deploy Users')){
		var index=null;
		  for(var q=0;q<groupsArr.length;q++){
		   if(groupsArr[q].get_title().toLowerCase() == Viacom.svpGroupsPopup.Constants.grpLstData.newGroups[m].toLowerCase()){
		    index=q;
		   }
		  }
			var newPerm = editGrpweb.get_roleDefinitions().getByName(Viacom.svpGroupsPopup.Constants.grpLstData.grpPermission[m]);
            var newcollPerm = SP.RoleDefinitionBindingCollection.newObject(editGrpCtx);
            newcollPerm.add(newPerm);
            var newWebAssgn = editGrpweb.get_roleAssignments();
            var roleAssignmentContribute = newWebAssgn.add(groupsArr[index], newcollPerm);
            editGrpCtx.load(groupsArr[index]);

            if(!((groupsArr[m].get_title().toLowerCase() ==webPrefixVal.toLowerCase() + " visitors") || (groupsArr[m].get_title().toLowerCase() ==webPrefixVal.toLowerCase() + " members") || (groupsArr[m].get_title().toLowerCase() ==webPrefixVal.toLowerCase() + " owners") || (groupsArr[m].get_title().toLowerCase() =="svp owners") || (groupsArr[m].get_title().toLowerCase() =="svp members") || (groupsArr[m].get_title().toLowerCase() =="svp visitors"))){
			var listCollPerm = null;
            listCollPerm = editGrpweb.get_roleDefinitions().getByName("TeamEdit");
            if (groupsArr[m].get_title().toLowerCase() == (webTitle + " - super users").toLowerCase() || groupsArr[m].get_title().toLowerCase() == ('records management project group')) {
                listCollPerm = editGrpweb.get_roleDefinitions().getByName("TeamSuperUser");
            }
            var collListPerm = SP.RoleDefinitionBindingCollection.newObject(editGrpCtx);
            collListPerm.add(listCollPerm);
            oListUpFront.get_roleAssignments().add(groupsArr[m], collListPerm);
            oListScatter.get_roleAssignments().add(groupsArr[m], collListPerm);
            oListEvents.get_roleAssignments().add(groupsArr[m], collListPerm);
            oListAdmin.get_roleAssignments().add(groupsArr[m], collListPerm);
            oListUncategory.get_roleAssignments().add(groupsArr[m], collListPerm);
            oListWeeklyHighlights.get_roleAssignments().add(groupsArr[m], collListPerm);
			}

            if ((groupsArr[m].get_title().toLowerCase() == Viacom.svpGroupsPopup.Constants.svpName.toLowerCase() + " - svp") || (groupsArr[m].get_title().toLowerCase() == Viacom.svpGroupsPopup.Constants.svpName.toLowerCase() + " - evp") || (groupsArr[m].get_title().toLowerCase() == Viacom.svpGroupsPopup.Constants.svpName.toLowerCase() + " - managers") || (groupsArr[m].get_title().toLowerCase() == (webTitle + " - super users").toLowerCase()) || (groupsArr[m].get_title().toLowerCase() == ('records management project group'))) {
                var newListPerm = null;
                if ((groupsArr[m].get_title().toLowerCase() == Viacom.svpGroupsPopup.Constants.svpName.toLowerCase() + " - svp") || (groupsArr[m].get_title().toLowerCase() == Viacom.svpGroupsPopup.Constants.svpName.toLowerCase() + " - evp") || (groupsArr[m].get_title().toLowerCase() == Viacom.svpGroupsPopup.Constants.svpName.toLowerCase() + " - managers")) {
                    newListPerm = editGrpweb.get_roleDefinitions().getByName("TeamEdit");
                }
                if (groupsArr[m].get_title().toLowerCase() == (webTitle + " - super users").toLowerCase() || groupsArr[m].get_title().toLowerCase() == ('records management project group')) {
                    newListPerm = editGrpweb.get_roleDefinitions().getByName("TeamSuperUser");
                }
                var newcollListPerm = SP.RoleDefinitionBindingCollection.newObject(editGrpCtx);
                newcollListPerm.add(newListPerm);
                oList.get_roleAssignments().add(groupsArr[m], newcollListPerm);
            }
		 }
        }
        editGrpCtx.executeQueryAsync(function () {
            //  alert('edit group permissions');
            $('#lblStatus').html('Groups permission edited for SVP..');
            Viacom.svpGroupsPopup.Constants.grpLstData.grpInOtherSites.push(Viacom.svpGroupsPopup.Constants.svpName + " - Super Users");
            Viacom.svpGroupsPopup.Constants.adGroupsNm.push(Viacom.svpGroupsPopup.Constants.svpName + " - Super Users");
            Viacom.createSVPWebPopup.CreateGrpInOtherSites(_spPageContextInfo.siteAbsoluteUrl + "/sites/clients", "TeamRead", "clients");
        }, function (sender, args) {
            console.log('Request failed at edit permission function. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
            //SP.UI.ModalDialog.commonModalDialogClose();
        });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        //SP.UI.ModalDialog.commonModalDialogClose();
        console.log('Exception at edit permission function. ' + e.message);
    }
};

Viacom.createSVPWebPopup.CreateGrpInOtherSites = function (sitesUrl, grpPerm, siteCol) {
    try {
        var grpforOtherSitesCtx = new SP.ClientContext(sitesUrl);
        var grpforOtherSitesWeb = grpforOtherSitesCtx.get_web();
        grpforOtherSitesCtx.load(grpforOtherSitesWeb);
        grpforOtherSitesCtx.executeQueryAsync(function () {
            var groupCollection = grpforOtherSitesWeb.get_siteGroups();
            var membersGRP = new SP.GroupCreationInformation();
            for (var m = 0; m < Viacom.svpGroupsPopup.Constants.grpLstData.grpInOtherSites.length; m++) {
                membersGRP.set_title(Viacom.svpGroupsPopup.Constants.grpLstData.grpInOtherSites[m]);
                membersGRP.set_description("SVP's Group for Web: " + Viacom.svpGroupsPopup.Constants.grpLstData.grpInOtherSites[m]);
                var oMembersGRP = groupCollection.add(membersGRP);
                var gpPermission = null;
                if (Viacom.svpGroupsPopup.Constants.grpLstData.grpInOtherSites[m] == Viacom.svpGroupsPopup.Constants.svpName + " - Super Users") {
                    gpPermission = grpforOtherSitesWeb.get_roleDefinitions().getByName("TeamSuperUser");
                }
                else {
                    gpPermission = grpforOtherSitesWeb.get_roleDefinitions().getByName(grpPerm);
                }
                var collPerm = SP.RoleDefinitionBindingCollection.newObject(grpforOtherSitesCtx);
                collPerm.add(gpPermission);
                var assignments = grpforOtherSitesWeb.get_roleAssignments();
                var roleAssignmentContribute = assignments.add(oMembersGRP, collPerm);
                grpforOtherSitesCtx.load(oMembersGRP);
            }
            grpforOtherSitesCtx.executeQueryAsync(function () {
                if (siteCol == "clients") {
                    Viacom.createSVPWebPopup.CreateGrpInOtherSites(_spPageContextInfo.siteAbsoluteUrl + "/sites/agencies", "TeamRead", "agencies");
                }
                if (siteCol == "agencies") {
                    Viacom.createSVPWebPopup.GetItemfromSVPData();
                }
            }, function (sender, args) {
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
            });
        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        })
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createSVPWebPopup.GetItemfromSVPData = function () {
    var itemId = null;
    var svpDataCtx = SP.ClientContext.get_current();
    var root = svpDataCtx.get_site().get_rootWeb();
    var oList = root.get_lists().getByTitle("SVP Data");
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml("<View><Query><Where><Contains><FieldRef Name='TeamName'/><Value Type='TaxonomyFieldType'>" + Viacom.svpGroupsPopup.Constants.svpName + "</Value></Contains></Where></Query></View>");
    var collListItem = oList.getItems(camlQuery);
    svpDataCtx.load(collListItem);
    svpDataCtx.executeQueryAsync(
                        function (sender, args) {
                            var listItemInfo = '';
                            var listItemEnumerator = collListItem.getEnumerator();
                            try {
                                while (listItemEnumerator.moveNext()) {
                                    var oListItem = listItemEnumerator.get_current();
                                    itemId = oListItem.get_item('ID');
                                    var svpName = new SP.FieldUrlValue();
                                    var MgrName = new SP.FieldUrlValue();
                                    var evpName = new SP.FieldUrlValue();
                                    var teamName = new SP.FieldUrlValue();
                                    var cpdName = new SP.FieldUrlValue();
                                    for (var m = 0; m < Viacom.svpGroupsPopup.Constants.grpNmToSetInLst.length; m++) {
                                        if (Viacom.svpGroupsPopup.Constants.grpNmToSetInLst[m].indexOf(Viacom.svpGroupsPopup.Constants.svpName + " - SVP") >= 0) {
                                            oListItem.set_item('SVP', SP.FieldUserValue.fromUser(Viacom.svpGroupsPopup.Constants.grpNmToSetInLst[m]));
                                        }
                                        else if (Viacom.svpGroupsPopup.Constants.grpNmToSetInLst[m].indexOf(Viacom.svpGroupsPopup.Constants.svpName + " - EVP") >= 0) {
                                            oListItem.set_item('Managers', SP.FieldUserValue.fromUser(Viacom.svpGroupsPopup.Constants.grpNmToSetInLst[m]));
                                        }
                                        else if (Viacom.svpGroupsPopup.Constants.grpNmToSetInLst[m].indexOf(Viacom.svpGroupsPopup.Constants.svpName + " - Team") >= 0) {
                                            oListItem.set_item('Team', SP.FieldUserValue.fromUser(Viacom.svpGroupsPopup.Constants.grpNmToSetInLst[m]));
                                        }
                                        else if (Viacom.svpGroupsPopup.Constants.grpNmToSetInLst[m].indexOf(Viacom.svpGroupsPopup.Constants.svpName + " - Managers") >= 0) {
                                            oListItem.set_item('CPD', SP.FieldUserValue.fromUser(Viacom.svpGroupsPopup.Constants.grpNmToSetInLst[m]));
                                        }
                                        else if (Viacom.svpGroupsPopup.Constants.grpNmToSetInLst[m].indexOf(Viacom.svpGroupsPopup.Constants.svpName + " - Client Planning") >= 0) {
                                            oListItem.set_item('EVP', SP.FieldUserValue.fromUser(Viacom.svpGroupsPopup.Constants.grpNmToSetInLst[m]));
                                        }
                                    }
                                    oListItem.update();
                                    break;
                                }
                                svpDataCtx.executeQueryAsync(function (sender, args) {
                                    $('#lblStatus').html('Groups updated in SVP Data list..');
                                   // Viacom.createSVPWebPopup.AssociateAdGroups(_spPageContextInfo.siteAbsoluteUrl, Viacom.svpGroupsPopup.Constants.adGroupsNm[0].toLowerCase(), "svp");
                                    // Viacom.createSVPWebPopup.ActivateFeature('b10adbab-e67b-4734-afa0-e7675d12eff1');
                                    Viacom.createSVPWebPopup.GetDomainValue();
                                }, function (sender, args) {
                                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                                    SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
                                });
                            }
                            catch (err) {
                                SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
                                return false;
                            }
                        }, function (sender, args) {
                            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                            SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
                        });
};

Viacom.createSVPWebPopup.GetUserToDeleteForSVPWeb = function (){
try {
        var getRootGrp = new SP.ClientContext(Viacom.svpGroupsPopup.Constants.newSiteUrl);
        var getSVWweb = getRootGrp.get_web();
        var currentUser = getRootGrp.get_web().get_currentUser();		
		getRootGrp.load(getSVWweb);
        getRootGrp.load(currentUser);
        getRootGrp.executeQueryAsync(function () {
			 Viacom.createSVPWebPopup.DeleteCurrentUserFromSVP(getRootGrp, getSVWweb,currentUser);
			}, function (sender, args) {
						alert('Error while getting current user from SVP Web');
						console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
						SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
			   });
	}
catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        console.log('Request failed. ' + e.message);
		}
};

Viacom.createSVPWebPopup.DeleteCurrentUserFromSVP = function(getRootGrp, getSVWweb,currentUser){
try {					
			var userId=currentUser.get_id();
			var delUserFromWeb = getSVWweb.get_roleAssignments();
                delUserFromWeb = delUserFromWeb.getByPrincipalId(userId);
                delUserFromWeb.deleteObject();
                getRootGrp.load(delUserFromWeb);
				
			var oListManage = getSVWweb.get_lists().getByTitle('Management');
			var oListDoc = getSVWweb.get_lists().getByTitle('Documents');
			var oListUpFront = getSVWweb.get_lists().getByTitle('Upfront');
			var oListScatter = getSVWweb.get_lists().getByTitle('Scatter');
			var oListEvents = getSVWweb.get_lists().getByTitle('Events');
			var oListAdmin = getSVWweb.get_lists().getByTitle('Administrative');
			var oListUncategory = getSVWweb.get_lists().getByTitle('Uncategorized');
			var oListWeeklyHighlights = getSVWweb.get_lists().getByTitle('Weekly Highlights');
			var oListMyTopAgencies = getSVWweb.get_lists().getByTitle('My Top Agencies');
			var oListMyTopClients= getSVWweb.get_lists().getByTitle('My Top Clients');
			
				    var delUserFromManage = oListManage.get_roleAssignments();
				     delUserFromManage = delUserFromManage.getByPrincipalId(userId);
					 delUserFromManage.deleteObject();					
					var delUserFromUpFront = oListUpFront.get_roleAssignments();
                     delUserFromUpFront = delUserFromUpFront.getByPrincipalId(userId);
                     delUserFromUpFront.deleteObject();
					var delUserFromScatter = oListScatter.get_roleAssignments();
                     delUserFromScatter = delUserFromScatter.getByPrincipalId(userId);
                     delUserFromScatter.deleteObject();
					var delUserFromEvents = oListEvents.get_roleAssignments();
                     delUserFromEvents = delUserFromEvents.getByPrincipalId(userId);
                     delUserFromEvents.deleteObject();
					var delUserFromAdmin = oListAdmin.get_roleAssignments();
                     delUserFromAdmin = delUserFromAdmin.getByPrincipalId(userId);
                     delUserFromAdmin.deleteObject();
					var delUserFromUncategory = oListUncategory.get_roleAssignments();
                     delUserFromUncategory = delUserFromUncategory.getByPrincipalId(userId);
                     delUserFromUncategory.deleteObject();
					var delUserFromWeek = oListWeeklyHighlights.get_roleAssignments();
                     delUserFromWeek = delUserFromWeek.getByPrincipalId(userId);
                     delUserFromWeek.deleteObject();
					var delUserFromTopAg= oListMyTopAgencies.get_roleAssignments();
						delUserFromTopAg = delUserFromTopAg.getByPrincipalId(userId);
						delUserFromTopAg.deleteObject();
					var delUserFromTopCl = oListMyTopClients.get_roleAssignments();
						delUserFromTopCl = delUserFromTopCl.getByPrincipalId(userId);
						delUserFromTopCl.deleteObject();	
					var delUserFromDoc = oListDoc.get_roleAssignments();
						delUserFromDoc = delUserFromDoc.getByPrincipalId(userId);
						delUserFromDoc.deleteObject();	
					 
				getRootGrp.load(delUserFromManage);
				getRootGrp.load(delUserFromUpFront);
				getRootGrp.load(delUserFromScatter);
				getRootGrp.load(delUserFromEvents);
				getRootGrp.load(delUserFromAdmin);
				getRootGrp.load(delUserFromUncategory);
				getRootGrp.load(delUserFromWeek);
				getRootGrp.load(delUserFromTopAg);
				getRootGrp.load(delUserFromTopCl);
				getRootGrp.load(delUserFromDoc);
				getRootGrp.executeQueryAsync(function () {
				  SP.UI.ModalDialog.commonModalDialogClose(SP.UI.Dialog.OK, 'Success!');
                  }, function (sender, args) {
                        console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
							//alert('Error while deleting user from created web and its list');
							SP.UI.ModalDialog.commonModalDialogClose(SP.UI.Dialog.OK, 'Success!');
                 });
	}
catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        console.log('Request failed. ' + e.message);
		}
};

Viacom.createSVPWebPopup.ActivateFeature = function (featureGuid) {
    try {
        var clientContext = new SP.ClientContext(Viacom.svpGroupsPopup.Constants.newSiteUrl);
        var web = clientContext.get_web();
        var guid = new SP.Guid(featureGuid);
        var featDef = web.get_features().add(guid, true, SP.FeatureDefinitionScope.None);
        clientContext.executeQueryAsync(function () {
            //alert('Success!');
            $('#lblStatus').html('Default value feature activated for svp web..');
            //window.frameElement.cancelPopUp();
            if (featureGuid == 'b10adbab-e67b-4734-afa0-e7675d12eff1') {
                Viacom.createSVPWebPopup.ActivateFeature('bc0fb689-0147-48d0-ad60-d405497595e2')
            }
            else if (featureGuid == 'bc0fb689-0147-48d0-ad60-d405497595e2') {
                Viacom.createSVPWebPopup.ActivateFeature('842ae8aa-f9b7-47d3-be14-928c54363049')
            }
            else {
                $('#lblStatus').html('Navigation feature activated for svp web..');
				Viacom.createSVPWebPopup.GetUserToDeleteForSVPWeb();
                //SP.UI.ModalDialog.commonModalDialogClose(SP.UI.Dialog.OK, 'Success!');
            }
        }, function (sender, args) {
            alert('feature activation problem');
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            SP.UI.ModalDialog.commonModalDialogClose(1, 'Success!');
        });
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
    }
}

Viacom.createSVPWebPopup.EditDeactivateFeature = function () {
    try {
        var EditClientAgencyDeactivateclientContext = new SP.ClientContext(Viacom.svpGroupsPopup.Constants.newSiteUrl);
        $('#lblStatus').html('Deactiavting Default Value Feature');
        var Deactivateweb = EditClientAgencyDeactivateclientContext.get_web();
        var DeactivateGuid = new SP.Guid('0e7e0863-72ba-4a9a-aa19-b99031ebf607');
        var DeactivatefeatDef = Deactivateweb.get_features().remove(DeactivateGuid, true);

        EditClientAgencyDeactivateclientContext.executeQueryAsync(function () {
            // alert('Success!');
            Viacom.createSVPWebPopup.EditActivateFeature()
        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            $('#lblStatus').html('Not Able to Deactiavting Default Value Feature,Please confirm Feature is activated on target client site');
			SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        })
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
	}
}

Viacom.createSVPWebPopup.EditActivateFeature = function () {
    try {
        var EditClientAgencyActivateclientContext = new SP.ClientContext(Viacom.svpGroupsPopup.Constants.newSiteUrl);
        $('#lblStatus').html('Activating Default Value Feature');
        var activateweb = EditClientAgencyActivateclientContext.get_web();
        var activateGuid = new SP.Guid('0e7e0863-72ba-4a9a-aa19-b99031ebf607');
        var activatefeatDef = activateweb.get_features().add(activateGuid, true, SP.FeatureDefinitionScope.None);
        EditClientAgencyActivateclientContext.executeQueryAsync(function () {
            // alert('Success!');
			Viacom.svpGroupsPopup.Constants.EditClientAgencySiteUrlArr=Viacom.svpGroupsPopup.Constants.EditClientAgencySiteUrl.split(',')
				if (Viacom.svpGroupsPopup.Constants.editClientAgencyCount < Viacom.svpGroupsPopup.Constants.EditClientAgencySiteUrlArr.length) {
				Viacom.svpGroupsPopup.Constants.editClientAgencyCount++;
				Viacom.createSVPWebPopup.EditClientAgencyTopClientItemDeletion(Viacom.svpGroupsPopup.Constants.EditClientAgencySiteUrlArr[Viacom.svpGroupsPopup.Constants.editClientAgencyCount-1]);
				}
            
        }, function (sender, args) {
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            $('#lblStatus').html('Not Able to Actiavting Default Value Feature');
			SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
        })
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
        SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
    }
}

Viacom.createSVPWebPopup.EditClientAgencyTopClientItemDeletion=function (editclientAgencyUrl ){
try {
		var EditAgencyClientContext = new SP.ClientContext(editclientAgencyUrl);
		$('#lblStatus').html('Deleting Agency Top Clients Data');
        var web = EditAgencyClientContext.get_web();
        var list = web.get_lists().getByTitle('Top Clients');

        var ClientNameAgencyTypeCaml = new SP.CamlQuery();
		var EditClientAbsURL=Viacom.svpGroupsPopup.Constants.newSiteUrl.replace(_spPageContextInfo.siteAbsoluteUrl,'');
        ClientNameAgencyTypeCaml.set_viewXml("<View><Query><Where><BeginsWith><FieldRef Name='ClientSiteURL'/><Value Type='URL'>"+EditClientAbsURL+"</Value></BeginsWith></Where></Query></View>");
        var ClientNameItems = list.getItems(ClientNameAgencyTypeCaml);


        EditAgencyClientContext.load(ClientNameItems);
        EditAgencyClientContext.executeQueryAsync(function () {
		    var enumeratorClientNameType = ClientNameItems.getEnumerator();
            while (enumeratorClientNameType.moveNext()) {
                var ClientNameTypeItem = enumeratorClientNameType.get_current();
				ClientNameTypeItem.deleteObject();
				}
				EditAgencyClientContext.executeQueryAsync(function () {
				if (Viacom.svpGroupsPopup.Constants.editClientAgencyCount < Viacom.svpGroupsPopup.Constants.EditClientAgencySiteUrlArr.length) {
				$('#lblStatus').html('Deleting Agency site top client data');
					Viacom.svpGroupsPopup.Constants.editClientAgencyCount++;
					Viacom.createSVPWebPopup.EditClientAgencyTopClientItemDeletion(Viacom.svpGroupsPopup.Constants.EditClientAgencySiteUrlArr[Viacom.svpGroupsPopup.Constants.editClientAgencyCount-1]);
				}
				else {
				 SP.UI.ModalDialog.commonModalDialogClose(SP.UI.Dialog.OK, 'Success!');
				 location.reload();
				}},
				function (sender, args) {
				console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
				$('#lblStatus').html('Not Able to Delete Agency site client data');
				SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
				});
				},
            function (sender, args) {
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            $('#lblStatus').html('Not Able to Delete Agency site top client data');
			SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
            });
		}
		
    catch (e) {
        console.log('Request failed. ' + e.message);
		SP.UI.ModalDialog.commonModalDialogClose(0, 'Fail!');
    }
}

