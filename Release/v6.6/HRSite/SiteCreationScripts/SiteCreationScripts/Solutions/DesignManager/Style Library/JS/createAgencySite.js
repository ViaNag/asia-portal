﻿var Viacom = Viacom || {};
Viacom.agencyGroups = {};
Viacom.createAgencyWeb = Viacom.createAgencyWeb || {};
Viacom.agencySiteTaxonomy = Viacom.agencySiteTaxonomy || {};
Viacom.agencyGroups.Constants = function () {
    return {
        mmsName: null,
        objTaxAgency: {},
        agencyCounter: null,
        newSiteUrl: null,
        objAgencyPopup: null
    }
};

//$(document).ready(function () {
//    //For browser which don't support toISoString method
//    if (!Date.prototype.toISOString) {
//        (function () {
//            function pad(number) {
//                var r = String(number);
//                if (r.length === 1) {
//                    r = '0' + r;
//                }
//                return r;
//            }
//            Date.prototype.toISOString = function () {
//                return this.getUTCFullYear()
//                    + '-' + pad(this.getUTCMonth() + 1)
//                    + '-' + pad(this.getUTCDate())
//                    + 'T' + pad(this.getUTCHours())
//                    + ':' + pad(this.getUTCMinutes())
//                    + ':' + pad(this.getUTCSeconds())
//                    + '.' + String((this.getUTCMilliseconds() / 1000).toFixed(3)).slice(2, 5)
//                    + 'Z';
//            };
//        }());
//    }
//    var scriptbase = _spPageContextInfo.webServerRelativeUrl + "_layouts/15/";
//    $.getScript(scriptbase + "SP.Runtime.js",
//        function () {
//            $.getScript(scriptbase + "SP.js", function () {
//                $.getScript(scriptbase + "SP.Taxonomy.js", Viacom.agencyGroups.Initialize());
//            });
//        }
//    );
//});

Viacom.agencyGroups.Initialize = function () {
    Viacom.agencyGroups.Constants.siteLevel = "Agency";
    Viacom.agencyGroups.Constants.lstGrp = "Site Groups Mapping";
    Viacom.agencyGroups.Constants.mmsName = "Managed Metadata Service";
    Viacom.agencyGroups.Constants.agencyCounter = null;
    Viacom.agencyGroups.Constants.newSiteUrl = null;
    Viacom.agencyGroups.Constants.objTaxAgency = {};
    Viacom.agencyGroups.Constants.objAgencyPopup = {};
    Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyNameValue = [];
    Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyIDValue = [];
    Viacom.agencyGroups.Constants.validateAgencyName = false;
    Viacom.agencyGroups.Constants.previousAgencyName = null;
    Viacom.agencyGroups.Constants.newSiteUrl = null;
    Viacom.agencyGroups.Constants.objTaxAgency.taxAgencySVPTeam = [];
    var preUrl = _spPageContextInfo.siteAbsoluteUrl + "/sites/agencies/";
    $('#preAgencyUrl').html(preUrl);
    $('#preAgencyUrlEdit').html(preUrl);
};

Viacom.createAgencyWeb.ValidateFields = function (Action) {
    try {

        if (Action != "create") {
            var taxAgencyName = $("span[id*='TaxonomyAgencyNameEdit']")[0].children[0];

            if (taxAgencyName.hasAttribute('value') && (taxAgencyName.value.length > 0) && taxAgencyName.value.split('|')[1] != '00000000-0000-0000-0000-000000000000') {
                Viacom.agencyGroups.Constants.validateAgencyName = true;
                if (Action == "populate") {
                    Viacom.agencyGroups.Constants.previousAgencyName = taxAgencyName.value;
                    return true;
                }
            }
            else {
                alert('Please select a valid agency');
                return false;
            }


            if (Action == "edit") {
                //var taxAgencyId = $("#agencyIdEdit").val();
                var taxAgencyNm = $("span[id*='TaxonomyAgencyNameEdit']")[0].children[0].value;
                var taxAgencyUrl = $("#preAgencyUrlEdit").val();
                //var taxSvpTm = Viacom.agencyGroups.Constants.objTaxAgency.taxAgencySVPTeam.length;


                if (taxAgencyNm.length > 0 && taxAgencyUrl.length > 0 && Viacom.agencyGroups.Constants.validateAgencyName) {

                    if (taxAgencyName.value != Viacom.agencyGroups.Constants.previousAgencyName) {
                        alert('Please click on search icon to fetch the details');
                        return false;
                    }
                    return true;
                }
                else {
                    alert("Please fill all the required fields")
                    return false;

                }

            }
        }

        var taxAgencyId = $('#agencyId').val();
        var taxAgencyNm = $('#agencyNm').val();
        var taxAgencyUrl = Viacom.agencyGroups.Constants.newSiteUrl.split("/sites/agencies")[1];
        //var taxSvpTm = Viacom.agencyGroups.Constants.objTaxAgency.taxAgencySVPTeam.length;
        if (taxAgencyNm.length > 0 && taxAgencyUrl != null) {
            if (taxAgencyUrl.length > 0) {
                return true;
            }
        }
        else {
            alert("Please fill all the required fields");
            return false;
        }
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error while doing validation for agency fields");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createAgencyWeb.CreateItemInList = function () {
    try {

        Viacom.createAgencyWeb.CallToCreateItemInSiteProvisioning();
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error while making call to create item in list");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createAgencyWeb.CallToCreateItemInSiteProvisioning = function () {
    try {
        var AgId = $('#agencyId').val();
        AgId = AgId.trim();

        var insertItems = [[]];
        //Viacom.agencyGroups.Constants.newSiteUrl = _spPageContextInfo.siteAbsoluteUrl + "/sites/agencies/" + $("#agencyWebUrl").val();
        insertItems[0] = ["MMD", "AgencyName", Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyNameValue[1]];
        insertItems[1] = ["SLT", "SiteURL", Viacom.agencyGroups.Constants.newSiteUrl];
        //insertItems[2] = ["MMD", "TeamName", Viacom.agencyGroups.Constants.objTaxAgency.taxAgencySVPTeam[1]];

        insertItems[2] = ["Choice", "SiteType", "AgencySite"];
        insertItems[3] = ["Choice", "ActionType", "CreateSite"];

        if (AgId.length > 0) {
            insertItems[4] = ["MMD", "AgencyID", Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyIDValue[1]];
        }

        //insertItems[5] = ["ContentType", "ContentType", "Agency Site Provision"];
        //listItem.set_item('', myContentType.get_id());

        //Viacom.createAgencyWeb.MakeEntryInList("Site Provisioning", arrObj);

        var clientContext = SP.ClientContext.get_current();
        var root = clientContext.get_site().get_rootWeb();
        var oList = root.get_lists().getByTitle("Site Provisioning");
        var listContentTypes = oList.get_contentTypes();

        clientContext.load(listContentTypes);

        var item = oList.addItem();
        for (var i = 0; i < insertItems.length; i++) {
            switch (insertItems[i][0]) {
                case "User": {
                    if (insertItems[i][2] != undefined && insertItems[i][2] != null)
                        item.set_item(insertItems[i][1], SP.FieldUserValue.fromUser(insertItems[i][2]));
                    break;
                }
                case "HyperLink": {
                    if (insertItems[i][2] != undefined && insertItems[i][2] != null) {
                        var rootSiteUrl = new SP.FieldUrlValue();
                        rootSiteUrl.set_url(insertItems[i][2]);
                        rootSiteUrl.set_description(insertItems[i][2]);
                        item.set_item(insertItems[i][1], rootSiteUrl);
                    }
                    break;
                }
                case "Date": {
                    if (insertItems[i][2] != undefined && insertItems[i][2] != null) {
                        var iDt = insertItems[i][2].toISOString();
                        item.set_item(insertItems[i][1], iDt);
                    }
                    break;
                }
                default: {
                    if (insertItems[i][2] != undefined && insertItems[i][2] != null)
                        item.set_item(insertItems[i][1], insertItems[i][2]);
                }
            }
        }
        item.update();
        clientContext.executeQueryAsync(function () {
            var ct_enumerator = listContentTypes.getEnumerator();
            while (ct_enumerator.moveNext()) {
                var ct = ct_enumerator.get_current();
                if (ct.get_name().toString() == 'Agency Site Provisioning') {
                    //we've got our content type, now let's get its name
                    item.set_item('ContentTypeId', ct.get_id().toString());
                    item.update();

                    clientContext.executeQueryAsync(function () {
                        SP.UI.ModalDialog.commonModalDialogClose();
                        alert("Site Creation initiated successfully");
                        location.reload();
                    }, function (sender, args) {
                        SP.UI.ModalDialog.commonModalDialogClose();
                        alert("Error in while updating list item");
                        console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                    });
                }
            }
            //SP.UI.ModalDialog.commonModalDialogClose();
            //alert("Site Creation initiated successfully");
        }, function (sender, args) {
            SP.UI.ModalDialog.commonModalDialogClose();
            alert("Error in either creating iitem in 'Sites' list or making call top open another popup");
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        });



    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error while making call to create item in provisioning list");
        console.log('Request failed. ' + e.message);
    }
}

Viacom.createAgencyWeb.getItemFromConfigLists = function (lstName, query, beforeEntryInSiteList) {
    try {
        var clientContext = SP.ClientContext.get_current();
        var root = clientContext.get_site().get_rootWeb();
        var oList = root.get_lists().getByTitle(lstName);
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml(query);
        var collListItem = oList.getItems(camlQuery);
        clientContext.load(collListItem);
        clientContext.executeQueryAsync(
              function (sender, args) {
                  if (collListItem.get_count() != 0) {
                      var listItemInfo = '';
                      var listItemEnumerator = collListItem.getEnumerator();
                      try {
                          while (listItemEnumerator.moveNext()) {
                              var oListItem = listItemEnumerator.get_current();
                              Viacom.agencyGroups.Constants.agencyCounter = oListItem.get_item('Counter');
                              Viacom.agencyGroups.Constants.agencyCounter += 1;
                              break;
                          }
                      }
                      catch (err) {
                          SP.UI.ModalDialog.commonModalDialogClose();
                          return false;
                      }
                  }
                  else {
                      Viacom.agencyGroups.Constants.agencyCounter = 1;
                  }
                  Viacom.createAgencyWeb.CallToCreateItemInLists(lstName, beforeEntryInSiteList);
              }, function (sender, args) {
                  console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace())
              });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createAgencyWeb.CallToCreateItemInLists = function (lstName, beforeEntryInSiteList) {
    try {
        var AgId = $('#agencyId').val();
        AgId = AgId.trim();
        if (lstName == "Agency Data" && beforeEntryInSiteList) {
            var arrObj = [[]];
            //Viacom.agencyGroups.Constants.newSiteUrl = _spPageContextInfo.siteAbsoluteUrl + "/sites/agencies/agency" + Viacom.agencyGroups.Constants.agencyCounter.toString();
            Viacom.agencyGroups.Constants.newSiteUrl = _spPageContextInfo.siteAbsoluteUrl + "/sites/agencies/" + $("#agencyWebUrl").val();
            arrObj[0] = ["MMD", "AgencyName", Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyNameValue[1]];
            arrObj[1] = ["SLT", "AgencyUrl", Viacom.agencyGroups.Constants.newSiteUrl];
            arrObj[2] = ["MMD", "TeamName", Viacom.agencyGroups.Constants.objTaxAgency.taxAgencySVPTeam[1]];
            if (AgId.length > 0) {
                arrObj[3] = ["MMD", "AgencyID", Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyIDValue[1]];
            }
            Viacom.createAgencyWeb.MakeEntryInList("Agency Data", arrObj);
        }
        else if (lstName == "Sites" && (!beforeEntryInSiteList)) {
            var sitesArrObj = [[]];
            sitesArrObj[0] = ["Choice", "SiteLevel", "Agency"];
            sitesArrObj[1] = ["HyperLink", "RootSiteCollectionURL", _spPageContextInfo.siteAbsoluteUrl + "/sites/agencies"];
            sitesArrObj[2] = ["SLT", "SiteValue", Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyNameValue[0]];
            sitesArrObj[3] = ["SLT", "SiteURL", Viacom.agencyGroups.Constants.newSiteUrl];
            sitesArrObj[4] = ["Date", "SiteCreationDate", new Date()];
            Viacom.createAgencyWeb.MakeEntryInList("Sites", sitesArrObj);
            // Viacom.createAgencyWeb.GetWebTemplates();
        }
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error while preparing data to create item in list");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createAgencyWeb.MakeEntryInList = function (lstName, insertItems) {
    try {
        var clientContext = SP.ClientContext.get_current();
        var root = clientContext.get_site().get_rootWeb();
        var oList = root.get_lists().getByTitle(lstName);
        var item = oList.addItem();
        for (var i = 0; i < insertItems.length; i++) {
            switch (insertItems[i][0]) {
                case "User": {
                    if (insertItems[i][2] != undefined && insertItems[i][2] != null)
                        item.set_item(insertItems[i][1], SP.FieldUserValue.fromUser(insertItems[i][2]));
                    break;
                }
                case "HyperLink": {
                    if (insertItems[i][2] != undefined && insertItems[i][2] != null) {
                        var rootSiteUrl = new SP.FieldUrlValue();
                        rootSiteUrl.set_url(insertItems[i][2]);
                        rootSiteUrl.set_description(insertItems[i][2]);
                        item.set_item(insertItems[i][1], rootSiteUrl);
                    }
                    break;
                }
                case "Date": {
                    if (insertItems[i][2] != undefined && insertItems[i][2] != null) {
                        var iDt = insertItems[i][2].toISOString();
                        item.set_item(insertItems[i][1], iDt);
                    }
                    break;
                }
                default: {
                    if (insertItems[i][2] != undefined && insertItems[i][2] != null)
                        item.set_item(insertItems[i][1], insertItems[i][2]);
                }
            }
        }
        item.update();
        clientContext.executeQueryAsync(function () {
            if (lstName == "Agency Data") {
                Viacom.createAgencyWeb.CallToCreateItemInLists("Sites", false);
            }
            if (lstName == "Sites") {
                Viacom.agencyGroups.Constants.objAgencyPopup.agencyNm = Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyNameValue[0];
                Viacom.createAgencyWeb.openDialog(Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyNameValue[0], Viacom.agencyGroups.Constants.newSiteUrl, Viacom.agencyGroups.Constants.objTaxAgency.taxAgencySVPTeam[0]);
            }
        }, function (sender, args) {
            SP.UI.ModalDialog.commonModalDialogClose();
            alert("Error in either creating iitem in 'Sites' list or making call top open another popup");
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error in creating item in master list");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createAgencyWeb.chkAgencyIDTerm = function (termName, termSetName, groupName, mmsName) {
    var termExist = false;
    try {
        termName = termName.trim();
        if (termName.length > 0) {
            var taxonomyContext = new SP.ClientContext.get_current();
            //var session = SP.Taxonomy.TaxonomySession.getTaxonomySession(taxonomyContext);
            //var termStores = session.get_termStores();
            //var termStore = termStores.getByName(mmsName);
            var session = SP.Taxonomy.TaxonomySession.getTaxonomySession(taxonomyContext);
            var termStore = session.getDefaultSiteCollectionTermStore();
            var group = termStore.get_groups().getByName(groupName);
            var termSet = group.get_termSets().getByName(termSetName);
            var termObj = termSet.get_terms().getByName(termName);
            taxonomyContext.load(termObj);
            taxonomyContext.executeQueryAsync(function () {
                if (termObj.get_name().toLowerCase() == termName.toLowerCase()) {
                    termExist = true;
                    Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyIDValue.push(termObj.get_name());
                    Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyIDValue.push(termObj.get_id()._m_guidString$p$0);
                }
                var query = "<View><Query><Where><Contains><FieldRef Name='AgencyName'/><Value Type='TaxonomyFieldType'>";
                query = query + $("#agencyNm").val() + "</Value></Contains></Where>";
                query = query + "</Query></View>";
                Viacom.createAgencyWeb.validationForExistingterms("Site Provisioning", query, true);
                //Viacom.createAgencyWeb.CreateItemInList();
            }, function (sender, args) {
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                if (!termExist && args.get_errorCode() == -2146233086) {
                    var errorCtx = new SP.ClientContext.get_current();
                    var errorSession = SP.Taxonomy.TaxonomySession.getTaxonomySession(errorCtx);
                    //var errorTermStore = errorSession.get_termStores();
                    //var errorTermStore = errorTermStore.getByName(mmsName);
                    var errorTermStore = errorSession.getDefaultSiteCollectionTermStore();
                    var errorgroup = errorTermStore.get_groups().getByName(groupName);
                    var errorTermSet = errorgroup.get_termSets().getByName(termSetName);
                    var errornewTerm = errorTermSet.createTerm(termName, 1033, SP.Guid.newGuid()._m_guidString$p$0);
                    errorCtx.load(errornewTerm);
                    errorCtx.executeQueryAsync(function () {
                        //  alert("Term Created: " + errornewTerm.get_name());
                        Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyIDValue.push(errornewTerm.get_name());
                        Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyIDValue.push(errornewTerm.get_id()._m_guidString$p$0);
                        var query = "<View><Query><Where><Contains><FieldRef Name='AgencyName'/><Value Type='TaxonomyFieldType'>";
                        query = query + $("#agencyNm").val() + "</Value></Contains></Where>";
                        query = query + "</Query></View>";
                        Viacom.createAgencyWeb.validationForExistingterms("Site Provisioning", query, true);
                        //Viacom.createAgencyWeb.CreateItemInList();
                    }, function (sender, args) {
                        SP.UI.ModalDialog.commonModalDialogClose();
                        alert("Error while creating new term for Agency ID");
                        console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                    });
                }
            });
        }
        else {
            var query = "<View><Query><Where><Contains><FieldRef Name='AgencyName'/><Value Type='TaxonomyFieldType'>";
            query = query + $("#agencyNm").val() + "</Value></Contains></Where>";
            query = query + "</Query></View>";
            Viacom.createAgencyWeb.validationForExistingterms("Site Provisioning", query, true);
        }
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error while checking whether giving Agency ID already exist or not");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createAgencyWeb.getTaxWebTaggingValue = function () {
    try {
        var taxAgencySVPTm = $("span[id*='ddlTaxAgencySVPTm']")[0].children[0];
        if (taxAgencySVPTm.hasAttribute('value')) {
            Viacom.agencyGroups.Constants.objTaxAgency.taxAgencySVPTeam = taxAgencySVPTm.getAttribute('value').split('|');
        }
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error in reading value for web tagging control");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createAgencyWeb.validationForExistingterms = function (lstName, query, agencyTerm) {
    try {
        var ctxUrl = window.location.protocol + "//" + window.location.host + _spPageContextInfo.siteServerRelativeUrl;
        var clientContext = new SP.ClientContext(ctxUrl);
        var root = clientContext.get_site().get_rootWeb();
        var oList = root.get_lists().getByTitle(lstName);
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml(query);
        var collListItem = oList.getItems(camlQuery);
        clientContext.load(collListItem);
        var tempAgencyArr = [];
        var tempAgencyUrlArr = [];
        clientContext.executeQueryAsync(
              function (sender, args) {
                  if (collListItem.get_count() != 0) {
                      var listItemInfo = '';
                      var listItemEnumerator = collListItem.getEnumerator();
                      try {
                          while (listItemEnumerator.moveNext()) {
                              var oListItem = listItemEnumerator.get_current();
                              var agencyNm = oListItem.get_item('AgencyName').$0_1;
                              var agencyUrl = oListItem.get_item('SiteURL');
                              var SiteType = oListItem.get_item('SiteType');
                              var ActionType = oListItem.get_item('ActionType');
                              tempAgencyArr.push(agencyNm);
                              var splitUrl = agencyUrl.split("/sites/agencies/");
                              tempAgencyUrlArr.push(splitUrl[1]);
                          }
                          var agencyExist = false;
                          if (SiteType == "AgencySite" && ActionType == "CreateSite")
                          {
                          for (var m = 0; m < tempAgencyArr.length; m++) {
                              if (tempAgencyArr[m].toLowerCase() == $("#agencyNm").val().toLowerCase()) {
                                  agencyExist = true;
                                  SP.UI.ModalDialog.commonModalDialogClose();
                                  alert("Site with similiar agency name: '" + tempAgencyArr[m] + "' already exists. Please choose another name.");
                                  break;
                              }
                              if (tempAgencyUrlArr[m].toLowerCase() == Viacom.agencyGroups.Constants.newSiteUrl.toLowerCase().split("/sites/agencies/")[1]) {
                                  agencyExist = true;
                                  SP.UI.ModalDialog.commonModalDialogClose();
                                  alert("Site with url: " + Viacom.agencyGroups.Constants.newSiteUrl + " already exists. Please choose another url.");
                                  break;
                              }
                          }
                      }
                          if (!agencyExist) {
                              Viacom.createAgencyWeb.CreateItemInList();
                          }
                      }
                      catch (err) {
                          SP.UI.ModalDialog.commonModalDialogClose();
                          alert("Error in iterating list items which are fetched");
                          return false;
                      }
                  }
                  else {
                      if (agencyTerm) {
                          var urlQuery = "<View><Query><Where><Contains><FieldRef Name='SiteURL'/><Value Type='Text'>/sites/agencies/" + Viacom.agencyGroups.Constants.newSiteUrl.split("/sites/agencies/")[1];
                          urlQuery = urlQuery + "</Value></Contains></Where></Query></View>";
                          Viacom.createAgencyWeb.validationForExistingterms("Site Provisioning", urlQuery, false);
                      }
                      if (!agencyTerm) {
                          Viacom.createAgencyWeb.CreateItemInList();
                      }

                  }
              }, function (sender, args) {
                  SP.UI.ModalDialog.commonModalDialogClose();
                  alert("Error while fetching data from master list");
                  console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
              });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error when validating Agency Name and Agency Url already exist in master list or not");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createAgencyWeb.chkAgencyTerm = function (termName, termSetName, groupName, mmsName) {
    SP.UI.ModalDialog.showWaitScreenWithNoClose('Please wait...', 'Populating data in configuration list...');
    groupName = 'M＆E Ad Sales';
    Viacom.agencyGroups.Constants.objTaxAgency.taxAgencySVPTeam.length = 0;
    Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyNameValue.length = 0;
    Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyIDValue.length = 0;
    Viacom.agencyGroups.Constants.newSiteUrl = window.location.protocol + "//" + window.location.host + "/sites/agencies/" + Viacom.createAgencyWeb.GetSiteUrl(termName);
    Viacom.agencyGroups.Constants.newSiteUrl = Viacom.agencyGroups.Constants.newSiteUrl.toLowerCase();
    var termExist = false;
    try {
        Viacom.agencyGroups.Constants.objTaxAgency.taxAgencySVPTeam.length = 0;
        //Viacom.agencyGroups.Constants.objTaxAgency.taxAgencySVPTeam.push($('#ddlTaxAgencySVPTm option:selected').text());
        //Viacom.agencyGroups.Constants.objTaxAgency.taxAgencySVPTeam.push($('#ddlTaxAgencySVPTm').val());
        //Viacom.createAgencyWeb.getTaxWebTaggingValue();
        var IsValidate = Viacom.createAgencyWeb.ValidateFields("create");
        if (IsValidate) {
            var taxonomyContext = new SP.ClientContext.get_current();
            //var session = SP.Taxonomy.TaxonomySession.getTaxonomySession(taxonomyContext);
            //var termStores = session.get_termStores();
            //var termStore = termStores.getByName(mmsName);
            var session = SP.Taxonomy.TaxonomySession.getTaxonomySession(taxonomyContext);
            var termStore = session.getDefaultSiteCollectionTermStore();
            var group = termStore.get_groups().getByName(groupName);
            var termSet = group.get_termSets().getByName(termSetName);
            var termObj = termSet.get_terms().getByName(termName);
            taxonomyContext.load(termObj);
            taxonomyContext.executeQueryAsync(function () {
                if (termObj.get_name().toLowerCase() == termName.toLowerCase()) {
                    termExist = true;
                    Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyNameValue.push(termObj.get_name());
                    Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyNameValue.push(termObj.get_id()._m_guidString$p$0);
                    //Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyIDValue.push($("#agencyId").val());
                    //Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyIDValue.push($("#agencyId").val());
                    Viacom.createAgencyWeb.chkAgencyIDTerm($('#agencyId').val(), "Agency ID", groupName, mmsName);
                    //Viacom.createAgencyWeb.CreateItemInList();
                }
            }, function (sender, args) {
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                if (!termExist && args.get_errorCode() == -2146233086) {
                    var errorCtx = new SP.ClientContext.get_current();
                    //var errorSession = SP.Taxonomy.TaxonomySession.getTaxonomySession(errorCtx);
                    //var errorTermStore = errorSession.get_termStores();
                    //var errorTermStore = errorTermStore.getByName(mmsName);
                    var errorSession = SP.Taxonomy.TaxonomySession.getTaxonomySession(errorCtx);
                    var errorTermStore = errorSession.getDefaultSiteCollectionTermStore();
                    var errorgroup = errorTermStore.get_groups().getByName(groupName);
                    var errorTermSet = errorgroup.get_termSets().getByName(termSetName);
                    var errornewTerm = errorTermSet.createTerm(termName, 1033, SP.Guid.newGuid()._m_guidString$p$0);
                    errorCtx.load(errornewTerm);
                    errorCtx.executeQueryAsync(function () {
                        // alert("Term Created: " + errornewTerm.get_name());
                        Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyNameValue.push(errornewTerm.get_name());
                        Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyNameValue.push(errornewTerm.get_id()._m_guidString$p$0);
                        //Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyIDValue.push($("#agencyId").val());
                        //Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyIDValue.push($("#agencyId").val());
                        Viacom.createAgencyWeb.chkAgencyIDTerm($('#agencyId').val(), "Agency ID", groupName, mmsName);
                        //  Viacom.createAgencyWeb.CreateItemInList();
                    }, function (sender, args) {
                        console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                        SP.UI.ModalDialog.commonModalDialogClose();
                        alert("Error in creating new agency term in managed metadata service");
                    });
                }
            });
        }
        else {
            SP.UI.ModalDialog.commonModalDialogClose();
            //alert("Please fill all the values");
        }
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error while creating or getting metadata terms");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createAgencyWeb.openDialog = function (parameterObj, newSiteUrl, svpName) {
    try {
        var diaOptions = SP.UI.$create_DialogOptions();
        diaOptions.url = _spPageContextInfo.siteAbsoluteUrl + '/Pages/WebCreationPopup.aspx?termobj=' + parameterObj + '&newSiteUrl=' + newSiteUrl + '&svpName=' + svpName + '&siteType=Agency';
        diaOptions.width = 300;
        diaOptions.height = 215;
        diaOptions.showClose = false;
        diaOptions.allowMaximize = false;
        diaOptions.title = "Please wait..";
        diaOptions.dialogReturnValueCallback = Function.createDelegate(null, Viacom.createAgencyWeb.CloseCallBack);
        SP.UI.ModalDialog.commonModalDialogClose();
        SP.UI.ModalDialog.showModalDialog(diaOptions);
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error while opening popup to create agency web");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createAgencyWeb.CloseCallBack = function (result, returnValue) {
    if (result == SP.UI.DialogResult.OK) {
        alert('Site created successfully!');
        location.reload();
    }
    else {
        if (returnValue != 'Fail!') {
            alert('Site created successfully!');
            location.reload();
        }
        else {
            alert('Site creation failed!');
        }
    }
};

//////// Edit Agencies Section /////////////////////

Viacom.createAgencyWeb.populateData = function () {
    if (Viacom.createAgencyWeb.ValidateFields("populate")) {
        try {
            var taxAgencyName = $("span[id*='TaxonomyAgencyNameEdit']")[0].children[0];
            var query = "<View><Query><Where><Contains><FieldRef Name='AgencyName'/><Value Type='TaxonomyFieldType'>";
            query = query + taxAgencyName.getAttribute('value').split('|')[0] + "</Value></Contains></Where>";
            query = query + "</Query></View>";

            var ctxUrl = window.location.protocol + "//" + window.location.host + _spPageContextInfo.siteServerRelativeUrl;
            var clientContext = new SP.ClientContext(ctxUrl);
            var root = clientContext.get_site().get_rootWeb();
            var oList = root.get_lists().getByTitle("Agency Data");
            var camlQuery = new SP.CamlQuery();
            camlQuery.set_viewXml(query);
            var collListItem = oList.getItems(camlQuery);
            clientContext.load(collListItem);
            var tempAgencyArr = [];
            var tempAgencyUrlArr = [];
            clientContext.executeQueryAsync(
                  function (sender, args) {
                      if (collListItem.get_count() != 0) {
                          var listItemInfo = '';
                          var listItemEnumerator = collListItem.getEnumerator();
                          try {
                              while (listItemEnumerator.moveNext()) {
                                  var oListItem = listItemEnumerator.get_current();
                                  var agencyUrl = oListItem.get_item('AgencyUrl');
                                  var agencyID = oListItem.get_item('AgencyID');

                                  Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyNameValue.push(oListItem.get_item('AgencyName').$0_1);
                                  Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyNameValue.push(oListItem.get_item('AgencyName').$1_1);
                                  //Viacom.agencyGroups.Constants.objTaxAgency.taxAgencySVPTeam.push(oListItem.get_item('TeamName').$0_1);
                                  //Viacom.agencyGroups.Constants.objTaxAgency.taxAgencySVPTeam.push(oListItem.get_item('TeamName').$1_1)

                                  //populate the fields on the form
                                  $("#preAgencyUrlEdit").val(agencyUrl);
                                  if (agencyID != null) {
                                      $("#agencyIdEdit").val(agencyID.$0_1);
                                  }
                                  break;
                              }
                              $("#updateAgency").removeAttr('disabled');

                          }
                          catch (err) {
                              //SP.UI.ModalDialog.commonModalDialogClose();
                              alert("Error in iterating list items which are fetched");
                              return false;
                          }
                      }
                      else {
                          alert("Could not get the Agency with the given name");
                      }
                  }, function (sender, args) {
                      //SP.UI.ModalDialog.commonModalDialogClose();
                      alert("Error while fetching data from master list");
                      console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                  });
        }

        catch (err) {
            SP.UI.ModalDialog.commonModalDialogClose();
            alert("Error in getting list items");
            return false;
        }

    }
};

Viacom.createAgencyWeb.addAgencyIDTerm = function () {

    if (Viacom.createAgencyWeb.ValidateFields("edit")) {
        SP.UI.ModalDialog.showWaitScreenWithNoClose('Please wait...', 'Populating data in configuration list...');
        var termExist = false;
        var termName = $("#agencyIdEdit").val();
        var termSetName = 'Agency ID';
        var groupName = 'M＆E Ad Sales';

        try {
            termName = termName.trim();
            if (termName.length > 0) {
                var taxonomyContext = new SP.ClientContext.get_current();
                //var session = SP.Taxonomy.TaxonomySession.getTaxonomySession(taxonomyContext);
                //var termStores = session.get_termStores();
                //var termStore = termStores.getByName(mmsName);
                var session = SP.Taxonomy.TaxonomySession.getTaxonomySession(taxonomyContext);
                var termStore = session.getDefaultSiteCollectionTermStore();
                var group = termStore.get_groups().getByName(groupName);
                var termSet = group.get_termSets().getByName(termSetName);
                var termObj = termSet.get_terms().getByName(termName);
                taxonomyContext.load(termObj);
                taxonomyContext.executeQueryAsync(function () {
                    if (termObj.get_name().toLowerCase() == termName.toLowerCase()) {
                        termExist = true;
                        Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyIDValue.push(termObj.get_name());
                        Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyIDValue.push(termObj.get_id()._m_guidString$p$0);
                    }

                    //var taxAgencyName = $("span[id*='TaxonomyAgencyNameEdit']")[0].children[0];
                    //var query = "<View><Query><Where><Contains><FieldRef Name='AgencyName'/><Value Type='TaxonomyFieldType'>";
                    //query = query + taxAgencyName.getAttribute('value').split('|')[0] + "</Value></Contains></Where>";
                    //query = query + "</Query></View>";
                    //Viacom.createAgencyWeb.validationForExistingterms("", query, true);
                    Viacom.createAgencyWeb.CallToCreateEditItemInSiteProvisioning();

                }, function (sender, args) {
                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                    if (!termExist && args.get_errorCode() == -2146233086) {
                        var errorCtx = new SP.ClientContext.get_current();
                        var errorSession = SP.Taxonomy.TaxonomySession.getTaxonomySession(errorCtx);
                        //var errorTermStore = errorSession.get_termStores();
                        //var errorTermStore = errorTermStore.getByName(mmsName);
                        var errorTermStore = errorSession.getDefaultSiteCollectionTermStore();
                        var errorgroup = errorTermStore.get_groups().getByName(groupName);
                        var errorTermSet = errorgroup.get_termSets().getByName(termSetName);
                        var errornewTerm = errorTermSet.createTerm(termName, 1033, SP.Guid.newGuid()._m_guidString$p$0);
                        errorCtx.load(errornewTerm);
                        errorCtx.executeQueryAsync(function () {
                            //  alert("Term Created: " + errornewTerm.get_name());
                            Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyIDValue.push(errornewTerm.get_name());
                            Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyIDValue.push(errornewTerm.get_id()._m_guidString$p$0);
                            //var query = "<View><Query><Where><Contains><FieldRef Name='AgencyName'/><Value Type='TaxonomyFieldType'>";
                            //query = query + $("#agencyNm").val() + "</Value></Contains></Where>";
                            //query = query + "</Query></View>";
                            //Viacom.createAgencyWeb.validationForExistingterms("Agency Data", query, true);
                            //Viacom.createAgencyWeb.CreateItemInList();
                            Viacom.createAgencyWeb.CallToCreateEditItemInSiteProvisioning();
                        }, function (sender, args) {
                            SP.UI.ModalDialog.commonModalDialogClose();
                            alert("Error while creating new term for Agency ID");
                            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                        });
                    }
                });
            }
            else {
                SP.UI.ModalDialog.commonModalDialogClose();
                alert("Please enter valid Agency Id for editing the Agency.");
            }
        }
        catch (e) {
            SP.UI.ModalDialog.commonModalDialogClose();
            alert("Error while checking whether giving Agency ID already exist or not");
            console.log('Request failed. ' + e.message);
        }
    }
};

Viacom.createAgencyWeb.CallToCreateEditItemInSiteProvisioning = function () {
    try {
        var AgId = $("#agencyIdEdit").val();
        AgId = AgId.trim();

        var insertItems = [[]];
        Viacom.agencyGroups.Constants.newSiteUrl = $("#preAgencyUrlEdit").val();
        insertItems[0] = ["MMD", "AgencyName", Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyNameValue[1]];
        insertItems[1] = ["SLT", "SiteURL", Viacom.agencyGroups.Constants.newSiteUrl];
        //insertItems[2] = ["MMD", "TeamName", Viacom.agencyGroups.Constants.objTaxAgency.taxAgencySVPTeam[1]];

        insertItems[2] = ["Choice", "SiteType", "AgencySite"];
        insertItems[3] = ["Choice", "ActionType", "EditSite"];

        if (AgId.length > 0) {
            insertItems[4] = ["MMD", "AgencyID", Viacom.agencyGroups.Constants.objTaxAgency.taxAgencyIDValue[1]];
        }

        var clientContext = SP.ClientContext.get_current();
        var root = clientContext.get_site().get_rootWeb();
        var oList = root.get_lists().getByTitle("Site Provisioning");
        var listContentTypes = oList.get_contentTypes();

        clientContext.load(listContentTypes);

        var item = oList.addItem();
        for (var i = 0; i < insertItems.length; i++) {
            switch (insertItems[i][0]) {
                case "User": {
                    if (insertItems[i][2] != undefined && insertItems[i][2] != null)
                        item.set_item(insertItems[i][1], SP.FieldUserValue.fromUser(insertItems[i][2]));
                    break;
                }
                case "HyperLink": {
                    if (insertItems[i][2] != undefined && insertItems[i][2] != null) {
                        var rootSiteUrl = new SP.FieldUrlValue();
                        rootSiteUrl.set_url(insertItems[i][2]);
                        rootSiteUrl.set_description(insertItems[i][2]);
                        item.set_item(insertItems[i][1], rootSiteUrl);
                    }
                    break;
                }
                case "Date": {
                    if (insertItems[i][2] != undefined && insertItems[i][2] != null) {
                        var iDt = insertItems[i][2].toISOString();
                        item.set_item(insertItems[i][1], iDt);
                    }
                    break;
                }
                default: {
                    if (insertItems[i][2] != undefined && insertItems[i][2] != null)
                        item.set_item(insertItems[i][1], insertItems[i][2]);
                }
            }
        }
        item.update();
        clientContext.executeQueryAsync(function () {
            var ct_enumerator = listContentTypes.getEnumerator();
            while (ct_enumerator.moveNext()) {
                var ct = ct_enumerator.get_current();
                if (ct.get_name().toString() == 'Agency Site Provisioning') {
                    //we've got our content type, now let's get its name
                    item.set_item('ContentTypeId', ct.get_id().toString());
                    item.update();

                    clientContext.executeQueryAsync(function () {
                        SP.UI.ModalDialog.commonModalDialogClose();
                        alert("Site update request initiated successfully");
                        location.reload();
                    }, function (sender, args) {
                        SP.UI.ModalDialog.commonModalDialogClose();
                        alert("Error in while updating list item");
                        console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                    });
                }
            }
            //SP.UI.ModalDialog.commonModalDialogClose();
            //alert("Site Creation initiated successfully");
        }, function (sender, args) {
            SP.UI.ModalDialog.commonModalDialogClose();
            alert("Error in either creating iitem in 'Sites' list or making call top open another popup");
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        });



    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error while making call to create item in provisioning list");
        console.log('Request failed. ' + e.message);
    }
}

Viacom.createAgencyWeb.GetSiteUrl = function (nmId) {
    var siteurl = nmId.replace(/[^a-zA-Z0-9]/g, '');
    return siteurl;
}