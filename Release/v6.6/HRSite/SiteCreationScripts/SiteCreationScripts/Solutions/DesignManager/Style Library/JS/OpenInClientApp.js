﻿var Viacom = Viacom || {};
Viacom.OpenInClientAppCommon = {};

Viacom.OpenInClientAppCommon.OpenInClientApp = function (LinkURL, FileExtention) {
    LinkURL = htmlUnescape(LinkURL);
    var fileFriendlyName = Viacom.OpenInClientAppCommon.GetFileExtentionName(FileExtention);
    var strApp = '';
    switch (fileFriendlyName) {
        case 'file_Excel':
            strApp = 'ms-excel';
            Viacom.OpenInClientAppCommon.CreateProtocolHandlerUrl(strApp, LinkURL);
            break;
        case 'file_Word':
            strApp = 'ms-word';
            Viacom.OpenInClientAppCommon.CreateProtocolHandlerUrl(strApp, LinkURL);
            break;
        case 'file_PowerPoint':
            strApp = 'ms-powerpoint';
            Viacom.OpenInClientAppCommon.CreateProtocolHandlerUrl(strApp, LinkURL);
            break;
        case 'file_Access':
            strApp = 'ms-access';
            Viacom.OpenInClientAppCommon.CreateProtocolHandlerUrl(strApp, LinkURL);
            break;
        case 'file_Visio':
            strApp = 'ms-visio';
            Viacom.OpenInClientAppCommon.CreateProtocolHandlerUrl(strApp, LinkURL);
            break;
        case 'file_Publisher':
            strApp = 'ms-publisher';
            Viacom.OpenInClientAppCommon.CreateProtocolHandlerUrl(strApp, LinkURL);
            break;
        case 'file_InfoPath':
            strApp = 'ms-infopath';
            Viacom.OpenInClientAppCommon.CreateProtocolHandlerUrl(strApp, LinkURL);
            break;
        case 'file_Project':
            strApp = 'ms-project';
            Viacom.OpenInClientAppCommon.CreateProtocolHandlerUrl(strApp, LinkURL);
            break;
        default:
            window.location.href = LinkURL;

    }
    if (event.preventDefault)
    { event.preventDefault(); }
    else
    { event.returnValue = false; }
}

Viacom.OpenInClientAppCommon.GetFileExtentionName = function (FileExtention) {
    var fileExtension = FileExtention.toLowerCase();
    if (fileExtension === 'mdb' || fileExtension === 'accdb' || fileExtension === 'accdt' || fileExtension === 'accdc' || fileExtension === 'accde' || fileExtension === 'accdr') {
        return 'file_Access';
    }
    else if (fileExtension === 'xlam' || fileExtension === 'xla' || fileExtension === 'csv' || fileExtension === 'ods' || fileExtension === 'xlb' || fileExtension === 'xlc' || fileExtension === 'xlm' || fileExtension === 'odc' || fileExtension === 'xls' || fileExtension === 'xlsb' || fileExtension === 'xlsm' || fileExtension === 'xlsx' || fileExtension === 'xlt' || fileExtension === 'xltb' || fileExtension === 'xltm' || fileExtension === 'xltx') {
        return 'file_Excel';
    }
    else if (fileExtension === 'xtp' || fileExtension === 'xtp2' || fileExtension === 'xsf' || fileExtension === 'xsn') {
        return 'file_InfoPath';
    }
    else if (fileExtension === 'ppam' || fileExtension === 'ppa' || fileExtension === 'odp' || fileExtension === 'pot' || fileExtension === 'potm' || fileExtension === 'potx' || fileExtension === 'pps' || fileExtension === 'ppsm' || fileExtension === 'ppsx' || fileExtension === 'ppt' || fileExtension === 'pptm' || fileExtension === 'pptx') {
        return 'file_PowerPoint';
    }
    else if (fileExtension === 'mpd' || fileExtension === 'mpp' || fileExtension === 'mpt') {
        return 'file_Project';
    }
    else if (fileExtension === 'ps' || fileExtension === 'pub') {
        return 'file_Publisher';
    }
    else if (fileExtension === 'vsdm' || fileExtension === 'vssm' || fileExtension === 'vssx' || fileExtension === 'vstm' || fileExtension === 'vsdx' || fileExtension === 'vstx' || fileExtension === 'asdx' || fileExtension === 'vdw' || fileExtension === 'vdx' || fileExtension === 'vsd' || fileExtension === 'vsl' || fileExtension === 'vss' || fileExtension === 'vst' || fileExtension === 'vsu' || fileExtension === 'vsw' || fileExtension === 'vsx' || fileExtension === 'vtx') {
        return 'file_Visio';
    }
    else if (fileExtension === 'xls' || fileExtension === 'rtf' || fileExtension === 'odt' || fileExtension === 'wps' || fileExtension === 'doc' || fileExtension === 'docm' || fileExtension === 'docx' || fileExtension === 'dot' || fileExtension === 'dotm' || fileExtension === 'dotx' || fileExtension === 'mht' || fileExtension === 'mhtml') {
        return 'file_Word';
    }
    return 'file_Document';
}

Viacom.OpenInClientAppCommon.CreateProtocolHandlerUrl = function (strApp, LinkURL) {
    var ieBrowser = isIE();
    try {
        if (ieBrowser) {
            var objEditor = StsOpenEnsureEx2("SharePoint.OpenDocuments.5");
            if (objEditor === null || objEditor === undefined) {
                objEditor = StsOpenEnsureEx2("SharePoint.OpenDocuments.3");
                if (objEditor !== null && objEditor !== undefined) {
                    objEditor.EditDocument(LinkURL);
                    return;
                }
                else {
                    objEditor = StsOpenEnsureEx2("SharePoint.OpenDocuments");
                    if (objEditor !== null && objEditor !== undefined) {
                        objEditor.EditDocument(LinkURL);
                        return;
                    }
                }
            }
            else {
                if (objEditor != null || objEditor !== undefined) {
                    objEditor.EditDocument(LinkURL)
                }
            }
        }
    }
    catch (e) {
        console.log("IE will use URI scheme.")
    }
    var ret = [];
    ret.push(strApp);
    ret.push(':');
    ret.push('ofe');
    ret.push('|u|');
    ret.push(LinkURL);
    var finalURL = ret.join('');
    window.location.href = finalURL;
}

function htmlUnescape(value) {
    return String(value)
        .replace(/&quot;/g, '"')
        .replace(/&#39;/g, "'")
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>')
        .replace(/&amp;/g, '&');
}

function isIE() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        return true;
    }
    else {                // If another browser, return false
        return false;
    }
    return false;
}



