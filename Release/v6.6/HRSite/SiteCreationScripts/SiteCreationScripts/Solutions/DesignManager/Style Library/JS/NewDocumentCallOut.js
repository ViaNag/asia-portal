var Viacom = Viacom || {};
//Objects for new document callout
Viacom.NewDocumentCallOut = {};
Viacom.NewDocumentCallOut.docTemplateListData = [];
Viacom.NewDocumentCallOut.length = null;
Viacom.NewDocumentCallOut.hasDocSet = false;
Viacom.NewDocumentCallOut.hasOtherCT = false;
Viacom.NewDocumentCallOut.TemplateLib = 'Templates'
Viacom.NewDocumentCallOut.CurrentListTitle = null;
Viacom.NewDocumentCallOut.docSetList = [];

//Objects for add to template library
Viacom.AddToTemplateLibCustomAction = {};
Viacom.AddToTemplateLibCustomAction.associatedLibraryChoiceValue = [];
Viacom.AddToTemplateLibCustomAction.selectedItem = null;
Viacom.AddToTemplateLibCustomAction.enableButton = null;
Viacom.AddToTemplateLibCustomAction.selectedFileName = null;
Viacom.AddToTemplateLibCustomAction.selectedFileUrl = null;
Viacom.AddToTemplateLibCustomAction.waitDialog = null;
Viacom.AddToTemplateLibCustomAction.actionName = "Add To Template Library";
Viacom.AddToTemplateLibCustomAction.permMessage = "You do not have sufficient permission.";
Viacom.AddToTemplateLibCustomAction.fileCheckOutMessage = "Selected file is checked out.";
Viacom.AddToTemplateLibCustomAction.errorMessage = "Something went wrong. Please try again.";
Viacom.AddToTemplateLibCustomAction.errorMessageDialogTitle = "Error";
Viacom.AddToTemplateLibCustomAction.messageDialogTitle = "Message";

//Objects for custom action
Viacom.PageComponent = {};

//init
Viacom.NewDocumentCallOut.init = function () {
    SP.SOD.executeFunc('clienttemplates.js', 'SPClientTemplates', function () {
        SPClientTemplates.TemplateManager.RegisterTemplateOverrides({
            OnPostRender: function (ctx) {
                var linkName = document.getElementById("idHomePageNewDocument-" + ctx.wpq);
                if (linkName != null) {
                    if (ctx.ContentTypesEnabled) {
                        if (typeof CalloutManager === "undefined") {
                            SP.SOD.executeFunc('callout.js', 'callout', function () { Viacom.NewDocumentCallOut.getCurrentListCT(ctx) });
                        }
                        else {
                            Viacom.NewDocumentCallOut.getCurrentListCT(ctx);
                        }
                    }
                }
            }
        });
    });
};

//Filter Library and get current List Content Type
Viacom.NewDocumentCallOut.getCurrentListCT = function (ctx) {
    Viacom.NewDocumentCallOut.CurrentListTitle = ctx.ListTitle;
    if (["Master Page Gallery", "Converted Forms", "Form Templates", "List Template Gallery", "Site Assets", "Site Collection Documents", "Pages", "Images", "Solution Gallery", "Style Library", "Theme Gallery", "Web Part Gallery"].indexOf(Viacom.NewDocumentCallOut.CurrentListTitle) <= -1) {
        Viacom.NewDocumentCallOut.listCT = [];
        Viacom.NewDocumentCallOut.ctOrder = [];
        var isDocSetCallout = false;
        var folderCtid = GetUrlKeyValue('FolderCTID', false, window.location.href);
        if (folderCtid != null && folderCtid != undefined && folderCtid != '') {
            if (folderCtid.indexOf("0x0120D520") == 0) {
                isDocSetCallout = true;
            }
        }
        if (!isDocSetCallout) {
            $.ajax({
                url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists('" + ctx.listName + "')/contenttypes",
                method: "GET",
                async: false,
                headers: { "Accept": "application/json; odata=verbose" },
                success: function (CTdata) {
                    Viacom.NewDocumentCallOut.getCTOrder(ctx, CTdata);
                },
                error: function (jqXHR, textStatus, error) {
                    console.log(error)
                }
            });
        }
        else {
            //For document set home page
            var scriptbase = _spPageContextInfo.siteAbsoluteUrl + "/_layouts/15/";
            SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
                $.getScript(scriptbase + 'SP.DocumentManagement.js', function () {
                    Viacom.NewDocumentCallOut.getDocumentSetContentTypes(folderCtid, ctx);
                });
            });
        }
    }
};

Viacom.NewDocumentCallOut.getDocumentSetContentTypes = function (folderCtid, ctx) {
    var ctx = SP.ClientContext.get_current();
    var currentWeb = ctx.get_web();
    var list = currentWeb.get_lists().getByTitle(Viacom.NewDocumentCallOut.CurrentListTitle);
    var docSetCT = list.get_contentTypes().getById(folderCtid);
    ctx.load(docSetCT);
    var docSetCtIds = null;
    ctx.executeQueryAsync(function () {
        var template = SP.DocumentSet.DocumentSetTemplate.getDocumentSetTemplate(ctx, docSetCT);
        docSetCtIds = template.get_allowedContentTypes();
        ctx.load(docSetCtIds);
        ctx.executeQueryAsync(
                 function () {
                     var getCTNmQuery = '';
                     var ctCount = 0;
                     var ctIdInDocSet = docSetCtIds.get_data().length;
                     docSetCtIds.get_data().forEach(function (ctId) {
                         ctCount++;
                         Viacom.NewDocumentCallOut.ctOrder.push([ctId.get_stringValue(), ctId.get_stringValue()]);
                         if (ctCount == ctIdInDocSet) {
                             getCTNmQuery += "(StringId eq '" + ctId.get_stringValue() + "')";
                         }
                         else {
                             getCTNmQuery += "(StringId eq '" + ctId.get_stringValue() + "') or ";
                         }
                     });
                     $.ajax({
                         url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/availablecontenttypes?$select=StringId,Name&$filter=" + getCTNmQuery,
                         method: "GET",
                         async: false,
                         headers: { "Accept": "application/json; odata=verbose" },
                         success: function (data) {
                             var templateQuery = '';
                             var dataLength = data.d.results.length;
                             if (dataLength != 0) {
                                 for (var i = 0; i < dataLength; i++) {
                                     for (var ct = 0; ct < Viacom.NewDocumentCallOut.ctOrder.length; ct++) {
                                         for (var allCt = 0; allCt < Viacom.NewDocumentCallOut.ctOrder.length; allCt++) {
                                             if (Viacom.NewDocumentCallOut.ctOrder[ct][0] == data.d.results[i].StringId) {
                                                 Viacom.NewDocumentCallOut.ctOrder[ct][1] = data.d.results[i].Name;
                                             }
                                         }
                                         if (data.d.results[i].StringId.indexOf("0x0120D520") == 0) {
                                             Viacom.NewDocumentCallOut.docSetList.push([data.d.results[i].StringId, data.d.results[0].Name])
                                             Viacom.NewDocumentCallOut.hasDocSet = true;
                                         }
                                         else {
                                             Viacom.NewDocumentCallOut.hasOtherCT = true;
                                         }
                                     }
                                     Viacom.NewDocumentCallOut.listCT.push([data.d.results[i].StringId, data.d.results[0].Name]);
                                     if (i == dataLength - 1) {
                                         templateQuery += "ContentType eq '" + data.d.results[i].Name + "'";
                                     }
                                     else {
                                         templateQuery += "ContentType eq '" + data.d.results[i].Name + "' or ";
                                     }
                                 }
                                 Viacom.NewDocumentCallOut.getTemplates(templateQuery);
                             }
                         },
                         error: function (jqXHR, textStatus, error) {
                             console.log(error);
                         }
                     });
                 },
                 function (sender, args) {
                     console.log(args.get_message());
                 });
    }, function (sender, args) {
        console.log(args.get_message());
    });
};


Viacom.NewDocumentCallOut.getCTOrder = function (ctx, CTdata) {
    var query = "";
    if (CTdata.d.results.length != 0) {
        for (var index = 0; index < CTdata.d.results.length; index++) {
            Viacom.NewDocumentCallOut.listCT.push([CTdata.d.results[index].StringId, CTdata.d.results[index].Name]);
        }
        $.ajax({
            url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists('" + ctx.listName + "')/RootFolder/ContentTypeOrder",
            method: "GET",
            async: false,
            headers: { "Accept": "application/json; odata=verbose" },
            success: function (data) {
                if (data.d.ContentTypeOrder.results.length != 0) {
                    for (var index = 0; index < data.d.ContentTypeOrder.results.length; index++) {
                        Viacom.NewDocumentCallOut.ctOrder.push([data.d.ContentTypeOrder.results[index].StringValue, data.d.ContentTypeOrder.results[index].StringValue]);
                    }
                }
                for (var ct = 0; ct < Viacom.NewDocumentCallOut.ctOrder.length; ct++) {
                    for (var allCt = 0; allCt < Viacom.NewDocumentCallOut.listCT.length; allCt++) {
                        if (Viacom.NewDocumentCallOut.ctOrder[ct][0] == Viacom.NewDocumentCallOut.listCT[allCt][0]) {
                            Viacom.NewDocumentCallOut.ctOrder[ct][1] = Viacom.NewDocumentCallOut.listCT[allCt][1];
                        }
                    }
                    if (Viacom.NewDocumentCallOut.ctOrder[ct][0].indexOf("0x0120D520") == 0) {
                        Viacom.NewDocumentCallOut.docSetList.push([Viacom.NewDocumentCallOut.ctOrder[ct][0], Viacom.NewDocumentCallOut.ctOrder[ct][1]])
                        Viacom.NewDocumentCallOut.hasDocSet = true;
                    }
                    else {
                        Viacom.NewDocumentCallOut.hasOtherCT = true;
                    }
                    if (ct == Viacom.NewDocumentCallOut.ctOrder.length - 1) {
                        query += "ContentType eq '" + Viacom.NewDocumentCallOut.ctOrder[ct][1] + "'";
                    }
                    else {
                        query += "ContentType eq '" + Viacom.NewDocumentCallOut.ctOrder[ct][1] + "' or ";
                    }
                }
                Viacom.NewDocumentCallOut.getTemplates(query);
            },
            error: function (jqXHR, textStatus, error) {
                console.log(error)
            }
        });
    }
};

//Get Templates from Template Library after success or fail Its call registerCallOut method
Viacom.NewDocumentCallOut.getTemplates = function (query) {
    $.ajax({
        url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('" + Viacom.NewDocumentCallOut.TemplateLib + "')/items?$select=ID,EncodedAbsUrl,TemplateName,CheckoutUserId,AssociatedLibraries,ContentTypeId,ContentType/Name&$expand=ContentType&$filter=substringof('" + encodeURIComponent(Viacom.NewDocumentCallOut.CurrentListTitle) + "',AssociatedLibraries) and (ShowInNewDocument eq 1) and (" + query + ")&$orderby=ContentTypeId asc",
        method: "GET",
        async: false,
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            //Template library exist so add button in ribbon
            if (["Master Page Gallery", "Converted Forms", "Form Templates", "List Template Gallery", "Site Assets", "Site Collection Documents", "Pages", "Images", "Solution Gallery", "Style Library", "Theme Gallery", "Web Part Gallery"].indexOf(Viacom.NewDocumentCallOut.CurrentListTitle) <= -1) {
                SP.SOD.executeOrDelayUntilScriptLoaded(function () {
                    //undefined check added to as in some times whose js loaded two times which overwrite ootb function, to prevent this undefined check is required
                    if (CUI.Ribbon.prototype.$1q_0_old === undefined) {
                        CUI.Ribbon.prototype.$1q_0_old = CUI.Ribbon.prototype.$1q_0;
                    }
                    CUI.Ribbon.prototype.$1q_0 = function () {
                        this.$1q_0_old();
                        $(document).trigger('ribbontabselected', [this.get_selectedTabCommand()]);
                    };
                }, 'cui.js');
                SP.SOD.executeOrDelayUntilScriptLoaded(function () {
                    (SP.Ribbon.PageManager.get_instance()).add_ribbonInited(function () {
                        $(document).trigger('ribbontabselected', [SP.Ribbon.PageManager.get_instance().get_ribbon().get_selectedTabCommand()]);
                    });
                    //Register page component
                    Viacom.PageComponent.registerClass('Viacom.PageComponent', CUI.Page.PageComponent);
                    Viacom.PageComponent.initializePageComponent();
                }, 'sp.ribbon.js');
            }

            if (data.d.results.length != 0) {
                Viacom.NewDocumentCallOut.fillTemplateListData(data.d);
            }
            Viacom.NewDocumentCallOut.length = Viacom.NewDocumentCallOut.docTemplateListData.length;
            Viacom.NewDocumentCallOut.registerCallOut(ctx);
        },
        error: function (jqXHR, textStatus, error) {
            console.log(error)
            Viacom.NewDocumentCallOut.length = 0;
            Viacom.NewDocumentCallOut.registerCallOut(ctx);
        }
    });
}

//Fill Template Data in Array
Viacom.NewDocumentCallOut.fillTemplateListData = function (docTemplateListData) {
    Viacom.NewDocumentCallOut.docTemplateListData = [];
    var fileExtensionArray = ["dotx", "docx", "xltx", "xlsx", "potx", "pptx"];
    var contentTypeId = null;
    var docTempListData = null;
    var fileExt = null;
    var fileAbsUrl = null;
    var templateName = null;
    $.each(docTemplateListData.results, function (val, item) {
        fileAbsUrl = item['EncodedAbsUrl'];
        fileExt = fileAbsUrl.substr((fileAbsUrl.lastIndexOf('.') + 1));

        if (fileExtensionArray.indexOf(fileExt) > -1 && !(item['CheckoutUserId'])) {
            templateName = item['TemplateName'];
            if (templateName == null || templateName.trim() == '') {
                templateName = decodeURI(fileAbsUrl.substring(fileAbsUrl.lastIndexOf("/") + 1, fileAbsUrl.lastIndexOf(".")));
            }
            if (item['ContentTypeId'] != contentTypeId) {
                contentTypeId = item['ContentTypeId'];
                var docTempListData = { contentType: '', templateInfo: [] };
                docTempListData.contentType = item['ContentType']['Name'];
                var templateInfo = { templateName: '', fileAbsUrl: '', fileExt: '' };
                templateInfo.templateName = templateName;
                templateInfo.fileAbsUrl = fileAbsUrl;
                templateInfo.fileExt = fileExt;
                docTempListData.templateInfo.push(templateInfo);
                Viacom.NewDocumentCallOut.docTemplateListData.push(docTempListData);
            }
            else {
                var lastIndex = Viacom.NewDocumentCallOut.docTemplateListData.length - 1;
                var templateInfo = { templateName: '', fileAbsUrl: '', fileExt: '' };
                templateInfo.templateName = templateName;
                templateInfo.fileAbsUrl = fileAbsUrl;
                templateInfo.fileExt = fileExt;
                Viacom.NewDocumentCallOut.docTemplateListData[lastIndex].templateInfo.push(templateInfo);
            }
        }
    });
};

//Register custom Callout on Add New Document Link
Viacom.NewDocumentCallOut.registerCallOut = function (ctx) {
    var targetElement = document.getElementById("idHomePageNewDocument-" + ctx.wpq);
    if (targetElement == null) targetElement = document.getElementById("idHomePageNewItem");

    if (CalloutManager.getFromLaunchPointIfExists(targetElement) == null) {
        if (Viacom.NewDocumentCallOut.length != null) {

            //remove the click function. This should remove the upload functionality
            targetElement.onclick = null;
            if (Viacom.NewDocumentCallOut.length > 0) {
                var defaultCallOutContent = Viacom.NewDocumentCallOut.getTemplateCallOutContent(ctx);
                var width = 500;
            }
            else {
                var defaultCallOutContent = Viacom.NewDocumentCallOut.getDefaultCallOutContent(ctx);
                var width = 280;
            }

            // configure options
            var createNewFilecalloutOptions = new CalloutOptions();
            createNewFilecalloutOptions.ID = 'js-newdocWOPI-WPQ2-custom';
            createNewFilecalloutOptions.launchPoint = targetElement;
            createNewFilecalloutOptions.contentWidth = width;
            createNewFilecalloutOptions.beakOrientation = 'leftRight';
            createNewFilecalloutOptions.content = defaultCallOutContent;
            createNewFilecalloutOptions.title = 'Create a new file';
            var createNewFileCallout = CalloutManager.createNew(createNewFilecalloutOptions);
            createNewFileCallout.addEventCallback('opening', function (calloutObject) {
                $(".callout-right .content:first-child").show();
                $(".callcoutTab ul li:first-child a").addClass('active');
                $('.callcoutTab li a').click(function () {
                    var tab_url = $(this).attr('href');
                    $('.callcoutTab li a').removeClass('active');
                    $(this).addClass('active');
                    $('.callout-right .content').hide();
                    $(tab_url).show();
                    return false;
                });
            });

            var uploadFileActionOptions = new CalloutActionOptions();
            uploadFileActionOptions.text = 'UPLOAD EXISTING FILE';
            uploadFileActionOptions.onClickCallback = function (event, action) {
                CalloutManager.closeAll();

                var dialogOptions = SP.UI.$create_DialogOptions();
                dialogOptions.url = _spPageContextInfo.webAbsoluteUrl + "/_layouts/15/Upload.aspx?List=" + ctx.listName + "&Source=" + window.location.protocol + "//" + window.location.host + window.location.pathname + "&RootFolder=" + ctx.rootFolder;

                dialogOptions.autoSize = true,
                dialogOptions.resizable = true,
                dialogOptions.scroll = false
                dialogOptions.title = "Upload a document";
                dialogOptions.dialogReturnValueCallback = Function.createDelegate(null, Viacom.NewDocumentCallOut.closeCallBack);
                SP.UI.ModalDialog.showModalDialog(dialogOptions);
            };
            var uploadFileAction = new CalloutAction(uploadFileActionOptions);

            createNewFileCallout.addAction(uploadFileAction);
        }
    }

}

//Get html of default callout
Viacom.NewDocumentCallOut.getDefaultCallOutContent = function (ctx) {
    var linkname = document.getElementById("idHomePageNewDocument-" + ctx.wpq);
    if (linkname == null) linkname = document.getElementById("idHomePageNewItem");

    if (ctx.rootFolder != '') {
        var popUpPageParameterWord = '&quot;' + _spPageContextInfo.webAbsoluteUrl + '/_layouts/15/CreateNewDocument.aspx?id=' + _spPageContextInfo.webAbsoluteUrl + '/_catalogs/masterpage/BlankTemplates/WordTemplate.docx&SaveLocation=' + encodeURIComponent(ctx.rootFolder) + '&DefaultItemOpen=0&Source=' +
                             window.location.protocol + "//" + window.location.host + window.location.pathname + '&OpenIn=0';
        var popUpPageParameterExcel = '&quot;' + _spPageContextInfo.webAbsoluteUrl + '/_layouts/15/CreateNewDocument.aspx?id=' + _spPageContextInfo.webAbsoluteUrl + '/_catalogs/masterpage/BlankTemplates/Exceltemplate.xlsx&SaveLocation=' + encodeURIComponent(ctx.rootFolder) + '&DefaultItemOpen=0&Source=' +
                               window.location.protocol + "//" + window.location.host + window.location.pathname + '&OpenIn=0';
        var popUpPageParameterPowerPoint = '&quot;' + _spPageContextInfo.webAbsoluteUrl + '/_layouts/15/CreateNewDocument.aspx?id=' + _spPageContextInfo.webAbsoluteUrl + '/_catalogs/masterpage/BlankTemplates/PPTtemplate.pptx&SaveLocation=' + encodeURIComponent(ctx.rootFolder) + '&DefaultItemOpen=0&Source=' +
                                window.location.protocol + "//" + window.location.host + window.location.pathname + '&OpenIn=0';
    }
    else {
        var popUpPageParameterWord = '&quot;' + _spPageContextInfo.webAbsoluteUrl + '/_layouts/15/CreateNewDocument.aspx?id=' + _spPageContextInfo.webAbsoluteUrl + '/_catalogs/masterpage/BlankTemplates/WordTemplate.docx&SaveLocation=' + encodeURIComponent(ctx.listUrlDir) + '&DefaultItemOpen=0&Source=' +
                                 window.location.protocol + "//" + window.location.host + window.location.pathname + '&OpenIn=0';
        var popUpPageParameterExcel = '&quot;' + _spPageContextInfo.webAbsoluteUrl + '/_layouts/15/CreateNewDocument.aspx?id=' + _spPageContextInfo.webAbsoluteUrl + '/_catalogs/masterpage/BlankTemplates/Exceltemplate.xlsx&SaveLocation=' + encodeURIComponent(ctx.listUrlDir) + '&DefaultItemOpen=0&Source=' +
                               window.location.protocol + "//" + window.location.host + window.location.pathname + '&OpenIn=0';
        var popUpPageParameterPowerPoint = '&quot;' + _spPageContextInfo.webAbsoluteUrl + '/_layouts/15/CreateNewDocument.aspx?id=' + _spPageContextInfo.webAbsoluteUrl + '/_catalogs/masterpage/BlankTemplates/PPTtemplate.pptx&SaveLocation=' + encodeURIComponent(ctx.listUrlDir) + '&DefaultItemOpen=0&Source=' +
                                window.location.protocol + "//" + window.location.host + window.location.pathname + '&OpenIn=0';

    }
    var callOutContent = '';
    if (Viacom.NewDocumentCallOut.hasOtherCT) {
        callOutContent = '<div id="js-newdocWOPI-divMain-WPQ2" class="ms-newdoc-callout-main" style="margin-left: 20px; margin-right: 20px;">' +
            '<a id="js-newdocWOPI-divWord-WPQ2" class="ms-newdoc-callout-item ms-displayBlock" onclick="CalloutManager.closeAll(); OpenPopUpPageWithTitle(' +
                popUpPageParameterWord + '&quot;, Viacom.NewDocumentCallOut.createDocumentCallBack); return false;" href="#">' +
                '<img id="js-newdocWOPI-divWord-img-WPQ2" src="/_layouts/15/images/lg_icdocx.png?rev=23" alt="Create a new Word document" class="ms-verticalAlignMiddle ms-newdoc-callout-img"/>' +
                '<h3 id="js-newdocWOPI-divWord-txt-WPQ2" class="ms-displayInline ms-newdoc-callout-text ms-verticalAlignMiddle ms-soften">Word document</h3>' +
            '</a>' +
            '<a id="js-newdocWOPI-divExcel-WPQ2" class="ms-newdoc-callout-item ms-displayBlock" onclick="CalloutManager.closeAll(); OpenPopUpPageWithTitle(' +
                popUpPageParameterExcel + '&quot;, Viacom.NewDocumentCallOut.createDocumentCallBack); return false;" href="#">' +
                '<img id="js-newdocWOPI-divExcel-img-WPQ2" src="/_layouts/15/images/lg_icxlsx.png?rev=23" alt="Create a new Excel workbook" class="ms-verticalAlignMiddle ms-newdoc-callout-img"/>' +
                '<h3 id="js-newdocWOPI-divExcel-txt-WPQ2" class="ms-displayInline ms-newdoc-callout-text ms-verticalAlignMiddle ms-soften">Excel workbook</h3>' +
            '</a>' +
            '<a id="js-newdocWOPI-divPowerPoint-WPQ2" class="ms-newdoc-callout-item ms-displayBlock" onclick="CalloutManager.closeAll(); OpenPopUpPageWithTitle(' +
                popUpPageParameterPowerPoint + '&quot;, Viacom.NewDocumentCallOut.createDocumentCallBack); return false;" href="#">' +
                '<img id="js-newdocWOPI-divPowerPoint-img-WPQ2" src="/_layouts/15/images/lg_icpptx.png?rev=23" alt="Create a new PowerPoint presentation" class="ms-verticalAlignMiddle ms-newdoc-callout-img"/>' +
                '<h3 id="js-newdocWOPI-divPowerPoint-txt-WPQ2" class="ms-displayInline ms-newdoc-callout-text ms-verticalAlignMiddle ms-soften">PowerPoint presentation</h3>' +
            '</a>';
    }

    var newfolderContent = '';
    var folderEnabled = false;
    if (typeof ctx.AllowCreateFolder != 'undefined') {
        folderEnabled = true;
        var newFolderParameter = '&quot;' + _spPageContextInfo.webAbsoluteUrl + '/_layouts/15/listform.aspx?ListId=' + ctx.listName + '&amp;PageType=8&amp;RootFolder=' + ctx.rootFolder + '&amp;Type=1&quot;';

        if (Viacom.NewDocumentCallOut.hasOtherCT) {
            newfolderContent = '<div class="newfolder newfolderCustom defaultDocuments">';
            newfolderContent += '<hr/>';
        }
        else {
            newfolderContent = '<div class="newfolder newfolderCustom">';
        }
        newfolderContent += '<a class="ms-newdoc-callout-item ms-displayBlock newFolderAdjustment" id="js-newdocWOPI-divFolder-WPQ2" onclick="CalloutManager.closeAll(); NewItem2(event,'
    			+ newFolderParameter + ');return false;" href="#"><img class="ms-verticalAlignMiddle ms-newdoc-callout-img" id="js-newdocWOPI-divFolder-img-WPQ2" ' +
    			'alt="Create a new folder" src="/_layouts/15/images/mb_folder.png?rev=23"><h3 class="ms-displayInline ms-newdoc-callout-text ms-verticalAlignMiddle ms-soften" ' +
    			'id="js-newdocWOPI-divFolder-txt-WPQ2">New folder</h3>' +
    		'</a></div>';
    }
    var docSetHtml = '';
    if (Viacom.NewDocumentCallOut.docSetList.length > 0) {
        for (var index = 0; index <= Viacom.NewDocumentCallOut.docSetList.length - 1; index++) {
            //var popUpPageParameterDocSet = 'http://sp2013-ddev-3:7777/sites/adsalesclientrelations/_layouts/15/NewDocSet.aspx?List=29a23f45-2dde-47f2-a95e-f57038450603&Source=http%3A%2F%2Fsp2013-ddev-3%3A7777%2Fsites%2Fadsalesclientrelations%2FAdministrative%2FForms%2FAllItems%2Easpx&ContentTypeId=0x0120D520003E598A3D634746A5BFCBEA119B9F0F3D00DD4944EA3A76244F93D9FBB6839D7319&RootFolder=%2Fsites%2Fadsalesclientrelations%2FAdministrative';
            var popUpPageParameterDocSet = '&quot;' + _spPageContextInfo.webAbsoluteUrl + '/_layouts/15/NewDocSet.aspx?List=' + ctx.listName + '&Source=' + window.location.protocol + '//' + window.location.host + window.location.pathname + '&ContentTypeId=' + Viacom.NewDocumentCallOut.docSetList[index][0] + '&RootFolder=' + ctx.listUrlDir + '&quot';
            if (Viacom.NewDocumentCallOut.hasOtherCT) {
                docSetHtml += '<div class="newfolder newfolderCustom defaultDocuments">';
            }
            else {
                docSetHtml += '<div class="newfolder newfolderCustom">';
            }
            if (!folderEnabled && index == 0 && Viacom.NewDocumentCallOut.hasOtherCT) {
                docSetHtml += '<hr/>';
            }
            docSetHtml += '<a class="ms-newdoc-callout-item ms-displayBlock newFolderAdjustment" id="js-newdocWOPI-divFolder-WPQ2" onclick="CalloutManager.closeAll(); NewItem2(event,'
    			+ popUpPageParameterDocSet + ');return false;" href="#"><img class="ms-verticalAlignMiddle ms-newdoc-callout-img" id="js-newdocWOPI-divFolder-img-WPQ2" ' +
    			'alt="Create a new folder" src="/_layouts/15/images/NewDocumentSet32.png?rev=23"><h3 class="ms-displayInline ms-newdoc-callout-text ms-verticalAlignMiddle ms-soften" ' +
    			'id="js-newdocWOPI-divFolder-txt-WPQ2">' + Viacom.NewDocumentCallOut.docSetList[index][1] + '</h3>' +
    		'</a></div>';
        }
    }
    callOutContent += newfolderContent + docSetHtml + '</div>';
    return callOutContent;
};

//Get Html of Template Content Type
Viacom.NewDocumentCallOut.getTemplateCallOutContent = function (ctx) {
    var linkname = document.getElementById("idHomePageNewDocument-" + ctx.wpq);
    if (linkname == null) linkname = document.getElementById("idHomePageNewItem");

    if (ctx.rootFolder != '') {

        var popUpPageParameterWord = '&quot;' + _spPageContextInfo.webAbsoluteUrl + '/_layouts/15/CreateNewDocument.aspx?id=' + _spPageContextInfo.webAbsoluteUrl + '/_catalogs/masterpage/BlankTemplates/WordTemplate.docx&SaveLocation=' + encodeURIComponent(ctx.rootFolder) + '&DefaultItemOpen=0&Source=' +
                             window.location.protocol + "//" + window.location.host + window.location.pathname + '&OpenIn=0';
        var popUpPageParameterExcel = '&quot;' + _spPageContextInfo.webAbsoluteUrl + '/_layouts/15/CreateNewDocument.aspx?id=' + _spPageContextInfo.webAbsoluteUrl + '/_catalogs/masterpage/BlankTemplates/Exceltemplate.xlsx&SaveLocation=' + encodeURIComponent(ctx.rootFolder) + '&DefaultItemOpen=0&Source=' +
                               window.location.protocol + "//" + window.location.host + window.location.pathname + '&OpenIn=0';
        var popUpPageParameterPowerPoint = '&quot;' + _spPageContextInfo.webAbsoluteUrl + '/_layouts/15/CreateNewDocument.aspx?id=' + _spPageContextInfo.webAbsoluteUrl + '/_catalogs/masterpage/BlankTemplates/PPTtemplate.pptx&SaveLocation=' + encodeURIComponent(ctx.rootFolder) + '&DefaultItemOpen=0&Source=' +
                                window.location.protocol + "//" + window.location.host + window.location.pathname + '&OpenIn=0';
    }
    else {
        var popUpPageParameterWord = '&quot;' + _spPageContextInfo.webAbsoluteUrl + '/_layouts/15/CreateNewDocument.aspx?id=' + _spPageContextInfo.webAbsoluteUrl + '/_catalogs/masterpage/BlankTemplates/WordTemplate.docx&SaveLocation=' + encodeURIComponent(ctx.listUrlDir) + '&DefaultItemOpen=0&Source=' +
                                 window.location.protocol + "//" + window.location.host + window.location.pathname + '&OpenIn=0';
        var popUpPageParameterExcel = '&quot;' + _spPageContextInfo.webAbsoluteUrl + '/_layouts/15/CreateNewDocument.aspx?id=' + _spPageContextInfo.webAbsoluteUrl + '/_catalogs/masterpage/BlankTemplates/Exceltemplate.xlsx&SaveLocation=' + encodeURIComponent(ctx.listUrlDir) + '&DefaultItemOpen=0&Source=' +
                               window.location.protocol + "//" + window.location.host + window.location.pathname + '&OpenIn=0';
        var popUpPageParameterPowerPoint = '&quot;' + _spPageContextInfo.webAbsoluteUrl + '/_layouts/15/CreateNewDocument.aspx?id=' + _spPageContextInfo.webAbsoluteUrl + '/_catalogs/masterpage/BlankTemplates/PPTtemplate.pptx&SaveLocation=' + encodeURIComponent(ctx.listUrlDir) + '&DefaultItemOpen=0&Source=' +
                                window.location.protocol + "//" + window.location.host + window.location.pathname + '&OpenIn=0';
    }
    var tabDiv = '<div id="js-newdocWOPI-divMain-WPQ2" class="ms-newdoc-callout-main" style="margin-left: 20px; margin-right: 20px;"><div class="callcoutTab"><ul>';

    var callOutContent = '<div class="callout-right">';
    if (Viacom.NewDocumentCallOut.hasOtherCT) {
        tabDiv += '<li><a href="#Documents">Documents</a></li>';
        callOutContent += '<div id="Documents" class="content">' +
            '<a id="js-newdocWOPI-divWord-WPQ2" class="ms-newdoc-callout-item ms-displayBlock" onclick="CalloutManager.closeAll(); OpenPopUpPageWithTitle(' +
                popUpPageParameterWord + '1&quot;, Viacom.NewDocumentCallOut.createDocumentCallBack); return false;" href="#">' +
                '<img id="js-newdocWOPI-divWord-img-WPQ2" src="/_layouts/15/images/lg_icdocx.png?rev=23" alt="Create a new Word document" class="ms-verticalAlignMiddle ms-newdoc-callout-img"/>' +
                '<h3 id="js-newdocWOPI-divWord-txt-WPQ2" class="ms-displayInline ms-newdoc-callout-text ms-verticalAlignMiddle ms-soften">Word document</h3>' +
            '</a>' +
            '<a id="js-newdocWOPI-divExcel-WPQ2" class="ms-newdoc-callout-item ms-displayBlock" onclick="CalloutManager.closeAll(); OpenPopUpPageWithTitle(' +
                popUpPageParameterExcel + '2&quot;, Viacom.NewDocumentCallOut.createDocumentCallBack); return false;" href="#">' +
                '<img id="js-newdocWOPI-divExcel-img-WPQ2" src="/_layouts/15/images/lg_icxlsx.png?rev=23" alt="Create a new Excel workbook" class="ms-verticalAlignMiddle ms-newdoc-callout-img"/>' +
                '<h3 id="js-newdocWOPI-divExcel-txt-WPQ2" class="ms-displayInline ms-newdoc-callout-text ms-verticalAlignMiddle ms-soften">Excel workbook</h3>' +
            '</a>' +
            '<a id="js-newdocWOPI-divPowerPoint-WPQ2" class="ms-newdoc-callout-item ms-displayBlock" onclick="CalloutManager.closeAll(); OpenPopUpPageWithTitle(' +
                popUpPageParameterPowerPoint + '3&quot;, Viacom.NewDocumentCallOut.createDocumentCallBack); return false;" href="#">' +
                '<img id="js-newdocWOPI-divPowerPoint-img-WPQ2" src="/_layouts/15/images/lg_icpptx.png?rev=23" alt="Create a new PowerPoint presentation" class="ms-verticalAlignMiddle ms-newdoc-callout-img"/>' +
                '<h3 id="js-newdocWOPI-divPowerPoint-txt-WPQ2" class="ms-displayInline ms-newdoc-callout-text ms-verticalAlignMiddle ms-soften">PowerPoint presentation</h3>' +
            '</a>' +
            '</div>';
    }
    for (var i = 0; i < Viacom.NewDocumentCallOut.docTemplateListData.length; i++) {
        var ctContentHtml = '';
        for (var j = 0; j < Viacom.NewDocumentCallOut.docTemplateListData[i].templateInfo.length; j++) {
            var templateName = Viacom.NewDocumentCallOut.docTemplateListData[i].templateInfo[j].templateName;
            var fileAbsUrl = Viacom.NewDocumentCallOut.docTemplateListData[i].templateInfo[j].fileAbsUrl;
            if (ctx.rootFolder != '') {
                var popUpPageParameterCustom = '&quot;' + _spPageContextInfo.webAbsoluteUrl + '/_layouts/15/CreateNewDocument.aspx?id=' + fileAbsUrl + '&SaveLocation=' + encodeURIComponent(ctx.rootFolder) + '&DefaultItemOpen=0&Source=' + window.location.protocol + "//" + window.location.host + window.location.pathname + '&OpenIn=0&isdlg=1';

            }
            else {
                var popUpPageParameterCustom = '&quot;' + _spPageContextInfo.webAbsoluteUrl + '/_layouts/15/CreateNewDocument.aspx?id=' + fileAbsUrl + '&SaveLocation=' + encodeURIComponent(ctx.listUrlDir) + '&DefaultItemOpen=0&Source=' + window.location.href + '&OpenIn=0&isdlg=1';
            }

            switch (Viacom.NewDocumentCallOut.docTemplateListData[i].templateInfo[j].fileExt) {
                case 'dotx':
                case 'docx':
                    ctContentHtml = ctContentHtml + '<a id="js-newdocWOPI-divWord-WPQ2" class="ms-newdoc-callout-item ms-displayBlock" onclick="CalloutManager.closeAll(); OpenPopUpPageWithTitle(' + popUpPageParameterCustom + '&quot;, Viacom.NewDocumentCallOut.createDocumentCallBack); return false;" href="#">' +
                      '<img id="js-newdocWOPI-divWord-img-WPQ2" src="/_layouts/15/images/lg_icdocx.png?rev=23" alt="Create a new Word document" class="ms-verticalAlignMiddle ms-newdoc-callout-img"/>' +
                      '<h3 id="js-newdocWOPI-divWord-txt-WPQ2" class="ms-displayInline ms-newdoc-callout-text ms-verticalAlignMiddle ms-soften">' + templateName + '</h3>' +
                   '</a>'
                    break;
                case 'xltx':
                case 'xlsx':
                    ctContentHtml = ctContentHtml + '<a id="js-newdocWOPI-divExcel-WPQ2" class="ms-newdoc-callout-item ms-displayBlock" onclick="CalloutManager.closeAll(); OpenPopUpPageWithTitle(' + popUpPageParameterCustom + '&quot;, Viacom.NewDocumentCallOut.createDocumentCallBack); return false;" href="#">' +
                      '<img id="js-newdocWOPI-divExcel-img-WPQ2" src="/_layouts/15/images/lg_icxlsx.png?rev=23" alt="Create a new Excel workbook" class="ms-verticalAlignMiddle ms-newdoc-callout-img"/>' +
                      '<h3 id="js-newdocWOPI-divExcel-txt-WPQ2" class="ms-displayInline ms-newdoc-callout-text ms-verticalAlignMiddle ms-soften">' + templateName + '</h3>' +
                   '</a>'
                    break;
                case 'potx':
                case 'pptx':
                    ctContentHtml = ctContentHtml + '<a id="js-newdocWOPI-divPowerPoint-WPQ2" class="ms-newdoc-callout-item ms-displayBlock" onclick="CalloutManager.closeAll(); OpenPopUpPageWithTitle(' + popUpPageParameterCustom + '&quot;, Viacom.NewDocumentCallOut.createDocumentCallBack); return false;" href="#">' +
                      '<img id="js-newdocWOPI-divPowerPoint-img-WPQ2" src="/_layouts/15/images/lg_icpptx.png?rev=23" alt="Create a new PowerPoint presentation" class="ms-verticalAlignMiddle ms-newdoc-callout-img"/>' +
                      '<h3 id="js-newdocWOPI-divPowerPoint-txt-WPQ2" class="ms-displayInline ms-newdoc-callout-text ms-verticalAlignMiddle ms-soften">' + templateName + '</h3>' +
                   '</a>'
                    break;
                default:
                    break;
            }
        }
        if (ctContentHtml != '') {
            callOutContent = callOutContent + '<div id="' + Viacom.NewDocumentCallOut.docTemplateListData[i].contentType.replace(/\s+/g, '') + '" class="content">' +
							ctContentHtml + '</div>';
            tabDiv = tabDiv + '<li><a href="#' + Viacom.NewDocumentCallOut.docTemplateListData[i].contentType.replace(/\s+/g, '') + '">' + Viacom.NewDocumentCallOut.docTemplateListData[i].contentType + '</a></li>';
        }
    }
    tabDiv = tabDiv + '</ul></div>';

    var newfolderContent = '';
    var folderEnabled = false;
    if (typeof ctx.AllowCreateFolder != 'undefined') {
        folderEnabled = true;
        var newFolderParameter = '&quot;' + _spPageContextInfo.webAbsoluteUrl + '/_layouts/15/listform.aspx?ListId=' + ctx.listName + '&amp;PageType=8&amp;RootFolder=' + ctx.rootFolder + '&amp;Type=1&quot;';

        newfolderContent = '<div class="newfolder"><hr/>' +
    		'<a class="ms-newdoc-callout-item ms-displayBlock newFolderAdjustment" id="js-newdocWOPI-divFolder-WPQ2" onclick="CalloutManager.closeAll(); NewItem2(event,'
    			+ newFolderParameter + ');return false;" href="#"><img class="ms-verticalAlignMiddle ms-newdoc-callout-img" id="js-newdocWOPI-divFolder-img-WPQ2" ' +
    			'alt="Create a new folder" src="/_layouts/15/images/mb_folder.png?rev=23"><h3 class="ms-displayInline ms-newdoc-callout-text ms-verticalAlignMiddle ms-soften" ' +
    			'id="js-newdocWOPI-divFolder-txt-WPQ2">New folder</h3>' +
    		'</a></div>';
    }
    var docSetHtml = '';
    if (Viacom.NewDocumentCallOut.docSetList.length > 0) {
        for (var index = 0; index <= Viacom.NewDocumentCallOut.docSetList.length - 1; index++) {
            //var popUpPageParameterDocSet = 'http://sp2013-ddev-3:7777/sites/adsalesclientrelations/_layouts/15/NewDocSet.aspx?List=29a23f45-2dde-47f2-a95e-f57038450603&Source=http%3A%2F%2Fsp2013-ddev-3%3A7777%2Fsites%2Fadsalesclientrelations%2FAdministrative%2FForms%2FAllItems%2Easpx&ContentTypeId=0x0120D520003E598A3D634746A5BFCBEA119B9F0F3D00DD4944EA3A76244F93D9FBB6839D7319&RootFolder=%2Fsites%2Fadsalesclientrelations%2FAdministrative';
            var popUpPageParameterDocSet = '&quot;' + _spPageContextInfo.webAbsoluteUrl + '/_layouts/15/NewDocSet.aspx?List=' + ctx.listName + '&Source=' + window.location.protocol + '//' + window.location.host + window.location.pathname + '&ContentTypeId=' + Viacom.NewDocumentCallOut.docSetList[index][0] + '&RootFolder=' + ctx.listUrlDir + '&quot';
            docSetHtml += '<div class="newfolder">';
            if (!folderEnabled && index == 0) {
                docSetHtml += '<hr/>';
            }
            docSetHtml += '<a class="ms-newdoc-callout-item ms-displayBlock newFolderAdjustment" id="js-newdocWOPI-divFolder-WPQ2" onclick="CalloutManager.closeAll(); NewItem2(event,'
    			+ popUpPageParameterDocSet + ');return false;" href="#"><img class="ms-verticalAlignMiddle ms-newdoc-callout-img" id="js-newdocWOPI-divFolder-img-WPQ2" ' +
    			'alt="Create a new folder" src="/_layouts/15/images/NewDocumentSet32.png?rev=23"><h3 class="ms-displayInline ms-newdoc-callout-text ms-verticalAlignMiddle ms-soften" ' +
    			'id="js-newdocWOPI-divFolder-txt-WPQ2">' + Viacom.NewDocumentCallOut.docSetList[index][1] + '</h3>' +
    		'</a></div>';
        }
    }
    callOutContent = tabDiv + callOutContent + '</div>' + newfolderContent + docSetHtml + '</div>';
    return callOutContent;
};

//Launch client app after document creation
Viacom.NewDocumentCallOut.createDocumentCallBack = function (result, returnValue) {

    if (result == SP.UI.DialogResult.OK && returnValue) {
        var sourcedoc = Viacom.NewDocumentCallOut.getQueryStringParamByName('sourcedoc', returnValue);
        var fileUrl = window.location.protocol + "//" + window.location.host + sourcedoc;
        var fileExtention = fileUrl.split('.').pop().split(/\#|\?/)[0];
        var strApp = '';
        Viacom.OpenInClientAppCommon.OpenInClientApp(fileUrl, fileExtention);
    }
    SetWindowRefreshOnFocus();
}

//OnCloseCallBack
Viacom.NewDocumentCallOut.closeCallBack = function (result, returnValue) {
    if (result == SP.UI.DialogResult.OK) {
        SP.UI.ModalDialog.RefreshPage(SP.UI.DialogResult.OK);
    } else {
        // logic 
    }
};

//Get query string parameter by name
Viacom.NewDocumentCallOut.getQueryStringParamByName = function (name, url) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(url);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};

//init with mds
Viacom.NewDocumentCallOut.initWithMDS = function () {
    var jsUrl = _spPageContextInfo.siteAbsoluteUrl + "/Style Library/JS/newdocumentcallOut.js";

    //Register init to rerun after ajax request
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(Viacom.NewDocumentCallOut.init);

    RegisterModuleInit(jsUrl, Viacom.NewDocumentCallOut.init);
};


// Add button 'Add To Templates Library' in ribbon
Viacom.AddToTemplateLibCustomAction.constants = function () {
    return {
        DialogBoxTitle: null,
        ListId: null,
        ItemId: null
    }
};

Viacom.AddToTemplateLibCustomAction.openAddDocPopup = function () {
    SP.UI.ModalDialog.showWaitScreenWithNoClose('Please wait...');
    Viacom.AddToTemplateLibCustomAction.constants.DialogBoxTitle = Viacom.AddToTemplateLibCustomAction.actionName;
    Viacom.AddToTemplateLibCustomAction.getAssociatedLibColChoiceVal();
};

Viacom.AddToTemplateLibCustomAction.getAssociatedLibColChoiceVal = function (listId, SeletedItemId) {
    try {
        $.ajax({
            url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('Templates')/Fields?$select=Choices&$filter=(InternalName eq 'AssociatedLibraries')&$orderby=Choices asc",
            method: "GET",
            async: false,
            headers: { "Accept": "application/json; odata=verbose" },
            success: function (data) {
                Viacom.AddToTemplateLibCustomAction.associatedLibraryChoiceValue = data.d.results[0].Choices.results;
                // Making data for add to template dialog
                var strOptions = '';
                for (var i = 0; i < Viacom.AddToTemplateLibCustomAction.associatedLibraryChoiceValue.length; i++) {
                    strOptions = strOptions + "<option value='" + i + "' >" + Viacom.AddToTemplateLibCustomAction.associatedLibraryChoiceValue[i] + "</option>";
                }
                var addToNewDocumentHtml = "<div id='divAddToTemplateLibCustomAction' class='addToTemplateLibCustomAction'>" +
								"<div class='error'><span></span></div>" +
								"<div class='addDocHeader'><span>Template Name</span>" +
								"<input type='text' name='TemplateName' value=''></div>" +
								"<div class='assciatedLists multipleChkbx'>" +
								"<span class='list-head'>Associated Libraries</span>" +
								"<select id='source' name='source' multiple='multiple'>" + strOptions + "</select></div>" +
								"<div class='div-buttons'>" +
								"<input type='button' value='Add' name='Add' onclick='Viacom.AddToTemplateLibCustomAction.addDocToTemplateLib()' />" +
								"<input type='button' value='Cancel' name='Cancel' onclick='Viacom.AddToTemplateLibCustomAction.cancel()'/></div></div>" +
								"<script>$(document).ready(function (){" +
								"$('#source').shuttle();$('#shuttle-control-src-dst').text('Add >');$('#shuttle-control-dst-src').text('< Remove');" +
								"$('#source').shuttle.transfer($('#source'),$('#shuttle-dst'));" +
								"});</script>"
                Viacom.AddToTemplateLibCustomAction.checkUserPermission(addToNewDocumentHtml);
            },
            error: function (jqXHR, textStatus, error) {
                console.log(error)
            }
        });
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
    }
};

Viacom.AddToTemplateLibCustomAction.checkUserPermission = function (templateDialogHtml) {
    var tempHtml = "<div id='divAddToTemplateLibCustomAction' class='addToTemplateLibCustomAction'>";
    var postTempHtml = "<input type='button' value='OK' style='float: right; margin-top: 30px;' onclick='Viacom.AddToTemplateLibCustomAction.cancel()'>";
    var dialogHtml = templateDialogHtml;
    $.ajax({
        url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists(guid'" + Viacom.AddToTemplateLibCustomAction.constants.ListId + "')/items(" + Viacom.AddToTemplateLibCustomAction.constants.ItemId + ")/file?$select=CheckOutType,Name,ServerRelativeUrl",
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (fileData) {
            if (fileData.d.CheckOutType != SP.CheckOutType.online) {
                var hasCurrenUserAddListItemPermission = '';
                //Ajax call to check permission of current user in Templates library
                $.ajax({
                    url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('" + Viacom.NewDocumentCallOut.TemplateLib + "')/EffectiveBasePermissions",
                    method: "GET",
                    dataType: "json",
                    headers: {
                        "Accept": "application/json;odata=verbose",
                        "content-type": "application/json;odata=verbose",
                        "X-RequestDigest": $('#__REQUESTDIGEST').val()
                    },
                    success: function (data) {
                        try {
                            var libPermissions = new SP.BasePermissions();
                            libPermissions.initPropertiesFromJson(data.d.EffectiveBasePermissions);
                            hasCurrenUserAddListItemPermission = libPermissions.has(SP.PermissionKind.addListItems);
                            if (hasCurrenUserAddListItemPermission) {
                                Viacom.AddToTemplateLibCustomAction.selectedFileName = fileData.d.Name;
                                Viacom.AddToTemplateLibCustomAction.selectedFileUrl = fileData.d.ServerRelativeUrl;
                                Viacom.AddToTemplateLibCustomAction.openDialog(Viacom.AddToTemplateLibCustomAction.constants.DialogBoxTitle, dialogHtml, 'divAddToTemplateLibCustomAction', 600, 350);
                            }
                            else {
                                tempHtml += Viacom.AddToTemplateLibCustomAction.permMessage;
                                tempHtml += postTempHtml + "</div>";
                                dialogHtml = tempHtml;
                                Viacom.AddToTemplateLibCustomAction.openDialog(Viacom.AddToTemplateLibCustomAction.messageDialogTitle, dialogHtml, 'divAddToTemplateLibCustomAction', 400, 70);
                            }
                        }
                        catch (e) {
                            console.log('Request failed. ' + e.message);
                            tempHtml += Viacom.AddToTemplateLibCustomAction.permMessage;
                            tempHtml += postTempHtml + "</div>";
                            dialogHtml = tempHtml;
                            Viacom.AddToTemplateLibCustomAction.openDialog(Viacom.AddToTemplateLibCustomAction.messageDialogTitle, dialogHtml, 'divAddToTemplateLibCustomAction', 400, 70);
                        }
                    },
                    error: function (err) {
                        console.log(error);
                        tempHtml += Viacom.AddToTemplateLibCustomAction.errorMessage;
                        tempHtml += postTempHtml + "</div>";
                        dialogHtml = tempHtml;
                        Viacom.AddToTemplateLibCustomAction.openDialog(Viacom.AddToTemplateLibCustomAction.messageDialogTitle, dialogHtml, 'divAddToTemplateLibCustomAction', 400, 70);
                    }
                });
            }
            else {
                tempHtml += Viacom.AddToTemplateLibCustomAction.fileCheckOutMessage;
                tempHtml += postTempHtml + "</div>";
                dialogHtml = tempHtml;
                Viacom.AddToTemplateLibCustomAction.openDialog(Viacom.AddToTemplateLibCustomAction.messageDialogTitle, dialogHtml, 'divAddToTemplateLibCustomAction', 400, 70);
            }
        },
        error: function (jqXHR, textStatus, error) {
            console.log(error);
            tempHtml += Viacom.AddToTemplateLibCustomAction.errorMessage;
            tempHtml += postTempHtml + "</div>";
            dialogHtml = tempHtml;
            Viacom.AddToTemplateLibCustomAction.openDialog(Viacom.AddToTemplateLibCustomAction.messageDialogTitle, dialogHtml, 'divAddToTemplateLibCustomAction', 400, 70);
        }
    });

};

Viacom.AddToTemplateLibCustomAction.openDialog = function (dialogTitle, dialogHtml, dialogHtmlContainerId, width, height) {
    try {
        $('body').append(dialogHtml);
        var diaOptions = SP.UI.$create_DialogOptions();
        diaOptions.html = document.getElementById(dialogHtmlContainerId);
        diaOptions.width = width;
        diaOptions.height = height;
        diaOptions.showClose = true;
        diaOptions.allowMaximize = false;
        diaOptions.title = dialogTitle;
        diaOptions.dialogReturnValueCallback = Function.createDelegate(null, Viacom.AddToTemplateLibCustomAction.closeCallBack);
        SP.UI.ModalDialog.commonModalDialogClose();
        SP.UI.ModalDialog.showModalDialog(diaOptions);
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        console.log('Request failed. ' + e.message);
    }
};

Viacom.AddToTemplateLibCustomAction.enableDisable = function () {
    var context = SP.ClientContext.get_current();
    var selectedItems = SP.ListOperation.Selection.getSelectedItems(context);
    Viacom.AddToTemplateLibCustomAction.constants();
    Viacom.AddToTemplateLibCustomAction.constants.ListId = SP.ListOperation.Selection.getSelectedList(context).replace(/[{}]/g, "");
    var ci = CountDictionary(selectedItems);
    if (ci == 1) {
        Viacom.AddToTemplateLibCustomAction.constants.ItemId = selectedItems[0].id;
        if (Viacom.NewDocumentCallOut.CurrentListTitle != Viacom.NewDocumentCallOut.TemplateLib) {
            Viacom.AddToTemplateLibCustomAction.enableButton = true;
        }
        else {
            Viacom.AddToTemplateLibCustomAction.enableButton = false;
        }
        return Viacom.AddToTemplateLibCustomAction.enableButton;
    }
    else {
        return false;
    }
};

Viacom.AddToTemplateLibCustomAction.addDocToTemplateLib = function () {
    Viacom.AddToTemplateLibCustomAction.waitDialog = SP.UI.ModalDialog.showWaitScreenWithNoClose('Processing...', '', 130, 340);
    try {
        var templateName = $('input:Text[name=TemplateName]').val();
        var associatedLibrariesResult = new Array();
        var listOptions = document.getElementById('shuttle-dst').options;
        for (var i = 0; i < listOptions.length; i++) {
            associatedLibrariesResult.push(listOptions[i].text);
        };
        var destinationlibUrl = _spPageContextInfo.webServerRelativeUrl + "/Templates/" + Viacom.AddToTemplateLibCustomAction.selectedFileName;

        $.ajax({
            url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/getfilebyserverrelativeurl('" + Viacom.AddToTemplateLibCustomAction.selectedFileUrl + "')/copyto(strnewurl='" + destinationlibUrl + "',boverwrite=true)",
            type: "POST",
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val(),
            },
            success: function (data) {
                console.log("Copy selected file to template library successfully");
                var context = SP.ClientContext.get_current();
                var currentWeb = context.get_web();
                context.load(currentWeb);
                var destFile = currentWeb.getFileByServerRelativeUrl(destinationlibUrl);
                destFile.checkOut();
                context.load(destFile);
                context.executeQueryAsync(
                    function (sender, args) {
                        var libFieldToUpdate = destFile.get_listItemAllFields();
                        context.load(libFieldToUpdate);
                        context.executeQueryAsync(
                            function (sender, args) {
                                libFieldToUpdate.set_item("AssociatedLibraries", associatedLibrariesResult);
                                libFieldToUpdate.set_item("TemplateName", templateName);
                                libFieldToUpdate.set_item("ShowInNewDocument", true);
                                libFieldToUpdate.update();
                                context.load(libFieldToUpdate);
                                destFile.checkIn();
                                context.load(destFile);
                                context.executeQueryAsync(
                                    function (sender, args) {
                                        console.log("Selected file added to template library.");
                                        Viacom.AddToTemplateLibCustomAction.waitDialog.close(1);
                                        SP.UI.ModalDialog.commonModalDialogClose(1);

                                        var message = STSHtmlEncode("Selected file added to template library.");

                                        // create sharepoint notification
                                        var notificationData = new SPStatusNotificationData("", message, _spPageContextInfo.siteAbsoluteUrl + "/PublishingImages/success.png", null);
                                        var notification = new SPNotification(SPNotifications.ContainerID.Status, "", false, null, null, notificationData);

                                        //show sharepoint notification tile
                                        notification.Show(false);
                                    }, function (sender, args) {
                                        console.log(args.get_message());
                                        Viacom.AddToTemplateLibCustomAction.waitDialog.close(0);
                                        $('.addToTemplateLibCustomAction div.error span').text("Failed to update file metadata properties in template library.");
                                        $('.addToTemplateLibCustomAction div.error').css('display', 'block');
                                    });
                            }, function (sender, args) {
                                console.log(args.get_message());
                                Viacom.AddToTemplateLibCustomAction.waitDialog.close(0);
                                $('.addToTemplateLibCustomAction div.error span').text("Failed to update file metadata properties in template library.");
                                $('.addToTemplateLibCustomAction div.error').css('display', 'block');
                            });
                    }, function (sender, args) {
                        console.log(args.get_message());
                        Viacom.AddToTemplateLibCustomAction.waitDialog.close(0);
                        $('.addToTemplateLibCustomAction div.error span').text("Failed to update file metadata properties in Template library.");
                        $('.addToTemplateLibCustomAction div.error').css('display', 'block');
                    });
            },
            error: function (jqXHR, textStatus, error) {
                console.log(error);
                Viacom.AddToTemplateLibCustomAction.waitDialog.close(0);
                $('.addToTemplateLibCustomAction div.error span').text("Failed to copy document in Template library.");
                $('.addToTemplateLibCustomAction div.error').css('display', 'block');
            }
        });

    }
    catch (e) {
        console.log('Request failed. ' + e.message);
        Viacom.AddToTemplateLibCustomAction.waitDialog.close(0);
        $('.addToTemplateLibCustomAction div.error span').text("Some error found to copy file to template library.");
        $('.addToTemplateLibCustomAction div.error').css('display', 'block');
    }
};

Viacom.AddToTemplateLibCustomAction.cancel = function () {
    SP.UI.ModalDialog.commonModalDialogClose(0);
};

Viacom.AddToTemplateLibCustomAction.closeCallBack = function (result) {
    SP.UI.ModalDialog.commonModalDialogClose(result);
};

//jquery  Function For Select Box
(function ($) {
    var methods = {
        init: function () {
            return this.each(function () {
                var $src = $(this);

                // Create the destination select box.
                var $dst = $('<select/>', {
                    id: 'shuttle-dst'
                });
                var $attr = $src.prop("attributes");

                // Copy the settings of the source selection to the destination.
                $.each($attr, function () {
                    if (this.name != 'id') {
                        if (this.name == 'name') {
                            $dst.attr(this.name, this.value + '-dst');
                        }
                        else {
                            $dst.attr(this.name, this.value);
                        }
                    }
                });

                // Provide button controls for transferring options between boxes.
                var $controls = $('<div id="shuttle-container-controls"/>').append('<button id="shuttle-control-src-dst">&raquo;</button>').append('<button id="shuttle-control-dst-src">&laquo;</button>');

                // Make the UI elements the same widths and heights.
                $dst.css('width', $src.width());
                $dst.attr('size', $src.attr('size'));

                // Organize the elements into a <div> hierarchy.
                $src.after($dst).after($controls);
                $src.wrap('<div id="shuttle-container-src" class="shuttle-container-src"/>');
                $dst.wrap('<div id="shuttle-container-dst" class="shuttle-container-dst"/>');
                $src.next().andSelf().wrapAll('<div id="shuttle-container-filter"/>');
                $('#shuttle-container-filter').nextUntil('#shuttle-container-dst').wrapAll('<div id="shuttle-container">');
                $('#shuttle-container-filter').
                prependTo('#shuttle-container');
                $('#shuttle-container-dst').
                appendTo('#shuttle-container');

                // Transfer all selected options from the source to the destination.
                $controls.find('#shuttle-control-src-dst').on('click', function (event) {
                    $.fn.shuttle.transfer($src, $dst);
                    return false;
                });

                // Transfer all selected options from the destionation to the source.
                $controls.find('#shuttle-control-dst-src').on('click', function (event) {
                    $.fn.shuttle.transfer($dst, $src);
                    return false;
                });

                // Create a copy of the source options to use when matching the regex.
                var $options = [];
                $src.find("option").each(function () {
                    $options.push({
                        value: $(this).val(),
                        text: $(this).text()
                    });
                });

                $src.data("options", $options);
            });
        }
    };

    $.fn.shuttle = function (method) {
        methods.init.apply(this);
    };

    $.fn.shuttle.transfer = function (src, dst) {
        dst.append($('option:selected', src).remove());
        var options = $("option", dst);
        options.sort(function (a, b) {
            return a.text.localeCompare(b.text);
        });
        dst.empty().append(options);
    };
})(jQuery);

//Page component implementation : Add button in ribbon and associate event handler to it.
Viacom.PageComponent = function () {
    Viacom.PageComponent.initializeBase(this);
}

Viacom.PageComponent.initializePageComponent = function () {
    var ribbonPageManager = SP.Ribbon.PageManager.get_instance();
    if (null !== ribbonPageManager) {
        var rbnInstance = Viacom.PageComponent.get_instance();
        ribbonPageManager.addPageComponent(rbnInstance);
    }
}

Viacom.PageComponent.get_instance = function () {
    if (Viacom.PageComponent.instance == null) {
        Viacom.PageComponent.instance = new Viacom.PageComponent();
    }
    return Viacom.PageComponent.instance;
}

Viacom.PageComponent.prototype = {
    //initialize params
    init: function () {
        this._handledCommands = new Object();
        this._handledCommands['RM.AddToTemplateLibrary'] = {
            enable: function (commandId) {
                //If control should be enabled
                return true;
            },
            handle: function (commandId, props, seq) {
                //Custom action click event execution start point
                if (commandId === 'RM.AddToTemplateLibrary') {
                    Viacom.AddToTemplateLibCustomAction.openAddDocPopup();
                }
            }
        };
        this._commands = ['RM.AddToTemplateLibrary'];
    },
    getFocusedCommands: function () { return []; },
    getGlobalCommands: function () { return this._commands; },
    canHandleCommand: function (commandId) {
        //Just call the enable function if available
        if (commandId === 'RM.AddToTemplateLibrary') {
            var returnValue = Viacom.AddToTemplateLibCustomAction.enableDisable();
            return returnValue;
        }
        else {
            return false;
        }
    },
    handleCommand: function (commandId, properties, sequence) {
        //Call handle
        return this._handledCommands[commandId].handle(commandId, properties, sequence);
    }
}

Viacom.AddToTemplateLibCustomAction.initRibbon = function () {
    var tabSelected = SP.Ribbon.PageManager.get_instance().get_ribbon().get_selectedTabCommand();
    if (tabSelected === "DocumentTab") {
        var actionCreated = document.getElementById('RM.AddToTemplateLibrary-Large');
        if (actionCreated === null) {
            var ribbon = (SP.Ribbon.PageManager.get_instance()).get_ribbon();
            var tab = ribbon.getChild("Ribbon.Document");
            var group = tab.getChild("Ribbon.Documents.Manage");
            var layout = group.getChild("Ribbon.Documents.Manage-LargeMedium");
            var section = layout.getChild("Ribbon.Documents.Manage-LargeMedium-0");
            var siteRelativeUrl = _spPageContextInfo.siteServerRelativeUrl;
            if (siteRelativeUrl.length > 1) {
                siteRelativeUrl += "/";
            }

            var btnProperties = new CUI.ControlProperties();
            btnProperties.Command = 'RM.AddToTemplateLibrary';
            btnProperties.Id = 'Ribbon.Library.Actions.AddToTemplateLibrary';
            btnProperties.TemplateAlias = 'o1';
            btnProperties.Image32by32 = siteRelativeUrl + 'PublishingImages/add-doc32.png';
            btnProperties.LabelText = Viacom.AddToTemplateLibCustomAction.actionName;

            var btnControl = new CUI.Controls.Button(ribbon, 'RM.AddToTemplateLibrary', btnProperties);
            var saveComponent = btnControl.createComponentForDisplayMode('Large');
            var row = section.getRow(1);
            row.addChild(saveComponent);
        }
    }
}

// Initialize new document callout
if (typeof _spPageContextInfo != "undefined" && _spPageContextInfo != null) {
    Viacom.NewDocumentCallOut.initWithMDS();
}
else {
    Viacom.NewDocumentCallOut.init();
}
//Binding ribbon selected event
$(document).on('ribbontabselected', function (e, selectedTabCommand) {
    Viacom.AddToTemplateLibCustomAction.initRibbon();
});

