
var Viacom = Viacom || {};
Viacom.AdSalesCustomActionDocShare = {}
var userGroups;
var enableButtonn;
Viacom.AdSalesCustomActionDocShare.buttonType;
Viacom.AdSalesCustomActionDocShare.fieldToFilter;

//Function called from Custom Action
Viacom.AdSalesCustomActionDocShare.ShareDocument = function (listID, itemID, buttonType) {
    Viacom.AdSalesCustomActionDocShare.buttonType = buttonType;
    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
        SP.UI.ModalDialog.showWaitScreenWithNoClose('Please wait...');
        Viacom.AdSalesCustomActionDocShare.GetClientData(listID, itemID, buttonType);
        //Viacom.AdSalesCustomActionDocShare.openDialog(listID, itemID, buttonType);
    });

};

//Function to open pagelayout page in dialog box 
//WebName == Site name
//WebTitle == Site name from site Url e.g "http://localhost/ACS0001" then WebTitle=ACS0001
Viacom.AdSalesCustomActionDocShare.openDialog = function (listID, ItemID, buttonType, webTitle) {
    try {
        var diaOptions = SP.UI.$create_DialogOptions();
        diaOptions.url = _spPageContextInfo.siteAbsoluteUrl + '/_catalogs/masterpage/AdSales/SourceWSShareDocumentsPageLayout.aspx?listID=' + listID + '&ItemID=' + ItemID + '&buttonType=' + buttonType + '&WebURL=' + _spPageContextInfo.webAbsoluteUrl + '&SiteURL=' + _spPageContextInfo.siteAbsoluteUrl + '&WebName=' + _spPageContextInfo.webTitle + '&WebTitle=' + webTitle;
        diaOptions.width = 700;
        diaOptions.height = 480;
        diaOptions.showClose = false;
        diaOptions.allowMaximize = false;
        diaOptions.title = "Share Document";
        diaOptions.dialogReturnValueCallback = Function.createDelegate(null, Viacom.AdSalesCustomActionDocShare.CloseCallBack);
        SP.UI.ModalDialog.commonModalDialogClose();
        SP.UI.ModalDialog.showModalDialog(diaOptions);
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        //alert("Error while opening popup For ShareDocument");
        console.log('Request failed. ' + e.message);
    }
};

//Popup close function
Viacom.AdSalesCustomActionDocShare.CloseCallBack = function (result, data) {
    if (typeof (data.d) != "undefined") {
        if (data.d.Key == "Success") {
            SP.UI.ModalDialog.commonModalDialogClose();
            Viacom.AdSalesCustomActionDocShare.openDialogMessage(data);

        }
        else {
            if (data.d.Key == "LinkToDoc") {
                SP.UI.ModalDialog.commonModalDialogClose();
                Viacom.AdSalesCustomActionDocShare.openDialogMessage(data);

            }
            else if (data.d.Key == 'Error') {
                SP.UI.ModalDialog.commonModalDialogClose();
                Viacom.AdSalesCustomActionDocShare.openDialogMessage(data);

            }
            else if (data.d.Key == 'ItemCheckedout') {
                Viacom.AdSalesCustomActionDocShare.openDialogMessage(data);

            }
            else {
                Viacom.AdSalesCustomActionDocShare.openDialogMessage(data);

            }
        }
    }
    else {
        if (data == 'CloseAndNotRefresh') {
            SP.UI.ModalDialog.commonModalDialogClose();
            //SP.UI.ModalDialog.RefreshPage(SP.UI.DialogResult.OK);
        }
        else if (data == 'CloseAndRefresh') {
            SP.UI.ModalDialog.RefreshPage(SP.UI.DialogResult.OK);

        }
        else {
            Viacom.AdSalesCustomActionDocShare.openDialogMessage(data);

        }
    }
};

Viacom.AdSalesCustomActionDocShare.openDialogMessage = function (data) {
    try {
        var message = "";
        var popUpCloseParameter = 'CloseAndNotRefresh';
        if (typeof (data.d) != "undefined") {
            message = data.d.Message;
            if (data.d.SharedURL != null) {
                popUpCloseParameter = 'CloseAndRefresh';
                var sharedUrl = "<a href='" + data.d.SharedURL + "' target='_blank'>" + Viacom.AdSalesCustomActionDocShare.buttonType + "</a>";
                message = message.replace('#SharedURL#', sharedUrl);
            }
        }
        else {
            //popUpCloseParameter = 'CloseAndNotRefresh';
            message = data;
        }
    }

    catch (e) {
        // SP.UI.ModalDialog.commonModalDialogClose();
        message = "Error while opening popup For ShareDocument.";
        // alert("Error while opening popup For ShareDocument");
        console.log('Request failed. ' + e.message);
    }

    //var htmlP = document.createElement('div');

    var htmlP = "<div id='ShareToAdSpaceMsgPopUp'><div class='ShareToAdSpaceMsg'>" + message + "</div><div style='text-align:center; margin-top: 20px;'><button type='button' class='shareToAddSpaceMesButton' onclick='SP.UI.ModalDialog.commonModalDialogClose(1, " + "&quot;" + popUpCloseParameter + "&quot;" + ")'>OK</button></div></div>";
    $('body').append(htmlP);
    var diaOptionsMsg = SP.UI.$create_DialogOptions();
    diaOptionsMsg.html = document.getElementById('ShareToAdSpaceMsgPopUp');
    diaOptionsMsg.width = 450;
    diaOptionsMsg.height = 130;
    diaOptionsMsg.showClose = false;
    diaOptionsMsg.allowMaximize = false;
    diaOptionsMsg.title = "Share Document";
    diaOptionsMsg.dialogReturnValueCallback = Function.createDelegate(null, Viacom.AdSalesCustomActionDocShare.CloseCallBack);
    // SP.UI.ModalDialog.commonModalDialogClose();
    SP.UI.ModalDialog.showModalDialog(diaOptionsMsg);

};

Viacom.AdSalesCustomActionDocShare.singleEnable = function () {

    var items = SP.ListOperation.Selection.getSelectedItems();
    var ci = CountDictionary(items);

    if (ci == 1) {
        if (enableButtonn == undefined) {
            var shareClientContext = new SP.ClientContext.get_current();
            var currentUser = shareClientContext.get_web().get_currentUser();
            shareClientContext.load(currentUser);

            userGroups = currentUser.get_groups();
            shareClientContext.load(userGroups);
            shareClientContext.executeQueryAsync(Viacom.AdSalesCustomActionDocShare.OnQuerySucceeded, Viacom.AdSalesCustomActionDocShare.OnQueryFailed);
        }
        else {
            return enableButtonn;
        }
    }
    else {
        return false;
    }
};

Viacom.AdSalesCustomActionDocShare.OnQuerySucceeded = function () {
    var groupName = 'Share To AdSpace Users';
    var groupsEnumerator = userGroups.getEnumerator();
    enableButtonn = false;
    while (groupsEnumerator.moveNext()) {
        var group = groupsEnumerator.get_current();
        if (group.get_title() == groupName) {
            enableButtonn = true;
            break;
        }
    }
    // Now we can call the Viacom.AdSalesCustomActionDocShare.singleEnable() function again
    RefreshCommandUI();
}

Viacom.AdSalesCustomActionDocShare.OnQueryFailed = function () {
    Viacom.AdSalesCustomActionDocShare.openDialogMessage('SPGroup permission is not configured properly');
    // alert('SPGroup permission is not configured properly');
}

//function to get client site related data from client data list
Viacom.AdSalesCustomActionDocShare.GetClientData = function (listID, itemID, buttonType) {
    try {
        if (buttonType === 'ClientSpace') {
            var filterValue = _spPageContextInfo.webServerRelativeUrl;
            Viacom.AdSalesCustomActionDocShare.fieldToFilter = "ClientSiteUrl";
            var clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl.split('/sites/')[0]);
            var oConfigList = clientContext.get_web().get_lists().getByTitle('Client Data');
            var camlQuery = new SP.CamlQuery();
            camlQuery.set_viewXml("<View><Query><Where><BeginsWith><FieldRef Name='" + Viacom.AdSalesCustomActionDocShare.fieldToFilter + "'/><Value Type='URL'>" + filterValue + "</Value></BeginsWith></Where></Query></View>");
            var collListItem = oConfigList.getItems(camlQuery);
            clientContext.load(collListItem, 'Include(ClientSiteUrl,ClientSequence)');
            clientContext.executeQueryAsync(
        function (sender, args) {
            var listItemEnumerator = collListItem.getEnumerator();
            var clientSiteExist = false;
            if (collListItem.get_count() > 0) {
                while (listItemEnumerator.moveNext()) {
                    var oListItem = listItemEnumerator.get_current();
                    if (oListItem.get_item('ClientSequence') != null && oListItem.get_item('ClientSiteUrl') != null) {
                        if (oListItem.get_item('ClientSiteUrl').$2_1.toLowerCase() === _spPageContextInfo.webAbsoluteUrl.toLowerCase()) {
                            var webTitle = "ASCS" + oListItem.get_item('ClientSequence');
                            Viacom.AdSalesCustomActionDocShare.openDialog(listID, itemID, buttonType, webTitle);
                            clientSiteExist = true;
                        }
                    }
                }
                if (!clientSiteExist) {
                    Viacom.AdSalesCustomActionDocShare.openDialogMessage("Client space site does not exist");
                }
            }
            else {
                Viacom.AdSalesCustomActionDocShare.openDialogMessage("Client space site does not exist");
            }
        });
        }
        else {

            // adspace
            var webTitle = _spPageContextInfo.webServerRelativeUrl.substring(_spPageContextInfo.siteServerRelativeUrl.length + 1);
            Viacom.AdSalesCustomActionDocShare.openDialog(listID, itemID, buttonType, webTitle);
        }

    }
    catch (e) {
        Viacom.AdSalesCustomActionDocShare.openDialogMessage('Something went wrong');
        console.log(e.message);

    }

}