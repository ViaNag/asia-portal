﻿var Viacom = Viacom || {};
Viacom.Chart = {}
Viacom.Chart.object = [];
Viacom.Chart.WorkItems = [];
Viacom.Chart.BusinessRecords = [];
Viacom.Chart.MonthArr = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
Viacom.Chart.BarGraphMonth = [];
Viacom.Chart.MyDoughnutChart = null;
Viacom.Chart.MyBarChart = null;
Viacom.Chart.ExcludeSiteArr = ["/sites/search", "/sites/colligoadminlite"]; //add exclude site collection name in lower case only
Viacom.Chart.Constants = function () {
    return {
        year: '',
        month: '',
        libraryUrl: 'RecordWiseUsage',
        webAppName: '',
        successCall: 0,
        fileNameBySite: '',
        monthCount: 0,
        selectedYear: '2016',
        libraryTitle: 'RecordWise Usage',
        monthNum: '',
        day: '',
    }
}


$(document).ready(function () {
    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
        Viacom.Chart.CreateGraph('#tabs-1');

    });

    $("#ddlSiteName").change(function () {
        $('#divBarMessage').hide();
        $('#legendBar').show();
        $('#barChartMonth').show();
        SP.UI.ModalDialog.showWaitScreenWithNoClose('Please wait...');
        //remove class 
        $("#ddlSiteName").removeClass('graph');
        if (Viacom.Chart.MyBarChart != null) {
            Viacom.Chart.MyBarChart.destroy();
        }
        Viacom.Chart.CreateBarGraph();

    });

    $("#ddlSiteNameTab1").change(function () {

        SP.UI.ModalDialog.showWaitScreenWithNoClose('Please wait...');
        $('#divDougMessage').hide();
        $('#legendDoughNut').show();
        $('#doughNutChart').show();
        //remove class 
        $("#doughNutChart").removeClass('graph');
        if (Viacom.Chart.MyDoughnutChart != null) {
            Viacom.Chart.MyDoughnutChart.destroy();
        }
        Viacom.Chart.CreateDoughnutCharts();

    });

    $("#ddlYear").change(function () {

        SP.UI.ModalDialog.showWaitScreenWithNoClose('Please wait...');
        $('#divBarMessage').hide();
        $('#legendBar').show();
        $('#barChartMonth').show();
        //remove class 
        $("#ddlSiteName").removeClass('graph');
        if (Viacom.Chart.MyBarChart != null) {
            Viacom.Chart.MyBarChart.destroy();
        }
        Viacom.Chart.SetDropDownValue('CreateBarGraph', false);

    });
});
//
Viacom.Chart.GetDateMonthYear = function (date) {

    var dateAttrribute = date.toDateString().split(' ');
    Viacom.Chart.Constants.year = dateAttrribute[3];
    Viacom.Chart.Constants.month = dateAttrribute[1];
    Viacom.Chart.Constants.day = dateAttrribute[2];
    Viacom.Chart.Constants.monthNum = date.getMonth();
    Viacom.Chart.Constants.libraryUrl = 'RecordWiseUsage';
    Viacom.Chart.Constants.libraryTitle = 'RecordWise Usage';

};

Viacom.Chart.CreateGraph = function (selectedTab) {
    //getting Current month
    var currentDate = new Date();
    Viacom.Chart.GetDateMonthYear(currentDate);
    Viacom.Chart.Constants();
    Viacom.Chart.Constants.successCall = 0;

    if (Viacom.Chart.Constants.webAppName == undefined) {
        var inputString = (_spPageContextInfo.webAbsoluteUrl).toUpperCase().split('HTTP://')[1];
        var fileNamePrefix = inputString.replace(/([-~!@#$%^&*()+=`{}\[\]\|\\:;'<>,.\/? ])+/g, '_');
        Viacom.Chart.Constants.webAppName = fileNamePrefix;
    }
    switch (selectedTab) {
        case '#tabs-1':
            if (($("#doughNutChart").attr('class')) != 'graph') {
                SP.UI.ModalDialog.showWaitScreenWithNoClose('Please wait...');
                $('#divDougMessage').hide();
                $('#legendDoughNut').show();
                $('#doughNutChart').show();
                //remove class 
                $("#doughNutChart").removeClass('graph');
                var ddlLength = document.getElementById("ddlSiteNameTab1")
                if (ddlLength.options.length == 0) {
                    Viacom.Chart.SetDropDownValue('CreateDoughnutCharts', true);
                }
            }
            //code block
            break;
        case '#tabs-2':
            // code block
            if (($("#barChartMonth").attr('class')) != 'graph') {
                SP.UI.ModalDialog.showWaitScreenWithNoClose('Please wait...');
                $('#divBarMessage').hide();
                $('#barChartMonth').show();
                $('#legendBar').show();

                //remove class 
                $("#barChartMonth").removeClass('graph');
                var ddlLength = document.getElementById("ddlSiteName");
                if (ddlLength.options.length == 0) {
                    Viacom.Chart.SetDropDownValue('CreateBarGraph', true);
                }
                else {
                    ddlLength = document.getElementById("ddlYear");
                    if (ddlLength.options.length == 0) {
                        Viacom.Chart.SetYearDropDown();
                    }
                    else {
                        Viacom.Chart.Constants.selectedYear = $("#ddlYear").val();
                        Viacom.Chart.CreateBarGraph('Web');
                    }

                }


            }
            break;
        default:
            //default code block
    }

};

Viacom.Chart.ReadDataFromRercordWiseList = function (fileName, callingFunctionName, readXMLFileCount, siteOrWeb) {
    var itemCount = 0;
    var month = '';
    if (callingFunctionName != 'CreateBarGraph') {
        $.ajax({
            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/GetByTitle('" + Viacom.Chart.Constants.libraryTitle + "')/items?$filter= Title eq '" + fileName + "'",
            method: "GET",
            headers: { "Accept": "application/json; odata=verbose" },
            success: function (data) {
                Viacom.Chart.Constants.successCall++;
                if (data != null) {
                    if (data.d.results.length > 0) {
                        switch (callingFunctionName) {
                            case 'CreateDoughnutCharts':
                                {

                                    var workItems = data.d.results[0]["WorkItems"];
                                    var businessRecords = data.d.results[0]["BusinessRecords"];
                                    if (workItems === '0' && businessRecords === '0') {
                                        $("#doughNutChart").addClass('graph');
                                        Viacom.Chart.ErrorMessage("No file exist for this site", null, callingFunctionName);
                                    }
                                    else {
                                        Viacom.Chart.SetDoughnutChartsAxis(data)

                                    }
                                }
                                break;

                        }
                    }
                    else {
                        $("#doughNutChart").addClass('graph');
                        Viacom.Chart.ErrorMessage("No file exist for this site", null, callingFunctionName);
                    }
                }
                else {
                    $("#doughNutChart").addClass('graph');
                    Viacom.Chart.ErrorMessage("No file exist for this site", null, callingFunctionName);
                }

            },
            error: function (data) {
                Viacom.Chart.ErrorMessage("Error Occured while fetching graph list data", data, callingFunctionName);
            }
        });
    }
    else {
        // ordery by created date required so that data come in sequence corresponding to month
        var url = '';
        if ($("#ddlSiteName").prop('selectedIndex') === 0) {
            url = _spPageContextInfo.webAbsoluteUrl + "/_api/web/Lists/GetByTitle('" + Viacom.Chart.Constants.libraryTitle + "')/GetItems(query=@v1)?$select=ReportDate,WorkItems,BusinessRecords&@v1={'FolderServerRelativeUrl' : '" + Viacom.Chart.Constants.libraryUrl + "/" + Viacom.Chart.Constants().selectedYear + "', 'ViewXml' : '<View Scope=RecursiveAll><Query><OrderBy><FieldRef Name=Created Ascending=True/></OrderBy><Where><And><Eq><FieldRef Name=Url /><Value Type=Text>" + fileName + "</Value></Eq><Eq><FieldRef Name=DataType /><Value Type=Text>" + "WebApp" + "</Value></Eq></And></Where></Query></View>'}";
        }
        else {
            url = _spPageContextInfo.webAbsoluteUrl + "/_api/web/Lists/GetByTitle('" + Viacom.Chart.Constants.libraryTitle + "')/GetItems(query=@v1)?$select=ReportDate,WorkItems,BusinessRecords&@v1={'FolderServerRelativeUrl' : '" + Viacom.Chart.Constants.libraryUrl + "/" + Viacom.Chart.Constants().selectedYear + "', 'ViewXml' : '<View Scope=RecursiveAll><Query><OrderBy><FieldRef Name=Created Ascending=True/></OrderBy><Where><And><Eq><FieldRef Name=Url /><Value Type=Text>" + fileName + "</Value></Eq><Eq><FieldRef Name=DataType /><Value Type=Text>" + "Site" + "</Value></Eq></And></Where></Query></View>'}";

        }
        $.ajax({
            url: url,
            method: "POST",
            headers: {
                "Accept": "application/json; odata=verbose",
                "Content-Type": "application/json; odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
            },
            success: function (data) {
                if (data != null) {
                    if (data.d.results.length != 0) {
                        // Viacom.Chart.Constants.monthNum  and index loop not required and check condition for selected year 2016
                        var monthIndex = '';
                        //var index = 0;
                        // if (Viacom.Chart.Constants.selectedYear === '2016') { monthIndex = 2; }
                        // else { monthIndex = 0; }
                        for (var index = 0; index < data.d.results.length; index++) {
                            var reportDate = new Date(data.d.results[index]["ReportDate"]);

                            Viacom.Chart.BarGraphMonth.push([Viacom.Chart.MonthArr[reportDate.getMonth()]]);
                            Viacom.Chart.WorkItems.push([data.d.results[index]["WorkItems"]]);
                            Viacom.Chart.BusinessRecords.push([data.d.results[index]["BusinessRecords"]]);
                            itemCount++;
                            //monthIndex++;
                        }
                        if (itemCount > 0) {
                            //item count to set width of graph
                            Viacom.Chart.SetBarChartsAxis(itemCount);
                        }
                        else {
                            $("#barChartMonth").addClass('graph');
                            Viacom.Chart.ErrorMessage("No file exist for this site", null, callingFunctionName);
                        }

                    }
                    else {
                        $("#barChartMonth").addClass('graph');
                        Viacom.Chart.ErrorMessage("No file exist for this site", null, callingFunctionName);

                    }

                }
                else {
                    $("#barChartMonth").addClass('graph');
                    Viacom.Chart.ErrorMessage("No file exist for this site", null, callingFunctionName);

                }

            },

            error: function (data) {
                $("#barChartMonth").addClass('graph');
                Viacom.Chart.ErrorMessage("Error Occured while fetching graph list data", data, callingFunctionName);


            }
        });
    }


};

Viacom.Chart.SetDoughnutChartsAxis = function (data) {

    var ctx = $("#doughNutChart").get(0).getContext("2d");
    //calling getdata for doughnut graph
    var doughnutGraphDataInJson = Viacom.Chart.GetDataForDoughnutGraphInJson(data);
    //Options
    var options = {
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke: true,

        //String - The colour of each segment stroke
        segmentStrokeColor: "#fff",

        //Number - The width of each segment stroke
        segmentStrokeWidth: 2,

        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts

        //Number - Amount of animation steps
        animationSteps: 100,

        //String - Animation easing effect
        animationEasing: "easeOutBounce",

        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate: true,

        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale: false,

        //String - A legend template
        legendTemplate: "<%for (var i=0; i<segments.length; i++){%><div class=\"chart-legend-cell\"><svg class=\"legend-color-container\" focusable=\"false\" width=\"14\" height=\"14\"><rect class=\"legend-color\" groupid=\"0\" x=\"0\" y=\"0\" width=\"12\" height=\"12\" fill=\"<%=segments[i].fillColor%>\"></rect></svg><div class=\"legend-label\" style=\"max-width: 79px;\"><%if(segments[i].label){%><%=segments[i].label%><%}%></div></div><%}%>"

    }
    Chart.types.Doughnut.extend({
        name: "CustomDoughNut",
        draw: function () {
            Chart.types.Doughnut.prototype.draw.apply(this, arguments);

            var width = this.chart.width,
                height = this.chart.height;

            var fontSize = (height / 180).toFixed(2);
            this.chart.ctx.font = fontSize + "em Verdana";
            this.chart.ctx.textBaseline = "middle";
            this.chart.ctx.fillStyle = "#000";

            var text = parseInt(data.d.results[0]["WorkItems"]) + parseInt(data.d.results[0]["BusinessRecords"]),
                textX = Math.round(width / 2),
                textY = height / 2;

            this.chart.ctx.fillText(text, textX, textY);
            onanimationend: Viacom.Chart.DoughNutLabel(Viacom.Chart.MyDoughnutChart);
        }
    });

    Viacom.Chart.MyDoughnutChart = new Chart(ctx).CustomDoughNut(doughnutGraphDataInJson, options);
    var legend = Viacom.Chart.MyDoughnutChart.generateLegend();
    document.getElementById('legendDoughNut').innerHTML = legend;
    $("#doughNutChart").addClass('graph');

};

Viacom.Chart.CreateDoughnutCharts = function (siteOrWeb) {

    var fileName = $("#ddlSiteNameTab1").val();
    Viacom.Chart.ReadDataFromRercordWiseList(fileName, 'CreateDoughnutCharts');

};

Viacom.Chart.GetDataForDoughnutGraphInJson = function (data) {


    var workItems = data.d.results[0]["WorkItems"];
    var businessRecords = data.d.results[0]["BusinessRecords"];
    var doughnutGraphData = []

    if (workItems > 0) {
        doughnutGraphData.push({
            value: workItems,
            color: "#009BCC",
            label: "Work Items"
        });
    }
    if (businessRecords > 0) {
        doughnutGraphData.push({
            value: businessRecords,
            color: "#F58B1F",
            label: "Business Records"
        });
    }
    return doughnutGraphData;
};

Viacom.Chart.SetDropDownValue = function (functionName, isFistTabSelected, isRecursive) {
    try {
        var url = '';
        var option = '';
        var month = '';
        if (isRecursive === undefined) {
            month = Viacom.Chart.Constants.month;

        }
        else {
            month = Viacom.Chart.MonthArr[Viacom.Chart.Constants.monthNum - 1].substring(0, 3);
        }
        if (Viacom.Chart.Constants().selectedYear === Viacom.Chart.Constants.year) {
            url = _spPageContextInfo.webAbsoluteUrl + "/_api/web/Lists/GetByTitle('" + Viacom.Chart.Constants.libraryTitle + "')/GetItems(query=@v1)?$select=Title,DataType,Url,SiteCollectionName&@v1={'FolderServerRelativeUrl' : '" + Viacom.Chart.Constants.libraryUrl + "/" + Viacom.Chart.Constants().selectedYear + "/" + month + "'}";//"/_api/search/query?querytext='contentclass:sts_site+path:" + _spPageContextInfo.webAbsoluteUrl + "/*'&selectproperties='Path,Title'&trimduplicates=false&rowlimit=500";
        }
        else {
            url = _spPageContextInfo.webAbsoluteUrl + "/_api/web/Lists/GetByTitle('" + Viacom.Chart.Constants.libraryTitle + "')/GetItems(query=@v1)?$select=Title,DataType,Url,SiteCollectionName&@v1={'FolderServerRelativeUrl' : '" + Viacom.Chart.Constants.libraryUrl + "/" + Viacom.Chart.Constants().selectedYear + "/Dec'}";

        }
        $.ajax({
            url: url,
            method: "POST",
            headers: {
                "Accept": "application/json; odata=verbose",
                "Content-Type": "application/json; odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
            },
            success: function (data) {
                if (data != null) {
                    if (data.d.results.length > 0) {
                        $.each(data.d.results, function (key, value) {
                            if (value.DataType != 'WebApp') {
                                if (value.Url != null) {
                                    var arraySite = value.Url.split(_spPageContextInfo.siteAbsoluteUrl);
                                    var excludeSitCollUrl = arraySite[arraySite.length - 1];
                                }
                                if (Viacom.Chart.ExcludeSiteArr.indexOf(excludeSitCollUrl.toLowerCase()) < 0) {
                                    if (value.Url.toLowerCase() === (_spPageContextInfo.siteAbsoluteUrl).toLowerCase()) {
                                        option = "<option value='" + value.Url + "'>" + "SVP" + "</option>";
                                        $("#ddlSiteName").append(option);
                                        if (isFistTabSelected) {
                                            option = "<option value='" + value.Title + "'>" + "SVP" + "</option>";
                                            $("#ddlSiteNameTab1").append(option);
                                        }
                                    }

                                    else {
                                        option = "<option value='" + value.Url + "'>" + value.SiteCollectionName + "</option>";
                                        $("#ddlSiteName").append(option);
                                        if (isFistTabSelected) {
                                            option = "<option value='" + value.Title + "'>" + value.SiteCollectionName + "</option>";
                                            $("#ddlSiteNameTab1").append(option);
                                        }
                                    }
                                }
                            }
                            else {

                                if (key > 0) {
                                    if (isFistTabSelected) {
                                        $("#ddlSiteNameTab1 option").eq(0).before($("<option></option>").val(value.Title).text("All"));
                                    }
                                    $("#ddlSiteName option").eq(0).before($("<option></option>").val(value.Url).text("All"));
                                }
                                else {
                                    option = "<option value='" + value.Url + "'>All</option>";
                                    $("#ddlSiteName").append(option);
                                    if (isFistTabSelected) {
                                        option = "<option value='" + value.Title + "'>All</option>";
                                        $("#ddlSiteNameTab1").append(option);
                                    }
                                }
                            }
                        });
                        //switch statement to call
                        switch (functionName) {
                            case 'CreateDoughnutCharts':
                                Viacom.Chart.CreateDoughnutCharts('Web');
                                break;
                            case 'CreateBarGraph':
                                ddlLength = document.getElementById("ddlYear");
                                if (ddlLength.options.length == 0) {
                                    Viacom.Chart.SetYearDropDown();
                                }
                                else {
                                    Viacom.Chart.Constants.selectedYear = $("#ddlYear").val();
                                    Viacom.Chart.CreateBarGraph('Web');
                                }

                                break;
                            default:
                                break;
                        }

                    }
                        // fetching data for dropdown if data not available for current month(generally it will occurs on 1st of each month) 
                    else {
                        if (Viacom.Chart.Constants.day === 1) {
                            Viacom.Chart.SetDropDownValue(functionName, isFistTabSelected, true);
                        }
                    }
                }

            },

            error: function (data) {
                Viacom.Chart.ErrorMessage("Error while binding site dropdown", data)
            }
        });
    }
    catch (e) {
        Viacom.Chart.ErrorMessage("Error while binding site dropdown", e)
    }

};

Viacom.Chart.CreateBarGraph = function (siteOrWeb) {
    Viacom.Chart.BarGraphMonth = [];
    Viacom.Chart.WorkItems = [];
    Viacom.Chart.BusinessRecords = [];
    Viacom.Chart.Constants.monthCount = 0;
    var fileName = $("#ddlSiteName").val();
    Viacom.Chart.ReadDataFromRercordWiseList(fileName, 'CreateBarGraph', '', siteOrWeb);



}

Viacom.Chart.SetBarChartsAxis = function (monthCount) {
    var barData = {
        labels: Viacom.Chart.BarGraphMonth,
        datasets: [
            {
                label: "Work Items",
                fillColor: "#009BCC",
                strokeColor: "#009BCC",
                data: Viacom.Chart.WorkItems
            },
        {
            label: "Business Records",
            fillColor: "#F58B1F",
            strokeColor: "#F58B1F",
            data: Viacom.Chart.BusinessRecords
        }

        ]
    }
    var options = {

        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero: true,

        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: true,

        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",

        //Number - Width of the grid lines
        scaleGridLineWidth: 1,

        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,

        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,

        //Boolean - If there is a stroke on each bar
        barShowStroke: true,

        //Number - Pixel width of the bar stroke
        barStrokeWidth: .5,

        //Number - Spacing between each of the X value sets
        barValueSpacing: 5,

        //Number - Spacing between data sets within X values
        barDatasetSpacing: 1,
        scaleShowLabels: true,
        showInlineValues: true,

        // String - Tooltip label font declaration for the scale label
        tooltipFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

        // Number - Tooltip label font size in pixels
        tooltipFontSize: 12,

        // String - Tooltip font weight style
        tooltipFontStyle: "normal",

        // String - Tooltip label font colour
        tooltipFontColor: "#fff",

        // String - Tooltip title font declaration for the scale label
        tooltipTitleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

        // Number - Tooltip title font size in pixels
        tooltipTitleFontSize: 12,

        // String - Tooltip title font weight style
        tooltipTitleFontStyle: "bold",

        // String - Tooltip title font colour
        tooltipTitleFontColor: "#fff",

        // Number - pixel width of padding around tooltip text
        tooltipYPadding: 2,

        // Number - pixel width of padding around tooltip text
        tooltipXPadding: 2,

        // Number - Size of the caret on the tooltip
        tooltipCaretSize: 2,

        // Number - Pixel radius of the tooltip border
        tooltipCornerRadius: 6,

        // Number - Pixel offset from point x to tooltip edge
        tooltipXOffset: 10,

        //String - A legend template
        legendTemplate: "<%for (var i=0; i<datasets.length; i++){%><div class=\"chart-legend-cell\"><svg class=\"legend-color-container\" focusable=\"false\" width=\"14\" height=\"14\"><rect class=\"legend-color\" groupid=\"0\" x=\"0\" y=\"0\" width=\"12\" height=\"12\" fill=\"<%=datasets[i].fillColor%>\"></rect></svg><div class=\"legend-label\" style=\"max-width: 79px;\"><%if(datasets[i].label){%><%=datasets[i].label%><%}%></div></div><%}%>"


    }
    //new Chart(income).Bar(barData, options);
    var canvas = document.getElementById("barChartMonth");
    if (monthCount <= 2) {
        canvas.width = 100 * 3;
    }
    else {
        canvas.width = 100 * monthCount;
    }
    canvas.height = 300;
    var barChart = document.getElementById("barChartMonth").getContext("2d");
    Chart.types.Bar.extend({
        name: "CustomBar",
        draw: function () {
            Chart.types.Bar.prototype.draw.apply(this, arguments);
            onanimationend: Viacom.Chart.BarLabel(Viacom.Chart.MyBarChart);
        }
    });


    Viacom.Chart.MyBarChart = new Chart(barChart).CustomBar(barData, options);
    var legend = Viacom.Chart.MyBarChart.generateLegend();
    document.getElementById('legendBar').innerHTML = legend;
    $("#barChartMonth").addClass('graph');

};

Viacom.Chart.ErrorMessage = function (message, data, callingFunctionName) {
    if (callingFunctionName === 'CreateBarGraph') {
        $('#divBarMessage').html("No data available");
        $('#divBarMessage').show();
        $('#legendBar').hide();
        $('#barChartMonth').hide();
    }
    if (callingFunctionName === 'CreateDoughnutCharts') {
        $('#divDougMessage').html("No data available");
        $('#divDougMessage').show();
        $('#legendDoughNut').hide();
        $('#doughNutChart').hide();
    }
    SP.UI.ModalDialog.commonModalDialogClose();
    if (data != null)
        console.log(data.responseJSON.error.message.value);
};

Viacom.Chart.SetYearDropDown = function () {
    try {
        for (i = new Date().getFullYear() ; i >= 2016; i--) {
            $('#ddlYear').append($('<option />').val(i).html(i));
            $("#ddlYear").attr('selectedIndex', 0);
        }
        Viacom.Chart.Constants.selectedYear = $("#ddlYear").val();
        Viacom.Chart.CreateBarGraph('Web');
    }
    catch (e) {
        Viacom.Chart.ErrorMessage("Error while binding year dropdown", e)
    }
};

Viacom.Chart.BarLabel = function (dChart) {
    if (dChart.options.showInlineValues) {
        if (dChart.name == "CustomBar") {
            dChart.eachBars(function (bar) {
                new Chart.Tooltip({
                    x: Math.round(bar.x),
                    y: Math.round(bar.tooltipPosition().y),
                    xPadding: dChart.options.tooltipXPadding,
                    yPadding: '-4',
                    fillColor: "transparent",
                    textColor: "#000",
                    fontFamily: dChart.options.tooltipFontFamily,
                    fontStyle: "bold",
                    fontSize: 14,
                    caretHeight: dChart.options.tooltipCaretSize,
                    cornerRadius: dChart.options.tooltipCornerRadius,
                    text: bar.value,
                    chart: dChart.chart
                }).draw();
            });
        }
    }
    SP.UI.ModalDialog.commonModalDialogClose();

};

Viacom.Chart.DoughNutLabel = function (dChart) {
    if (dChart.name === "CustomDoughNut") {
        var segments = dChart.segments;
        $.each(segments, function (index, bar) {
            var tooltipPosition = Viacom.Chart.tooltipPosition(bar);
            new Chart.Tooltip({
                x: Math.round(tooltipPosition.x),
                y: Math.round(tooltipPosition.y),
                xPadding: dChart.options.tooltipXPadding,
                yPadding: -10,
                fillColor: "transparent",
                textColor: dChart.options.tooltipFillColor,
                fontFamily: dChart.options.tooltipFontFamily,
                fontStyle: "bold",
                fontSize: 16,
                caretHeight: dChart.options.tooltipCaretSize,
                cornerRadius: dChart.options.tooltipCornerRadius,
                text: bar.value,
                chart: dChart.chart
            }).draw();
        });
    }
    SP.UI.ModalDialog.commonModalDialogClose();
}

Viacom.Chart.tooltipPosition = function (a) {
    var ang = 10;
    if (a.endAngle != undefined) {
        ang = a.endAngle;
    }
    var centreAngle = a.startAngle + ((ang - a.startAngle) / 2);
    var rangeFromCentre = (a.outerRadius - a.innerRadius) / 2 + a.innerRadius;
    return {
        x: a.x + (Math.cos(centreAngle) * rangeFromCentre),
        y: a.y + (Math.sin(centreAngle) * rangeFromCentre)
    };

};



