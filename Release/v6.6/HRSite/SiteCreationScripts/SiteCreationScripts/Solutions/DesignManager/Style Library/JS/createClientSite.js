﻿var Viacom = Viacom || {};
Viacom.clientGroups = {};
Viacom.createClientWeb = Viacom.createClientWeb || {};
Viacom.clientSiteTaxonomy = Viacom.clientSiteTaxonomy || {};
Viacom.clientGroups.Constants = function () {
    return {
        mmsName: null,
        objTaxClient: {},
        clientCounter: null,
        newSiteUrl: null,
        objClientPopup: null,
        objClientRelatedData: null,
        agencyTypeCount: null,
        termAgTypeArr: null,
        agencytypeLength: null
    }
};

Viacom.clientGroups.Initialize = function () {
    Viacom.clientGroups.Constants.siteLevel = "Client";
    Viacom.clientGroups.Constants.lstGrp = "Site Groups Mapping";
    Viacom.clientGroups.Constants.mmsName = "Managed Metadata Service";
    Viacom.clientGroups.Constants.clientCounter = null;
    Viacom.clientGroups.Constants.newSiteUrl = null;
    Viacom.clientGroups.Constants.objTaxClient = {};
    Viacom.clientGroups.Constants.objClientPopup = {};
    Viacom.clientGroups.Constants.termAgTypeArr = [[]];
    Viacom.clientGroups.Constants.agencytypeLength = null;
    Viacom.clientGroups.Constants.objTaxClient.taxClientCategoryValue = [];
    Viacom.clientGroups.Constants.objTaxClient.taxClientSegmentValue = [];
    Viacom.clientGroups.Constants.objTaxClient.taxClientName = [];
    Viacom.clientGroups.Constants.objTaxClient.assignedCpd = [];
    Viacom.clientGroups.Constants.objTaxClient.allAgencyType = [[]];
    Viacom.clientGroups.Constants.objClientRelatedData = {};
    Viacom.clientGroups.Constants.objClientRelatedData.svpReg = [];
    Viacom.clientGroups.Constants.objClientRelatedData.svpType = [];
    Viacom.clientGroups.Constants.objClientRelatedData.svpUrl = null;
    Viacom.clientGroups.Constants.objClientRelatedData.agencyId = [];
    Viacom.clientGroups.Constants.objClientRelatedData.primaryAgSVPTm = [];
    Viacom.clientGroups.Constants.objClientRelatedData.primaryAgencyNm = [];
    Viacom.clientGroups.Constants.objClientRelatedData.digitalAgencyNm = [];
    Viacom.clientGroups.Constants.objClientRelatedData.planningAgencyNm = [];
    Viacom.clientGroups.Constants.clientDetails = new Array();
    Viacom.clientGroups.Constants.agencyTypeCount = null;
};

Viacom.createClientWeb.ValidateFields = function (Action) {
    try {


        if (Action != "create") {
            var taxClientName = $("span[id*='TaxonomyClientNameEdit']")[0].children[0];

            if (taxClientName.hasAttribute('value') && (taxClientName.value.length > 0) && taxClientName.value.split('|')[1] != '00000000-0000-0000-0000-000000000000') {
                Viacom.clientGroups.Constants.validateClientName = true;
                if (Action == "populate") {
                    Viacom.clientGroups.Constants.previousClientName = taxClientName.value;
                    return true;
                }
            }
            else {
                alert('Please select a valid client');
                return false;
            }

            if (Action == "edit") {
                var ClientUrl = $("#preClientUrlEdit").val();
                var taxClientCategoryEdit = $("span[id*='ClientCategoryEdit']")[0].children[0].value;
                var taxClientSegmentEdit = $("span[id*='ClientSegmentEdit']")[0].children[0].value;

                //get the CPD users
                //Viacom.createClientWeb.getPickerInputElement("assignedCpdsEdit")
                //var assignedCpdEdit = Viacom.clientGroups.Constants.objTaxClient.assignedCpd.length;


                if (taxClientName.value.length > 0 && taxClientName.value.split('|')[1] != '00000000-0000-0000-0000-000000000000' && ClientUrl.length > 0 && taxClientCategoryEdit.length > 0 && taxClientCategoryEdit.split('|')[1] != '00000000-0000-0000-0000-000000000000' && taxClientSegmentEdit.length > 0 && taxClientSegmentEdit.split('|')[1] != '00000000-0000-0000-0000-000000000000' && Viacom.clientGroups.Constants.validateClientName) {

                    if (taxClientName.value != Viacom.clientGroups.Constants.previousClientName) {
                        alert('Please click on search icon to fetch the details');
                        return false;
                    }
                    else {
                        return true;
                    }
                }
                else {
                    alert("Please fill all the required fields")
                    return false;

                }

            }
        }

        var taxClientNm = $('#clientNm').val();
        var clientCatValue = Viacom.clientGroups.Constants.objTaxClient.taxClientCategoryValue.length;
        var clientSegValue = Viacom.clientGroups.Constants.objTaxClient.taxClientSegmentValue.length;
        var agencyTypeCount = Viacom.clientGroups.Constants.agencytypeLength;
        var ClientURL = Viacom.clientGroups.Constants.newSiteUrl.split("/sites/clients/")[1];
        if (!(taxClientNm.length > 0 && clientCatValue > 0 && clientSegValue > 0 && ClientURL != null)) {
            return false;
        }
        else {
            return true;
        }


    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error while validating client fields");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createClientWeb.ValidateAgencyTypes = function (Action) {
    //Validate if atleast one Agency type is selected for the client
    var allClientAgencies;
    if (Action != "create") {
        allClientAgencies = $("span[id*='clientAgencyNmEdit']");
    }
    else {
        allClientAgencies = $("span[id*='clientAgencyNmNew']");
    }
    var flag = false;
    for (var k = 0; k < allClientAgencies.length; k++) {
        if (allClientAgencies[k].children[0].value.length > 0 && allClientAgencies[k].children[0].value.split('|')[1] != '00000000-0000-0000-0000-000000000000') {
            flag = true;
            break;
        }
    }
    if (flag) {
        return true;
    }
    else {
        return false;
    }
};

Viacom.createClientWeb.CreateItemInList = function () {
    try {
        Viacom.createClientWeb.CallToCreateItemInSiteProvisioning();
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error in making call to creating item in master list");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createClientWeb.CallToCreateItemInSiteProvisioning = function () {
    try {
        var insertItems = [[]];
        insertItems[0] = ["MMD", "ClientName", Viacom.clientGroups.Constants.objTaxClient.taxClientName[1]];
        insertItems[1] = ["Text", "SiteURL", Viacom.clientGroups.Constants.newSiteUrl];
        insertItems[2] = ["MMD", "ClientCategory", Viacom.clientGroups.Constants.objTaxClient.taxClientCategoryValue[1]];
        insertItems[3] = ["MMD", "ClientSegment", Viacom.clientGroups.Constants.objTaxClient.taxClientSegmentValue[1]];

        //Add CPD Managers group by default
        Viacom.clientGroups.Constants.objTaxClient.assignedCpd.push("Client Planning Managers");
        insertItems[4] = ["MultiUser", "AssignedClientPlanner", Viacom.clientGroups.Constants.objTaxClient.assignedCpd];
        insertItems[5] = ["Choice", "SiteType", "ClientSite"];
        insertItems[6] = ["Choice", "ActionType", "CreateSite"];


        //Client Types dynamically
        var k = 7;
        var allTaxSpans = $("span[id*='agencyTypeNew']");
        for (var i = 0; i < allTaxSpans.length; i++) {
            var taxAgency = $("span[id*='clientAgencyNmNew" + allTaxSpans[i].id.split('agencyTypeNew')[1] + "']")[0].children[0].value;
            if (taxAgency != null) {
                insertItems[k] = ["MMD", allTaxSpans[i].getAttribute('taxFieldNameProvisioning'), taxAgency];
                k++;
            }
        }

        if ($("#rdmCLientId").val().trim().length > 0) {
            insertItems[k] = ["SLT", "RDMClientID", $("#rdmCLientId").val()];
            k++;
        }
        if ($("#gabrielClientId").val().trim().length > 0) {
            insertItems[k] = ["SLT", "GabrielClientID", $("#gabrielClientId").val()];
            k++;
        }
        if ($("#wideOrbitClientId").val().trim().length > 0) {
            insertItems[k] = ["SLT", "WideOrbitClientID", $("#wideOrbitClientId").val()];
            k++;
        }
        if ($("#adFrontClientId").val().trim().length > 0) {
            insertItems[k] = ["SLT", "AdFrontClientID", $("#adFrontClientId").val()];
            k++;
        }



        var clientContext = SP.ClientContext.get_current();
        var root = clientContext.get_site().get_rootWeb();
        var oList = root.get_lists().getByTitle("Site Provisioning");
        var listContentTypes = oList.get_contentTypes();
        clientContext.load(listContentTypes);

        var item = oList.addItem();
        for (var i = 0; i < insertItems.length; i++) {
            switch (insertItems[i][0]) {
                case "MultiUser": {
                    var users = [];
                    users.length = 0;
                    for (var k = 0; k < insertItems[i][2].length; k++) {
                        if (insertItems[i][2][k] != undefined && insertItems[i][2][k] != null)
                            users.push(SP.FieldUserValue.fromUser(insertItems[i][2][k]));
                    }
                    item.set_item(insertItems[i][1], users);
                    break;
                }
                case "User": {
                    if (insertItems[i][2] != undefined && insertItems[i][2] != null)
                        item.set_item(insertItems[i][1], SP.FieldUserValue.fromUser(insertItems[i][2]));
                    break;
                }
                case "HyperLink": {
                    if (insertItems[i][2] != undefined && insertItems[i][2] != null) {
                        var rootSiteUrl = new SP.FieldUrlValue();
                        rootSiteUrl.set_url(insertItems[i][2]);
                        rootSiteUrl.set_description(insertItems[i][2]);
                        item.set_item(insertItems[i][1], rootSiteUrl);
                    }
                    break;
                }
                case "Date": {
                    if (insertItems[i][2] != undefined && insertItems[i][2] != null) {
                        var iDt = insertItems[i][2].toISOString();
                        item.set_item(insertItems[i][1], iDt);
                    }
                    break;
                }
                default: {
                    if (insertItems[i][2] != undefined && insertItems[i][2] != null)
                        item.set_item(insertItems[i][1], insertItems[i][2]);
                }
            }
        }
        item.update();
        clientContext.executeQueryAsync(function () {
            var ct_enumerator = listContentTypes.getEnumerator();
            while (ct_enumerator.moveNext()) {
                var ct = ct_enumerator.get_current();
                if (ct.get_name().toString() == 'Client Site Provisioning') {
                    //we've got our content type, now let's get its name
                    item.set_item('ContentTypeId', ct.get_id().toString());
                    item.update();

                    clientContext.executeQueryAsync(function () {
                        SP.UI.ModalDialog.commonModalDialogClose();
                        alert("Site Creation initiated successfully");
                        location.reload();
                    }, function (sender, args) {
                        SP.UI.ModalDialog.commonModalDialogClose();
                        alert("Error in while updating list item");
                        console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                    });
                }
            }
            //SP.UI.ModalDialog.commonModalDialogClose();
            //alert("Site Creation initiated successfully");
        }, function (sender, args) {
            SP.UI.ModalDialog.commonModalDialogClose();
            alert("Error in either creating iitem in 'Sites' list or making call top open another popup");
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        });



    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error while making call to create item in provisioning list");
        console.log('Request failed. ' + e.message);
    }
}


Viacom.createClientWeb.getItemFromSVPList = function (agencyName, agencyId, agencyUrl, svpTm, agencyTypeText) {
    try {
        var svpType = [];
        var svpReg = [];
        var svpUrl = null;
        var agencyType = [];
        var taxonomyContext = new SP.ClientContext.get_current();
        var session = SP.Taxonomy.TaxonomySession.getTaxonomySession(taxonomyContext);
        //var termStores = session.get_termStores();
        //var termStore = termStores.getByName(Viacom.clientGroups.Constants.mmsName);
        //var session = SP.Taxonomy.TaxonomySession.getTaxonomySession(taxonomyContext);
        var termStore = session.getDefaultSiteCollectionTermStore();
        var group = termStore.get_groups().getByName('M＆E Ad Sales');
        var termSet = group.get_termSets().getByName('Agency Types');
        var termObj = termSet.get_terms().getByName(agencyName[2]);
        taxonomyContext.load(termObj);
        taxonomyContext.executeQueryAsync(function () {
            if (termObj.get_name().toLowerCase() == agencyTypeText.toLowerCase()) {
                termExist = true;
                agencyType.length = 0;
                agencyType.push(termObj.get_name());
                agencyType.push(termObj.get_id()._m_guidString$p$0);

                var clientContext = SP.ClientContext.get_current();
                var root = clientContext.get_site().get_rootWeb();
                var oList = root.get_lists().getByTitle("SVP Data");
                var camlQuery = new SP.CamlQuery();
                var query = "<View><Query><Where><Contains><FieldRef Name='TeamName'/><Value Type='TaxonomyFieldType'>" + svpTm[0] + "</Value></Contains></Where></Query></View>";;
                camlQuery.set_viewXml(query);
                var collListItem = oList.getItems(camlQuery);
                clientContext.load(collListItem);
                clientContext.executeQueryAsync(
                      function (sender, args) {
                          if (collListItem.get_count() != 0) {
                              var listItemInfo = '';
                              var listItemEnumerator = collListItem.getEnumerator();
                              while (listItemEnumerator.moveNext()) {
                                  var oListItem = listItemEnumerator.get_current();
                                  var svpNm = oListItem.get_item('TeamName').$0_1;
                                  if (svpNm == svpTm[0]) {
                                      svpType[0] = oListItem.get_item('TeamType').$0_1;
                                      svpType[1] = oListItem.get_item('TeamType').$1_1;
                                      svpUrl = oListItem.get_item('SVPSiteUrl').$2_1;
                                      svpReg[0] = oListItem.get_item('TeamRegion').$0_1;
                                      svpReg[1] = oListItem.get_item('TeamRegion').$1_1;
                                      break;
                                  }
                              }
                              var sitesArrObj = [[]];
                              sitesArrObj[0] = ["MMD", "ClientName", Viacom.clientGroups.Constants.objTaxClient.taxClientName[1]];
                              sitesArrObj[1] = ["HyperLink", "ClientSiteUrl", Viacom.clientGroups.Constants.newSiteUrl];
                              sitesArrObj[2] = ["SLT", "RDMClientID", $('#rdmCLientId').val()];
                              sitesArrObj[3] = ["SLT", "GabrielClientID", $('#gabrielClientId').val()];
                              sitesArrObj[4] = ["SLT", "WideOrbitClientID", $('#wideOrbitClientId').val()];
                              sitesArrObj[5] = ["SLT", "AdFrontClientID", $('#adFrontClientId').val()];
                              sitesArrObj[6] = ["MMD", "ClientCategory", Viacom.clientGroups.Constants.objTaxClient.taxClientCategoryValue[1]];
                              sitesArrObj[7] = ["MMD", "ClientSegment", Viacom.clientGroups.Constants.objTaxClient.taxClientSegmentValue[1]];
                              sitesArrObj[8] = ["MMD", "AgencyName", agencyName[1]];
                              sitesArrObj[9] = ["MMD", "AgencyType", agencyType[1]];
                              sitesArrObj[10] = ["HyperLink", "AgencyURL", agencyUrl];
                              sitesArrObj[11] = ["MMD", "SVPTeamName", svpTm[1]];
                              sitesArrObj[12] = ["MMD", "SVPTeamRegion", svpReg[1]];
                              sitesArrObj[13] = ["MMD", "SVPTeamType", svpType[1]];
                              sitesArrObj[14] = ["HyperLink", "SVPSiteURL", svpUrl];
                              if (agencyId != null) {
                                  sitesArrObj[15] = ["MMD", "AgencyID", agencyId[1]];
                              }
                              if (Viacom.clientGroups.Constants.agencyTypeCount == Viacom.clientGroups.Constants.objTaxClient.allAgencyType.length - 1) {
                                  Viacom.createClientWeb.MakeEntryInList("Site Mapping", sitesArrObj, true);
                              }
                              else {
                                  Viacom.createClientWeb.MakeEntryInList("Site Mapping", sitesArrObj, false);
                              }
                          }
                          else {
                              SP.UI.ModalDialog.commonModalDialogClose();
                              alert("Error in master data. SVP team name not exist in SVP master data which should exist.");
                              console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                          }

                      }, function (sender, args) {
                          SP.UI.ModalDialog.commonModalDialogClose();
                          alert("Error in fetching items from SVP Data master list");
                          console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                      });
            }
        }, function (sender, args) {
            SP.UI.ModalDialog.commonModalDialogClose();
            alert("Error while fetching Agency Types terms from managed metadata service");
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Exception occured while fetching Agency Types terms from managed metadata service");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createClientWeb.getItemFromConfigLists = function (lstName, query, beforeCalltoAgency, agencyTypeText, agencyName) {
    try {
        var agencyId = [];
        var svpTm = [];
        agencyId.length = 0;
        var agencyUrl = null;
        var clientContextAD = SP.ClientContext.get_current();
        var root = clientContextAD.get_site().get_rootWeb();
        var oList = root.get_lists().getByTitle(lstName);
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml(query);
        var collListItemAd = oList.getItems(camlQuery);
        clientContextAD.load(collListItemAd);
        clientContextAD.executeQueryAsync(
              function (sender, args) {
                  if (collListItemAd.get_count() != 0) {
                      var listItemInfo = '';
                      var listItemEnumerator = collListItemAd.getEnumerator();
                      try {
                          var getClientAgency = false;
                          while (listItemEnumerator.moveNext()) {
                              var oListItem = listItemEnumerator.get_current();
                              if (lstName == "Agency Data") {
                                  var agNm = oListItem.get_item('AgencyName').$0_1;
                                  if (agNm.toLowerCase() == agencyName[0].toLowerCase()) {
                                      if (oListItem.get_item('AgencyID') != null) {
                                          agencyId[0] = oListItem.get_item('AgencyID').$0_1;
                                          agencyId[1] = oListItem.get_item('AgencyID').$1_1;
                                      }
                                      agencyUrl = oListItem.get_item('AgencyUrl');
                                      svpTm[0] = oListItem.get_item('TeamName').$0_1;
                                      svpTm[1] = oListItem.get_item('TeamName').$1_1;
                                      if (Viacom.clientGroups.Constants.agencyTypeCount == 0) {
                                          Viacom.clientGroups.Constants.objClientRelatedData.primaryAgSVPTm.push(svpTm[0]);
                                          Viacom.clientGroups.Constants.objClientRelatedData.primaryAgSVPTm.push(svpTm[1]);
                                      }
                                      getClientAgency = true;

                                      break;
                                  }
                              }

                          }
                          if (getClientAgency) {
                              Viacom.createClientWeb.getItemFromSVPList(agencyName, agencyId, agencyUrl, svpTm, agencyTypeText);
                          }
                          else {
                              SP.UI.ModalDialog.commonModalDialogClose();
                              alert("Agency master data is not correct. Given agency not exist in agency master data.");
                          }
                      }
                      catch (err) {
                          SP.UI.ModalDialog.commonModalDialogClose();
                          alert("Error in iterating fetched items from master list");
                          return false;
                      }
                  }
                  else {
                      SP.UI.ModalDialog.commonModalDialogClose();
                      alert("Error as selected agency data not exist in 'Agency Data' master list");
                  }
              }, function (sender, args) {
                  SP.UI.ModalDialog.commonModalDialogClose();
                  alert("Error in fetching items from master list");
                  console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace())
              });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Exception occured in fetching items from master list");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createClientWeb.CallToCreateItemInLists = function (lstName, beforeEntryInSiteList) {
    try {
        if (lstName == "Client Data" && beforeEntryInSiteList) {
            var arrObj = [[]];
            Viacom.clientGroups.Constants.newSiteUrl = _spPageContextInfo.siteAbsoluteUrl + "/sites/clients/";
            arrObj[0] = ["MMD", "ClientName", Viacom.clientGroups.Constants.objTaxClient.taxClientName[1]];
            arrObj[1] = ["HyperLink", "ClientSiteUrl", Viacom.clientGroups.Constants.newSiteUrl];
            arrObj[2] = ["MMD", "ClientCategory", Viacom.clientGroups.Constants.objTaxClient.taxClientCategoryValue[1]];
            arrObj[3] = ["MMD", "ClientSegment", Viacom.clientGroups.Constants.objTaxClient.taxClientSegmentValue[1]];
            arrObj[4] = ["MultiUser", "AssignedCPDs", Viacom.clientGroups.Constants.objTaxClient.assignedCpd];
            var k = 5;
            if ($("#rdmCLientId").val().trim().length > 0) {
                arrObj[k] = ["SLT", "RDMClientID", $("#rdmCLientId").val()];
                k++;
            }
            if ($("#gabrielClientId").val().trim().length > 0) {
                arrObj[k] = ["SLT", "GabrielClientID", $("#gabrielClientId").val()];
                k++;
            }
            if ($("#wideOrbitClientId").val().trim().length > 0) {
                arrObj[k] = ["SLT", "WideOrbitClientID", $("#wideOrbitClientId").val()];
                k++;
            }
            if ($("#adFrontClientId").val().trim().length > 0) {
                arrObj[k] = ["SLT", "AdFrontClientID", $("#adFrontClientId").val()];
                k++;
            }
            Viacom.createClientWeb.MakeEntryInList("Client Data", arrObj, false);
        }
        else if (lstName == "Sites" && (!beforeEntryInSiteList)) {
            var sitesArrObj = [[]];
            sitesArrObj[0] = ["Choice", "SiteLevel", "Client"];
            sitesArrObj[1] = ["HyperLink", "RootSiteCollectionURL", _spPageContextInfo.siteAbsoluteUrl + "/sites/clients"];
            sitesArrObj[2] = ["SLT", "SiteValue", Viacom.clientGroups.Constants.objTaxClient.taxClientName[0]];
            sitesArrObj[3] = ["SLT", "SiteURL", Viacom.clientGroups.Constants.newSiteUrl];
            sitesArrObj[4] = ["Date", "SiteCreationDate", new Date()];
            Viacom.createClientWeb.MakeEntryInList("Sites", sitesArrObj, false);
        }
        else if (lstName == "Site Mapping" && (!beforeEntryInSiteList)) {
            var agencyQuery = "<View><Query><Where><Contains><FieldRef Name='AgencyName'/><Value Type='TaxonomyFieldType'>" + Viacom.clientGroups.Constants.objTaxClient.allAgencyType[0][0] + "</Value></Contains></Where></Query></View>";
            var agencyTypeText = Viacom.clientGroups.Constants.objTaxClient.allAgencyType[0][2];
            Viacom.clientGroups.Constants.agencyTypeCount = 0;
            Viacom.createClientWeb.getItemFromConfigLists("Agency Data", agencyQuery, false, agencyTypeText, Viacom.clientGroups.Constants.objTaxClient.allAgencyType[0]);
        }
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error while preparing data to populate in master list");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createClientWeb.MakeEntryInList = function (lstName, insertItems, final) {
    try {
        var clientContext = SP.ClientContext.get_current();
        var root = clientContext.get_site().get_rootWeb();
        var oList = root.get_lists().getByTitle(lstName);
        var item = oList.addItem();
        for (var i = 0; i < insertItems.length; i++) {
            switch (insertItems[i][0]) {
                case "MultiUser": {
                    var users = [];
                    users.length = 0;
                    for (var k = 0; k < insertItems[i][2].length; k++) {
                        if (insertItems[i][2][k] != undefined && insertItems[i][2][k] != null)
                            users.push(SP.FieldUserValue.fromUser(insertItems[i][2][k]));
                    }
                    item.set_item(insertItems[i][1], users);
                    break;
                }
                case "User": {
                    if (insertItems[i][2] != undefined && insertItems[i][2] != null)
                        item.set_item(insertItems[i][1], SP.FieldUserValue.fromUser(insertItems[i][2]));
                    break;
                }
                case "HyperLink": {
                    if (insertItems[i][2] != undefined && insertItems[i][2] != null) {
                        var rootSiteUrl = new SP.FieldUrlValue();
                        rootSiteUrl.set_url(insertItems[i][2]);
                        rootSiteUrl.set_description(insertItems[i][2]);
                        item.set_item(insertItems[i][1], rootSiteUrl);
                    }
                    break;
                }
                case "Date": {
                    if (insertItems[i][2] != undefined && insertItems[i][2] != null) {
                        var iDt = insertItems[i][2].toISOString();
                        item.set_item(insertItems[i][1], iDt);
                    }
                    break;
                }
                default: {
                    if (insertItems[i][2] != undefined && insertItems[i][2] != null)
                        item.set_item(insertItems[i][1], insertItems[i][2]);
                }
            }
        }
        item.update();
        clientContext.executeQueryAsync(function () {
            if (lstName == "Client Data") {
                Viacom.createClientWeb.CallToCreateItemInLists("Sites", false);
            }
            if (lstName == "Sites") {
                Viacom.createClientWeb.CallToCreateItemInLists("Site Mapping", false);
            }
            if (lstName == "Site Mapping" && Viacom.clientGroups.Constants.agencyTypeCount <= Viacom.clientGroups.Constants.objTaxClient.allAgencyType.length - 1 && (!final)) {
                var agencyQuery = null;
                Viacom.clientGroups.Constants.agencyTypeCount++;
                var m = Viacom.clientGroups.Constants.agencyTypeCount;
                agencyQuery = "<View><Query><Where><Contains><FieldRef Name='AgencyName'/><Value Type='TaxonomyFieldType'>" + Viacom.clientGroups.Constants.objTaxClient.allAgencyType[m][0] + "</Value></Contains></Where></Query></View>";
                var agencyTypeText = Viacom.clientGroups.Constants.objTaxClient.allAgencyType[m][2];
                Viacom.createClientWeb.getItemFromConfigLists("Agency Data", agencyQuery, false, agencyTypeText, Viacom.clientGroups.Constants.objTaxClient.allAgencyType[m]);
            }
            if (final) {
                Viacom.createClientWeb.openDialog(Viacom.clientGroups.Constants.objTaxClient.taxClientName[0], Viacom.clientGroups.Constants.newSiteUrl, Viacom.clientGroups.Constants.objClientRelatedData.primaryAgSVPTm[0]);
            }
        }, function (sender, args) {
            SP.UI.ModalDialog.commonModalDialogClose();
            alert("Error in creating new item in master list");
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Exception occured while creating item in master list");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createClientWeb.getPickerInputElement = function (identifier) {
    var tags = document.getElementsByTagName('DIV');
    Viacom.clientGroups.Constants.objTaxClient.assignedCpd = [];
    for (var i = 0; i < tags.length; i++) {
        var tempString = tags[i].id;
        if (tempString.indexOf(identifier) > 0) {
            var innerSpans = tags[i].getElementsByTagName("SPAN");
            for (var j = 0; j < innerSpans.length; j++) {
                if (innerSpans[j].id == 'content') {
                    Viacom.clientGroups.Constants.objTaxClient.assignedCpd.push(innerSpans[j].innerHTML);
                }
            }
        }
    }
};

Viacom.createClientWeb.getPeopleEditorValue = function () {
    try {
        Viacom.createClientWeb.getPickerInputElement('assignedCpds');
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error in reading value for people picker control");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createClientWeb.getGenericPeopleEditor = function (spanId, peopleObj) {
    try {
        var svpNmVal = $(spanId)[0].children[0].getAttribute('Value');
        if (svpNmVal.length != 0) {
            svpNmVal = svpNmVal.replace(/"/g, "'");
            peopleObj.push(svpNmVal.split("displaytext='")[1].split("' ")[0]);
            peopleObj.push(svpNmVal.split("description='")[1].split("'")[0]);
            peopleObj.push(svpNmVal.split("key='")[1].split("'")[0]);
        }
        else {
            peopleObj.length = 0;
        }
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error in getting value from people picker rendered html");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createClientWeb.getTaxWebTaggingValue = function () {
    try {
        var taxClientCategory = $("span[id*='ClientCategory']")[0].children[0];
        if (taxClientCategory.hasAttribute('value')) {
            Viacom.clientGroups.Constants.objTaxClient.taxClientCategoryValue = taxClientCategory.getAttribute('value').split('|');
        }
        var taxClientSegment = $("span[id*='ClientSegment']")[0].children[0];
        if (taxClientSegment.hasAttribute('value')) {
            Viacom.clientGroups.Constants.objTaxClient.taxClientSegmentValue = taxClientSegment.getAttribute('value').split('|');
        }
        var allAgencytype = $("span[id*='agencyTypeNew']");
        var k = 0;
        for (var i = 0; i < allAgencytype.length; i++) {
            var id = $("span[id*='agencyTypeNew']")[i].getAttribute('id');
            var agencyTypeValue = $("span[id*='agencyTypeNew']")[0].getAttribute('value');
            var no = id.replace("agencyType", "");
            var taxid = "clientAgencyNm" + no;
            var spanHtml = "span[id*='" + taxid + "']";
            var spanText = null;
            for (var m = 0; m < Viacom.clientGroups.Constants.termAgTypeArr.length; m++) {
                var typeId = '#' + id;
                var typeValue = $(typeId).attr('value').toLowerCase();
                if (taxid == "clientAgencyNm1") {
                    if (Viacom.clientGroups.Constants.termAgTypeArr[m][0].indexOf('Linear') >= 0) {
                        spanText = Viacom.clientGroups.Constants.termAgTypeArr[m][0];
                        Viacom.clientGroups.Constants.termAgTypeArr.splice(m, 1);
                        break;
                    }
                }
                else if (Viacom.clientGroups.Constants.termAgTypeArr[m][0].toLowerCase().indexOf(typeValue.toLowerCase()) >= 0 && taxid != "clientAgencyNm1") {
                    spanText = Viacom.clientGroups.Constants.termAgTypeArr[m][0];
                    Viacom.clientGroups.Constants.termAgTypeArr.splice(m, 1);
                    break;
                }
            }
            var spanId = '#' + id;
            $(spanId).attr('value', spanText);
            var taxClientAgencyType = $(spanHtml)[0].children[0];
            if (taxClientAgencyType.hasAttribute('value')) {
                if (taxClientAgencyType.getAttribute('value').length != 0) {
                    Viacom.clientGroups.Constants.objTaxClient.allAgencyType[k] = taxClientAgencyType.getAttribute('value').split('|');
                    Viacom.clientGroups.Constants.objTaxClient.allAgencyType[k].push(spanText);
                    k++;
                }
            }
        }
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error in reading value for web tagging control");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createClientWeb.validationForExistingtermNUrl = function (lstName, query, clientTerm) {
    try {
        var ctxUrl = window.location.protocol + "//" + window.location.host + _spPageContextInfo.siteServerRelativeUrl;
        var clientContext = new SP.ClientContext(ctxUrl);
        var root = clientContext.get_site().get_rootWeb();
        var oList = root.get_lists().getByTitle(lstName);
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml(query);
        var collListItem = oList.getItems(camlQuery);
        clientContext.load(collListItem);
        var tempClientArr = [];
        var tempClientUrlArr = [];
        clientContext.executeQueryAsync(
              function (sender, args) {
                  if (collListItem.get_count() != 0) {
                      var listItemInfo = '';
                      var listItemEnumerator = collListItem.getEnumerator();
                      try {
                          while (listItemEnumerator.moveNext()) {
                              var oListItem = listItemEnumerator.get_current();
                              var clientNm = oListItem.get_item('ClientName').$0_1;
                              var clientUrl = oListItem.get_item('SiteURL');
                              var SiteType = oListItem.get_item('SiteType');
                              var ActionType = oListItem.get_item('ActionType');
                              tempClientArr.push(clientNm);
                              var splitUrl = clientUrl.split("/sites/clients/");
                              tempClientUrlArr.push(splitUrl[1]);
                          }
                          var clientExist = false;
                          if (SiteType == "ClientSite" && ActionType == "CreateSite") {
                              for (var m = 0; m < tempClientArr.length; m++) {
                                  if (tempClientArr[m].toLowerCase() == $("#clientNm").val().toLowerCase()) {
                                      clientExist = true;
                                      SP.UI.ModalDialog.commonModalDialogClose();
                                      alert("Site with client name: '" + tempClientArr[m] + "' already exists. Please choose another name.");
                                      break;
                                  }
                                  if (tempClientUrlArr[m].toLowerCase() == Viacom.clientGroups.Constants.newSiteUrl.toLowerCase().split("/sites/clients/")[1]) {
                                      clientExist = true;
                                      SP.UI.ModalDialog.commonModalDialogClose();
                                      alert("Site with url: " + Viacom.clientGroups.Constants.newSiteUrl + " already exists. Please choose another url.");
                                      break;
                                  }
                              }
                          }
                          if (!clientExist) {
                              Viacom.createClientWeb.CreateItemInList();
                          }
                      }
                      catch (err) {
                          SP.UI.ModalDialog.commonModalDialogClose();
                          alert("Error in iterating list items which are fetched");
                          return false;
                      }
                  }
                  else {
                      if (clientTerm) {
                          var urlQuery = "<View><Query><Where><Contains><FieldRef Name='SiteURL'/><Value Type='Text'>/sites/clients/" + Viacom.clientGroups.Constants.newSiteUrl.split("/sites/clients/")[1];
                          urlQuery = urlQuery + "</Value></Contains></Where></Query></View>";
                          Viacom.createClientWeb.validationForExistingtermNUrl("Site Provisioning", urlQuery, false);
                      }
                      if (!clientTerm) {
                          Viacom.createClientWeb.CreateItemInList();
                      }
                          
                  }
              }, function (sender, args) {
                  SP.UI.ModalDialog.commonModalDialogClose();
                  alert("Error while fetching data from master list");
                  console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace())
              });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error when validating Client Name and Client Url already exist in master list or not");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createClientWeb.chkClientTerm = function (termName, termSetName, groupName, mmsName) {
    SP.UI.ModalDialog.showWaitScreenWithNoClose('Please wait...', 'Populating data in master list...');
	groupName = 'M＆E Ad Sales';
    Viacom.clientGroups.Constants.objTaxClient.allAgencyType.length = 0;
    Viacom.clientGroups.Constants.objTaxClient.taxClientCategoryValue.length = 0;
    Viacom.clientGroups.Constants.objTaxClient.taxClientSegmentValue.length = 0;
    Viacom.clientGroups.Constants.objTaxClient.taxClientName.length = 0;
    Viacom.clientGroups.Constants.objTaxClient.assignedCpd.length = 0;
    Viacom.clientGroups.Constants.objClientRelatedData.primaryAgencyNm.length = 0;
    Viacom.clientGroups.Constants.newSiteUrl = window.location.protocol + "//" + window.location.host + "/sites/clients/" + Viacom.createClientWeb.GetSiteUrl(termName);
    Viacom.clientGroups.Constants.newSiteUrl = Viacom.clientGroups.Constants.newSiteUrl.toLowerCase();
    var termExist = false;
    try {
        var agencyTypeContext = new SP.ClientContext.get_current();
        var agencyTypesession = SP.Taxonomy.TaxonomySession.getTaxonomySession(agencyTypeContext);
        var agencyTypetermStore = agencyTypesession.getDefaultSiteCollectionTermStore();
        var agencyTypegroup = agencyTypetermStore.get_groups().getByName('M＆E Ad Sales');
        var agencyTypetermSet = agencyTypegroup.get_termSets().getByName('Agency Types');
        var agencyTypeterms = agencyTypetermSet.getAllTerms();
        agencyTypeContext.load(agencyTypeterms);
        agencyTypeContext.executeQueryAsync(function () {
            var agTyEnumerator = agencyTypeterms.getEnumerator();
            Viacom.clientGroups.Constants.termAgTypeArr = [[]];
            Viacom.clientGroups.Constants.termAgTypeArr.length = 0;
            while (agTyEnumerator.moveNext()) {
                var currentTerm = agTyEnumerator.get_current();
                var agencyTypeTerm = currentTerm.get_name();
                var termGuid = currentTerm.get_id()._m_guidString$p$0;
                var arr = [];
                arr.length = 0;
                arr[0] = agencyTypeTerm;
                arr[1] = termGuid;
                Viacom.clientGroups.Constants.termAgTypeArr.push(arr);
            }
            Viacom.clientGroups.Constants.agencytypeLength = Viacom.clientGroups.Constants.termAgTypeArr.length;
            Viacom.createClientWeb.getTaxWebTaggingValue();
            Viacom.createClientWeb.getPeopleEditorValue();
            var IsValidate = Viacom.createClientWeb.ValidateFields('create');
            if (IsValidate) {
                var taxonomyContext = new SP.ClientContext.get_current();
                var session = SP.Taxonomy.TaxonomySession.getTaxonomySession(taxonomyContext);
                var termStore = session.getDefaultSiteCollectionTermStore();
                var group = termStore.get_groups().getByName(groupName);
                var termSet = group.get_termSets().getByName(termSetName);
                var termObj = termSet.get_terms().getByName(termName);
                taxonomyContext.load(termObj);
                taxonomyContext.executeQueryAsync(function () {
                    if (termObj.get_name().toLowerCase() == termName.toLowerCase()) {
                        termExist = true;
                        Viacom.clientGroups.Constants.objTaxClient.taxClientName.length = 0;
                        Viacom.clientGroups.Constants.objTaxClient.taxClientName.push(termObj.get_name());
                        Viacom.clientGroups.Constants.objTaxClient.taxClientName.push(termObj.get_id()._m_guidString$p$0);
                        var query = "<View><Query><Where><Contains><FieldRef Name='ClientName'/><Value Type='TaxonomyFieldType'>";
                        query = query + $("#clientNm").val() + "</Value></Contains></Where>";
                        query = query + "</Query></View>";
                        Viacom.createClientWeb.validationForExistingtermNUrl("Site Provisioning", query, true);

                        //Viacom.createClientWeb.CreateItemInList();
                        //alert("Site with client name: " + $("#clientNm").val() + " already exists. Please choose another name.");
                    }
                }, function (sender, args) {
                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                    if (!termExist && args.get_errorCode() == -2146233086) {
                        var errorClientCtx = new SP.ClientContext.get_current();
                        var errorClientSession = SP.Taxonomy.TaxonomySession.getTaxonomySession(errorClientCtx);
                        //var errorTermStores = errorSession.get_termStores();
                        //var errorTermStore = errorTermStores.getByName(mmsName);
                        var errorClientTermStore = errorClientSession.getDefaultSiteCollectionTermStore();
                        var errorClientgroup = errorClientTermStore.get_groups().getByName(groupName);
                        var errorClientTermSet = errorClientgroup.get_termSets().getByName(termSetName);
                        var errorClientnewTerm = errorClientTermSet.createTerm(termName, 1033, SP.Guid.newGuid()._m_guidString$p$0);
                        errorClientCtx.load(errorClientnewTerm);
                        errorClientCtx.executeQueryAsync(function () {
                            //alert("Term Created: " + errornewTerm.get_name());
                            Viacom.clientGroups.Constants.objTaxClient.taxClientName.length = 0;
                            Viacom.clientGroups.Constants.objTaxClient.taxClientName.push(errorClientnewTerm.get_name());
                            Viacom.clientGroups.Constants.objTaxClient.taxClientName.push(errorClientnewTerm.get_id()._m_guidString$p$0);
                            //Viacom.createClientWeb.CreateItemInList();
                            var query = "<View><Query><Where><Contains><FieldRef Name='ClientName'/><Value Type='TaxonomyFieldType'>";
                            query = query + $("#clientNm").val() + "</Value></Contains></Where>";
                            query = query + "</Query></View>";
                            Viacom.createClientWeb.validationForExistingtermNUrl("Site Provisioning", query, true);
                        }, function (sender, args) {
                            SP.UI.ModalDialog.commonModalDialogClose();
                            alert("Error in creating new client term in managed metadata service");
                            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                        });
                    }
                });
            }
            else {
                SP.UI.ModalDialog.commonModalDialogClose();
                alert("Some values are missing. Please fill those with any of one agency value. User must enter one of agency value.");
            }
        }, function (sender, args) {
            SP.UI.ModalDialog.commonModalDialogClose();
            alert("Error in fetching agency type terms from managed metadata service");
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        });
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error while creating or getting metadata terms");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createClientWeb.openDialog = function (parameterObj, newSiteUrl, svpName) {
    try {
        var diaOptions = SP.UI.$create_DialogOptions();
        diaOptions.url = _spPageContextInfo.siteAbsoluteUrl + '/Pages/WebCreationPopup.aspx?termobj=' + parameterObj + '&newSiteUrl=' + newSiteUrl + '&svpName=' + svpName + '&siteType=Client';
        diaOptions.width = 300;
        diaOptions.height = 215;
        diaOptions.showClose = false;
        diaOptions.allowMaximize = false;
        diaOptions.title = "Please wait..";
        diaOptions.dialogReturnValueCallback = Function.createDelegate(null, Viacom.createAgencyWeb.CloseCallBack);
        SP.UI.ModalDialog.commonModalDialogClose();
        SP.UI.ModalDialog.showModalDialog(diaOptions);
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Exception occured while calling popup for client web creation");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createClientWeb.CloseCallBack = function (result, returnValue) {
    if (result == SP.UI.DialogResult.OK) {
        alert('Site created successfully!');
        location.reload();
    }
    else {
        alert('Site creation failes!');
    }
};


/////////////////// Edit Client section /////
Viacom.createClientWeb.populateData = function () {
    try {

        if (Viacom.createClientWeb.ValidateFields('populate')) {
            SP.UI.ModalDialog.showWaitScreenWithNoClose('Please wait...', 'Fetching information...');
            var taxClientName = $("span[id*='TaxonomyClientNameEdit']")[0].children[0];
            var ctxUrl = window.location.protocol + "//" + window.location.host + _spPageContextInfo.siteServerRelativeUrl;
            var clientContext = new SP.ClientContext(ctxUrl);
            var root = clientContext.get_site().get_rootWeb();
            var oList = root.get_lists().getByTitle("Site Mapping");
            var camlQuery = new SP.CamlQuery();

            var query = "<View><Query><Where><Eq><FieldRef Name='ClientName'/><Value Type='TaxonomyFieldType'>";
            query = query + taxClientName.getAttribute('value').split('|')[0] + "</Value></Eq></Where>";
            query = query + "</Query></View>";
            camlQuery.set_viewXml(query);

            var collListItem = oList.getItems(camlQuery);
            clientContext.load(collListItem);

            var agencyTypes = new Array();
            var assignedCPDs = new Array();
            clientContext.executeQueryAsync(
				  function (sender, args) {
				      if (collListItem.get_count() != 0) {
				          var listItemInfo = '';
				          var listItemEnumerator = collListItem.getEnumerator();
				          try {
				              while (listItemEnumerator.moveNext()) {
				                  var oListItem = listItemEnumerator.get_current();


				                  if (Viacom.clientGroups.Constants.clientDetails.length == 0) {

				                      Viacom.clientGroups.Constants.clientDetails = {
				                          TaxClientName: oListItem.get_item('ClientName').$0_1,
				                          TaxClientNameId: oListItem.get_item('ClientName').$1_1,
				                          SiteUrl: oListItem.get_item('ClientSiteUrl').$2_1,
				                          TaxClientCategory: oListItem.get_item('ClientCategory').$0_1,
				                          TaxClientCategoryId: oListItem.get_item('ClientCategory').$1_1,
				                          TaxClientSegment: oListItem.get_item('ClientSegment').$0_1,
				                          TaxClientSegmentId: oListItem.get_item('ClientSegment').$1_1,
				                          RDMClientID: oListItem.get_item('RDMClientID'),
				                          GabrielClientID: oListItem.get_item('GabrielClientID'),
				                          WideOrbitClientID: oListItem.get_item('WideOrbitClientID'),
				                          AdFrontClientID: oListItem.get_item('AdFrontClientID')
				                      }
				                  }
								  var clntAgencyNameLbl='';
								  var clntAgencyNameGuid='';
								  if(oListItem.get_item('AgencyName'))
								  {
									clntAgencyNameLbl=oListItem.get_item('AgencyName').$0_1;
									clntAgencyNameGuid=oListItem.get_item('AgencyName').$1_1;
								  }
				                  agencyTypes[agencyTypes.length] = {
				                      AgencyType: oListItem.get_item('AgencyType').$0_1,
				                      AgencyTypeId: oListItem.get_item('AgencyType').$1_1,
				                      AgencyName: clntAgencyNameLbl,
				                      AgencyId: clntAgencyNameGuid,
				                  }

				              }
				              Viacom.clientGroups.Constants.clientDetails.ClientAgencies = agencyTypes;

				              //Get Assigned CPDs from Cleint Data list

				              var oList = root.get_lists().getByTitle("Client Data");
				              var collClientDataItems = oList.getItems(camlQuery);
				              clientContext.load(collClientDataItems);

				              clientContext.executeQueryAsync(
								  function (sender, args) {
								      var listItemInfo = '';
								      var listItemEnumerator = collClientDataItems.getEnumerator();

								      while (listItemEnumerator.moveNext()) {
								          var oListItem = listItemEnumerator.get_current();
								          assignedCPDs = {
								              AssignedCPDs: oListItem.get_item('AssignedCPDs')
								          }
								      }

								      Viacom.clientGroups.Constants.clientDetails.AssignedCPDs = assignedCPDs;

								      Viacom.clientGroups.BindData();
								  },
									  function (sender, args) {
									      SP.UI.ModalDialog.commonModalDialogClose();
									      alert("Error while fetching data from Client Data list");
									      console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace())
									  }
							  );

				          }
				          catch (err) {
				              SP.UI.ModalDialog.commonModalDialogClose();
				              alert("Error in iterating list items which are fetched");
				              return false;
				          }
				      }
				      else {
				          alert('Could not get the details of the given client');
				      }
				  }, function (sender, args) {
				      SP.UI.ModalDialog.commonModalDialogClose();
				      alert("Error while fetching data from master list");
				      console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace())
				  });
        }
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error when validating Client Name and Client Url already exist in master list or not");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.clientGroups.BindData = function () {
    if (Viacom.clientGroups.Constants.clientDetails.SiteUrl != null) {
        try {
            $("#preClientUrlEdit").val(Viacom.clientGroups.Constants.clientDetails.SiteUrl);

            Viacom.clientGroups.populateTaxonomyField(Viacom.clientGroups.Constants.clientDetails.TaxClientCategory, "ClientCategoryEdit");

            Viacom.clientGroups.populateTaxonomyField(Viacom.clientGroups.Constants.clientDetails.TaxClientSegment, "ClientSegmentEdit");

            //Populate Agecny Types dynamically
            Viacom.clientGroups.BindAgencyTypes();

            //Assigned CPDs
            Viacom.clientGroups.SetUserFieldValue("assignedCpdsEdit", Viacom.clientGroups.Constants.clientDetails.AssignedCPDs);



            //RDMClientID
            $("#rdmCLientIdEdit").val(Viacom.clientGroups.Constants.clientDetails.RDMClientID);
            $("#gabrielClientIdEdit").val(Viacom.clientGroups.Constants.clientDetails.GabrielClientID);
            $("#wideOrbitClientIdEdit").val(Viacom.clientGroups.Constants.clientDetails.WideOrbitClientID);
            $("#adFrontClientIdEdit").val(Viacom.clientGroups.Constants.clientDetails.AdFrontClientID);

            //Bind operation complete. close the wait modal popoup
            SP.UI.ModalDialog.commonModalDialogClose();
        }
        catch (err) {
            SP.UI.ModalDialog.commonModalDialogClose();
            console.log(err.message);
            alert("Error in binding data to the page");
            return false;
        }
    }
};


Viacom.clientGroups.populateTaxonomyField = function (taxLabel, controlId) {
    $("div[id*='" + controlId + "'][id*='editableRegion']").html(taxLabel);
    var myContainer = $("#" + $("div[id*='" + controlId + "'][id*='editableRegion']").attr('id').replace('editableRegion', ''));
    var divElement = myContainer.get(0);
    var controlObject = new Microsoft.SharePoint.Taxonomy.ControlObject(divElement);
    controlObject.validateAll();
};

Viacom.clientGroups.SetUserFieldValue = function (fieldName, userNames) {

    var _PeoplePicker = $("div[id*='" + fieldName + "_upLevelDiv']");
    var users = "";
    for (var i = 0; i < userNames.AssignedCPDs.length; i++) {
        users = users + userNames.AssignedCPDs[i].$2e_1 + "; ";
    }
    _PeoplePicker.html(users);
    $("a[id*='" + fieldName + "_checkNames']").trigger('click')

};

Viacom.clientGroups.BindAgencyTypes = function () {
    try {
        var allTaxSpans = $("span[id*='agencyTypeEdit']");
        for (var i = 0; i < allTaxSpans.length; i++) {
            var agencies = $.grep(Viacom.clientGroups.Constants.clientDetails.ClientAgencies, function (e) { return e.AgencyType == allTaxSpans[i].getAttribute('value'); });

            var agenciesToResolve = "";
            for (var j = 0; j < agencies.length; j++) {
				if(agencies[j].AgencyName != '' && agencies[j].AgencyName !== null)
				{
					agenciesToResolve = agenciesToResolve + agencies[j].AgencyName + "; ";
				}
            }

            if (agenciesToResolve.length > 0) {
                Viacom.clientGroups.populateTaxonomyField(agenciesToResolve, "clientAgencyNmEdit" + allTaxSpans[i].id.split('agencyTypeEdit')[1]);
            }



        }
    }
    catch (err) {
        SP.UI.ModalDialog.commonModalDialogClose();
        console.log(err.message);
        alert("Error in binding binding agencies to the page");
        return false;
    }
};

Viacom.clientGroups.UpdateSite = function () {
    try {

        if (Viacom.createClientWeb.ValidateFields('edit')) {
            SP.UI.ModalDialog.showWaitScreenWithNoClose('Please wait...', 'Initializing Site update request...');
            var insertItems = [[]];

            insertItems[0] = ["MMD", "ClientName", Viacom.clientGroups.Constants.clientDetails.TaxClientNameId];
            insertItems[1] = ["MMD", "ClientSegment", $("span[id*='ClientSegmentEdit']")[0].children[0].value.split('|')[1]];
            insertItems[2] = ["MMD", "ClientCategory", $("span[id*='ClientCategoryEdit']")[0].children[0].value.split('|')[1]];
            insertItems[3] = ["Text", "RDMClientID", $("#rdmCLientIdEdit").val()];
            insertItems[4] = ["Text", "GabrielClientID", $("#gabrielClientIdEdit").val()];
            insertItems[5] = ["Text", "WideOrbitClientID", $("#wideOrbitClientIdEdit").val()];
            insertItems[6] = ["Text", "AdFrontClientID", $("#adFrontClientIdEdit").val()];
            insertItems[7] = ["Text", "SiteURL", Viacom.clientGroups.Constants.clientDetails.SiteUrl];
            insertItems[8] = ["Choice", "SiteType", "ClientSite"];
            insertItems[9] = ["Choice", "ActionType", "EditSite"];

            //get the latest values from ppl picker
            Viacom.createClientWeb.getPickerInputElement('assignedCpdsEdit');
            //Add CPD Managers group by default
            Viacom.clientGroups.Constants.objTaxClient.assignedCpd.push("Client Planning Managers");
            insertItems[10] = ["MultiUser", "AssignedClientPlanner", Viacom.clientGroups.Constants.objTaxClient.assignedCpd];

            //Client Types dynamically
            var k = 11;
            var allTaxSpans = $("span[id*='agencyTypeEdit']");
            for (var i = 0; i < allTaxSpans.length; i++) {
                var taxAgency = $("span[id*='clientAgencyNmEdit" + allTaxSpans[i].id.split('agencyTypeEdit')[1] + "']")[0].children[0].value;
                if (taxAgency != null) {
                    //taxAgency = taxAgency.split(";")
                    //for(var j=0; j < taxAgency.length; j++ )
                    //{
                    insertItems[k] = ["MMD", allTaxSpans[i].getAttribute('taxFieldNameProvisioning'), taxAgency];
                    //insertItems[k] = ["MMD", allTaxSpans[i].getAttribute('taxFieldNameProvisioning'),  taxAgency[j].split('|')[1]];
                    k++;
                    //}

                }
            }


            var clientContext = SP.ClientContext.get_current();
            var root = clientContext.get_site().get_rootWeb();
            var oList = root.get_lists().getByTitle("Site Provisioning");
            var listContentTypes = oList.get_contentTypes();
            clientContext.load(listContentTypes);

            var item = oList.addItem();
            for (var i = 0; i < insertItems.length; i++) {
                switch (insertItems[i][0]) {
                    case "MultiUser": {
                        var users = [];
                        users.length = 0;
                        for (var k = 0; k < insertItems[i][2].length; k++) {
                            if (insertItems[i][2][k] != undefined && insertItems[i][2][k] != null)
                                users.push(SP.FieldUserValue.fromUser(insertItems[i][2][k]));
                        }
                        item.set_item(insertItems[i][1], users);
                        break;
                    }
                    case "User": {
                        if (insertItems[i][2] != undefined && insertItems[i][2] != null)
                            item.set_item(insertItems[i][1], SP.FieldUserValue.fromUser(insertItems[i][2]));
                        break;
                    }
                    case "HyperLink": {
                        if (insertItems[i][2] != undefined && insertItems[i][2] != null) {
                            var rootSiteUrl = new SP.FieldUrlValue();
                            rootSiteUrl.set_url(insertItems[i][2]);
                            rootSiteUrl.set_description(insertItems[i][2]);
                            item.set_item(insertItems[i][1], rootSiteUrl);
                        }
                        break;
                    }
                    case "Date": {
                        if (insertItems[i][2] != undefined && insertItems[i][2] != null) {
                            var iDt = insertItems[i][2].toISOString();
                            item.set_item(insertItems[i][1], iDt);
                        }
                        break;
                    }
                    default: {
                        if (insertItems[i][2] != undefined && insertItems[i][2] != null)
                            item.set_item(insertItems[i][1], insertItems[i][2]);
                    }
                }
            }
            item.update();
            clientContext.executeQueryAsync(function () {
                var ct_enumerator = listContentTypes.getEnumerator();
                while (ct_enumerator.moveNext()) {
                    var ct = ct_enumerator.get_current();
                    if (ct.get_name().toString() == 'Client Site Provisioning') {
                        //we've got our content type, now let's get its name
                        item.set_item('ContentTypeId', ct.get_id().toString());
                        item.update();

                        clientContext.executeQueryAsync(function () {
                            SP.UI.ModalDialog.commonModalDialogClose();
                            alert("Site update request initiated successfully");
                            location.reload();
                        }, function (sender, args) {
                            SP.UI.ModalDialog.commonModalDialogClose();
                            alert("Error in while updating list item");
                            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                        });
                    }
                }
                //SP.UI.ModalDialog.commonModalDialogClose();
                //alert("Site Creation initiated successfully");
            }, function (sender, args) {
                SP.UI.ModalDialog.commonModalDialogClose();
                alert("Error in either creating item in 'Sites' list or making call top open another popup");
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            });


        }
    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error while making call to create item in provisioning list");
        console.log('Request failed. ' + e.message);
    }
};

Viacom.createClientWeb.GetSiteUrl = function (nmId) {
    var siteurl = nmId.replace(/[^a-zA-Z0-9]/g, '');
    return siteurl;
}