if (!window.console) {
    console = { log: function () { } };
}

var Viacom = Viacom || {};
Viacom.DeleteWeb = {};
Viacom.DeleteWeb = Viacom.DeleteWeb || {};

Viacom.DeleteWeb.Constants = function () {
    return {
        taxSVPSite: null,
        taxAgencySite: null,
        taxClientSite: null,
        typeOfSite: null,
        listSVPData: null,
        listAgencyData: null,
        listClientData: null
    }
};

$(document).ready(function () {

    ExecuteOrDelayUntilScriptLoaded(Viacom.DeleteWeb.Initialize, "sp.js");
});


Viacom.DeleteWeb.Initialize = function () {
    Viacom.DeleteWeb.Constants.taxSVPSite = null;
    Viacom.DeleteWeb.Constants.taxAgencySite = null;
    Viacom.DeleteWeb.Constants.taxClientSite = null;
    Viacom.DeleteWeb.Constants.typeOfSite = null;
    Viacom.DeleteWeb.Constants.listSVPData = "SVP Data";
    Viacom.DeleteWeb.Constants.listAgencyData = "Agency Data";
    Viacom.DeleteWeb.Constants.listClientData = "Client Data";


};

Viacom.DeleteWeb.ValidatePage = function () {
    try {
        Viacom.DeleteWeb.Constants.taxSVPSite = $("span[id*='deleteSVPSite']")[0].children[0].value;
        Viacom.DeleteWeb.Constants.taxAgencySite = $("span[id*='deleteAgencySite']")[0].children[0].value;
        Viacom.DeleteWeb.Constants.taxClientSite = $("span[id*='deleteClientSite']")[0].children[0].value;

        //only one site should be selected at once
        var count = 0;
        var siteControl;
        if (Viacom.DeleteWeb.Constants.taxSVPSite.length > 0) {
            count++;
            siteControl = Viacom.DeleteWeb.Constants.taxSVPSite;
            Viacom.DeleteWeb.Constants.typeOfSite = "SVP";
        }
        if (Viacom.DeleteWeb.Constants.taxAgencySite.length > 0) {
            count++;
            siteControl = Viacom.DeleteWeb.Constants.taxAgencySite;
            Viacom.DeleteWeb.Constants.typeOfSite = "AGENCY";
        }
        if (Viacom.DeleteWeb.Constants.taxClientSite.length > 0) {
            count++;
            siteControl = Viacom.DeleteWeb.Constants.taxClientSite;
            Viacom.DeleteWeb.Constants.typeOfSite = "CLIENT";
        }

        if (count > 1) {
            alert("Please select only one site to delete at a time");
            return false;
        }

        if (siteControl != null) {
            if (siteControl.split("|")[1] == '00000000-0000-0000-0000-000000000000') {
                alert("Please select a valid site");
                return false;
            }

            return siteControl;
        }
        else {
            alert("Could not get the site details");
            return false;
        }

    }
    catch (e) {
        SP.UI.ModalDialog.commonModalDialogClose();
        alert("Error while validating fields");
        console.log('Validation failed: ' + e.message);
    }


};

Viacom.DeleteWeb.Delete = function () {
    var siteTaxId = Viacom.DeleteWeb.ValidatePage();
    if (siteTaxId != false) {
        try {
            SP.UI.ModalDialog.showWaitScreenWithNoClose('Please wait...', 'Processing the request...');

            var ctxUrl = window.location.protocol + "//" + window.location.host + _spPageContextInfo.siteServerRelativeUrl;
            var clientContext = new SP.ClientContext(ctxUrl);
            var root = clientContext.get_site().get_rootWeb();

            var oList;
            var query;
            var fieldURLName;
            switch (Viacom.DeleteWeb.Constants.typeOfSite) {
                case "SVP":
                    oList = root.get_lists().getByTitle(Viacom.DeleteWeb.Constants.listSVPData);
                    fieldURLName = "SVPSiteUrl";
                    query = "<View><Query><Where><Eq><FieldRef Name='TeamName'/><Value Type='TaxonomyFieldType'>";
                    query = query + siteTaxId.split('|')[0] + "</Value></Eq></Where>";
                    query = query + "</Query></View>";
                    break;
                case "AGENCY":
                    oList = root.get_lists().getByTitle(Viacom.DeleteWeb.Constants.listAgencyData);
                    fieldURLName = "AgencyUrl";
                    query = "<View><Query><Where><Eq><FieldRef Name='AgencyName'/><Value Type='TaxonomyFieldType'>";
                    query = query + siteTaxId.split('|')[0] + "</Value></Eq></Where>";
                    query = query + "</Query></View>";
                    break;
                case "CLIENT":
                    oList = root.get_lists().getByTitle(Viacom.DeleteWeb.Constants.listClientData);
                    fieldURLName = "ClientSiteUrl";
                    query = "<View><Query><Where><Eq><FieldRef Name='ClientName'/><Value Type='TaxonomyFieldType'>";
                    query = query + siteTaxId.split('|')[0] + "</Value></Eq></Where>";
                    query = query + "</Query></View>";
                    break;
                default:
                    alert("Error in processing the request"); return;
            }

            var camlQuery = new SP.CamlQuery();
            camlQuery.set_viewXml(query);

            var collListItem = oList.getItems(camlQuery);
            clientContext.load(collListItem);

            clientContext.executeQueryAsync(
				 function (sender, args) {
				     if (collListItem.get_count() != 0) {
				         var listItemInfo = '';
				         var listItemEnumerator = collListItem.getEnumerator();
				         try {
				             while (listItemEnumerator.moveNext()) {
				                 var oListItem = listItemEnumerator.get_current();

				                 var oList = root.get_lists().getByTitle("Site Provisioning");
				                 var listContentTypes = oList.get_contentTypes();
				                 clientContext.load(listContentTypes);

				                 var item = oList.addItem();
				                 if (oListItem.get_item(fieldURLName).$2_1 != null) {
				                     item.set_item("SiteURL", oListItem.get_item(fieldURLName).$2_1);
				                 }
				                 else { //In case of Agencies, Url field is SLT
				                     item.set_item("SiteURL", oListItem.get_item(fieldURLName));
				                 }
				                 item.set_item("ActionType", "DeleteSite");
				                 //set SVP/AGENCY/CLIENT Name
				                 //item.set_item("TeamName", oListItem.get_item(fieldURLName).$2_1);
				                 switch (Viacom.DeleteWeb.Constants.typeOfSite) {
				                     case "SVP":
				                         item.set_item("SiteType", "SVPSite");
				                         item.set_item("TeamName", siteTaxId);
				                         break;
				                     case "AGENCY":
				                         item.set_item("SiteType", "AgencySite");
				                         item.set_item("AgencyName", siteTaxId);
				                         break;
				                     case "CLIENT":
				                         item.set_item("SiteType", "ClientSite");
				                         item.set_item("ClientName", siteTaxId);
				                         break;
				                     default:
				                         alert("Error in processing the request"); return;
				                 }


				                 item.update();
				                 clientContext.executeQueryAsync(function () {
				                     var ct_enumerator = listContentTypes.getEnumerator();
				                     while (ct_enumerator.moveNext()) {
				                         var ct = ct_enumerator.get_current();
				                         var flag = false;
				                         if (ct.get_name().toString() == 'Delete Site Provisioning') {
				                             flag = true;
				                             item.set_item('ContentTypeId', ct.get_id().toString());
				                             item.update();

				                             clientContext.executeQueryAsync(function () {
				                                 SP.UI.ModalDialog.commonModalDialogClose();
				                                 alert("Site delete request initiated successfully");
				                                 location.reload();
				                             }, function (sender, args) {
				                                 SP.UI.ModalDialog.commonModalDialogClose();
				                                 alert("Error while adding entry to provisioning list");
				                                 console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
				                             });
				                             break;
				                         }
				                     }
				                     if (!flag) {
				                         SP.UI.ModalDialog.commonModalDialogClose();
				                         alert("Could not find the Delete Site Content Type");
				                     }

				                 }, function (sender, args) {
				                     SP.UI.ModalDialog.commonModalDialogClose();
				                     alert("Error in adding data to provisioning list");
				                     console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
				                 });
				             }
				         }
				         catch (err) {
				             SP.UI.ModalDialog.commonModalDialogClose();
				             alert("Error in iterating list items");
				             return false;
				         }
				     }
				     else {
				         SP.UI.ModalDialog.commonModalDialogClose();
				         alert('Could not get the details of the selected site');
				     }
				 },
				  function (sender, args) {
				      SP.UI.ModalDialog.commonModalDialogClose();
				      alert("Error while fetching data from master list");
				      console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace())
				  }
			);
        }
        catch (e) {
            SP.UI.ModalDialog.commonModalDialogClose();
            alert("Error while queuing site delete request");
            console.log('Request failed. ' + e.message);
        }
    }
};