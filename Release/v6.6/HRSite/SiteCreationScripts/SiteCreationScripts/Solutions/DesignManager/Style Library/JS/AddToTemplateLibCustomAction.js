var Viacom = Viacom || {};
Viacom.AddToTemplateLibCustomAction = {};
Viacom.AddToTemplateLibCustomAction.AssociatedLibraryChoiceValue = [];
Viacom.AddToTemplateLibCustomAction.SelectedItem = null;
Viacom.AddToTemplateLibCustomAction.EnableButton = null;
Viacom.AddToTemplateLibCustomAction.Context = null;
Viacom.AddToTemplateLibCustomAction.CurrentLib = null;
Viacom.AddToTemplateLibCustomAction.CurrentWeb = null;
Viacom.AddToTemplateLibCustomAction.SelectedFile = null;
Viacom.AddToTemplateLibCustomAction.waitDialog = null;

Viacom.AddToTemplateLibCustomAction.Constants = function () {
    return {
        DialogBoxTitle: null,
        ListId: null,
        ItemId: null
    }
};

Viacom.AddToTemplateLibCustomAction.openAddDocPopup = function (listId, SeletedItemId) {
    Viacom.AddToTemplateLibCustomAction.Constants();
    Viacom.AddToTemplateLibCustomAction.Constants.ListId = listId.replace(/[{}]/g, "");
    Viacom.AddToTemplateLibCustomAction.Constants.ItemId = SeletedItemId;
    Viacom.AddToTemplateLibCustomAction.Constants.DialogBoxTitle = "Add to Template Library";
    Viacom.AddToTemplateLibCustomAction.getAssociatedLibColChoiceVal();
};

Viacom.AddToTemplateLibCustomAction.getAssociatedLibColChoiceVal = function (listId, SeletedItemId) {
    try {
        $.ajax({
            url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('Templates')/Fields?$select=Choices&$filter=(InternalName eq 'AssociatedLibraries')&$orderby=Choices asc",
            method: "GET",
            async: false,
            headers: { "Accept": "application/json; odata=verbose" },
            success: function (data) {
                Viacom.AddToTemplateLibCustomAction.AssociatedLibraryChoiceValue = data.d.results[0].Choices.results;
                Viacom.AddToTemplateLibCustomAction.openDialog();
            },
            error: function (jqXHR, textStatus, error) {
                console.log(error)
            }
        });
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
    }
};

Viacom.AddToTemplateLibCustomAction.openDialog = function () {
    try {
        var strOptions = '';
        for (var i = 0; i < Viacom.AddToTemplateLibCustomAction.AssociatedLibraryChoiceValue.length; i++) {
            strOptions = strOptions + "<option value='" + i + "' >" + Viacom.AddToTemplateLibCustomAction.AssociatedLibraryChoiceValue[i] + "</option>";
        }

        var addToNewDocumentHtml = "<div id='divAddToTemplateLibCustomAction' class='addToTemplateLibCustomAction'>" +
								"<div class='error'><span></span></div>" +
								"<div class='addDocHeader'><span>Template Name</span>" +
								"<input type='text' name='TemplateName' value=''></div>" +
								"<div class='assciatedLists multipleChkbx'>" +
								"<span class='list-head'>Associated Libraries</span>" +
								"<select id='source' name='source' multiple='multiple'>" + strOptions + "</select></div>" +
								"<div class='div-buttons'>" +
								"<input type='button' value='Add' name='Add' onclick='Viacom.AddToTemplateLibCustomAction.addDocToTemplateLib()' />" +
								"<input type='button' value='Cancel' name='Cancel' onclick='Viacom.AddToTemplateLibCustomAction.cancel()'/></div></div>" +
								"<script>$(document).ready(function (){" +
								"$('#source').shuttle();$('#shuttle-control-src-dst').text('Add >');$('#shuttle-control-dst-src').text('< Remove');" +
								"$('#source').shuttle.transfer($('#source'),$('#shuttle-dst'));" +
								"});</script>"

        $('body').append(addToNewDocumentHtml);
        var diaOptions = SP.UI.$create_DialogOptions();
        diaOptions.html = document.getElementById('divAddToTemplateLibCustomAction');
        diaOptions.width = 600;
        diaOptions.height = 350;
        diaOptions.showClose = true;
        diaOptions.allowMaximize = false;
        diaOptions.title = Viacom.AddToTemplateLibCustomAction.Constants.DialogBoxTitle;
        diaOptions.dialogReturnValueCallback = Function.createDelegate(null, Viacom.AddToTemplateLibCustomAction.closeCallBack);
        SP.UI.ModalDialog.commonModalDialogClose();
        SP.UI.ModalDialog.showModalDialog(diaOptions);
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
    }
};

Viacom.AddToTemplateLibCustomAction.EnableDisable = function () {
    Viacom.AddToTemplateLibCustomAction.Context = SP.ClientContext.get_current();
    var selectedItems = SP.ListOperation.Selection.getSelectedItems(Viacom.AddToTemplateLibCustomAction.Context);
    var ci = CountDictionary(selectedItems);
    if (ci == 1) {
        if (Viacom.AddToTemplateLibCustomAction.EnableButton == null) {
            Viacom.AddToTemplateLibCustomAction.CurrentWeb = Viacom.AddToTemplateLibCustomAction.Context.get_web();
            Viacom.AddToTemplateLibCustomAction.Context.load(Viacom.AddToTemplateLibCustomAction.CurrentWeb);

            Viacom.AddToTemplateLibCustomAction.CurrentLib = Viacom.AddToTemplateLibCustomAction.CurrentWeb.get_lists().getById(SP.ListOperation.Selection.getSelectedList(Viacom.AddToTemplateLibCustomAction.Context))
            Viacom.AddToTemplateLibCustomAction.Context.load(Viacom.AddToTemplateLibCustomAction.CurrentLib);

            var currentItem = Viacom.AddToTemplateLibCustomAction.CurrentLib.getItemById(selectedItems[0].id);
            Viacom.AddToTemplateLibCustomAction.Context.load(currentItem);

            Viacom.AddToTemplateLibCustomAction.SelectedFile = currentItem.get_file();
            Viacom.AddToTemplateLibCustomAction.Context.load(Viacom.AddToTemplateLibCustomAction.SelectedFile);

            //Excecuting executeQueryAsync to copy the file				
            Viacom.AddToTemplateLibCustomAction.Context.executeQueryAsync(
				function (sender, args) {
				    var listTitle = Viacom.AddToTemplateLibCustomAction.CurrentLib.get_title();
				    Viacom.AddToTemplateLibCustomAction.EnableButton = false;
				    if (listTitle != "Templates" && Viacom.AddToTemplateLibCustomAction.SelectedFile.get_checkOutType() != SP.CheckOutType.online) {
				        Viacom.AddToTemplateLibCustomAction.EnableButton = true;
				        RefreshCommandUI();
				    }
				}, function (sender, args) {
				    console.log(args.get_message());
				}
			);


            Viacom.AddToTemplateLibCustomAction.Context.executeQueryAsync(Viacom.AddToTemplateLibCustomAction.onCurrentListLoadSucceeded, Viacom.AddToTemplateLibCustomAction.onCurrentListLoadFailed);
        }
        else {
            var enableButton = Viacom.AddToTemplateLibCustomAction.EnableButton;
            Viacom.AddToTemplateLibCustomAction.EnableButton = null;
            return enableButton;
        }
    }
    else {
        return false;
    }
};

Viacom.AddToTemplateLibCustomAction.addDocToTemplateLib = function () {
    Viacom.AddToTemplateLibCustomAction.waitDialog = SP.UI.ModalDialog.showWaitScreenWithNoClose('Processing...', '', 130, 340);
    try {
        var showInDocument = $('input:checkbox[name=ShowInNewDocument]').is(':checked');
        var templateName = $('input:Text[name=TemplateName]').val();
        var associatedLibrariesResult = new Array();
        var listOptions = document.getElementById('shuttle-dst').options;
        for (var i = 0; i < listOptions.length; i++) {
            associatedLibrariesResult.push(listOptions[i].text);
        };

        if (Viacom.AddToTemplateLibCustomAction.SelectedFile != null) {
            var destinationlibUrl = Viacom.AddToTemplateLibCustomAction.CurrentWeb.get_serverRelativeUrl() + '/Templates/' + Viacom.AddToTemplateLibCustomAction.SelectedFile.get_name();

            Viacom.AddToTemplateLibCustomAction.SelectedFile.copyTo(destinationlibUrl, true);

            //Excecuting executeQueryAsync to copy the file
            Viacom.AddToTemplateLibCustomAction.Context.executeQueryAsync(
				function (sender, args) {
				    console.log("Copy selected file to template library successfully.");
				    var destFile = Viacom.AddToTemplateLibCustomAction.CurrentWeb.getFileByServerRelativeUrl(destinationlibUrl);
				    destFile.checkOut();
				    Viacom.AddToTemplateLibCustomAction.Context.load(destFile);
				    Viacom.AddToTemplateLibCustomAction.Context.executeQueryAsync(
						function (sender, args) {
						    var libFieldToUpdate = destFile.get_listItemAllFields();
						    Viacom.AddToTemplateLibCustomAction.Context.load(libFieldToUpdate);
						    Viacom.AddToTemplateLibCustomAction.Context.executeQueryAsync(
								function (sender, args) {
								    libFieldToUpdate.set_item("AssociatedLibraries", associatedLibrariesResult);
								    libFieldToUpdate.set_item("TemplateName", templateName);
								    libFieldToUpdate.set_item("ShowInNewDocument", true);
								    libFieldToUpdate.update();
								    Viacom.AddToTemplateLibCustomAction.Context.load(libFieldToUpdate);
								    destFile.checkIn();
								    Viacom.AddToTemplateLibCustomAction.Context.load(destFile);
								    Viacom.AddToTemplateLibCustomAction.Context.executeQueryAsync(
										function (sender, args) {
										    console.log("Selected file copy to template library and update metadata properties successfully.");
										    Viacom.AddToTemplateLibCustomAction.waitDialog.close(1);
										    SP.UI.ModalDialog.commonModalDialogClose(1);

										    var message = STSHtmlEncode("Selected File added to template library.");

										    // create sharepoint notification
										    var notificationData = new SPStatusNotificationData("", message, _spPageContextInfo.siteAbsoluteUrl + "/PublishingImages/success.png", null);
										    var notification = new SPNotification(SPNotifications.ContainerID.Status, "", false, null, null, notificationData);

										    //show sharepoint notification tile
										    notification.Show(false);
										}, function (sender, args) {
										    console.log(args.get_message());
										    Viacom.AddToTemplateLibCustomAction.waitDialog.close(0);
										    $('.addToTemplateLibCustomAction div.error span').text("Faild to update library column in Template library.");
										    $('.addToTemplateLibCustomAction div.error').css('display', 'block');
										});
								}, function (sender, args) {
								    console.log(args.get_message());
								    Viacom.AddToTemplateLibCustomAction.waitDialog.close(0);
								    $('.addToTemplateLibCustomAction div.error span').text("Faild to update file metadata properties in Template library.");
								    $('.addToTemplateLibCustomAction div.error').css('display', 'block');
								});
						}, function (sender, args) {
						    console.log(args.get_message());
						    Viacom.AddToTemplateLibCustomAction.waitDialog.close(0);
						    $('.addToTemplateLibCustomAction div.error span').text("Faild to update file metadata properties in Template library.");
						    $('.addToTemplateLibCustomAction div.error').css('display', 'block');
						});
				}, function (sender, args) {
				    console.log(args.get_message());
				    Viacom.AddToTemplateLibCustomAction.waitDialog.close(0);
				    $('.addToTemplateLibCustomAction div.error span').text("Faild to copy document in Template library.");
				    $('.addToTemplateLibCustomAction div.error').css('display', 'block');
				});
        }
    }
    catch (e) {
        console.log('Request failed. ' + e.message);
        Viacom.AddToTemplateLibCustomAction.waitDialog.close(0);
        $('.addToTemplateLibCustomAction div.error span').text("Some error found to copy file to template library.");
        $('.addToTemplateLibCustomAction div.error').css('display', 'block');
    }
};


Viacom.AddToTemplateLibCustomAction.cancel = function () {
    SP.UI.ModalDialog.commonModalDialogClose(0);
};

Viacom.AddToTemplateLibCustomAction.closeCallBack = function (result) {
    SP.UI.ModalDialog.commonModalDialogClose(result);
};

//jquery  Function For Select Box
(function ($) {
    var methods = {
        init: function () {
            return this.each(function () {
                var $src = $(this);

                // Create the destination select box.
                var $dst = $('<select/>', {
                    id: 'shuttle-dst'
                });
                var $attr = $src.prop("attributes");

                // Copy the settings of the source selection to the destination.
                $.each($attr, function () {
                    if (this.name != 'id') {
                        if (this.name == 'name') {
                            $dst.attr(this.name, this.value + '-dst');
                        }
                        else {
                            $dst.attr(this.name, this.value);
                        }
                    }
                });

                // Provide button controls for transferring options between boxes.
                var $controls = $('<div id="shuttle-container-controls"/>').append('<button id="shuttle-control-src-dst">&raquo;</button>').append('<button id="shuttle-control-dst-src">&laquo;</button>');

                // Make the UI elements the same widths and heights.
                $dst.css('width', $src.width());
                $dst.attr('size', $src.attr('size'));

                // Organize the elements into a <div> hierarchy.
                $src.after($dst).after($controls);
                $src.wrap('<div id="shuttle-container-src" class="shuttle-container-src"/>');
                $dst.wrap('<div id="shuttle-container-dst" class="shuttle-container-dst"/>');
                $src.next().andSelf().wrapAll('<div id="shuttle-container-filter"/>');
                $('#shuttle-container-filter').nextUntil('#shuttle-container-dst').wrapAll('<div id="shuttle-container">');
                $('#shuttle-container-filter').
                prependTo('#shuttle-container');
                $('#shuttle-container-dst').
                appendTo('#shuttle-container');

                // Transfer all selected options from the source to the destination.
                $controls.find('#shuttle-control-src-dst').on('click', function (event) {
                    $.fn.shuttle.transfer($src, $dst);
                    return false;
                });

                // Transfer all selected options from the destionation to the source.
                $controls.find('#shuttle-control-dst-src').on('click', function (event) {
                    $.fn.shuttle.transfer($dst, $src);
                    return false;
                });

                // Create a copy of the source options to use when matching the regex.
                var $options = [];
                $src.find("option").each(function () {
                    $options.push({
                        value: $(this).val(),
                        text: $(this).text()
                    });
                });

                $src.data("options", $options);
            });
        }
    };

    $.fn.shuttle = function (method) {
        methods.init.apply(this);
    };

    $.fn.shuttle.transfer = function (src, dst) {
        dst.append($('option:selected', src).remove());
        var options = $("option", dst);
        options.sort(function (a, b) {
            return a.text.localeCompare(b.text);
        });
        dst.empty().append(options);
    };
})(jQuery);