if ($snapin -eq $null) 
{    
	Write-Output "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
} 

# ===================================================================================
# Log output to CSV file
# ===================================================================================
function WriteCSVLog($string)
{
   $string | out-file -Filepath $logCSVLocation -Encoding ASCII -append
}

# =================================================================================
#
# FUNC:	Get-ScriptLogFileName
# DESC:	Gets the log file nme based on invocation script name
#		Suffice the script with execution date and returns filename string 
#		in format <<Log File Name>>_<<ddMMMyyyyHHmmss>>.log
#
# =================================================================================
function Get-ScriptLogFileName([string]$logFilePrefix)
{
		$currentDate = Get-Date -Format "ddMMMyyyyHHmmss"
		$logFileName = $logFilePrefix + $currentDate  + ".log"

		Return $logFileName
}

function UpdateMMDNotesField([string]$ConfigPath = "")
{
    $currentPath=Get-Location
    $logCSVLocation=[string]$currentPath + "\Logs\FixMMDNoteField_$(get-date -format `"yyyyMMdd_hhmmsstt`").csv"
    $columnsPropertyList = @()
    $prvWebID=""
    $cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
    else
    {
	    Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
        Write-Output "Sucessfully read config file $ConfigPath file"
        $webUrl=$cfg.Fields.WebUrl
        $excludeSiteCollections=$cfg.Fields.ExcludeSiteCollections -split "|"
		# if level is Web then it will run for all the site collection under this webapplication
		if($cfg.Fields.Level -eq "Web")
		{
			$siteCollList=Get-SPSite -limit all -WebApplication $webUrl
		}
		else{
			$siteCollList=$cfg.Fields.IncludeSiteCollection -split "#"
		}
		
        $count=0;
        $webIdList=""
        $listIDList=""
        WriteCSVLog "Status,Field Display Name,Field Internal Name,Execution Level,Site Collection Url,Web Url,List Name, Content Type Name,Message"
        foreach($site in $siteCollList)
        {
           if(!($excludeSiteCollections -ccontains $site.ServerRelativeUrl))
            { 
			
				if($cfg.Fields.Level -eq "Web")
				{
				
				}
				else{
					
						$siteCollUrl = $webUrl + $site
                        try{
						$site = Get-SPSite -Identity $siteCollUrl
						$isSiteExist=$true
					}
					catch{
						Write-Host "`nProcessing start for site collection : $($siteCollUrl)" -ForegroundColor DarkGreen -BackgroundColor Red
						Write-Output "`nProcessing start for site collection : $($siteCollUrl)"
						WriteCSVLog "Error,,,Site Column,$($siteCollUrl),,,$($Error)"
						$isSiteExist=$false
					}
				}
				if($isSiteExist)
				{
					$web=$null
					$siteColl=$null
					$cts=$null
					$web =Get-SPWeb -Identity $site.Url
					$siteColl=$site
					$cts = $web.ContentTypes
					Write-Host "`nProcessing start for site collection : $($siteColl.Url)" -ForegroundColor DarkGreen -BackgroundColor Yellow
					Write-Output "`nProcessing start for site collection : $($siteColl.Url)"
					if($web -ne $null)                {                                                                                                                                                                     
						foreach($field in $cfg.Fields.Field)
						{
							try
							{
								$clm = $null
								try
								{
									$clm = $web.Fields.GetFieldByInternalName($field.InternalName)
								}
								catch
								{
									Write-Host "`nField '$($column.InternalName)' not found in web in function UpdateMMDNotesField '$($web.Site.Url)' `n" -ForegroundColor Yellow
									Write-Output "`nField '$($column.InternalName)' not found in web in function UpdateMMDNotesField '$($web.Site.Url)' `n"
									WriteCSVLog "Error,,$($field.InternalName),UpdateMMDNotesField,$($web.Site.Url),,,,$($Error)"  
								}
								if($clm -ne $null)
								{
									UpdatSiteColumnLeveleMMDNoteField $field $web.Url
									$ctCount=$cts.Count
									for ($i=0; $i -lt $ctCount; $i++)
									{
										$ct=$cts[$i]
										$ctField=$null
										$ctField = $ct.FieldLinks[$field.InternalName]
											if($ctField -ne $null)
												{
												   #functionName $ct,$field.InternalName,$web
												   UpdateMMDNoteFieldInContentType $ct $field.InternalName
												   #$count++
												}
										 # }
									}

									try
									{
										 $fieldAssociation=$clm.ListsFieldUsedIn()
										 foreach($associatedList in $fieldAssociation)
										 {
											if($prvWebID -ne $associatedList.WebID)
											 {
												$web=$siteColl.OpenWeb($associatedList.WebID)
											 }
												
												UpdateMMDNoteFieldInListContentType $web.Url $field.InternalName $associatedList.ListID
												UpdateListField $web.Url $associatedList.ListID $field.InternalName
												$prvWebID=$associatedList.WebID
												$web.Dispose()
										 }
								 
									 }
									 catch
									 {
										Write-Output "Exception in UpdateMMDNotesField for web level update method.`nException :" $Error
										Write-Host "Exception in UpdateMMDNotesField for web level update method.`nException :" $Error -ForegroundColor Red
									   WriteCSVLog "Error,,$($field.InternalName),Start-Web level,$($web.Site.Url),,,,$($Error)"
								
									 }
								}
							}
							catch
							{
							  Write-Output "Exception in UpdateMMDNotesField method.`nException :" $Error
							  Write-Host "Exception in UpdateMMDNotesField method.`nException :" $Error -ForegroundColor Red
							  WriteCSVLog "Error,,$($field.InternalName),Start,$($web.Site.Url),,,,$($Error)"  
							}
							 
						} #end of inner for loop
					  Write-Host "Successfully done" -ForegroundColor green
					}
					else
					{
					   Write-Host "Error:-In getting web for  $web" -ForegroundColor Red
					   Write-Output "Error:-In getting web for  $web"
					}  
				$web.Dispose()
				$site.Dispose()
				$siteColl.Dispose()
			   }
            }
						
			
        
    }#end of for loop

}
}

function UpdatSiteColumnLeveleMMDNoteField([object] $column,$weburl)
{
    try
    {
        $error.Clear()
        $clm = $null
		$isUpdate=$false
        $web=get-spweb $weburl
        try
        {
            $clm = $web.Fields.GetFieldByInternalName($column.InternalName)
        }
        catch
        {
            Write-Host "`nField '$($column.InternalName)' not found in web in function UpdatSiteColumnLeveleMMDNoteField '$($web.Site.Url)' `n" -ForegroundColor Yellow
            Write-Output "`nField '$($column.InternalName)' not found in web in function UpdatSiteColumnLeveleMMDNoteField '$($web.Site.Url)' `n"
        }
        if($clm -ne $null)
        {
            # Boolean values must be converted before they are assigned in PowerShell.
            if($clm.ReadOnlyField)
            {
                $clm.ReadOnlyField = $false
				$isUpdate=$true
            }
            if(!$column.Hidden)
            {
                $type = $clm.GetType()
                $mi = $type.GetMethod("SetFieldBoolValue",[System.Reflection.BindingFlags]$([System.Reflection.BindingFlags]::NonPublic -bor [System.Reflection.BindingFlags]::Instance))
                $mi.Invoke($clm, @("CanToggleHidden",$true))
                $clm.Update($true)
                $clm = $web.Fields.GetFieldByInternalName($column.InternalName)
                $clm.Hidden = $true
				$isUpdate=$true
            }
			if($isUpdate)
			{
            $clm.Update($true)
            $web.Update()
		    Write-Host "`nField '$($column.InternalName)' properties updated successfully.`n" -ForegroundColor Green
            Write-Output "`nField '$($column.InternalName)' properties updated successfully.`n"
            WriteCSVLog "Success,$($column.DisplayName),$($column.InternalName),Site Column,$($web.Site.Url),,,"
			}
			else
			{
			Write-Host "`nField '$($column.InternalName)' properties updated successfully.`n" -ForegroundColor Yellow
            Write-Output "`nField '$($column.InternalName)' properties updated successfully.`n"
            WriteCSVLog "NA,$($column.DisplayName),$($column.InternalName),Site Column,$($web.Site.Url),,,"
			}
        }
    $web.Dispose()
    }
    catch
    {
         Write-Host "`nException for column '$($column.FieldName)' `n" $Error -ForegroundColor Red
         Write-Output "`nException for column '$($column.FieldName)' `n" $Error 
         WriteCSVLog "Error,$($column.DisplayName),$($column.InternalName),Site Column,$($web.Site.Url),,,,$($Error)"
    }
}



# =================================================================================
#
# FUNC:	Update MMD note field in List Content type
# DESC:	
#
# =================================================================================

function UpdateMMDNoteFieldInListContentType($webUrl,$fieldInternalName,$libGuid)
{
    try
    { 
        $error.Clear()
        $subWeb=Get-Spweb $webUrl
        $list =$subWeb.Lists[$libGuid]
        $contentTypeList  = $list.ContentTypes
        $lstCTCount=$contentTypeList.Count
		$isUpdate=$false

        for($i=0; $i -lt $lstCTCount; $i++)
                {      
                    $contentType =$null
                    $contentType = $contentTypeList[$i]   
					if($contentType -ne $null)
					{
                        $field=$null
                        $field = $contentType.FieldLinks[$fieldInternalName]
						$isUpdate=$false
                        if($field -ne $null)
                        {
						  
                            if($field.ReadOnlyField)
                            {
						        $field.ReadOnly = $false
								$isUpdate=$true
                            }
                            if(!$field.Hidden)
                            {
						        $field.Hidden = $true
								$isUpdate=$true
                            }
							if($isUpdate)
							{
								$contentType.Update()
								$subWeb.Update()
								Write-Host "`nField '$($fieldInternalname)' properties updated successfully in UpdateMMDNoteFieldInListContentType method.`n" -ForegroundColor Green
								Write-Output "`nField '$($fieldInternalname)' properties updated successfully in UpdateMMDNoteFieldInListContentType method.`n"
								WriteCSVLog "Success,,$($fieldInternalname),ListCT,$($web.Url),,$($list.Title),$($contentType.Name),"
							}
							else
							{
								Write-Host "`nField '$($fieldInternalname)' properties updated successfully in UpdateMMDNoteFieldInListContentType method.`n" -ForegroundColor Yellow
								Write-Output "`nField '$($fieldInternalname)' properties updated successfully in UpdateMMDNoteFieldInListContentType method.`n"
								WriteCSVLog "NA,,$($fieldInternalname),ListCt,$($web.Url),,$($list.Title),$($contentType.Name),"
							} 

                        }
					}
					$error.clear()
                }

        $subWeb.Dispose()
    }
    catch
    {
        Write-Output "Exception in UpdateMMDNoteFieldInListContentType method.`nException :" $Error
        Write-Host "Exception in UpdateMMDNoteFieldInListContentType method.`nException :" $Error -ForegroundColor Red
		WriteCSVLog "Error,,$($fieldInternalname),ListCt,$($web.Url),,$($list.Title),$($contentType.Name),$($Error)"
    }
}

# =================================================================================
#
# FUNC:	Update MMD note field in List 
# DESC:	
#
# =================================================================================
function UpdateListField($webUrl,$listGuid,$fieldInternalname)
{
    try
    {
        $web=Get-SPWeb $webUrl
        $error.Clear()
        $field =$null
		$isUpdate=$false
        foreach($eachLibrary in $library)
            {
                $field  = $web.Lists[$listGuid].Fields.GetFieldByInternalName($fieldInternalname)
				$isUpdate=$false
                if($field -ne $null)
                {
                # Boolean values must be converted before they are assigned in PowerShell.
                if($field.ReadOnlyField)
                {
                    $field.ReadOnlyField = $false
					$isUpdate=$true
                }
                if(!$field.Hidden)
                {
                   $field.Hidden = $true
				   $isUpdate=$true
                }
				if($isUpdate)
				{
					$field.Update()
					$web.Update()
					Write-Host "`nField '$($fieldInternalname)' properties updated successfully.`n" -ForegroundColor Green
					Write-Output "`nField '$($fieldInternalname)' properties updated successfully.`n"
					WriteCSVLog "Success,,$($fieldInternalname),List,$($web.Url),,$($list.Name),,"
				}
				else
				{
					Write-Host "`nField '$($fieldInternalname)' properties updated successfully.`n" -ForegroundColor Yellow
					Write-Output "`nField '$($fieldInternalname)' properties updated successfully.`n"
					WriteCSVLog "NA,,$($fieldInternalname),List,$($web.Url),,$($list.Name),,"
				}
                }
               
            }
        $web.Dispose()
    }
    catch [Exception]
    {
        Write-Output "Exception in UpdateListField method.`nException :" $Error
        Write-Host "Exception in UpdateListField method.`nException :" $Error -ForegroundColor Red
        WriteCSVLog "Error,,$($fieldInternalname),List,$($web.Url),,,,$($Error)"
    }
}




# =================================================================================
#
# FUNC: UpdateMMDNoteFieldProperties
# DESC: Update MMD note properties for content type level
#
# =================================================================================
function UpdateMMDNoteFieldInContentType($contentType,$columnInternalName)
{
    try
    {  
        $error.Clear()       
        $clm=$null
		$isUpdate=$false
        try
        {
            $clm = $contentType.FieldLinks[$columnInternalName]
            if($clm -ne $null)
            {
                # Boolean values must be converted before they are assigned in PowerShell.
                if($clm.ReadOnlyField)
                {
                    $clm.ReadOnlyField = $false
					$isUpdate=$true
                }
                if(!$clm.Hidden)
                {
                   $clm.Hidden = $true
				   $isUpdate=$true
                }
				if($isUpdate)
				{
					$contentType.Update()
					#$web.Update()
					Write-Host "`nField '$($columnInternalName)' properties updated successfully.`n" -ForegroundColor Green
					Write-Output "`nField '$($columnInternalName)' properties updated successfully.`n"
					WriteCSVLog "Success,,$($columnInternalName),Content Type,$($web.Site.Url),,,$($contentType.Name)"
				}
				else
				{
					Write-Host "`nField '$($columnInternalName)' properties updated successfully.`n" -ForegroundColor Yellow
					Write-Output "`nField '$($columnInternalName)' properties updated successfully.`n"
					WriteCSVLog "NA,,$($columnInternalName),Content Type,$($web.Site.Url),,,$($contentType.Name)"
				}
            }
     
        }
        catch
        {
            Write-Host "`nField '$($columnInternalName)' not found Content Type '$($web.Site.Url)' `n" -ForegroundColor Red
            Write-Output "`nField '$($columnInternalName)' not found Content Type '$($web.Site.Url)' `n"
            WriteCSVLog "Error,,$($columnInternalName),Content Type,$($web.Site.Url),,,$($contentType.Name),$($Error)"
        }
        
    }
    catch
    {
        Write-Output "Exception in UpdateMMDNoteFieldInContentType method.`nException :" $Error
        Write-Host "Exception in UpdateMMDNoteFieldInContentType method.`nException :" $Error -ForegroundColor Red
        WriteCSVLog "Error,,$($columnInternalName),Content Type,$($web.Site.Url),,,$($contentType.Name),$($Error)"
    }
}



