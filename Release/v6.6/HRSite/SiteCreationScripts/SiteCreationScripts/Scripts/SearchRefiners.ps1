﻿# ===================================================================================
# Change acces request email ID function
# ===================================================================================

function CreateRefiners([string]$ConfigPath = "")
{
    $error.Clear()
    $cfg = [xml](get-content $ConfigPath)

    # Exit if config file is invalid
    if( $? -eq $false ) 
    {
        Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
    }
    Write-Output "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    if($Error.Count -eq 0)
	{
        if( $? -eq $false ) 
	    {
            Write-Output "Could not read config file. Exiting ..."
            Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
	    }
        if(![string]::IsNullOrEmpty($cfg.Search.Site))
        {
			try
			{
				foreach($Site in $cfg.Search.Site)
				{
                    $web = Get-SPWeb $Site.SiteUrl
                    foreach($refinerPage in $Site.Page)
                    {
                        
                        $page = $web.GetFile($refinerPage.pageUrl)
                        if ($page.Level -eq [Microsoft.SharePoint.SPFileLevel]::Checkout)
                        { 
                              if ($page.CheckedOutBy.UserLogin -eq $spWeb.CurrentUser.UserLogin) 
                              {
                                    Write-Host "Page has already been checked out " -ForegroundColor Yellow
                                    Write-Output "Page has already been checked out "|out-null
                                
                              } 
                              else 
                              {
                                    $SPWeb.CurrentUser.LoginName
                                    $page.UndoCheckOut()
                                    $page.CheckOut()
                                    Write-Host "Check out the page override" -ForegroundColor Yellow
                                    Write-Output "Check out the page override"
                              }  
                  
                        }
                        else
                        {
                           $page.CheckOut() 
                           Write-Host "Check out the page" -ForegroundColor Green
                           Write-Output "Check out the page"
                        }
                        $pageUrl = $refinerPage.pageUrl
                        SetRefinersOnPage $web $pageUrl $refinerPage $Site $page
						$page.CheckIn("Checkin by PowerShell")
						$page.Publish("Published by PowerShell")
						Write-Host "Page has been checked in" -ForegroundColor Green
						Write-Output "Page has been checked in"| Out-Null 
                    }
				}
			}
			catch
			{
				Write-Host "(ERROR : "$_.Exception.Message")" -ForegroundColor Red
                Write-Output "(ERROR : "$_.Exception.Message")"
			}
        }
		else
		{
			Write-Output "SiteUrl is empty for $($Site.Url)"|out-null    
            Write-Host "SiteUrl is empty for $($Site.Url)" -ForegroundColor Yellow
		}
    }
}


function SetRefinersOnPage($web,$pageUrl,$refinerPage,$Site,$page)
{
    try
    {
    $webPartManager = $web.GetLimitedWebPartManager($Site.SiteUrl+$pageUrl, [System.Web.UI.WebControls.WebParts.PersonalizationScope]::Shared)
    $refinerWebPart = $webpartmanager.webparts | ? { $_.title -eq 'Refinement' }
    $newRefinerConfigurations = @()
$newJsonObject = @{}

$jsonString = @"
{
"maxNumberRefinementOptions" : "",
"propertyName" :"",
"type" : "",
"displayTemplate" : "",
"displayName" : ""
}
"@
        foreach($refiner in $refinerPage.Refiners)
        {
            $newRefinerJson = ConvertFrom-Json $jsonString
            $newRefinerJson.propertyName = $refiner.PropertyName
            $newRefinerJson.displayName = $refiner.DisplayName
            $newRefinerJson.displayTemplate = $refiner.DisplayTemplate
            $newRefinerJson.maxNumberRefinementOptions = $refiner.MaxRefiners
            $newRefinerJson.type = $refiner.Type
            $newRefinerConfigurations += $newRefinerJson


            $newJsonObject.refinerConfigurations = $newRefinerConfigurations

            $refinerWebPart.SelectedRefinementControlsJson = ConvertTo-Json $newJsonObject -Depth 3 -Compress

            $webpartmanager.SaveChanges($refinerWebPart)
            Write-Host "Refiner $($refiner.PropertyName) has been set on page $($pageUrl)" -ForegroundColor Green
            Write-Output "Refiner $($refiner.PropertyName) has been set on page $($pageUrl)" 

        }
    }
    catch
    {
			Write-Host "(ERROR : "$_.Exception.Message")" -ForegroundColor Red
            Write-Output "(ERROR : "$_.Exception.Message")"        
    }
}

