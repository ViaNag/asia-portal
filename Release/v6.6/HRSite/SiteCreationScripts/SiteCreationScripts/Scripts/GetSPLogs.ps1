# =================================================================================
#
# FUNC: GetSPLogs
# DESC: Get SP Logs for time/Corelations ID
# =================================================================================
function GetSPLogs([string]$ConfigPath = "")
{
    try
    {
        $error.Clear()
        $cfg = [xml](get-content $ConfigPath)

	    # Exit if config file is invalid
	    if( $? -eq $false ) 
        {
		    Write-Host "Could not read config file. Exiting ..."  -ForegroundColor Red
            Write-Output "Could not read config file. Exiting ..."
	    }
	
	    Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
        Write-Output "Sucessfully read config file $ConfigPath file`n"
	    if($Error.Count -eq 0)
	    {
            try 
            {
                $Logs = $cfg.Logs
				
				foreach($Log in $Logs.Log)
				{
					{param($operationFolder)}
					
					
					$date = Get-Date -UFormat "%Y-%m-%d---%H-%M-%S"
					
					$logFolderLocation="D:\SiteCreation\Logs\" + $operationFolder + "\SPLog" + $date + ".log"
					
					$CorrelationID = $Log.Correlation
					$StartDateForLog = $Log.StartTimeForLog
					$EndDateForLog = $Log.EndTimeForLog
					
					if(![string]::IsNullOrEmpty($CorrelationID))
					{
						Write-host	"Generating Logs for the Correlation ID - " $CorrelationID -ForegroundColor Yellow
						Merge-SPLogFile -Path $logFolderLocation -Correlation $CorrelationID
					}
					Elseif(![string]::IsNullOrEmpty($StartDateForLog) -and ![string]::IsNullOrEmpty($EndDateForLog))
					{
						Write-host	"Generating Logs for the Start Time - " $StartDateForLog "And End Time - " $EndDateForLog -ForegroundColor Yellow
						Merge-SPLogFile -Path $logFolderLocation -StartTime $StartDateForLog -EndTime $EndDateForLog
					}
				}
            }
            catch
            {
                Write-Host "`nException :" $Error -ForegroundColor Red
                Write-Output "`nException :" $Error
            }
	    }
	    else
	    {
		    Write-Host $Error -ForegroundColor "Red"
            Write-Output $Error
	    }
    }
    catch
    {
        Write-Host "`nException to create group `n" $Error -ForegroundColor Red
        Write-Output "`nException to create group`n" $Error
    }
}