﻿#add sharepoint cmdlets
if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
{    
      Add-PsSnapin Microsoft.SharePoint.PowerShell
}

function CreateNavigation([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
		Write-Output "Could not read config file. Exiting ..." |out-null
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
	Write-Output "Sucessfully read config file $ConfigPath file" |out-null
	if($Error.Count -eq 0)
	{

		$NavXml= $cfg
		foreach($webUrl in $NavXml.Navigation.WebUrl)
		{
			Write-Host
			Write-Host  -f Yellow "-------------Creating a navigation on web "$webUrl.URL"----------------"
			Write-Host
			$SPWeb =Get-SPWeb -Identity $webUrl.URL
			$pubweb  = $SPWeb.Navigation.SearchNav
			$count = $SPWeb.navigation.SearchNav.count
		   
			#delete all Current Navigation Link
			for( $i=$count;$i-ge 0;$i--)
			{   
				  $node = $pubweb.navigation.SearchNav[$i]
				  if($node -ne $null)
				  {
					  $pubweb.navigation.SearchNav.delete($node)
				  }

			}
			
			#create Current Navigation Link
			
			foreach($NavRS in $webUrl.Nav)
			{
				$node = new-object -TypeName "Microsoft.SharePoint.Navigation.SPNavigationNode" -ArgumentList $NavRS.Title, $NavRS.URL, $true
				$pubweb.Navigation.SearchNav.AddAsFirst($node)
			}

		}
		Write-Host "Navigation has been created" -ForegroundColor Green
		Write-Output "Navigation has been created" |out-null
	}
}