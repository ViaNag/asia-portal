﻿#[void][System.reflection.Assembly]::LoadWithPartialName("Microsoft.SharePoint") 

function GetAllSites()
{
    $farm = [Microsoft.SharePoint.Administration.SPFarm]::Local
    $websvcs = $farm.Services | where -FilterScript {$_.GetType() -eq [Microsoft.SharePoint.Administration.SPWebService]}
    $expireyDate=(Get-date).AddYears($Global.Year)
    $records = @()
    foreach ($websvc in $websvcs) 
    {
        foreach ($webapp in $websvc.WebApplications) 
        {
             foreach ($site in $webapp.Sites) 
             {
                 foreach ($web in $site.AllWebs) 
                 {
                    $records += GetRecords $web $expireyDate
                    $web.Dispose()
                 }
                 $site.Dispose() 
             }         
        }

    }

    [string] $location = Get-Location
    $filename  = $location + "\CSV\" + $farm.Name + ".csv"
    $records| Where-Object {$_}|Select-Object WebApplication,SiteCollection,WebTitle,WebURL,ListTitle,ListUrl,ItemId,ItemTitle,LastModifiedOn,LastModifiedBy | ConvertTo-Csv -notype >$filename
    Write-Host "CSV file created on " +  $filename -ForegroundColor Green
    
}

function ProcessSites()
{
    $expireyDate=(Get-date).AddYears($Global.Year)
     
    [string] $location = Get-Location
    foreach ($site in $Global.Sites.Site) 
    {
        $Id=$site.Url
        Write-Host $Id -ForegroundColor Green 
        $oSite = Get-SPSite $Id
        $records = @()
        foreach ($web in $oSite.AllWebs) 
        {
            $records += GetDocsToDeleted $web $expireyDate
            $web.Dispose()
        }
        $oSite.Dispose()
        $filename  = $location + "\CSV\" + $site.Name + ".csv"
        $records| Where-Object {$_}|Select-Object WebApplication,SiteCollection,WebTitle,WebURL,ListTitle,ListUrl,ItemId,ItemTitle,LastModifiedOn,LastModifiedBy,DocumentCreatedBy,DocumentCreatedOn,DocumentName,DocumentURL,ToBeDeleted,ToBeUpdated | ConvertTo-Csv -notype >$filename
        Write-Host "CSV file created on " +  $filename -ForegroundColor Green       

    }

    
    
}

function GetRecords($web,$expireyDate)
{
    $results = @()
    foreach ($list in $web.Lists)
    {
        $query= New-Object Microsoft.SharePoint.SPQuery;
        $expireyDate = [microsoft.sharepoint.utilities.sputility]::CreateISO8601DateTimeFromSystemDateTime($expireyDate);

        $query.Query="<Where><Leq><FieldRef Name='Modified' /><Value Type='DateTime' IncludeTimeValue='False'>$expireyDate</Value></Leq></Where>";    
        $items=$list.GetItems($query);

        if($items.Count -gt 0)
        {
            foreach($item in $items)
            {
                  
                $details = New-Object -TypeName PSObject -Property @{            
                                WebApplication   = $web.Site.WebApplication.Name              
                                SiteCollection     = $web.Site.Url                 
                                WebTitle      = $web.Title 
                                WebURL   = $web.Url              
                                ListTitle     = $list.Title                 
                                ListUrl      = $list.RootFolder.Url
                                ItemId   = $item.ID.ToString()              
                                ItemTitle     = $item.Title                 
                                LastModifiedOn      = $item["Modified"] 
                                LastModifiedBy      = $item["Modified By"] 
                                ToBeDeleted    ="Y"
                            }                           
                $results += $details
                
            }
        }
        
    }
    return $results
}

function GetDocsToDeleted($web,$expireyDate)
{
    $results = @()
    $ctName = "Document"  
    $count = 0  
    foreach ($list in $web.Lists)
    {
         $IsValid= IsValidList $list
        
        if($IsValid)
        {
            foreach ($ctype in $list.ContentTypes)  
            {  
                
               if ($ctype.Name -eq $ctName)  
               {  
                    Write-Host $list.DefaultViewUrl -ForegroundColor Yellow 
                    $count++ 
                    $query= New-Object Microsoft.SharePoint.SPQuery;
                    $expireyDate = [microsoft.sharepoint.utilities.sputility]::CreateISO8601DateTimeFromSystemDateTime($expireyDate);

                    $query.Query="<Where><Leq><FieldRef Name='Modified' /><Value Type='DateTime' IncludeTimeValue='False'>$expireyDate</Value></Leq></Where>";
                    $query.ViewAttributes="Scope = 'Recursive'";   
                    $items=$list.GetItems($query);

                    if($items.Count -gt 0)                                                                                                   
                    {
                        foreach($item in $items)
                        {
                  
                            $details = New-Object -TypeName PSObject -Property @{            
                                            WebApplication   = $web.Site.WebApplication.Name              
                                            SiteCollection     = $web.Site.Url                 
                                            WebTitle      = $web.Title 
                                            WebURL   = $web.Url              
                                            ListTitle     = $list.Title                 
                                            ListUrl      = $list.RootFolder.Url
                                            ItemId   = $item.ID.ToString()              
                                            ItemTitle     = $item.Title                 
                                            LastModifiedOn      = $item["Modified"] 
                                            LastModifiedBy      = $item["Modified By"] 
                                            DocumentCreatedBy=$item["Created By"]
                                            DocumentCreatedOn=$item["Created"]
                                            DocumentName=$item.Name
                                            DocumentURL=$item.URL
                                            ToBeDeleted    ="N"
                                            ToBeUpdated="Y"
                                        }                           
                            $results += $details
                
                        }
                    }
                }
            }
        }
        
    }
    Write-Host "Found $count lists using the content type '$ctName'." -ForegroundColor Green 
    return $results
}
function CleanData()
{
    $farm = [Microsoft.SharePoint.Administration.SPFarm]::Local
    [string] $location = Get-Location

   
    foreach ($site in $Global.Sites.Site) 
    {
         $path  = $location + "\CSV\" + $site.Name + ".csv"
         $ImportCsv = Import-csv -path $path
        foreach ($item in $ImportCsv)
        {
       
            if($item.ToBeDeleted -eq $Global.StatusColumn.Value)
            {
           
                $web = Get-SPWeb $item.WebURL
                if($web -ne $null)
                {
                    $listUrl = $web.Url + "\" + $item.ListUrl
                    $list = $web.GetList($listUrl)
                    #$list = $web.Lists[$item.ListTitle]
                    if($list -ne $null)
                    {
                       $listItem = $list.GetItemByID($item.ItemId)
                       if($listItem -ne $null)
                       {
                            $listItem.Delete()
                            Write-Host "Item Deleted: Title " $item.Name " :List " $item.ListTitle " :Web " $item.WebURL -ForegroundColor Green
                       }

                    }
                
                    $web.Dispose()
                }
            }
            if($item.ToBeUpdated -eq "Y")
            {
           
                $web = Get-SPWeb $item.WebURL
                if($web -ne $null)
                {
                    $listUrl = $web.Url + "\" + $item.ListUrl
                    $list = $web.GetList($listUrl)
                    if($list -ne $null)
                    {
                       $listItem = $list.GetItemByID($item.ItemId)
                       if($listItem -ne $null)
                       {
                            
                            $UId=$item.LastModifiedBy.Split(';#')[0]
                            #$ModifiedDate = [microsoft.sharepoint.utilities.sputility]::CreateISO8601DateTimeFromSystemDateTime($item.LastModifiedOn);
                           $ModifiedDate =  Get-Date $item.LastModifiedOn
                            #$UName=$item.LastModifiedBy.Split(';#')[1]
                            #$SPFieldUserValue = New-Object Microsoft.SharePoint.SPFieldUserValue($web,$UId,$UName) 
                            #$listItem["Editor"]=$SPFieldUserValue

                            $SPUser=$web.AllUsers.GetByID("17")
                            $oldValue=$listItem["Editor"]
                            $listItem["Editor"]=$SPUser
                            $listItem["Modified"]=$ModifiedDate
                           $listItem.UpdateOverwriteVersion()

                            $NewValue=$listItem["Editor"]
                            Write-Host "Item Updated- Title: " $item.Name " List: " $item.ListTitle " Web: " $item.WebURL " Old User:" $oldValue " New Value:" $NewValue -ForegroundColor Green
                       }

                    }
                
                    $web.Dispose()
                }
            }
        } 
    }
}

function ContentTypeCleanUp()
{
    $farm = [Microsoft.SharePoint.Administration.SPFarm]::Local
    $websvcs = $farm.Services | where -FilterScript {$_.GetType() -eq [Microsoft.SharePoint.Administration.SPWebService]}
    $expireyDate=(Get-date).AddYears($Global.Year)
    $records = @()
    foreach ($websvc in $websvcs) 
    {
        foreach ($webapp in $websvc.WebApplications) 
        {
             foreach ($site in $webapp.Sites) 
             {
                 SetContentType $site
                 $site.Dispose() 
             }         
        }

    }

    [string] $location = Get-Location
    $filename  = $location + "\CSV\" + $farm.Name + ".csv"
    $records| Where-Object {$_}|Select-Object WebApplication,SiteCollection,WebTitle,WebURL,ListTitle,ListUrl,ItemId,ItemTitle,LastModifiedOn,LastModifiedBy | ConvertTo-Csv -notype >$filename
    Write-Host "CSV file created on " +  $filename -ForegroundColor Green
    
}

function ContentTypeCleanUpConfigValuesOnly([string]$ConfigPath = "")
{
    $error.Clear()
    $ctCleanUpCSV= import-csv -Path $ConfigPath
    if( $? -eq $false ) 
    {
	     Write-Output "Could not read config file. Exiting ..." -ForegroundColor Red
         Write-Host "Could not read config file. Exiting ..."
	}
	Write-Output "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Host "Sucessfully read config file $ConfigPath file"
	if($Error.Count -eq 0)
	{
	    if( $? -eq $false ) 
	    {
            Write-Output "Could not read config file. Exiting ..." -ForegroundColor Red`
            Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
	    }
        $records = @()
        foreach ($site in $ctCleanUpCSV) 
        {
                $records +=SetContentType $site
                $site.Dispose() 
                [string] $location = Get-Location
                $filename  = $location + "\CSV\" + $site.Name + ".csv"
                $records| Where-Object {$_}|Select-Object WebApplication,SiteCollection,WebTitle,WebURL,ListTitle,ListUrl,ItemId,ItemTitle,LastModifiedOn,LastModifiedBy,DocumentCreatedBy,DocumentCreatedOn,DocumentName,DocumentURL,CheckedOutByUser | ConvertTo-Csv -notype >$filename
                Write-Output "CSV file created on "  $filename 
        }         
   }
    
}

function SetContentType($site)
{
   

    $oSite = Get-SPSite $site.SiteUrl    
    $rootWeb = $oSite.RootWeb
    $oldCt = $rootWeb.ContentTypes[$site.OldContentType]
    $newCt = $rootWeb.ContentTypes[$site.NewContentType]
   
    #$usages = [Microsoft.Sharepoint.SPContentTypeUsage]::GetUsages($oldCt)
    $result = @()
   
     if($newCt -ne $null)
    {
        foreach($contentType in [Microsoft.SharePoint.SPContentTypeUsage]::GetUsages($oldCt))
	    {
            if($contentType.IsUrlToList)
            {                                                        
               #$url = $Global.WebApplication.Url + $contentType.Url.Substring(0,$contentType.Url.LastIndexOf("/"))
           
                    #$ctWeb = Get-SPWeb $url.Replace("Lists","")
                    #$list = $rootWeb.GetList($contentType.Url)
                    Write-Host $contentType.Url -ForegroundColor Green
                    Write-Output $contentType.Url |Out-Null
                    
                    $IterateMore=$true
                    foreach ($web in $oSite.AllWebs)
                    {
                        foreach ($lst in $web.Lists)
                        {
                            
                           if ($lst.RootFolder.ServerRelativeUrl -eq $contentType.Url)
                           {
                              $list=$lst
                              $IterateMore=$false
                              break
                           }
                        }
                        #$web.Dispose()
                        
                        if (!$IterateMore)
                        {
                            break;
                        }
                    }
                    $IsValid= IsValidList $list $site     
                    if($IsValid)
                    {
                        $listCTNames=$list.ContentTypes | Select Name
                        $newctExist=$false
                        $oldctExist=$false
                        foreach($ctIterator in $listCTNames)
                        {
                            $ctIteratiorNm=[System.String]$ctIterator.Name
                            if($ctIteratiorNm -eq $site.NewContentType)
                            {
                                $newctExist=$true
                                $break
                            }
                        }
                        foreach($ctIterator in $listCTNames)
                        {
                            $ctIteratiorNm=[System.String]$ctIterator.Name
                            if($ctIteratiorNm -eq $site.OldContentType)
                            {
                                $oldctExist=$true
                                $break
                            }
                        }
                        if($oldctExist)
                        {
                            if(!$newctExist)
                            {
                                $list.ContentTypesEnabled = $true
                                $c = $list.ContentTypes.Add($newCt)
                                $list.Update()
                            }
                            $CTNew = $list.ContentTypes[$site.NewContentType]
                            $CTOld = $list.ContentTypes[$site.OldContentType]
     
                            SetDefaultContentTypeOnList $list $CTNew
                            if($CTOld -ne $null)
                            {
                                $result +=UpdateContentTypeNew $web $list $CTOld $CTNew $results
                            }
                            $list.ContentTypesEnabled = $false
                            $list.Update()
                        }
                    }
                    $web.Dispose()
            }
        }
    

        
    
     $rootWeb.Dispose()
    }
   else
   {
     Write-Host "Content Type doesn't exist.Please make the required changes" -ForegroundColor Green
     Write-Output "Content Type doesn't exist.Please make the required changes" |Out-Null
   }
        return $result
}

function SetDefaultContentTypeOnList($list,$CTNew)
{ 
    
    $newOrder = New-Object System.Collections.Generic.List[Microsoft.SharePoint.SPContentType] 
    $newOrder.Add($list.ContentTypes[$CTNew.Name])  
    $list.RootFolder.UniqueContentTypeOrder = $newOrder 
    $list.RootFolder.Update() 
    $list.Update() 
}

function UpdateContentType ($web,$list,$oldCT,$newCt)
{
    $list.Items | ForEach-Object {
    #Check if the item content type currently equals the old content type specified
        if ($_.ContentType.Name -eq $oldCT.Name)
        {
            #Check the check out status of the file
            if ($_.File.CheckOutType -eq "None")
            {
                #Change the content type association for the item
                $_.File.CheckOut()
                write-host "Resetting content type for file" $_.Name "from" $oldCT.Name "to" $newCT.Name
                Write-Output "Resetting content type for file" $_.Name "from" $oldCT.Name "to" $newCT.Name |Out-Null
                $_["ContentTypeId"] = $newCT.ID
                $_.Update()
                $_.File.CheckIn("Content type changed to " + $newCT.Name, 1)
            }
            else
            {
                write-host "File" $_.Name "is checked out to" $_.File.CheckedOutByUser.ToString() "and cannot be modified"
                Write-Output "File" $_.Name "is checked out to" $_.File.CheckedOutByUser.ToString() "and cannot be modified" |Out-Null
            }
        }
        else
        {
            write-host "File" $_.Name "is associated with the content type" $_.ContentType.Name "and shall not be modified"
            Write-Output "File" $_.Name "is associated with the content type" $_.ContentType.Name "and shall not be modified" |Out-Null
        }
    }

    $list.ContentTypes.Delete($oldCT.Id)
    $list.Update()
}


function UpdateContentTypeNew ($web,$list,$oldCT,$newCT,$results)
{

    $countCheckedOutitems=0;
    foreach($val in $list.Items)
    {
        
    #Check if the item content type currently equals the old content type specified
        if ($val.ContentType.Name -eq $oldCT.Name)
        {
            #Check the check out status of the file
            if ($val.File.CheckOutStatus  -eq "None")
            {
                #Change the content type association for the item
                Write-Host  "Resetting content type for list "$list.Title "- file" $val.Name "from" $oldCT.Name "to" $newCT.Name
                Write-Output  "Resetting content type for list "$list.Title "- file" $val.Name "from" $oldCT.Name "to" $newCT.Name |Out-Null
        
                $val["ContentTypeId"] = $newCT.ID

                $val.SystemUpdate()
                
            }
            else
            {
                Write-Host $list.Title "- File" $val.Name "is checked out to" $val.File.CheckedOutByUser.ToString() "and cannot be modified"
                Write-Output $list.Title "- File" $val.Name "is checked out to" $val.File.CheckedOutByUser.ToString() "and cannot be modified" |Out-Null
                $countCheckedOutitems++;
                #$val.File.UndoCheckOut()
                #write-host "Checkout overriden" -foregroundcolor Yellow
                #$val.File.CheckOut()
                #write-host "Resetting content type for file" $val.Name "from" $oldCT.Name "to" $newCT.Name
                #$val["ContentTypeId"] = $newCT.ID
                #$val.Update()
                
                  
                    $details = New-Object -TypeName PSObject -Property @{            
                                    WebApplication   = $web.Site.WebApplication.Name              
                                    SiteCollection     = $web.Site.Url                 
                                    WebTitle      = $web.Title 
                                    WebURL   = $web.Url              
                                    ListTitle     = $list.Title                 
                                    ListUrl      = $list.RootFolder.Url
                                    ItemId   = $val.ID.ToString()              
                                    ItemTitle     = $val.Title                 
                                    LastModifiedOn      = $val["Modified"] 
                                    LastModifiedBy      = $val["Modified By"] 
                                    DocumentCreatedBy=$val["Created By"]
                                    DocumentCreatedOn=$val["Created"]
                                    DocumentName=$val.Name
                                    DocumentURL=$val.URL
                                    CheckedOutByUser=$val.File.CheckedOutByUser.ToString()
                                }                           
                    $results += $details
                
                 

            }
            
        }
        else
        {
        
            Write-Host "List" $list.Title "- File" $val.Name "is associated with the content type" $val.ContentType.Name "and shall not be modified"
            Write-Output "List" $list.Title "- File" $val.Name "is associated with the content type" $val.ContentType.Name "and shall not be modified" |Out-Null
        }
    }
  

    if($countCheckedOutitems -eq 0)
    {
        $list.ContentTypes.Delete($oldCT.ID)
        $list.Update()
    }
    return $results
}

function IsValidList($list,$site)
{
    if(![string]::IsNullOrEmpty($site.RestrictedLists))
    {
        $restrictedLibraries = $site.RestrictedLists.Split(",")
        foreach($listName in $restrictedLibraries)
        {
            if($listName -eq $list.Title)
            {
                return $false
            }
        }
    }
    return $true
}

function HideColumnsFromCT()
{
    foreach ($site in $Global.Sites.Site) 
    {
         $Id=$site.Url
        Write-Host $Id -ForegroundColor Green 
        $oSite = Get-SPSite $Id
        
        foreach ($web in $oSite.AllWebs) 
        {
             HideColumns $web 
            $web.Dispose()
        }
        $oSite.Dispose()
    }
}
function HideColumns($web)
{

   
    for ($i=0; $i -lt $web.Lists.Count; $i++)
    {
        $list =$web.lists[$i] 
        $IsValid= IsValidList $list
  
        if($IsValid)
        {
             $FieldsToUpdate = @()
             foreach ($field in $list.Fields) 
             {
                 $IfHide= IfColumnExists $field
                 if($IfHide)
                 {
                    $FieldsToUpdate+=UpdateDefaultAndHideColumns $field
                 }
             }
             foreach($Field in $FieldsToUpdate)
             {
                $Field.Update()
             }
             
        }
    }
    
}

function IfColumnExists($listfield)
{
    foreach ($field in $Global.HiddenFields.Field) 
    {
        if($listfield.InternalName.Contains($field.Name) -or $listfield.Title.Contains($field.Name)) 
        {
             $listfield.DefaultValue = $field.Value
             $listfield.Update()
             return $true
        }
    }
    return $false
}

function UpdateDefaultAndHideColumns($field)
{
             
     $field.ShowInNewForm = $false
     $field.ShowInDisplayForm = $true
     $field.ShowInEditForm = $false
     #$field.Update() 
     return $field 

}

function ChangeNavigationUrls()
{
    [string] $location = Get-Location
    foreach ($site in $Global.Sites.Site) 
    {
        $Id=$site.Url
        $oSite = Get-SPSite $Id
        #$records = @()
        foreach ($web in $oSite.AllWebs) 
        {
           
            GetNavigationNodes $web
            $web.Dispose()
        }
        $oSite.Dispose()
              

    }
}

function GetNavigationNodes($web)
{
    $FindString=$Global.OldURL
    $ReplaceString=$Global.NewURL
    $navigationNodes = $web.Navigation.TopNavigationBar
    UpdateNodeURL($navigationNodes)

    $navigationNodes = $web.Navigation.QuickLaunch 
    UpdateNodeURL($navigationNodes)
}
function UpdateNodeURL($navigationNodes)
{
     if ($navigationNodes -ne $null)
    {
        foreach($navNode in $navigationNodes)
        {
            if($navNode.Url -match $FindString)
            {
                 $linkUrl = $navNode.Url
                Write-Host "Updating $linkUrl with new URL"
                $navNode.Url = $navNode.Url.Replace($FindString,$ReplaceString)
                $navNode.Update()
            }
             foreach($navNodeChild in $navNode.Children) 
            {
                if($navNodeChild.Url -match $FindString)
                {
                    $linkUrl = $navNodeChild.Url
                    Write-Host "Updating $linkUrl with new URL"
                    $navNodeChild.Url = $navNodeChild.Url.Replace($FindString,$ReplaceString)
                    $navNodeChild.Update()
                }
            }
        }
    }
}

function GetWorkflowsInfo()
{
$records=@()
    foreach ($site in $Global.Sites.Site) 
    {
        $Id=$site.Url
        $oSite = Get-SPSite $Id
        foreach ($web in $oSite.AllWebs) 
        {
             foreach ($list in $web.Lists)
             {
                 foreach ($wa in $list.WorkflowAssociations)
                 {
                     write-host "Site:" $wa.ParentWeb.Url ", List:" $wa.ParentList.Title ", Workflow:" $wa.Name
                      $details = New-Object -TypeName PSObject -Property @{            
                                    WebApplication   = $web.Site.WebApplication.Name              
                                    SiteCollection     = $web.Site.Url                 
                                    WebTitle      = $web.Title 
                                    WebURL   = $web.Url              
                                    ListTitle     = $wa.ParentList.Title                
                                    Workflow  = $wa.Name
                                    
                                }                           
                    $records += $details
                 }
             }
        }
       
    }
        [string] $location = Get-Location
        $filename  = $location + "\CSV\" + $site.Name + ".csv"
        $records| Where-Object {$_}|Select-Object WebApplication,SiteCollection,WebTitle,WebURL,ListTitle,Workflow | ConvertTo-Csv -notype >$filename
        Write-Host "CSV file created on " +  $filename -ForegroundColor Green 
}
function GetModifiedByColumnValues()
{
     
    
    $records = @()
    foreach ($site in $Global.Sites.Site) 
    {
            $records +=GetColumnVals $site
            $site.Dispose() 
            [string] $location = Get-Location
            $filename  = $location + "\CSV\" + $site.Name + ".csv"
            $records| Where-Object {$_}|Select-Object WebApplication,SiteCollection,WebTitle,WebURL,ListTitle,ListUrl,ItemId,ItemTitle,LastModifiedOn,LastModifiedBy,DocumentCreatedBy,DocumentCreatedOn,DocumentName,DocumentURL | ConvertTo-Csv -notype >$filename
            Write-Output "CSV file created on "  $filename 
    } 

    
    
}



function GetColumnVals($site)
{
  

    $oSite = Get-SPSite $site.Url    
    $rootWeb = $oSite.RootWeb
    $oldCt = $rootWeb.ContentTypes[$Global.OldContentType]
    $result = @()
   
  
     if($oldCt -ne $null)
    {
        $oldCt=[Microsoft.SharePoint.SPContentTypeUsage]::GetUsages($oldCt)
        foreach($contentType in [Microsoft.SharePoint.SPContentTypeUsage]::GetUsages($oldCt))
	    {
            if($contentType.IsUrlToList)
            {                                                        
                    Write-Host $contentType.Url -ForegroundColor Green
                    $IterateMore=$true
                    foreach ($web in $oSite.AllWebs)
                    {
                        foreach ($lst in $web.Lists)
                        {
                            
                           if ($lst.RootFolder.ServerRelativeUrl -eq $contentType.Url)
                           {
                              $list=$lst
                              $IterateMore=$false
                              break
                           }
                        }
                        #$web.Dispose()
                        
                        if (!$IterateMore)
                        {
                            break;
                        }
                    }
                    $IsValid= IsValidList $list      
                    if($IsValid)
                    {
                       
                         foreach($val in $list.Items)
                         {    
                            $details = New-Object -TypeName PSObject -Property @{            
                                            WebApplication   = $web.Site.WebApplication.Name              
                                            SiteCollection     = $web.Site.Url                 
                                            WebTitle      = $web.Title 
                                            WebURL   = $web.Url              
                                            ListTitle     = $list.Title                 
                                            ListUrl      = $list.RootFolder.Url
                                            ItemId   = $val.ID.ToString()              
                                            ItemTitle     = $val.Title                 
                                            LastModifiedOn      = $val["Modified"] 
                                            LastModifiedBy      = $val["Modified By"] 
                                            DocumentCreatedBy=$val["Created By"]
                                            DocumentCreatedOn=$val["Created"]
                                            DocumentName=$val.Name
                                            DocumentURL=$val.URL
                                          
                                        }                           
                            $results += $details
                        }
                   
                    }
                    $web.Dispose()
            }
        }

     $rootWeb.Dispose()
    }
   
        return $result
}


function UpdateImagePaths()
{
    foreach ($site in $Global.Sites.Site) 
    {
        $Id=$site.Url
        $oSite = Get-SPSite $Id
        foreach($web in $oSite.AllWebs)
        {
	        foreach($list in $web.Lists)
	        {
                $OldURL=$list.ImageUrl
                if($OldUrl -NotMatch "/15/")
                {
                    $NewURL= $OldURL.Substring(0, $OldURL.IndexOf('/') + 1 + $OldURL.Substring($OldURL.IndexOf('/') + 1).IndexOf('/'))
                    $NewURL= $NewURL+"/15"
                    $a=$OldURL.Substring($OldURL.IndexOf("/",$OldURL.IndexOf("/") + 1))
                    $NewURL= $NewURL+$a
                    if($NewURL -match "gif")
                    {
                        $NewURL = $NewURL.Replace("gif","png")
                    }
                    $NewURL=$NewURL+"?rev=23"
                    $list.ImageUrl = $NewURL
                    $list.Update()
		            Write-Host "Web URL: "$web.Url "List Title:" $list.Title "Old ImageUrl:" $OldURL "New ImageUrl:" $NewURL
                }
	        }
        }
    }
}



function SetDefaultValueForExistingItemsMMDColumns()
{
    foreach ($site in $Global.Sites.Site) 
    {
         $Id=$site.Url
        Write-Host $Id -ForegroundColor Green 
        $oSite = Get-SPSite $Id
        
        foreach ($web in $oSite.AllWebs) 
        {
             SetValues $web 
            $web.Dispose()
        }
        $oSite.Dispose()
    } 
}
function SetValues($web)
{
    $ctName="Viacom Document"
    for ($i=0; $i -lt $web.Lists.Count; $i++)
    {
        $list =$web.lists[$i] 
        $IsValid= IsValidList $list 
        if($IsValid)
        {
           foreach ($ctype in $list.ContentTypes)  
           {  
               
               if ($ctype.Name -eq $ctName)  
               {
                  foreach ($field in $Global.MMDColumns.Field)
                 {
                     foreach ($listfield in $list.Fields) 
                    {
                        if($listfield.InternalName.Contains($field.Name) -or $listfield.Title.Contains($field.Name)) 
                        {
                            SetTaxonomyFieldValue $list $field 
                            break
                        }
                    }
                 }
                 break
             }
           }
          
        }
    }
}
function SetTaxonomyFieldValue($l,$field)
{
    $managedmetadataField = $l.Fields[$field.Name] -as [Microsoft.SharePoint.Taxonomy.TaxonomyField];            
    $term=GetTerm $managedmetadataField       
               
       if($term -ne $null)
       {
            foreach($item in $list.Items)
            {
          
                $managedmetadataField.SetFieldValue($item,$term);            
                $item.SystemUpdate();
            }  
        }               
       
    
}
function GetTerm($managedmetadataField)
{
    $tsId = $managedmetadataField.TermSetId;            
    $termStoreId = $managedmetadataField.SspId;            
    $tsession = Get-SPTaxonomySession -Site $l.ParentWeb.Site;            
    $tstore =  $tsession.TermStores[$termStoreId];              
    $tset = $tstore.GetTermSet($tsId);                  
    $termName = $field.Value;            
    $terms = $tset.GetTerms($termName,$false);            
    $term = $null;  
   
    if($terms.Count -ne 0)            
    {            
               
        $term = $terms[0];
    }
    return $term
}
function UpdateMMDSiteColumns()
{
   
    foreach ($site in $Global.Sites.Site) 
    {
         $Id=$site.Url
        
        Write-Host $Id -ForegroundColor Green 
        $oSite = Get-SPSite $Id
        $rootWeb = $oSite.RootWeb
        foreach ($MMDfield in $Global.MMDColumns.Field) 
        {
            $field = [Microsoft.SharePoint.Taxonomy.TaxonomyField]$rootWeb.Fields.GetFieldByInternalName($MMDfield.InternalName)
             Write-Host $field.DefaultValue
            $session = New-Object Microsoft.SharePoint.Taxonomy.TaxonomySession($oSite)
            $mmsServiceName = $Global.MMDServiceName
            $grpName=$Global.MMDGroupName
            $termStore = $session.TermStores[$mmsServiceName]
            $group = $termStore.Groups[$grpName]
            $termSet = $group.TermSets[$MMDfield.Name]
            $terms = $termSet.GetTerms(100)
           
            $term = $terms | ?{$_.Name -eq $MMDfield.Value} 
            $a= "-1;#"+$term.Name+"|"+$term.Id


           $field.DefaultValue = $a
           $field.Update($true)

            
        }
        $rootWeb.Dispose()
       
        $oSite.Dispose()
    }
   
}

function AddColumnsToCT()
{
    foreach ($site in $Global.Sites.Site) 
    {
            AddColumns $site
            $site.Dispose() 
          
    } 
}

function AddColumns($site)
{
    $oSite = Get-SPSite $site.Url    
    $rootWeb = $oSite.RootWeb
    $listCT = $rootWeb.ContentTypes[$Global.NewContentType]
    foreach ($Column in $Global.SiteColumns.Column) 
    {
      $link = $rootWeb.Fields.GetFieldByInternalName($Column.InternalName)

       if($listCT -ne $null -and $link -ne $null)
       {
        
        foreach($contentType in [Microsoft.SharePoint.SPContentTypeUsage]::GetUsages($listCT))
	    {
            if($contentType.IsUrlToList)
            {                                                        
                    Write-Host $contentType.Url -ForegroundColor Green
                    $IterateMore=$true
                    foreach ($web in $oSite.AllWebs)
                    {
                        foreach ($lst in $web.Lists)
                        {
                            
                           if ($lst.RootFolder.ServerRelativeUrl -eq $contentType.Url)
                           {
                              $list=$lst
                              $IterateMore=$false
                              break
                           }
                        }
                        
                        if (!$IterateMore)
                        {
                            break;
                        }
                    }
                    $IsValid= IsValidList $list      
                    if($IsValid)
                    {
                        $ct=$list.ContentTypes[$Global.NewContentType]
                        foreach($l in $ct.FieldLinks)
                        {
                        Write-Host $l.Id
                        Write-Host $l.Name
                        }
                        if ($ct.FieldLinks[$Column.InternalName] -eq $null)
                        {

                            $ct.FieldLinks.Add($link)
                            $ct.Update($false)
                            $list.Update()
                        }
                   
                    }
            }
        }
    }
   
     
    }
    $rootWeb.Dispose()
}