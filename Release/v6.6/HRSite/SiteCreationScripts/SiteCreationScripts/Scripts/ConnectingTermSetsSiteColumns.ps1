﻿$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"
    Write-Output "Loading SharePoint Powershell Snapin"     
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

function CallConnectTerms([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..."  -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $($ConfigPath) file`n"
	if($Error.Count -eq 0)
	{
        try 
        {
            $error.clear()
            foreach($Site in $cfg.Sites.Site)
            {
                SetConnectTerms $Site
            }
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host $Error -ForegroundColor Red
        Write-Output $Error
	}

}

function SetConnectTerms($site)
{
    $siteObj =Get-SPSite  $site.Url
    $web = $siteObj.OpenWeb();
	$session = new-object Microsoft.SharePoint.Taxonomy.TaxonomySession($siteObj)
	$termStore = $Session.TermStores[$site.TermStore];
    foreach ($grpName in $site.TermGroup) 
	{ 
        if($grpName.Name -eq "M&E Ad Sales")
        {
            $grpName.Name="M＆E Ad Sales"
        }
	    $group=$termStore.Groups[$grpName.Name]
        if($group)
        {
            foreach($col in $grpName.Column)
            {
                $termToMap=$col.TermSet
                $termSet=""
                $term=""
                $associateToTerm=$false
                if($termToMap.Contains(";"))
                {
                     $multiLevelTaxonomy=$termToMap.Split(";")
                     $m=0;
                     foreach($tax in $multiLevelTaxonomy)
                     {
                          if($m -eq 0)
                          {
                              $termSet=$group.TermSets[$tax]
                              $term=$group.TermSets[$tax]
                              $associateToTerm=$true
                          }
                          else
                          {
                              $term=$term.Terms[$tax]
                          }
                          $m++;
                     }
                }
                else
                {
			        $termSet = $group.TermSets[$termToMap]
                }
                try
                {
                    $targetField = [Microsoft.SharePoint.Taxonomy.TaxonomyField]$web.Fields.GetFieldByInternalName($col.Name)
                }
                catch
                {
                    $Error.Clear()
                    Write-Host "$($col.Name) field does not exist $($termSet.Name)"   -ForegroundColor Yellow
                    Write-Output "$($col.Name) field does not exist"
                }
			    if($targetField -ne $null -and $termSet -ne $null)
			    {
				    $targetField.sspid = $termstore.id
				    $targetField.termsetid = $termSet.id
                    if($associateToTerm -eq $true)
                    {
                        $targetField.AnchorId=$term.Id
                    }
				    $targetField.Update($true)
                    Write-Host "$($col.Name) connecting with Term set! $($termSet.id) Namely $($termSet.Name)" -ForegroundColor Green
                    Write-Output "$($col.Name) connecting with Term set! $($termSet.id) Namely $($termSet.Name)"
			    }
            }
        }
    }
}	   

