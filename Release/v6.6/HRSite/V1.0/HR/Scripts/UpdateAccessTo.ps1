﻿$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

function UpdateAccessTo([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$lookUpXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green      

    $sitecollectionUrl =  $lookUpXml.Sites.SiteCollection

    $ColumnName = "LinkAccessTo"

    # Get Site Collection URL
    $siteCollection = Get-SPSite $sitecollectionUrl
    
    $rootWeb = $siteCollection.RootWeb 	

    $siteColumn = $rootWeb.Fields.GetFieldByInternalName($ColumnName)
    if($siteColumn -ne $null)
    {
        $siteColumn.AllowMultipleValues=$true       
        $siteColumn.Update($true)

        Write-Host "Site Column" $ColumnName "updated to Allow Multiple Values" -ForegroundColor Green

        $siteColumn = $null
    }
    
	$rootWeb.Dispose()				
}
