﻿$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

function ReOrderViewFields([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$lookUpXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green      

    
    $lookUpXml.Sites.Site |
	ForEach-Object {  

        $Subsite = $_.SiteUrl
        
        $web = Get-SPWeb -Identity $Subsite	

        $listName = $_.ListName
        $viewName = $_.View
        $viewFieldsOrder = $_.ViewFieldsOrder
        $viewFieldsColl = $viewFieldsOrder.Split(",");
           
        try
        {
             $list = $web.Lists[$listName]
             $view = $list.Views[$viewName]

             $view.ViewFields.DeleteAll()

             foreach ($viewField in $viewFieldsColl) 
	         {
                 $view.ViewFields.Add($viewField)
                 $view.Update()
             }

             Write-Host  $viewName "View Fields updated successfully in List " $listName  -ForegroundColor Green
        }
        catch
        {
             Write-Host $list "or" $viewName "not found" in $web -ForegroundColor Red
        }  
        $web.Dispose()	
    }       
				
}
