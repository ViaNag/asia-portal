﻿$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

function RemoveIsPromoted([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$lookUpXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green      

    $sitecollectionUrl =  $lookUpXml.Sites.SiteCollection

    $ColumnName = "IsPromoted"

    # Get Site Collection URL
    $siteCollection = Get-SPSite $sitecollectionUrl
    
    $rootWeb = $siteCollection.RootWeb 	
	
	#Get Links List
	$List = $rootWeb.Lists["Links"];
	
	# Get All Views
	$AllViews=$List.Views;

	# Delete Source column from all views
	for ($i = 0; $i -lt $AllViews.Count; $i++)
	{
		$View =$AllViews[$i];
		$View.ViewFields.Delete($ColumnName);
		$View.Update();

	}
	Write-Host "Column " $ColumnName "has been deleted from all listviews of Links list from site :" $siteWeb.Url -ForegroundColor Green
        
	$rootWeb.Dispose()				
}
