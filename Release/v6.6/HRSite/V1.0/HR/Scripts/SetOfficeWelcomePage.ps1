﻿if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin Microsoft.SharePoint.PowerShell
}

function SetWelcomePage()
{
$wsh = new-object -comobject wscript.shell

    $spWebUrl = Read-Host "Enter the K-onda SiteCollection url to set office site welcome page"

   

$SPWeb =Get-SPWeb -Identity ($spWebUrl + "/services/offices")
	#$pageUrl = Read-Host "Enter the Page name to set welcome page like 'Home.aspx'"
	$folder = $SPWeb.RootFolder
	$folder.WelcomePage = "Pages/" + "landing.aspx"
	$folder.update()
Write-Host "Success: Welcome Page for :" $SPWeb.Title  -ForegroundColor Green	
	$SPWeb.Dispose()

$SPWeb =Get-SPWeb -Identity ($spWebUrl + "/services/offices/china")
	#$pageUrl = Read-Host "Enter the Page name to set welcome page like 'Home.aspx'"
	$folder = $SPWeb.RootFolder
	$folder.WelcomePage = "Pages/" + "landing.aspx"
	$folder.update()
Write-Host "Success: Welcome Page for :" $SPWeb.Title  -ForegroundColor Green	
	$SPWeb.Dispose()

$SPWeb =Get-SPWeb -Identity ($spWebUrl + "/services/offices/singapore")
	#$pageUrl = Read-Host "Enter the Page name to set welcome page like 'Home.aspx'"
	$folder = $SPWeb.RootFolder
	$folder.WelcomePage = "Pages/" + "landing.aspx"
	$folder.update()	
	Write-Host "Success: Welcome Page for :" $SPWeb.Title  -ForegroundColor Green
	$SPWeb.Dispose()

$SPWeb =Get-SPWeb -Identity ($spWebUrl + "/services/offices/japan")
	#$pageUrl = Read-Host "Enter the Page name to set welcome page like 'Home.aspx'"
	$folder = $SPWeb.RootFolder
	$folder.WelcomePage = "Pages/" + "landing.aspx"
	$folder.update()
Write-Host "Success: Welcome Page for :" $SPWeb.Title  -ForegroundColor Green	
	$SPWeb.Dispose()
		
$folder.update()
$SPWeb.Dispose()

}
