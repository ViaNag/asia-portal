﻿$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

function AddViewItemsConfig([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$lookUpXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green      

 	
	# Add  items to Config list if not already present
	$lookUpXml.Sites.Site |
	ForEach-Object {
			# Get site url, title 
			$siteURL = $_.SiteURL
			$siteWeb = Get-SPWeb $siteURL        
			$listTitle = $_.ListTitle
			
			#Get Config List
			$configList = $siteWeb.Lists[$listTitle];
			
				# Add new item to list
				$_.Item |
				ForEach-Object {
				$title = $_.Title
				$key = $_.Key
				$value = $_.Value
				$configItems = $configList.Items | where {$_['Key'] -eq $key}
				if($configItems)
				{
					foreach($item in $configItems)
					{
					$item["Title"] = $title
					$item["Key"] = $key
					$item["Value1"] = $value
					 
					#Update the object so it gets saved to the list
					$item.Update()
					Write-Host "Item with Title : " $title " Key :" $key" and Value : "$value "already exists in the list. Update has been made to the item." -ForegroundColor Yellow
					}
				}
				else
				{
					#Create a new item
					$newItem = $configList.Items.Add()
					 
					#Add properties to this list item
					$newItem["Title"] = $title
					$newItem["Key"] = $key
					$newItem["Value1"] = $value
					 
					#Update the object so it gets saved to the list
					$newItem.Update()
				}
			}
			 $siteWeb.Dispose()
		}
		
        Write-Host "Items added to configuration list"-ForegroundColor Green
      	
}
