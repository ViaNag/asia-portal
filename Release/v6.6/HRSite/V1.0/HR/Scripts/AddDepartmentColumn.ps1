﻿$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}
Add-PSSnapin "Microsoft.SharePoint.Powershell" 
function AddDepartmentColumn([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$lookUpXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green      

    $sitecollectionUrl =  $lookUpXml.Sites.SiteUrl

    # Get Site Collection URL
    $siteCollection = Get-SPSite $sitecollectionUrl
    
    
    
    $rootWeb = $siteCollection.RootWeb 	
    
	Write-Host "In the Site Collection"  $rootWeb.url

    $ctName = "ContentCategoriesFeed"  

    $colName = "ContentPublisherDepartment"

    $ct=$rootWeb.ContentTypes[$ctName]
    if($ct)
    {
        $fld=$rootWeb.Fields.GetFieldByInternalName($colName)
        $fldLink=New-Object Microsoft.SharePoint.SPFieldLink($fld)
        $ct.FieldLinks.Add($fldLink)

        # Delete Department column from CT if present
        $fldDepartment = New-Object Microsoft.SharePoint.SPFieldLink ($rootWeb.Fields.GetFieldByInternalName("ViacomDepartment"))
        if($fldDepartment)
        {
            $ct.FieldLinks.Delete($fldDepartment.Id)
        }

        $ct.Update($true)

        Write-Host $colName "column added in ContentCategoriesFeed List"  -ForegroundColor Green
    }
    else
    {
        Write-Host $ctName "Content Type not found"  -ForegroundColor Red
    }
    
        
	$rootWeb.Dispose()				
}
