﻿Add-PSSnapin Microsoft.SharePoint.Powershell

function AddColumnInList([String]$configFileName = "")
{
    [string] $currentLocation = Get-Location

    Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green

    # Check that the config file exists.
    if (-not $(Test-Path -Path $configFileName -Type Leaf))
    {
	    Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
    }

    $configXml = [xml]$(get-content $configFileName)
    if( $? -eq $false ) 
    {
	    Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
    }

    # Get Site Collection URL
    $siteCollectionURL = $configXml.Sites.SiteCollection

    if ($configXml.Sites)
    {
	    foreach ($SubSite in $configXml.Sites.Site)
	    {
		    Write-Host "Adding columns in Externally shared Content List" -ForegroundColor Yellow

            Add-SiteColumn -SiteUrl $siteCollectionURL

            Write-Host "Successfully added column in Externally shared Content List" -ForegroundColor Green
        }
    }

}

function Add-SiteColumn
{
    
    Param (
           [parameter(Mandatory=$true)][string]$SiteUrl
           )

    $listName = "Externally shared Content"

         $fieldXMLSiteURL = '<Field Type="Text" Name="SiteURL"                   
                     DisplayName="SiteURL"  
                     StaticName="SiteURL"  
                     Group="Viacom Adspace Site Columns"  
                     Hidden="FALSE"  
                     Required="FALSE"  
                     Sealed="FALSE"  
                     ShowInDisplayForm="FALSE"  
                     ShowInEditForm="FALSE"    
                     ShowInNewForm="FALSE"
                     Overwrite="TRUE">
                     <Default>0</Default>
                     </Field>' 
    
    $fieldXMLListId = '<Field Type="Text" Name="ListsID"                   
                     DisplayName="ListsID"  
                     StaticName="ListsID"  
                     Group="Viacom Adspace Site Columns"    
                     Required="FALSE"  
                     Sealed="FALSE"
                     Hidden="FALSE"  
                     ShowInDisplayForm="FALSE"  
                     ShowInEditForm="FALSE"    
                     ShowInNewForm="FALSE"
                     Overwrite="TRUE">
                     <Default>0</Default>
                     </Field>'


 
    $siteCollection = Get-SPSite $SiteUrl 

    $rootWeb = $siteCollection.RootWeb

    $list = $rootWeb.Lists[$listName]

    if( $list.Fields.ContainsField("ListsID") -eq $true)
    {  
        $list.Fields.Delete("ListsID")
    }
        $list.Fields.AddFieldAsXml($fieldXMLListId,$false,[Microsoft.SharePoint.SPAddFieldOptions]::AddFieldToDefaultView)

           
           
    $list.Update()

    if( $list.Fields.ContainsField("SiteURL") -eq $true)
    {  
        $list.Fields.Delete("SiteURL")
    }
    $list.Fields.AddFieldAsXml($fieldXMLSiteURL,$false,[Microsoft.SharePoint.SPAddFieldOptions]::AddFieldToDefaultView)
    
    $list.Update()
}