﻿# =================================================================================
#
#Main Function
#
# =================================================================================
function CreateContentTypes([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)
    if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
	{    
		  Add-PsSnapin Microsoft.SharePoint.PowerShell
	}

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $($ConfigPath) file"
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
            foreach($ct in $cfg.ContentTypes.ContentType)
            {
                ContentTypeToAdd $ct
            }
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host $Error -ForegroundColor Red
        Write-Output $Error
	}
}

# =================================================================================
#
# FUNC: ContentTypeToAdd
# DESC: Create contenttype if not exist else associate fields to it.#
# =================================================================================
function ContentTypeToAdd([object] $ct)
{
    try
    {
        $error.Clear()
        $site = new-object Microsoft.SharePoint.SPSite $ct.SiteURL
        $web = $site.RootWeb
        # Check if the contenttype already exist or not
        $ctype=$web.AvailableContentTypes[$ct.ContentTypeName]
        if(!$ctype)
        {
            $ctypeId=$null
            if([string]::IsNullOrEmpty($ct.Guid))
            {
	            $ctypeId = $web.AvailableContentTypes[$ct.ParentContentType]
            }
            else
            {
                $ctypeId = new-object Microsoft.SharePoint.SPContentTypeId($ct.Guid)
            }
            $ctype = new-object Microsoft.SharePoint.SPContentType($ctypeId, $web.contenttypes, $ct.ContentTypeName)
            $web.contenttypes.add($ctype)
            $web.update()
            Write-Host "Content type $($ct.ContentTypeName) added successfully at web : " $ct.SiteUrl -ForegroundColor Green
            Write-Output "Content type $($ct.ContentTypeName) added successfully at web : " $ct.SiteUrl
        }
        else
        {
            Write-Host "Content type $($ct.ContentTypeName) already exist at web : "$ct.SiteUrl -ForegroundColor Yellow
            Write-Output "Content type $($ct.ContentTypeName) already exist at web : "$ct.SiteUrl
        }
        $existingCtype=$web.ContentTypes[$ct.ContentTypeName]
	    $existingCtype.Group = $ct.ContentTypeGroupName
        $existingCtype.Description = $ct.Description
		try
		{
			$field = $web.Fields.GetFieldByInternalName($ct.SiteColumn)
		}
	    catch
		{
			 $error.clear()
			 Write-Host "Field $($ct.SiteColumn) does not exist" -ForegroundColor Yellow
			 Write-Output "Field $($ct.SiteColumn) does not exist"
		}
		if($field)
		{
			$fieldLink=New-Object Microsoft.SharePoint.SPFieldLink $field
			$existingCtype.FieldLinks.Add($fieldLink)
			
			$existingCtype.Update($true)
			if(![string]::IsNullOrEmpty($ct.Hidden))
			{
				[boolean]$existingCtype.FieldLinks[$ct.SiteColumn].Hidden = [System.Convert]::ToBoolean($ct.Hidden)
			}
			if(![string]::IsNullOrEmpty($ct.Required))
			{
				[boolean]$existingCtype.FieldLinks[$ct.SiteColumn].Required = [System.Convert]::ToBoolean($ct.Required)
			}
			if(![string]::IsNullOrEmpty($ct.ReadOnly))
			{
				[boolean]$existingCtype.FieldLinks[$ct.SiteColumn].ReadOnly = [System.Convert]::ToBoolean($ct.ReadOnly)
			}	
			$existingCtype.Update($true)
			Write-Host "Field $($ct.SiteColumn) added to content type $($ct.ContentTypeName) successfully at web : " $ct.SiteUrl -ForegroundColor Green
			Write-Output "Field $($ct.SiteColumn) added to content type $($ct.ContentTypeName) successfully at web : " $ct.SiteUrl
			$web.Dispose()
		}
		else
        {
                     $existingCtype.Update($true)
			        Write-Host "Field $($ct.SiteColumn) added to content type $($ct.ContentTypeName) successfully at web : " $ct.SiteUrl -ForegroundColor Green
			        Write-Output "Field $($ct.SiteColumn) added to content type $($ct.ContentTypeName) successfully at web : " $ct.SiteUrl
			        $web.Dispose()
        }
	}
    catch
    {
        Write-Host "`nException for contentType :" $ct.ContentTypeName "`n" $Error -ForegroundColor Red
        Write-Output "`nException for contentType :" $ct.ContentTypeName "`n" $Error
    }
}