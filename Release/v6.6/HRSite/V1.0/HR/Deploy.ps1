﻿[string] $currentLocation = Get-Location
$opDefFileName = "Operations.xml"
$configurationFileName = "Configuration.xml"

# ==================================================================================
#
# FUNC:	Get-ScriptLogFileName
# DESC:	Gets the log file nme based on invocation script name
#		Suffice the script with execution date and returns filename string 
#		in format <<Log File Name>>_<<ddMMMyyyy>>.log
#
# =================================================================================
function Get-ScriptLogFileName([string]$loggingAction)
{
		$currentDate = Get-Date -Format "ddMMMyyyy"
		$logFileName = $loggingAction + $currentDate  + ".log"

		Return $logFileName
}

function script:ErrorExit()
{
    exit 1
}


#check the parameters
if($args.Count -ne 1)
{ 
    Write-Host "Usage: .\Deploy.ps1 <<Folder name of webapplication in configuration folder>>" -ForegroundColor Red
    ErrorExit
}



$opDefFileNamePath = $currentLocation + "\configuration\" + $args[0] + "\" + $opDefFileName
$configurationFileNameWithPath = $currentLocation + "\configuration\" + $args[0] + "\" + $configurationFileName

if (-not $(Test-Path -Path $opDefFileNamePath -Type Leaf))
{
	Write-Error "Operations definition file $opDefFileNamePath does not exist." 
	Exit
}

$Operations = [xml]$(get-content $opDefFileNamePath)

if( $? -eq $false ) 
{
	Write-Host "Could not read Operation file. Exiting ..." -ForegroundColor Red`
	Exit
}

if (-not $(Test-Path -Path $configurationFileNameWithPath -Type Leaf))
{
	Write-Error "Configuration definition file '" + $configurationFileNameWithPath + "' does not exist." 
}

$Configuration = [xml]$(get-content $configurationFileNameWithPath)

if( $? -eq $false ) 
{
	Write-Host "Could not read configuration definition file. Exiting ..." -ForegroundColor Red`
	Exit
}

$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'}
if ($snapin -eq $null) 
{
    Write-Host "Loading SharePoint Powershell Snapin" -ForegroundColor Green
    Add-PSSnapin "Microsoft.SharePoint.Powershell"
}

foreach ($Operation in $Operations.Operations.Operation)
{
	$OpName = $Operation.Name
	$Enabled = $Operation.Enabled
	$Method = $Operation.Method
	$script = ". .\scripts\" + $Operation.script
	$parameterfile = $currentLocation + "\configuration\" +  $args[0] + "\" + $Operation.parameterfile
	$executeasSeprateProcess = $Operation.executeasSeprateProcess
	$endScriptOnFail = $Operation.endScriptOnFail.ToString().ToLower()
	$Action = $Operation.LogFileName
	if ($Enabled.ToLower() -eq "true")
	{
		$logFileName = Get-ScriptLogFileName($Action)
		$logFileName = ".\Logs\" + $logFileName
		$Error.Clear()
		Start-SPAssignment -Global
		Start-Transcript -Path $logFileName

		$importScript = $script 
		Invoke-Expression $importScript 
		$command = $Method + " " + $parameterfile 
		Invoke-Expression -Command $command
		
		Stop-Transcript
		Stop-SPAssignment -Global
		
		if(($Error.Count -gt 0) -and ($endScriptOnFail -eq "true"))
		{
			Break
			Exit-PSSession
		}
	}
}

# =================================================================================
#
# FUNC:	Get-ScriptLogFileName
# DESC:	Gets the log file nme based on invocation script name
#		Suffice the script with execution date and returns filename string 
#		in format <<Log File Name>>_<<ddMMMyyyy>>.log
#
# =================================================================================
function Get-ScriptLogFileName
{
	if($myInvocation.ScriptName) 
	{ 
		[string]$scriptName = $myInvocation.ScriptName 
		$scriptName = $scriptName.Substring($scriptName.LastIndexOf('\')+1)
		$currentDate = Get-Date -Format "ddMMMyyyy"
		$logFileName = $scriptName.Replace(".ps1","_" + $currentDate  + ".log")

		Return $logFileName
	}
	else 
	{ 
		[string]$scriptName = $myInvocation.MyCommand.Definition 
		$scriptName = $scriptName.Substring($scriptName.LastIndexOf('\')+1)
		$currentDate = Get-Date -Format "ddMMMyyyy"
		$logFileName = $scriptName.Replace(".ps1","_" + $currentDate  + ".log")

		Return $logFileName
	}
}