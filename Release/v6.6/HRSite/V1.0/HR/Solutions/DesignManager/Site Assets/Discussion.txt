<div style="width: 100%; background-color: white;" class="discussion-container">
    <div class="ms-comm-heroLinkContainer">
        <a id="forum0-NewPostLink" href="/sites/Asia/getinvolved/Discussions/Lists/Community%20Discussion/NewForm.aspx"
            title="add new discussion" class="ms-textXLarge ms-heroCommandLink"><span>ADD NEW DISCUSSION</span>
			 </a>
        <div class="ms-clear">
        </div>
    </div>
    <div id="navcontainer">
        <ul>
            <li><a href="#" id="recent-discussion">Recent </a> <span class="greenroom-cat-separator">|</span> </li>
            <li><a href="#" id="currentuser-discussion">My Discussion </a> <span class="greenroom-cat-separator">|</span> </li>
        </ul>
    </div>
    <div id="Discussions" style="width: 600px;">
    </div>
</div>
<script type="text/javascript">
    var discussionHTML = null;
    var categories;
    var categoryDiv;
    var posts;
    var replies;
    var reply;
    var authorDetailsData = '<div class="authorsDetailsData"></div>';
    var categoryData = '<div class="categoryData"></div>';

    $(document).ready(function () {
        Discussion.BuildNavigation();
        
        $('#recent-discussion').click(function () {
            $("#Discussions").empty();
            $('#Discussions').append(discussionHTML);

        });


        $('#currentuser-discussion').click(function () {
            $("#Discussions").empty();

            $(discussionHTML).find('div#greenroom-discussion-username a').each(function () {

                currentItemAuthorID = $(this).attr('href');

                var index = currentItemAuthorID.indexOf("=");
                if (index != -1)
                    currentItemAuthorID = currentItemAuthorID.substring(index + 1, currentItemAuthorID.length);

                if (currentItemAuthorID == _spPageContextInfo.userId) {
                    var tempHtml = null;
                    tempHtml = $(this).parent().parent().html();
                    tempHtmlDivWrapper = '<div id="user-discussions" style="margin-left:-10px;margin-bottom: 35px;">' + tempHtml + '</div>';
                    $('#Discussions').append(tempHtmlDivWrapper);
                }

            });

            //            Discussion.GetDiscussions(_spPageContextInfo.siteAbsoluteUrl + "/getinvolved/Discussions/_api/lists/getbytitle('Discussions%20List')/items?$filter=Author/Id eq " + _spPageContextInfo.userId);
            //return false;
        });


        $('.category-discussion').click(function () {

            $("#Discussions").empty();

            var category = $(this).text();
            var index = category.indexOf("|");

            if (index != -1)
                category = category.substring(0, index - 1);

				category = $.trim(category);
            $(discussionHTML).find('div#greenroom-discussion-date a').each(function () {

                if ($(this).text() == category) {

                    var tempHtml = null;
                    tempHtml = $(this).parent().parent().html();
                    tempHtmlDivWrapper = '<div id="user-discussions" style="margin-left:-10px;margin-bottom: 35px;">' + tempHtml + '</div>';
                    $('#Discussions').append(tempHtmlDivWrapper);
                }

            });

            // Discussion.GetDiscussions(_spPageContextInfo.siteAbsoluteUrl + "/getinvolved/Discussions/_api/lists/getbytitle('Discussions%20List')/items?$filter=CategoriesLookup/Id eq " + category);
            // return false;
        });
    });

    var Discussion = {

        GetDiscussions: function (strUrl) {
            $.ajax({
                url: strUrl,
                async: false,
                type: "GET",
                dataType: "json",
                headers: {
                    Accept: "application/json;odata=verbose"
                },
                success: function (data, textStatus, jqXHR) {
                    try {

                        posts = '<div class="posts"></div>';
                        replies = '<div class="replies"></div>';
                        var size = data.d.results.length;
                        for (var i = 0; i < size; i++) {
                            
                            var currentItem = data.d.results[i];

                            //if - item is a reply
                            if (currentItem.ParentItemID != null) {


                                var reply = "<div class='reply'><div class='parentID'>" + currentItem.ParentItemID + "</div></div>";
                                var divLength = replies.length;
                                replies = replies.substring(0, divLength - 6);
                                replies = replies + reply + "</div>";

                            }
                            //else - item is a post
                            else {								
                                var currentItemId = currentItem.ID;
                                var title = currentItem.Title;
                                var body = currentItem.Body;
                                var authorId = currentItem.AuthorId;
                                var createdDate = currentItem.Created;
                                var categoryLookup = currentItem.CategoriesLookupId;
								var currentItemFileRef = currentItem.EncodedAbsUrl;
								
                                var post = '<div class="post"><div class="postId">' + currentItemId + '</div>\
                                <div class="postTitle">' + title + '</div>\
                                <div class="postBody">' + body + '</div>\
                                <div class="postAuthorId">' + authorId + '</div>\
                                <div class="postCreatedDate">' + createdDate + '</div>\
                                <div class="postcategoryLookup">' + categoryLookup + '</div>\
								<div class="postcurrentItemFileRef">' + currentItemFileRef + '</div>\
                                </div>';

                                var divLength = posts.length;
                                posts = posts.substring(0, divLength - 6);
                                posts = posts + post + "</div>";

                            }
                        } //end for loop

                        Discussion.GetPostAuthorLoginName(posts, replies, categoryData);

                    } catch (e) {
                        //console.log(e);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //console.log("Error getting Post Details" + jqXHR.responseText);
                }
            });
        },
        GetPostAuthorLoginName: function (posts, replies, categoryData) {

            var currentItemAuthorLoginName;
            var authorsData = '<div class="authorsData"></div>';
            var flag = false;
            var postAuthorID;
            var currentPostID;
            var currentItemCategoryTitle;
            var replyCount = 0;
			var postCount=1;
            $(posts).find('div.post').each(function () {

                currentItemAuthorLoginName = null;

                //find category for each post
                var currentPostHTML = "<div class='post'>" + $(this).html() + "</div>";
                var currentPostCategoryLookup = $(currentPostHTML).find("div.postcategoryLookup").text();
                
                $(categoryData).find('div.category').each(function () {

                    var categoryID = $(this).find("div.categoryID").text();
                    if (categoryID == currentPostCategoryLookup)
                    { currentItemCategoryTitle = $(this).find("div.categoryTitle").text(); ; return false; }

                });

                //calculate replyCount for each post
                replyCount = 0;
                currentPostID = $(this).children(".postId").text();
                $(replies).find('div.reply').each(function () {

                    var parentID = $(this).find("div.parentID").text();
                    if (parentID == currentPostID)
                    { replyCount++; }

                });

                //find Login Name for each post in local storage : authorsData 
                postAuthorID = $(this).children(".postAuthorId").text();

                if ($(authorsData).text() != "") {

                    flag = false;
                    $(authorsData).find('div.postAuthor').each(function () {
                        if ($(this).children(".authorID").text() == postAuthorID) {

                            currentItemAuthorLoginName = $(this).children(".postAuthorLoginName").text();
                            flag = true;
                            return false;
                        }
                    });

                }

                //find Login Name for each post using ajax call if it is not found in local storage : authorsData
                if (flag == false) {
                    $.ajax({
                        url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/GetUserById(" + postAuthorID + ")/LoginName",
                        async: false,
                        type: "GET",
                        dataType: "json",
                        headers: {
                            Accept: "application/json;odata=verbose"
                        },
                        success: function (data, textStatus, jqXHR) {
                            try {

                                currentItemAuthorLoginName = data.d.LoginName.split('|')[1];
                                var authorDiv = '<div class="postAuthor"><div class="authorID">' + postAuthorID + '</div><div class="postAuthorLoginName">' + currentItemAuthorLoginName + '</div></div>';

                                var divLength = authorsData.length;
                                authorsData = authorsData.substring(0, divLength - 6);
                                authorsData = authorsData + authorDiv + "</div>";

                            }
                            catch (e) {
                                //console.log(e);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            //console.log("Error getting User Login Name : " + jqXHR.responseText);
                        }
                    });
                } //end if (flag == false)

                Discussion.GetAuthorProperties(currentPostHTML, currentItemAuthorLoginName, currentItemCategoryTitle, replyCount);
				//Limit post count on UI
				if(postCount++ > 9){
					return false;
				}

            });
        },
        GetAuthorProperties: function (currentPostHTML, currentItemAuthorLoginName, currentItemCategoryTitle, replyCount) {

            var pictureUrl = "";
            var displayName = "";
            var flag = false;

            //find User Profile Properties for each post in local storage : authorDetailsData 
            if ($(authorDetailsData).text() != "") {

                flag = false;
                $(authorDetailsData).find('div.authorDetailDiv').each(function () {

                    if ($(this).children(".authorLoginName").text() == currentItemAuthorLoginName) {

                        pictureUrl = $(this).children(".pictureUrl").text();
                        displayName = $(this).children(".displayName").text();						
                        flag = true;
                        return false;
                    }
                });

            }

            //find User Profile Properties for each post using ajax call if it is not found in local storage : authorDetailsData
            if (flag == false) {
                $.ajax({
                    url: _spPageContextInfo.siteAbsoluteUrl + "/_api/SP.UserProfiles.PeopleManager/GetPropertiesFor(accountName=@v)?@v='" + currentItemAuthorLoginName + "'",
                    async: false,
                    type: "GET",
                    dataType: "json",
                    headers: {
                        Accept: "application/json;odata=verbose"
                    },
                    success: function (data, textStatus, jqXHR) {
                        try {

                            if (data.d.PictureUrl == null) {
                                pictureUrl = '_layouts/15/images/Person.gif';
                            } else {
                                pictureUrl = data.d.PictureUrl;
                            }
                            displayName = data.d.DisplayName;
							//Added check to swap first name last name
							if(displayName.indexOf(",") > -1)
							{
								displayName = displayName.split(",")[1]+" "+ displayName.split(",")[0];
							}

                            var authorDetailDiv = '<div class="authorDetailDiv"><div class="authorLoginName">' + currentItemAuthorLoginName + '</div><div class="pictureUrl">' + pictureUrl + '</div><div class="displayName">' + displayName + '</div></div>';

                            var divLength = authorDetailsData.length;
                            authorDetailsData = authorDetailsData.substring(0, divLength - 6);
                            authorDetailsData = authorDetailsData + authorDetailDiv + "</div>";

                        } catch (e) {
                            //console.log(e);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //console.log("Error getting User Profile Properties : " + jqXHR.responseText);
                    }
                });
            }
            Discussion.BuildHtml(currentPostHTML, currentItemCategoryTitle, displayName, pictureUrl, replyCount);
        },
        BuildHtml: function (currentPostHTML, currentItemCategoryTitle, displayName, pictureUrl, replyCount) {

            //extract data from currentPostHTML
            var title = $(currentPostHTML).find("div.postTitle").text();
            var body = $(currentPostHTML).find("div.postBody").text();
            var authorId = $(currentPostHTML).find("div.postAuthorId").text();
            var createdDate = $(currentPostHTML).find("div.postCreatedDate").text();
            var categoryLookup = $(currentPostHTML).find("div.postcategoryLookup").text();
			var currentItemFileRef= $(currentPostHTML).find("div.postcurrentItemFileRef").text();
            var category = currentItemCategoryTitle;
            var repliesCount = replyCount;

            //getting createdDate attributes to show in required format
            var itemDate = new Date(createdDate);
            var hours = itemDate.getHours();
            var minutes = (itemDate.getMinutes() < 10 ? '0' : '') + itemDate.getMinutes();
            var date = itemDate.getDate();
            var month = itemDate.getMonth();
            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            var strHtml;
            //Build HTML for each post
            if ($.trim(body) == "null" || $.trim(body) == "")
                strHtml = '<div id="user-discussions" style="margin-left:-10px;margin-bottom: 35px;"><div id="greenroom-discussion-userimage" style="float: left;"><a href="' + _spPageContextInfo.siteAbsoluteUrl + '/_layouts/15/userdisp.aspx?ID=' + authorId + '"><img class="circular" src="' + pictureUrl + '" style="width: 80px;height: 80px;"/></a></div>' +           
            '<div id="greenroom-discussion-comments-usercomment" style="padding-top: 40px;height: 150px;"><div class="greenroom-discussion-comments-subject" "><a href="'+currentItemFileRef+'"><span style="font-weight: bold!important">' + title + '</span></a></div><a href="' + currentItemFileRef + '" style="color:#000000;">' + title + '</a></div>' +
            '<div id="greenroom-discussion-date" style="margin-left: 115px;">' + monthNames[month] + ' ' + date + ' at ' + hours + '.' + minutes + ' in <a href="' + _spPageContextInfo.siteAbsoluteUrl + '/getinvolved/Discussions/SitePages/Category.aspx?CategoryID=' + categoryLookup + '&SiteMapTitle=' + category + '">' + category + '</a></div>' +
            '<div id="cswp-tile-comments-lg"><a class="homepage-discussion-reply" href="' + currentItemFileRef+ '"><span id="cswp-tile-comments-text-lg">Replies</span><span id="cswp-tile-comments-num-lg">' + repliesCount + '</span></a></div>' +
            '</div>';
            else
                strHtml = '<div id="user-discussions" style="margin-left:-10px;margin-bottom: 35px;"><div id="greenroom-discussion-userimage" style="float: left;"><a href="' + _spPageContextInfo.siteAbsoluteUrl + '/_layouts/15/userdisp.aspx?ID=' + authorId + '"><img class="circular" src="' + pictureUrl + '" style="width: 80px;height: 80px;"/></a></div>' +
                 '<div id="greenroom-discussion-comments-usercomment" style="padding-top:15px;height: 150px;"><div class="greenroom-discussion-comments-subject"  ><a href="'+currentItemFileRef+'"><span style="font-weight: bold!important">' + title + '</span></a></div> <a href="' + currentItemFileRef + '" style="color:#000000;">' + body + '</a></div>' +
             '<div id="greenroom-discussion-username" style="float: left;padding-top:10px;">By<a href="' + _spPageContextInfo.siteAbsoluteUrl + '/_layouts/15/userdisp.aspx?ID=' + authorId + '"> ' + displayName + '</a></div>' +
            '<div id="greenroom-discussion-date" style="margin-left: 115px;">' + monthNames[month] + ' ' + date + ' at ' + hours + '.' + minutes + ' in <a href="' + _spPageContextInfo.siteAbsoluteUrl + '/getinvolved/Discussions/SitePages/Category.aspx?CategoryID=' + categoryLookup + '&SiteMapTitle=' + category + '">' + category + '</a></div>' +
            '<div id="cswp-tile-comments-lg"><a class="homepage-discussion-reply" href="' + currentItemFileRef + '"><span id="cswp-tile-comments-text-lg">Replies</span><span id="cswp-tile-comments-num-lg">' + repliesCount + '</span></a></div>' +
            '</div>';

            $('#Discussions').append(strHtml);

        },
        BuildNavigation: function () {

            $.ajax({

                url: _spPageContextInfo.siteAbsoluteUrl + "/getinvolved/Discussions/_api/lists/getbytitle('Categories')/items",
                async: false,
                type: "GET",
                dataType: "json",
                headers: {
                    Accept: "application/json;odata=verbose"
                },
                success: function (data, textStatus, jqXHR) {
                    try {

                        var categoriesCount = data.d.results.length;
                        for (var i = 0; i < categoriesCount; i++) {

                            var currentCategoryItem = data.d.results[i];
                            var categoryID = currentCategoryItem.ID;
                            var currentItemCategoryTitle = currentCategoryItem.Title;

                            var strHtml = '<li><a href="#" class="category-discussion" data-category="' + categoryID + '">' + currentItemCategoryTitle + ' </a> <span class="greenroom-cat-separator">|</span> </li>';
                            $("#navcontainer ul").append(strHtml);

                            var categoryDiv = '<div class="category"><div class="categoryID">' + categoryID + '</div><div class="categoryTitle">' + currentItemCategoryTitle + '</div></div>';
                            var divLength = categoryData.length;
                            categoryData = categoryData.substring(0, divLength - 6);
                            categoryData = categoryData + categoryDiv + "</div>";
                        }
                        Discussion.GetDiscussions(_spPageContextInfo.siteAbsoluteUrl + "/getinvolved/Discussions/_api/lists/getbytitle('Discussions%20List')/items?$select=ParentItemID,ID,Title,Body,AuthorId,Created,CategoriesLookupId,FileRef,EncodedAbsUrl&$orderby=Created desc");
                        discussionHTML = $("#Discussions").html();

                    } catch (e) {
                        //console.log(e);
                    }
                },

                error: function (jqXHR, textStatus, errorThrown) {
                    //console.log("Error getting CAtegory Detaits : " + jqXHR.responseText);
                }
            });

        }

    }</script>
