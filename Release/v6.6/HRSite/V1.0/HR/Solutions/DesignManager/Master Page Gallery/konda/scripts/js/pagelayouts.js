﻿/*----------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------Get Query String Parameter Value-------------------------------------*/
function GetQueryStringValues(URLParam) {
    /* This function gets querystring parameter values */
    $.extend({
        getParamValue: function(paramName) {
            parName = paramName.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
            var pattern = '[\\?&]' + paramName + '=([^&#]*)';
            var regex = new RegExp(pattern);
            var matches = regex.exec(window.location.href);
            if (matches == null) return '';
            else return decodeURIComponent(matches[1].replace(/\+/g, ' '));
        }
    });
    var paramvalue = $.getParamValue(URLParam);
    return paramvalue;
}

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/
var PageLayout,currentweb,newcurrentweb;
$(document).ready(function(){
	SP.SOD.executeFunc('sp.js', 'SP.ClientContext', TimeStamp);
	SP.SOD.executeFunc('SP.Core.js', 'Reputation.js');
});
/*----------------------------------------------------------------------------------------------------------------*/

function TimeStamp() {
    var currentcontext = null;
    currentcontext = new SP.ClientContext.get_current();
    currentweb = currentcontext.get_web();
    currentcontext.load(currentweb);
    currentcontext.executeQueryAsync(Function.createDelegate(this, this.TimeStampOnSuccess), Function.createDelegate(this, this.TimeStampOnFailed));
}

/*----------------------------------------------------------------------------------------------------------------*/
var parentString;

function TimeStampOnSuccess() {
	$('#greenroom-subsite-title').append(currentweb.get_title());
    var subsiteurl = currentweb.get_serverRelativeUrl();

    var urlParts = subsiteurl.split('/')[1];
    parentString = '/' + urlParts;

    var newcurrentcontext = null;
    //newcurrentcontext = new SP.ClientContext(parentString);
    newcurrentcontext = new SP.ClientContext(subsiteurl);	// Change Request for displaying current site Title instead 1st level site Title.
    
    newcurrentweb = newcurrentcontext.get_web();
    newcurrentcontext.load(newcurrentweb);
    newcurrentcontext.executeQueryAsync(Function.createDelegate(this, this.ParentWebOnSuccess), Function.createDelegate(this, this.ParentWebOnFailure));

    var PageURL = window.location.href;
    PageURL = PageURL.split('.aspx')[0];
    var PageName = PageURL.substring((PageURL.lastIndexOf('/')) + 1);

    if (subsiteurl == '/') {
        subsiteurl = '';
    }

    getPageTimeStampInfo(PageName, subsiteurl);
    //RecentlyCommented(subsiteurl);
    DisplayFollow();
    DisplayCSWPViewAll();

    GetLikeCount();
    $("a.MyLikeButton").click(function() {
        LikePage();
    });
}

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

function LikePage() {

    var like = false;
    //var likeButtonText = $("a.MyLikeButton").text();	Working Like functionality with Text as 'Like'/'Unlike'...
    var likeButtonText = $("a.MyLikeButton div").text();
    if (likeButtonText != "") {
        if (likeButtonText == "Like")
            like = true;

        var aContextObject = new SP.ClientContext();
        EnsureScriptFunc('reputation.js', 'Microsoft.Office.Server.ReputationModel.Reputation', function() {
            Microsoft.Office.Server.ReputationModel.
            Reputation.setLike(aContextObject,
                _spPageContextInfo.pageListId.substring(1, 37),
                _spPageContextInfo.pageItemId, like);

            aContextObject.executeQueryAsync(
                function() {
                    //alert(String(like));
                    GetLikeCount();
                }, function(sender, args) {
                    //alert('F0');
                });
        });
    }
}

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

function GetLikeCount() {

    var context = new SP.ClientContext(_spPageContextInfo.webServerRelativeUrl);
    var list = context.get_web().get_lists().getById(_spPageContextInfo.pageListId);
    var item = list.getItemById(_spPageContextInfo.pageItemId);

    context.load(item, "LikedBy", "ID", "LikesCount");
    context.executeQueryAsync(Function.createDelegate(this, function(success) {
        // Check if the user id of the current users is in the collection LikedBy. 
        var likeDisplay = true;
        var $v_0 = item.get_item('LikedBy');
        var itemc = item.get_item('LikesCount');
        if (!SP.ScriptHelpers.isNullOrUndefined($v_0)) {
            for (var $v_1 = 0, $v_2 = $v_0.length; $v_1 < $v_2; $v_1++) {
                var $v_3 = $v_0[$v_1];
                if ($v_3.$1E_1 === _spPageContextInfo.userId) {
                    //cb(true, item.get_item('LikesCount'));
                    //alert("Liked by me");
                    likeDisplay = false;
                }
            }
        }
        ChangeLikeText(likeDisplay, itemc);

    }), Function.createDelegate(this, function(sender, args) {
        //alert('F1');
    }));
}

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

function ChangeLikeText(like, count) {

    /*-------------------Working Code for LIKE functionality with TEXT----------------

        if (like) {
            $("a.MyLikeButton").text('Like');
        }
        else {
            $("a.MyLikeButton").text('Unlike');
        }
        var htmlstring = String(count) + ' ' +'<img alt="" src="/SiteCollectionImages/pagelayouts/greenroom-count-likes.png" />';
        if (count > 0)
            $(".mylikecount").html(htmlstring)
        else
            $(".mylikecount").html("");

    --------------------------------------------------------------------------------*/
	var htmlstring = '';
    if (like) {
        $("a.MyLikeButton").html('<div title="Like this article" id="cswp-like-text">Like</div>');
		 htmlstring = String(count) + ' ' + '<img alt="" src="/SiteCollectionImages/pagelayouts/greenroom-count-likes.png" />';		
		 $("#greenroom-page-content-count-likes").removeClass("page-content-count-likes-activestate");
    } else {
        $("a.MyLikeButton").html('<div title="Like this article" id="cswp-like-text">Unlike</div>');
		 htmlstring = String(count) + ' ' + '<img alt="" src="/SiteCollectionImages/pagelayouts/greenroom-count-likes-active.png" />';
		 $("#greenroom-page-content-count-likes").addClass("page-content-count-likes-activestate");
    }
   
    if (count > 0)
        $(".mylikecount").html(htmlstring)
    else
        $(".mylikecount").html(String(0) + ' ' + '<img Title="Likes" src="/SiteCollectionImages/pagelayouts/greenroom-count-likes.png" />');

}

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

function TimeStampOnFailed() {
   // console.log('Time stamp failed');
}

/*----------------------------------------------------------------------------------------------------------------*/

function ParentWebOnSuccess() {
    var parentsitetitle = newcurrentweb.get_title();
    var parentsiteURL = newcurrentweb.get_url();
    //$('#greenroom-subsite-title').append(parentsitetitle);
    //GetCommentCount(parentsiteURL);
	if (parentString != '/') {
           GetCommentCount(parentString);	// Change Request as per displaying current site Title instead 1st level site Title.
	 }
}

/*----------------------------------------------------------------------------------------------------------------*/

function ParentWebOnFailure() {
 //   console.log('Please try again...!!!');
}

/*------------------------ TO get the COMMENTS COUNT for current Article Page ------------------------------------*/

function GetCommentCount(subsiteurl) {
	var relativeUrl = _spPageContextInfo.serverRequestPath;
//	var relativeUrlParts = window.location.href.split(_spPageContextInfo.siteAbsoluteUrl);
//	if(relativeUrlParts.length > 1)
//	{
//		relativeUrl = relativeUrlParts[1];
//	}
    var call = $.ajax({
        url: _spPageContextInfo.siteAbsoluteUrl + "/getinvolved" + "/_api/lists/getbytitle('Comments')/items?" +
            "$select=Title" +
            "&$filter=(Title eq '" + relativeUrl + "') and (ReportAbuse eq '0')",
        type: "GET",
        dataType: "json",
        headers: {
            Accept: "application/json;odata=verbose"
        }
    });

    call.done(function(data, textStatus, jqXHR) {
        try {
            $('.comments').append(data.d.results.length);
        } catch (e) {}
    });

    call.fail(function(jqXHR, textStatus, errorThrown) {
        //alert("Error generating Report: " + jqXHR.responseText);
    });
}

/*-----------------------------------function to get Page Info (Last Created)------------------------------------*/

function getPageTimeStampInfo(PageName, subsiteurl) {
    var call = $.ajax({
        url: subsiteurl + "/_api/lists/getbytitle('Pages')/items?" +
            "$select=FileRef,Created,Modified,PublishingPageLayout" +
            //"&$expand=CreatedBy"+
            "&$filter=FileRef eq '" + subsiteurl + "/Pages/" + PageName + ".aspx'", //+
        //"&$top=20",
        type: "GET",
        dataType: "json",
        headers: {
            Accept: "application/json;odata=verbose"
        }
    });

    call.done(function(data, textStatus, jqXHR) {
        try {
            var CreatedDate = data.d.results[0].Created;
            var formatCreatedDate = new Date(CreatedDate);

            PageLayout = data.d.results[0].PublishingPageLayout.Description;
            //GetRelatedArticles(subsiteurl,PageLayout);

            var today = new Date();

            var lastCreated = DateDiff(today, formatCreatedDate);
             var lastCreated = DateDiff(today, formatCreatedDate);

            $('#TimeStampCreated').append('Article added ' + lastCreated + ' days ago. Please <a href="mailto:DL_Development-AMERICAS@viacom.com?subject=K-Onda Feedback">send us your feedback</a> if any of the content below goes against <a href="/Pages/Community-Guidelines.aspx" title="K-Onda community guidelines" >K-Onda community guidelines</a>.');


            var ModifiedDate = data.d.results[0].Modified;
            var formatModifiedDate = new Date(ModifiedDate);
            var lastModified = DateDiff(today, formatModifiedDate);
            $('#TimeStampModified').append('Page last updated ' + lastModified + ' days ago. Please <a href="mailto:DL_Development-AMERICAS@viacom.com?subject=K-Onda Feedback">send us your feedback</a> if you have any comments or if you believe the below content is out of date.');



        } catch (e) {
            //alert(e);
        }
    });

    call.fail(function(jqXHR, textStatus, errorThrown) {
        //alert("Error generating Report: " + jqXHR.responseText);
    });
}

/*----------------------------------------------------------------------------------------------------------------*/
/*-----------------------------------function to calculate date difference----------------------------------------*/

function DateDiff(date1, date2) {
    date1.setHours(0);
    date1.setMinutes(0, 0, 0);
    date2.setHours(0);
    date2.setMinutes(0, 0, 0);
    var datediff = Math.abs(date1.getTime() - date2.getTime()); // difference 
    return parseInt(datediff / (24 * 60 * 60 * 1000), 10); //Convert values days and return value      
}

/*--------------------------------------THIS FUNCTION IS NOT USED ANYMORE-----------------------------------------*/
/*------------------------------------------Most Recently Commented-----------------------------------------------*/

function RecentlyCommented(subsiteurl) {
    var call = $.ajax({
        url: subsiteurl + "/_api/lists/getbytitle('MicroFeed')/items?" +
            "$select=Content,ReplyCount,LikesCount,Created,Title,PeopleCount,PostAuthor" +
            //"&$expand=CreatedBy"+
            //"&$filter=FileRef eq '/Pages/"+PageName+".aspx'",
            "&$orderby=PeopleCount desc,Created desc" +
            "&$top=1",
        type: "GET",
        dataType: "json",
        headers: {
            Accept: "application/json;odata=verbose"
        }
    });

    call.done(function(data, textStatus, jqXHR) {
        try {
            var Content = data.d.results[0].Content;
            var formatContent = '<div style="font-size:10pt">' + Content + '</div>';

            var CommentedUser = data.d.results[0].Title;
            var formatCommentedUser = '<div style="font-size:10pt;font-weight:bold">' + CommentedUser + '</div>';

            var CreatedDate = data.d.results[0].Created;
            CreatedDate = CreatedDate.split('Z')[0].replace('T', ' at ');

            var NumberofLikes = data.d.results[0].LikesCount;

            var formatCreatedDate = '<br /><div style="font-size:8pt;text-decoration:italics">' + CreatedDate + '&nbsp;&nbsp; | &nbsp;&nbsp; ' + NumberofLikes + '&nbsp;<img style="background-color:#4EB19F" src="/SiteCollectionImages/pagelayouts/greenroom-article-comments-likes.png" /></div>';

            var PostAuthor = data.d.results[0].PostAuthor;

            if (Content == null) {
                //$('.greenroom-sidebar-top-comment').append($("#greenroom-post-comment"));
                $('.greenroom-sidebar-top-comment').append('There are no comments to be displayed. To add, click on "Add A Comment"');
            } else {
                $('.greenroom-sidebar-top-comment').append('<div><div style="float:left" id="ProfilePicture"></div><div style="float:left;padding-left:4%;">' + formatCommentedUser + formatContent + formatCreatedDate + '</div></div></div>');
                GetProfileDetails(PostAuthor);
            }
        } catch (e) {
            //alert(e);
        }
    });

    call.fail(function(jqXHR, textStatus, errorThrown) {
        //alert("Error generating Report: " + jqXHR.responseText);
    });
}

/*--------------------------------------THIS FUNCTION IS NOT USED ANYMORE-----------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

function GetProfileDetails(PostAuthor) {
    var call = $.ajax({
        url: "/_api/SP.UserProfiles.PeopleManager/GetPropertiesFor(accountName=@v)?@v='" + PostAuthor + "'" +
            "&$select=PictureUrl" +
            //"&$expand=CreatedBy"+
            "&$filter=DirectReports", //+
        //"&$top=20",
        type: "GET",
        dataType: "json",
        headers: {
            Accept: "application/json;odata=verbose"
        }
    });

    call.done(function(data, textStatus, jqXHR) {
        try {
            var ProfilePicture = data.d.PictureUrl;

            if (ProfilePicture == null) {
                $('#ProfilePicture').append('<img class="circular" src="/_layouts/15/images/PersonPlaceholder.42x42x32.png?rev=23" />');
            } else {
                $('#ProfilePicture').append('<img class="circular" src="' + ProfilePicture + '" />');
            }
        } catch (e) {
            //alert(e);
        }
    });

    call.fail(function(jqXHR, textStatus, errorThrown) {
        //alert("Error getting Profile Details" + jqXHR.responseText);
    });
}

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/
function DisplayFollow() {
    var follow = $("#RibbonContainer-TabRowRight").find('a[id*="follow_button"]');

    $('#greenroom-subsite-title-follow').append(follow);
    $('#site_follow_button').html('<img src="/SiteCollectionImages/pagelayouts/greenroom-subsite-title-follow.png"/>');

}

/*----------------------------------------------------------------------------------------------------------------*/
/*-----------------------------------------Create Outlook Calendar Event------------------------------------------*/

function CreateOutlookEvent() {
    var StartDate = $('#EventStartDate div').text();
    var formatStartDate = new Date(StartDate);
    formatStartDate.setHours(formatStartDate.getHours() + 4);
    var msgData1 = formatStartDate.format('yyyyMMddTHHmmssZ');

    var EndDate = $('#EventEndDate div').text();
    var formatEndDate = new Date(EndDate);
    formatEndDate.setHours(formatEndDate.getHours() + 4);
    var msgData2 = formatEndDate.format('yyyyMMddTHHmmssZ');

    var msgData3 = $('#EventAddress div').text();

    /*msgData1 = '20140804T134500Z';
       msgData2 = '20140804T140000Z';
       msgData3 = 'California';    */

    var icsMSG = "BEGIN:VCALENDAR\nVERSION:2.0\nPRODID:-//Our Company//NONSGML v1.0//EN\nBEGIN:VEVENT\nUID:me@google.com\nDTSTAMP:20120315T170000Z\nATTENDEE;CN=My Self ;RSVP=TRUE:MAILTO:me@gmail.com\nORGANIZER;CN=Me:MAILTO::me@gmail.com\nDTSTART:" + msgData1 + "\nDTEND:" + msgData2 + "\nLOCATION:" + msgData3 + "\nSUMMARY:Our Meeting Office\nEND:VEVENT\nEND:VCALENDAR";

    window.open("data:text/calendar;charset=utf8," + escape(icsMSG));
}

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------------------------------------------*/
/*------------------------------------Add view all link for CSWP headers------------------------------------------*/
function DisplayCSWPViewAll() {
        $('#landing-page h2.ms-webpart-titleText a').append($('<span class="landing-page-view-all">View All &gt;&gt;</span>'));
		/* Set Company blog view all section to visit blog as per Defect ID 208 */
		var viacomBlogSpan= $( "span[Title]:contains('Viacom Blog')" );
		if(viacomBlogSpan != null && viacomBlogSpan != undefined && viacomBlogSpan.length >0)
		{
			viacomBlogSpanView=viacomBlogSpan.find('h2.ms-webpart-titleText a  .landing-page-view-all');
			if(viacomBlogSpanView != null && viacomBlogSpanView != undefined && viacomBlogSpanView.length >0)
			{
				viacomBlogSpanView.text("Visit Blog");
			}
		}
    }
/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------------------------------------------*/
/*------------------------CODE TO SHARE ARTICLE VIA NEWSFEED AND EMAIL ------------------------*/
    function OpenNewsFeed() {
        //replace the <newsfeedpageUrl> with page containing the newsfeed page URL
        var articleURL = window.location.href;
        var options = {
            //url: _spPageContextInfo.siteAbsoluteUrl+"/Pages/newsfeed.aspx?Isdlg=1",
            url: _spPageContextInfo.siteAbsoluteUrl+"/newsfeed/Pages/newsfeed.aspx?ArticleUrl="+articleURL+"&Isdlg=1",
            args: {
                sourceUrl: window.location.href
            }
        };
        ArticlePagePublishingImage('Timeline',_spPageContextInfo.webAbsoluteUrl,_spPageContextInfo.serverRequestPath);
        SP.SOD.execute("sp.ui.dialog.js", "SP.UI.ModalDialog.showModalDialog", options);

    }
	function  ArticlePagePublishingImage(SharedVia,webAbsoluteUrl,serverRequestPath){
		var fileLeafRef = serverRequestPath.toString().toLowerCase().split('pages/')[1];
		var clientContextPageImage = new SP.ClientContext(webAbsoluteUrl);
		oListPageImage = clientContextPageImage .get_web().get_lists().getByTitle('Pages');	
		var camlQueryPageImage = new SP.CamlQuery();
		camlQueryPageImage.set_viewXml("<View Scope='RecursiveAll'><Query><Where><Eq><FieldRef Name='FileLeafRef' /><Value Type='Text'>"+fileLeafRef+"</Value></Eq></Where></Query><ViewFields><FieldRef Name='FileLeafRef' /><FieldRef Name='PublishingPageImage' /><FieldRef Name='ID' /></ViewFields></View>"); 
		this.collListItemPageImage = oListPageImage.getItems(camlQueryPageImage);
		clientContextPageImage.load(collListItemPageImage);
		clientContextPageImage.executeQueryAsync(Function.createDelegate(this, function(success) {
		 var listItemEnumeratorPageImage = collListItemPageImage.getEnumerator();
			while (listItemEnumeratorPageImage.moveNext())
			{
				var oListItemPageImage = listItemEnumeratorPageImage.get_current();	
				var pageImage =  oListItemPageImage.get_item('PublishingPageImage');	
				if(pageImage != null)
					{
					pageImage = pageImage.split('src')[1].split('"')[1].split('"')[0];
					}
			 InsertIntoMostSharedWithPageImages(SharedVia,pageImage);
			}
		}), Function.createDelegate(this, function(sender, args) {
		   // console.log(args.get_message());
		}));	
	}
	function InsertIntoMostSharedWithPageImages(SharedVia,pageImage) {
		var relativeUrl = _spPageContextInfo.serverRequestPath;
	//	var relativeUrlParts = window.location.href.split(_spPageContextInfo.siteAbsoluteUrl);
	//	if(relativeUrlParts.length > 1)
	//	{
	//		relativeUrl = relativeUrlParts[1];
	//	}
        var currentcontext = new SP.ClientContext.get_current();
        var rootWeb = currentcontext.get_site().get_rootWeb();
        var currentweb = currentcontext.get_web();
        currentcontext.load(currentweb);
        var currentuser = currentweb.get_currentUser();
        currentuser.retrieve();
        var oList = rootWeb.get_lists().getByTitle('Most Shared');
		var mostSharedCamlQueryXml = "<View><Query><ViewFields><FieldRef Name='Shares' /></ViewFields><Where><Eq><FieldRef Name=\'Url\'/><Value Type=\'Text\'>" + relativeURL + "</Value></Eq></Where><OrderBy><FieldRef Name=\'Shares\' Ascending=\'False\'/></OrderBy></Query><RowLimit>1</RowLimit></View>";
		var mostSharedCamlQuery = new SP.CamlQuery();
		mostSharedCamlQuery.set_viewXml(mostSharedCamlQueryXml);
		this.mostSharedColListItem = oList.getItems(mostSharedCamlQuery);
        currentcontext.load(oList);
		currentcontext.load(mostSharedColListItem);
        currentcontext.load(currentuser);
        currentcontext.executeQueryAsync( //submit query to the server
            function() {
			var sharesCount = 0;
				var msListItemEnumerator = mostSharedColListItem.getEnumerator();
				while (msListItemEnumerator.moveNext()) {
					var msListItem = msListItemEnumerator.get_current();
					sharesCount = msListItem.get_item("Shares");
				}
            	var articleName= $('.page-header').text().trim();
            	var userId = currentUser.get_id();
				 var thumbnailImage = "";
				if(pageImage != null)
				{
					thumbnailImage=pageImage;
				}
				else{
					if($('#greenroom-article-content').find('img').length > 0){ 
					thumbnailImage=$('#greenroom-article-content').find('img').attr('src');
					}	 
				}                     
                var siteName = _spPageContextInfo.webServerRelativeUrl.split('/')[1];
                var itemCreateInfo = new SP.ListItemCreationInformation();
                oListItem = oList.addItem(itemCreateInfo);
                oListItem.set_item('SharedBy', userId);
                oListItem.set_item('SharedSiteTitle', siteName);
                oListItem.set_item('SharedBy', userId);
                oListItem.set_item('Sharedvia', SharedVia);
                oListItem.set_item('Url', relativeUrl);
                oListItem.set_item('Title', articleName);
                oListItem.set_item('ThumbnailImage',thumbnailImage );
				oListItem.set_item('Shares',sharesCount+1);
                oListItem.update();
               // currentcontext.executeQueryAsync(InsertIntoMostSharedWithPageImages_Success,InsertIntoMostSharedWithPageImages_Error);
				currentcontext.executeQueryAsync(function() {
      
             console.log("Success");
        // do whatever else you need to do here
    }, function(sender, args) {
         console.log(args.get_message()); //handle error in error callback
    });
            },
            function(sender, args) {
              //  console.log(args.get_message()); //handle error in error callback
            }
        );
    }
	
    function InsertIntoMostShared(SharedVia) {
		var relativeUrl = _spPageContextInfo.serverRequestPath;
		//var relativeUrlParts = window.location.href.split(_spPageContextInfo.siteAbsoluteUrl);
		//if(relativeUrlParts.length > 1)
		//{
		//	relativeUrl = relativeUrlParts[1];
		//}
        var currentcontext = new SP.ClientContext.get_current();
        var rootWeb = currentcontext.get_site().get_rootWeb();
        var currentweb = currentcontext.get_web();
        currentcontext.load(currentweb);
        var currentuser = currentweb.get_currentUser();
        currentuser.retrieve();
        var oList = rootWeb.get_lists().getByTitle('Most Shared');
		var mostSharedCamlQueryXml = "<View><Query><ViewFields><FieldRef Name='Shares' /></ViewFields><Where><Eq><FieldRef Name=\'Url\'/><Value Type=\'Text\'>" + relativeURL + "</Value></Eq></Where><OrderBy><FieldRef Name=\'Shares\' Ascending=\'False\'/></OrderBy></Query><RowLimit>1</RowLimit></View>";
		var mostSharedCamlQuery = new SP.CamlQuery();
		mostSharedCamlQuery.set_viewXml(mostSharedCamlQueryXml);
		this.mostSharedColListItem = oList.getItems(mostSharedCamlQuery);
        currentcontext.load(oList);
		currentcontext.load(mostSharedColListItem);
        currentcontext.load(currentuser);
        currentcontext.executeQueryAsync( //submit query to the server
            function() {
			var sharesCount = 0;
				var msListItemEnumerator = mostSharedColListItem.getEnumerator();
				while (msListItemEnumerator.moveNext()) {
					var msListItem = msListItemEnumerator.get_current();
					sharesCount = msListItem.get_item("Shares");
				}
            	var articleName= $('.page-header').text().trim();
            	var userId = currentUser.get_id();
                var thumbnailImage = "/SiteCollectionImages/pagelayouts/default-article-thumbnail.png";
				if($('#greenroom-article-content').find('img').length > 0){ 
				thumbnailImage=$('#greenroom-article-content').find('img').attr('src');
				}	                
                var siteName = _spPageContextInfo.webServerRelativeUrl.split('/')[1];
                var itemCreateInfo = new SP.ListItemCreationInformation();
                oListItem = oList.addItem(itemCreateInfo);
                oListItem.set_item('SharedBy', userId);
                oListItem.set_item('SharedSiteTitle', siteName);
                oListItem.set_item('SharedBy', userId);
                oListItem.set_item('Sharedvia', SharedVia);
                oListItem.set_item('Url', relativeUrl);
                oListItem.set_item('Title', articleName);
                oListItem.set_item('ThumbnailImage',thumbnailImage );
				oListItem.set_item('Shares',sharesCount+1);
                oListItem.update();
                currentcontext.executeQueryAsync();
            },
            function(sender, args) {
              //  console.log(args.get_message()); //handle error in error callback
            }
        );


    }
   
   function OnEmailClick(){
	//InsertIntoMostShared('Email');
	ArticlePagePublishingImage('Email',_spPageContextInfo.webAbsoluteUrl,_spPageContextInfo.serverRequestPath);
    //$(this).href
    window.location = 'mailto:?subject=An Asia Portal article has been shared with you&body=I thought you might find this Asia portal article interesting: ' + window.location;

}

//show Admin block to specific group users
var userGroups;
$(document).ready(function () {
    ExecuteOrDelayUntilScriptLoaded(CheckCurrentUserMembership, 'sp.js');
});
function OnResult(isMemmber) {
    if (isMemmber) {
        // $('nobr:contains("Promote Request")').parent().parent().parent().css("display", "none");
        $('#greenroom-admin-block-blue').css("display", "block");
    }
}

function CheckCurrentUserMembership() {

    var clientContext = new SP.ClientContext.get_current();
    var currentUser = clientContext.get_web().get_currentUser();
    clientContext.load(currentUser);

    userGroups = currentUser.get_groups();
    clientContext.load(userGroups);
    clientContext.executeQueryAsync(OnQuerySucceeded, OnQueryFailed);
}

function OnQuerySucceeded() {
    var isMember = false;
    var groupsEnumerator = userGroups.getEnumerator();
    while (groupsEnumerator.moveNext()) {
        var group = groupsEnumerator.get_current();
        if ((group.get_title() == "konda Owners") || (group.get_title() == "konda Members")) {
            isMember = true;
            break;
        }
    }

    OnResult(isMember);
}

function OnQueryFailed() {
    OnResult(false);
}


/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

