﻿# =================================================================================
#
#Main Function to apply security
#
# =================================================================================
function ApplySecurity([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..."  -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file`n"
	if($Error.Count -eq 0)
	{
        try 
        {
            $error.clear()
            foreach($security in $cfg.Securities.Security)
            {
                SecurityToApply $security
            }
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host $Error -ForegroundColor "Red"
        Write-Output $Error
	}
}

# =================================================================================
#
# FUNC: SecurityToApply
# DESC: Apply security like break inheritance/create groups based on security scope
# =================================================================================
function SecurityToApply([object] $security)
{
    try
    {
        $error.Clear()
        $web = Get-SPWeb $security.SiteURL
        $securityScope=$security.SecurityScope
        if($securityScope -eq "Web")
        {
            if(!$web.HasUniqueRoleAssignments)
            {
                $web.BreakRoleInheritance($false,$false)
            }
            AssociateGroupAndUser $security $web $null
			Write-Host "Security successfully applied at web : $($security.SiteURL) for scope web with Group $($security.Group) " -ForegroundColor Green
			Write-Output "Security successfully applied at web : $($security.SiteURL) for scope web with Group $($security.Group)"
        }
        elseif(($securityScope -eq "List") -and (![string]::IsNullOrEmpty($security.ListName))) 
        {
            $list=$web.Lists[$security.ListName]
            if($list)
            {
                if(![string]::IsNullOrEmpty($security.FolderUrl))
                {
                    AddSecurityGroupToFolder $security $web $list 
					Write-Host "Security successfully applied at web : $($security.SiteURL) for scope list with list folder url $($security.FolderUrl) with Group $($security.Group)  " -ForegroundColor Green
        			Write-Output "Security successfully applied at web : $($security.SiteURL) for scope list with list folder url $($security.FolderUrl ) with Group $($security.Group)"
                }
                else
                {
                    if(!$list.HasUniqueRoleAssignments)
                    {
                        $list.BreakRoleInheritance($false,$false)
                    }
                    AssociateGroupAndUser $security $web $list
					Write-Host "Security successfully applied at web : $($security.SiteURL) for scope list with list title: $($security.ListName) with Group $($security.Group) " -ForegroundColor Green
        			Write-Output "Security successfully applied at web : $($security.SiteURL) for scope list with list title: $($security.ListName) with Group $($security.Group)"
                }
            }
            else
            {
                Write-Host "List $($security.ListName) does not exist at web : $($security.SiteURL)" $Error -ForegroundColor Yellow
                Write-Output "List $($security.ListName) does not exist at web : $($security.SiteURL)" $Error
            }
        }       

        $web.Dispose()
 	}
    catch
    {
        Write-Host "`nException to apply security:$($security.SecurityScope) at url :$($security.SiteURL)`n" $Error -ForegroundColor Red
        Write-Output "`nException to apply security:$($security.SecurityScope) at url :$($security.SiteURL)`n" $Error
    }
}


# =================================================================================
#
# FUNC: AssociateGroup
# DESC: Associate group to web/list
# =================================================================================
function AssociateGroupAndUser([object] $security,$web,$list)
{
    try
    {
        $error.Clear()
        if(![string]::IsNullOrEmpty($security.Group))
        {
            if(![string]::IsNullOrEmpty($security.Permission))
            {
                $groupToAdd=$web.SiteGroups[$security.Group]
                $roleAssignment = new-object Microsoft.SharePoint.SPRoleAssignment($groupToAdd)  
	            $roleDefinition = $web.Site.RootWeb.RoleDefinitions[$security.Permission]  
	            $roleAssignment.RoleDefinitionBindings.Add($roleDefinition)
                if($list)
                {
                    $list.RoleAssignments.Add($roleAssignment)
                    $list.Update()
                }
                else
                {
                    $web.RoleAssignments.Add($roleAssignment)
                }
                $web.Update()
            }
            else
            {
                Write-Host "Permission level is null or empty for url $($security.SiteURL)" -ForegroundColor Yellow
                Write-Output "Permission level is null or empty for url $($security.SiteURL)"
            }
        }
        $web.Update()
 	}
    catch
    {
        Write-Host "`nException to apply security:" $security.SecurityScope "at url :"  $security.SiteURL "`n" $Error -ForegroundColor Red
        Write-Output "`nException to apply security:" $security.SecurityScope "at url :"  $security.SiteURL "`n" $Error
    }
}

# =================================================================================
#
# FUNC: AssociateGroup
# DESC: Associate group to web/list
# =================================================================================
function AddSecurityGroupToFolder([object] $security,$web,$list)
{
    try
    {
        $error.Clear()
        if(![string]::IsNullOrEmpty($security.Group))
        {
            if(![string]::IsNullOrEmpty($security.Permission))
            {
                $PermissionLevel = $security.Permission
                $group = $web.SiteGroups[$security.Group]
                $folder = $web.GetFolder($security.SiteURL+$security.FolderUrl)
                $folderValues = $security.FolderUrl
                if ($folder.Exists)
                {                    
                    $item = $folder.Item   
                    $item.BreakRoleInheritance($false)
                    $roleAssignment = New-Object microsoft.sharepoint.SPRoleAssignment($group);
                    $roleDefinition = $web.RoleDefinitions[$PermissionLevel];
                    $roleAssignment.RoleDefinitionBindings.Add($roleDefinition);   
                    $item.RoleAssignments.Add($roleAssignment);       
                    $folder.Update();   
                }
                else
                {   
                    $mainFolderName = $security.FolderUrl.LastIndexOf("/")
                    $folderName = $security.FolderUrl.Substring($mainFolderName + 1)                 
                    $newFolder = $list.AddItem("", [Microsoft.SharePoint.SPFileSystemObjectType]::Folder, $folderName)
                    $newFolder.Update()
                    Write-Host "`n$($security.FolderUrl) folder created" -ForegroundColor Green
                    Write-Output "$($security.FolderUrl) folder created"
                    $item = $folder.Item   
                    $item.BreakRoleInheritance($false)
                    $roleAssignment = New-Object microsoft.sharepoint.SPRoleAssignment($group);
                    $roleDefinition = $web.RoleDefinitions[$PermissionLevel];
                    $roleAssignment.RoleDefinitionBindings.Add($roleDefinition);   
                    $item.RoleAssignments.Add($roleAssignment);       
                    $folder.Update(); 
                }
            }
        }
    }
    catch
    {
        Write-Host "`nException to apply security:" $security.SecurityScope "at url :"  $security.SiteURL "`n" $Error -ForegroundColor Red
        Write-Output "`nException to apply security:" $security.SecurityScope "at url :"  $security.SiteURL "`n" $Error
    }
}



# =================================================================================
#
# FUNC: CreateGroups
# DESC: Create Group at site collection level
# =================================================================================
function CreateGroups([string]$ConfigPath = "")
{
    try
    {
        $error.Clear()
        $cfg = [xml](get-content $ConfigPath)

	    # Exit if config file is invalid
	    if( $? -eq $false ) 
        {
		    Write-Host "Could not read config file. Exiting ..."  -ForegroundColor Red
            Write-Output "Could not read config file. Exiting ..."
	    }
	
	    Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
        Write-Output "Sucessfully read config file $ConfigPath file`n"
	    if($Error.Count -eq 0)
	    {
            try 
            {
                $Groups=$cfg.Groups
                $siteUrl=$Groups.Url
                if(![string]::IsNullOrEmpty($siteUrl))
                {
                    $siteCol=Get-SPSite $siteUrl
                    $web=$siteCol.RootWeb
                    foreach($grp in $Groups.Group)
                    {
                        CreateGroupAndAssociateUser $grp $web
                    }
                    $web.Dispose()
                }
            }
            catch
            {
                Write-Host "`nException :" $Error -ForegroundColor Red
                Write-Output "`nException :" $Error
            }
	    }
	    else
	    {
		    Write-Host $Error -ForegroundColor "Red"
            Write-Output $Error
	    }
    }
    catch
    {
        Write-Host "`nException to create group `n" $Error -ForegroundColor Red
        Write-Output "`nException to create group`n" $Error
    }
}

# =================================================================================
#
# FUNC: CreateGroupAndAssociateUser
# DESC: Create Group if not exist and associate user to that group
# =================================================================================
function CreateGroupAndAssociateUser([object] $grp,$web)
{
    try
    {
        $error.Clear()
        if(![string]::IsNullOrEmpty($grp.GroupName))
        {
            $SPGroup=$web.SiteGroups[$grp.GroupName]
			$grpOwner=$web.Site.Owner
			if(![string]::IsNullOrEmpty($grp.GroupOwner))
			{
                try
                {
                    $updateGrpOwner=$web.SiteGroups[$grp.GroupOwner]
					if(!$updateGrpOwner)
					{
						$updateGrpOwner=$web.EnsureUser($grp.GroupOwner)
					}
                }
                catch
                {
                    $error.Clear()
                    Write-Host "Group Owner $($grp.GroupOwner) does not exits for site url : $($web.Url)"  -ForegroundColor Yellow           
					Write-Output "Group Owner $($grp.GroupOwner) does not exits for site url : $($web.Url)"
                }
			}
			
            if(!$SPGroup)
            {
                $web.SiteGroups.Add($grp.GroupName, $grpOwner, $grpOwner, $grp.GroupDescription)
                Write-Host "Group $($grp.GroupName) created successfully at url : $($web.Url)"  -ForegroundColor Green           
                Write-Output "Group $($grp.GroupName) created successfully at url : $($web.Url)" 
            }
            else
            {
                Write-Host "Group $($grp.GroupName) Already exists at url :  $($web.Url)" -ForegroundColor Green
                Write-Output "Group $($grp.GroupName) Already exists at url : $($web.Url)"
            }
            
            $group = $web.SiteGroups[$grp.GroupName]  
            if($group)
            {
				#Group Property
                if($updateGrpOwner)
                {
				    $group.Owner = $updateGrpOwner
                }
				if(![string]::IsNullOrEmpty($grp.OnlyAllowMembersViewMembership))
				{
					[boolean]$group.OnlyAllowMembersViewMembership=[System.Convert]::ToBoolean($grp.OnlyAllowMembersViewMembership)
				}
				
				if(![string]::IsNullOrEmpty($grp.AllowMembersEditMembership))
				{
					[boolean]$group.AllowMembersEditMembership=[System.Convert]::ToBoolean($grp.AllowMembersEditMembership)
				}
				$group.update() 
                Write-Host "Group $($grp.GroupName) property updated successfully at url : " $web.Url -ForegroundColor Green
                Write-Output "Group $($grp.GroupName) property updated successfully at url : " $web.Url

                #User adding this group
                if(![string]::IsNullOrEmpty($grp.UserName))
                {
                   
					try
					{
						$userToAdd = $web.EnsureUser($grp.UserName)
                        $group.AddUser($userToAdd)
                        Write-Host "User $($grp.UserName) added successfully to group $($grp.GroupName) at url :$($web.Url) "  -ForegroundColor Green
                        Write-Output "User $($grp.UserName) added successfully to group $($grp.GroupName) at url : $($web.Url)"
					}
					catch
					{
						$error.Clear()
                        Write-Host "User $($grp.UserName) does not exist at url $($web.Url)" -ForegroundColor Yellow
                        Write-Output "User $($grp.UserName) does not exist at url $($web.Url)`n"
					}
                }
               
            }
            $web.Update()
        }
 	}
    catch
    {
        Write-Host "`nException to create group at url :"  $web.Url "`n" $Error -ForegroundColor Red
        Write-Output "`nException to create group at url :"  $web.Url "`n" $Error
    }
}
