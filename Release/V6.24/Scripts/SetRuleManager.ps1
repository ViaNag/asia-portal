﻿function SetRulemanagers([string]$ConfigPath = "")
# =================================================================================
#
#Main Function to Set Rule Manager of site and Number of days to wait before sending an e-mail for Content organizer for multiple site
#
# =================================================================================
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $($ConfigPath) file"
    
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
            foreach($site in $cfg.Sites.Site)
            {
                SetRulemanager $site
            }
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host $Error -ForegroundColor Red
        Write-Output $Error
	}
}

# =================================================================================
#
# FUNC: SetRulemanager
# DESC: Function to Set Rule Manager of site and Number of days to wait before sending an e-mail for Content organizer for site.
# =================================================================================
function SetRulemanager([object] $site)
{
    try
    {
        $Error.Clear()
        $web= Get-SPWeb $site.Url
        $ruleManager=$web.Properties["_routerManagerEmail"];
        [int]$noOfDays=[int]$site.NoOfDays
		$web.Properties["_routerStaleContentThreshold"]= $noOfDays

        if(![string]::IsNullOrEmpty($site.RuleManager) -AND !($ruleManager.toLower() -like '*'+$site.RuleManager.toLower()+'*') )
		{            
			if(![string]::IsNullOrEmpty($ruleManager))
			{				
				$web.Properties["_routerManagerEmail"]=$ruleManager + "," + $site.RuleManager
			}
			else
			{
				$web.Properties["_routerManagerEmail"]= $site.RuleManager
			}
		}
        $web.Properties.Update();
		$web.Update();       

        Write-Host "Rule managers update successfully at web : " $site.Url -ForegroundColor Green
		Write-Output "Rule managers update successfully at web :" $site.Url 
	}
    catch
    {
        Write-Host "`nException for SetRulemanager at web : $($site.Url) `n" $Error -ForegroundColor Red
        Write-Output "`nException for SetRulemanager at web : $($site.Url) `n" $Error
    }	
}
