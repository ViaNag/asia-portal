# =================================================================================
#
#Main Function
#
# =================================================================================
function AddAllContentTypes([string]$ConfigPath = "")
{
	$libList = Import-Csv $ConfigPath

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file"
	#load assembly in case library will be a CS Record Library
	[Reflection.Assembly]::Load("GimmalSoft.ComplianceSuite.WorkingWithRecords, Version=1.0.0.0, Culture=neutral, PublicKeyToken=d56cc47ac40ed024") 
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
			
				foreach($libItem in $libList)
				{
					$msg = "Checking library " + $libItem.LibName + " in site " + $web.Title + "....."
					Write-Host $msg -ForegroundColor Green 
					Write-Output $msg
					$web = Get-SPWeb $libItem.SiteURL -ErrorVariable err -ErrorAction SilentlyContinue -AssignmentCollection $assignmentCollection

					if ($err)
					{
					   #do something with error like Write-Error/Write-Warning or maybe just create a new web with New-SPWeb
					   $msg = "Site URL " + $libItem.SiteURL + " is invalid"
					   Write-Host $msg -ForegroundColor Green 
					   Write-Output $msg
					}
					else
					{
						$listCollection = $web.Lists
						$spLibrary = $listCollection.TrygetList($libItem.LibName)
						if($spLibrary -ne $null)
						{
							$msg = "Library " + $libItem.LibName + " exists in site " + $web.Title
							Write-Host $msg -ForegroundColor Green 
							Write-Output $msg
							#Calling function to add content types
							InstallDefaultRecordSettings $libItem.SiteURL $spLibrary 
						}     
						else
						{
						   $msg = "Library " + $libItem.LibName + " with URL " + $libItem.URL + " not exists in site " + $web.Title
						   Write-Host $msg -ForegroundColor Green 
						   Write-Output $msg

						}
				   
					}
					$web.Dispose()
				}
			
            Write-Host "End : Addition of content type:- `n" -ForegroundColor Green
            Write-Output "End : Addition of content type:- `n"
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host $Error -ForegroundColor Red
        Write-Output $Error
	}
}


# =================================================================================
#
# FUNC: InstallDefaultRecordSettings
# DESC: Add all content type to list/library
#
# =================================================================================

function InstallDefaultRecordSettings($SiteURL,$spList)
{    
   

    #Add Default Record Content Types To List or Library
    try
    {
        $success = [GimmalSoft.ComplianceSuite.WorkingWithRecords.Containers]::AddDefaultRecordContentTypesToList($SiteURL, $spList.ID)
        #$success = $containers.AddDefaultRecordContentTypesToList($webUrl, $spList.ID)
        if($success -eq $true)
        {
            Write-Host "Add default record content types to list: SUCCESS"
			Write-Output "Add default record content types to list: SUCCESS"
        }
        else
        {
            Write-Host "Add default record content types to list: FAILED."
            Write-Host "Check the ULS logs for details."
			Write-Output "Add default record content types to list: FAILED."
            Write-Output "Check the ULS logs for details."
        }
    }
    catch [Exception]
    {
        Write-Host "Add default record content types to list: FAILED"
		 Write-Output "Add default record content types to list: FAILED"
        $msg = $_.Exception.Message + " - " + $_.Exception.Stacktrace
        Write-Host $msg
		Write-Output $msg
    }
}



