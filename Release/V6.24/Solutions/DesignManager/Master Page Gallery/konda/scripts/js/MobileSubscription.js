var check = false;
$(document).ready(function () {
	$("nobr:contains('Please justify request for claims')").append('<span class="ms-accentText" title="This is a required field."> *</span>');
	var oldSubmitClickText = $(".divButtonControls input[value='Save']").attr('onclick');
	var colonIndex = oldSubmitClickText.indexOf(':');
	var submitClickTextSub = oldSubmitClickText.substring(colonIndex + 1)
	var newSubmitClickText = "checkPhoneFormat();if(check){" + submitClickTextSub + ";}else{return false;}";
	$(".divButtonControls input[value='Save']").attr('onclick', newSubmitClickText); 
	});
	
	function checkPhoneFormat()
	{
		$("#customError").hide();
		check = true;
		var phone = $("input[title='Phone Number Required Field']");
		var phoneNumber = phone.val();
		if(phoneNumber != "")
		{ 
			var first = phoneNumber.substring(0,1);
			if(phoneNumber.length != 8){
			WriteErrorMessage(phone, "Enter a valid mobile number");
			check = false;
			}
			
			else if(first != "9" && first != "8"){
				
				WriteErrorMessage(phone, "Enter a valid mobile number");
				check = false;
			}
		}
		
			var ua = window.navigator.userAgent;
			var msie = ua.indexOf("MSIE ");

			if (msie > 0) // If Internet Explorer, return version number
			{
				//alert(parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
				var justifControl = $('iframe[id$="TextField_iframe"]');
				var justification = $('iframe[id$="TextField_iframe"]').contents().find("body").find("div").html();
				if(justification == "")
				{
					WriteErrorMessage(justifControl, "You must specify a value for this required field.");
					check = false;
				}
				
			}
			else  // If another browser, return 0
			{
				//alert('otherbrowser');
				var justifControl = $('textarea[title="Please justify request for claims"]');
				var justification = $('textarea[title="Please justify request for claims"]').val();
				if(justification == "")
				{
					WriteErrorMessage(justifControl, "You must specify a value for this required field.");
					check = false;
				}
			}

			return false;

		
	}
	
	function WriteErrorMessage(inputElement, message){

	   var errorMessageMarkup = '<br/><span class="errorMessage ms-formvalidation ms-csrformvalidation" id="customError"><span role="alert">' + message + '<br></span></span>';
	   $(inputElement).parent().append(errorMessageMarkup);

}