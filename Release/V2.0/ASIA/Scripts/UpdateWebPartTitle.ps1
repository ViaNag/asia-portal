if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
{    
      Add-PSSnapin Microsoft.SharePoint.PowerShell
}

function DeleteWebParts()
{
$spFile.CheckOut()
#Initialise the Web part manager for the specified profile page.
$spWebPartManager = $spWeb.GetLimitedWebPartManager($spFile.URL, [System.Web.UI.WebControls.WebParts.PersonalizationScope]::Shared)


#Remove the Web part from that page
foreach ($webpart in ($spWebPartManager.WebParts | Where-Object {$_.Title -eq $webpartTitle}))
{
    write-host $spWeb +": Existing Web part - " + $webpart.Title + " : " + $webpart.ID
    #Delete the existing webpart
    $spWebPartManager.DeleteWebPart($webpart)
    write-host "Deleted the existing" + $webpart.Title + "web part."
}
$spFile.Update()
$spFile.CheckIn("Update page via PowerShell",[Microsoft.SharePoint.SPCheckinType]::MajorCheckIn)
$spFile.Publish("");
$spWeb.Update()
$spWeb.Dispose()
}

function UpdateWebPartTitle
{
$spFile.CheckOut()
#Initialise the Web part manager for the specified profile page.
$spWebPartManager = $spWeb.GetLimitedWebPartManager($spFile.URL, [System.Web.UI.WebControls.WebParts.PersonalizationScope]::Shared)
#Remove the Web part from that page
foreach ($webpart in ($spWebPartManager.WebParts ))
{
    write-host "Web Url: " + $spWeb +" Existing Web part - " + $webpart.Title + " : " + $webpart.ID
	if(($webPart.Title -eq $webpartTitle))
	{    
     write-host "Current Title:"  + $webpart.Title
     $webPart.Title = $UpdatedWebPartTitle
     $spWebPartManager.SaveChanges($webPart);
     write-host "Updated Title:"  + $webpart.Title
	}
}	
$spFile.Update()
$spFile.CheckIn("Update page via PowerShell",[Microsoft.SharePoint.SPCheckinType]::MajorCheckIn)
$spFile.Publish("");
$spWeb.Update()
$spWeb.Dispose()
}


function CallUpdateWebPartTitle([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)
	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
	if($Error.Count -eq 0)
	{
		$webAppFile = $cfg
		$error.clear()
		$sites = $webAppFile.Sites
		foreach($site in $Sites.Site)	
		{
		# Get the list for operation to perform
		$siteUrl= $site.SiteUrl
		$pageName = $site.PageName
		$webpartTitle = $site.WebPartName
		$UpdatedWebPartTitle = $site.UpdatedWebPartName

		#****Give the site url where one want to update page properties***** 

		$spWeb = get-spWeb $siteUrl
		$spFile = $spWeb.GetFile($pageName)

		###Calling function
		UpdateWebPartTitle
		}
	}
}


