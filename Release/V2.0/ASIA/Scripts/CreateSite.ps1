# =================================================================================
#
#Main Function
#
# =================================================================================
function CreateSite([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file"
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
		    $siteCollectionConfig=$cfg.SelectNodes("/Webapplication/Site") | Where-Object {$_.IsSiteCollection -eq "True"}
            $webConfig=$cfg.SelectNodes("/Webapplication/Site") | Where-Object {($_.IsSiteCollection -eq "false") -or ($_.IsSiteCollection -eq $null) -or ($_.IsSiteCollection -eq "")}

            Write-Host "Start : Creation of site collection opeartion:- `n" -ForegroundColor Green
            Write-Output "Start : Creation of site collection opeartion:- `n"
            CreateSiteCollection $siteCollectionConfig
            Write-Host "End : Creation of site collection opeartion:- `n" -ForegroundColor Green
            Write-Output "End : Creation of site collection opeartion:- `n"

            Write-Host "Start : Creation of web opeartion:- `n" -ForegroundColor Green
            Write-Output "Start : Creation of web opeartion:- `n"
            CreateWeb $webConfig
            Write-Host "End : Creation of web opeartion:- `n" -ForegroundColor Green
            Write-Output "End : Creation of web opeartion:- `n"
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host $Error -ForegroundColor Red
        Write-Output $Error
	}
}


# =================================================================================
#
# FUNC: CreateWeb
# DESC: Create web in existing site collection
#
# =================================================================================
function CreateWeb([object] $webConfig)
{
    try
    {
        foreach($siteConfiguration in $webConfig)
	        {
                $exists = (Get-SPWeb $siteConfiguration.SiteURL-ErrorAction SilentlyContinue) -ne $null
                $error.clear()
                if(!$exists)
                {
                    $error.clear()
	                if(!([string]$siteConfiguration.SiteURL -eq ""))
	                {
		                Write-Host "Creating Sites :" $siteConfiguration.SiteName "(" $siteConfiguration.SiteURL ")." -ForegroundColor Green
                        Write-Output "Creating Sites :" $siteConfiguration.SiteName "(" $siteConfiguration.SiteURL ")."
		                $Web = New-SPWeb -Url $siteConfiguration.SiteURL -Name $siteConfiguration.SiteName -Description $siteConfiguration.Description -Language 1033 -Template $siteConfiguration.Template 
		                if($Error.Count -eq 0)
		                {
			                Write-Host "Success: Creating Sites :" $siteConfiguration.SiteName "(" $siteConfiguration.SiteURL ")." -ForegroundColor Green
                            Write-Output "Success: Creating Sites :" $siteConfiguration.SiteName "(" $siteConfiguration.SiteURL ")."
		                }
		                else
		                {
			                Write-Host "Error: Creating Sites :" $siteConfiguration.SiteName "(" $siteConfiguration.SiteURL "). Exiting script..." -foregroundcolor Red
                            Write-Output "Error: Creating Sites :" $siteConfiguration.SiteName "(" $siteConfiguration.SiteURL "). Exiting script..."
			                Write-Host $Error -ForegroundColor Red
                            Write-Output $Error
	                        $error.clear()
		                }
                        # Call function to configure it treeview and navigation settings
                        if($treeView -eq "true")
                        {
                            $web.TreeViewEnabled=$true
                        }
                        else
                        {
                            $web.TreeViewEnabled=$false
                        }
                        $web.Update()
	                }	
                }
                else
                {
                    Write-host "Web already exist at url $($siteConfiguration.SiteURL)" -ForegroundColor Yellow
                    Write-Output "Web already exist at url $($siteConfiguration.SiteURL)" 
                }
            }
    }
    catch
    {
        Write-Host "`nException in creating web :" $Error -ForegroundColor Red
        Write-Output "`nException in creating web :" $Error
    }
}

# =================================================================================
#
# FUNC: CreateSiteCollection
# DESC: Create site collection in web application.
#
# =================================================================================
function CreateSiteCollection([object] $siteCollectionConfig)
{
    try
    {
        foreach($siteConfiguration in $siteCollectionConfig)
	        {
                $exists = (Get-SPWeb $siteConfiguration.SiteURL-ErrorAction SilentlyContinue) -ne $null
                $error.clear()
                if(!$exists)
                {
	                if(!([string]$siteConfiguration.SiteName -eq ""))
	                {
                        if(![string]::IsNullOrEmpty($siteConfiguration.ContentDbName))
                        {
                            $dbExists = ( Get-SPContentDatabase -Identity $siteConfiguration.ContentDbName -ErrorAction SilentlyContinue) -ne $null
                            $error.Clear()
                            if(!(([string]::IsNullOrEmpty($siteConfiguration.DatabaseServerName)) -or ([string]::IsNullOrEmpty($siteConfiguration.WebAppUrl))) -and !$dbExists)
                            {
                                New-SPContentDatabase -Name $siteConfiguration.ContentDbName -DatabaseServer $siteConfiguration.DatabaseServerName -WebApplication $siteConfiguration.WebAppUrl | out-null
                                Write-Host "Creating new content database with name: $($siteConfiguration.ContentDbName)" -ForegroundColor Green
                                Write-Output "Creating new content database with name: $($siteConfiguration.ContentDbName)"
                            }
		                    Write-Host "Creating Site Collection:" $siteConfiguration.SiteName "(" $siteConfiguration.SiteURL ")." -ForegroundColor Green
                            Write-Output "Creating Site Collection:" $siteConfiguration.SiteName "(" $siteConfiguration.SiteURL ")."
		                    $SiteCollection = New-SPSite -Url $siteConfiguration.SiteURL -Name $siteConfiguration.SiteName -Description $siteConfiguration.Description -Language 1033 -ContentDatabase $siteConfiguration.ContentDbName -Template $siteConfiguration.Template -OwnerAlias $siteConfiguration.AdminAccount
                        }
                        else
                        {
                            Write-Host "Creating Site Collection:" $siteConfiguration.SiteName "(" $siteConfiguration.SiteURL ")." -ForegroundColor Green
                            Write-Output "Creating Site Collection:" $siteConfiguration.SiteName "(" $siteConfiguration.SiteURL ")."
		                    $SiteCollection = New-SPSite -Url $siteConfiguration.SiteURL -Name $siteConfiguration.SiteName -Description $siteConfiguration.Description -Language 1033 -Template $siteConfiguration.Template -OwnerAlias $siteConfiguration.AdminAccount
                        }
						if(!$dbExists -and ![string]::IsNullOrEmpty($siteConfiguration.ContentDbName))
						{
							Get-SPContentDatabase -Site $siteConfiguration.SiteURL | Set-SPContentDatabase -MaxSiteCount 1 -WarningSiteCount 0
						}
                        
                        if(![string]::IsNullOrEmpty($siteConfiguration.Quota))
                        {
							$quotaTemplate = [Microsoft.SharePoint.Administration.SPWebService]::ContentService.QuotaTemplates | Where-Object {$_.Name -eq $siteConfiguration.Quota}
							if($quotaTemplate)
							{
								Set-SPSite -Identity $siteConfiguration.SiteURL -QuotaTemplate $quotaTemplate 
								Write-Host "Quota set successfully on site $($siteConfiguration.SiteURL) with name $($siteConfiguration.Quota)" -ForegroundColor Green
								Write-Output "Quota set successfully on site $($siteConfiguration.SiteURL) with name $($siteConfiguration.Quota)"
							}
							else
							{
								Write-Host "Quota template $($siteConfiguration.Quota) not found" -ForegroundColor Green
								Write-Output "Quota template $($siteConfiguration.Quota) not found"
							}
                        }
						if(![string]::IsNullOrEmpty($siteConfiguration.PrimaryOwner))
						{
							Set-SPSite -Identity $siteConfiguration.SiteURL -OwnerAlias $siteConfiguration.PrimaryOwner
							$error.clear()
						}
						if(![string]::IsNullOrEmpty($siteConfiguration.SecondaryOwner))
						{
							Set-SPSite -Identity $siteConfiguration.SiteURL -SecondaryOwnerAlias $siteConfiguration.SecondaryOwner
							$error.clear()
						}
		                if($Error.Count -eq 0)
		                {
			                Write-Host "Success: Creating Site Collection:" $siteConfiguration.SiteName "(" $siteConfiguration.SiteURL ")." -ForegroundColor Green
                            Write-Output "Success: Creating Site Collection:" $siteConfiguration.SiteName "(" $siteConfiguration.SiteURL ")."
		                }
		                else
		                {
			                Write-Host "Error: Creating Site Collection:" $siteConfiguration.SiteName "(" $siteConfiguration.SiteURL "). Exiting script..." -foregroundcolor Red
                            Write-Output "Error: Creating Site Collection:" $siteConfiguration.SiteName "(" $siteConfiguration.SiteURL "). Exiting script..."
			                Write-Host $Error -ForegroundColor Red
                            Write-Output $Error
		                }
			
		                $primaryOwner = ""
 		                $secondaryOwner = ""
  		                if ($siteConfiguration.CreateDefaultGroups -eq "true")
		                {
	 		                Write-Host "Removing any existing visitors group for Site collection $($SiteConfiguration.SiteURL)" -foregroundcolor Blue
                            Write-Output "Removing any existing visitors group for Site collection $($SiteConfiguration.SiteURL)"
	 		                #This is here to fix the situation where a visitors group has already been assigned
	 		                #$SiteCollection.RootWeb.AssociatedVisitorGroup = $null;
	 		                $SiteCollection.RootWeb.Update();
			                Write-Host "Creating Owners group for Site collection $($SiteConfiguration.SiteURL)" -foregroundcolor blue
                            Write-Output "Creating Owners group for Site collection $($SiteConfiguration.SiteURL)"
	 		                $SiteCollection.RootWeb.CreateDefaultAssociatedGroups($primaryOwner, $secondaryOwner, $siteConfiguration.SiteName)
	 		                $SiteCollection.RootWeb.Update();
		                }
                        # Call function to configure it treeview and navigation settings
                        $web= $SiteCollection.RootWeb
                        if($treeView -eq "true")
                        {
                            $web.TreeViewEnabled=$true
                        }
                        else
                        {
                            $web.TreeViewEnabled=$false
                        }
                        $web.Update()
 	                }
                }
                else
                {
                    Write-host "Site collection already exist at url $($siteConfiguration.SiteURL)" -ForegroundColor Yellow
                    Write-Output "Site collection exist at url $($siteConfiguration.SiteURL)" 
                }
            }
    }
    catch
    {
        Write-Host "'nException in creating site collections" $Error -ForegroundColor Red
        Write-Output "'nException in creating site collections" $Error
    }	
}


# ===================================================================================
# FUNC: ActivateFeatures
# DESC: Given some xml containing features, activate those features
# ===================================================================================
function ActivateFeatures([String]$ConfigFileName = "")
{

	# Check that the config file exists.
	if (-not $(Test-Path -Path $configFileName -Type Leaf))
	{
		Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
	}

	$configXml = [xml]$(get-content $configFileName)
	if( $? -eq $false ) 
	{
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	$Error.Clear()

	if($configXml -ne $null)
	{
		foreach ($feature in $configxml.Features.Feature)
		{
            $url = $feature.SiteURL
            
	    	Write-Host "Activating features at Web: $url" -ForegroundColor Green
            Write-Output "Activating features at Web: $url"
            $webFeature=Get-SPFeature -ErrorAction SilentlyContinue -Web $url | ? { $_.id -eq $feature.id }
            $siteFeature=Get-SPFeature -ErrorAction SilentlyContinue -Site $url | ? { $_.id -eq $feature.id }
            $error.Clear()
				
				if(($webFeature -eq $null) -and ($siteFeature -eq $null))
				{
		    		Write-Host "Activating feature $($feature.Name)" -ForegroundColor Green
                    Write-Output "Activating feature $($feature.Name)"
		    		Enable-SPFeature -Identity $feature.id -Url $url
					if($Error.Count -gt 0)
					{
						Write-Host "Error: Activating feature $($feature.Name)" -ForegroundColor Red
                        Write-Output "Error: Activating feature $($feature.Name)"
						break
					}
					Write-Host "Success: Activating feature $($feature.Name)" -ForegroundColor Green
                    Write-Output "Success: Activating feature $($feature.Name)"
				}
                else
                {
                    Write-Host "Feature $($feature.Name) already activated." -ForegroundColor Yellow
                    Write-Output "Feature $($feature.Name) already activated."
          
                    Write-Host "Deactivating Feature $($feature.Name)" -ForegroundColor Yellow
                    Write-Output "Deactivating Feature $($feature.Name)"
                    Disable-SPFeature -Identity $feature.id -Url $url -Force -Confirm: $false

                    Write-Host "Feature deactivated $($feature.Name)" -ForegroundColor Green
                    Write-Output "Feature deactivated $($feature.Name)"

                    Write-Host "Activating feature $($feature.Name)" -ForegroundColor Green
                    Write-Output "Activating feature $($feature.Name)"
		    		Enable-SPFeature -Identity $feature.id -Url $url -Force
					if($Error.Count -gt 0)
					{
                        Write-Output "Error: Activating feature $($feature.Name)"
						Write-Host "Error: Activating feature $($feature.Name)" -ForegroundColor Red
						break
					}
                    Write-Output "Success: Activating feature $($feature.Name)"
					Write-Host "Success: Activating feature $($feature.Name)" -ForegroundColor Green

                }
		}
        Write-Output "Completed activating features at at web: $url"
		Write-Host "Completed activating features at at web: $url" -ForegroundColor Green
	}
	else
	{
		Write-Host "Error: Configuration xml is empty" -ForegroundColor Red
        Write-Output "Error: Configuration xml is empty"
	}

}

# =================================================================================
#
#Main Function to deploy design package
#
# =================================================================================
function DesignPackage([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file"
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
            foreach($site in $cfg.Sites.Site)
            {
                DeployDesignPackage $site
            }
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host $Error -ForegroundColor Red
        Write-Output $Error
	}
}

# =================================================================================
#
# FUNC: DeployDesignPackage
# DESC: Deploy design package at site collection level.
#
# =================================================================================
function DeployDesignPackage([object] $site)
{
    try
    {
        [boolean]$IsDesignMgr=[System.Convert]::ToBoolean(@{$true="false";$false=$site.IsDesignManager}[$site.IsDesignManager -eq ""])
        if($IsDesignMgr)
        {
            $siteobj = Get-SPSite $site.SiteURL
		    [string] $filePath = Get-Location
		    $filePath = $filePath + "\" + [string]$site.PackageUrl
		    $filePath = $filePath -replace "\\.\\", "\"
		    $fileName = [System.IO.Path]::GetFileName($filePath)
		
		    $package = New-Object Microsoft.SharePoint.Publishing.DesignPackageInfo($fileName, [Guid]::Empty, 1, 0)
		    $tempFolderName = "temp_designupload_" + ([Guid]::NewGuid()).ToString()
		    $tempFolder = $siteobj.RootWeb.RootFolder.SubFolders.Add($tempFolderName)

		    $fileBinary = [System.IO.File]::OpenRead($filePath)
		    $file = $tempFolder.Files.Add($fileName, $fileBinary, $true)
		    $fileBinary.Close()

		    [Microsoft.SharePoint.Publishing.DesignPackage]::Install($siteobj, $package, $file.Url)
		    [Microsoft.SharePoint.Publishing.DesignPackage]::Apply($siteobj, $package)

		    Write-Host "Design Package installed successfully at site $($site.SiteURL)" -foreground green
            Write-Output "Design Package installed successfully at site $($site.SiteURL)" 
		    $tempFolder.Delete()
		    $siteobj.Dispose()
        }
    }
    catch
    {
        Write-Host "'nException in deploying design package at site $($site.SiteURL)" $Error -ForegroundColor Red
        Write-Output "'nException in deploying design package at site $($site.SiteURL)" $Error
    }	
}


# =================================================================================
#
# FUNC: SetSearchCenterUrl
# DESC: funtion to set search center url.
#
# =================================================================================
function SetSearchCenterUrl([object] $ConfigPath)
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file"
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
            if($cfg.Webapplication.Site)
            {
                foreach($site in $cfg.Webapplication.Site)
                {
                    if(![string]::IsNullOrEmpty( $site.SiteURL))
                    {
                        $srchWeb=Get-SPWeb $site.SiteURL
                        if($srchWeb)
                        {
                            $srchWeb.AllProperties["SRCH_SB_SET_WEB"] = "{""Inherit"":false,""ResultsPageAddress"":""" + $site.SearchResultPageUrl + """,""ShowNavigation"":false}"
                            $srchWeb.AllProperties["SRCH_ENH_FTR_URL_WEB"] = $site.SearchCenterSiteUrl
                            $srchWeb.Update();
                            Write-Host "`nSearch settings updated for web :$($site.SiteURL)" -ForegroundColor Green
                            Write-Output "`nSearch settings updated for web :$($site.SiteURL)"
                        }
                        else
                        {
                            Write-Host "`nWeb not found at url :" $site.SiteURL -ForegroundColor Yellow
                            Write-Output "`nWeb not found at url : $($site.SiteURL)"
                        }
                    }
                }
            }
        }
        catch
        {
            Write-Host "`nException in setting search center :" $Error -ForegroundColor Red
            Write-Output "`nException in setting search center :" $Error
        }
    }
}