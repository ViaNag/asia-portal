$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

function AssociateLookUp([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$lookUpXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green      

    $sitecollectionUrl =  $lookUpXml.SiteCollection.SiteUrl

    # Get Site Collection URL
    $siteCollection = Get-SPSite $sitecollectionUrl
    
    
    
    $rootWeb = $siteCollection.RootWeb 	
    
	Write-Host "In the Site Collection"  $web.url  

    $lookUpXml.SiteCollection.Subsite |
	ForEach-Object {  

        $Subsite = $_.SubSiteUrl

        if($Subsite -eq "")
        {
            $web = $siteCollection.OpenWeb();
        }
        else
        {
            $web = Get-SPWeb -Identity $Subsite	
        }
        

        $_.mapping |
	    ForEach-Object {
	    		
            $lookupListName = $_.LookUpListName
            $columnName = $_.SiteColumnName

            $addtionalField = $_.AdditionalField

            $listName = $_.ListName        
            $list = $web.Lists[$listName]
            $column = $list.Fields.GetFieldByInternalName($columnName)
            $additionalColumn = $null
            if($addtionalField -ne "")
            {
                $additionalColumn = $list.Fields.GetFieldByInternalName($addtionalField)
            }

            # Get LookUp Column By Internal name
            # $column = $rootWeb.Fields.GetFieldByInternalName($columnName)

            if ($column -ne $null)     
            {
                   # Get List to be associated as Lookup
                   $lookupList = $rootWeb.Lists[$lookupListName]
            
                   if ($lookupList -ne $null) 
                   {
                   
                        $newLookupListID = "{"+$lookupList.ID.ToString()+"}"            
                        $newLookupWebID = $rootWeb.ID.ToString()
                        # Replace WebId and List in the schema of site column
                        $column.SchemaXml = $column.SchemaXml.Replace($column.LookupWebId.ToString(), $newLookupWebID)
                        $column.SchemaXml = $column.SchemaXml.Replace($column.LookupList.ToString(), $newLookupListID)
                        $column.Update()

                        if ($additionalColumn -ne $null)     
                        {
                            $additionalColumn.SchemaXml = $additionalColumn.SchemaXml.Replace($additionalColumn.LookupWebId.ToString(), $newLookupWebID)
                            $additionalColumn.SchemaXml = $additionalColumn.SchemaXml.Replace($additionalColumn.LookupList.ToString(), $newLookupListID)
                            $additionalColumn.Update()

                        }
                                  
                        write-host $columnName "Look Up  field updated" -ForegroundColor Green                   
                        
                   }
                   
                   $lookupListName = $null
                   $columnName = $null

                   $addtionalField = $null

                   $listName = $null       
                   $list = $null
                   $column = $null
                   $additionalColumn = $null
                   $lookupList = $null 
                   $newLookupListID = $null
                   $newLookupWebID = $null
            }        
        }
    }
        
	$rootWeb.Dispose()				
}
