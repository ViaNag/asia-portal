Add-PSSnapin Microsoft.SharePoint.Powershell

function Create-AllMetadataView
{
    
    Param (
           [parameter(Mandatory=$true)][string]$SiteUrl,
           [parameter(Mandatory=$true)][string]$SubSiteUrl,
           [parameter(Mandatory=$true)][string]$CSVPath
           )
 
    # Import CSV
    [array]$FolderList = Import-Csv $CSVPath
 
    # Script Variables
    $subSiteWeb = Get-SPWeb $SubSiteUrl
	
	#All metadata view
	#$foldersViewName = "Folders"
	$allMetadataViewName = "All Metadata"
	$defaultViewName = "Default View"
	
	$foldersViewFields = New-Object System.Collections.Specialized.StringCollection
	$foldersViewFields.Add("DocIcon") > $null
	$foldersViewFields.Add("LinkFilename") > $null

	#Add the column names from the ViewField property to a string collection
	$viewFields = New-Object System.Collections.Specialized.StringCollection
	$viewFields.Add("DocIcon") > $null 
	$viewFields.Add("Play") > $null
	$viewFields.Add("LinkFilename") > $null
    $viewFields.Add("AdSpaceContentCategory") > $null    
    $viewFields.Add("DocPublishingFrequency") > $null
    $viewFields.Add("DocNetwork") > $null
    $viewFields.Add("DocYear") > $null
    $viewFields.Add("DocQuarter") > $null
    $viewFields.Add("DocMarketplace") > $null
    $viewFields.Add("DocPlatform") > $null
	$viewFields.Add("ContentPublisherDepartment") > $null
	$viewFields.Add("Cluster") > $null
	$viewFields.Add("Demos") > $null
	$viewFields.Add("RecordSeries") > $null
	$viewFields.Add("Modified") > $null
	$viewFields.Add("Editor") > $null
	
	#Add the column names from the ViewField property to a string collection
	$allMetadataViewFields = New-Object System.Collections.Specialized.StringCollection
	$allMetadataViewFields.Add("DocIcon") > $null
	$allMetadataViewFields.Add("Play") > $null
	$allMetadataViewFields.Add("LinkFilename") > $null
    $allMetadataViewFields.Add("AdSpaceContentCategory") > $null
    $allMetadataViewFields.Add("AdSpaceDescription") > $null
    $allMetadataViewFields.Add("Modified") > $null
	$allMetadataViewFields.Add("Editor") > $null
    $allMetadataViewFields.Add("DocNetwork") > $null
    $allMetadataViewFields.Add("DocYear") > $null
    $allMetadataViewFields.Add("DocQuarter") > $null
    $allMetadataViewFields.Add("DocMarketplace") > $null
    $allMetadataViewFields.Add("DocPlatform") > $null
	$allMetadataViewFields.Add("LifecycleState") > $null
	$allMetadataViewFields.Add("Shareable") > $null
    $allMetadataViewFields.Add("DocThumbnailImage") > $null
	$allMetadataViewFields.Add("ContentPublisherDepartment") > $null
	$allMetadataViewFields.Add("Cluster") > $null
	$allMetadataViewFields.Add("Demos") > $null
	$allMetadataViewFields.Add("RecordSeries") > $null
	
    
	
	$viewQuery = "<Where><Or><IsNull><FieldRef Name='IsArchived'/></IsNull><Eq><FieldRef Name='IsArchived' /><Value Type='Boolean'>No</Value></Eq></Or></Where>"
	
    Foreach ($folderGroup in $FolderList) {
		
		if($folderGroup.LibraryName -ne '')
        {
            Write-Host ($folderGroup.LibraryName+" Name")
            
            $docLib = $subSiteWeb.Lists[$folderGroup.LibraryName]
			
			$folderView = $docLib.Views["Folders"]
			$allMetadataView = $docLib.Views["All Metadata"]
			$defaultView = $docLib.Views["Default View"]
			
			if($folderView -ne $null)
			{
				#Delete this view from the list
				$docLib.Views.Delete($folderView.ID)
			}
			
			if($allMetadataView -ne $null)
			{
				#Delete this view from the list
				$docLib.Views.Delete($allMetadataView.ID)
			}
			
			if($defaultView -ne $null)
			{
				#Delete this view from the list
				$docLib.Views.Delete($defaultView.ID)
			}
			#Create the view in the Navigation list
			#$folderView = $docLib.Views.Add($foldersViewName, $foldersViewFields, $viewQuery, 50, $true, $false)
			#Apply JSLink
			#$folderView.JSLink = "~sitecollection/_catalogs/masterpage/adspacemasterpageandlayouts/js/jslink/folderview.js"
			#$folderView.Update()
			#Write-Host ("JSLink applied on view '" + $folderView.Title + "'")
			
			$allMetadataView = $docLib.Views.Add($allMetadataViewName, $allMetadataViewFields, $viewQuery, 50, $true, $false)
			$defaultView = $docLib.Views.Add($defaultViewName, $viewFields, $viewQuery, 50, $true, $true)
			
			Write-Host ("View '" + $allMetadataView.Title + "' created in list '" + $docLib.Title + "' on site " + $subSiteWeb.Url)
			Write-Host ("View '" + $defaultView.Title + "' created in list '" + $docLib.Title + "' on site " + $subSiteWeb.Url)

            $AllDocumentsView = $docLib.Views["All Documents"]
            if($AllDocumentsView -ne $null)
			{
                Write-Host ("View '" + $AllDocumentsView.Title + "' deleted on list '" + $docLib.Title + "' on site " + $subSiteWeb.Url)                
				#Delete this view from the list
				$docLib.Views.Delete($AllDocumentsView.ID)
			}			
			

             Write-Host ("Hiding columns from New, edit and view from in list '" + $docLib.Title + "' on site " + $subSiteWeb.Url)

            for ($i = 0; $i -lt $docLib.Fields.Count; $i++)
            {
                 $targetField = $docLib.Fields[$i]
				 $internalName = $targetField.InternalName

                 if($targetField -ne $null)
					{
						if(($internalName -eq "Division") -or ($internalName -eq "ViacomDepartment") -or ($internalName -eq "AdSpaceContentCategory") -or ($internalName -eq "LifecycleState") -or ($internalName -eq "FolderID") -or ($internalName -eq "ProxyThumbnailURL") -or ($internalName -eq "SourceVideoURL") -or ($internalName -eq "ProxyVideoURL") -or ($internalName -eq "ProjectID") -or ($internalName -eq "AssetID") -or ($internalName -eq "ProxyRTMP_Streamer") -or ($internalName -eq "ProxyRTMPURL") -or ($internalName -eq "ProxyRTMPTStreamer") -or ($internalName -eq "ProxyRTMPTURL") -or ($internalName -eq "IsVideo") -or ($internalName -eq "Play") -or ($internalName -eq "ContentPublisherDepartment") -or ($internalName -eq "Cluster") -or ($internalName -eq "Demos") -or ($internalName -eq "RecordSeries"))
						 {
							 Write-Host "Updating Field" $internalName -ForegroundColor Yellow

							 $targetField.ShowInEditForm = $false
							 $targetField.ShowInNewForm = $false			
							 $targetField.ShowInDisplayForm = $false
							 $targetField.Update($true)
							 Write-Host "Updated Field" $internalName -ForegroundColor Green						
						}
					}
			}
        }        
    }
    $subSiteWeb.Dispose()
}

function CreateNavForFolders([String]$configFileName = "")
{
    [string] $currentLocation = Get-Location

    Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green

    # Check that the config file exists.
    if (-not $(Test-Path -Path $configFileName -Type Leaf))
    {
	    Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
    }

    $configXml = [xml]$(get-content $configFileName)
    if( $? -eq $false ) 
    {
	    Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
    }

    if ($configXml.SubSiteFolders)
    {
	    foreach ($SubSite in $configXml.SubSiteFolders.Site)
	    {
		    Write-Host "Creating all metadata view for libraries in " + $SubSite.SubSiteUrl + " with " + $SubSite.CSVFile + "CSV file" -ForegroundColor Yellow

                $csvFullPath = $currentLocation + "\DataFiles\View\" + $SubSite.CSVFile

                Create-AllMetadataView -SiteUrl $SubSite.SiteUrl -SubSiteUrl $SubSite.SubSiteUrl -CSVPath $csvFullPath

                Write-Host "Successfully Created navigation for " + $SubSite.SubSiteUrl + " with " + $SubSite.CSVFile + "CSV file" -ForegroundColor Green
        }
    }
}