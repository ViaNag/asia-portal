if ((Get-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue) -eq $null) 
{
    Add-PSSnapin "Microsoft.SharePoint.PowerShell"
}

function CreateDeviceChannels([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$NavXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green 

    $siteUrl =  $NavXml.SiteCollection.url
    $site =Get-SPSite  $siteUrl
    $web = $site.OpenWeb();

    $list = $web.Lists["Device Channels"]

    $itemTablet = $list.Items.Add()

    $itemTablet["Name"] = "Tablet"
    $itemTablet["Alias"] = "Tablet"
    $itemTablet["Description"] = "Device Channel for Tablets"
    $itemTablet["Device Inclusion Rules"] = "iPad"
    $itemTablet["Active"] = $true

    $itemTablet.Update()

    $itemIPhone = $list.Items.Add()

    $itemIPhone["Name"] = "iPhone"
    $itemIPhone["Alias"] = "iPhone"
    $itemIPhone["Description"] = "Device Channel for iPhone"
    $itemIPhone["Device Inclusion Rules"] = "iPhone"
    $itemIPhone["Active"] = $true

    $itemIPhone.Update()

    $itemAndroid = $list.Items.Add()

    $itemAndroid["Name"] = "Android"
    $itemAndroid["Alias"] = "Android"
    $itemAndroid["Description"] = "Device Channel for Android"
    $itemAndroid["Device Inclusion Rules"] = "Android"
    $itemAndroid["Active"] = $true

    $itemAndroid.Update()

    $web.Dispose()
}