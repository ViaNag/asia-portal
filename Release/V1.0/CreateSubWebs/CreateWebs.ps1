$siteUrl = Read-Host "Enter the site url"
$rootWeb = Get-SPWeb $siteUrl
[xml]$configFile = Get-Content CreateWebConfiguration.xml
$createWebNode = $configFile.CreateWeb
$webtemplate = "BLANKINTERNET#0"
$subWebTemplate = "{d828199f-d17a-4b5f-9ecf-de1171364a02}#IntranetWebTemplate"
$childSubWebTemplate = "{d828199f-d17a-4b5f-9ecf-de1171364a02}#IntranetWebTemplate"
$template = $rootWeb.Site.GetWebTemplates(1033) | Where-Object {$_.Name -eq $subWebTemplate}
foreach($webNode in $createWebNode.Web)
{
	try
	{
		$webTitle = $webNode.Title
		$webUrl = $webNode.URL
		$rootWebURL = $rootWeb.Url
		$newWebUrl = $rootWebURL+"/"+$webUrl
		if($webNode.Create -eq "true"){
		Write-Host "Creating Web "$webTitle "on Url:"$newWebUrl -ForegroundColor Yellow
		$Web = New-SPWeb -Url $newWebUrl -Name $webTitle -Description "" -Language 1033 -Template $template -UseParentTopNav
		$Web.ApplyWebTemplate($subWebTemplate)
		Write-Host "Template is found and applied successfully" -ForegroundColor Green
		}
		foreach($subWebNode in $webNode.SubWeb)
		{
			try
			{
				$subWebTitle = $subWebNode.Title
				$subWebUrl = $subWebNode.URL
				$newSubWebUrl = $newWebUrl+"/"+$subWebUrl
				Write-Host "Creating Web "$subWebTitle "on Url:"$newSubWebUrl -ForegroundColor Yellow
				$SubWeb = New-SPWeb -Url $newSubWebUrl -Name $subWebTitle -Description "" -Language 1033 -Template $template -UseParentTopNav
				$SubWeb.ApplyWebTemplate($subWebTemplate)
				Write-Host "Template is found and applied successfully" -ForegroundColor Green
				foreach($subChildWebNode in $subWebNode.ChildSubWeb)
				{
					try
					{
						$childSubWebTitle = $subChildWebNode.Title
						$childSubWebUrl = $subChildWebNode.URL
						$newChildSubWebUrl = $newSubWebUrl+"/"+$childSubWebUrl
						Write-Host "Creating Web "$childSubWebTitle "on Url:"$newChildSubWebUrl -ForegroundColor Yellow
						$ChildSubWeb = New-SPWeb -Url $newChildSubWebUrl -Name $childSubWebTitle -Description "" -Language 1033 -Template $template -UseParentTopNav
						$ChildSubWeb.ApplyWebTemplate($subWebTemplate)
						Write-Host "Template is found and applied successfully" -ForegroundColor Green
					}
					catch{
						Write-Host $_.Exception.Message -ForegroundColor Red
					}
				}
			}
			catch{
				Write-Host $_.Exception.Message -ForegroundColor Red
			}
		}
	}
	catch{
		Write-Host $_.Exception.Message -ForegroundColor Red
	}
}