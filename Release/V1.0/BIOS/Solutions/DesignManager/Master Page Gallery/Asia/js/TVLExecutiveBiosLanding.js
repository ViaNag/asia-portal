﻿(function () {
    var itemCtx = {};
    itemCtx.Templates = {};
    itemCtx.Templates.Header = pagingControl; "<ul class='cbs-List2'>";
    itemCtx.Templates.Item = ItemOverrideFun; +"</ul>";
    itemCtx.BaseViewID = 1;
    itemCtx.ListTemplateType = 850;
    SPClientTemplates.TemplateManager.RegisterTemplateOverrides(itemCtx);
})();

function ItemOverrideFun(ctx) {
    var liContent = "";
    var readmore = "Read More";
    var count = 1;
    var image = encodeURI(ctx.CurrentItem.SmallBiosImage);
    var title = ctx.CurrentItem.Title;
    var name = ctx.CurrentItem.FileLeafRef;
    var siteRelativeUrl = _spPageContextInfo.siteServerRelativeUrl.length == 1 ? "" : _spPageContextInfo.siteServerRelativeUrl;
    if (image != "") 
	{	
		 if(image.indexOf("?RenditionID=2")!=-1)
        {
            var RendIdIndex = image.indexOf("?RenditionID");
            image= image.substring(0,RendIdIndex);
			image=image+"?RenditionID=2";
        }
        liContent += '<li style="list-style-type: none;" class="ExecBio col-md-4 col-sm-4 col-xs-6"><div class="showcontainer"><div class="cbs-largePictureContainer" style="padding-bottom:5px;"><a class="cbs-pictureImgLink" href="' + name + '" title="' + title + '"><span class="ShowDescription" style="text-align:center"><br/><br/><span class="BlueButtons2 newreadmorebutton">' + readmore + '</span></span></a><img class="imgDimensions" src="' + image + '"/></div><a class="ShowTitleAnchor" href="' + name + '" title="' + title + '"><div class="ShowTitleBlock" ><h3 class="h2Show">' + title + '</h3></div></a></div></li>';
    }
    else {
        liContent += '<li style="list-style-type: none;" class="ExecBio col-md-4 col-sm-4 col-xs-6"><div class="showcontainer"><div class="cbs-largePictureContainer" style="padding-bottom:5px;"><a class="cbs-pictureImgLink" href="' + name + '" title="' + title + '"><span class="ShowDescription" style="text-align:center"><br/><br/><span class="BlueButtons2 newreadmorebutton">' + readmore + '</span></span></a><img class="imgDimensions" src="' + siteRelativeUrl + '/SiteAssets/NewPicture.png"/></div><a class="ShowTitleAnchor" href="' + name + '" title="' + title + '"><div class="ShowTitleBlock"><h3 class="h2Show">' + title + '</h3></div></a></div></li>';
    }
    return liContent;
}

function pagingControl(ctx) {
    var footerHtml = "";
    var firstRow = ctx.ListData.FirstRow;
    var lastRow = ctx.ListData.LastRow;
    var prev = ctx.ListData.PrevHref;
    var next = ctx.ListData.NextHref;

    footerHtml += "<div align='left'><table class=\"ms-bottompaging\" style='width:100%;'><tr><td><style>.left-container .ms-wpContentDivSpace{margin:0 -15px}</style>";
    if (prev)
        footerHtml += "<span class='showprev hidden'><a class='ms-commandLink ms-promlink-button ms-promlink-button-enabled' href='" + prev + "'><span class='ms-promlink-button-image'><img class='ms-promlink-button-left' src='/_layouts/15/images/spcommon.png?rev=23' /></span></a><span>&nbsp;&nbsp;</span></span>";
    else
        footerHtml += "<span class=\"ms-promlink-button-inner hidden\"></span>";
    if (prev && next)
        footerHtml += " ";
    if (next)
        footerHtml += "<span class='shownext hidden'><span>&nbsp;&nbsp;</span><a class='ms-commandLink ms-promlink-button ms-promlink-button-enabled' href='javascript:void(0);' onclick=\"RefreshPageTo(event, &quot;" + next + "&quot;);return false;\"><span class='ms-promlink-button-image'><img class='ms-promlink-button-right' src='/_layouts/15/images/spcommon.png?rev=23'/></span></a></span>";
    else
        footerHtml += "</td></tr></table></div>";
    return footerHtml;
}

