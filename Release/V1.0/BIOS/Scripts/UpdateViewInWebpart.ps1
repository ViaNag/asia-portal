﻿$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

function UpdateViewInWebpart([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$lookUpXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green      

    
    $lookUpXml.Sites.Site |
	ForEach-Object {  

        $Subsite = $_.SiteUrl
        
        $web = Get-SPWeb -Identity $Subsite	

        $listName = $_.ListName
        $viewName = $_.View
        $pageURL = $_.Page
        $webPartTitle = $_.WebPartTitle

        $list = $web.Lists[$listName]
        $view = $list.Views[$viewName]

        $web.AllowUnsafeUpdates  = $true 
        $file = $web.GetFile($pageURL)
        $file.CheckOut()            

        $wpm = $web.GetLimitedWebPartManager($pageURL, [System.Web.UI.WebControls.WebParts.PersonalizationScope]::Shared)
        foreach($item in $wpm.WebParts)
        {
           if($item.Title -eq $webPartTitle)
           {               
               $wp = $item
               break
           }
        }
        if($wp)
        {
           $listVWP = New-Object Microsoft.SharePoint.WebPartPages.XsltListViewWebPart
           $listVWP.Title = $wp.Title
           $listVWP.ChromeType = [System.Web.UI.WebControls.WebParts.PartChromeType]::None
           $listVWP.Visible = $true
           $listVWP.EnableViewState = $true
           $listVWP.ListName = $wp.ListName
           $listVWP.ViewGuid = $view.ID.ToString("B").ToUpper()           
           $listVWP.ListId = [System.Guid]$wp.ListId
           $listVWP.JSLink = $wp.JSLink

           $zoneID = $wp.ZoneID
           $zoneIndex = $wp.ZoneIndex
           
           $wpm.DeleteWebPart($wp);
           $wpm.AddWebPart($listVWP, $zoneID, $zoneIndex);
           
           $file.CheckIn("")
           $file.update() 
           $file.Publish("Updated")
           
           $web.AllowUnsafeUpdates  = $false
           $wp.Dispose()
           Write-Host "View updated succesfully on" $pageURL "in" $web.Title  -ForegroundColor Green
        }
       
        $web.Dispose()	
    }       
				
}
