﻿$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

function ColumnReordering([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$lookUpXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green      

    $siteURL =  $lookUpXml.Sites.SiteURL
	$site=Get-SPSite $siteURL 
	$web = $site.Rootweb
	$columnOrder= @('FileLeafRef','Title','Division','ViacomDepartment','LifecycleState','DocContentType','DocCreated','DocCreatedBy','DocModified','DocModifiedBy','DocSourceLibrary','RecordDeclarationDate','Shareable','MaxShareDuration','IsArchived','FolderID','ProxyThumbnailURL','SourceVideoURL','ProxyVideoURL','ProjectID','AssetID','ProxyRTMP_Streamer','ProxyRTMPURL','ProxyRTMPTStreamer','ProxyRTMPTURL','IsVideo','Play','AdSpaceContentCategory','DocNetwork','DocPublishingFrequency','DocMarketplace','DocPlatform','DocQuarter','DocYear','ContentPublisherDepartment','Cluster','Demos','RecordSeries','AdSpaceDescription','DocThumbnailImage')
	# Update CT Column order
	$lookUpXml.Sites.ContentType |
	ForEach-Object {
        $contentTypeName = $_.Name
		
		$ct = $web.ContentTypes[$contentTypeName] 
		if($ct)
		{
		$ct.FieldLinks.Reorder($columnOrder) 
		$ct.Update($true)
		Write-Host "Column Order for content type" $contentTypeName "has been changed" -ForegroundColor Green
		}
		else
		{
		Write-Host "Content Type Not Found" -ForegroundColor Green
		}
    }
	$web.Dispose()				
}
