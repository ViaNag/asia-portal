if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
{    
      Add-PSSnapin Microsoft.SharePoint.PowerShell
}

function UpdatePageProperties()
{
$spFile.CheckOut()
$spFile.ListItemAllFields["Name"] = "UpdatePagePropertiesTest"
$spFile.ListItemAllFields["Title"] = "Asia"
$spFile.ListItemAllFields.Update()
$spFile.Update()
$spFile.CheckIn("Update page Title & Name via PowerShell",[Microsoft.SharePoint.SPCheckinType]::MajorCheckIn)
$spFile.Publish("");
$spWeb.Update()
$spWeb.Dispose()
}


function CallUpdatePageProperties([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)
	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
	if($Error.Count -eq 0)
	{
		$webAppFile = $cfg
		$error.clear()
		$sites = $webAppFile.Sites
		foreach($site in $Sites.Site)	
		{
		# Get the list for operation to perform
		$siteUrl= $site.SiteUrl
		$pageName = $site.PageName

		#****Give the site url where one want to update page properties***** 

		$spWeb = get-spWeb $siteUrl
		$spFile = $spWeb.GetFile($pageName)

		###Calling function
		UpdatePageProperties
		}
	}
}