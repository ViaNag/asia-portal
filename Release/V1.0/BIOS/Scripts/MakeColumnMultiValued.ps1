﻿$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

function MakeColumnMultiValued([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$lookUpXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green      

    $sitecollectionUrl =  $lookUpXml.Sites.SiteCollection
	#Columns coll for multi valued
	$listColArray = "DocYear","DocNetwork","DocQuarter","DocPlatform","Demos","AdSpaceContentCategory"
    # Get Site Collection URL
    $siteCollection = Get-SPSite $sitecollectionUrl    
    $rootWeb = $siteCollection.RootWeb 	
	
	foreach ( $ColumnName in $listColArray ) 
		{ 
			$siteColumn = $rootWeb.Fields.GetFieldByInternalName($ColumnName)
			if($siteColumn -ne $null)
			{
				$siteColumn.AllowMultipleValues=$true       
				$siteColumn.Update()
				Write-Host "Site Column" $ColumnName "changed to multivalued" -ForegroundColor Green
				$siteColumn = $null
			}
		}	
    
	
    $lookUpXml.Sites.Site |
	ForEach-Object {
        $SiteURL = $_.SiteURL        
        $siteWeb = Get-SPWeb $SiteURL 
        $csvFullPath = $currentLocation + "\DataFiles\View\" + $_.CSVFile
        # Import CSV
        [array]$Lists = Import-Csv $csvFullPath
        Foreach ($list in $Lists) {		
		    if($list.LibraryName -ne '')
            {
				foreach ( $ColumnName in $listColArray ) 
					{
						$docLib = $siteWeb.Lists[$list.LibraryName]
						$column = $docLib.Fields.GetFieldByInternalName($ColumnName)
						$column.AllowMultipleValues=$true      
						$column.Update()
						$column = $null
						Write-Host "Column" $ColumnName "changed to multivalued in" $siteWeb.Url -ForegroundColor Green
					}
            }
        }        
        $siteWeb.Dispose()
    }             
	$rootWeb.Dispose()				
}
