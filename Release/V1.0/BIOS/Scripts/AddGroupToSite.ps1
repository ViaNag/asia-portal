$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

function AddGroupToSite([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$lookUpXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green      

	$lookUpXml.Sites.Site |
	ForEach-Object {  

        $url = $_.url
		$web= Get-SPWeb -Identity $url	
        $_.Group |
	    ForEach-Object {
	    	$grpName = $_.Name
            $permission = $_.PermissionLevel
			$account = $web.SiteGroups[$grpName]
			$assignment = New-Object Microsoft.SharePoint.SPRoleAssignment($account)
			$role = $web.RoleDefinitions[$permission]
			$assignment.RoleDefinitionBindings.Add($role);
			$web.RoleAssignments.Add($assignment)
			Write-Host "Group added to site" -ForegroundColor Green      
            }        
        }
    }
   			

