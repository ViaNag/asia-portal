﻿Add-PSSnapin Microsoft.SharePoint.Powershell

function UpdateAccessTo([String]$configFileName = "")
{
    [string] $currentLocation = Get-Location

    Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green

    # Check that the config file exists.
    if (-not $(Test-Path -Path $configFileName -Type Leaf))
    {
	    Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
    }

    $configXml = [xml]$(get-content $configFileName)
    if( $? -eq $false ) 
    {
	    Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
    }

    # Get Site Collection URL
    $siteCollection = $configXml.Sites.SiteCollection

    $spWeb = Get-SPWeb $siteCollection 

    $navigationList = $spWeb.Lists["Navigation"]

    $items = $navigationList.items
    
    [Microsoft.SharePoint.SPFieldUserValueCollection] $valueCollection = new-object Microsoft.SharePoint.SPFieldUserValueCollection
     
    foreach($item in @($items))  
    {
        $value = $item["LinkHeader"]  
        $headerTitle = $item["HeaderTitle"]
        
        if($value)
        {
            
            if($value.Contains("International Sales"))
            {
               $accessTo = $configXml.Sites.International.AccessTo
            }
            if($value.Contains("Domestic Sales"))
            {
               $accessTo = $configXml.Sites.Domestic.AccessTo
            }
            if($value.Contains("Financial Reports"))
            {
               $accessTo = $configXml.Sites.FinancialReports.AccessTo
            }
        }
        if($headerTitle)
        {
            if($headerTitle.Contains("International Sales"))
            {
               $accessTo = $configXml.Sites.International.AccessTo
            }
            if($headerTitle.Contains("Domestic Sales"))
            {
               $accessTo = $configXml.Sites.Domestic.AccessTo
            }
            if($headerTitle.Contains("Financial Reports"))
            {
               $accessTo = $configXml.Sites.FinancialReports.AccessTo
            }
            if($headerTitle.Contains("ClientSpace"))
            {
               $accessTo = $configXml.Sites.ClientSpace.AccessTo
            }
        }
    
        if($accessTo)
        {
             $groupSplit=$accessTo.Split(';')
    	   
    	  foreach($separateGroup in $groupSplit)
    	  {
                 #Check for group name and add to collection
                 $spGroup = $spWeb.Groups[$separateGroup.Trim()]
                 $spFieldUserValue = New-Object Microsoft.SharePoint.SPFieldUserValue($spWeb, $spGroup.ID, $spGroup.Name); 
                 $valueCollection.Add($spFieldUserValue)
    	  }
             $item["AccessTo"] = [Microsoft.SharePoint.SPFieldUserValueCollection]$valueCollection
             #Update the item
             $item.Update()
        }
        $valueCollection.Clear()
        $accessTo=$null       
    
            
    }
    Write-Host "AccessTo fields updated in Navigation list successfully" -ForegroundColor Green
}



