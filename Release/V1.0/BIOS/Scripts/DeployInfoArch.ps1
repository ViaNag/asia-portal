# check to ensure Microsoft.SharePoint.PowerShell is loaded
$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'}
if ($snapin -eq $null) 
{
    Write-Host "Loading SharePoint Powershell Snapin" -ForegroundColor Green
    Add-PSSnapin "Microsoft.SharePoint.Powershell"
}

# =================================================================================
$Comment = 'System Approval - Deployment Process'
$Recursive = $true
$ApproveFolders = $true
$Extension = ".*"
# =================================================================================


# =================================================================================
# FUNC: Approve-Object
# DESC: Approve file or folder object
# =================================================================================
function Approve-Object($object)
{
	# If someone unpublished the version or checked out, the item goes in draft mode
	$modInfo = $object.Item.ModerationInformation
	
	# If publishing status is draft then publish with major version Folder's wont go in 'Draft mode'
	if($modInfo.Status -eq [Microsoft.SharePoint.SPModerationStatusType]::Draft)
	{
		$object.Publish($Comment)
	}
	
	# Get the moderation information again as page goes from draft to pending approval
	$modInfo = $object.Item.ModerationInformation
	
	if($modInfo.Status -eq [Microsoft.SharePoint.SPModerationStatusType]::Pending)
	{
		if(($ApproveFolders -eq "true") -and ($object.GetType() -eq [Microsoft.SharePoint.SPFolder]))
		{
			# Its a folder we need to approve it in different way ;)
			$object.Item.ModerationInformation.Status = [Microsoft.SharePoint.SPModerationStatusType]::Approved
			$object.Item.ModerationInformation.Comment = $Comment;
			$object.Item.Update();
		}
		else
		{
			
			# Its a file
			$object.Approve($Comment)
		}
	}
}

# =================================================================================
# FUNC: Approve-File
# DESC: Approve the file
# =================================================================================
function Approve-File($file, $moderated)
{
	if($Extension -eq ".*" -or $_.Name.EndsWith($Extension))
	{
		# Check if the file is checked in
		if($file.Item.Level -eq [Microsoft.SharePoint.SPFileLevel]::Checkout)
		{
			$file.CheckIn($Comment,[Microsoft.SharePoint.SPCheckInType]::MajorCheckin )
		}
		
		# If moderation is enabled process the document for approval
		if($moderated)
		{
			Approve-Object $file
		}
	}
}

# =================================================================================
# FUNC: Approve-FolderContent
# DESC: Approve contents inside the folders : Recursive
# =================================================================================
function Approve-FolderContent($folder, $moderated)
{
	# Approve files inside this folder
	if($folder.Folder.Files)
	{
		$folder.Folder.Files | ForEach-Object{
			Approve-File $_ $moderated
		}
	}
}

# =================================================================================
# FUNC: Approve-LibraryContent
# DESC: Approve content document libraries
# =================================================================================
function Approve-LibraryContent($list)
{
	
	# Process top level items
	$list.Items | ForEach-Object{
			Approve-File $_.File $list.EnableModeration
	}
	
	$list.Folders | ForEach-Object{
		
		# Approve folder itself its in pending status
		Approve-Object $_.Folder
		
		# Approve contents inside the folder
		Approve-FolderContent $_ $list.EnableModeration
	}
}

# =================================================================================
# FUNC: Approve-WebContent
# DESC: Iterate through all webs : Recursive
# =================================================================================
function Approve-WebContent($web)
{
	# Check if its a publishing web 
	if ([Microsoft.SharePoint.Publishing.PublishingWeb]::IsPublishingWeb($web) -eq $true)
	{
		# Now Update all lists and libraries in this web
		Write-Host -ForegroundColor Green "Approving web contents - URL : " $web.Url
		
		# Approve content in all lists and libraries
		$web.Lists | ForEach-Object{
		
			# Check if this is a document library
			if($_.GetType() -eq [Microsoft.SharePoint.SPDocumentLibrary])
			{
				# Check if version history is enabled for this library
				if($_.EnableVersioning)
				{
					Approve-LibraryContent $_
				}
			}
			
		}
	}
}

# =================================================================================
# FUNC: ReadConfig
# DESC: Read Config File
# =================================================================================
function script:ReadConfig([string]$filePath)
{
	[System.Xml.XmlDocument] $config = new-object System.Xml.XmlDocument
    [string] $configFileName = Get-Location

	$config.Load($configFileName)
    
    if( $? -eq $false ) 
	{
        Write-Host "Invalid config file" -ForegroundColor Red
        return;
    }
    return $config;
}

function SetProperty([Microsoft.SharePoint.SPWeb]$web, [string]$key, [string]$value)
{
	if($web.AllProperties.ContainsKey($key) -eq $false)
	{
		$web.AllProperties.Add($key, $value)
	}
	else
	{
		$newWeb.AllProperties[$key] = $value
	}
}
# =================================================================================
# FUNC: CreateChildWebs
# DESC: Recursive function to create subsites
# =================================================================================
function CreateChildWebs($parentSiteNode, [string]$siteURL, [string]$cultureString, [string]$addToLocaleList, $approveContents )
{
    foreach ($subsite in $parentSiteNode)
   	{
        $ssName = $subsite.RelativeURL
        $url = $siteURL + "/" + $subsite.RelativeURL
        
		$newWeb = Get-SPWeb -Identity $url -ErrorAction SilentlyContinue
		if($newWeb -eq $null)
		{
			Write-Host "Create subsite at url " $url -ForegroundColor Green
			$Error.Clear()
			
			if($subsite.isWebTemplate -eq "false")
			{
    			$newWeb = New-SPWeb -Url $url -Description $subsite.Description -Language 1033 -AddToTopNav:$false -Name $subsite.Name -Template $site.template -UniquePermissions:$false -UseParentTopNav:$true -WarningAction SilentlyContinue
			}
			else
			{
				$w = get-spweb $webURL 
				$template = $w.GetAvailableWebTemplates(1033) | ? { $_.name -eq $subsite.template }			
				$w.Webs.Add($subsite.RelativeURL, $subsite.Name, $subsite.Description, 1033, $template, $false, $false)
			}

			if($Error.Count -gt 0)
			{
				Write-Host "Error: Creating web at Url $url: Exiting Script..." -ForegroundColor Red
				Return
			}
		
			if($Error.Count -gt 0)
			{
				Write-Host "Error: Updating Locale Settings at $url: Exiting Script..." -ForegroundColor Red
				Return
			}
		}
		else
		{
			Write-Host "Subsite exist at url " $url -ForegroundColor Green
		}
        if ($subsite.Features -ne $null)
		{
			foreach ($feature in $subsite.Features.Feature)
			{
				if((Get-SPFeature -Web $url | ? { $_.id -eq $feature.id }) -eq $null )
				{
		    		Write-Host "Activating feature $($feature.Name)" -ForegroundColor Green
		    		Enable-SPFeature -Identity $feature.id -Url $url -Force
					if($Error.Count -gt 0)
					{
						Write-Host "Error: Activating feature $($feature.Name)" -ForegroundColor Red
						break
					}
					Write-Host "Success: Activating feature $($feature.Name)" -ForegroundColor Green
				}
				else
				{
					Write-Host "Warning: Feature already activated $($feature.Name)" -ForegroundColor Green
				}
			}
		}	
		if($Error.Count -gt 0)
		{
			Return
		}

		# Publish the files now
		if ($approveContents -eq "Yes")
		{
			Approve-WebContent($newWeb);
		}

		if($Error.Count -gt 0)
		{
			Write-Host "Error: Approving the Contents: Exiting Script..." -ForegroundColor Red
			Return
		}		
	}
}

function script:ErrorExit()
{
    exit 1
}

function CreateInfoArch([String]$ConfigFileName = "")
{
	#read the xml file
	Write-Host "Reading config file " $args[0] "..." -ForegroundColor Green

	$config = [xml]$(get-content $configFileName)

	if($config -eq $null) #invalid or no configuration found
	{
    	ErrorExit
	} 

	#get webapplication Node from Configuration file
	$webApplication = $Configuration.Configuration.Farm.WebApplications.Webapplication | Where-Object {$_.name -match $config.Root.WebApplicationName}
	
	#get webapplication Node from Configuration file
	$site = $webApplication.Sites.Site | Where-Object {$_.Title -match $config.Root.SiteCollection.Name}

	$rootSite = Get-SPSite -Identity $site.Path
	$rootSite.OpenWeb(); 
	$openWeb = $rootSite.RootWeb; 
	# Publish the files now
	if ($approveContents -eq "Yes")
	{
		Approve-WebContent($openWeb);
	}
	$webURL = $rootSite.URL
	$openWeb.Dispose()
	$rootSite.Dispose()

	if($Error.Count -gt 0)
	{
		Write-Host "Error: Approving the Contents on Root Site: Exiting Script..." -ForegroundColor Red
		ErrorExit
	}

	if ($config.HasChildNodes)
	{
		ForEach($site in $config.SelectNodes("Root/SiteCollection/Site"))
		{   
	    	$siteRelativeURL = $site.RelativeURL
	    	$url = $webURL + "/" + $siteRelativeURL
			
	    	Write-Host $url -ForegroundColor Green
			$newWeb = Get-SPWeb -Identity $url -ErrorAction SilentlyContinue 
			if($newWeb -eq $null)
			{
				$Error.Clear()
				if($site.isWebTemplate -eq "false")
				{
	    			$newWeb = New-SPWeb -Url $url -Description $site.Description -Language 1033 -AddToTopNav:$false -Name $site.Name -Template $site.template -UniquePermissions:$false -UseParentTopNav:$true -WarningAction SilentlyContinue
				}
				else
				{
					$w = get-spweb $webURL 
					$template = $w.GetAvailableWebTemplates(1033) | ? { $_.name -eq $site.template }
					$w.Webs.Add($site.RelativeURL, $site.Name, $site.Description, 1033, $template, $false, $false)
				}
			}
		
			if($Error.Count -gt 0)
			{
				Write-Host "Error: Creating web at Url $url: Exiting Script..." -ForegroundColor Red
				Break
			}

			if ($site.Features -ne $null)
			{
				foreach ($feature in $site.Features.Feature)
				{
					if((Get-SPFeature -Web $url | ? { $_.id -eq $feature.id }) -eq $null )
					{
			    		Write-Host "Activating feature $($feature.Name)" -ForegroundColor Green
			    		Enable-SPFeature -Identity $feature.id -Url $url -Force
						if($Error.Count -gt 0)
						{
							Write-Host "Error: Activating feature $($feature.Name)" -ForegroundColor Red
							break
						}
						Write-Host "Success: Activating feature $($feature.Name)" -ForegroundColor Green
					}
					else
					{
						Write-Host "Warning: Feature already activated $($feature.Name)" -ForegroundColor Green
					}
				}
			}
			if($Error.Count -gt 0)
			{
				Break
			}

			# Publish the files now
			if ($approveContents -eq "Yes")
			{
				Approve-WebContent($newWeb);
			}

			if($Error.Count -gt 0)
			{
				Write-Host "Error: Approving the Contents: Exiting Script..." -ForegroundColor Red
				Break
			}
	     
		    if ($site.HasChildNodes -eq $true)
		    {
		        $childSites = $site.SelectNodes("Site")
		        CreateChildWebs $childSites  $url $cultureString $addToLocaleList $approveContents
				if($Error.Count -gt 0)
				{
					Write-Host "Error: Creating Child Webs: Exiting Script..." -ForegroundColor Red
					Break
				}
		    }
		}	
	}
}
