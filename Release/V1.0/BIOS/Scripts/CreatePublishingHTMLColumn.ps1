function CreateColumn()
{
 
#Assign fieldXML variable with XML string for site column  
  
$fieldXML = '<Field Type="HTML"  
Name="'+ $Name + '"  
Description="'+ $Name + '"  
DisplayName="' + $DisplayName + '"  
StaticName="'+ $Name + '"  
Group="Custom Columns"  
Hidden="FALSE"  
Required="FALSE"  
Sealed="FALSE"  
ShowInDisplayForm="TRUE"  
ShowInEditForm="TRUE"  
ShowInListSettings="TRUE"  
ShowInNewForm="TRUE"
RichText="TRUE"
RichTextMode="ThemeHtml"></Field>'  
 
#Output XML to console  
write-host $fieldXML
 
#Create site column from XML string  
$web.Fields.AddFieldAsXml($fieldXML)  
 
 
#Dispose of Web and Site objects  
$web.Dispose() 
} 

function UpdateHypelinkColumn()
{
  $w = Get-SPWeb "http://sp2013-dev-12:45704/sites/Asia-Bios"
  $f = ($w.Fields["SmallBiosImage"]) 
  write-host $f.DisplayFormat;
  $f.DisplayFormat = SPUrlFieldFormatType.Image;
  $f.update()
  $w.Dispose()

  Write-Host "Column has been created" -ForegroundColor Green
  Write-Output "Column has been created"
}


function CallCreateColumn([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)
	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
		Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
	Write-Output "Sucessfully read config file $ConfigPath file"

	if($Error.Count -eq 0)
	{
		$webAppFile = $cfg
		$error.clear()
		$sites = $webAppFile.Sites
		foreach($site in $Sites.Site)	
		{
		# Get the list for operation to perform
		$siteUrl= $site.SiteUrl
		$Name = $site.ColumnName
		$DisplayName = $site.ColumnDisplayName

		#****Give the site url where one want to update page properties***** 

		$site = get-spSite $siteUrl
        $web = $site.RootWeb

		###Calling function
		CreateColumn
		}
	}
}