﻿Add-PSSnapin Microsoft.SharePoint.Powershell

function CreateMediaSiloConfigList([String]$configFileName = "")
{
    [string] $currentLocation = Get-Location

    Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green

    # Check that the config file exists.
    if (-not $(Test-Path -Path $configFileName -Type Leaf))
    {
	    Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
    }

    $configXml = [xml]$(get-content $configFileName)
    if( $? -eq $false ) 
    {
	    Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
    }

    if ($configXml.Site)
    {
        # Get Site Collection and root web
        $site = Get-SPSite $configXml.Site.SiteUrl
        [Microsoft.SharePoint.SPWeb]$web = $site.RootWeb
        $ListName = $configXml.Site.List.Name
        $ListDescription = $configXml.Site.List.Description
        $ListTemplate = $configXml.Site.List.ListTemplate
        $Fields = $configXml.Site.List.Fields
        $Rows = $configXml.Site.List.Rows
        Write-Host "Creating $ListName list..." -ForegroundColor Yellow
        CreateList $web $ListName $ListDescription $ListTemplate $Fields $Rows
           
		$web.Dispose()
		$site.Dispose()
    }

}

function CreateList([Microsoft.SharePoint.SPWeb] $web, $ListName, $ListDescription, $ListTemplate, $Fields, $Rows)
{   
    $spListCollection = $web.Lists
    $spList = $spListCollection.TryGetList($ListName)
    if($spList -ne $null)
    {
        Write-Host "List $ListName already exists in the site" -ForegroundColor Yellow
        }
    else
    {
        $spTemplate = $web.ListTemplates[$ListTemplate] 	 
	    $lstId = $spListCollection.Add($ListName, $ListDescription, $spTemplate)
        Write-Host "List $ListName is created successfully" -ForegroundColor Green
	    $spList = $web.Lists[[System.GUID]($lstId)]
        $titleColumn = $spList.Fields["Title"]
        #Set Title to optional and hidden
        $titleColumn.Required = $false
        $titleColumn.Hidden = $true
        #Update Title column and list
        $titleColumn.Update()
        $spList.Update()
	    foreach ($field in $Fields.Field)
	    {	
		    $spList.Fields.AddFieldAsXml($field.OuterXml, $true,[Microsoft.SharePoint.SPAddFieldOptions]::AddFieldToDefaultView)            
	    }
        $spList.Update()
        Write-Host "Fields are created in $ListName list" -ForegroundColor Green
        Write-Host "Inserting row data in $ListName list..." -ForegroundColor Yellow
        # inserting row's data 
        InsertingRows $web $spList $Rows
        Write-Host "All rows are successfully added in $ListName list..." -ForegroundColor Green
    }
}

function InsertingRows($web, $spList, $Rows)
{
    foreach($row in $Rows.Row)
    {
        #Create a new item
        $newItem = $spList.Items.Add()
  
        #Add properties to this list item
        $newItem["KEY"] = $row.KEY
        $newItem["VALUE"] = $row.VALUE
  
        #Update the object so it gets saved to the list
        $newItem.Update()
    }
}