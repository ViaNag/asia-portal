$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}
#Create Custom Permission Level 
function ManagePermissionLevel([String]$ConfigFileName = "")
{

# Check that the config file exists.
	if (-not $(Test-Path -Path $configFileName -Type Leaf))
	{
		Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
	}

	$configXml = [xml]$(get-content $configFileName)
	if( $? -eq $false ) 
	{
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
	}
	$web = Get-SPWeb $configXml.GlobalConfiguration.Site.Url
	 
	#Create New Permission Level
	foreach($p in $configXml.GlobalConfiguration.Site.Permissions.Permission)
	{
		$ContributeNoDelete =New-Object Microsoft.SharePoint.SPRoleDefinition
		$ContributeNoDelete.Name=$p.Name
		$ContributeNoDelete.Description=$p.Description
		$ContributeNoDelete.BasePermissions=$p.PermissionSet
		$web.RoleDefinitions.Add($ContributeNoDelete);
		write-host $ContributeNoDelete.Name
		write-host "Permission level created successfully"
	}
}
