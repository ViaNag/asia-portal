﻿$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

function ShowHideFields([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$lookUpXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green      

    $sitecollectionUrl =  $lookUpXml.SiteCollection.SiteUrl

    # Get Site Collection URL
    $siteCollection = Get-SPSite $sitecollectionUrl
    
    
    
    $rootWeb = $siteCollection.RootWeb 	
    
	Write-Host "In the Site Collection"  $web.url  

    $lookUpXml.SiteCollection.Subsite |
	ForEach-Object {  

        $Subsite = $_.SubSiteUrl
        
        $web = Get-SPWeb -Identity $Subsite	

        $_.mapping |
	    ForEach-Object {
	    		
           $listName = $_.ListName
           $columnName = $_.Column
           $showInDisplayForm = $_.ShowInDisplayForm
           $showInNewForm = $_.ShowInNewForm
           $showInEditForm = $_.ShowInEditForm
           
           try
           {
                $list = $web.Lists[$listName]
                $column = $list.Fields.GetFieldByInternalName($columnName)

                $column.ShowInDisplayForm = [System.Convert]::ToBoolean($showInDisplayForm)
                $column.ShowInNewForm = [System.Convert]::ToBoolean($showInNewForm)
                $column.ShowInEditForm = [System.Convert]::ToBoolean($showInEditForm)
                $column.Update()
                


                Write-Host $column "updated successfully in List " $listName  -ForegroundColor Green
           }
           catch
           {
                Write-Host $list "not found" in $web -ForegroundColor Red
           }            
        }
    }
        
	$rootWeb.Dispose()				
}
