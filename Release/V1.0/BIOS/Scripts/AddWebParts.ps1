# =================================================================================
#
#Main Function to Add web Parts to publishing page
#
# =================================================================================
function AddWebpartsToPage([string]$ConfigPath = "")
{
    $Error.Clear();
    
	$cfg = [xml](get-content $ConfigPath)
   
    if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
	{    
		  Add-PsSnapin Microsoft.SharePoint.PowerShell
	}
    
	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $($ConfigPath) file"
	if($Error.Count -eq 0)
	{
        try
        {
            $Error.Clear()
            foreach($webpart in $cfg.Webparts.RemoveWebpart)
            {
                RemoveWebPartToPage $webpart
            }
            $Error.Clear()
            foreach($webpart in $cfg.Webparts.Webpart)
            {
                AddWebPartToPage $webpart
            }            
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host $Error -ForegroundColor Red
        Write-Output $Error
	}

} 

# =================================================================================
#
# FUNC: AddWebPartPublish
# DESC: Add search query ,list view and Members Web Part to given publishing page .
# =================================================================================
function  AddWebPartToPage([object] $webpart)
{		
	$siteUrl=$webpart.SiteURL
	$pagePath=$webpart.PagePath
	$calendarWP=[System.Convert]::ToBoolean(@{$true="false";$false=$webpart.CalendarWP}[$webpart.CalendarWP -eq ""])
	$webpartZone=$webpart.WebpartZone
	$index= $webpart.Index
    $webpartTitle=$webpart.Title
	$JSLink=$webpart.JSLink 
    [boolean]$SiteUserWP=[System.Convert]::ToBoolean(@{$true="false";$false=$webpart.IsSiteUserWP}[$webpart.IsSiteUserWP -eq ""])
	[boolean]$isContentSearchWebpart=[System.Convert]::ToBoolean(@{$true="false";$false=$webpart.IsContentSearchWebpart}[$webpart.IsContentSearchWebpart -eq ""])
    $DisplayType=$webpart.DisplayType
	
	$pageUrl = $siteUrl + $pagePath
	$spWeb = Get-SPWeb $siteUrl -ErrorAction Stop
    $site=$spWeb.Site

	[Microsoft.SharePoint.Publishing.PublishingWeb]$pubWeb = [Microsoft.SharePoint.Publishing.PublishingWeb]::GetPublishingWeb($spWeb);
	$allowunsafeupdates = $spWeb.AllowUnsafeUpdates
	$spWeb.AllowUnsafeUpdates = $true

	$page = $spWeb.GetFile($pageUrl);
	if ($page.Level -eq [Microsoft.SharePoint.SPFileLevel]::Checkout)
	{ 
	  if ($page.CheckedOutBy.UserLogin -eq $spWeb.CurrentUser.UserLogin) 
	  {
		Write-Host "Page has already been checked out " -ForegroundColor Yellow
		Write-Output "Page has already been checked out "
		
	  } 
	  else 
	  {
		$SPWeb.CurrentUser.LoginName
		$page.UndoCheckOut()
		$page.CheckOut()
		Write-Host "Check out the page override" -ForegroundColor Yellow
		Write-Output "Check out the page override"
	  }  
	  
	}
	else
	{
	   $page.CheckOut() 
	   Write-Host "Check out the page" -ForegroundColor Green
	   Write-Output "Check out the page"
	}
	
	try
	{
		#Initialise the Web part manager for the specified profile page.
		$spWebPartManager = $spWeb.GetLimitedWebPartManager($pageUrl, [System.Web.UI.WebControls.WebParts.PersonalizationScope]::Shared)

		if ($isContentSearchWebpart)
		{
            $searchScope=$webpart.SearchScope
			$sspApp = Get-SPEnterpriseSearchServiceApplicationProxy $webpart.SearchServiceName 
            $fedManager = New-Object Microsoft.Office.Server.Search.Administration.Query.FederationManager($sspApp)   
            $searchOwner = New-Object Microsoft.Office.Server.Search.Administration.SearchObjectOwner([Microsoft.Office.Server.Search.Administration.SearchObjectLevel]::$searchScope, $site.RootWeb)
            $source = $fedManager.GetSourceByName($webpart.ResultSourceName, $searchOwner)            

			$contentSearchWebpart=New-Object Microsoft.Office.Server.Search.WebControls.ContentBySearchWebPart
			$contentSearchWebpart.GroupTemplateId= $webpart.GroupTemplateId
			$contentSearchWebpart.ItemTemplateId=$webpart.ItemTemplateId
			$contentSearchWebpart.RenderTemplateId= $webpart.RenderTemplateId
			$contentSearchWebpart.ResultsPerPage=$webpart.ResultsPerPage
			$contentSearchWebpart.Title=$webpartTitle

            $dataProviderJSON=$contentSearchWebpart.DataProviderJSON

			$dataProviderJSON=$dataProviderJSON.Replace("{searchboxquery}",$webpart.SearchQuery)
            $dataProviderJSON=$dataProviderJSON.Replace("`"SourceID`":null","`"SourceID`":`"" + $source.Id + "`"")
			$dataProviderJSON=$dataProviderJSON.Replace("`"FallbackSort`":null","`"FallbackSort`":" +$webpart.FallbackSort )
			$dataProviderJSON=$dataProviderJSON.Replace("`"FallbackSortJson`":`"null`"","`"FallbackSortJson`":`"" + $webpart.FallbackSortJson + "`"")
            $contentSearchWebpart.DataProviderJSON=$dataProviderJSON
			$spWebPartManager.AddWebPart($contentSearchWebpart, $webpartZone, $index)
			$spWebPartManager.SaveChanges($contentSearchWebpart)
			
            Write-Host "Webpart $webpartTitle added successfully" -ForegroundColor Green
	        Write-Output "Webpart $webpartTitle added successfully"
		} 
		else
		{   if(!$SiteUserWP)  
            {               
			    $list= $spWeb.Lists[$webpart.ListName];
			    If($list -ne $null )
			    {
				    Write-Host "The list named $list is existing" -ForegroundColor Green
				    Write-Output "The list named $list is existing"
					$listViewWebPart=$null
					if($calendarWP)
					{
						$listViewWebPart = New-Object Microsoft.SharePoint.WebPartPages.ListViewWebPart
					}
					else
					{
						$listViewWebPart = New-Object Microsoft.SharePoint.WebPartPages.XsltListViewWebPart
					}
				    $listViewWebPart.Title = $webpartTitle
				    $listViewWebPart.ListName = ($list.ID).ToString("B").ToUpper()
					$listViewWebPart.JSLink = $JSLink
				    $view = $list.Views[$webpart.ViewName];
				    $listViewWebPart.ViewGuid = $view.ID.ToString("B").ToUpper();
                 
				    #$listViewWebPart.ChromeType = "none"
				    $spWebPartManager.AddWebPart($listViewWebPart, $webpartZone, $index)
				    $spWebPartManager.SaveChanges($listViewWebPart)
                    Write-Host "Webpart $webpartTitle added successfully" -ForegroundColor Green
	                Write-Output "Webpart $webpartTitle added successfully" 
			    }
                Else
			    {
				     Write-Host "The list named $list is not existing" -ForegroundColor Red
				     Write-Output "The list named $list is not existing"
			    }
            }
            else
            {
                $MemberWP=New-Object Microsoft.SharePoint.WebPartPages.MembersWebPart
                $MemberWP.Title=$webpartTitle
                $GroupId=$null
                if(![string]::IsNullOrEmpty($webpart.Group))
                {
                    $GroupId=$spWeb.SiteGroups[$webpart.Group]
                }
                if($GroupId)
                {
                    $MemberWP.MembershipGroupId=$GroupId.ID
                }
                if(![string]::IsNullOrEmpty($DisplayType))
                {
                    if($DisplayType -eq "GroupMembership")
                    {
                        $MemberWP.DisplayType=[Microsoft.SharePoint.WebpartPages.MembersWebpartDisplayType]::GroupMembership
                    }
                    elseif($DisplayType -eq "WebMemberGroup")
                    {
                        $MemberWP.DisplayType=[Microsoft.SharePoint.WebpartPages.MembersWebpartDisplayType]::WebMemberGroup
                    }
                    elseif($DisplayType -eq "WebUserGroups")
                    {
                        $MemberWP.DisplayType=[Microsoft.SharePoint.WebpartPages.MembersWebpartDisplayType]::WebUserGroups
                    }
                }
                $spWebPartManager.AddWebPart($MemberWP, $webpartZone, $index)
				$spWebPartManager.SaveChanges($MemberWP)
                Write-Host "Webpart $webpartTitle added successfully" -ForegroundColor Green
	            Write-Output "Webpart $webpartTitle added successfully"
            }    
			                     
		} 
				

		#Check to ensure the page is checked out by you, and if so, check it in    
		if ($page.CheckedOutBy.UserLogin -eq $spWeb.CurrentUser.UserLogin)    
		{        
			$page.CheckIn("Page checked in automatically by PowerShell script") 
			Write-Host "Page has been checked in" -ForegroundColor Green
			Write-Output "Page has been checked in"    
		}
        $error.Clear()
        $ErrorActionPreference = "SilentlyContinue"
		$page.Publish("Published")
        $ErrorActionPreference="Continue"
        if($error.Count > 0)
        {
            Write-Host "Page need not required to be published because of following reason : $(Error)" -ForegroundColor Yellow
            Write-Output "Page need not required to be published because of following reason : $(Error)"
        }
		Write-Host "Page has been published success" -ForegroundColor Green
		Write-Output "Page has been published success"	
		$pubWeb.Close()
		$spWeb.Update()
		$spWeb.AllowUnsafeUpdates = $allowunsafeupdates 
		$spWeb.Dispose() 
							
	}
	catch
	{
		Write-Host "(ERROR : "$_.Exception.Message")" -ForegroundColor Red
		Write-Output "(ERROR : "$_.Exception.Message")"
	} 
}

# =================================================================================
#
# FUNC: RemoveWebPartToPage
# DESC: Remove WebPart from  given  publishing page .#
# =================================================================================
function  RemoveWebPartToPage([object] $webpart)
{		
	$siteUrl=$webpart.SiteURL
	$pagePath=$webpart.PagePath
    $webpartTitle=$webpart.Title
	
	$pageUrl = $siteUrl + $pagePath
	$spWeb = Get-SPWeb $siteUrl -ErrorAction Stop
    $site=$spWeb.Site

	[Microsoft.SharePoint.Publishing.PublishingWeb]$pubWeb = [Microsoft.SharePoint.Publishing.PublishingWeb]::GetPublishingWeb($spWeb);
	$allowunsafeupdates = $spWeb.AllowUnsafeUpdates
	$spWeb.AllowUnsafeUpdates = $true

	$page = $spWeb.GetFile($pageUrl);
	if ($page.Level -eq [Microsoft.SharePoint.SPFileLevel]::Checkout)
	{ 
	  if ($page.CheckedOutBy.UserLogin -eq $spWeb.CurrentUser.UserLogin) 
	  {
		Write-Host "Page has already been checked out " -ForegroundColor Yellow
		Write-Output "Page has already been checked out "
		
	  } 
	  else 
	  {
		$SPWeb.CurrentUser.LoginName
		$page.UndoCheckOut()
		$page.CheckOut()
		Write-Host "Check out the page override" -ForegroundColor Yellow
		Write-Output "Check out the page override"
	  }  
	  
	}
	else
	{
	   $page.CheckOut() 
	   Write-Host "Check out the page" -ForegroundColor Green
	   Write-Output "Check out the page"
	}
	
	try
	{
		#Initialise the Web part manager for the specified profile page.
		$spWebPartManager = $spWeb.GetLimitedWebPartManager($pageUrl, [System.Web.UI.WebControls.WebParts.PersonalizationScope]::Shared)
        foreach($removeWP in $spWebPartManager.Webparts)
        {
            if($removeWP.Title -eq $webpartTitle)
            {
                $spWebPartManager.DeleteWebPart($removeWP)
            }
        }

		#Check to ensure the page is checked out by you, and if so, check it in    
		if ($page.CheckedOutBy.UserLogin -eq $spWeb.CurrentUser.UserLogin)    
		{        
			$page.CheckIn("Page checked in automatically by PowerShell script") 
			Write-Host "Page has been checked in" -ForegroundColor Green
			Write-Output "Page has been checked in"    
		}
        $error.Clear()
        $ErrorActionPreference = "SilentlyContinue"
		$page.Publish("Published")
        $ErrorActionPreference="Continue"
        if($error.Count > 0)
        {
            Write-Host "Page need not required to be published because of following reason : $(Error)" -ForegroundColor Yellow
            Write-Output "Page need not required to be published because of following reason : $(Error)"
        }
		Write-Host "Page has been published success" -ForegroundColor Green
		Write-Output "Page has been published success"
		
		$pubWeb.Close()
		$spWeb.Update()							
		$spWeb.AllowUnsafeUpdates = $allowunsafeupdates 
		$spWeb.Dispose() 
	}
	catch
	{
		Write-Host "(ERROR : "$_.Exception.Message")" -ForegroundColor Red
		Write-Output "(ERROR : "$_.Exception.Message")"
	} 
}