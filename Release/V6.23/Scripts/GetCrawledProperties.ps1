﻿# ================================================================================================
# Get Crawled Property
# ================================================================================================
# ================================================================================================
# Reading configuration file and performing operations accrodingly
# ================================================================================================
function GetCrawledProperty([string]$ConfigPath = "")
{
    $CraedPropertyXml=[xml](get-content $ConfigPath)
	if($? -eq $false) 
	{
		Write-Host "Could not read GetCrawledProperty.xml file. Exiting ..." -ForegroundColor Red`
        Write-Output "Could not read GetCrawledProperty.xml file. Exiting ..."
	}
	else
    {
		Write-Host "Read GetCrawledProperty.xml file successfully....." -ForegroundColor Green`
		Write-Output "Read GetCrawledProperty.xml file successfully...."
		try
		{
			Write-Output "Reading crawled property..."
			Write-Host "Reading crawled property..." -ForegroundColor Green`

			$serviceApplication=$CraedPropertyXml.CrawledProperties.CrawledProperty.SearchServiceName
			$category=$CraedPropertyXml.CrawledProperties.CrawledProperty.Category
			$searchapp = Get-SPEnterpriseSearchServiceApplication -Identity $serviceApplication
			$cat = Get-SPEnterpriseSearchMetadataCategory -SearchApplication $searchapp -Identity $category

			$filePath = GetUniqueFileNameWithCurrentPath "CrawlProperty" "yyyy-MM-dd_hhmmss" "xml"
			$content = "<?xml version=""1.0"" encoding=""utf-8"" ?>" + `
						"<CrawlProperties><SearchServiceApplication>" + $serviceApplication + "</SearchServiceApplication>"
			WriteToFile $filePath $content

			$crawlProperties = Get-SPEnterpriseSearchMetadataCrawledProperty -SearchApplication $searchapp -Category $cat

			foreach($crawlProperty in $crawlProperties)
			{
				$crawlPropertyName = [System.Security.SecurityElement]::Escape($crawlProperty.Name)
				Write-Host Reading $crawlPropertyName and its property

				$content = [String]::Format("<CrawlProperty Name = {0} CategoryName = {1} Propset = {2}" + `
				" IsMappedToContents = {3} IsNameEnum = {4} SchemaId = {5}" + `
				" VariantType = {6}></CrawlProperty>", `
				"`"$($crawlPropertyName)`"", "`"$($crawlProperty.CategoryName)`"", "`"$($crawlProperty.Propset)`"", "`"$($crawlProperty.IsMappedToContents)`"", "`"$($crawlProperty.IsNameEnum)`"",`
				"`"$($crawlProperty.SchemaId)`"", "`"$(GetCrawledPropertyType $crawlProperty.VariantType $crawlPropertyName)`"")

				WriteToFile $filePath $content
			}

			$content = "</CrawlProperties>"
			WriteToFile $filePath $content

			Write-Host "Exported to '$filePath'" -ForegroundColor Green`
		}
		catch 
		{
			Write-Output "Exception while fetching crawled property " $Error
			Write-Host "Exception while fetching crawled property" $Error -ForegroundColor Red`
		}
      }
}


# ===================================================================================
# Getting unique output xml filepath
# ===================================================================================
function GetUniqueFileNameWithCurrentPath([string]$prefix, [string]$fileFormat, [string]$extention)
{
   try
   {
     
	 Write-Host "Getting filepath ....." -ForegroundColor Green`
    $currentPath = Get-Location | select Path
    $fileName = [datetime]::Now.ToString($fileFormat)
    $filePath = $currentPath.Path + "\" + $prefix + "_" + $fileName + "." + $extention
    return $filePath
    }
    catch
    {
     Write-Output "Exception while getting filepath" $Error
	 Write-Host "Exception while getting filepath" $Error -ForegroundColor Red`
    }
     
    return $filePath
}

# ===================================================================================
# Function to write to file
# ===================================================================================
function WriteToFile
{
    param([string]$fileName, [string]$content)
     
    $fileObj = New-Object System.IO.StreamWriter($fileName, $true)
    try
    {
        $fileObj.WriteLine($content)
    }
    finally
    {
        if($fileObj -ne $null)
        {
            $fileObj.Dispose()  
        }
    }
}

# ===================================================================================
# Getting type of crawled properties from its variant type
# ===================================================================================
function GetCrawledPropertyType([string]$variantType, [string]$crawledPropertyName)
{
    try
    {
     $type = ""
        Switch ($variantType)
          {
           31 { $type= "Text" }
           20 { $type = "Integer" }
           5 { $type = "Decimal"}
           64 { $type= "DateTime" }
           11 { $type ="YesNo" }
           default { $type ="Text" }

          }

          return $type
      }
      catch
      {
       Write-Output "Exception while getting type from variant type for $crawledPropertyName" $Error
	   Write-Host "Exception while getting type from variant type for $crawledPropertyName" $Error -ForegroundColor Red`
      }
}
 
