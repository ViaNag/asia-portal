var check = false;
$(document).ready(function () {
	var oldSubmitClickText = $(".divButtonControls input[value='Save']").attr('onclick');
	var colonIndex = oldSubmitClickText.indexOf(':');
	var submitClickTextSub = oldSubmitClickText.substring(colonIndex + 1)
	var newSubmitClickText = "checkPhoneFormat();if(check){" + submitClickTextSub + ";}else{return false;}";
	$(".divButtonControls input[value='Save']").attr('onclick', newSubmitClickText); 
	});
	
	function checkPhoneFormat()
	{
		$("#customError").hide();
		check = true;
		var phone = $("input[title='Phone Number Required Field']");
		var phoneNumber = phone.val();
		if(phoneNumber != "")
		{ 
			var first = phoneNumber.substring(0,1);
			if(phoneNumber.length != 8){
			WriteErrorMessage(phone, "Enter a valid mobile number");
			check = false;
			}
			
			else if(first != "9" && first != "8"){
				
				WriteErrorMessage(phone, "Enter a valid mobile number");
				check = false;
			}
		}
		
	}
	
	function WriteErrorMessage(inputElement, message){

	   var errorMessageMarkup = '<br/><span class="errorMessage ms-formvalidation ms-csrformvalidation" id="customError"><span role="alert">' + message + '<br></span></span>';
	   $(inputElement).parent().append(errorMessageMarkup);

}