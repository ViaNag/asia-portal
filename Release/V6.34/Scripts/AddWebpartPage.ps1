﻿# =================================================================================
#
#Main Function to Add pages for given page layouts
#
# =================================================================================
function CreateSitePages([string]$ConfigPath = "")
{
    $Error.Clear();
	$cfg = [xml](get-content $ConfigPath) 
	
    if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
	{    
		  Add-PsSnapin Microsoft.SharePoint.PowerShell
	}

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $($ConfigPath) file"
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
            foreach($page in $cfg.Pages.Page)
            {
                CreateSitePage $page
            }
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host $Error -ForegroundColor Red
        Write-Output $Error
	}
}


# =================================================================================
#
# FUNC: CreatePublishingPage
# DESC: Create publishing page for given page layout .
# =================================================================================
function CreateSitePage([object] $pageXmlObj)
{    
	try
    {
        $error.Clear()		

		# Get the SiteURL
		$webUrl = $pageXmlObj.WebURL

        #Get the LibURL
        $LibURL=$pageXmlObj.LibURL
			
		# Get the PageLayout URL
		$layoutTemplate = $pageXmlObj.PageLayoutCode
		
		
		# Get the Title of the Page which is going to get created
		$pageTitle = $pageXmlObj.PageTitle
		
		
$web = Get-SPWeb $webUrl 
$list = $web.GetList($LibURL)
$xml = "<?xml version=""1.0"" encoding=""UTF-8""?><Method ID=""0,NewWebPage""><SetList Scope=""Request"">" + $list.ID + "</SetList><SetVar Name=""Cmd"">NewWebPage</SetVar><SetVar Name=""ID"">New</SetVar><SetVar Name=""Type"">WebPartPage</SetVar><SetVar Name=""WebPartPageTemplate"">" + $layoutTemplate + "</SetVar><SetVar Name=""Overwrite"">true</SetVar><SetVar Name=""Title"">" + $pageTitle + "</SetVar></Method>"
$result = $web.ProcessBatchData($xml)
Write-Host "Page Added Successfully $($pageTitle)" -ForegroundColor Green
# Possible LayoutTemplate values:
# 1 - Full Page, Vertical
# 2 - Header, Footer, 3 Columns
# 3 - Header, Left Column, Body
# 4 - Header, Right Column, Body
# 5 - Header, Footer, 2 Columns, 4 Rows
# 6 - Header, Footer, 4 Columns, Top Row
# 7 - Left Column, Header, Footer, Top Row, 3 Columns
# 8 - Right Column, Header, Footer, Top Row, 3 Columns

}
    catch
    {
        Write-Host "`nException for page :" $pageXmlObj.PageTitle "`n" $Error -ForegroundColor Red
        Write-Output "`nException for page :" $pageXmlObj.PageTitle "`n" $Error
    }
}