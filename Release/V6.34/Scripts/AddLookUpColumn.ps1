if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
{    
      Add-PSSnapin Microsoft.SharePoint.PowerShell
}

function AddLookUpColumn()
{ 
#Check if lookup already exists then delete
if($Web.Fields.ContainsField($LookupColumnInternalName)){
    Write-host "Deleting existing site column..." + $displayName
    $Web.Fields.Delete($LookupColumnInternalName)
    $Web.Update();
}
	$addLookupResult = $Web.Fields.AddLookup($LookupColumnInternalName, $SourceList.ID, $SourceWeb.ID, $false);
	$addLookupField = $Web.Fields.GetFieldByInternalName($addLookupResult);
	$addLookupField.Title = $LookupColumnDisplayName;
	$addLookupField.LookupField = $SourceList.Fields.GetFieldByInternalName($SourceLookupColumnInternalName).InternalName;
	$addLookupField.Update();
	Write-Host New site column added.
	Write-Host
	$Web.Dispose()
}


function LookUpColumnDetails([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)
	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
	if($Error.Count -eq 0)
	{
		$webAppFile = $cfg
		$error.clear()
		$sites = $webAppFile.Sites
		foreach($site in $Sites.Site)	
		{
			#Where the Source List for Lookup Exists
			$SourceWebURL=	$site.SourceSiteUrl
			$SourceListName= $site.SourceListName
			$SourceLookupColumnInternalName= $site.TargetLookupField
			
			#Where the Lookup Site column Going to get created
			$WebURL= $site.SiteUrl
			$LookupColumnDisplayName= $site.DisplayName
			$LookupColumnInternalName = $site.InternalName
						
			#Get the Source and Target Webs and List
			$SourceWeb = Get-SPWeb $SourceWebURL
			$SourceList = $SourceWeb.Lists[$SourceListName]
			$Web = Get-SPWeb $WebURL
		

		###Calling function
		AddLookUpColumn
		}
	}
}