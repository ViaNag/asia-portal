﻿# ===================================================================================
# Log output to CSV file
# ===================================================================================
function WriteCSVLog($string)
{
   $string | out-file -Filepath $logCSVLocation -Encoding ASCII -append
}

# ===================================================================================
# Function to Delete Files of Library in Batch
# ===================================================================================

function DeleteFilesInBatch([string]$ConfigPath = "")
{
    $currentPath=Get-Location
    $logCSVLocation=[string]$currentPath + "\Configuration\AdSales\DeleteFilesInBatch$(get-date -format `"yyyyMMdd_hhmmsstt`").csv"

    $error.Clear()
    $cfg = [xml](get-content $ConfigPath)

    # Exit if config file is invalid
    if( $? -eq $false ) 
    {
        Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
    }
    Write-Output "Sucessfully read config file $ConfigPath file"
    Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    if($Error.Count -eq 0)
	{
        if( $? -eq $false ) 
	    {
            Write-Output "Could not read config file. Exiting ..."
            Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
	    }
    WriteCSVLog "Web Url,Library Name,Items left After Deletion,Folders left After Deletion,Status,Message"
        if(![string]::IsNullOrEmpty($cfg.Sites.SiteUrl))
        {
			try
			{
				foreach($Siteurl in $cfg.Sites.SiteUrl)
				{
                    $ErrorActionPreference = "SilentlyContinue"
					if($Siteurl.IsSubWebs -eq "1")
					{
						$Subsites=Get-SPSite $Siteurl.Url               
						$webCol=$Subsites.AllWebs                     
					}
					else
					{	            
						$webCol=Get-SPWeb $Siteurl.Url
					}
                    $error.Clear()
                    $systemLib = $cfg.Sites.SiteUrl.Libraries -split ","
                    $folderToExclude = $cfg.Sites.SiteUrl.Folders -split ","
                    $siteType = $cfg.Sites.SiteUrl.SiteType
                    
                    if($webCol -ne $null)
                    {
                        foreach($web in $webCol)
                        {
                           foreach($library in $web.Lists)
                           {                           
                                if($library.BaseType -eq "DocumentLibrary") 
                                {
                                    if(!$systemLib.Contains($library.Title))
                                    {
                                        $format = "<Method><SetList Scope=`"Request`">$($library.ID)</SetList><SetVar Name=`"ID`">{0}</SetVar><SetVar Name=`"Cmd`">Delete</SetVar><SetVar Name=`"owsfileref`">{1}</SetVar></Method>"                           
                                        try
			                            {
                                            checkinFiles $library
                                            if($siteType -eq 'RecordsCenter')
                                            {
                                                UnDeclareRecord $web $library
                                            }
                                            Write-Host "Deleting Files in $($library.Title) " -ForegroundColor Green
                                            Write-Output "Deleting Files in $($library.Title) "|out-null
                                            DeleteFiles $library $format $folderToExclude $web                                        
                                        }
			                            catch
			                            {
				                            Write-Output "Exception for $($web.Url)"|out-null    
				                            Write-Host "Exception for $($web.Url)" -ForegroundColor Yellow
			                            }
                                    }
                                }                            
                            }                                                             
                        }
                    }
				}
			}
			catch
			{
				Write-Output "Exception for $($Siteurl.Url)"|out-null    
				Write-Host "Exception for $($Siteurl.Url)" -ForegroundColor Yellow
			}
        }
		else
		{
			Write-Output "SiteUrl is empty for $($Siteurl.Url)"|out-null    
            Write-Host "SiteUrl is empty for $($Siteurl.Url)" -ForegroundColor Yellow
		}
    }
}

# ===================================================================================
# Function to get files and delete them
# ===================================================================================

function DeleteFiles($library,$format,$folderToExclude,$web)
{    
    try
    {
        Write-Host "Deleting $($library.Items.Count) items" -ForegroundColor Green
        Write-Output "Deleting $($library.Items.Count) items"|out-null
        while($library.Items.Count -gt 0)
        { 
            $query = new-object "Microsoft.SharePoint.SPQuery" 
            $query.ViewFields="<FieldRef Name=`"ID`" />" 
            $query.ViewAttributes = "Scope=`"Recursive`"" 
        
            $items = $library.GetItems($query) 
            $xmlToDeleteFiles = BatchDeleteCommandForFiles $items
            if($xmlToDeleteFiles -ne 'Error')
            {
                $result = $web.ProcessBatchData($xmlToDeleteFiles)  
                if ($result.Contains("ErrorText")){ break; } 
                Write-Host "Deleted. $($library.Items.Count) items left..." -ForegroundColor Green
                Write-Output "Deleted. $($library.Items.Count) items left..."|out-null
            }  
        }      
        
        DeleteFolders $library $folderToExclude $web
        $log=[string]::Empty 
        $log=$log+$web.Url+","+$library.Title+","+$library.Items.Count+","+$library.Folders.Count+",Pass"+",Deleted Successfully"
        WriteCSVLog $log        
    }
    catch
    {
        Write-Host "(ERROR in DeleteFiles  : "$_.Exception.Message")" -ForegroundColor Red
        Write-Output "(ERROR in DeleteFiles : "$_.Exception.Message")"|out-null
        $log=[string]::Empty 
        $log=$log+$web.Url+","+$library.Title+","+$filesBeforeDeletion+","+$library.Folders.Count+",Pass"+$_.Exception.Message
        WriteCSVLog $log
    }
 
}

# ===================================================================================
# Function to get folders of library and delete them
# ===================================================================================

function DeleteFolders($library,$folderToExclude,$web)
{
    try
    {
        Write-Host "Deleting folders..." -ForegroundColor Green
        Write-Output "Deleting folders..."|out-null 
        $folderItems=$library.Folders
        if($folderItems.Count -ne 0)
        {
            $xmlToDeleteFolders = BuildBatchDeleteCommand $folderItems $folderToExclude
            if($xmlToDeleteFolders -ne 'Error')
            {
                $result = $web.ProcessBatchData($xmlToDeleteFolders)
                if ($result.Contains("ErrorText")){ break; } 
                Write-Host "Folders Deleted for library $($library.Title)" -ForegroundColor Green
                Write-Output "Folders Deleted for library $($library.Title)"|out-null
            }
        }
    }
    catch
    {
        Write-Host "(ERROR in DeleteFolders  : "$_.Exception.Message")" -ForegroundColor Red
        Write-Output "(ERROR in DeleteFolders : "$_.Exception.Message")"|out-null
    }    
}

# ===================================================================================
# Function to make batch xml command to delete files and Doc set
# ===================================================================================

function BatchDeleteCommandForFiles($items) 
{
    try
    { 
        $sb = new-object System.Text.StringBuilder 
        $sb.Append("<?xml version=`"1.0`" encoding=`"UTF-8`"?><Batch>") 
        $items | %{ 
            $item = $_ 
            if($item.File -eq $null)
            {
            $sb.AppendFormat($format,$item.ID.ToString(),$item.ClientFormItemData.Url.ToString()) 
            }
            else
            {
            $sb.AppendFormat($format,$item.ID.ToString(),$item.File.ServerRelativeUrl.ToString())
            }
        } 
        $sb.Append("</Batch>") 
        return $sb.ToString()        
    }
    catch
    {
        Write-Host "(ERROR in BuildBatchDeleteCommand  : "$_.Exception.Message")" -ForegroundColor Red
        Write-Output "(ERROR in BuildBatchDeleteCommand : "$_.Exception.Message")"|out-null
        
        return "Error"
    }  
}

# ===================================================================================
# Function to build xml batch command to delete folders
# ===================================================================================
 
function BuildBatchDeleteCommand($folders , $folderToExclude) 
{
    try
    { 
        $sb = new-object System.Text.StringBuilder 
        $sb.Append("<?xml version=`"1.0`" encoding=`"UTF-8`"?><Batch>") 
    
            foreach($folder in $folders)
            {
                if(!$folderToExclude.Contains($folder.Name))
                {                 
                    $sb.AppendFormat($format,$folder.ID.ToString(),$folder.Folder.ServerRelativeUrl.ToString())                
                }
            }
     
        $sb.Append("</Batch>") 
        return $sb.ToString()
    }
    catch
    {
        Write-Host "(ERROR in BuildBatchDeleteCommand  : "$_.Exception.Message")" -ForegroundColor Red
        Write-Output "(ERROR in BuildBatchDeleteCommand : "$_.Exception.Message")"|out-null
        
        return "Error"
    } 
}

# ===================================================================================
# Function to checkin any checkout files before deletion
# =================================================================================== 

function checkinFiles($library)
{
    try
    {
        $CheckedOutFiles = $library.Items | Where-Object { $_.File.CheckOutStatus -ne "None"}
        ForEach($item in $CheckedOutFiles)
        {
            if($item.ProgId -ne 'Sharepoint.DocumentSet')
            {
                $library.GetItemById($item.Id).file.CheckIn("Checked in by Admin")
                write-host "File:'$($item.Name)' has been checked in!"
            }        
        }
    }
    catch
    {
        Write-Host "(ERROR in CheckinFiles  : "$_.Exception.Message")" -ForegroundColor Red
        Write-Output "(ERROR in CheckinFiles : "$_.Exception.Message")"|out-null
    }
}

# ===================================================================================
# Function to undeclare records of records library before deletion
# ===================================================================================

function UnDeclareRecord($web,$library)
{
    foreach($recordItem in $library.items)
    {
        try
        {
            $IsRecord = [Microsoft.Office.RecordsManagement.RecordsRepository.Records]::IsRecord($recordItem)
	        if ($IsRecord -eq $true)
            {
		        Write-Host "Undeclared $($recordItem.Title)" -ForegroundColor Green
                Write-Output "Undeclared $($recordItem.Title)"|out-null
		        [Microsoft.Office.RecordsManagement.RecordsRepository.Records]::UndeclareItemAsRecord($recordItem)
	        }
        }
        catch
        {
             Write-Host "(ERROR in UnDeclareRecord  : "$_.Exception.Message")" -ForegroundColor Red
             Write-Output "(ERROR in UnDeclareRecord : "$_.Exception.Message")"|out-null   
        }
    }
}














