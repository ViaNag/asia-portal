﻿##################################################################################################
# ================================================================================================
# Edit permission on libraries for svp and clients sites
# ================================================================================================
##################################################################################################


if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

# ===============================================================================================================
# FUNC: EditListPermission
# DESC: Edit list according to configurations mentioned in xml
# ===============================================================================================================
function EditListPermission($Site,$url,$web)
{
    if($Site.EditPermissions)
    {
        try
        {
           foreach($list in $Site.EditPermissions.List)
           {
            $ListToBreak = $web.Lists[$list.Name]

				if($ListToBreak)
					{
                        if(!$ListToBreak.HasUniqueRoleAssignments)
                        {
                            $ListToBreak.BreakRoleInheritance($false,$false)
                        }
                        if($ListToBreak.HasUniqueRoleAssignments)
                        {
                        foreach( $grp in $list.Group)
                        {
    
                        if ($grp.Permission -ne "None")
                         {
                             $SPGroup = $web.SiteGroups[$grp.Name]
                             $id=$SPGroup.ID
                             $ListToBreak.RoleAssignments.RemoveById($id)
                             $ListToBreak.Update()

                                $assignment = New-Object Microsoft.SharePoint.SPRoleAssignment($SPGroup)
                                $assignment.RoleDefinitionBindings.Add(($web.RoleDefinitions | Where-Object { $_.Name -eq $grp.Permission }))
                                $ListToBreak.RoleAssignments.Add($assignment)
                         }
                         else
                         {
                            $grpName=$null
                                if($grp.Name -eq "- Super Users")
                                {
                                    $webTitle=$web.Title
                                    $grpName = "SVP "+$webTitle+" "+$grp.Name
                                }
                                else
                                {
                                    $grpName = $grp.Name
                                }
                                    $RemoveGroup = $web.Groups[$grpName]
                                    $Id=$RemoveGroup.ID  
                                    $ListToBreak.RoleAssignments.RemoveById($Id)
                                }
                         }
                         $ListToBreak.Update()
                        }
                         
                     }
                       
                    else
                    {
                         Write-Host "List $($list.Name) not found on web " -ForegroundColor Red 
                    }
            }
            Write-Host "Unique permission assignment to libraries finished successfully for web $($web.Title)" -ForegroundColor Yellow
        }
        catch
        {
            Write-Host "Exception while fetching list for web : " $Error $url -ForegroundColor Red 
        }
    }
    else
    {
        Write-Host "BreakListInheritance node not present in xml" $Error -ForegroundColor Red 
    }
}

# ===================================================================================
# Reading configuration file and performing operations accrodingly
# ===================================================================================
    
	
function CallEditPermissionInSites([string]$ConfigPath = "")
{
    $cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
	if($Error.Count -eq 0)
	{
		

	$configXml = $cfg
	if( $? -eq $false ) 
	{
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
	}
    if ($configXml.Sites)
	{
		foreach ($SiteCol in $configXml.Sites.Site)
		{
			try
			{
                $path=$SiteCol.Path
                $siteCollection=New-Object Microsoft.SharePoint.SPSite($path)
                $currentWeb=$siteCollection.OpenWeb()               
                try
                 {
                 Write-Host "`nConfiguration for web : $($currentWeb.Title) starts:-"  -ForegroundColor Yellow
                 Write-Host "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
                
                 Write-Host "`nEditing permissions to libraries : " $currentWeb.Title "`n" -ForegroundColor Yellow
                 EditListPermission $SiteCol $currentWeb.Url $currentWeb   
                      
                 Write-Host "All operation finish for web : " $currentWeb.Title -ForegroundColor Yellow 
                 Write-Host "============================================================================================================================================================================="
                 }
                catch
                 {
                     Write-Host "Exception while fetching web at site collection: " $siteCollection.RootWeb.Title $Error -ForegroundColor Red
                 }                
            }
            catch
            {
            Write-Host "Exception while fetching sites collection at url: " $SiteCol.Path  $Error -ForegroundColor Red
            }
        }
    }



	}
}
