if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
{    
      Add-PSSnapin Microsoft.SharePoint.PowerShell
}

function CreateSearchLinkNavigation()
{

	$Identity = Read-Host "Enter the SiteCollection url : "
 
 Write-Host -ForegroundColor Red "============================================="
 Write-Host -ForegroundColor Green "Updating Search Navigation at URL " -NoNewline;
 Write-Host -ForegroundColor Green $Identity

 $s = Get-SPSite $Identity
 $w = $s.RootWeb

 foreach ($w in $s.AllWebs) { 
  Write-Host -ForegroundColor Red "============================================="
  Write-Host -ForegroundColor Green "Updating Search Navigation at URL " -NoNewline;
  Write-Host -ForegroundColor Green $w.Url
  
  $SearchNav = $w.Navigation.SearchNav
  
  IF ($SearchNav -ne $NULL)
  {
   Write-Host -ForegroundColor Red "This Site Search Navigation Already containing values";
   $SearchNav
    Write-Host -ForegroundColor Green "Deleting Navigation nodes";
    $cnt= $SearchNav.Count
    for($i=0; $i-lt $cnt; $i++)  
       {  
       write-host  -ForegroundColor Green  "Deleting Node title : "$SearchNav[0].Title 
           $SearchNav[0].Delete() 
        } 
  }
 
  

   Write-Host -ForegroundColor Green "Adding Search Navigation GreenRoom Asia";
   $Title = "GreenRoom Asia"
   $RelativeUrl = $Identity + "/Search/pages/AsiaResults.aspx"
   $node = new-object -TypeName "Microsoft.SharePoint.Navigation.SPNavigationNode" -ArgumentList $Title, $RelativeUrl, $true
   $w.Navigation.SearchNav.AddAsLast($node)
   
   Write-Host -ForegroundColor Green "Adding Search Navigation GreenRoom";
   $Title = "GreenRoom"
   $RelativeUrl = $Identity + "/Search/pages/GreenroomResults.aspx"
   $node = new-object -TypeName "Microsoft.SharePoint.Navigation.SPNavigationNode" -ArgumentList $Title, $RelativeUrl, $true
   $w.Navigation.SearchNav.AddAsLast($node)


  Write-Host -ForegroundColor Green "Adding Search Navigation Everything";
   $Title = "Everything"
   $RelativeUrl = $Identity + "/Search/pages/results.aspx"
   $node = new-object -TypeName "Microsoft.SharePoint.Navigation.SPNavigationNode" -ArgumentList $Title, $RelativeUrl, $true
   $w.Navigation.SearchNav.AddAsLast($node)
   
   Write-Host -ForegroundColor Green "Adding Search Navigation People";
   $Title = "People"
   $RelativeUrl = $Identity + "/Search/pages/peopleresults.aspx"
   $node = new-object -TypeName "Microsoft.SharePoint.Navigation.SPNavigationNode" -ArgumentList $Title, $RelativeUrl, $true
   $w.Navigation.SearchNav.AddAsLast($node)

   Write-Host -ForegroundColor Green "Adding Search Navigation This Site";
   $Title = "This Site"
   $RelativeUrl = $Identity + "/Search/Pages/thisSiteResults.aspx"
   $node = new-object -TypeName "Microsoft.SharePoint.Navigation.SPNavigationNode" -ArgumentList $Title, $RelativeUrl, $true
   $w.Navigation.SearchNav.AddAsLast($node)

  

  
  Write-Host -ForegroundColor Red "============================================="
    } 
 
 $w.Dispose()
 $s.Dispose()
 Write-Host -ForegroundColor Red "============================================="
}




