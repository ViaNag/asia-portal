if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
{    
      Add-PSSnapin Microsoft.SharePoint.PowerShell
}

function UpdateTitle()
{
$list = $spWeb.lists[$LibraryName]
if(!$list)
{
Write-Host $LibraryName "Does Not Exists!!" -ForegroundColor Red
}
else
{
$list.Title= $LibraryNewTitle;
$list.Update();
write-host "New Title of the list: " $list.Title
}


}


function UpdateListTitle([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)
	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
	if($Error.Count -eq 0)
	{
		$webAppFile = $cfg
		$error.clear()
		$sites = $webAppFile.Sites
		foreach($site in $Sites.Site)	
		{
		# Get the list for operation to perform
		$siteUrl= $site.SiteUrl
		$LibraryName = $site.LibraryOldName
		$LibraryNewTitle = $site.LibraryNewName
		

		$spWeb = get-spWeb $siteUrl

		###Calling function
		UpdateTitle
		}
		$spWeb.Dispose()
	}
}