if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
{    
      Add-PSSnapin Microsoft.SharePoint.PowerShell
}

function UpdatePageProperties()
{
$spFile.CheckOut()
$spFile.ListItemAllFields["Name"] = $UpdatedPageName
$spFile.ListItemAllFields["Title"] = $UpdatedPageTitle
$spFile.ListItemAllFields.Update()
$newPage = "Pages/"  + $UpdatedPageName + ".aspx"
$spFile = $spWeb.GetFile($newPage)
$spFile.Update()
$spFile.CheckIn("")
$spWeb.Update()
$spWeb.Dispose()
}


function CallUpdatePageProperties([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)
	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
	if($Error.Count -eq 0)
	{
		$webAppFile = $cfg
		$error.clear()
		$sites = $webAppFile.Sites
		foreach($site in $Sites.Site)	
		{
		# Get the list for operation to perform
		$siteUrl= $site.SiteUrl
		$pageName = $site.PageName
		$UpdatedPageName = $site.UpdatedPageName
		$UpdatedPageTitle = $site.UpdatedPageTitle

		#****Give the site url where one want to update page properties***** 

		$spWeb = get-spWeb $siteUrl
		$spFile = $spWeb.GetFile($pageName)

		###Calling function
		UpdatePageProperties
		}
	}
}