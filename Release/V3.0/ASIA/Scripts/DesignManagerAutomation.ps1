﻿ #cls   
#$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'}
#Add-PSSnapin "Microsoft.SharePoint.Powershell"
function UploadDesignFiles([string] $ConfigFileName)
{
    $configXml = [xml]$(get-content $ConfigFileName)
    if( $? -eq $false ) 
    {
	    Write-Host "Error: Could not read configuration definition file. Exiting ..." -ForegroundColor Red
	    Exit
    }
	foreach($designMgr in $configXml.DesignManagers.DesignManager)
	{
		foreach($rootFolder in $designMgr.Folders)
		{
			$web = Get-SPWeb $designMgr.SiteUrl
			$web.AllowUnsafeUpdates=$true
			$BrandingFolder = $currentLocation + "\Solutions\" + $rootFolder.Path
			$AllFolders=Get-ChildItem -Path $BrandingFolder | where {$_.PsIsContainer}
			foreach($folder in $AllFolders)
			{
				$masterPageList = $web.Lists[$folder.Name]
				$folderPath = $BrandingFolder + "\" + $folder.Name 
				write-host "Start -  Processing Folder " $folderPath -ForegroundColor Green
				GetFiles $masterPageList $folderPath "" $configXml.DesignManager.Keys $folder.Name
				write-host "End -  Processing Folder " $folderPath -ForegroundColor Green
			}

			$web.AllowUnsafeUpdates=$false
			$web.Dispose()
		}
	}
}

function GetFiles($library, $path, $folderName, $keys, $rootFolder)
{
    $fs = $folderName
        
    $files = Get-ChildItem -Path $path | where {!$_.PsIsContainer}
    $fileFolder = GetFolder $library $fs
    foreach ($item in $files) 
    {
        if($item.Extension -eq ".html")
        {
            ReplaceText $item $keys $fileFolder
        }

        UploadFile $library.ParentWeb $fileFolder $item
        write-host "File Uploaded " $item.FullName "  Structure " $fs -ForegroundColor Green
    }

    $folder = Get-ChildItem -Path $path | where {$_.PsIsContainer}

    foreach ($item in $folder) 
    {
        $startIndex = $item.FullName.IndexOf($rootFolder)+$rootFolder.Length
        $fs = $item.FullName.Substring($startIndex,$item.FullName.Length-$startIndex).Replace("\","/")
        GetFiles $library $item.FullName $fs $keys $rootFolder
    }

} 

function GetFolder($library, $folderPath)
{
    $path = $library.RootFolder.Url                 
    $fPath = $library.RootFolder.Url
    
    foreach($f in $folderPath.Split("/"))
    {
        if($f-ne "")
        {
            $path = $path + "/"+ $f
        }
        
        $folder = $library.ParentWeb.GetFolder($path)
    
        if (!$folder.Exists)
        {
            $folder = $library.AddItem($fPath, [Microsoft.SharePoint.SPFileSystemObjectType]::Folder, $f)
            $folder.Update();
            $folder = $library.ParentWeb.GetFolder($path)
            write-host "Folder Created " $folder.Url -ForegroundColor Green
        }
        
        $fPath = $folder.Url
    }

    return $folder
}

function UploadFile($web, $folder, $file)
{
    $checkInComment="Check In"
    $publishComment="published"
    $approveComment="Approved" 
    if ([Microsoft.SharePoint.Publishing.PublishingSite]::IsPublishingSite($web.Site)) 
	{
	    $isDesignManagerFile=$false;
	    $stream = ([System.IO.FileInfo] (Get-Item $file.FullName)).OpenRead()
        $destUrl = $folder.Url + "/" + $file.Name;
		$masterPageFile=$web.GetFile($destUrl)
	    if($masterPageFile.Exists)
        {
            if($masterPageFile.CheckOutStatus -eq "None")
            {
                $masterPageFile.CheckOut();
            }
                
            try
            {
                $masterPageFile = $folder.Files.Add($destUrl,[System.IO.Stream]$stream,$true)
            }
            catch
            {
                write-host $_ -ForegroundColor Red
            }
        }
        else
        {
            
            try
            {
                $masterPageFile = $folder.Files.Add($destUrl,[System.IO.Stream]$stream,$true)
            }
            catch
            {
                write-host $_ -ForegroundColor Red
            }
            
            if($file.Extension -eq ".html")
            {
                $masterPageFile.Update();         
                $web.Update();
                $item = $masterPageFile.Item;
                $item["Description"] = "Uploaded through powershell";
                $item.Update();
                $isDesignManagerFile = $true;
            }
        }
        $stream.close()
        if($masterPageFile.CheckOutStatus -ne "None")						  
        {
            $masterPageFile.CheckIn($checkInComment);
        }	
        if ($masterPageFile.CheckOutStatus -eq "None" -and $masterPageFile.Item.ParentList.EnableMinorVersions -and $masterPageFile.Item.ParentList.EnableVersioning )
        {
            $masterPageFile.Publish($publishComment);
        }
			
        if ($masterPageFile.CheckOutStatus -eq "None" -and $masterPageFile.Item.ParentList.EnableModeration)
        {
            $masterPageFile.Approve($approveComment);
        }	
        if($isMasterPageFile)
        {	  
            $masterPageFile.Update();  
        }       
        $web.Update();
    }

} 


function ReplaceText($file,$tokens,$folder)
{
	$filePath = $file.FullName
	$fileUrl = $folder.ParentWeb.Url + "/" + $folder.Url + "/" + $file.Name
	$content = $(get-content $filePath)
	if( $? -eq $false ) 
	{
		Write-Host "Could not read sql file. Exiting ..." -ForegroundColor Red
		ErrorExit
	}
    

	$content =$content -replace "##FileUrl##", $fileUrl
    foreach($key in $tokens.Key)
    {
        $val = $key.Value
        if($key.AppendUrl -eq "true")
        {
            $val = $folder.ParentWeb.Url + "/" + $key.Value
        }

	    $content =$content -replace $key.Name, $val
    }

    $content | Out-File $filePath -force
}
	
 
