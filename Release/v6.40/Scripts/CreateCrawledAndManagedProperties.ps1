# =================================================================================
#
# FUNC: CreateCrawlProperties
# DESC: Reads from xml file to add crawled properties.
#
# =================================================================================
function CreateCrawlProperties([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file`n"
	if($Error.Count -eq 0)
	{
		Write-Host "Before executing the script please ensure that Search Service Application is already created, running and name is correct in CreateCrawledProperties.xml file." -ForegroundColor Green
        Write-Output "Before executing the script please ensure that Search Service Application is already created, running and name is correct in CreateCrawledProperties.xml file."

		$XmlDoc = $cfg
		$searchapp = Get-SPEnterpriseSearchServiceApplication $XmlDoc.SearchProperties.SSAName

		#loop through crawled properties to create/modify the crawl property
		$CrawledPropNodeList = $XmlDoc.SearchProperties.CrawledProperties
		foreach ($CrawledPropNode in $CrawledPropNodeList.CrawledProperty)
		{
			$SPCrawlProp = $CrawledPropNode.Name
			$SPCrawlPropType = $CrawledPropNode.Type
			
			#Create Crawled Property if it doesn't exist
			if (!(Get-SPEnterpriseSearchMetadataCrawledProperty -SearchApplication $searchapp -Name $SPCrawlProp -ErrorAction "silentlycontinue"))
			{
				switch ($SPCrawlPropType)
				{
				    "Text" {$crawlprop = New-SPEnterpriseSearchMetadataCrawledProperty -SearchApplication $searchapp -Category SharePoint -VariantType 31 -Name $SPCrawlProp -IsNameEnum $false -PropSet $CrawledPropNode.PropSet -WarningAction Ignore}
				    "Integer" {$crawlprop = New-SPEnterpriseSearchMetadataCrawledProperty -SearchApplication $searchapp -Category SharePoint -VariantType 20 -Name $SPCrawlProp -IsNameEnum $false -PropSet $CrawledPropNode.PropSet -WarningAction Ignore}  
				    "Decimal" {$crawlprop = New-SPEnterpriseSearchMetadataCrawledProperty -SearchApplication $searchapp -Category SharePoint -VariantType 5 -Name $SPCrawlProp -IsNameEnum $false -PropSet $CrawledPropNode.PropSet -WarningAction Ignore}  
				    "DateTime" {$crawlprop = New-SPEnterpriseSearchMetadataCrawledProperty -SearchApplication $searchapp -Category SharePoint -VariantType 64 -Name $SPCrawlProp -IsNameEnum $false -PropSet $CrawledPropNode.PropSet -WarningAction Ignore}
				    "YesNo" {$crawlprop = New-SPEnterpriseSearchMetadataCrawledProperty -SearchApplication $searchapp -Category SharePoint -VariantType 11 -Name $SPCrawlProp -IsNameEnum $false -PropSet $CrawledPropNode.PropSet -WarningAction Ignore}
				    default {$crawlprop = New-SPEnterpriseSearchMetadataCrawledProperty -SearchApplication $searchapp -Category SharePoint -VariantType 31 -Name $SPCrawlProp -IsNameEnum $false -PropSet $CrawledPropNode.PropSet -WarningAction Ignore}
				}
                Write-Host "Crawl property $($CrawledPropNode.Name) created successfully." -ForegroundColor Green
                Write-Output "Crawl property $($CrawledPropNode.Name) created successfully."
			}
            else
            {
                Write-Host "Crawl property $($CrawledPropNode.Name) already exists." -ForegroundColor Yellow
                Write-Output "Crawl property $($CrawledPropNode.Name) already exists."
            }
            $error.Clear()
		}
        $ErrorActionPreference="Continue"
		write-host -ForegroundColor Green "Please perform full crawl"
        Write-Output "Please perform full crawl"
	}
}

# =================================================================================
#
# FUNC: CreateManagedProperties
# DESC: Reads from xml file to add managed properties.
#
# =================================================================================
function CreateManagedProperties([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file`n"
	if($Error.Count -eq 0)
	{
		Write-Host "Before executing the script please ensure that Search Service Application is already created, running and name is correct in CreatedManagedProperties.xml file." -ForegroundColor Green
        Write-Output "Before executing the script please ensure that Search Service Application is already created, running and name is correct in CreatedManagedProperties.xml file."

		$searchapp = Get-SPEnterpriseSearchServiceApplication $cfg.SearchProperties.SSAName

		#set enable for scoping true for manage property Content Type
		Set-SPEnterpriseSearchMetadataManagedProperty -SearchApplication $searchapp -Identity ContentType -EnabledForScoping $true

		$PropertyNodeList = $cfg.SearchProperties.ManagedProperties
		foreach ($PropertyNode in $PropertyNodeList.ManagedProperty)
		{
            $Error.Clear()
			$SharePointProp = $PropertyNode.Name
			$SharePointPropType = $PropertyNode.Type
			$SharePointPropMapList = $PropertyNode.Map
			$MultValue = $PropertyNode.MultiValue
			$Refinable=$PropertyNode.Refinable
            $Searchable = $PropertyNode.Searchable
			$Queryable=$PropertyNode.Queryable
			#add managed property
			#remove it if it already exists
			if ($managedProp = Get-SPEnterpriseSearchMetadataManagedProperty -SearchApplication $searchapp -Identity $SharePointProp -ErrorAction "silentlycontinue")
			{
				$managedProp.DeleteAllMappings()
				$managedProp.Delete()
				$searchapp.Update()
			}
			$prop=New-SPEnterpriseSearchMetadataManagedProperty -SearchApplication $searchapp -Name $SharePointProp -Type $SharePointPropType -EnabledForScoping $true 

			 if($MultValue -eq "true")
	         {
		        $prop.HasMultipleValues = $true
	         }
		     else
		     {
			    $prop.HasMultipleValues = $false
		     }
		     if($Refinable -eq "true")
		     {
			    $prop.Refinable = $true
		     }
             if($Searchable -eq "true")
		     {
			    $prop.Searchable = $true
		     }
             if($Queryable -eq "true")
		     {
			    $prop.Queryable = $true
		     }

			$prop.PutInPropertyBlob = 1
			$prop.Update()
			$managedProperty = Get-SPEnterpriseSearchMetadataManagedProperty -SearchApplication $searchapp -Identity $SharePointProp
            $error.Clear()
            if($managedProperty -ne $null)
            {
                $error.Clear()
			    #add multiple crawled property mappings
			    foreach ($SharePointPropMap in $SharePointPropMapList)
			    {
                    $Error.Clear()
				    $SPMapCat = $SharePointPropMap.Category
				    $SPMapName = $SharePointPropMap.InnerText

				    $category = Get-SPEnterpriseSearchMetadataCategory –SearchApplication $searchapp –Identity $SPMapCat
				    $prop = Get-SPEnterpriseSearchMetadataCrawledProperty -SearchApplication $searchapp -Category $category -Name $SPMapName -ErrorAction SilentlyContinue
                    if(!$Error)
                    {
                        if($prop -ne $null)
                        {
				            New-SPEnterpriseSearchMetadataMapping -SearchApplication $searchapp -CrawledProperty $prop -ManagedProperty $managedProperty -ErrorAction SilentlyContinue
                            if($Error)
                            { 
                                Write-Host "Failed to map crawl property $($SPMapName) to managed property $($PropertyNode.Name)" -ForegroundColor Yellow
                                Write-Output "Failed to map crawl property $($SPMapName) to managed property $($PropertyNode.Name) -$($error)"
                            }
                        }
                        else
                        {
                            Write-Host "Crawl property $($SPMapName) does not exist" -ForegroundColor Yellow
                            Write-Output "Crawl property $($SPMapName) does not exist"
                        }
                    }
			    }
            }
            else
            {
                Write-Host "Error while creating Managed property $($PropertyNode.Name)- $($error)" -ForegroundColor Green
                Write-Output "Error while creating Managed property $($PropertyNode.Name)- $($error)"
            }
            Write-Host "Managed property $($PropertyNode.Name) created successfully" -ForegroundColor Green
            Write-Output "Managed property $($PropertyNode.Name) created successfully"
		}
	}
}
# =================================================================================
#
# FUNC: UpdateManagedProperties
# DESC: Reads from xml file to update managed properties.
#
# =================================================================================
function UpdateManagedProperties([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file`n"
	if($Error.Count -eq 0)
	{
		Write-Host "Before executing the script please ensure that Search Service Application is already created, running and name is correct in CreatedManagedProperties.xml file." -ForegroundColor Green
        Write-Output "Before executing the script please ensure that Search Service Application is already created, running and name is correct in CreatedManagedProperties.xml file."

		$searchapp = Get-SPEnterpriseSearchServiceApplication $cfg.SearchProperties.SSAName

		#set enable for scoping true for manage property Content Type
		Set-SPEnterpriseSearchMetadataManagedProperty -SearchApplication $searchapp -Identity ContentType -EnabledForScoping $true

		$PropertyNodeList = $cfg.SearchProperties.ManagedProperties
		foreach ($PropertyNode in $PropertyNodeList.ManagedProperty)
		{
            $Error.Clear()
			$SharePointProp = $PropertyNode.Name
			$SharePointPropType = $PropertyNode.Type
			$SharePointPropMapList = $PropertyNode.Map
			$MultValue = $PropertyNode.MultiValue
			$Refinable=$PropertyNode.Refinable
            $Searchable = $PropertyNode.Searchable
			$Queryable=$PropertyNode.Queryable
			#add managed property
			#remove it if it already exists
			$managedProp = Get-SPEnterpriseSearchMetadataManagedProperty -SearchApplication $searchapp -Identity $SharePointProp -ErrorAction "silentlycontinue"
            if($managedProp -ne $null)
            {
                $mappedProperties=$managedProp.GetMappings()
                $mapCrawl=@{}
                $mapCrawl.Name=@{}
                $mapCrawl.Category=@{}
                for($i=0; $i -lt $mappedProperties.Count; $i++)
                {
                    $mapCrawl.Name.Add($i,$mappedProperties[$i].CrawledPropertyName)
                    $mapCrawl.Category.Add($i,[string]$mappedProperties[$i].CrawledPropset)
                    $index++
                }
			    if($MultValue -eq "true")
			    {
				    $managedProp.HasMultipleValues = $true
			    }
			    else
			    {
				    $managedProp.HasMultipleValues = $false
			    }
			    if($Refinable -eq "true")
			    {
				    $managedProp.Refinable = $true
			    }
			    if($Searchable -eq "true")
			    {
				    $managedProp.Searchable = $true
			    }
			    if($Queryable -eq "true")
			    {
				    $managedProp.Queryable = $true
			    }

			    $managedProp.PutInPropertyBlob = 1
			    $managedProp.Update()
            }
			$managedProperty = Get-SPEnterpriseSearchMetadataManagedProperty -SearchApplication $searchapp -Identity $SharePointProp
            
            $error.Clear()
            if($managedProperty -ne $null)
            {
                $error.Clear()
			    #add multiple crawled property mappings
			    foreach ($SharePointPropMap in $SharePointPropMapList)
			    {
                    $Error.Clear()
				    $SPMapCat = $SharePointPropMap.Category
				    $SPMapName = $SharePointPropMap.InnerText

				    $category = Get-SPEnterpriseSearchMetadataCategory –SearchApplication $searchapp –Identity $SPMapCat
				    $prop = Get-SPEnterpriseSearchMetadataCrawledProperty -SearchApplication $searchapp -Category $category -Name $SPMapName -ErrorAction SilentlyContinue
                    $map = Get-SPEnterpriseSearchMetadataMapping -SearchApplication $searchapp -ManagedProperty $managedProperty -CrawledProperty $prop
                    if($map)
                    {
                        Remove-SPEnterpriseSearchMetadataMapping -Identity $map -confirm:$false
                    }
                    if(!$Error)
                    {
                        if($prop -ne $null)
                        {
				            New-SPEnterpriseSearchMetadataMapping -SearchApplication $searchapp -CrawledProperty $prop -ManagedProperty $managedProperty -ErrorAction SilentlyContinue
                            if($Error)
                            { 
                                Write-Host "Failed to map crawl property $($SPMapName) to managed property $($PropertyNode.Name)" -ForegroundColor Yellow
                                Write-Output "Failed to map crawl property $($SPMapName) to managed property $($PropertyNode.Name) -$($error)"
                            }
                        }
                        else
                        {
                            Write-Host "Crawl property $($SPMapName) does not exist" -ForegroundColor Yellow
                            Write-Output "Crawl property $($SPMapName) does not exist"
                        }
                    }
			    }
            }
            else
            {
                Write-Host "Error while creating Managed property $($PropertyNode.Name)- $($error)" -ForegroundColor Green
                Write-Output "Error while creating Managed property $($PropertyNode.Name)- $($error)"
            }
            Write-Host "Managed property $($PropertyNode.Name) created successfully" -ForegroundColor Green
            Write-Output "Managed property $($PropertyNode.Name) created successfully"
		}
	}
}
