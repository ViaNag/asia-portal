# =================================================================================
#
#Main Function to create Left Navigation
#
# =================================================================================
function AddNewItemToQuickLaunch([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Successfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Successfully read config file $ConfigPath file`n"
	if($Error.Count -eq 0)
	{
        try
        {
            $error.Clear()
            foreach($webNav in $cfg.LeftNavigations.WebNavigation)
            {
                $web = Get-SPWeb $webNav.Url                
				$quickLaunch = $web.Navigation.QuickLaunch; 
				foreach($lNav in $webNav.LeftNavigation)
				{
					$headerLink = $quickLaunch | where {$_.title -eq $lNav.HeaderName}
					if(!$headerLink)
					{
						$newHeaderLink = New-Object Microsoft.SharePoint.Navigation.SPNavigationNode($lNav.HeaderName,$lNav.HeaderURL,$true);   
						$headerLink = $quickLaunch.AddAsLast($newHeaderLink); 
						write-host "Header link added to quickLaunch successfully at $($webNav.Url)"  -ForegroundColor Green
						write-output "Header link added to quickLaunch successfully at $($webNav.Url)" 
					}
					
					if(![string]::IsNullOrEmpty( $lNav.LinkName))
					{
						$newLink = New-Object Microsoft.SharePoint.Navigation.SPNavigationNode($lNav.LinkName,$lNav.LinkURL,$true);   
						$headerLink.Children.AddAsFirst($newLink); 
						write-host "Link added to quickLaunch for Header Link :$($lNav.HeaderName) successfully at $($webNav.Url)"  -ForegroundColor Green
						write-output "Link added to quickLaunch for Header Link :$($lNav.HeaderName) successfully at $($webNav.Url)" 
					}
					
				}
				if($webNav.NavigationType -eq "Structural Navigation")
				{
					$webNavSettings = New-Object Microsoft.SharePoint.Publishing.Navigation.WebNavigationSettings($web);
					$webNavSettings.CurrentNavigation.Source = "PortalProvider";
					$webNavSettings.Update();
				}
				$web.Update()
                $web.Dispose()    
            }
        }
        catch
        {
            Write-Host "`nException in AddNewItemToQuickLaunch method :" $Error -ForegroundColor Red
            Write-Output "`nException in AddNewItemToQuickLaunch method :" $Error
        }
	}
	else
	{
		Write-Host $Error -ForegroundColor Red
        Write-Output $Error
	}
}