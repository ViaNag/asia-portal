﻿##################################################################################################
# ================================================================================================
# MAin function to create termset
# ================================================================================================
##################################################################################################


function SetTermsRecursive ($termstore,[Microsoft.SharePoint.Taxonomy.TermSetItem] $termsetitem, $parentnode)
{
	$parentnode.term |
		ForEach-Object {
			## create the term
			if($_ -ne $null)
			{
                $newterm=$null
                $newterm=$termsetitem.Terms[$_.name]
                if($newterm -eq $null)
                    {
                        $termName=$_.name
                        if($termName.contains("&"))
                        {
                            $termName = $termName -replace "&","＆"
                            $newterm=$termsetitem.Terms[$termName]
                        }
                       if($newterm -eq $null)
                        { 
				            $newterm = $termsetitem.CreateTerm($_.name, 1033)
                            $termstore.CommitAll()
				            Write-Host "Added term $($_.name)" -ForegroundColor Green 
                            Write-Host "Added term $($_.name)"
                        }
                    }
				SetTermsRecursive $termstore $newterm $_
			}
		}
}

function CallCreateTermSets([string]$ConfigPath = "")
{
    #Start-SPAssignment -Global
    #Start-Transcript -Path ".\TermStores.log" -Force
$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file"
	if($Error.Count -eq 0)
	{
		echo "Extracting information from the $XMLPath"

		#Access the TermStore data
		$TermStoreData = [xml](get-content $ConfigPath)
        $termStoreNm=$TermStoreData.Termstore
        $grpName=$termStoreNm.TermGroup
        if(![string]::IsNullOrEmpty( $termStoreNm.Site) -and ![string]::IsNullOrEmpty( $termStoreNm.Name))
        {
		    $site = Get-SPSite $termStoreNm.Site 
		    $session = new-object Microsoft.SharePoint.Taxonomy.TaxonomySession($site)
		    $termstore = $session.TermStores[$termStoreNm.Name]
			if($termstore)
			{
                if($termStoreNm)
                {
                 $termStoreGroups=$termStoreNm.TermGroup
                if($termStoreGroups)
                {
                foreach($grpName in $termStoreGroups)
                {
                    if([string]$grpName.name -eq "M&E Ad Sales")
					{
					   $group=$termstore.Groups["M＆E Ad Sales"]
					}
					else
					{
						$group=$termstore.Groups[$grpName.name]
					}
                    if ($group -ne $null)
			        {
                        $group=$termstore.Groups[$grpName.name]
                    }
                    else
                    {
                        $group=$termStore.CreateGroup($grpName.name)
                        $termStore.CommitAll()
                    }
				    foreach($termSetNm in $grpName.Termset)
					    {
						    ## create the termset
						    $group=""
						    if([string]$grpName.name -eq "M&E Ad Sales")
							    {
							       $group=$termstore.Groups["M＆E Ad Sales"]
							    }
						    else
							    {
								    $group=$termstore.Groups[$grpName.name]
							    }
						    if ($group -ne $null)
						    {
							    $termSet=$group.TermSets[$termSetNm.name]
							    if($termSet -eq $null)
								    {
									    $termSet=$group.CreateTermSet($termSetNm.name)
									    $termStore.CommitAll()
									    Write-Host "Added termset $($termSet.name)" -ForegroundColor Green 
									    Write-Output "Added termset $($termSet.name)"
								    }
							    SetTermsRecursive $termstore $termSet $termSetNm
						    }
                            
					    }
                }
                }
                }
				$termstore.CommitAll()
			}
			else
			{
				Write-Host -ForegroundColor Green "Termstore not found with name $($termStoreNm.Name)"
				Write-Output "Termstore not fouund with name $($termStoreNm.Name)"
			}
        }
	}
#Stop-Transcript
#Stop-SPAssignment -Global
}

