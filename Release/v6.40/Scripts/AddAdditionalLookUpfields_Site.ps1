﻿$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

function AddAdditionalLookUpColumn()
{
try
{
#Get lookup what already exist
$lookupField = $sourceWeb.Fields.GetFieldByInternalName($existingLookupInternalName)

#Add Additional lookup
$addLookupResult = $sourceWeb.Fields.AddDependentLookup($addLookupInternalName, $lookupField.ID)
$addLookupField = $sourceWeb.Fields.GetFieldByInternalName($addLookupResult);
$addLookupField.Title = $addLookupDisplayName
$addLookupField.LookupField = $sourceWeb.Fields.GetFieldByInternalName($targetFieldInternalName).InternalName;
$addLookupField.Update();
write-host $addLookupDisplayName  "has added to site" $sourceWeb
}
catch
{
Write-Host "`nException :" $Error -ForegroundColor Red
}
}

# =================================================================================
#
#Main Function to Get Library details
#
# =================================================================================
function GetColumnDetails([string]$ConfigPath = "") 
{
	$cfg = [xml](get-content $ConfigPath)
	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
	if($Error.Count -eq 0)
	{
		$webAppFile = $cfg
		$error.clear()
		$sites = $webAppFile.Sites
		foreach($site in $Sites.Site)	
		{
		# Get the list for operation to perform
		$sourceSiteUrl= $site.SourceSiteUrl
		$targetFieldInternalName= $site.TargetFieldInternalName
		$existingLookupInternalName = $site.ExistingLookupInternalName
		$addLookupInternalName = $site.AddLookupInternalName
		$addLookupDisplayName = $site.AddLookupDisplayName

		#****Give the site url where one want to update page properties***** 

		$sourceWeb = get-spsite $sourceSiteUrl
        $sourceWeb = $sourceWeb.RootWeb;

		###Calling function
		AddAdditionalLookUpColumn
		}
	}
} 