﻿# =================================================================================
#
#Main Function
#
# =================================================================================
function CreateSiteColumns([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file`n"
   
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
            foreach($column in $cfg.SiteColumns.Column)
            {
                SiteColumnToAdd $column
            }
        }
        catch
        {
            Write-Output "`nException :" $Error
            Write-Host "`nException :" $Error -ForegroundColor Red
        }
	}
	else
	{
		Write-Host $Error -ForegroundColor Red
        Write-Output $Error
	}
}

# =================================================================================
#
# FUNC: SiteCOlumnToAdd
# DESC: Create site column in existing site collection
#
# =================================================================================
function SiteColumnToAdd([object] $column)
{
    try
    {
        $error.Clear()
        $site = new-object Microsoft.SharePoint.SPSite $column.SiteUrl
        $web = $site.RootWeb
        $ActionType=$column.ActionType
        if(![string]::IsNullOrEmpty($column.PushChanges))
        {
            $PushChanges=[System.Convert]::ToBoolean($column.PushChanges)
        }
        else
        {
            $PushChanges=$false
        }
        if($ActionType -eq "Add" -or $ActionType -eq "Update")
        {
            # Get the type of the field
            $type = $column.Type
            if(($type -eq "TaxonomyFieldType") -or ($type -eq "TaxonomyFieldTypeMulti"))
            {
                # Get the Taxonomy session of your site collection
                $session = new-object Microsoft.SharePoint.Taxonomy.TaxonomySession($site)
                $termStore = $session.TermStores[$column.TermStore]
                # Get the term store group which stores the term sets you want to retrieve.
                $taxGroup=$column.TaxonomyGroup
                if($column.TaxonomyGroup -eq "M&E Ad Sales")
                {
                    $taxGroup="M＆E Ad Sales"
                }
                $termStoreGroup = $termStore.Groups[$taxGroup]

                      # Get the term set you want to associate with this field. 
                      $termSet = $termStoreGroup.TermSets[$column.TermSet]
                      # In most cases, the anchor on the managed metadata field is set to a lower level under the root term of the term set. In such cases, specify the term in the spreadsheet and do the following
                      $termID=""
                      if(($column.TermSet -ne $column.Term) -and ($column.Term -ne "") -and ($column.Term -ne $null))
                      {
                            #Get all terms under term set
                            $terms = $termSet.GetAllTerms()

                            $termToMap =$column.Term
                            if($termToMap.Contains(";"))
                            {
                                $multiLevelTaxonomyTerm=$termToMap.Split(";")
                                $m=0;
                                foreach($termLabel in $multiLevelTaxonomyTerm)
                                {
                                   $term = $terms | Where-Object {$_.Name -eq $termLabel}   
                                   $terms = $term.Terms    
                                }
                            }
                            else
                            {
                                 #Get the term to map the column to
                                 $term = $terms | Where-Object {$_.Name -eq $column.Term}
                            }


                            #Get the GUID of the term to map the metadata column anchor to
                            $termID = $term.Id
                        }
                     else # In cases when you want to set the anchor at the root of the term set, leave the  value as blank. Empty guids will error out when you run the script but will accomplish what you need to do i.e. set the anchor at the root of the termset
                       {                                
                            $termID = [System.GUID]::empty
                       } 
                     # Create the new managed metadata field
                     if($ActionType -eq "Add")
                     {
                         $newSiteColumn=$null
                         if([string]::IsNullOrEmpty($column.Guid))
                         {
                            $newSiteColumn = $web.Fields.CreateNewField("TaxonomyFieldType", $column.InternalName)
                            $web.Fields.Add($newSiteColumn) | Out-Null
                            $web.Update()
                         }
                         else
                         {
                            $fieldXML=$null
                            $fieldXML = '<Field Type="TaxonomyFieldType" SourceID="http://schemas.microsoft.com/sharepoint/v3" Name="'+$column.InternalName+
                                         '" ID="{' + $column.Guid+'}'+
                                         '" DisplayName="'+$column.ColumnName+
                                         '"></Field>' 
                            $web.Fields.AddFieldAsXml($fieldXML) | Out-Null
                            $web.update()
                         }
                     }

                     $newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)

                     # Update the properties of the new field.
                     if($ActionType -eq "Add" -or $ActionType -eq "Update")
                     {
                         $newSiteColumn.SspId = $termSet.TermStore.ID
                         $newSiteColumn.TermSetId = $termSet.Id 
                         $newSiteColumn.AnchorId = $termID
                         if($type -eq "TaxonomyFieldTypeMulti")
                         {
                            $newSiteColumn.AllowMultipleValues = $true
                         }
                         # Add the the new column to the Site collection's Root web's Fields Collection
                         $newSiteColumn.Update($PushChanges)
                         $web.Update()
                     } 
            }
             elseif($type -eq "Choice" -or $type -eq "MultiChoice" -or $type -eq "OutcomeChoice")
             {
                    # Build a string array with the choice values separating the values at ","
                    $choiceFieldChoices = @($column.Choices.choice)

                    # Declare a new empty String collection
                    $stringColl = new-Object System.Collections.Specialized.StringCollection

                    # Add the choice fields from array to the string collection
                    $stringColl.AddRange($choiceFieldChoices)

                    # Create a new choice field and add it to the web using overload method
                    if($ActionType -eq "Add")
                    {
                        $newSiteColumn=$null
                        if([string]::IsNullOrEmpty($column.Guid))
                        {
                            $newSiteColumn = $web.Fields.Add($column.InternalName,[Microsoft.SharePoint.SPFieldType]::$type, $siteColumn.Required, $false, $stringColl)
                            $web.Update()
                        }
                        else
                        {
                           $fieldXML=$null
                           $fieldXML = '<Field Type="' + [Microsoft.SharePoint.SPFieldType]::$type + '" SourceID="http://schemas.microsoft.com/sharepoint/v3" FillInChoice="FALSE" Name="'+$column.InternalName+
                                         '" DisplayName="'+$column.ColumnName+
                                         '" ID="{' + $column.Guid+'}"'+
                                         '>'
                            if($column.Choices.choice.Count -gt 0)
                            {
                               $fieldXML=$fieldXML+"<CHOICES>"
                               foreach($choice in $choiceFieldChoices)
                               {
                                    $fieldXML=$fieldXML+'<CHOICE>'+ $choice.Trim() +'</CHOICE>'
                               }
                               $fieldXML=$fieldXML+"</CHOICES>"
                            }
                           $fieldXML=$fieldXML+ '</Field>'
                           $web.Fields.AddFieldAsXml($fieldXML)
                           $web.update()
                        }
                    }
                    $newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)
                    if($ActionType -eq "Update")
                    {
                        $newSiteColumn.choices.clear()
                        $newSiteColumn.update($PushChanges)
                        $web.update()
                        $newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)
                        $newSiteColumn.choices.addrange($choiceFieldChoices)
                        $newSiteColumn.update($PushChanges)
                        $web.update()
                    }
             }
			 elseif($type -eq "Number")
             {
                if($ActionType -eq "Add")
                {
					$newSiteColumn=$null
					
                    if([string]::IsNullOrEmpty($column.Guid))
                    {
                        $newSiteColumn = $web.Fields.CreateNewField([Microsoft.SharePoint.SPFieldType]::$type, $column.InternalName)                        
						$web.Fields.Add($newSiteColumn)
                        $web.update()
                    }
                    else
                    {
                     $fieldXML=$null
                        $fieldXML = '<Field Type="' + [Microsoft.SharePoint.SPFieldType]::$type + '"'+' SourceID="http://schemas.microsoft.com/sharepoint/v3" Name="'+$column.InternalName+
                                     '" ID="{' + $column.Guid+'}'+
                                     '" DisplayName="'+$column.ColumnName + '"></Field>'
									 
                        $web.Fields.AddFieldAsXml($fieldXML)
                        $web.update()
                    }
                }
                $newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)			 
             }
             elseif($type -eq "Image")
             {
                if($ActionType -eq "Add")
                {
				   $newSiteColumn=$null
                    if([string]::IsNullOrEmpty($column.Guid))
                    {
                        $newSiteColumn = $web.Fields.CreateNewField("Image", $column.InternalName)
                        $web.Fields.Add($newSiteColumn)
                        $web.update()
                    }
                    else
                    {
                        $fieldXML=$null
                        $fieldXML = '<Field Type="' + "Image" + '"'+' SourceID="http://schemas.microsoft.com/sharepoint/v3" Name="'+$column.InternalName+
                                     '" ID="{' + $column.Guid+'}'+
                                     '" DisplayName="'+$column.ColumnName + '"></Field>'
                        $web.Fields.AddFieldAsXml($fieldXML)
                        $web.update()
                    }
                }
                $newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)
             }
             elseif($type -eq "Calculated")
             {
                $formula ="=" + $column.Formula
                if($ActionType -eq "Add")
                {		  
					$newSiteColumn=$null
                    $choiceFieldChoices = @($column.CalculatedFields.Field)					
                    if([string]::IsNullOrEmpty($column.Guid))
                    {                      
                      # Add the the new column to the Site collection's Root web's Fields Collection  
                      $web.Fields.Add($column.InternalName,$type,$false)
                      $newSiteColumn=$web.Fields.GetField($column.InternalName)             
                      $newSiteColumn.Formula = $formula
                      $newSiteColumn.Update($PushChanges)
                      $web.Update()
                    }
                    else
                    {
                        $fieldXML=$null
                        $fieldXML = '<Field Type="' + [Microsoft.SharePoint.SPFieldType]::$type + '"'+' SourceID="http://schemas.microsoft.com/sharepoint/v3" Name="'+$column.InternalName+
                                     '" ID="{' + $column.Guid+'}'+
                                     '" DisplayName="'+$column.ColumnName + '">'
                        
						$fieldXML=$fieldXML+"<Formula>=Formula</Formula>" 

                        if($column.CalculatedFields.Field.Count -gt 0)
                        {
                           $fieldXML=$fieldXML+"<FieldRefs>"
                           foreach($choice in $choiceFieldChoices)
                           {
                                $fieldXML=$fieldXML+ "<FieldRef Name='" + $choice + "'/>"
                           }
                           $fieldXML=$fieldXML+"</FieldRefs>"
                        }
                        $fieldXML=$fieldXML+ '</Field>'   						 

						$web.Fields.AddFieldAsXml($fieldXML)
                        $web.update()
                    }
                }

                $newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)
                
                $newSiteColumn.Formula = $formula
                $newSiteColumn.Update()
                $web.Update()
                	
             }
             elseif($type -eq "HTML")
             {
                $newSiteColumn=$null
                $fieldXML=$null
                
                if([string]::IsNullOrEmpty($column.Guid))
                {
                       
                    $fieldXML = '<Field Type="' + $type + '"'+' SourceID="http://schemas.microsoft.com/sharepoint/v3" Name="'+$column.InternalName+
                                    '" DisplayName="'+$column.ColumnName + '" RichText="TRUE" RichTextMode="ThemeHtml"></Field>'
                }
                else
                {
                    $fieldXML = '<Field Type="' + $type + '"'+' SourceID="http://schemas.microsoft.com/sharepoint/v3" Name="'+$column.InternalName+
                                    '" ID="{' + $column.Guid+'}'+
                                    '" DisplayName="'+$column.ColumnName + '" RichText="TRUE" RichTextMode="ThemeHtml"></Field>'
                        
                }
                $web.Fields.AddFieldAsXml($fieldXML) | Out-Null
                $web.update()
            }
             else
             {
                    # Create the new field and add it to the web
                if($ActionType -eq "Add")
                {
                    $newSiteColumn=$null
                    if($type -eq "UserMulti")
                    {
                        $type="User"
                    }
                    $newSiteColumn=$null
                    if([string]::IsNullOrEmpty($column.Guid))
                    {
                        $newSiteColumn = $web.Fields.CreateNewField([Microsoft.SharePoint.SPFieldType]::$type, $column.InternalName)
                        $web.Fields.Add($newSiteColumn) | Out-Null
                        $web.update()
                    }
                    else
                    {
                        $fieldXML=$null
                        $fieldXML = '<Field Type="' + [Microsoft.SharePoint.SPFieldType]::$type + '"'+' SourceID="http://schemas.microsoft.com/sharepoint/v3" Name="'+$column.InternalName+
                                     '" ID="{' + $column.Guid+'}'+
                                     '" DisplayName="'+$column.ColumnName + '"></Field>'
                        $web.Fields.AddFieldAsXml($fieldXML) | Out-Null
                        $web.update()
                    }
                }
                $newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)
             }
              Write-Host "Site column $($column.ColumnName) with action type $($ActionType) operation completed successfully at web:- $($column.SiteUrl)" -ForegroundColor Green
              Write-Output "Site column $($column.ColumnName) with action type $($ActionType) operation completed successfully at web:- $($column.SiteUrl)"

              if($ActionType -eq "Add" -or $ActionType -eq "Update")
              {
                UpdateColumnProperites $column $web $PushChanges
              }
        }
    }
    catch
    {
        Write-Host "`nException for site column :" $column.ColumnName "`n" $Error -ForegroundColor Red
        Write-Output "`nException for site column :" $column.ColumnName "`n" $Error
    }
}

# =================================================================================
#
# FUNC: UpdateColumnProperites
# DESC: Update site column properties in existing site collection
#
# =================================================================================
function UpdateColumnProperites([object] $column, $web, $PushChanges)
{
    try
    {
        $error.Clear()
        SetDefaultValue $column $PushChanges
        $web=Get-SPWeb $column.SiteUrl
        $clm = $null
        $clm = $web.Fields.GetFieldByInternalName($column.InternalName)

        # Add or remove any properties here

        if(![string]::IsNullOrEmpty($column.ColumnName))
        {
             $clm.Title = $column.ColumnName
        }

        if(![string]::IsNullOrEmpty($column.Description))
        {
            $clm.Description = $column.Description
        }
        if(![string]::IsNullOrEmpty($column.GroupName))
        {
            $clm.Group = $column.GroupName             
        }
       
		if(![string]::IsNullOrEmpty($column.ColumnValidation))
        {
            $columnValidation=$column.ColumnValidation
            if($columnValidation -eq "Empty")
            {
                $columnValidation = "";    
            } 
             $clm.ValidationFormula= $columnValidation      
        }

        if(![string]::IsNullOrEmpty($column.JSLink))
		{
			$clm.JSLink=$column.JSLink
		} 
		
		if(![string]::IsNullOrEmpty($column.CalculatedValueFormula))
		{
			if($column.CalculatedValueFormula.StartsWith("="))
			{
				$clm.DefaultFormula=$column.CalculatedValueFormula;
			}
			else
			{
				$clm.DefaultFormula="=" + $column.CalculatedValueFormula
			}
		} 
		
        # Boolean values must be converted before they are assigned in PowerShell.
        if(![string]::IsNullOrEmpty($column.ShowInNewForm))
        {
            [boolean]$clm.ShowInNewForm = [System.Convert]::ToBoolean($column.ShowInNewForm)
        }
        if(![string]::IsNullOrEmpty($column.ShowInDisplayForm))
        {
            [boolean]$clm.ShowInDisplayForm = [System.Convert]::ToBoolean($column.ShowInDisplayForm)
        }
        if(![string]::IsNullOrEmpty($column.ShowInEditForm))
        {
            [boolean]$clm.ShowInEditForm = [System.Convert]::ToBoolean($column.ShowInEditForm)
        }
        if(![string]::IsNullOrEmpty($column.Hidden))
        {
            [boolean]$clm.Hidden = [System.Convert]::ToBoolean($column.Hidden)
        }
        if(![string]::IsNullOrEmpty($column.Required))
        {
            [boolean]$clm.Required = [System.Convert]::ToBoolean($column.Required)
        }
         if(![string]::IsNullOrEmpty($column.ReadOnlyField))
        {
            [boolean]$clm.ReadOnlyField = [System.Convert]::ToBoolean($column.ReadOnlyField)
        }
        if($type -eq "Note")
        {
            if($column.Format -eq "PlainText")
            {
                [boolean]$clm.RichText = $false
            }
            else
            {
                [boolean]$clm.RichText = $true
            }
        }
		elseif($column.Type -like 'User*')
		{
            $format = $column.Format
			if(![string]::IsNullOrEmpty($format))
			{
				$clm.SelectionMode=[Microsoft.SharePoint.SPFieldUserSelectionMode]::$format
			}
            
            if($column.Type -eq "UserMulti")
            {
                $clm.AllowMultipleValues = $true
            }

            if(![string]::IsNullOrEmpty($column.UserGroupName))
            {
                
                $spGroup=$web.SiteGroups[$column.UserGroupName]
                if($spGroup)
                {
                    $clm.SelectionGroup=$spGroup.ID
                }
                else
                {
                    Write-Host "Group $($column.UserGroupName) does not exists at web:- $($column.SiteUrl) for updating column $($column.InternalName)" -ForegroundColor Red
                    Write-Output "Group $($column.UserGroupName) does not exists at web:- $($column.SiteUrl) for updating column $($column.InternalName)" 
                }
            }
            else
            {
                $clm.SelectionGroup = 0; 
            }

		}
        elseif($column.Type -like "Number")
        {
            if(![string]::IsNullOrEmpty($column.Max))
            {
                $clm.MaximumValue = $column.Max
            }
            if(![string]::IsNullOrEmpty($column.Min ))
            {
                $clm.MinimumValue = $column.Min 
            } 
			if(![string]::IsNullOrEmpty($column.Format))
			{
				$clm.DisplayFormat=$column.Format;
			}
        }
		elseif($column.Type -like "Choice")
        {
			if(![string]::IsNullOrEmpty($column.Format))
            {
				if($column.Format -eq "RadioButtons")
				{
					$clm.EditFormat=[Microsoft.SharePoint.SPChoiceFormatType]::RadioButtons
				}
            }          
        }
        elseif($column.Type -like "DateTime")
        {
			if(![string]::IsNullOrEmpty($column.Format))
            {
				$clm.DisplayFormat=$column.Format
				
            }          
        } 
        elseif($column.Type -like "Calculated")
        {
            $clm.OutputType=$column.Format;
        }

             

        # Update the site column
        $clm.Update($PushChanges)
        $web.Update()
    }
    catch
    {
         Write-Host "`nException for site column :" $column.ColumnName "`n" $Error -ForegroundColor Red
         Write-Output "`nException for site column :" $column.ColumnName "`n" $Error 
    }
}

# =================================================================================
#
# FUNC: SetDeafultValue
# DESC: Set default value for column at site collection level
#
# =================================================================================
function SetDefaultValue([object] $column,$PushChanges)
{
    try
    {
        $error.Clear()
        $type=$column.Type
        if(($type -eq "TaxonomyFieldType") -or ($type -eq "TaxonomyFieldTypeMulti"))
            {
                # Get the Taxonomy session of your site collection
                $site = new-object Microsoft.SharePoint.SPSite $column.SiteUrl
                $session = new-object Microsoft.SharePoint.Taxonomy.TaxonomySession($site)
                $termStore = $session.TermStores[$column.TermStore]
                $taxGroup=$column.TaxonomyGroup
                if($column.TaxonomyGroup -eq "M&E Ad Sales")
                {
                    $taxGroup="M＆E Ad Sales"
                }
                $termStoreGroup = $termStore.Groups[$taxGroup]
                $termSet = $termStoreGroup.TermSets[$column.TermSet]
				$web=$site.RootWeb
                $field=$web.Fields.GetFieldByInternalName($column.InternalName)
                $setDefVal=""
                if($type -eq "TaxonomyFieldTypeMulti")
                {
                    $defaultValues = @($column.DefaultValues.Split(","))
                    foreach($value in $defaultValues)
                    {
                        if(($value -ne $null) -and ($value -ne ""))
                        {
                            $term=$termSet.Terms[$value]
							$wssIDToSet = AddTaxonomyHiddenListItem $web $term 
                            $setDefVal=[string]$setDefVal+$wssIDToSet + ";#" + $term.GetPath() + [Microsoft.SharePoint.Taxonomy.TaxonomyField]::TaxonomyGuidLabelDelimiter + $term.Id.ToString()+";#"
                        }
                    }
                    $setDefVal=$setDefVal.TrimEnd(";#")
                }
                elseif($type -eq "TaxonomyFieldType")
                {
                    $defaultValues = $column.DefaultValues
                    if(($defaultValues -ne $null) -and ($defaultValues -ne ""))
                    {
                        $term=$termSet.Terms[$defaultValues]
						$wssIDToSet = AddTaxonomyHiddenListItem $web $term 
                        $setDefVal=[string]$wssIDToSet + ";#" + $term.GetPath() + [Microsoft.SharePoint.Taxonomy.TaxonomyField]::TaxonomyGuidLabelDelimiter + $term.Id.ToString()
                    }
                }
                $field.DefaultValue=$setDefVal
                $field.Update($PushChanges)
            }
            elseif(($type -ne "UserMulti") -and ($type -ne "User") -and ($type -ne "Note"))
            {
                $defaultValues = $column.DefaultValues
                if(($defaultValues -ne $null) -and ($defaultValues -ne ""))
                {
                    $field=$web.Fields.GetFieldByInternalName($column.InternalName)
                    $field.DefaultValue=$defaultValues
                    $field.Update($PushChanges)
                }
            }
    }
    catch
    {
        Write-Host "`nException for site column :" $column.ColumnName "`n" $Error -ForegroundColor Red
        Write-Output "`nException for site column :" $column.ColumnName "`n" $Error 
    }
}

# =================================================================================
#
# FUNC: AddTaxonomyHiddenListItem
# DESC: Add term to taxonomy hidden list.
#
# =================================================================================
function AddTaxonomyHiddenListItem($w,$termToAdd)
{
      $wssid = $null; #return value
      $count = 0;     
      $l = $w.Lists["TaxonomyHiddenList"]; 
      #check if Hidden List Item already exists
      foreach($item in $l.Items){
        $xml = [xml]$item.xml;#cast the xml TaxonomyHiddenList item values
        $temID = $xml.row.ows_IdForTerm #get the IdForTerm, this is the key that unlocks all the doors
        if($temID -eq $termToAdd.ID){ #compare the IdForTerm in the TaxonomyHiddenList item to the term in the termstore
            Write-Host $item.Name "Taxonomy Hidden List Item already exists" -ForegroundColor Red
            $wssid =  $item.ID; #get and return the WSSID needed to set the default clumn value
            return $wssid;
        } 
      }
      $newListItem = $l.Items.ADD();
      $newListItem["Title"] = $termToAdd.Name;
      $newListItem["IdForTermStore"] = $termToAdd.TermStore.ID;
      $newListItem["IdForTerm"] = $termToAdd.ID;
      $newListItem["IdForTermSet"] = $termToAdd.TermSet.ID;
      $newListItem["Term"] = $termToAdd.Name;
      $newListItem["Path"] = $divTerm.Name + ":" + $termToAdd.Name;
      $newListItem["CatchAllDataLabel"] = $termToAdd.Name + "#Љ|";  #"Љ" special char
      $newListItem["Term1033"] = $termToAdd.Name;
      $newListItem["Path1033"] = $divTerm.Name + ":" + $termToAdd.Name;
      $newListItem.Update();
      foreach($item in $l.Items){
        $xml = [xml]$item.xml;
        $temID = $xml.row.ows_IdForTerm
        if($temID -eq $termToAdd.ID){
            $wssid =  $item.ID; #get and return the WSSID needed to set the default clumn value
        } 
      }     
	  return $wssid;      
}

