var Viacom = Viacom || {};
Viacom.APAC = Viacom.APAC || {};


Viacom.APAC = {
	customItemOperationContactHtml :function (ctx) {
    var header = '<div class="content-padding"><table style="width:100%"><tbody><tr><th title="ID number of issue">Issue Id</th><th title="Title of issue">Title</th><th  title="Type of Issue">Category</th><th title="Detailed description of issue">Detailed Description</th><th title="Territory/Cluster where issue was reported">Territory/Cluster</th><th title="Priority of issue">Business Impact</th><th title="User who has reported the issue">Reported by</th><th title="Documents attached to issue">Attachment</th><th title="Date issue was submitted">Issue Logged Date</th><th title="User assigned to issue based on category selected">Assigned to</th><th title="New user assigned to issue">Reassigned to</th><th title="Target date the issue will be resolved">Target Resolution Date</th><th title="Current Status of issue">Status</th><th title="Description of solution">Resolution/Remarks</th><th title="Last date the item was modified">Last Modified Date</th></tr>';
    return header;

	},
	pagingControl: function(ctx){
		var firstRow = ctx.ListData.FirstRow;
        var lastRow = ctx.ListData.LastRow;
        var prev = ctx.ListData.PrevHref;
        var next = ctx.ListData.NextHref;
		if(firstRow != undefined)
		{
        var html = "</tbody></table></div><div class='Paging'>";
        html += prev ? "<a class='ms-commandLink ms-promlink-button ms-promlink-button-enabled' href='" + prev + "'><span class='ms-promlink-button-image'><img class='ms-promlink-button-left' src='/_layouts/15/images/spcommon.png?rev=23' /></span></a>" : "";
        html += "<span class='ms-paging'><span class='First'>" + firstRow + "</span> - <span class='Last'>" + lastRow + "</span></span>";
        html += next ? "<a class='ms-commandLink ms-promlink-button ms-promlink-button-enabled' href='" + next + "'><span class='ms-promlink-button-image'><img class='ms-promlink-button-right' src='/_layouts/15/images/spcommon.png?rev=23'/></span></a>" : "";
        html += "</div>";
		}
		else
		var html = '</tbody></table></div>';
        return html;
	
	}
};

(function () {
    var overrideCtx = {};
    overrideCtx.Templates = {};
    overrideCtx.Templates.Header = Viacom.APAC.customItemOperationContactHtml;
  //  overrideCtx.Templates.Item = RenderItemTemplate(overrideCtx) + '</tbody></table></div></div>';
    overrideCtx.Templates.Footer = Viacom.APAC.pagingControl;
    overrideCtx.BaseViewID = 1;
    overrideCtx.ListTemplateType = 100;

    SPClientTemplates.TemplateManager.RegisterTemplateOverrides(overrideCtx);
})();

