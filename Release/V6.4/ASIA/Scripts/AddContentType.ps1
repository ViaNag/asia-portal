# =================================================================================
#
#Main Function to Add web Parts to publishing page
#
# =================================================================================
function GetLibraryDetails([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)
	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
	if($Error.Count -eq 0)
	{
		$webAppFile = $cfg
		$error.clear()
		$sites = $webAppFile.Sites
		foreach($site in $Sites.Site)	
		{
		# Get the list for operation to perform
		$siteUrl= $site.SiteUrl
		$libraryName = $site.LibraryName
		$contentType = $site.ContentType
		$contentTypeOrder = $site.ContentTypeOrder

		#****Give the site url where one want to update page properties***** 

		$web = get-spWeb $siteUrl
		$site = $web.Site

		###Calling function
		AddContentType
		}
	}
} 

# =================================================================================
#
# FUNC: AddWebPartPublish
# DESC: AddLibrary on to the page.
# =================================================================================
function AddContentType()
{    

    #Make sure content types are allowed on the list specified
    $docLibrary = $web.Lists[$libraryName]

    if ($docLibrary -ne $null)
    {
        $docLibrary.ContentTypesEnabled = $true
        $docLibrary.Update()

        #Add site content types to the list
        $ctToAdd = $site.RootWeb.ContentTypes[$contentType]
        $ct = $docLibrary.ContentTypes.Add($ctToAdd)
        write-host "Content type" $ct.Name "added to library" $docLibrary.Title
        $docLibrary.Update()
		#SetContentTypeOrder $web $docLibrary $contentTypeOrder 
    }
    else
    {
        write-host "The library" $libraryName "does not exist in site" $web.Title
    }


#Dispose of the site object
$web.Dispose()
}


# =================================================================================
#
# FUNC: SetContentTypeOrder
# DESC: SetContentTypeOrder in list.
# =================================================================================
function SetContentTypeOrder($web,$list,$contentTypeOrder)
{
# check if list contains the first content type of the $CTOrder variable (we assume that they have the other content types as well)
    try
    {
		$error.clear()
		if($contentTypeOrder)
		{
			if(($list -ne $null) -and (![string]::IsNullOrEmpty($contentTypeOrder)))
			{
				$CTOrder = $contentTypeOrder -split ","
				if(![string]::IsNullOrEmpty($CTOrder[0]))
				{
					$CTOrder[0]= ([string]$CTOrder[0]).Trim()
					if($list.ContentTypes[$CTOrder[0]] -ne $null)
					{
						# get the current ContentTypeOrder which is a List equivalent in C#
						$currentListOrder = $list.RootFolder.ContentTypeOrder
						# delete the current order - we cannot delete the first one as we need to have one
						for($i = $currentListOrder.Count; $i -gt 0; $i--)
						{
							$lct = $currentListOrder[$i]
							$disableoutput = $currentListOrder.Remove($lct)
						}
						# add the first contenttype to the order so we can remove the old left over content type
						$currentListOrder.Add($list.ContentTypes[$CTOrder[0]])
						# delete the left over content type
						$disableoutput = $currentListOrder.Remove($currentListOrder[0])
						# add the other content types to the order list
						foreach($ctToAdd in $CTOrder | where { $_ -ne $CTOrder[0]})
						{
							$ctToAdd=([string]$ctToAdd).Trim()
							if(![string]::IsNullOrEmpty($ctToAdd))
							{
								if($list.ContentTypes[$ctToAdd] -ne $null)
								{
									$currentListOrder.Add($list.ContentTypes[$ctToAdd])
								}
								else
								{
									Write-host "Content type $($ctToAdd) not exist in list $($list.Title)" -ForegroundColor Yellow
									Write-Output "Content type $($ctToAdd) not exist in list $($list.Title)"
								}
							}
						}
						# update the order list
						$list.RootFolder.UniqueContentTypeOrder = $currentListOrder
						$list.RootFolder.Update()
						# output the list URL of which the list is updated
						Write-Host "List $list content type order updated" -ForegroundColor Green
						Write-Output "List $list content type order updated"
					}
				}
			}
		}
		else
		{
			Write-host "Either content type order node not exist or not having data in xml" -ForegroundColor Yellow
			Write-Output "Either content type order node not exist or not having data in xml"
		}
    }
    catch
    {
         Write-Host "`nException : List $list while updating content type order`n" -ForegroundColor Red
         Write-Output "`nException : List $list while updating content type order`n"
    }
}