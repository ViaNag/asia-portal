# =================================================================================
#
#Main Function to Add web Parts to publishing page
#
# =================================================================================
function GetLibraryDetails([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)
	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
	if($Error.Count -eq 0)
	{
		$webAppFile = $cfg
		$error.clear()
		$sites = $webAppFile.Sites
		foreach($site in $Sites.Site)	
		{
		# Get the list for operation to perform
		$siteUrl= $site.SiteUrl
		$libraryName = $site.LibraryName
		$libraryTemplate = $site.LibraryTemplate
		$description = $site.Description

		#****Give the site url where one want to update page properties***** 

		$web = get-spWeb $siteUrl

		###Calling function
		CreateLibrary
		}
	}
} 

# =================================================================================
#
# FUNC: AddWebPartPublish
# DESC: AddLibrary on to the page.
# =================================================================================
function CreateLibrary()
{    
      if($web.Lists.TryGetList($libraryName) -eq $null)
      {
            Write-Host -ForegroundColor yellow "Creating" $libraryName " ........................."
            $template=$web.ListTemplates[$libraryTemplate]
            $web.Lists.Add($libraryName,$description,$template)
            Write-Host -ForegroundColor Green $libraryName " is created successfully."
      }
      else
      {
        Write-Host -ForegroundColor Yellow $libraryName " already exists."
      }
}
