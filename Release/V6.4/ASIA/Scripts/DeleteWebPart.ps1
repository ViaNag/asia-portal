if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
{    
      Add-PSSnapin Microsoft.SharePoint.PowerShell
}

function DeleteWebParts()
{
$spFile.CheckOut()
#Initialise the Web part manager for the specified profile page.
$spWebPartManager = $spWeb.GetLimitedWebPartManager($spFile.URL, [System.Web.UI.WebControls.WebParts.PersonalizationScope]::Shared)


#Remove the Web part from that page
foreach ($webpart in ($spWebPartManager.WebParts | Where-Object {$_.Title -eq $webpartTitle}))
{
    write-host $spWeb +": Existing Web part - " + $webpart.Title + " : " + $webpart.ID
    #Delete the existing webpart
    $spWebPartManager.DeleteWebPart($webpart)
    write-host "Deleted the existing" + $webpart.Title + "web part."
}
$spFile.Update()
$spFile.CheckIn("Update page via PowerShell",[Microsoft.SharePoint.SPCheckinType]::MajorCheckIn)
$spFile.Publish("");
$spWeb.Update()
$spWeb.Dispose()
}

function UpdateWebPart
{
$spFile.CheckOut()
#Initialise the Web part manager for the specified profile page.
$spWebPartManager = $spWeb.GetLimitedWebPartManager($spFile.URL, [System.Web.UI.WebControls.WebParts.PersonalizationScope]::Shared)
#Remove the Web part from that page
foreach ($webpart in ($spWebPartManager.WebParts ))
{
    
	if(($webPart.GetType() -eq [Microsoft.SharePoint.WebPartPages.ContentEditorWebPart]) -and ($webPart.ContentLink -like '*'+$OldLink+'*'))
	{
	 write-host "Web Url: "  $spWeb 
	 write-host "Web part Title- "  $webpart.Title
     $contentLinkVal = $webPart.ContentLink
     write-host "Current link:"  + $webPart.ContentLink
     $contentLinkVal = $contentLinkVal.ToLower().replace($OldLink,$NewLink)
     $webPart.ContentLink = $contentLinkVal
     $spWebPartManager.SaveChanges($webPart);
     write-host "Updated link:"  + $webPart.ContentLink
	}
	$titleUrl =  $webPart.TitleUrl
	if(($titleUrl -ne $null) -and ($titleUrl -like '*'+$OldLink+'*'))
	{
	write-host "Current TitleUrl:" + $titleUrl
	$titleUrl = $titleUrl.ToLower().replace($OldLink,$NewLink)
	$webPart.TitleUrl = $titleUrl
	write-host "Updated TitleUrl:" + $titleUrl
	$spWebPartManager.SaveChanges($webpart)
	}
}	
$spFile.Update()
$spFile.CheckIn("Update page via PowerShell",[Microsoft.SharePoint.SPCheckinType]::MajorCheckIn)
$spFile.Publish("");
$spWeb.Update()
$spWeb.Dispose()
}


function CallDeleteWebPart([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)
	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
	if($Error.Count -eq 0)
	{
		$webAppFile = $cfg
		$error.clear()
		$sites = $webAppFile.Sites
		foreach($site in $Sites.Site)	
		{
		# Get the list for operation to perform
		$siteUrl= $site.SiteUrl
		$pageName = $site.PageName
		$webpartTitle = $site.WebPartName

		#****Give the site url where one want to update page properties***** 

		$spWeb = get-spWeb $siteUrl
		$spFile = $spWeb.GetFile($pageName)

		###Calling function
		DeleteWebParts
		}
	}
}


function CallUpdateWebPart([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)
	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
	if($Error.Count -eq 0)
	{
		$webAppFile = $cfg
		$error.clear()
		$sites = $webAppFile.Sites
		foreach($site in $Sites.Site)	
		{
		
		# Get the list for operation to perform
		$siteUrl= $site.SiteUrl
		$pageName = $site.PageName
		$OldLink = $site.OldLink
		$NewLink = $site.NewLink

		#****Give the site url where one want to update page properties***** 

		$spWeb = get-spWeb $siteUrl
		$spFile = $spWeb.GetFile($pageName)

		###Calling function
		UpdateWebPart
		}
	}
}

