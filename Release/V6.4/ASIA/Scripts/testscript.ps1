﻿# =================================================================================
#
#Main Function to create lists
#
# =================================================================================
function CreateLists([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $($ConfigPath) file"
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
            foreach($lst in $cfg.Libraries.Library)
            {
                ListToAdd $lst
            }
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host $Error -ForegroundColor Red
        Write-Output $Error
	}
}

function SayHi([string]$ConfigPath = "")
{
Write-Host "Hi";
	
}

# =================================================================================
#
# FUNC: ListToAdd
# DESC: Create list if not exist else associate content type and fields to it.
# =================================================================================
function ListToAdd([object] $lst)
{
    try
    {
        $error.Clear()
        $site = new-object Microsoft.SharePoint.SPSite $lst.SiteURL
        $rootWeb=$site.RootWeb
        $web = $site.OpenWeb()
        # Check if the list already exist or not
        $list=$web.Lists[$lst.LibraryName]
        if(!$list)
        {
	        $list = $web.Lists.Add($lst.LibraryName,$lst.Description,$web.ListTemplates[$lst.Template])
            $web.update()
            Write-Host "List $($lst.LibraryName) created successfully at web : " $lst.SiteURL -ForegroundColor Green
            Write-Output "List $($lst.LibraryName) created successfully at web : " $lst.SiteURL
        }
        else
        {
            Write-Host "List $($lst.LibraryName) already exist at web : "$lst.SiteUrl -ForegroundColor Yellow
            Write-Output "List $($lst.LibraryName) already exist at web : "$lst.SiteUrl
        }
        $listAdded=$web.Lists[$lst.LibraryName]
        if(![string]::IsNullOrEmpty($lst.ContentType))
        {
            $ct=$web.AvailableContentTypes[$lst.ContentType]
            if(!$listAdded.ContentTypes[$ct.Name])
            {
                $listAdded.ContentTypes.Add($ct)
                Write-Host "ContentType $($lst.ContentType) added to list $($lst.LibraryName) at web : " $lst.SiteURL -ForegroundColor Green
                Write-Output "ContentType $($lst.ContentType) added to list $($lst.LibraryName) at web : " $lst.SiteURL
            }
            else
            {
                Write-Host "ContentType $($lst.ContentType) already exist in list $($lst.LibraryName) at web : " $lst.SiteURL -ForegroundColor Yellow
                Write-Output "ContentType $($lst.ContentType) already exist in list $($lst.LibraryName) at web : " $lst.SiteURL
            }
        }
        if(![string]::IsNullOrEmpty($lst.ListFields))
        {
            try
            {
                $field=$web.AvailableFields.GetFieldByInternalName($lst.ListFields)
                if(!$listAdded.Fields.Contains($field.Id))
                {
                    $listAdded.Fields.Add($field)
                    Write-Host "Field $($lst.ListFields) added to list $($lst.LibraryName) at web : " $lst.SiteURL -ForegroundColor Green
                    Write-Output "Field $($lst.ListFields) added to list $($lst.LibraryName) at web : " $lst.SiteURL
                }
                else
                {
                    Write-Host "Field $($lst.ListFields) already exist in list $($lst.LibraryName) at web : " $lst.SiteURL -ForegroundColor Yellow
                    Write-Output "Field $($lst.ListFields) already exist in list $($lst.LibraryName) at web : " $lst.SiteURL
                }
            }
            catch
            {
                Write-Host "Field $($lst.ListFields) not exist on web : "$lst.SiteURL -ForegroundColor Yellow
                Write-Output "Field $($lst.ListFields) not exist on web : "$lst.SiteURL
                $error.Clear()
            }
        }
        $listAdded.Description=$lst.Description
        $listAdded.Title=$lst.LibraryName
        $listAdded.Update()
        UpdateListProperties $lst $web $listAdded
		SetContentTypeOrder $lst $web $listAdded
		RemoveContentType $lst $web $listAdded
        $keys = @($lst.KeyFilters.Split(","))
        if($keys)
        {
            ApplyKeyFilterToLibrary $lst.LibraryName $lst.SiteURL $web $keys
        }
        $web.Dispose()
	}
    catch
    {
        Write-Host "`nException for contentType :" $ct.ContentTypeName "`n" $Error -ForegroundColor Red
        Write-Output "`nException for contentType :" $ct.ContentTypeName "`n" $Error
    }
}

# =================================================================================
#
#Main Function to create list views
#
# =================================================================================
function UpdateListProperties([object] $lstProp,$web,$list)
{
     try
     {
        # Check if the list already exist or not
        if($list)
        {
			#Attaching taxonomy event reciever to list
			$spEventReceiverAdding = $list.EventReceivers.Add()
			$spEventReceiverAdding.Assembly = "Microsoft.SharePoint.Taxonomy, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"
			$spEventReceiverAdding.Class = "Microsoft.SharePoint.Taxonomy.TaxonomyItemEventReceiver"
			$spEventReceiverAdding.Type = "ItemAdding"
			$spEventReceiverAdding.SequenceNumber = 1001
			$spEventReceiverAdding.Update()
			$spEventReceiverUpdating = $list.EventReceivers.Add()
			$spEventReceiverUpdating.Assembly = "Microsoft.SharePoint.Taxonomy, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"
			$spEventReceiverUpdating.Class = "Microsoft.SharePoint.Taxonomy.TaxonomyItemEventReceiver"
			$spEventReceiverUpdating.Type = "ItemUpdating"
			$spEventReceiverUpdating.SequenceNumber = 1002
			$spEventReceiverUpdating.Update()
			$ErrorActionPreference= "silentlycontinue"
            if(![string]::IsNullOrEmpty($lstProp.EnableVersioning))
            {
				[boolean]$list.EnableVersioning=[System.Convert]::ToBoolean(@{$true="false";$false=$lstProp.EnableVersioning}[$lstProp.EnableVersioning -eq ""])
            }
            if(![string]::IsNullOrEmpty($lstProp.ForceCheckout))
            {
				[boolean]$list.ForceCheckout=[System.Convert]::ToBoolean(@{$true="false";$false=$lstProp.ForceCheckout}[$lstProp.ForceCheckout -eq ""])
            }
            if(![string]::IsNullOrEmpty($lstProp.EnableFolderCreation))
            {
				[boolean]$list.EnableFolderCreation=[System.Convert]::ToBoolean(@{$true="false";$false=$lstProp.EnableFolderCreation}[$lstProp.EnableFolderCreation -eq ""])
            }
            if(![string]::IsNullOrEmpty($lstProp.EnableContentType))
            {
				[boolean]$list.ContentTypesEnabled=[System.Convert]::ToBoolean(@{$true="false";$false=$lstProp.EnableContentType}[$lstProp.EnableContentType -eq ""])
            }
	        $list.Update()
			$ErrorActionPreference= "Continue"
			$error.clear()
            Write-Host "List $($lstProp.LibraryName) property updated successfully at web : " $lstProp.SiteURL -ForegroundColor Green
            Write-Output "List $($lstProp.LibraryName) property updated successfully at web : " $lstProp.SiteURL
        }
     }
    catch
    {
        Write-Host "`nException on List $($lstProp.LibraryName) while updating property`n" $Error -ForegroundColor Red
        Write-Output "`nException on List $($lstProp.LibraryName) while updating property`n" $Error
    }
}

# =================================================================================
#
# FUNC: SetContentTypeOrder
# DESC: SetContentTypeOrder in list.
# =================================================================================
function SetContentTypeOrder([object] $lstProp,$web,$list)
{
# check if list contains the first content type of the $CTOrder variable (we assume that they have the other content types as well)
    try
    {
		$error.clear()
		if($lstProp.ContentTypeOrder)
		{
			if(($list -ne $null) -and (![string]::IsNullOrEmpty($lstProp.ContentTypeOrder)))
			{
				$CTOrder = $lstProp.ContentTypeOrder -split ","
				if(![string]::IsNullOrEmpty($CTOrder[0]))
				{
					$CTOrder[0]= ([string]$CTOrder[0]).Trim()
					if($list.ContentTypes[$CTOrder[0]] -ne $null)
					{
						# get the current ContentTypeOrder which is a List equivalent in C#
						$currentListOrder = $list.RootFolder.ContentTypeOrder
						# delete the current order - we cannot delete the first one as we need to have one
						for($i = $currentListOrder.Count; $i -gt 0; $i--)
						{
							$lct = $currentListOrder[$i]
							$disableoutput = $currentListOrder.Remove($lct)
						}
						# add the first contenttype to the order so we can remove the old left over content type
						$currentListOrder.Add($list.ContentTypes[$CTOrder[0]])
						# delete the left over content type
						$disableoutput = $currentListOrder.Remove($currentListOrder[0])
						# add the other content types to the order list
						foreach($ctToAdd in $CTOrder | where { $_ -ne $CTOrder[0]})
						{
							$ctToAdd=([string]$ctToAdd).Trim()
							if(![string]::IsNullOrEmpty($ctToAdd))
							{
								if($list.ContentTypes[$ctToAdd] -ne $null)
								{
									$currentListOrder.Add($list.ContentTypes[$ctToAdd])
								}
								else
								{
									Write-host "`nContent type $($ctToAdd) not exist in list $($list.Title)`n" -ForegroundColor Yellow
									Write-Output "`nContent type $($ctToAdd) not exist in list $($list.Title)`n"
								}
							}
						}
						# update the order list
						$list.RootFolder.UniqueContentTypeOrder = $currentListOrder
						$list.RootFolder.Update()
						# output the list URL of which the list is updated
						Write-Host "`nList $($lstProp.LibraryName) content type order updated`n" -ForegroundColor Green
						Write-Output "`nList $($lstProp.LibraryName) content type order updated`n"
					}
				}
			}
		}
		else
		{
			Write-host "`Either content type order node not exist or not having data in xml`n" -ForegroundColor Yellow
			Write-Output "`Either content type order node not exist or not having data in xml`n"
		}
    }
    catch
    {
         Write-Host "`nException : List $($lstProp.LibraryName) while updating content type order`n" -ForegroundColor Red
         Write-Output "`nException : List $($lstProp.LibraryName) while updating content type order`n"
    }
}

# =================================================================================
#
# FUNC: RemoveContentType
# DESC: Remove content type form list.
# =================================================================================
function RemoveContentType([object] $lstProp,$web,$list)
{
    try
    {
		$error.clear()
		if($lstProp.RemoveContentTypes)
		{
			if(($list -ne $null) -and (![string]::IsNullOrEmpty($lstProp.RemoveContentTypes)))
			{
				$CTRemove = $lstProp.RemoveContentTypes -split ","
				if($CTRemove -ne $null)
				{
					if($CTRemove.length -gt 0)
					{
						foreach($ct in $CTRemove)
						{
							$ct=([string]$ct).Trim()
							if(![string]::IsNullOrEmpty($ct))
							{
								$ctToRemove = $list.ContentTypes[$ct]
								if($ctToRemove -ne $null)
								{
									if($list.ContentTypes.Count -gt 1)
									{
										Write-host "`nRemoving content type $($ctToRemove.Name) from list $($list.Title)`n" -ForegroundColor Green
										Write-Output "`nRemoving content type" $($ctToRemove.Name) from list" $($list.Title)`n"
										$list.ContentTypes.Delete($ctToRemove.Id)
										$list.Update()
									}
									else
									{
										Write-host "`nContent type $($ctToRemove.Name) cannot be deleted from list $($list.Title) as list doesnot have more than one content type`n" -ForegroundColor Yellow
										Write-Output "`nContent type $($ctToRemove.Name) cannot be deleted from list $($list.Title) as list doesnot have more than one content type`n"
									}
								}
								else
								{
									Write-host "`nContent type $($ctToRemove.Name) not found in list $($list.Title)`n" -ForegroundColor Yellow
									Write-Output "`nContent type $($ctToRemove.Name) not found in list $($list.Title)`n"
								}
							}
						}
					}
				}
			}
		}
		else
		{
			Write-host "`nEither remove content type node not exist or having empty data in xml`n" -ForegroundColor Yellow
			Write-Output "`nEither remove content type node not exist or having empty data in xml`n" 
		}
    }
    catch
    {
         Write-Host "`nException : List $($lstProp.LibraryName) while removing content type order`n" -ForegroundColor Red
         Write-Output "`nException : List $($lstProp.LibraryName) while removing content type order`n"
    }
}

# =================================================================================
#
#Main Function to create list views
#
# =================================================================================
function CreateListViews([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file"
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
            foreach($VW in $cfg.Views.View)
            {
                ViewsToAdd $VW
            }
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host $Error -ForegroundColor Red
        Write-Output $Error
	}
}

# =================================================================================
#
# FUNC: ViewsToAdd
# DESC: Create view if not exist else modify it.
# =================================================================================
function ViewsToAdd([object] $VW)
{
    try
    {
        $error.Clear()
        $site = new-object Microsoft.SharePoint.SPSite $VW.SiteURL
        $web = $site.OpenWeb()
        # Get list
        $list=$web.Lists[$VW.LibraryName]
        if(!$list)
        {
            Write-Host "List $($lst.LibraryName) not exist at web : " $VW.SiteURL -ForegroundColor Green
            Write-Output "List $($lst.LibraryName) not exist at web : " $VW.SiteURL
        }
        else
        {
            #Query property
            $grpQuery=""
            $sortQuery=""
			$filterQuery=""
			if(![string]::IsNullOrEmpty($VW.FilterByQuery))
            {
				$filterQuery=$VW.FilterByQuery
            }
            if(![string]::IsNullOrEmpty($VW.GroupBy))
            {
                    $grpQuery="<GroupBy Collapse='TRUE' GroupLimit='30'>"
                    $grpFields = @($VW.GroupBy.Split(","))
                    foreach($field in $grpFields)
                    {
                        if(![string]::IsNullOrEmpty($field))
                            {
                                $grpQuery= $grpQuery + "<FieldRef Name='" + $field + "' />"
                            }
                    }
                     $grpQuery= $grpQuery + "</GroupBy>"
            }
            if(![string]::IsNullOrEmpty($VW.SortingBy))
            {
                    $sortQuery="<OrderBy>"
                    $sortFields = @($VW.SortingBy.Split(","))
                    foreach($field in $sortFields)
                    {
                        if(![string]::IsNullOrEmpty($field))
                            {
                                $sortQuery= $sortQuery + "<FieldRef Name='" + $field + "' />"
                            }
                    }
                     $sortQuery= $sortQuery + "</OrderBy>"
            }
            $viewQuery = $filterQuery+$grpQuery + $sortQuery
            $existingView=$list.Views[$VW.ViewName]
            $viewRowLimit = 30
            $viewPaged = $true
            [boolean]$viewDefaultView = [System.Convert]::ToBoolean(@{$true="false";$false=$VW.IsDefaultView}[$VW.IsDefaultView -eq ""])
            [boolean]$viewRecursive=[System.Convert]::ToBoolean(@{$true="false";$false=$VW.Recursive}[$VW.Recursive -eq ""])
            if(!$existingView)
            {
                $viewFields = New-Object System.Collections.Specialized.StringCollection
                $viewFields.Add($VW.Fields) > $null

                #Create the view in the destination list
                $newview = $list.Views.Add($VW.ViewName, $viewFields, $viewQuery, $viewRowLimit, $viewPaged, $viewDefaultView)
                Write-Host ("View '" + $newview.Title + "' created in list '" + $list.Title + "' on site " + $web.Url) -ForegroundColor Green
                Write-Output ("View '" + $newview.Title + "' created in list '" + $list.Title + "' on site " + $web.Url)
                $existingView=$newview
            }
            else
            {
                $viewFields = $existingView.ViewFields.ToStringCollection()
                if($viewFields.Contains($VW.Fields))
                {
                    Write-Host ("Field '" + $VW.Fields + "' already added in view '" + $VW.ViewName + "' on site " + $web.Url) -ForegroundColor Green
                    Write-Output ("Field '" + $VW.Fields + "' already added in view '" + $VW.ViewName + "' on site " + $web.Url)
                    $existingView.ViewFields.MoveFieldTo($VW.Fields,$existingView.ViewFields.Count)
                    $existingView.Update()   
                }
                else
                {
                    $existingView.ViewFields.add($VW.Fields)
                    $existingView.Update()
                    Write-Host ("Field '" + $VW.Fields + "' added in view '" + $VW.ViewName + "' on site " + $web.Url) -ForegroundColor Green
                    Write-Output ("Field '" + $VW.Fields + "' added in view '" + $VW.ViewName + "' on site " + $web.Url)
                }
            }
             if(![string]::IsNullOrEmpty($VW.IsDefaultView))
             {
                $existingView.DefaultView=$viewDefaultView
             }
             if(![string]::IsNullOrEmpty($VW.Recursive))
             {
                if($viewRecursive)
                {
                    $existingView.Scope=[Microsoft.SharePoint.SPViewScope]::Recursive
                }
                else
                {
                    $existingView.Scope=[Microsoft.SharePoint.SPViewScope]::Default
                }
             }
             $existingView.Query=$viewQuery
             $existingView.Paged=$viewPaged
             $existingView.RowLimit=$viewRowLimit
             $existingView.Update()
        }
        $web.Dispose()
	}
    catch
    {
        Write-Host "`nException for View :" $VW.ViewName "`n" $Error -ForegroundColor Red
        Write-Output "`nException for View :" $VW.ViewName "`n" $Error
    }
}

# ===================================================================================
# FUNC: ApplyKeyFilterToLibrary
# DESC: Given some xml containing library and keys to set filters on it
# ===================================================================================
function ApplyKeyFilterToLibrary($library,$url,$web,$keys)
{	
    try
    {
        $Error.Clear()
        $list = $web.Lists[$library]
        try
        {
            $listNavSettings = [Microsoft.Office.DocumentManagement.MetadataNavigation.MetadataNavigationSettings]::GetMetadataNavigationSettings($list)
            $listNavSettings.ClearConfiguredHierarchies()
            $listNavSettings.ClearConfiguredKeyFilters()

        #Configure key filters by adding columns
        foreach($key in $keys)
        {
            if(![string]::IsNullOrEmpty($key))
            {
                $listNavSettings.AddConfiguredKeyFilter($list.Fields.GetFieldByInternalName($key))
            }
        }
            [Microsoft.Office.DocumentManagement.MetadataNavigation.MetadataNavigationSettings]::SetMetadataNavigationSettings($list, $listNavSettings, $true)
            $list.RootFolder.Update()
            Write-Host "Success : Key filters for library $($library) set."  -ForegroundColor Green
            Write-Output "Success : Key filters for library $($library) set." 
        }
        catch
        {
            Write-Host "Exception: Configuring key filters for library $($library) for web : " $url $Error -ForegroundColor Red
            Write-Output "Exception: Configuring key filters for library $($library) for web : " $url $Error 
        }
    }
    catch
    {
        Write-Host "Exception: Fetching library $($library) for web : " $url $Error -ForegroundColor Red
        Write-Output "Exception: Fetching library $($library) for web : " $url $Error 
    }
Write-Host "Key filter for libraries in web $($url) set successfully" -ForegroundColor Green
Write-Output "Key filter for libraries in web $($url) set successfully"
}

SayHi