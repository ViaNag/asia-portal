if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
{    
      Add-PSSnapin Microsoft.SharePoint.PowerShell
}

function DeleteList()
{
$libraries = $LibraryName.split(',')
foreach($library in $libraries)
{
$list = $spWeb.lists[$library]
$list.Delete()
write-host $list + " has been deleted from site: " + $spWeb
}
$spWeb.Dispose()
}


function CallDeleteList([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)
	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
	if($Error.Count -eq 0)
	{
		$webAppFile = $cfg
		$error.clear()
		$sites = $webAppFile.Sites
		foreach($site in $Sites.Site)	
		{
		# Get the list for operation to perform
		$siteUrl= $site.SiteUrl
		$LibraryName = $site.LibraryName

		#****Give the site url where one want to update page properties***** 

		$spWeb = get-spWeb $siteUrl

		###Calling function
		DeleteList
		}
	}
}