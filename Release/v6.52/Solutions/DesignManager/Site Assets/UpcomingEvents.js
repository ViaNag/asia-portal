var LatestEvents = {
    LatestEventsHtml: '',
    defaultImageUrl: '/Sites/Asia/SiteCollectionImages/pagelayouts/default-article-thumbnail.png',
    locationFormat: '<div id="apiwp-latest-location-sm">ReplaceValue</div>',
    addressFormat: '<div id="apiwp-latest-address-sm">ReplaceValue</div>',
    startDateFormat: '<span class="apiwp-latest-start-event">ReplaceValue</span>',
    endDateFormat: '<span class="apiwp-latest-end-event">ReplaceValue</span>',
    descriptionFormat: '<div id="apiwp-latest-description-sm">ReplaceValue</div>',
    titleFormat: '<div id="apiwp-latest-tile-sm">ReplaceValue</div>',
    descriptionBoxFormat: '<div id="apiwp-latest-description-box-sm">ReplaceValue</div>',
    titleBoxFormat: '<a href="http://inclusion.viacom.com/" target="_blank"><div id="apiwp-latest-tile-box-sm">ReplaceValue</div></a>',
    imageContainerFormat: '<div class="apiwp-latest-image-container"><a href="http://inclusion.viacom.com/" target="_blank" style="background-image:url(ReplaceValue);"></a></div>',
    tileTitleContainerFormat: '<div id="apiwp-latest-tile-title-container-sm"><div id="apiwp-latest-tile-subsite-sm" class="apiwp-latest-tile-event"> ReplaceValue</div></div>',
    itemContainerFormat: '<li class="tile-small"><div id="apiwp-latest-tile-container-sm">ReplaceValue</div></li>',
    latestEventsData: null,
    latestEventsImagesData: null,
    latestEventsApiURL: 'http://inclusion.viacom.com/viacom-event-api/latest/viacom-event-api/latest',
    itemCount: 4,
    GetLatestEventsData: function () {
        /*var serviceURL = _spPageContextInfo.siteAbsoluteUrl + '/_vti_bin/Viacom.Intranet.WCFService/IntranetService.svc/' + 'GetLatestEvents?strServiceURL=' + LatestEvents.latestEventsApiURL + '&strItemCount=' + LatestEvents.itemCount;
        serviceURL = serviceURL.split("'").join("''");
        $.ajax({
            url: serviceURL,
            cache: false,
            dataType: 'json',
            type: 'GET',
            success: function (data, success) {*/
                //LatestEvents.latestEventsData = JSON.parse(data);
                //LatestEvents.latestEventsData = LatestEvents.latestEventsData.Collection;
                LoadContentSearchResult.ProcessToMergeEvents();
                LoadContentSearchResult.ProcessMergedEvents(LatestEvents.itemCount);
				LoadContentSearchResult.MobileCarousel();				
            /*},
            error: function (data) {
                LatestEvents.latestEventsData = data;
            }
        });*/
    },
    GenerateLatestEventItemHtml: function (eventItem) {
        var htmlText = '';
        var location = eventItem.location;
        var title = eventItem.title;
        var description = eventItem.description;
        var address = eventItem.address;
        var date = eventItem.date;
        var imagebase64 = eventItem.imageObjBase64;
        var startDate = '';
        var endDate = '';
        if (date) {
            startDate = new Date(date.from);
            startDate = startDate.format('MMM. dd, yyyy');
            endDate = new Date(date.to);
            endDate = endDate.format('MMM. dd, yyyy');
        }
        var descriptionHtml = '';
        if (location || address || description) {
            if (location) {
                descriptionHtml += LatestEvents.locationFormat.replace("ReplaceValue", location);
            }
            if (location) {
                descriptionHtml += LatestEvents.addressFormat.replace("ReplaceValue", address);
            }

            if (location) {
                descriptionHtml += LatestEvents.descriptionFormat.replace("ReplaceValue", description);
            }
            descriptionHtml = LatestEvents.descriptionBoxFormat.replace("ReplaceValue", descriptionHtml);
            descriptionHtml = '';
        }
        var tileHtml = '';
        if (title) {
            tileHtml += LatestEvents.titleFormat.replace("ReplaceValue", title);
            tileHtml = LatestEvents.titleBoxFormat.replace("ReplaceValue", tileHtml);
        }
        var imageHtml = '';
        if (imagebase64) {
            imageHtml += LatestEvents.imageContainerFormat.replace("ReplaceValue", "data:image/png;base64," + imagebase64);
        }
        else {
            imageHtml += LatestEvents.imageContainerFormat.replace("ReplaceValue", LatestEvents.defaultImageUrl);
        }
        var tileTitleContainerHtml = '';
        if (endDate || startDate) {
            if (startDate) {
                tileTitleContainerHtml += LatestEvents.startDateFormat.replace("ReplaceValue", startDate);
            }
            tileTitleContainerHtml = LatestEvents.tileTitleContainerFormat.replace("ReplaceValue", tileTitleContainerHtml);
        }
        htmlText = tileTitleContainerHtml + imageHtml + tileHtml + descriptionHtml;
        htmlText = LatestEvents.itemContainerFormat.replace("ReplaceValue", htmlText);
        return htmlText;
    }
}



var LoadContentSearchResult = {
    UpcomingEventsArray: [],
    MergedEventsArray: [],
    outerItemBoxFormatSmall: '<li class="tile-small"><div id="cswp-tile-container-sm" class="Inclusion-Upcoming-Events">ReplaceValue</div></li>',
    titleOuterBoxFormatSmall: '<div id="cswp-tile-subsite-title-container-sm">ReplaceValue</div>',
    likeOuterBoxFormatSmall: '<div title="Like this article" id="cswp-tile-like-sm" class="cswp-like-text cswp-tile-like-event">ReplaceValue</div>',
    outerItemBoxFormatMedium: '<li><div id="cswp-tile-container-med" class="Inclusion-Upcoming-Events">ReplaceValue</div></li>',
    titleOuterBoxFormatMedium: '<div id="cswp-tile-subsite-title-container-med">ReplaceValue</div>',
    likeOuterBoxFormatMedium: '<div title="Like this article" id="cswp-tile-like-med" class="cswp-like-text cswp-tile-like-event">ReplaceValue</div>',
    ProcessToMergeEvents: function () {
        /*for (var index = 0 ; index < LatestEvents.latestEventsData.length ; index++) {
            var inclusionEventItem = LatestEvents.latestEventsData[index];
            if (inclusionEventItem) {
                LoadContentSearchResult.MergedEventsArray.push({ EventType: "Inclusion", StartDate: inclusionEventItem.date.from, EventObject: inclusionEventItem });
            }
        }*/
        for (var index = 0 ; index < LoadContentSearchResult.UpcomingEventsArray.length ; index++) {
            var upcomingEventItem = LoadContentSearchResult.UpcomingEventsArray[index];
            if (upcomingEventItem) {
                LoadContentSearchResult.MergedEventsArray.push({ EventType: "Upcoming", StartDate: upcomingEventItem.StartDate, EventObject: upcomingEventItem });
            }
        }
        LoadContentSearchResult.MergedEventsArray.sort(function (a, b) {
            return new Date(a.StartDate).getTime() - new Date(b.StartDate).getTime()
        });
    },
    ProcessMergedEvents: function (itemCount) {
        var count = 0;
        if (itemCount <= LoadContentSearchResult.MergedEventsArray.length) {
            while (count < itemCount && count < LoadContentSearchResult.MergedEventsArray.length) {
                var eventItemObj = LoadContentSearchResult.MergedEventsArray[count];
                /*if (eventItemObj.EventType == "Inclusion") {
                    var eventItem = eventItemObj.EventObject;
                    var itemHTML = LatestEvents.GenerateLatestEventItemHtml(eventItem);
                    $("#UpcomingEventsHomepage ul").append(itemHTML);
                }
                else */if (eventItemObj.EventType == "Upcoming") {
                    var upcomingItem = eventItemObj.EventObject;
                    var itemHTML = LoadContentSearchResult.GenerateUpcomingEventsItemSmallTileHtml(upcomingItem);
                    $("#UpcomingEventsHomepage ul").append(itemHTML);
                    DisplayTemplateGetLikeCount(upcomingItem.EventLinkURL, upcomingItem.ListID, upcomingItem.ListItemID, upcomingItem.SiteTitle, upcomingItem.path, upcomingItem.formatlistID);
                }
                count++;
            }
			$("#UpcomingEventsHomepage").show();
			$("#latest").show();
        }
    },
    GenerateUpcomingEventsItemSmallTileHtml: function (upcomingItem) {
        var eventHtml = "";
        var titleBoxFormat = ' <div id="cswp-tile-subsite-sm" class="cswp-tile-event"><a href="' + upcomingItem.EventLinkURL + '">' + upcomingItem.StartDate + '</a></div>';
        var likeEventBoxFormat = '<span><span class="DTlikecount' + upcomingItem.formatlistID + upcomingItem.ListItemID + '"></span><a href="#" onclick="DTLikePage(\'' + upcomingItem.EventLinkURL + '\',\'' + upcomingItem.ListID + '\',\'' + upcomingItem.ListItemID + '\',\'' + upcomingItem.SiteTitle + '\',\'' + upcomingItem.path + '\',\'' + upcomingItem.formatlistID + '\')" class="DTLikeButton' + upcomingItem.formatlistID + '' + upcomingItem.ListItemID + '"></a></span>';
        likeEventBoxFormat = LoadContentSearchResult.likeOuterBoxFormatSmall.replace("ReplaceValue", likeEventBoxFormat);

        eventHtml += titleBoxFormat + likeEventBoxFormat;

        var pictureOuterBoxFormat = '<div><div class="cbs-picture3LinesContainer" ><div class="cbs-picture3LinesImageContainer"><a class="cbs-pictureImgLink" href="' + upcomingItem.EventLinkURL + '" title="' + $htmlEncode(upcomingItem.Title) + '"> ' + upcomingItem.ArticleThumbnail + ' </a></div> </div> </div> ';

        var descriptionBoxFormat = '<div id="cswp-tile-title-box-sm"><div id="cswp-tile-title-sm"><a class="cbs-picture3LinesLine1Link" style="word-wrap: break-word;" href="' + upcomingItem.EventLinkURL + '" title="' + $htmlEncode(upcomingItem.Title) + '" id="' + upcomingItem.ListID + upcomingItem.ListItemID + '">' + upcomingItem.Title + '  </a></div></div>';

        eventHtml = LoadContentSearchResult.titleOuterBoxFormatSmall.replace("ReplaceValue", eventHtml);
        eventHtml += pictureOuterBoxFormat + descriptionBoxFormat;
        eventHtml = LoadContentSearchResult.outerItemBoxFormatSmall.replace("ReplaceValue", eventHtml);
        return eventHtml;
    },
    GenerateUpcomingEventsItemMediumTileHtml: function (upcomingItem) {
        var eventHtml = "";
        var titleBoxFormat = ' <div id="cswp-tile-subsite-med" class="cswp-tile-event"><a href="' + upcomingItem.EventLinkURL + '">' + upcomingItem.StartDate + '</a></div>';
        var likeEventBoxFormat = '<span><span class="DTlikecount' + upcomingItem.formatlistID + upcomingItem.ListItemID + '"></span><a href="#" onclick="DTLikePage(\'' + upcomingItem.EventLinkURL + '\',\'' + upcomingItem.ListID + '\',\'' + upcomingItem.ListItemID + '\',\'' + upcomingItem.SiteTitle + '\',\'' + upcomingItem.path + '\',\'' + upcomingItem.formatlistID + '\')" class="DTLikeButton' + upcomingItem.formatlistID + '' + upcomingItem.ListItemID + '"></a></span>';
        likeEventBoxFormat = LoadContentSearchResult.likeOuterBoxFormatMedium.replace("ReplaceValue", likeEventBoxFormat);

        eventHtml += titleBoxFormat + likeEventBoxFormat;

        var pictureOuterBoxFormat = '<div><div class="cbs-picture3LinesContainer" ><div class="cbs-picture3LinesImageContainer"><a class="cbs-pictureImgLink" href="' + upcomingItem.EventLinkURL + '" title="' + $htmlEncode(upcomingItem.Title) + '"> ' + upcomingItem.ArticleThumbnail + ' </a></div> </div> </div> ';

        var descriptionBoxFormat = '<div id="cswp-tile-title-box-med"><div id="cswp-tile-title-med"><a class="cbs-picture3LinesLine1Link" style="word-wrap: break-word;" href="' + upcomingItem.EventLinkURL + '" title="' + $htmlEncode(upcomingItem.Title) + '" id="' + upcomingItem.ListID + upcomingItem.ListItemID + '">' + upcomingItem.Title + '  </a></div></div>';

        eventHtml = LoadContentSearchResult.titleOuterBoxFormatMedium.replace("ReplaceValue", eventHtml);
        eventHtml += pictureOuterBoxFormat + descriptionBoxFormat;
        eventHtml = LoadContentSearchResult.outerItemBoxFormatMedium.replace("ReplaceValue", eventHtml);
        return eventHtml;
    },
	//code for mobile carousel
	MobileCarousel: function(){
                   function carousel(){
                   if($( window ).width() <= 1024){
                   $("#UpcomingEventsHomepage ul").owlCarousel({items : 4, itemsDesktop : [1199, 4], itemsDesktopSmall : [979, 2],itemsMobile : [479, 1] });
                  }
                   if($( window ).width() > 1024){
                                                if(typeof $('#UpcomingEventsHomepage ul').data('owlCarousel') != 'undefined') {
                                                                $('#UpcomingEventsHomepage ul').data('owlCarousel').destroy();
                                                                $('#UpcomingEventsHomepage ul').removeClass('owl-carousel').removeClass('owl-theme');
                                                }
                  }
                  
                 }
                carousel();
                $( window ).resize(function() {
                carousel();
                });
}
}




