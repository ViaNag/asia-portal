﻿# =================================================================================
#
#Main Function to set site collection Administrator
#
# =================================================================================
function SetSiteCollectionsAdmins([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)
    
    if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
	{    
		  Add-PsSnapin Microsoft.SharePoint.PowerShell
	}

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..."  -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file"
	if($Error.Count -eq 0)
	{
        try 
        {
            $error.clear()
            foreach($siteCollection in $cfg.SiteCollections.SiteCollection)
            {
                SetSiteCollectionAdmins $siteCollection
            }
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host $Error -ForegroundColor "Red"
        Write-Output $Error
	}
}

# =================================================================================
#
# FUNC: SetSiteCollectionAdmins
# DESC: Set given set of user to site collection administrator
# =================================================================================
function SetSiteCollectionAdmins([object] $siteCollection)
{
    try
    {
        $Error.Clear()
        
		$site =  Get-SPSite -Identity $siteCollection.Url

		$web=$site.RootWeb;

		if($web.IsRootWeb)
		{
			foreach($user in $siteCollection.User)
			{
				try
				{			
					$NewAdmin = $web.EnsureUser($user)

					$NewAdmin.IsSiteAdmin = $true

					$NewAdmin.Update()
					Write-Host "User  $($user) Site Collection Admin Successfully Added at url : $($siteCollection.Url)" -ForegroundColor Green
					Write-Output "User  $($user) Site Collection Admin Successfully Added at url : $($siteCollection.Url)"
				}
				catch
				{
					Write-Host "`nUser not exist for site: $($siteCollection.Url) for user : $($user) `n" -ForegroundColor Yellow
					Write-Output "`nUser not exist for site: $($siteCollection.Url) for user : $($user) `n"
                    $error.Clear()
				}
			}	
		}
 	}
    catch
    {
        Write-Host "`nException to url :"  $siteCollection.Url "`n" $Error -ForegroundColor Red
        Write-Output "`nException to url :"  $siteCollection.Url "`n" $Error
    }
}
