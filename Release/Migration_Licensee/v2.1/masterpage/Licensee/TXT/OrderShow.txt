<script language="javascript" src="/programming/licensee-asia/_catalogs/masterpage/Licensee/JS/jquery-1.7.2.min.js" type="text/javascript"></script>
<script language="javascript" src="/programming/licensee-asia/_catalogs/masterpage/Licensee/JS/grid.locale-en.js" type="text/javascript"></script>
<script language="javascript" src="/programming/licensee-asia/_catalogs/masterpage/Licensee/JS/jquery.jqGrid.src.js" type="text/javascript"></script>
<script language="javascript" src="/programming/licensee-asia/_catalogs/masterpage/Licensee/JS/OrderShows.js" type="text/javascript"></script>

<script language="javascript" src="/programming/licensee-asia/_catalogs/masterpage/Licensee/JS/ShowsDialog.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" media="screen" href="/programming/licensee-asia/_catalogs/masterpage/Licensee/CSS/Licensee.css" />
<link rel="stylesheet" type="text/css" media="screen" href="/programming/licensee-asia/_catalogs/masterpage/Licensee/CSS/ui.jqgrid.css" />
<link rel="stylesheet" type="text/css" media="screen" href="/programming/licensee-asia/_catalogs/masterpage/Licensee/CSS/jquery-ui-custom.css" />
<div class="loadingImageCenter">
<img src="/programming/licensee-asia/_catalogs/masterpage/Licensee/Images/ajax-loader.gif" id="loading" style="Display:none;"/>
</div>
<div>
	<div>
		<div id="OwnersPanel" style="display: none;">
			<input type="button" class="cc-btn" value= "Create Show" onclick="Viacom.ShowModelCheck.OpenDialog('/Lists/Shows1/NewForm.aspx?IsDlg=1')">
			<input type="button" class="cc-btn" value= "Create Season" onclick="Viacom.ShowModelCheck.OpenDialog('/Lists/Seasons/NewForm.aspx')">
			<input type="button" class="cc-btn" value= "Create Episode" onclick="Viacom.ShowModelCheck.OpenDialog('/Lists/Episodes/NewForm.aspx')">
		</div>
	</div>
</div>
<div class="clearfix"></div>
<table id="ShowsJqGrid" width="100%"></table>
<div id="pagerShowGrid"></div>
<div id="submitPanel">
			<input type="button" class="cc-btn" value= "Order" onclick="selectedRows()">
</div>

<style>
.jqfoot {
    display: none!important;
}
.ui-jqgrid tr.jqgrow td{    white-space: normal;}
</style>
 
<script type="text/javascript">
//var currentSelectedShows = new Array();
var selectedEpisodeIds = null;
var requestingUser;
//var addNewItemUrl = "/_api/Web/Lists/GetByTitle('Show Order')/Items";
$(document).ready(function () {
       SP.SOD.executeFunc('SP.js', 'SP.ClientContext', checkUserGroup);
	});
   
   function checkUserGroup(){
   IsCurrentUserMemberOfGroup("Show Order Admin", function (isCurrentUserInGroup) {
   if(isCurrentUserInGroup)
   {
      $('#OwnersPanel').show();
   }
});
};

//Create entry is Show Order list of selected episodes.
 function selectedRows ()
 {
 var $grid = $("#ShowsJqGrid"); 
 var selIds = $grid.jqGrid("getGridParam", "selarrrow");
 for(i = 0; i < selIds.length; i++){
				//var showSeasonExist = (searchShowSeason($('#ShowsJqGrid').jqGrid ('getCell', selIds[i], 'ShowName.Title'),$('#ShowsJqGrid').jqGrid ('getCell', selIds[i], //'SeasonName.Title'),$('#ShowsJqGrid').jqGrid ('getCell', selIds[i], 'Title'),currentSelectedShows));
				//if(!showSeasonExist)
				//{
              // currentSelectedShows[currentSelectedShows.length] = {
                          //  ShowName: $('#ShowsJqGrid').jqGrid ('getCell', selIds[i], 'ShowName.Title'),
                           // SeasonName: $('#ShowsJqGrid').jqGrid ('getCell', selIds[i], 'SeasonName.Title'),
							//Artist: $('#ShowsJqGrid').jqGrid ('getCell', selIds[i], 'ShowName.Artist'),
                            //Year: $('#ShowsJqGrid').jqGrid ('getCell', selIds[i], 'ShowName.Year'),
							//tempBrand: $('#ShowsJqGrid').jqGrid ('getCell', selIds[i], 'ShowName.tempBrand'),
                           // tempSource: $('#ShowsJqGrid').jqGrid ('getCell', selIds[i], 'ShowName.tempSource'),
							//Duration: $('#ShowsJqGrid').jqGrid ('getCell', selIds[i], 'ShowName.Duration'),
                            //Synopsis: $('#ShowsJqGrid').jqGrid ('getCell', selIds[i], 'ShowName.Synopsis'),
							//Comments: $('#ShowsJqGrid').jqGrid ('getCell', selIds[i], 'ShowName.Comments'),
                            //RandCStatus: $('#ShowsJqGrid').jqGrid ('getCell', selIds[i], 'ShowName.RandCStatus'),
							//RandCStatus: $('#ShowsJqGrid').jqGrid ('getCell', selIds[i], 'ShowName.RandCExpiryDate'),
							//Episodes: $('#ShowsJqGrid').jqGrid ('getCell', selIds[i], 'Title'),
							//RequestingUser: requestingUser
							//IDs: $('#ShowsJqGrid').jqGrid ('getCell', selIds[i], 'ID')
							var episodeID = $('#ShowsJqGrid').jqGrid ('getCell', selIds[i], 'ID');
							if(episodeID != null && episodeID !="")
							{
							if(selectedEpisodeIds == null)
							{selectedEpisodeIds = $('#ShowsJqGrid').jqGrid ('getCell', selIds[i], 'ID');}
							else
							{
							selectedEpisodeIds = selectedEpisodeIds + ";" + $('#ShowsJqGrid').jqGrid ('getCell', selIds[i], 'ID');
							}
							}
							
                        };
				//}
            
			if(selIds.length>0)
			{Viacom.ShowModelCheck.OpenDialog('/Lists/Show%20Order/NewForm.aspx?EpisodesIds='+selectedEpisodeIds);}
			else
			{
			showMessage("Request Status","Please select episode(s) to order.");
			}
			selectedEpisodeIds = null;
			};
			
	 

 
 //Adding item in Show Order List.
 function addNewItem(url, arrData,arrLength) {
 
    $.ajax({
        url: _spPageContextInfo.webAbsoluteUrl + url,
        type: "POST",
        headers: {
            "accept": "application/json;odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val(),
            "content-Type": "application/json;odata=verbose"
        },
        data: JSON.stringify(arrData[arrLength-1]),
        success: function (data) {
			arrData.pop();
          if(arrData.length>0)
			{
				addNewItem(addNewItemUrl,arrData,arrData.length);
			}
			else
			{
			showMessage("Request Status","Request submitted successfully");
			}
        },
        error: function (error) {
            showMessage("Request Status","Request not submitted");
        }
    });
}
     //Show model dailog for alerts
    showMessage = function (title,message) {
        var htmlElement = document.createElement('p');
        htmlElement.innerHTML = message;
        var options = {
            title: title,
			//autoSize: true,
            width: 300,
            height: 100,
            html: htmlElement
        };

        SP.UI.ModalDialog.showModalDialog(options);
    };
 
 //Checking before adding item in Show Order List for same Shows & Seasons.
 function searchShowSeason(showName,seasonName, episodeName, Array){
    for (var i=0; i < Array.length; i++) {
        if ((Array[i].ShowName === showName) && (Array[i].SeasonName === seasonName))
		{
			Array[i].Episodes = Array[i].Episodes +", "+episodeName; 
            return true;
        }
		
    }
	return false;
}
//Check if user is Part of Show Order Admin Group to show Admin Panel.
	function IsCurrentUserMemberOfGroup(groupName, OnComplete) {

        var currentContext = new SP.ClientContext.get_current();
        var currentWeb = currentContext.get_web();

        var currentUser = currentContext.get_web().get_currentUser();
        currentContext.load(currentUser);

        var allGroups = currentWeb.get_siteGroups();
        currentContext.load(allGroups);

        var group = allGroups.getByName(groupName);
        currentContext.load(group);

        var groupUsers = group.get_users();
        currentContext.load(groupUsers);

        currentContext.executeQueryAsync(OnSuccess,OnFailure);

        function OnSuccess(sender, args) {
            requestingUser = currentUser.get_id();
			var userInGroup = false;
            var groupUserEnumerator = groupUsers.getEnumerator();
            while (groupUserEnumerator.moveNext()) {
                var groupUser = groupUserEnumerator.get_current();
                if (groupUser.get_id() == currentUser.get_id()) {
                    userInGroup = true;
                    break;
                }
            }  
            OnComplete(userInGroup);
        }

        function OnFailure(sender, args) {
            OnComplete(false);
        }    
}

 
</script>