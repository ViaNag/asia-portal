﻿/*
 * This javascript is use to open create show page
 * Page will redirect to new site welcome page once show is created successfully
 */
var Viacom = Viacom || {};
Viacom.ShowModelCheck = Viacom.ShowModelCheck || {};

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


Viacom.ShowModelCheck={
	OpenDialog:function(pageURL) {
		var redirectType = getParameterByName('RedirectType');
		var options = SP.UI.$create_DialogOptions();
		options.url = _spPageContextInfo.siteAbsoluteUrl + pageURL;
		options.width = 800;
		options.height = 800;
		options.dialogReturnValueCallback = function (res, retVal) {
			if (res== SP.UI.DialogResult.OK) {
				 if (retVal[0]) {
					 location.reload();
					//var RedirectUrl = window.location.protocol + "//" + window.location.host + retVal[0];
					//Viacom.ShowModelCheck.ShowDialogMessage(RedirectUrl);
					//window.location.href = RedirectUrl;
				}
			}
			if (res== SP.UI.DialogResult.cancel) {
					if (retVal[0]) {
				}
			}
		  }  
	SP.UI.ModalDialog.showModalDialog(options);
	}
}



































