﻿var ShowsListColumn = {
    Title: "Title",
    Id: "Id"
};
var SeasonsListColumn = {
    SeasonNumber: "SeasonNumber",
    ShowName: "ShowName",
    NoOfEpisodes: "NoOfEpisodes",
    Cycles: "Cycles",
    Id: "Id"
};
var EpisodesListColumn = {
    SeasonNumber: "SeasonNumber",
    ShowName: "ShowName",
    Cycles: "Cycles",
    EpisodeNumber: "EpisodeNumber",
    Id: "Id"
};

var ListNames = {
    Shows: "Shows",
    Seasons: "Seasons",
    Episodes: "Episodes"
};

var FormFieldMessage = {
    EpisodeAlreadyCreated: "",
    EpisodesCreated: ""
};
var MultipleEpisodesSeasonForm = {
    ExitingEpisodes: [],
    NoOfEpisodes: 0,
    GetSeasonDetails: function (seasonId) {
        debugger;
        var queryUrl = _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('" + ListNames.Seasons + "')/Items?$select=" + SeasonsListColumn.SeasonNumber + "," + SeasonsListColumn.NoOfEpisodes + "," + SeasonsListColumn.ShowName + "/Id&$expand=" + SeasonsListColumn.ShowName + "/Id&$filter=Id eq " + seasonId;
        $.ajax({
            url: queryUrl,
            type: "Get",
            headers: {
                "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                "Accept": "application/json;odata=verbose",
                "Content-Type": "application/atom+xml; odata=verbose"
            },
            success: function (data) {
                if (data.d.results.length > 0) {
                    var currentItem = data.d.results[0];
                    var noOfEpisodes = currentItem[SeasonsListColumn.NoOfEpisodes];
                    var showId = currentItem[SeasonsListColumn.ShowName].Id;
                    var seasonNumber = currentItem[SeasonsListColumn.SeasonNumber];
                    var cyclesId = seasonId;
                    if (noOfEpisodes > 0) {
                        MultipleEpisodesSeasonForm.CreateEpisodes(showId, seasonId, seasonNumber, cyclesId, noOfEpisodes);
                    }
                    else {
                        MultipleEpisodesSeasonForm.RedirectOnSave();
                    }
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    },
    CreateEpisodes: function (showId, seasonId, seasonNumber, cyclesId, numberOfEpisodes) {
        var clientContext = SP.ClientContext.get_current();
        var oList = clientContext.get_web().get_lists().getByTitle(ListNames.Episodes);
        FormFieldMessage.EpisodesCreated = "";
        for (var index = 0; index < numberOfEpisodes; index++) {
            var episodeNumber = seasonNumber + (index < 9 ? "0" + (index + 1) : "" + (index + 1));
            var item = oList.addItem();
            if (showId) {
                item.set_item(EpisodesListColumn.ShowName, showId);
            }
            item.set_item(EpisodesListColumn.SeasonNumber, seasonId);
            item.set_item(EpisodesListColumn.Cycles, cyclesId);
            item.set_item(EpisodesListColumn.EpisodeNumber, episodeNumber);
            item.update();
            FormFieldMessage.EpisodesCreated += "Episode Number " + episodeNumber + " created \n";

        }
        clientContext.executeQueryAsync(function () {
            // MultipleEpisodesSeasonForm.DisplayMesage(FormFieldMessage.EpisodesCreated);
            MultipleEpisodesSeasonForm.RedirectOnSave();
        },
        function (sender, args) {
            MultipleEpisodesSeasonForm.DisplayMesage(args.get_message());
        });
    },
    InitEpisodesCreation: function () {
        var hiddenValue = parseInt($("#hidItemID").val());
        if (hiddenValue > 0) {
            $("#hidItemID").val("-1");
            MultipleEpisodesSeasonForm.GetSeasonDetails(hiddenValue);
        }
        $("input[type='submit'][id*='btnCancel']").on('click', function () {
            SP.SOD.executeFunc('sp.js', 'SP.ClientContext', MultipleEpisodesSeasonForm.RedirectOnCancel);
        })
        $("input[type='submit'][id*='btnSave']").on('click', function () {
            var isValid = true;
            var seasonNum = $("input[title*='Season Number']").val();
            if (!isNaN(seasonNum)) {
                seasonNum = parseInt(seasonNum);
                if (seasonNum > 99) {
                    isValid = false;
                }

            }
            else {
                isValid = false;
            }
            if (!isValid) {
                MultipleEpisodesSeasonForm.DisplayMesage("Season number only allow 2 digits");
            }
            return isValid;
        })
    },
    RedirectOnSave: function () {
        var isDlg = MultipleEpisodesSeasonForm.GetParameterByName("IsDlg");
        if (isDlg == 1) {
            SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK);
        }
        else {
            var sourceUrl = MultipleEpisodesSeasonForm.GetParameterByName("Source");
            if (sourceUrl) {
                window.location.href = sourceUrl;
            }
            else {
                location.reload(true);
            }
        }
    },
    RedirectOnCancel: function () {
        var isDlg = MultipleEpisodesSeasonForm.GetParameterByName("IsDlg");
        if (isDlg == 1) {
            SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel);
        }
        else {
            var sourceUrl = MultipleEpisodesSeasonForm.GetParameterByName("Source");
            if (sourceUrl) {
                window.location.href = sourceUrl;
            }
            else {
                location.reload(true);
            }
        }
    },
    /*Extract parameter from query string*/
    GetParameterByName: function (querystring) {
        var result = null;
        var match = RegExp('[?&]' + querystring.toLowerCase() + '=([^&]*)').exec(window.location.search.toLowerCase());
        if (match) {
            if (match[1]) {
                result = match && decodeURIComponent(match[1].replace(/\+/g, ' '));
            }
        }
        return result;
    },
    /*Display message on UI*/
    DisplayMesage: function (message, callback) {
        var element = document.createElement('div');
        element.innerHTML = message;
        var option = {
            html: element,
            title: 'Message',
            allowMaximize: false,
            showClose: true,
            autoSize: true
        };
        if (callback) {
            option.dialogReturnValueCallback = callback;
        }
        SP.UI.ModalDialog.showModalDialog(option);
    }
};



$(document).ready(function () {
    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', MultipleEpisodesSeasonForm.InitEpisodesCreation);
});
