$(document).ready(function () {
    Viacom.ShowGrid.WebAppUrl = window.location.protocol + "//" + window.location.host;
    Viacom.ShowGrid.bindGrids();

    // 
    $("#ShowsJqGrid").on("change", "input[type=checkbox]", function (e) {
        var currentCB = $(this);
        var grid = jQuery('#ShowsJqGrid');
        var isChecked = this.checked;
        if (currentCB.is(".groupHeader")) {  //if group header is checked, to check all child checkboxes                                                                                           
            var checkboxes = currentCB.closest('tr').
              nextUntil('tr.ShowsJqGridghead_1').find('.cbox[type="checkbox"]');
            checkboxes.each(function () {
                if (!this.checked || !isChecked)
                    grid.setSelection($(this).closest('tr').attr('id'), true);
            });
        } else {  //when child checkbox is checked
            var allCbs = currentCB.closest('tr').prevAll("tr.ShowsJqGridghead_1:first").
                            nextUntil('tr.ShowsJqGridghead_1').andSelf().find('[type="checkbox"]');
            var allSlaves = allCbs.filter('.cbox');
            var master = allCbs.filter(".groupHeader");

            var allChecked = !isChecked ? false : allSlaves.filter(":checked").length === allSlaves.length;
            master.prop("checked", allChecked);
        }
    });

});

var Viacom = Viacom || {};

Viacom.Constants = {
    gridDiv: "ShowsJqGrid",
    pagerDiv: "pagerShowGrid",
    NoRecordMessage: "No Data Found",
    EpisodesListName: "Episodes",
    popupHeight: "600",
    popupWidth: "480",
    ClusterArray: [],
    UniqueClusters: [],
    UniqueArray: [],
    ClusterName: "",
    RegionName: "",
    GetSelecteddata: "",
    ChannelGridID: "",
    ChannelGridNumber: ""
};
Viacom.ShowGrid = {
    //call functions to display data for all departments based on Query String
    bindGrids: function () {
        Viacom.ShowGrid.SetGridProperties();
    },
    SetGridProperties: function () {
        var colNames = ['', '', 'Show Name', 'Season Number', 'Cycles', 'Episode Number', 'Artist', 'Year', 'Brand', 'Source', 'Duration (mins)', 'Synopsis', 'Comments', 'R&C Status', 'R&C Expiry Date', 'Episode Name', 'Episode ID'];
        var colModel = [
        { name: 'edit', search: false, index: 'ID', width: 30, sortable: false, formatter: Viacom.ShowGrid.editLink },
        { name: 'delete', search: false, index: 'ID', width: 35, sortable: false, formatter: Viacom.ShowGrid.deleteLink },
  		{ name: 'ShowName.Title', align: 'center', sortable: true, search: true },
		{ name: 'SeasonNumber.SeasonNumber', align: 'center', sortable: true, search: true },
        { name: 'Cycles.Cycles', align: 'center', sortable: true, search: true },
        { name: 'EpisodeNumber', align: 'center', sortable: true, search: true },
        { name: 'ShowName.Artist', align: 'center', sortable: true, search: true },
        { name: 'ShowName.Year', align: 'center', sortable: true, search: true },
        { name: 'ShowName.tempBrand', align: 'center', sortable: true, search: true },
		{ name: 'ShowName.tempSource', align: 'center', sortable: true, search: true },
        { name: 'ShowName.Duration', align: 'center', sortable: true, search: true },
        { name: 'Synopsis', align: 'center', sortable: true, search: true },
        { name: 'ShowName.Comments', align: 'center', sortable: true, search: true },
        { name: 'ShowName.RandCStatus', align: 'center', sortable: true, search: true },
        { name: 'ShowName.RandCExpiryDate', align: 'center', sortable: true, search: true },
        { name: 'Title', align: 'center', sortable: true, search: true },
		{ name: 'ID', align: 'center', sortable: true, search: true, hidden: true }
        ];

        $.ajax({
            url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('Episodes')/Items?$select=Title,ShowName/Title,ShowName/Artist,ShowName/Year,ShowName/tempBrand,ShowName/tempSource,ShowName/Duration,Synopsis,ShowName/Comments,ShowName/RandCStatus,ShowName/RandCExpiryDate,SeasonNumber/SeasonNumber,Cycles/Cycles,EpisodeNumber,ID&$expand=ShowName,SeasonNumber,Cycles&$orderby=ShowName/Title,SeasonNumber/SeasonNumber,Cycles/Cycles,EpisodeNumber&$top=5000",
            type: "Get",
            headers: {
                "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                "Accept": "application/json;odata=verbose",
                "Content-Type": "application/atom+xml; odata=verbose"
            },
            async: false,
            success: function (data) {
                if (data.d.results.length != 0) {

                    var grid = "#" + Viacom.Constants.gridDiv;
                    var pager = "#" + Viacom.Constants.pagerDiv;
                    $(grid).jqGrid({
                        datastr: data.d.results,
                        datatype: "jsonstring",
                        jsonReader: {
                            repeatitems: false
                        },
                        colNames: colNames,
                        colModel: colModel,
                        pager: pager,
                        rowNum: 20,
                        hidegrid: true,
                        rowList: [20, 30, 50],
                        height: 'auto',
                        autowidth: true,
                        shrinkToFit: true,
                        forceFit: true,
                        loadonce: true,
                        gridview: false,
                        ignoreCase: true,
                        multiselect: true,
                        caption: "Order Shows",
                        Search: true,
                        grouping: true,
                        groupingView: {
                            groupField: ["ShowName.Title", "SeasonNumber.SeasonNumber", "Cycles.Cycles"],
                            groupText: ['', '<input type="checkbox" class="groupHeader"/> <b>  {0}  </b>'],
                            groupColumnShow: [true, true],
                            groupOrder: ["asc", "asc"],
                            groupSummary: [true, true],
                            groupCollapse: true,
                            groupDataSorted: true
                        }

                    }).navGrid(pager, { edit: false, add: false, del: false, search: false, refresh: true }).filterToolbar({ stringResult: true, searchOnEnter: true });

                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    },
    editLink: function (cellValue, options, rowdata, action) {
        return "<a href='javascript:Viacom.ShowGrid.editRecord(" + rowdata.ID + ")' class='ui-icon ui-icon-pencil'></a>";
    },
    deleteLink: function (cellValue, options, rowdata, action) {
        return "<a href='javascript:Viacom.ShowGrid.deleteRecord(" + rowdata.ID + ")' class='ui-icon ui-icon-closethick'></a>";
    },
    deleteRecord: function (id) {
        var message = "You're about to delete an Episode. If you wish to proceed click OK";
        var htmlElement = document.createElement('div');
        htmlElement.innerHTML = message + "<br></br><input type='button' style ='float:right' value='OK' onclick='SP.UI.ModalDialog.commonModalDialogClose(1, \"OK\");' /><input type='button' style ='float:right' value='Cancel' onclick='SP.UI.ModalDialog.commonModalDialogClose(0, \"Cancel\");' />";
        var options = {
            title: ' ',
            width: 300,
            height: 100,
            html: htmlElement
        };
        options.dialogReturnValueCallback = function (res, retVal) {
            if (res == 1) {
                var clientContext = SP.ClientContext.get_current();
                var oList = clientContext.get_web().get_lists().getByTitle('Episodes');
                var item = oList.getItemById(id);
                item.recycle();
                clientContext.executeQueryAsync(function () {
                    showMessageConfirm("Request Status", "Request submitted successfully");
                },
                function (sender, args) {
                    showMessageConfirm("Request Status", args.get_message());
                });
            }
            if (res == 0) {
            }
        }
        SP.UI.ModalDialog.showModalDialog(options);

    },
    editRecord: function (id) {
        var editLinkUrl = "/Lists/Episodes/EditForm.aspx?ID=" + id;
        Viacom.ShowModelCheck.OpenDialog(editLinkUrl);
    }
};
