﻿var ShowsListColumn = {
    Title: "Title",
    Id: "Id"
};
var SeasonsListColumn = {
    SeasonNumber: "SeasonNumber",
    ShowName: "ShowName",
    NoOfEpisodes: "NoOfEpisodes",
    Cycles: "Cycles",
    Id: "Id"
};
var EpisodesListColumn = {
    SeasonNumber: "SeasonNumber",
    ShowName: "ShowName",
    Cycles: "Cycles",
    EpisodeNumber: "EpisodeNumber",
    Id: "Id"
};

var ListNames = {
    Shows: "Shows",
    Seasons: "Seasons",
    Episodes: "Episodes"
};

var FormFieldMessage = {
    SeasonNumber: "Please Select Season Number",
    ShowName: "Please Select Show Name",
    Cycles: "Please Select Cycles",
    EpisodeNotChanges: "No new Episodes are added",
    EpisodeNumber: "Please input valid Number",
    EpisodeAlreadyCreated: "",
    EpisodesCreated: ""
};
var MultipleEpisodesForm = {
    ExitingEpisodes: [],
    NoOfEpisodes: 0,
    ShowNameSelector: "select#ShowNameLookupField",
    SeasonNumberSelector: "select#SeasonNumberLookupField",
    CycleSelector: "select#CyclesLookupField",
    GetShowNameLookupField: function () {
        $.ajax({
            url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('" + ListNames.Shows + "')/Items?$select=" + ShowsListColumn.Title + "," + ShowsListColumn.Id + "&$orderby=" + ShowsListColumn.Title,
            type: "Get",
            headers: {
                "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                "Accept": "application/json;odata=verbose",
                "Content-Type": "application/atom+xml; odata=verbose"
            },
            success: function (data) {
                if (data.d.results.length > 0) {
                    MultipleEpisodesForm.PopulateShowNameLookupField(data.d.results);
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    },
    PopulateShowNameLookupField: function (results) {
        MultipleEpisodesForm.ClearShowNameLookupField();
        for (var index = 0; index < results.length; index++) {
            var currItem = results[index];
            var optValue = currItem[ShowsListColumn.Title];
            if (optValue) {
                var option = '<option  value="' + currItem[ShowsListColumn.Id] + '">' + optValue + '</option>';
                $(MultipleEpisodesForm.ShowNameSelector).append(option);
            }
        }
    },
    ClearShowNameLookupField: function () {
        $(MultipleEpisodesForm.ShowNameSelector).html('<option selected="selected" value="0">(None)</option>');
    },
    GetSeasonNumberLookupField: function (showId) {
        var queryUrl = _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('" + ListNames.Seasons + "')/Items?$select=" + SeasonsListColumn.SeasonNumber + "," + ShowsListColumn.Id + "," + SeasonsListColumn.ShowName + "/Id&$expand=" + SeasonsListColumn.ShowName + "/Id&$filter=" + SeasonsListColumn.ShowName + "/Id eq " + showId;
        $.ajax({
            url: queryUrl,
            type: "Get",
            async: false,
            headers: {
                "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                "Accept": "application/json;odata=verbose",
                "Content-Type": "application/atom+xml; odata=verbose"
            },
            success: function (data) {
                if (data.d.results.length > 0) {
                    MultipleEpisodesForm.PopulateSeasonNumberLookupField(data.d.results);
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    },
    PopulateSeasonNumberLookupField: function (results) {
        MultipleEpisodesForm.ClearSeasonNumberLookupField();
        var unique = [];
        results.forEach(function (d) {
            var found = false;
            unique.forEach(function (u) {
                if (u[SeasonsListColumn.SeasonNumber] == d[SeasonsListColumn.SeasonNumber]) {
                    found = true;
                }
            });
            if (!found) {
                unique.push(d);
            }
        });
        for (var index = 0; index < unique.length; index++) {
            var currItem = unique[index];
            var optValue = currItem[SeasonsListColumn.SeasonNumber];
            if (optValue) {
                var option = '<option value="' + currItem[SeasonsListColumn.Id] + '">' + optValue + '</option>';
                $(MultipleEpisodesForm.SeasonNumberSelector).append(option);
            }

        }
    },
    ClearSeasonNumberLookupField: function (results) {
        $(MultipleEpisodesForm.SeasonNumberSelector).html('<option selected="selected" value="0">(None)</option>');
    },
    GetCyclesLookupField: function (showId, seasonId, seasonNumber) {
        var queryUrl = _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('" + ListNames.Seasons + "')/Items?$select=" + SeasonsListColumn.SeasonNumber + "," + SeasonsListColumn.Id + "," + SeasonsListColumn.Cycles + "," + SeasonsListColumn.NoOfEpisodes + "," + SeasonsListColumn.ShowName + "/Id&$expand=" + SeasonsListColumn.ShowName + "/Id&$filter=" + SeasonsListColumn.ShowName + "/Id eq " + showId + " and " + SeasonsListColumn.SeasonNumber + " eq '" + seasonNumber + "'";
        $.ajax({
            url: queryUrl,
            type: "Get",
            async: false,
            headers: {
                "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                "Accept": "application/json;odata=verbose",
                "Content-Type": "application/atom+xml; odata=verbose"
            },
            success: function (data) {
                if (data.d.results.length > 0) {
                    MultipleEpisodesForm.PopulateCyclesLookupField(data.d.results);
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    },
    GetNoOfEpisodesField: function (seasonId) {
        var queryUrl = _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('" + ListNames.Seasons + "')/Items?$select=" + SeasonsListColumn.Id + "," + SeasonsListColumn.NoOfEpisodes + "&$filter=Id eq " + seasonId;
        $.ajax({
            url: queryUrl,
            type: "Get",
            async: false,
            headers: {
                "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                "Accept": "application/json;odata=verbose",
                "Content-Type": "application/atom+xml; odata=verbose"
            },
            success: function (data) {
                if (data.d.results.length > 0) {
                    var noOfEpisodes = data.d.results[0][SeasonsListColumn.NoOfEpisodes];
                    if (noOfEpisodes) {
                        $("#NoOfEpisodesNumberField").val(noOfEpisodes);
                        MultipleEpisodesForm.NoOfEpisodes = parseInt(noOfEpisodes);
                    }
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    },
    GetExitingEpisodes: function (showId, seasonId, seasonNumber, cyclesId) {
        var queryUrl = _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('" + ListNames.Episodes + "')/Items?$select=" + EpisodesListColumn.Cycles + "/Id," + EpisodesListColumn.Id + "," + EpisodesListColumn.EpisodeNumber + "," + EpisodesListColumn.ShowName + "/Id&$expand=" + EpisodesListColumn.Cycles + "/Id," + EpisodesListColumn.ShowName + "/Id&$filter=" + EpisodesListColumn.ShowName + "/Id eq " + showId + " and " + EpisodesListColumn.Cycles + "/Id eq '" + cyclesId + "'";
        $.ajax({
            url: queryUrl,
            type: "Get",
            headers: {
                "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                "Accept": "application/json;odata=verbose",
                "Content-Type": "application/atom+xml; odata=verbose"
            },
            success: function (data) {
                MultipleEpisodesForm.CreateEpisodes(data.d.results, showId, seasonId, seasonNumber, cyclesId);
            },
            error: function (data) {
                console.log(data);
            }
        });
    },
    CreateEpisodes: function (results, showId, seasonId, seasonNumber, cyclesId) {
        var numberOfEpisodes = parseInt($("#NoOfEpisodesNumberField").val());
        var clientContext = SP.ClientContext.get_current();
        var oList = clientContext.get_web().get_lists().getByTitle(ListNames.Episodes);
        FormFieldMessage.EpisodeAlreadyCreated = "";
        FormFieldMessage.EpisodesCreated = "";
        for (var index = MultipleEpisodesForm.NoOfEpisodes; index < numberOfEpisodes; index++) {
            var episodeNumber = seasonNumber + (index < 9 ? "0" + (index + 1) : "" + (index + 1));
            if (!MultipleEpisodesForm.CheckExitingEpisodes(results, episodeNumber)) {
                var item = oList.addItem();
                item.set_item(EpisodesListColumn.ShowName, showId);
                item.set_item(EpisodesListColumn.SeasonNumber, cyclesId);
                item.set_item(EpisodesListColumn.Cycles, cyclesId);
                item.set_item(EpisodesListColumn.EpisodeNumber, episodeNumber);
                item.update();
                FormFieldMessage.EpisodesCreated += "Episode Number " + episodeNumber + " created \n";
            }
            else {
                FormFieldMessage.EpisodeAlreadyCreated += episodeNumber + ",";
            }

        }
        clientContext.executeQueryAsync(function () {
            MultipleEpisodesForm.UpdateSeasonEpisodeNumber(seasonId);
        },
        function (sender, args) {
            MultipleEpisodesForm.DisplayMesage(args.get_message());
        });
    },
    UpdateSeasonEpisodeNumber: function (seasonId) {
        var numberOfEpisodes = parseInt($("#NoOfEpisodesNumberField").val());
        var clientContext = SP.ClientContext.get_current();
        var oList = clientContext.get_web().get_lists().getByTitle(ListNames.Seasons);
        var item = oList.getItemById(seasonId);
        item.set_item(SeasonsListColumn.NoOfEpisodes, numberOfEpisodes);
        item.update();
        clientContext.executeQueryAsync(function () {
            if (FormFieldMessage.EpisodeAlreadyCreated.length > 0) {
                FormFieldMessage.EpisodeAlreadyCreated = FormFieldMessage.EpisodeAlreadyCreated.replace(/,\s*$/, "");
                FormFieldMessage.EpisodeAlreadyCreated = "Episodes " + FormFieldMessage.EpisodeAlreadyCreated + " already exists";
                MultipleEpisodesForm.DisplayMesage(FormFieldMessage.EpisodeAlreadyCreated, MultipleEpisodesForm.RedirectOnSave);
            }
            else {
                MultipleEpisodesForm.RedirectOnSave();
            }
        },
        function (sender, args) {
            MultipleEpisodesForm.DisplayMesage(args.get_message());
        });
    },
    CheckExitingEpisodes: function (results, episodeNumber) {
        var result = false;
        var found = $.grep(results, function (v) {
            return v[EpisodesListColumn.EpisodeNumber] == episodeNumber;
        });
        if (found.length > 0) {
            result = true;
        }
        return result;
    },
    ValidateEpisodes: function () {
        var showId = $(MultipleEpisodesForm.ShowNameSelector).find(":selected").val();
        var seasonId = $(MultipleEpisodesForm.SeasonNumberSelector).find(":selected").val();
        var seasonNumber = $(MultipleEpisodesForm.SeasonNumberSelector).find(":selected").text();
        var cyclesId = $(MultipleEpisodesForm.CycleSelector).find(":selected").val();
        if (showId > 0) {
            if (seasonId > 0) {
                if (cyclesId > 0) {
                    var episode = $("#NoOfEpisodesNumberField").val();
                    if (!isNaN(episode)) {
                        episode = parseInt(episode);
                        if (parseInt(episode) > parseInt(MultipleEpisodesForm.NoOfEpisodes)) {
                            MultipleEpisodesForm.GetExitingEpisodes(showId, seasonId, seasonNumber, cyclesId);
                        }
                        else {
                            MultipleEpisodesForm.DisplayMesage(FormFieldMessage.EpisodeNotChanges);
                        }
                    }
                    else {
                        MultipleEpisodesForm.DisplayMesage(FormFieldMessage.EpisodeNumber);
                    }
                }
                else {
                    MultipleEpisodesForm.DisplayMesage(FormFieldMessage.Cycles);
                }
            }
            else {
                MultipleEpisodesForm.DisplayMesage(FormFieldMessage.SeasonNumber);
            }
        }
        else {
            MultipleEpisodesForm.DisplayMesage(FormFieldMessage.ShowName);
        }
    },
    PopulateCyclesLookupField: function (results) {
        MultipleEpisodesForm.ClearCyclesLookupField();
        for (var index = 0; index < results.length; index++) {
            var currItem = results[index];
            var optValue = currItem[SeasonsListColumn.Cycles];
            if (optValue) {
                var option = '<option  value="' + currItem[SeasonsListColumn.Id] + '">' + optValue + '</option>';
                $(MultipleEpisodesForm.CycleSelector).append(option);
            }

        }
    },
    ClearCyclesLookupField: function (results) {
        $(MultipleEpisodesForm.CycleSelector).html('<option selected="selected" value="0">(None)</option>');
    },
    InitLookupFieldSelector: function () {
        $(MultipleEpisodesForm.ShowNameSelector).on('change', function () {
            MultipleEpisodesForm.ClearSeasonNumberLookupField();
            $("#NoOfEpisodesNumberField").val(0);
            MultipleEpisodesForm.NoOfEpisodes = 0;
            MultipleEpisodesForm.ClearCyclesLookupField();
            MultipleEpisodesForm.GetSeasonNumberLookupField(this.value);
        })
        $(MultipleEpisodesForm.SeasonNumberSelector).on('change', function () {
            $("#NoOfEpisodesNumberField").val(0);
            MultipleEpisodesForm.NoOfEpisodes = 0;
            var showId = $(MultipleEpisodesForm.ShowNameSelector).find(":selected").val();
            var seasonId = $(MultipleEpisodesForm.SeasonNumberSelector).find(":selected").val();
            var seasonNumber = $(MultipleEpisodesForm.SeasonNumberSelector).find(":selected").text();
            MultipleEpisodesForm.GetCyclesLookupField(showId, seasonId, seasonNumber);
        })
        $(MultipleEpisodesForm.CycleSelector).on('change', function () {
            $("#NoOfEpisodesNumberField").val(0);
            MultipleEpisodesForm.NoOfEpisodes = 0;
            var seasonId = $(MultipleEpisodesForm.CycleSelector).find(":selected").val();
            var cyclesId = $(MultipleEpisodesForm.CycleSelector).find(":selected").val();
            MultipleEpisodesForm.GetNoOfEpisodesField(cyclesId);
        })
        $('input[name="SaveItem"][type="button"]').on('click', function () {
            SP.SOD.executeFunc('sp.js', 'SP.ClientContext', MultipleEpisodesForm.ValidateEpisodes);
        })
        $('input[name="CancelItem"][type="button"]').on('click', function () {
            SP.SOD.executeFunc('sp.js', 'SP.ClientContext', MultipleEpisodesForm.RedirectOnCancel);
        })
    },
    RedirectOnSave: function () {
        var isDlg = MultipleEpisodesForm.GetParameterByName("IsDlg");
        if (isDlg == 1) {
            SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK);
        }
        else {
            var sourceUrl = MultipleEpisodesForm.GetParameterByName("Source");
            if (sourceUrl) {
                window.location.href = sourceUrl;
            }
            else {
                location.reload(true);
            }
        }
    },
    RedirectOnCancel: function () {
        var isDlg = MultipleEpisodesForm.GetParameterByName("IsDlg");
        if (isDlg == 1) {
            SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel);
        }
        else {
            var sourceUrl = MultipleEpisodesForm.GetParameterByName("Source");
            if (sourceUrl) {
                window.location.href = sourceUrl;
            }
            else {
                location.reload(true);
            }
        }
    },
    /*Extract parameter from query string*/
    GetParameterByName: function (querystring) {
        var result = null;
        var match = RegExp('[?&]' + querystring.toLowerCase() + '=([^&]*)').exec(window.location.search.toLowerCase());
        if (match) {
            if (match[1]) {
                result = match && decodeURIComponent(match[1].replace(/\+/g, ' '));
            }
        }
        return result;
    },
    /*Display message on UI*/
    DisplayMesage: function (message, callback) {
        var element = document.createElement('div');
        element.innerHTML = message;
        var option = {
            html: element,
            title: 'Message',
            allowMaximize: false,
            showClose: true,
            autoSize: true
        };
        if (callback) {
            option.dialogReturnValueCallback = callback;
        }
        SP.UI.ModalDialog.showModalDialog(option);
    }
};



$(document).ready(function () {
    MultipleEpisodesForm.InitLookupFieldSelector();
    MultipleEpisodesForm.GetShowNameLookupField();
});
