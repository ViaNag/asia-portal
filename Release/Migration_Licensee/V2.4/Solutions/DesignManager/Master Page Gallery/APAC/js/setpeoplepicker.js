<script src="/asia/fpassc/_catalogs/masterpage/APAC/js/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function () {
var userid = _spPageContextInfo.userId;
function GetCurrentUser() {
var requestUri = _spPageContextInfo.webAbsoluteUrl + "/_api/web/getuserbyid(" + userid + ")";
var requestHeaders = { "accept" : "application/json;odata=verbose" };
$.ajax({
  url : requestUri,
  contentType : "application/json;odata=verbose",
  headers : requestHeaders,
  success : onSuccess,
  error : onError
});
}
function onSuccess(data, request){
 var loginName = data.d.Title;
 SetUserFieldValue("people",loginName);
}
function onError(error) {
  //alert(error);
}
function SetUserFieldValue(fieldName, userName) {
 var _PeoplePicker = $("div[title='" + fieldName + "']");
 var _PeoplePickerTopId = _PeoplePicker.attr('id');
 var _PeoplePickerEditer = $("input[title='" + fieldName + "']");
 _PeoplePickerEditer.val(userName);
 var _PeoplePickerOject = SPClientPeoplePicker.SPClientPeoplePickerDict[_PeoplePickerTopId];
 _PeoplePickerOject.AddUnresolvedUserFromEditor(true);
}
GetCurrentUser();
});
</script>