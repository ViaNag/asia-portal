var Viacom = Viacom || {};
Viacom.APAC = Viacom.APAC || {};


Viacom.APAC.customItemOperationContactHtml = function (ctx) {
    var header = '<div class="content-padding"><table style="width:100%"><tbody><tr><th title="ID number of issue">Issue Id</th><th title="Title of issue">Title</th><th  title="Type of Issue">Category</th><th title="Detailed description of issue">Detailed Description</th><th title="Territory/Cluster where issue was reported">Territory/Cluster</th><th title="Priority of issue">Business Impact</th><th title="User who has reported the issue">Reported by</th><th title="Documents attached to issue">Attachment</th><th title="Date issue was submitted">Issue Logged Date</th><th title="User assigned to issue based on category selected">Assigned to</th><th title="New user assigned to issue">Reassigned to</th><th title="Target date the issue will be resolved">Target Resolution Date</th><th title="Current Status of issue">Status</th><th title="Description of solution">Resolution/Remarks</th><th title="Last date the item was modified">Last Modified Date</th></tr>';
    return header;

};

(function () {
    var overrideCtx = {};
    overrideCtx.Templates = {};
    overrideCtx.Templates.Header = Viacom.APAC.customItemOperationContactHtml;
    //overrideCtx.Templates.Item = RenderItemTemplate(overrideCtx) + '</tbody></table></div></div>';
    overrideCtx.Templates.Footer = '</tbody></table></div>';
    overrideCtx.BaseViewID = 1;
    overrideCtx.ListTemplateType = 100;

    SPClientTemplates.TemplateManager.RegisterTemplateOverrides(overrideCtx);
})();

