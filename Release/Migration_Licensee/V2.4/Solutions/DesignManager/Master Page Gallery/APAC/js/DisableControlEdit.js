//$(document).ready(function(){
$(window).load(function(){
$("input[Title='Territory/Cluster Required Field']").attr('disabled','true');
$("input[Title='Issue ID']").attr('disabled','true');
$("input[Title='Title Required Field']").attr('disabled','true');
$("select[Title='Category Required Field']").attr('disabled','true');
$("select[Title='Business Impact Required Field']").attr('disabled','true');
$("input[Title='Issue Logged Date Required Field']").attr('disabled','true');
$("textarea[Title='Detailed Description Required Field']").attr('disabled','true');	
$("select[Id*='DateTimeFieldDateHours']")[0].disabled = true;
$("select[Id*='DateTimeFieldDateMinutes']")[0].disabled = true;
$("img[Id*='DateTimeFieldDateDatePickerImage']")[0].style.display="none";
$("input.sp-peoplepicker-editorInput[title='Assigned To, Enter a name or email address...']").attr("disabled","disabled")
$("input.sp-peoplepicker-editorInput[title='Pre-Reassignment, Enter a name or email address...']").attr("disabled","disabled")
$("input.sp-peoplepicker-editorInput[title='Reported By Required Field, Enter a name or email address...']").attr("disabled","disabled")

$("div[title='Assigned To']").find('.sp-peoplepicker-delImage').hide();
$("div[title='Reported By Required Field']").find('.sp-peoplepicker-delImage').hide();
$("div[title='Pre-Reassignment']").find('.sp-peoplepicker-delImage').hide();
});