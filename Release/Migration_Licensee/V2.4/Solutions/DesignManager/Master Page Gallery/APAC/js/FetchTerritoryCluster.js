

$(document).ready(function () { ExecuteOrDelayUntilScriptLoaded(APAC.IssueNew.GetUserGroup, "sp.js"); });

	 
	
	var APAC = APAC || {};
	var grpId="";
	var grpTitle="";
	var allGroups="";
	APAC.Constants = {

		TerritoryClusterList : "Territory Cluster"
		
		
	}
	APAC.IssueNew = {
		GetUserGroup:function(){
			
			var clientContext = new SP.ClientContext.get_current();
			var oWeb = clientContext.get_web();
			var currentUser = oWeb.get_currentUser();
			allGroups = currentUser.get_groups();
			clientContext.load(allGroups);

			clientContext.executeQueryAsync(APAC.IssueNew.OnSuccess, APAC.IssueNew.OnFailure);
		},
		OnSuccess:function(sender, args)
			{
				var grpsEnumerator = allGroups.getEnumerator();
				while(grpsEnumerator.moveNext())
				{
					//This object will have the group instance
					//which the current user is part of. 
					//This is can be processed for further 
					//operations or business logic.
					var group = grpsEnumerator.get_current();

					//This gets the title of each group
					//while traversing through the enumerator.			
					grpId = group.get_id();
					grpTitle = group.get_title();
					APAC.IssueNew.GetTerritoryCluster(grpId,grpTitle);
				}
			},
		OnFailure:function(sender, args)
			{
				console.log(args.get_message());
			},
		GetTerritoryCluster: function (grpId,grpTitle) {
			
			
			var UserId = _spPageContextInfo.userId;
			
			var ShowTerritoryClusterQuery = _spPageContextInfo.siteAbsoluteUrl+ "/_api/web/lists/getbytitle('"+APAC.Constants.TerritoryClusterList+"')/Items?Select=ILT_UsersNameId&$filter=(ILT_UsersName eq "+grpId+")";
			//?$select="+APAC.Constants.BrandLookUpColor +"&$expand="+APAC.Constants.BrandLookup;
			
			$.ajax({
				url: ShowTerritoryClusterQuery,
				type: "GET",
				headers: { "accept": "application/json;odata=verbose" },
				success: function (data) {
					if(data.d.results.length > 0)
					{
						var territory = data.d.results[0].ILT_TerritoryCluster;
						if(territory!="")
						$("input[Title='Territory/Cluster Required Field']").val(territory);
						$("input[Title='Territory/Cluster Required Field']").attr("disabled","disabled");
						 var _PeoplePicker = $("div[title='Territory/Cluster Group Required Field']");
						 var _PeoplePickerTopId = _PeoplePicker.attr('id');
						 var _PeoplePickerEditer = $("input[title*='Territory/Cluster Group']");
						 _PeoplePickerEditer.val(grpTitle);
						 var _PeoplePickerOject = SPClientPeoplePicker.SPClientPeoplePickerDict[_PeoplePickerTopId];
						 _PeoplePickerOject.AddUnresolvedUserFromEditor(true);
						 $("nobr:contains('Territory/Cluster Group')").closest("tr").hide();
					}
				},
				error: function (xhr) {
					console.log(xhr.status);
					
				}
			});  
		}
			
		}
	

	