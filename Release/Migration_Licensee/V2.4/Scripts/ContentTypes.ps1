﻿# =================================================================================
#
#Main Function
#
# =================================================================================
function CreateContentTypes([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)
    if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
	{    
		  Add-PsSnapin Microsoft.SharePoint.PowerShell
	}

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $($ConfigPath) file"
     
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
            foreach($ct in $cfg.ContentTypes.ContentType)
            {
                ContentTypeToAdd $ct
            }
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host "Error: " $Error -ForegroundColor Red
        Write-Output "Error: " $Error
	}
}

# =================================================================================
#
# FUNC: ContentTypeToAdd
# DESC: Create contenttype if not exist else associate fields to it.#
# =================================================================================
function ContentTypeToAdd([object] $ct)
{
    try
    {
        $error.Clear()
        $site = new-object Microsoft.SharePoint.SPSite $ct.SiteURL
        $web = $site.RootWeb
        # Check if the contenttype already exist or not
        $ctype=$web.AvailableContentTypes[$ct.ContentTypeName]
		$actionType=$ct.ActionType

		if(![string]::IsNullOrEmpty($ct.PushChanges))
        {
            $PushChanges=[System.Convert]::ToBoolean($ct.PushChanges)
        }
        else
        {
            $PushChanges=$false
        }
		
        if(!$ctype)
        {
            $ctypeId=$null
            if([string]::IsNullOrEmpty($ct.Guid))
            {
	            $ctypeId = $web.AvailableContentTypes[$ct.ParentContentType]
            }
            else
            {
                $ctypeId = new-object Microsoft.SharePoint.SPContentTypeId($ct.Guid)
            }
            $ctype = new-object Microsoft.SharePoint.SPContentType($ctypeId, $web.contenttypes, $ct.ContentTypeName)
            $web.contenttypes.add($ctype) | Out-Null
            $web.update()
            Write-Host "Content type $($ct.ContentTypeName) added successfully at web : $($ct.SiteUrl)"  -ForegroundColor Green
            Write-Output "Content type $($ct.ContentTypeName) added successfully at web : $($ct.SiteUrl)" 
        }
        else
        {
            Write-Host "Content type $($ct.ContentTypeName) already exist at web :$($ct.SiteUrl)" -ForegroundColor Yellow
            Write-Output "Content type $($ct.ContentTypeName) already exist at web :$($ct.SiteUrl)"
        }
        $existingCtype=$web.ContentTypes[$ct.ContentTypeName]
        if(![string]::IsNullOrEmpty($ct.EnableDIP))				
        {
            $customXsnSchema = "http://schemas.microsoft.com/office/2006/metadata/customXsn"
                   
            #load old settings
            $customXsnXml = $existingCtype.XmlDocuments[$customXsnSchema];
            
            #load it in an Xml Document
            [System.Xml.XmlDocument]$xmlDoc = new-object System.Xml.XmlDocument;
            if(![string]::IsNullOrEmpty($customXsnXml)) 
            {            
                # delete the old one
                $existingCtype.XmlDocuments.Delete($customXsnSchema);
            }
            $customXsnXml="<customXsn xmlns=`"http://schemas.microsoft.com/office/2006/metadata/customXsn`"><xsnLocation></xsnLocation><cached>True</cached><openByDefault>" + $ct.EnableDIP +"</openByDefault><xsnScope></xsnScope></customXsn>";
            $xmlDoc.LoadXml($customXsnXml);                
           
            # add the new xml
            $existingCtype.XmlDocuments.Add($xmlDoc);
            # update the CT
            $existingCtype.Update($PushChanges);
        }
		if(![string]::IsNullOrEmpty($ct.ContentTypeGroupName))
		{
			$existingCtype.Group = $ct.ContentTypeGroupName
		}
		if(![string]::IsNullOrEmpty($ct.Description))
		{
			$existingCtype.Description=$ct.Description
		}
		if(![string]::IsNullOrEmpty($ct.JSLink))
		{
			$existingCtype.JSLink=$ct.JSLink
		}
		
		try
		{
			$field = $web.Fields.GetFieldByInternalName($ct.SiteColumn)
		}
	    catch
		{
			 $error.clear()
			 Write-Host "Field $($ct.SiteColumn) does not exist" -ForegroundColor Yellow
			 Write-Output "Field $($ct.SiteColumn) does not exist"
		}
		if($field)
		{
			if($actionType -eq "Add" -or $actionType -eq "Update")
			{
				if($actionType -eq "Add")
				{
					$fieldLink=New-Object Microsoft.SharePoint.SPFieldLink $field
					$existingCtype.FieldLinks.Add($fieldLink)			
					$existingCtype.Update($PushChanges)
					Write-Host "Field $($ct.SiteColumn) added to content type $($ct.ContentTypeName) successfully at web : $($ct.SiteUrl)" -ForegroundColor Green
					Write-Output "Field $($ct.SiteColumn) added to content type $($ct.ContentTypeName) successfully at web :$($ct.SiteUrl)" 
                   
				}
				
				if(![string]::IsNullOrEmpty($ct.Hidden))
				{
					[boolean]$existingCtype.FieldLinks[$ct.SiteColumn].Hidden = [System.Convert]::ToBoolean($ct.Hidden)
				}
				if(![string]::IsNullOrEmpty($ct.Required))
				{
					[boolean]$existingCtype.FieldLinks[$ct.SiteColumn].Required = [System.Convert]::ToBoolean($ct.Required)
				}
				if(![string]::IsNullOrEmpty($ct.ReadOnly))
				{
					[boolean]$existingCtype.FieldLinks[$ct.SiteColumn].ReadOnly = [System.Convert]::ToBoolean($ct.ReadOnly)
				}
				if(![string]::IsNullOrEmpty($ct.SiteColumnDisplayName))
				{
					$existingCtype.FieldLinks[$ct.SiteColumn].DisplayName = $ct.SiteColumnDisplayName
				}
				
				$existingCtype.Update($PushChanges)

                $docSet=$web.ContentTypes[$ct.ContentTypeName]
                $docSetId=[string]$docSet.Id
                if($docSetId.StartsWith("0x0120D520"))
                {
                    [Microsoft.Office.DocumentManagement.DocumentSets.DocumentSetTemplate]$docSetCT = [Microsoft.Office.DocumentManagement.DocumentSets.DocumentSetTemplate]::GetDocumentSetTemplate($docSet);
                    $SharedColumn=$null;
                    $AllowContentTypes=$null
                    $RemoveCOntenttypeDocSet=$null
                   
                    if(![string]::IsNullOrEmpty($ct.AllowedContentTypes))
				    {
                        #stor all allowContentType in array
                        $ctById = @{}
                        $existsAllowCt=$docSetCT.AllowedContentTypes.GetEnumerator()
                        $i=0
                        while($existsAllowCt.MoveNext())
                        {
                               $currentCT=$existsAllowCt.Current
                               $ctById.Add($i,$currentCT)
                               $i++
                        }

                        #Clear allowContentType
                        $docSetCT.AllowedContentTypes.Clear()

                        #Add AllowContentType from Config file
					    $AllowContentTypes = [string]$ct.AllowedContentTypes
                        $AllowContentTypes=$AllowContentTypes.Split(",")
                        foreach($addCT in $AllowContentTypes)
                        {
                            if(![string]::IsNullOrEmpty($addCT))
                            {
                                $docSetCT.AllowedContentTypes.Add($web.ContentTypes[$addCT].Id) | Out-Null;
                            }
                        }

                        #Add in allow ContentType which was cleared above
                        foreach($key in $ctById.Keys)
                        {
                            $docSetCT.AllowedContentTypes.Add($ctById[$key]) | Out-Null
                        }
				    }

                    

                    if(![string]::IsNullOrEmpty($ct.SharedColumn))
				    {
					    $SharedColumn = [System.Convert]::ToBoolean($ct.SharedColumn)
				    }

                    if($SharedColumn)
                    {
                        $docSetCT.SharedFields.Add($docSet.Fields.GetFieldByInternalName($ct.SiteColumn));
                    }
                    
                    if(![string]::IsNullOrEmpty($ct.RemoveContentTypesFromDocSet))
				    {
					    $RemoveContentTypesFromDocSet = [string]$ct.RemoveContentTypesFromDocSet
                        $RemoveContentTypesFromDocSet=$RemoveContentTypesFromDocSet.Split(",")
                        foreach($removeCT in $RemoveContentTypesFromDocSet)
                        {
                            if(![string]::IsNullOrEmpty($removeCT))
                            {
                                $docSetCT.AllowedContentTypes.Remove($web.ContentTypes[$removeCT].Id);
                            }
                        }
				    }

					#set  welcome page fields for Document set content type
					if(![string]::IsNullOrEmpty($ct.WelcomePageFields))
				    {
                        $welcomePageFields=([string]$ct.WelcomePageFields).Split(",")
						$docSetCT.WelcomePageFields.Clear()
						foreach($wcField in $welcomePageFields)
						{
							if(![string]::IsNullOrEmpty($wcField))
							{
								$docSetCT.WelcomePageFields.Add($docSet.Fields.GetFieldByInternalName($wcField));
							}
						}
				    }
                    $docSetCT.Update($PushChanges);
                }

				Write-Host "Field $($ct.SiteColumn) already exists in content type $($ct.ContentTypeName) updated successfully at web : $($ct.SiteUrl)" -ForegroundColor Green
				Write-Output "Field $($ct.SiteColumn) already exists in content type $($ct.ContentTypeName) updated successfully at web : $($ct.SiteUrl)"
			}
			elseif($actionType -eq "Remove")
			{
                $webAppUrl=$web.Site.WebApplication.GetResponseUri([Microsoft.SharePoint.Administration.SPUrlZone]::Default).AbsoluteUri
                $webAppUrl=$webAppUrl.Substring(0,$webAppUrl.LastIndexOf("/"))
				$usages = [Microsoft.SharePoint.SPContentTypeUsage]::GetUsages( $existingCtype) | Where-Object { $_.IsUrlToList }
				
				#remove field link from content type for usage list
				$usages | ForEach-Object {
					$listUrl = $webAppUrl + $_.Url
					$listSite = New-Object Microsoft.SharePoint.SPSite($listUrl)
					$listWeb = $listSite.OpenWeb()
					$list = $listWeb.GetList($listUrl)
                    if($List.Fields.ContainsField($ct.SiteColumn))
                    {
                        $listField = $list.Fields.GetFieldByInternalName($ct.SiteColumn)
					}
					$lstContentType = $list.ContentTypes[$_.Id]
                    if($listField)
                    {
					    $lstContentType.FieldLinks.Delete($listField.Id) 
				        $lstContentType.Update()
					    $list.Update()
					    $listWeb.Dispose()
					    $listSite.Dispose()
					    Write-Host "Field $($ct.SiteColumn) remove from content type $($ct.ContentTypeName) successfully for list $($list.Title)  at web : $($ct.SiteUrl)"  -ForegroundColor Green
					    Write-Output "Field $($ct.SiteColumn) remove from content type $($ct.ContentTypeName) successfully for  list $($list.Title) at web : $($ct.SiteUrl) " 
                    }	
				}
				
				$existingCtype.FieldLinks.Delete($field.Id) 
				$existingCtype.Update($PushChanges)
				Write-Host "Field $($ct.SiteColumn) remove from content type $($ct.ContentTypeName) successfully at web :$($ct.SiteUrl)" -ForegroundColor Green
				Write-Output "Field $($ct.SiteColumn) remove from content type $($ct.ContentTypeName) successfully at web :$($ct.SiteUrl)"				
			}
			
			$web.Dispose()
		}
	}
    catch
    {
        Write-Host "`nException for contentType :" $ct.ContentTypeName "`n" $Error -ForegroundColor Red
        Write-Output "`nException for contentType :" $ct.ContentTypeName "`n" $Error
    }
}