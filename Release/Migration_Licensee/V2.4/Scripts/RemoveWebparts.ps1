﻿# ===================================================================================
# FUNC: UpdatePageLayout
# DESC: Update Page layouts
# ===================================================================================
 function RemoveWebparts([String]$ConfigFileName = "")
 {
     if (-not $(Test-Path -Path $configFileName -Type Leaf))
	{
		Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
	}

	$configXml = [xml]$(get-content $configFileName)
	
	if( $? -eq $false ) 
	{
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..." | Out-Null
	}

	foreach($Page in $configXml.Configuration.Pages.Page)     
	{
		Removewebpart $configXml.Configuration.PublishingSite.URL $Page.WebpartName $Page.Name
	}
}
function Removewebpart([string]$WebURL, [string]$WebpartName,[string]$PublishingPageName)
{
     $Web = Get-SPWeb $WebURL
     #Get Publishing Site and Web
     $PublishingSite = New-Object Microsoft.SharePoint.Publishing.PublishingSite($Web.Site)
     $PublishingWeb = [Microsoft.SharePoint.Publishing.PublishingWeb]::GetPublishingWeb($web)

     $pageUrl=$WebURL + "/Pages/" + $PublishingPageName
     $publishingPages = $PublishingWeb.GetPublishingPages()
     $PublishingPage = $PublishingWeb.GetPublishingPage($pageUrl)       
     $PublishingPage.CheckOut()

     #Initialise the Web part manager for the specified profile page.
	 $spWebPartManager = $Web.GetLimitedWebPartManager($pageUrl, [System.Web.UI.WebControls.WebParts.PersonalizationScope]::Shared)

    #Remove the Web part from that page
	foreach ($webpart in ($spWebPartManager.WebParts | Where-Object {$_.Title -eq $WebpartName}))
	{
		write-host ": Existing Web part - " + $webpart.Title + " : " + $webpart.ID
		#Delete the existing webpart
		$spWebPartManager.DeleteWebPart($spWebPartManager.WebParts[$webpart.ID])
	}
		
     write-host "Deleted the web parts."
     $PublishingPage.CheckIn("Page layout Updated via PowerShell")
     $PublishingPage.ListItem.File.Publish("Published via PowerShell")
 }