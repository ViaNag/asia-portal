﻿# ==============================================================================================================================================================================================================================================================
#
# FUNC: AssociateRetentionToCT
# DESC: Function to associate rentemtion policy to content types. Policy to move document to recycle bin when document modified date is greater or equal to 5 year + Modified date will be automatically added at second stage for each content type as default.
# ==============================================================================================================================================================================================================================================================
function AssociateRetentionToCT([string]$ConfigPath = "")
{
$Error.Clear()
$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file`n" 
	if($Error.Count -eq 0)
	{
        $CTGroups=$cfg.Retentions.Retention | Group CTName, Url
        foreach($CTGroup in  $CTGroups)
        {
            $error.Clear()
            try
            {
                $ContentTypeName=$CTGroup.Name.Split(",")[0]
                $SiteColl=$CTGroup.Name.Split(",")[1]
                if(![string]::IsNullOrEmpty($SiteColl))
                {
                    $site = get-spsite $SiteColl
		            $web = $site.openweb()
                    $webApp=$site.WebApplication
                    if(![string]::IsNullOrEmpty($ContentTypeName))
                    {
                        $contentType = $web.ContentTypes[$ContentTypeName];
                        if($contentType -ne $null)
                        {
                            $newPolicy = [Microsoft.Office.RecordsManagement.InformationPolicy.Policy]::GetPolicy($contentType);
                            if($newPolicy -eq $null)
                            {
		                        [Microsoft.Office.RecordsManagement.InformationPolicy.Policy]::CreatePolicy($contentType, $null);
		                        $newPolicy = [Microsoft.Office.RecordsManagement.InformationPolicy.Policy]::GetPolicy($contentType);
                            }
                            $expirypolicyexists=$false;
                            $existingPolicy=$null;
                            if ($newPolicy.Items.Count -ne 0)
                            {
                                foreach ($policyitem in $newPolicy.Items)
                                {
                                    if ($policyitem.Name -eq "Retention")
                                    {
                                        $expirypolicyexists=$true;
                                        $existingPolicy=$policyitem;
                                    }
                                }
                            }
                            if($expirypolicyexists -eq $false)
                            {
                                $policyXml="<Schedules nextStageId='3'>"+
		                            "<Schedule type='Default'>"+
		                            "<stages>"+
		                            "<data stageId='1' stageDeleted='true'></data>";
                                $stageId=2;
                                foreach($stage in $CTGroup.Group)
                                {
                                    $Period=$stage.Period;
		                            $Property=$stage.Property;
		                            $PeriodType=$stage.PeriodType;
                                    $ActionType=$stage.ActionType;
                                    $ActionId=$stage.ActionId;
                                    $destId=$null
                                    foreach($officialFileHost in $webApp.OfficialFileHosts)
                                    {
                                        if($officialFileHost.OfficialFileName -eq $ActionType)
                                        {
                                            $destId=$officialFileHost.UniqueId.ToString()
                                        }
                                    }
		                            $policyXml=$policyXml + "<data stageId='"+  $stageId +"'>"+
		                            "<formula id='Microsoft.Office.RecordsManagement.PolicyFeatures.Expiration.Formula.BuiltIn'>"+
		                            "<number>"+$Period+"</number>"+"<property>"+$Property+"</property>"+
		                            "<period>"+$PeriodType+"</period>"+
		                            "</formula>"+
		                            "<action type='action' id='" +$ActionId + "'";
                                    if(![string]::IsNullOrEmpty($destId))
                                    { 
                                        $policyXml= $policyXml + " destnId='" + $destId + "'";
                                    }
                                    $policyXml= $policyXml +" destnExplanation='Send To Viacom Record Center' destnName='" + $ActionType + "' />"+
		                            "</data>"+
                                    $stageId++;
                                }
		                        $policyXml=$policyXml + "</stages>"+
		                                    "</Schedule>"+
		                                    "</Schedules>";
                                $newPolicy.Items.Add("Microsoft.Office.RecordsManagement.PolicyFeatures.Expiration",$policyXml);
		                        $newPolicy.Update();
		                        write-host "$($ContentTypeName) : Retention Policy assigned successfully." -foregroundcolor green
                                write-output "$($ContentTypeName) : Retention Policy assigned successfully."
                            }
                            else
                            {
                                write-host "$($ContentTypeName) : Retention Policy already exist!." -foregroundcolor yellow
                                write-output "$($ContentTypeName) : Retention Policy already exist!."
                            }
                        }
                        else
                        {
                           write-host "$($ContentTypeName) : content type soes not exist!" -foregroundcolor yellow
                           write-output "$($ContentTypeName) : content type soes not exist!"
                        }
                        
                    }
                    else
                    {
                        write-host "Content Type Name is empty for content type $($ContentTypeName) so retention cannot be set." -foregroundcolor yellow
                        write-output "Content Type Name url is empty for content type $($ContentTypeName) so retention cannot be set."
                    }
                }
                else
                {
                     write-host "Site collection url is empty for content type $($ContentTypeName) so retention cannot be set." -foregroundcolor yellow
                     write-output "Site collection url is empty for content type $($ContentTypeName) so retention cannot be set."
                }
            }
            catch
             {
                write-host "Exception : Setting Retention Policy for  $($CTGroup.Name.Split(",")[0])." $Error -foregroundcolor Red
                write-output "Exception : Setting Retention Policy for  $($CTGroup.Name.Split(",")[0])." $Error
             }
        }
	 }
 }
