$ver = $host | select version
if ($ver.Version.Major -gt 1) {$host.Runspace.ThreadOptions = "ReuseThread"} 
if ((Get-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue) -eq $null) 
{
    Add-PSSnapin "Microsoft.SharePoint.PowerShell"
}

function SetLibrarySettings([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file"
	
    try
    {
        foreach($currentNode in $cfg.ColumnIndexes.ColumnIndex)   
        {
			try 
			{
				if([string]::IsNullOrEmpty($currentNode.SiteURL))
				{
					Write-Host "Error: The SiteURL is empty ..." -ForegroundColor Red
					Write-Output "Error: The Site URL is empty"
				}
				else
				{
					$web =  Get-SPWeb $currentNode.SiteURL
					$list = $web.Lists[$currentNode.LibraryName];
					
					if($list)
					{
						if ($currentNode.ActionType -eq "Add")
						{
							if($currentNode.UniqueColumnName)
							{
								$uniqueColumn = $list.Fields[$currentNode.UniqueColumnName];
								$uniqueColumn.Indexed = $true;
								$uniqueColumn.EnforceUniqueValues = $true;
								$uniqueColumn.Update();
								
								Write-Host "Column has been made unique : " $currentNode.SiteURL $currentNode.LibraryName $currentNode.UniqueColumnName -ForegroundColor Green
								Write-Output "Column Indexes has been made unique : " $currentNode.SiteURL $currentNode.LibraryName $currentNode.UniqueColumnName
							}
							elseif ($currentNode.SecondaryColumn)
							{
								$primaryColumn = $list.Fields[[GUID]$currentNode.PrimaryColumn];
								#$primaryColumn.Indexed = $true;
								#$primaryColumn.Update = $true;
								
								$secondaryColumn = $list.Fields[[GUID]$currentNode.SecondaryColumn];
								#$secondaryColumn.Indexed = $true;
								#$secondaryColumn.Update = $true;
								
								$list.FieldIndexes.Add($primaryColumn, $secondaryColumn);
								
								Write-Host "Column Indexes has been enabled for : " $currentNode.SiteURL $currentNode.LibraryName $currentNode.PrimaryColumn $currentNode.SecondaryColumn -ForegroundColor Green
								Write-Output "Column Indexes has been enabled for : " $currentNode.SiteURL $currentNode.LibraryName $currentNode.PrimaryColumn $currentNode.SecondaryColumn
							}
							else
							{
								$primaryColumn = $list.Fields[[GUID]$currentNode.PrimaryColumn];
								$primaryColumn.Indexed = $true;
								$primaryColumn.Update();
								$list.FieldIndexes.Add($primaryColumn);
								
								Write-Host "Column Indexes has been enabled for : " $currentNode.SiteURL $currentNode.LibraryName $currentNode.PrimaryColumn $currentNode.SecondaryColumn -ForegroundColor Green
								Write-Output "Column Indexes has been enabled for : " $currentNode.SiteURL $currentNode.LibraryName $currentNode.PrimaryColumn $currentNode.SecondaryColumn
							}							
						}
						elseif ($currentNode.ActionType -eq "Delete")
						{
							if ($currentNode.SecondaryColumn)
							{
								$list.FieldIndexes.Delete([GUID] $currentNode.IndexID);
							}
							else 
							{
								$primaryColumn = $list.Fields[[GUID]$currentNode.PrimaryColumn];
								$primaryColumn.Indexed = $false;
								$primaryColumn.Update();
							}
						}
						else 
						{
							# This case should not happen as in excel sheet only 2 options are present and are mandatory
							Write-Host "Error: Action Type not found. Site: " $currentNode.SiteURL ", List:" $currentNode.LibraryName $currentNode.PrimaryColumn $currentNode.SecondaryColumn $currentNode.UniqueColumnName -ForegroundColor Red
							Write-Output "Error: Action type not found. Site:  " $currentNode.SiteURL " List:" $currentNode.LibraryName $currentNode.PrimaryColumn $currentNode.SecondaryColumn $currentNode.UniqueColumnName
						}
					}
					else
					{
						Write-Host "Error: List not found. Site: " $currentNode.SiteURL ", List:" $currentNode.LibraryName $currentNode.PrimaryColumn $currentNode.SecondaryColumn $currentNode.UniqueColumnName -ForegroundColor Red
						Write-Output "Error: List not found. Site:  " $currentNode.SiteURL " List:" $currentNode.LibraryName $currentNode.PrimaryColumn $currentNode.SecondaryColumn $currentNode.UniqueColumnName
					}
				}
			}
			catch
			{
				Write-Host "`nException :" $currentNode.SiteURL $currentNode.LibraryName $currentNode.PrimaryColumn $currentNode.SecondaryColumn "`n" $Error $currentNode.UniqueColumnName -ForegroundColor Red
				Write-Output "`nException :" $currentNode.SiteURL $currentNode.LibraryName $currentNode.PrimaryColumn $currentNode.SecondaryColumn "`n" $Error $currentNode.UniqueColumnName
			}
		}
	}
    catch
    {
        Write-Host "`nException :" $Error -ForegroundColor Red
        Write-Output "`nException :" $Error
    }
}