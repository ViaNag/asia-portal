﻿##################################################################################################
# ================================================================================================
# MAin function to create termset
# ================================================================================================
##################################################################################################


function SetTermsRecursive ($termstore,[Microsoft.SharePoint.Taxonomy.TermSetItem] $termsetitem, $parentnode)
{
	$parentnode.term |
		ForEach-Object {
			## create the term
			if($_ -ne $null)
			{
                $newterm=$null
                $newterm=$termsetitem.Terms[$_.name]
                if($newterm -eq $null)
                    {
                        $termName=$_.name
                        if($termName.contains("&quot;"))
                        {
                            $termName = $termName -replace "&quot;","＂"
                        }                       	
				
                        if($termName.contains("&"))
                        {
                            $termName = $termName -replace "&","＆"
                        }
                        
                        $newterm=$termsetitem.Terms[$termName]
                        if($newterm -eq $null)
                        { 
                            try
                            {
                                $termName=[System.Web.HttpUtility]::HtmlDecode($_.name)	
				                $newterm = $termsetitem.CreateTerm($termName, 1033)
                                $termstore.CommitAll()
				                Write-Host "Added term $($termName)" -ForegroundColor Green 
                                Write-Host "Added term $($termName)"
                            }
                            catch
                            {
                                 Write-Host "`nException for Creating term name $($termName) under TermSet Name : $($termSet.name) and  TermGroup :$($grpName.name) `n " $Error -ForegroundColor Red 
                                 Write-Output "`nException for Creating term name $($termName) under TermSet Name : $($termSet.name) and  TermGroup :$($grpName.name) `n "
                            }
                        }
                    }
				SetTermsRecursive $termstore $newterm $_
			}
		}
}

function CallCreateTermSets([string]$ConfigPath = "")
{
    #Start-SPAssignment -Global
    #Start-Transcript -Path ".\TermStores.log" -Force
    $cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file"
	if($Error.Count -eq 0)
	{
		try
        {
		    #Access the TermStore data
		    $TermStoreData = [xml](get-content $ConfigPath)
            $termStoreNm=$TermStoreData.Termstore
            $grpName=$termStoreNm.TermGroup
            if(![string]::IsNullOrEmpty( $termStoreNm.Site) -and ![string]::IsNullOrEmpty( $termStoreNm.Name))
            {
		        $site = Get-SPSite $termStoreNm.Site 
		        $session = new-object Microsoft.SharePoint.Taxonomy.TaxonomySession($site)
				if($termStoreNm.Name.contains("&"))
				{
					$termStoreNm.Name = $termStoreNm.Name -replace "&","＆"
				}
		        $termstore = $session.TermStores[$termStoreNm.Name]
			    if($termstore)
			    {
                    if($termStoreNm)
                    {
					    $termStoreGroups=$termStoreNm.TermGroup
					    if($termStoreGroups)
					    {
						    foreach($grpName in $termStoreGroups)
						    {
							    $grpName.name=[System.Web.HttpUtility]::HtmlDecode($grpName.name)
								if($grpName.name.contains("&"))
								{
									$grpName.name = $grpName.name -replace "&","＆"
									$group=$termstore.Groups[$grpName.name]
								}
							    #if([string]$grpName.name -eq "M&E Ad Sales")
							    #{
							    #   $group=$termstore.Groups["M＆E Ad Sales"]
							    #}
							    else
							    {
								    $group=$termstore.Groups[$grpName.name]
							    }
							    if ($group -ne $null)
							    {
								    $group=$termstore.Groups[$grpName.name]
							    }
							    else
							    {
								    $group=$termStore.CreateGroup($grpName.name)
								    $termStore.CommitAll()
							    }
							    foreach($termSetNm in $grpName.Termset)
							    {
								    ## create the termset
								    $group=""
								    #if([string]$grpName.name -eq "M&E Ad Sales")
								    #{
								    #   $group=$termstore.Groups["M＆E Ad Sales"]
								    #}
									if($grpName.name.contains("&"))
									{
										$grpName.name = $grpName.name -replace "&","＆"
										$group=$termstore.Groups[$grpName.name]
									}
								    else
								    {
									    $group=$termstore.Groups[$grpName.name]
								    }
								    if ($group -ne $null)
								    {
										if($termSetNm.name.contains("&"))
										{
											$termSetNm.name = $termSetNm.name -replace "&","＆"
										}
									    $termSet=$group.TermSets[$termSetNm.name]
									    if($termSet -eq $null)
										    {
											    $termSet=$group.CreateTermSet($termSetNm.name)
											    $termStore.CommitAll()
											    Write-Host "Added termset $($termSet.name)" -ForegroundColor Green 
											    Write-Output "Added termset $($termSet.name)"
										    }
									    SetTermsRecursive $termstore $termSet $termSetNm
								    }
								
							    }
						    }
					    }
                    }
				    $termstore.CommitAll()
			    }
			    else
			    {
				    Write-Host -ForegroundColor Green "Termstore not found with name $($termStoreNm.Name)"
				    Write-Output "Termstore not fouund with name $($termStoreNm.Name)"
			    }
            }
        }
        catch
        {            
            Write-Host "`nException for TermGroup :$($grpName.name) , TermSet Name : $($termSet.name) `n " $Error -ForegroundColor Red 
            Write-Output "`nException for TermGroup :$($grpName.name) , TermSet Name : $($termSet.name) `n" $Error
        }
	}
}

