# =================================================================================
#
#Main Function to create Left Navigation
#
# =================================================================================
function AddNewItemToTopNavigation([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..."
	}
	
	Write-Host "Successfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Successfully read config file $ConfigPath file`n"
	if($Error.Count -eq 0)
	{
        try
        {
            $error.Clear()
            foreach($gNav in $cfg.GlobalNavigations.GlobalNavigation)
            {
                $web=Get-SPWeb $gNav.SiteURL
                $CreateSPNavigationNode = [Microsoft.SharePoint.Publishing.Navigation.SPNavigationSiteMapNode]::CreateSPNavigationNode
                if([string]::isNullOrEmpty($gNav.LinkName))
			    {
                    
				    $newNode = $CreateSPNavigationNode.Invoke($gNav.HeaderName, $gNav.HeaderURL, [Microsoft.SharePoint.Publishing.NodeTypes]::Heading, $web.Navigation.TopNavigationBar)
				    $newNode.Properties["Audience"] = ";;;;" + $gNav.HeaderTargetAudience
				    $newNode.Update()
				    Write-Host "$($gNav.HeaderName): heading  Added Successfully." -ForegroundColor Green
				    Write-Output "$($gNav.HeaderName): heading  Added Successfully." 
                }
                else
                {
                    $GlobalNavigationNodes = $web.Navigation.TopNavigationBar
                    $ParentNode = $GlobalNavigationNodes | Where-Object { $_.Title -eq $gNav.HeaderName }
                    #$ParentNode=[Microsoft.SharePoint.Client.NavigationNode]$gNav.HeaderName
                    $NodeCollection = $ParentNode.Children
                    $linkNode = $CreateSPNavigationNode.Invoke($gNav.LinkName, $gNav.LinkURL, [Microsoft.SharePoint.Publishing.NodeTypes]::AuthoredLinkPlain, $NodeCollection)
					$linkNode.Properties["Audience"] = ";;;;" + $link.LinkTargetAudience
					$linkNode.Update()  
                }

				$web.Update()
                $web.Dispose()    
            }
        }
        catch
        {
            Write-Host "`nException in AddNewItemToTopNavigation method :" $Error -ForegroundColor Red
            Write-Output "`nException in AddNewItemToTopNavigation method :" $Error
        }
	}
	else
	{
		Write-Host $Error -ForegroundColor Red
        Write-Output $Error
	}
}