﻿# =================================================================================
#
#Main Function for create Orginaizer rule
#
# ================================================================================
function CreateContentOrganizerRule([string]$ConfigPath = "")
{
	$Error.Clear();
	$config = [xml](get-content $ConfigPath)
    if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
	{    
		  Add-PsSnapin Microsoft.SharePoint.PowerShell
	}
    
	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
        $msg = "Could not read config file: " + $ConfigPath + ". Exiting ..."
		Logging $msg "Red"
	}
	
	$msg = "Successfully read config file: " + $ConfigPath 
	Logging $msg "Green"
	
	if($Error.Count -eq 0)
	{
        try
        {	
			$Error.Clear()
			$orgRulesConfig = $config.RecordConfigurations.OrganizerRules
			foreach($orgRuleConfig in $orgRulesConfig.OrganizerRule)	
			{	
				$caWebUrl = $orgRuleConfig.CentralAdminUrl
				$targetWebUrl = $orgRuleConfig.TargetWebUrl
				$termstorename = $orgRuleConfig.MMDServiceName
				$caWeb = Get-SPWeb $caWebUrl
				
				#overwrite existing rule or not
				[boolean]$overwrite = [System.Convert]::ToBoolean(@{$true="false";$false=$orgRuleConfig.IsOverwriteExistingRule}[$orgRuleConfig.IsOverwriteExistingRule -eq ""])
				
				$orgruleCSVFilePath = $currentLocation + "\configuration\" +  $operationFolder + "\" + $orgRuleConfig.ConfigFileName
				
				$orgruleCSV = Import-Csv $orgruleCSVFilePath
				$msg = "Successfully read organizerRule CSV file and file path is " + $orgruleCSVFilePath 
				Logging $msg "Green"
				
				#get target web object
				$targetWeb = Get-SPWeb $targetWebUrl
							
				#import rules
				CreateRulesFromExcelFile $targetWeb $caWeb $termstorename $overwrite $orgruleCSV				
			}
			
		}
		catch
        {
            $msg = "Exception :" + $Error 
			Logging $msg "Red"
        }
	}
	else
	{
		$msg = "Exception :" + $Error 
		Logging $msg "Red"
	}
}


##Function CreateRulesFromExcelFile
##$caWeb - web object of Central Administration
##$WebUrl - Target web object
##$termstorename - Managed metadata service name
##$overwrite - true in case Overwrite Existing Rule otherwise false
##$orgruleCSV - content organizer rules csv file content
##This function will create content organizer rules from an Excel file 
function CreateRulesFromExcelFile($web,$caWeb,$termStoreName,$overwrite,$orgruleCSV)
{    
    #loop through each rule and create them
   
    #initialize row counter
    $k = 0

    foreach($orgrule in $orgruleCSV){
        #flag to skip creation if an error appears in the conditions or when deleting an existing rule
        $errorFlag = $false
                
        $RuleName = $orgrule.Name
        $RuleEnabled = $orgrule.Enabled
        $RuleDescription = $orgrule.Description
        $RulePriority = $orgrule.Priority
        $RuleCT = $orgrule.ContentType
        $RuleAliases = $orgrule.Aliases
        $RuleExternalLocation = $orgrule.ExternalLocation
        $RuleTargetLibrary = $orgrule.TargetLibrary
        $RuleFolder = $orgrule.TargetFolder
        $RuleAC = $orgrule.AutoCreateProperty
        $RuleACF = $orgrule.AutoCreateFormat

        Logging "Reading rule $RuleName from csv Config file..." "Cyan"

        if($RuleEnabled -eq "true") {
            $enab = $true
        } else {
            $enab = $false
        }
        
        $ct = $web.Site.RootWeb.ContentTypes[$RuleCT]
        #Write-Host "RuleCT is" $RuleCT
        #Write-Host "ct is" $ct
        
        $ali = $RuleAliases
        $lib = $RuleTargetLibrary
        
        if($RuleFolder) {
            $fold = $RuleFolder
        } else {
            $fold = $null
        }
        
        if($RuleAC) {
            $ac = @{}
            $ac["Name"] = $RuleAC
            $ac["Format"] = $RuleACF
        } else {
            $ac = $null
        }
		
        #get the conditions
        if($orgrule.ConditionProperty1 -ne "" -and $orgrule.ConditionProperty1 -ne $null) {
            $conds = @{}
            #$i=11
            $j=1
            $ConditionProperty = "ConditionProperty" + $j
            while ($orgrule.$ConditionProperty -ne "" -and $orgrule.$ConditionProperty -ne $null -and $j -le 6){
                #$_.Conditions | foreach-object {
                #for($i = 10; $i -le 6; $i++) {
                    #$nodeName = "Condition" + $i
                    $ConditionProperty = "ConditionProperty" + $j
                    #$propIndex = $i
                    $ConditionOperator = "ConditionOperator" + $j
                    #$operIndex = $i + 1
                    $ConditionValue = "ConditionValue" + $j
                    #$valIndex = $i + 2
                    
                    #break the for loop if the condition property is $null
                    try {
                        $testNode = $orgrule.$ConditionProperty
                    } catch [System.Exception] {
                        Logging "Exception: In Configuration file row $k - Rule condition $ConditionProperty is null" "red"
                        break
                    }

                    #break the for loop if the condition property name is blank
                    if($orgrule.$ConditionProperty -eq "") {
                        Logging "Exception: In Configuration file row $k - Rule condition $ConditionProperty is blank" "red"
                        break
                    }
                    
                    $conds[$j-1] = @{}
                    $conds[$j-1]["Name"] = $orgrule.$ConditionProperty
                    $conds[$j-1]["Oper"] = $orgrule.$ConditionOperator
                    
                    $INameField = $web.Site.RootWeb.Fields.GetFieldByInternalName($conds[$j-1]["Name"])  
                    #check if the column is managed metadata and format the value accordingly
                    if($INameField.GetType().Name -eq "TaxonomyField") { 

                        #check for term name issue for '&'
                        #if($orgrule.$ConditionValue -like "M&E Ad Sales/*") {
                        #    $orgrule.$ConditionValue = $orgrule.$ConditionValue -ireplace "M&E Ad Sales","M＆E Ad Sales"
                        #}
                        if($orgrule.$ConditionValue.contains("&")) {
                            $orgrule.$ConditionValue = $orgrule.$ConditionValue -ireplace "&","＆"
                        }

                        $valueString = createMMValueString $caWeb $termStoreName $conds[$j-1]["Name"] $orgrule.$ConditionValue
                        if($valueString["status"] -eq "OK") {
                            $conds[$j-1]["Val"] = $valueString["result"]
                        } else {
                            $errorFlag = $true
                            Logging $valueString["status"] "red"
                            return
                        }
                    } else {
                        $conds[$j-1]["Val"] = $orgrule.$ConditionValue
                    }
                    $Name = $conds[$j-1]["Name"]
                    $Operator = $conds[$j-1]["Oper"]
                    $Value = $conds[$j-1]["Val"]
                    Logging "Condition property $j is $Name" 
                    Logging "Condition operator $j is $Operator" 
                    Logging "Condition value $j is $Value"
                
                $condscount = $conds[$j-1].Count
                #Logging "Number of condition properties is $condscount"
                #$i=$i+3 #increment index of org rule condition
                $j += 1
                $ConditionProperty = "ConditionProperty" + $j #"get" next condition property
            }
			if ($orgrule.ConditionOperator1 -eq "" -or $orgrule.ConditionOperator1 -eq $null)  {#ConditionOperator1 in excel spreadsheet is not empty
				$conds = $null
				$errorFlag = $true
				Logging "Error: Condition property is empty but condition operator is specified. $($orgrule.ConditionOperator1)" "Red"
			}
			elseif (($orgrule.ConditionValue1 -eq "" -or $orgrule.ConditionValue1 -eq $null) -and ($orgrule.ConditionOperator1 -eq "IsEmpty" -or $orgrule.ConditionOperator1 -eq "IsNotEmpty"))  {#ConditionValue1 in excel spreadsheet is not empty
				$conds = $null
				$errorFlag = $true
				Logging "Error: Condition property is empty but condition value is specified" "Red"
			}			
        }
		else  {#conditionproperty1 in excel spreadsheet is empty
            $conds = $null
			Logging "Condition property is empty." "Yellow"
        }
        
            if($errorFlag -eq $false -and $conds -ne $null) {
                Logging "Conditions for rule: OK" "Green"
            }
        
            #if overwrite set to $true, delete the existing rule if it exists
            if($overwrite) {
                #check if a rule with the name $name already exists
                foreach($item in $web.Lists["Content Organizer Rules"].Items) {
                    if($item.Name -eq $RuleName) {
                        Logging "Deleting rule, $RuleName..."
                    
                        #try to delete the rule
                        try {
                            $item.Delete()
                            Logging "Deleted" "Green"
                            break
                        } catch [System.Exception] {
                            $errorFlag = $true
                            Logging $_.Exception.Message "Red"
                        }
                    }
                }            
            }
        
            #create the rule only if no errors occured in the conditions or when deleting an existing rule
            if($errorFlag -eq $false) {
                Logging "Creating rule, $RuleName..."
            
                #create rule
                $response = createOrgRule $web $RuleName $RuleDescription $RulePriority $ct $ali $conds $lib $fold $ac $enab $RuleExternalLocation
                
                if($response["status"] -eq "OK") {
                    Logging "$RuleName was created successfully" "Green"
                } else {
                    Logging $response["status"] "Red"
                }
            }
    Logging ""
    }
}


#createOrgRule - creates an organization rule in a specific site
#arguments:
#  $web           - SPWeb object of the site where rule will be created; Required
#  $name          - Name of the rule; Required
#  $description   - Description of the rule
#  $priority      - Priority of the rule; Required
#  $contentType   - SPContentType object of the content type the rule will run on; Required
#  $alias         - Alias of the content type in a string; Multiple aliases delimited with a '/'
#  $conditions    - Hash table holding the properties of the conditions
#                   Table must be formatted as follows
#                         $var[#]["Name"]
#                         $var[#]["Oper"]
#                         $var[#]["Val"]
#                   Where # is the condition number (even if only one condition)
#                   Name is the display name of the field
#                   Val is optional if Name is "Content Type"
#  $targetLibrary - The display name of the target folder; Required
#  $targetFolder  - The string of the target folder relative path; ex. 'Folder 1/Sub Folder'
#                   Set to $null if destination is the top level of the library
#  $autoCreate    - Hash table holding the properties of the auto create folder
#                   Table must be formatted as follows
#                         $var["Name"]
#                         $var["Format"]
#                   Where Name is the display name of the field
#                   Format is the SharePoint-defined format string
#  $enabled       - Boolean that determines if a rule should be enabled when created; Defaults to $true
#  $externallocation - Value of the external routing location, if provided. If it is, target library and target folder will be ignored.
#
#return:
#  Returns the response hash table with the following properties
#          Result - ECMDocumentRouterRule object of the newly created rule
#          Status - OK if no errors, Error message otherwise
function createOrgRule($web,$name,$description,$priority,$contentType,$alias,$conditions,$targetLibrary,$targetFolder,$autoCreate,$enabled = $true, $externallocation="")
{
    #intialize the returned array
    $response = @{}
    $response["result"] = $null
    $response["status"] = $null
    
    #validate required fields are not $null
    if($web -eq $null -or $name -eq $null -or $priority -eq $null -or $contentType -eq $null -or $targetLibrary -eq $null) {
        $response["status"] = "One or more required parameters are null"
        Logging "web is $web"
        Logging "name is $name"
        Logging "priority is $priority"
        Logging "CT is $contenttype"
        Logging "library is $targetLibrary"
        return $response
    }

    #validate that $web is a SPWeb object
    if($web.GetType().Name -ne "SPWeb") {
        $response["status"] = "The web variable passed is not a valid SPWeb object"
        return $response
    }

    #validate that $web is configured for content organizer rules
    $routingWeb = New-Object Microsoft.Office.RecordsManagement.RecordsRepository.EcmDocumentRoutingWeb($web)
    if($routingWeb.IsRoutingEnabled -eq $false) {
        $response["status"] = "The site " + $web.URL + " is not enabled for content organizer rules"
        return $response
    }

    #check if a rule with the name $name already exists
    foreach($item in $web.Lists["Content Organizer Rules"].Items) {
        if($item.Name -eq $name) {
            $response["status"] = "A rule with the name $name already exists"
            return $response
        }
    }
    
    #validate that $priority is a number between 1 and 5
    if($priority -lt "1" -or $priority -gt "9") {
        $response["status"] = "The priority is not a value between 1 and 5"
        return $response
    }
    
    #validate that $contentType is a SPContentType object
    if($contentType.GetType().Name -ne "SPContentType") {
        $response["status"] = "The content type variable passed is not a valid SPContentType object"
        return $response
    }
    
    #validate that $contentType is a member of $web
    if($web.Site.RootWeb.ContentTypes[$contentType.Name] -eq $null) {
        $response["status"] = "The content type " + $contentType.Name + " is not a member of " + $web.URL
        return $response
    }
    
    #validate that $conditionS is an array with the required fields
    $conditionCount = $conditions.Count
    if($conditionCount -ne $null -and $conditionCount -ne "0") {

        #loop through each condition and check to see if the properties are null
        for($i = 1; $i -le $conditionCount; $i++) {
            #ensure there are exactly three properties

            if($conditions[$i-1].Count -ne "3") {
                $response["status"] = "The number of properties for condition $i is wrong"
                return $response
            }
            
            #check the three conditions
            if($conditions[$i-1]["Name"] -eq $null -or $conditions[$i-1]["Name"] -eq "") {
                $response["status"] = "The property name for condition $i is missing"
                return $response
            }
            if($conditions[$i-1]["Oper"] -eq $null -or $conditions[$i-1]["Oper"] -eq "") {
                $response["status"] = "The operator for condition $i is missing"
                return $response
            }
			
			if($conditions[$i-1]["Oper"] -ne "IsEmpty" -and $conditions[$i-1]["Oper"] -ne "IsNotEmpty")
			{
				if($conditions[$i-1]["Name"] -ne "Content Type" -and ($conditions[$i-1]["Val"] -eq $null -or $conditions[$i-1]["Val"] -eq "")) {
					$response["status"] = "The property value for condition $i is missing"
					return $response
				}
			}
            
            #ensure the condition property is part of the content type
            #$field = $contentType.Fields[$conditions[$i-1]["Name"]]
            $field = $contentType.Fields.GetFieldByInternalName($conditions[$i-1]["Name"])
            if($field -eq $null) {
                $response["status"] = "The field " + $conditions[$i-1]["Name"] + " in condition $i does not belong to the content type " + $contentType.Name
                return $response
            }
            
            #set the condition GUID and internal name
            $conditions[$i-1]["Name"] = $field.Title #overwrite in cases of case sensitivity
            $conditions[$i-1]["IntName"] = $field.InternalName
            $conditions[$i-1]["GUID"] = $field.ID
        }
    }
   
    #validate that $externallocation, if provided is a valid send to connection
    if($externallocation -ne ""){
        $webapp = $web.Site.WebApplication
        $OfficialFileHoststemp = $webapp.OfficialFileHosts | ?{ $_.OfficialFileName -eq $externallocation}
        if($OfficialFileHoststemp -eq $null){
            $response["status"] = "The external location $externallocation does not exist as a Send To connection. Check the Send To connections for the appropriate web application."
            return $response
        } else {
            Write-Host $OfficialFileHoststemp.OfficialFileName
        }
    }

    #validate that $targetLibrary exists
    if($externallocation -eq "" -and $targetLibrary -ne "" -and $web.Lists[$targetLibrary] -eq $null) {
        $response["status"] = "Exception: The library $targetLibrary does not exist in " + $web.URL
        return $response
    }
    
    #validate that $targetPath exists
    if($externallocation -eq "" -and $targetFolder -ne "" -and $web.GetFolder($web.Lists[$targetLibrary].RootFolder.ServerRelativeURL + "/" + $targetFolder).Exists -eq $false) {
        $response["status"] = "Exception: The target folder $targetFolder does not exist in library $targetLibrary"
        return $response
    }
    
    #validate that $contentType is a member of $targetLibrary
    if($externallocation -eq "" -and $targetLibrary -ne "" -and $web.Lists[$targetLibrary].ContentTypes[$contentType.Name] -eq $null) {
        $response["status"] = "Exception: The content type " + $contentType.Name + " is not associated with library $targetLibrary"
        return $response
    }
    
    #validate that $autoCreate is an array with the required fields
    $autoCreateCount = $autoCreate.Count
    if($autoCreateCount -ne $null -and $autoCreateCount -gt "0") {
        #ensure there are exactly two properites
        if($autoCreateCount -ne "2") {
            $response["status"] = "The number of properties for folder auto create is wrong"
            return $response
        }
        
        #check the two property fields
        if($autoCreate["Name"] -eq $null -or $autoCreate["Name"] -eq "") {
            $response["status"] = "The property name for the folder auto create is missing"
            return $response
        }
        if($autoCreate["Format"] -eq $null -or $autoCreate["Format"] -eq "") {
            $response["status"] = "The format for the folder auto create is missing"
            return $response
        }
        
        #ensure the auto create property is part of the content type
        $field = $contentType.Fields.GetFieldByInternalName($autoCreate["Name"])
        if($field -eq $null) {
            $response["status"] = "The field " + $autoCreate["Name"] + " in the auto create property does not belong to the content type " + $contentType.Name
            return $response
        }

        #set the auto create GUID and internal name     
        $autoCreate["Name"] = $field.Title #overwrite in cases of case sensitivity
        $autoCreate["IntName"] = $field.InternalName
        $autoCreate["GUID"] = $field.ID
    }    

    #create the org rule
    $rule = New-Object Microsoft.Office.RecordsManagement.RecordsRepository.EcmDocumentRouterRule($web)
    $rule.Name = $name
    $rule.Description = $description
    $rule.Priority = $priority
    $rule.ContentTypeString = $contentType.Name
    $rule.Aliases = $alias
    
    #add the conditions to the rule
    $conditionCount = $conditions.Count
    if($conditionCount -ne $null -and $conditionCount -ne "0") {
        $conditionXML = "<conditions>"
        #loop through each condition and create the xml string
        for($i = 0; $i -lt $conditionCount; $i++) {
            $conditionXML += "<Condition Column='" 
            $conditionXML += $conditions[$i]["GUID"]
            $conditionXML += "|"
            $conditionXML += $conditions[$i]["IntName"]
            $conditionXML += "|"
            $conditionXML += $conditions[$i]["Name"]
            $conditionXML += "' Operator='"
            $conditionXML += $conditions[$i]["Oper"]
            $conditionXML += "' Value='"
            $conditionXML += $conditions[$i]["Val"]
            $conditionXML += "' />"
        }
        $conditionXML = $conditionXML + "</conditions>"
        $rule.ConditionsString = $conditionXML
    }
    
    #set the target path
    if($externallocation -eq "" -or $externallocation -eq $null){
        $targetPath = $web.Lists[$targetLibrary].RootFolder.ServerRelativeURL + "/" + $targetFolder
        $rule.TargetPath = $targetPath
        $rule.RouteToExternalLocation = $false
    } else {
        $rule.RouteToExternalLocation = $true
        $rule.TargetPath = $externallocation
    }
        
    #add the auto create properties to the rule
    $autoCreateCount = $autoCreate.Count
    if($autoCreateCount -ne $null -and $autoCreateCount -gt "0") {
        $rule.AutoFolderSettings.AutoFolderPropertyName = $autoCreate["Name"]
        $rule.AutoFolderSettings.AutoFolderPropertyInternalName = $autoCreate["IntName"]
        $rule.AutoFolderSettings.AutoFolderPropertyId = $autoCreate["GUID"]
        $rule.AutoFolderSettings.AutoFolderFolderNameFormat = $autoCreate["Format"]
        $rule.AutoFolderSettings.Enabled = $true
    }
   
    $rule.CustomRouter = ""
    #$rule.RouteToExternalLocation = $false
    $rule.Enabled = $enabled

	#$rule
    $rule.Update()
    
    #return the results
    $response["result"] = $rule
    $response["status"] = "OK"
    return $response
}




#create the string required for the value when the column is managed metadata
function createMMValueString($web,$storeName,$columnName,$value)
{
    #intialize the returned array
    $response = @{}
    $response["result"] = $null
    $response["status"] = $null

    #validate that atleast a group, term set, and term has been passed
    $termArray = $value.split("/")
    if($termArray.Count -lt 3) {
        $response["status"] = "Exception: The column " + $columnName + " is a managed metadata column and the value " + $value + " is not formatted as 'Group/Term Set/Term/Term/etc.'"
        return $response
    }
    
    $term = getManagedTerm $web $storeName $value
    if($term["status"] -eq "OK") {
        $response["result"] = "1;#" + $term["result"].Name + "|" + $term["result"].ID
        $response["status"] = "OK"
        return $response
    } else {
        $response["status"] = $term["status"]
        return $response
    }
}

#getManagedTerm - returns the object of the managed group, term set, or term
#arguments:
#  $web       - SPWeb object of Central Administration
#  $storeName - The name of the term store
#  $term      - The managed term in the string form of 'Group/Term Set/Term/Term/etc.'
#return:
#  Returns the response hash table with the following properties
#          Result - TermStore, Group, TermSet, or Term object of the managed group, term set, or term
#          Status - OK if no errors, Error message otherwise
function getManagedTerm($web,$storeName,$term)
{
    #intialize the returned array
    $response = @{}
    $response["result"] = $null
    $response["status"] = $null
    
    #validate required fields are not $null
    if($web -eq $null -or $storeName -eq $null) {
        $response["status"] = "One or more required parameters are null"
        return $response
    }

    #validate that $web is a SPWeb object
    if($web.GetType().Name -ne "SPWeb") {
        $response["status"] = "The web variable passed is not a valid SPWeb object"
        return $response
    }
    
    #object for the managed termstore
    $taxonomy = new-object Microsoft.SharePoint.Taxonomy.TaxonomySession($web.Site, $true);

    #check if the store exists
	if($storeName.contains("&"))
	{
		$storeName = $storeName -replace "&","＆"
	}
    $termStore = $taxonomy.TermStores[$storeName];
    if($termStore -eq $null) {
        $response["status"] = "The term store $storeName does not exist"
        return $response
    }

    #return the term store id
    if($term -eq $null) {
        $response["result"] = $termStore
        $response["status"] = "OK"
        return $response
    }

    #split the term string into an array and count the number of elements
    $termArray = $term.split("/")
    $termCount = $termArray.count

    #check if the group exits
    $group = $termStore.Groups[$termArray[0]]
    if($group -eq $null) {
        $response["status"] = "The term group " + $termArray[0] + " does not exist"
        return $response
    }
    
    #return the group id
    if($termCount -eq "1") {
        $response["result"] = $group
        $response["status"] = "OK"
        return $response
    }
    
    #check if the termset exits
    $termset = $group.TermSets[$termArray[1]]
    if($termset -eq $null) {
        $response["status"] = "Exception: The term set " + $termArray[1] + " is invalid"
        return $response
    }

    #return the term set id
    if($termCount -eq "2") {
        $response["result"] = $termset
        $response["status"] = "OK"
        return $response
    }
    
    #loop through the remaining term hierarchy to check if each term exists
    for($i = 2; $i -lt $termCount; $i++) {
        $term = $termset.Terms[$termArray[$i]]
        #if the term does not exist, return the error message
        if($term -eq $null) {
            $response["status"] = "Exception: The term " + $termArray[$i] + " is invalid"
            return $response
        }
        
        #get the next term level
        $termset = $term
    }
    
    #return the term id
    $response["result"] = $term
    $response["status"] = "OK"
    return $response
}


# =================================================================================
#
#Main Function for delete Orginaizer rule
#
# ================================================================================
function DeleteContentOrganizerRule([string]$ConfigPath = "")
{
	$Error.Clear();
	$config = [xml](get-content $ConfigPath)
    if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
	{    
		  Add-PsSnapin Microsoft.SharePoint.PowerShell
	}
    
	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
        $msg = "Could not read config file: " + $ConfigPath + ". Exiting ..."
		Logging $msg "Red"
	}
	
	$msg = "Successfully read config file: " + $ConfigPath 
	Logging $msg "Green"
	
	if($Error.Count -eq 0)
	{
        try
        {	
			$Error.Clear()
			$orgRulesConfig = $config.RecordConfigurations.OrganizerRules
			foreach($orgRuleConfig in $orgRulesConfig.OrganizerRule)	
			{	
				$caWebUrl = $orgRuleConfig.CentralAdminUrl
				$targetWebUrl = $orgRuleConfig.TargetWebUrl
				$termstorename = $orgRuleConfig.MMDServiceName
				
				#isDelete existing rule or not
				[boolean]$isDelete = [System.Convert]::ToBoolean(@{$true="false";$false=$orgRuleConfig.isDelete}[$orgRuleConfig.isDelete -eq ""])
				
				$orgruleCSVFilePath = $currentLocation + "\configuration\" +  $operationFolder + "\" + $orgRuleConfig.ConfigFileName
				
				$orgruleCSV = Import-Csv $orgruleCSVFilePath
				$msg = "Successfully read organizerRule CSV file and file path is " + $orgruleCSVFilePath 
				Logging $msg "Green"
				
				#get target web object
				$targetWeb = Get-SPWeb $targetWebUrl
							
				#import rules
				DeleteRulesFromExcelFile $targetWeb $isDelete $orgruleCSV				
			}
			
		}
		catch
        {
            $msg = "Exception :" + $Error 
			Logging $msg "Red"
        }
	}
	else
	{
		$msg = "Exception :" + $Error 
		Logging $msg "Red"
	}
}


##Function DeleteRulesFromExcelFile
##$web - Target web object
##$isDelete - true in case delete Existing Rule otherwise false
##$orgruleCSV - content organizer rules csv file content
##This function will delete content organizer rules 
function DeleteRulesFromExcelFile($web,$isDelete,$orgruleCSV)
{    
    #loop through each rule and create them
   
    #initialize row counter
    $k = 0

    foreach($orgrule in $orgruleCSV){
        #flag to skip creation if an error appears in the conditions or when deleting an existing rule
        $errorFlag = $false
                
        $RuleName = $orgrule.Name        

        Logging "Reading rule $RuleName from csv Config file..." "Cyan"

        $isRuleExists=$false
		#if delete set to $true, delete the existing rule if it exists
		if($isDelete) {
			#check if a rule with the name $name already exists
			foreach($item in $web.Lists["Content Organizer Rules"].Items) {
				if($item.Name -eq $RuleName) {
					Logging "Deleting rule, $RuleName..."
				    $isRuleExists=$true
					#try to delete the rule
					try {
						$item.Delete()
						Logging "$RuleName Deleted sucessfully" "Green"
						break
					} catch [System.Exception] {
						$errorFlag = $true
						Logging $_.Exception.Message "Red"
					}                    
				}               
			}  
            if(!$errorFlag -and !$isRuleExists)
            {
                Logging "Rule name '$($RuleName)' not exists on web - $($web.Url)" "Yellow"
            }          
		}        
		Logging ""
    }
}
