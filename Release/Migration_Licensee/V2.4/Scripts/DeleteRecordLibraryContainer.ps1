﻿
# ===================================================================================
# Function to Delete Record Library Container 
# ===================================================================================

function DeleteRecordLibraryContainer([string]$ConfigPath = "")
{
    $error.Clear()
    $cfg = [xml](get-content $ConfigPath)

    # Exit if config file is invalid
    if( $? -eq $false ) 
    {
        Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..."
    }
    Write-Output "Sucessfully read config file $ConfigPath file"
    Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    if($Error.Count -eq 0)
	{
        if( $? -eq $false ) 
	    {
            Write-Output "Error: Could not read config file. Exiting ..."
            Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
	    }
   
        if(![string]::IsNullOrEmpty($cfg.Sites.Site))
        {
			try
			{
				foreach($site in $cfg.Sites.Site)
				{
                    $ErrorActionPreference = "SilentlyContinue"						            
					$web=Get-SPWeb $site.Url
                    $folders=$site.Folders
                    foreach($folder in $folders.Folder)
				    {	
                        $libName = $folder.Library
                        $folderUrlToDelete = $folder.FolderUrlToDelete
                        $folderExists=$false
                        if($web -ne $null)
                        {
                            try
			                {
							    $folderInLib = $web.Lists[$libName].Folders.Url
							    foreach($eachFolderInLib in $folderInLib)
							    {								
								    if($folderUrlToDelete -eq $eachFolderInLib)
								    {
									    $folderToDelete = $web.GetFolder($site.Url + $folderUrlToDelete)
									    if($folderToDelete.ItemCount -gt 0)
									    {
										    foreach($folderItem in $folderToDelete.Files)
										    {
											    UnDeclareRecord $web $folderItem
											    $folderItem.Item.Delete()
										    }
									    }
									    $folderToDelete.Delete()    
                                        $folderExists =$true                              
								    }
							    }
                                if($folderExists)
                                {
                                    Write-Host "Folder '$($folderUrlToDelete)' deleted successfully in library $($libName) for site $($site.Url)" -ForegroundColor Green
                                    Write-Output "Folder '$($folderUrlToDelete)' deleted successfully in library $($libName) for site $($site.Url)" | out-null
                                }
                                else
                                {
                                   Write-Host "Folder '$($folderUrlToDelete)' not found in library $($libName) for site $($site.Url)" -ForegroundColor Yellow
                                   Write-Output "Folder '$($folderUrlToDelete)' not found in library $($libName) for site $($site.Url)" | out-null 
                                }
                            }
			                catch
			                {
				                Write-Output "Exception for $($_.Exception.Message)"|out-null    
				                Write-Host "Exception for $($_.Exception.Message)" -ForegroundColor Yellow
			                }
                        }
				    }
                }
			}
			catch
			{
				Write-Output "Exception for $($Siteurl.Url)"|out-null    
				Write-Host "Exception for $($Siteurl.Url)" -ForegroundColor Yellow
			}
        }
		else
		{
			Write-Output "SiteUrl is empty for $($Siteurl.Url)"|out-null    
            Write-Host "SiteUrl is empty for $($Siteurl.Url)" -ForegroundColor Yellow
		}
    }
}

# ===================================================================================
# Function to undeclare records of records library before deletion
# ===================================================================================

function UnDeclareRecord($web,$eachItem)
{
    try
    {
        $IsRecord = [Microsoft.Office.RecordsManagement.RecordsRepository.Records]::IsRecord($eachItem.Item)
	    if ($IsRecord -eq $true)
        {   
		    [Microsoft.Office.RecordsManagement.RecordsRepository.Records]::UndeclareItemAsRecord($eachItem.Item)
	    }
    }
    catch
    {
		Write-Host "(ERROR in UnDeclareRecord  : "$_.Exception.Message")" -ForegroundColor Red
		Write-Output "(ERROR in UnDeclareRecord : "$_.Exception.Message")"|out-null   
    }    
}