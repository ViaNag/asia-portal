﻿# ===================================================================================
# FUNC: UpdatePageLayout
# DESC: Update Page layouts
# ===================================================================================
 function DeletePublishingPage([String]$ConfigFileName = "")
 {
     if (-not $(Test-Path -Path $configFileName -Type Leaf))
	{
		Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
	}

	$configXml = [xml]$(get-content $configFileName)
	
	if( $? -eq $false ) 
	{
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..." | Out-Null
	}

	foreach($Page in $configXml.Configuration.Pages.Page)     
	{
		DeletePage $configXml.Configuration.PublishingSite.URL $Page.Name
	}
}
function DeletePage([string]$WebURL, [string]$PublishingPageName)
{
     $Web = Get-SPWeb $WebURL
     #Get Publishing Site and Web
     $PublishingSite = New-Object Microsoft.SharePoint.Publishing.PublishingSite($Web.Site)
     $PublishingWeb = [Microsoft.SharePoint.Publishing.PublishingWeb]::GetPublishingWeb($web)

     $pageUrl=$WebURL + "/Pages/" + $PublishingPageName
     $publishingPages = $PublishingWeb.GetPublishingPages()
     $PublishingPage = $PublishingWeb.GetPublishingPage($pageUrl)       
     $file = $web.GetFile($PublishingPage.Uri.ToString())

     [Microsoft.SharePoint.SPListItem]$spListItem = $file.Item

     $spListItem.Delete()
 }