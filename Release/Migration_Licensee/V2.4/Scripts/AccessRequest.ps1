﻿# ===================================================================================
# Change access request email ID function
# ===================================================================================

function ChangeAccessReqEmail([string]$ConfigPath = "")
{
    $error.Clear()
    $cfg = [xml](get-content $ConfigPath)

    # Exit if config file is invalid
    if( $? -eq $false ) 
    {
        Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..."
    }
    Write-Output "Successfully read config file $ConfigPath file"
    Write-Host "Successfully read config file $ConfigPath file" -ForegroundColor Green
    if($Error.Count -eq 0)
	{
        if( $? -eq $false ) 
	    {
            Write-Output "Error: Could not read config file. Exiting ..."
            Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
	    }
        try
		{
			foreach($site in $cfg.Webapplication.Site)
			{
				$error.Clear()
				$accessRequestEmail = $site.AccessRequestEmailId
				if($site.isSiteCollection -eq "True")
				{					
					$siteCol=Get-SPSite $site.SiteURL               
					$webCol=$siteCol.AllWebs
					if($webCol -ne $null)
					{
						foreach($web in $webCol)
						{
						   try
						   {
							   #if ($web.HasUniqueRoleAssignments)
							   {
									$web.RequestAccessEmail = $accessRequestEmail
									$web.Update()
									Write-Output "Access Request Email set at " $web.Url |out-null    
									Write-Host "Access Request Email set at " $web.Url -ForegroundColor Cyan
							   } 
							}
							catch
							{
								Write-Output "Exception for $($web.Url) `n $($Error)"|out-null    
								Write-Host "Exception for $($web.Url) `n $($Error)" -ForegroundColor Red
							}      
						}
					}
				}
				elseif($site.isSiteCollection -eq "False")
				{
					$web=Get-SPWeb $site.SiteURL
					try
					{						
						#if ($web.HasUniqueRoleAssignments)
						{
							$web.RequestAccessEmail = $accessRequestEmail
							$web.Update()
							Write-Output "Access Request Email set at " $web.Url |out-null
							Write-Host "Access Request Email set at " $web.Url -ForegroundColor Cyan
						} 
					}
					catch
					{
						Write-Output "Exception for $($web.Url) `n $($Error)"|out-null    
						Write-Host "Exception for $($web.Url) `n $($Error)" -ForegroundColor Red
					} 
				}
			}
		}
		catch
		{
			Write-Output "Exception for $($site.SiteURL) `n $($Error)" |out-null    
			Write-Host "Exception for $($site.SiteURL) `n $($Error)" -ForegroundColor Red
		}		
    }
}
