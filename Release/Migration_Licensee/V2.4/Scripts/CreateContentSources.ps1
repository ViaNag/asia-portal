﻿# =================================================================================
# FUNC: CreateContentSources
# DESC: Main function to create content source
# Please handle below code in deploy.ps1 file
#	$ver = $host | select version 
#	if($Ver.version.major -gt 1) {$Host.Runspace.ThreadOptions = "ReuseThread"} 
# Load all required assemby in deploy.ps1 file
# =================================================================================
function CreateContentSources([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $($ConfigPath) file"
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
			$searchServiceName=$cfg.ContentSources.SearchServiceName
			
			#create content sources
            foreach($contentSource in $cfg.ContentSources.ContentSource)
            {
                CreateContentSource $contentSource $searchServiceName
            }			
        }
        catch
        {
            Write-Host "`nException in CreateContentSources :" $Error -ForegroundColor Red
            Write-Output "`nException in CreateContentSources:" $Error
        }
	}
	else
	{
		Write-Host "Error in CreateContentSources:" $Error -ForegroundColor Red
        Write-Output "Error in CreateContentSources:" $Error
	}
}

# =================================================================================
# FUNC: CreateContentSource
# DESC: Create Content Source
# =================================================================================
function CreateContentSource([object] $contentSource, $searchServiceName)
{
	try
	{
		#get Search Service application proxy		
		$searchapp =Get-SPEnterpriseSearchServiceApplicationProxy $searchServiceName 
		
		#get all content sources
		$existingContentSources = Get-SPEnterpriseSearchCrawlContentSource -SearchApplication $searchapp
		
		#check if the content source exists
		$existingContentSources | ForEach-Object {
			if ($_.Name.ToString() -eq $contentsource.Name)
			{
				$SPContentSource = Get-SPEnterpriseSearchCrawlContentSource -SearchApplication $SearchServiceApplication -Identity $_.Id
				Write-Host "Content Source: $($contentsource.Name) already exist, so it will be deleted and re-created" -ForegroundColor Green
				Write-Output "Content Source: $($contentsource.Name) already exist, so it will be deleted and re-created" 
				$SPContentSource = Remove-SPEnterpriseSearchCrawlContentSource
				return;
			}
		}
		
		#create content source 
		New-SPEnterpriseSearchCrawlContentSource -SearchApplication $searchapp -Type $contentSource.CType -name $contentSource.ContentSourceName -StartAddresses $contentsource.SharedPath
		
		#foreach Crawl Schedule
		foreach ($crawlSchedule in $contentsource.CrawlSchedules.CrawlSchedule)
		{
			if ($crawlSchedule.Repeat -eq "Monthly")
			{
				#set the monthly schedule
				$SPContentSource | Set-SPEnterpriseSearchCrawlContentSource -StartAddresses $StartAddresses -ScheduleType $crawlSchedule.Type -MonthlyCrawlSchedule -CrawlScheduleMonthsOfYear $crawlSchedule.MonthsOfYear -CrawlScheduleStartDateTime $crawlSchedule.StartDateTime
			}
			elseif ($crawlSchedule.Repeat -eq "Weekly")
			{
				#set the weekly schedule
				$SPContentSource | Set-SPEnterpriseSearchCrawlContentSource -StartAddresses $StartAddresses -ScheduleType $crawlSchedule.Type -WeeklyCrawlSchedule -CrawlScheduleStartDateTime $crawlSchedule.StartDateTime -CrawlScheduleDaysOfWeek $crawlSchedule.DaysOfWeek  -CrawlScheduleRunEveryInterval $crawlSchedule.RunEveryInterval
			}
			elseif ($crawlSchedule.Repeat -eq "Daily")
			{
				$SPContentSource | Set-SPEnterpriseSearchCrawlContentSource -StartAddresses $StartAddresses -ScheduleType $crawlSchedule.Type -DailyCrawlSchedule -CrawlScheduleRunEveryInterval $crawlSchedule.RunEveryInterval -CrawlScheduleRepeatInterval $crawlSchedule.RepeatInterval -CrawlScheduleRepeatDuration $crawlSchedule.RepeatDuration
			}
		}		
	}
	catch
	{
		Write-Host "(ERROR in CreateContentSource: $($_.Exception.Message))" -ForegroundColor Red
		Write-Output "(ERROR in CreateContentSource: $($_.Exception.Message))" 
	} 
}