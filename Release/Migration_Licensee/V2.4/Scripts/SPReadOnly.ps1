﻿
# =================================================================================
#
#Main Function to Set site collection as readonly
#
# =================================================================================
function UpdateSPSiteLockState([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $($ConfigPath) file`n"
    
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
            foreach($site in $cfg.Sites.Site)
            {
				UpdateSiteCollectionLockState $site.URL $site.LOCKSTATE
            }
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host "Error: " $Error -ForegroundColor Red
        Write-Output "Error: " $Error
	}
}

# =================================================================================
#
# FUNC: UpdateSiteCollectionLockState
# DESC: Update site collection lock state
# =================================================================================
function UpdateSiteCollectionLockState($SiteCollectionURL, $lockState)
{
    $error.Clear()
        
    try
	{
		Set-SPSite -Identity $SiteCollectionURL -LockState $lockState
		Write-Host "`Site collection with URL $($SiteCollectionURL) updated with lockstate : $($lockState)" -ForegroundColor Green
		Write-Output "`Site collection with URL $($SiteCollectionURL) updated with lockstate : $($lockState)" -ForegroundColor Green
	}
	catch
    {
        Write-Host "`nException for updating site collection lock state with URL: " $($SiteCollectionURL) "`n" $Error -ForegroundColor Red
        Write-Output "`nException for updating site collection lock state with URL: " $($SiteCollectionURL) "`n" $Error -ForegroundColor Red
    }
}