﻿##################################################################################################
# ================================================================================================
# Search Page setting
# ================================================================================================
##################################################################################################


if ($snapin -eq $null) 
{    
	Write-Output "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

# ===================================================================================
# Reading configuration file and performing operations accrodingly
# ===================================================================================
 function SearchPageSetting([string]$ConfigPath = "")
{
$NavXml=[xml](get-content $ConfigPath)
if($? -eq $false) 
	{
		Write-Host "Error: Could not read SearchPageURL.xml file. Exiting ..." -ForegroundColor Red`
        Write-Output "Error: Could not read SearchPageURL.xml file. Exiting ..."
	}
else{
	Write-Host "Read SearchPageURL.xml file successfully....." -ForegroundColor Green`
	Write-Output "Read SearchPageURL.xml file successfully...."
	Write-Host "Starting search setting process....." -ForegroundColor Green`
	Write-Output "Starting search setting process....."
		try
			{
				foreach($siteurl in $NavXml.Sites.Site)
				{
					
					
					if(($siteurl.Level -eq 'Both'))
					 {
						 $sitecol = Get-SPSite $siteurl.URL
						 $sitecol.AllWebs | ForEach-Object {
							 $oweb = $_
							 $oweb.AllProperties["SRCH_SB_SET_WEB"] = '{"Inherit":false,"ResultsPageAddress":"'+$siteurl.SearchPageURL+'","ShowNavigation":false}'
							 $oweb.AllProperties["SRCH_ENH_FTR_URL_WEB"] = $siteurl.SearchCenterURL
							 $oweb.Update()
							 Write-Host "Search setting for site $oweb.URL is done successfully....." -ForegroundColor Green`
							 Write-Output "Search setting for site $oweb.URL is done successfully....."
						 }
					 }
				
					 if(($siteurl.Level -eq 'RootSite') -Or ($siteurl.Level -eq 'Subsite'))
					 {
						 $web = Get-SPWeb $siteurl.URL
						 $web.AllProperties["SRCH_SB_SET_WEB"] = '{"Inherit":false,"ResultsPageAddress":"'+$siteurl.SearchPageURL+'","ShowNavigation":false}'
						 $web.AllProperties["SRCH_ENH_FTR_URL_WEB"] = $siteurl.SearchCenterURL
						 $web.Update()
						 Write-Host "Search setting for site $web.URL is done successfully....." -ForegroundColor Green`
						 Write-Output "Search setting for site $web.URL is done successfully....."
					 }
				}
			}
			catch
			{
				Write-Output "Exception while fetching url from config file: $($url) " $Error -ForegroundColor Red
				Write-Host "Exception while fetching url from config file: $($url) " $Error -ForegroundColor Red
			}
		}
}

