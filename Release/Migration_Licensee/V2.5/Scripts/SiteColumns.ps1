﻿# =================================================================================
#
#Main Function
#
# =================================================================================
function CreateSiteColumns([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Exception: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Exception: Could not read config file. Exiting ..."
	}
	
	Write-Host "Successfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Successfully read config file $ConfigPath file`n"
   
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
            foreach($column in $cfg.SiteColumns.Column)
            {
                SiteColumnToAdd $column
            }
        }
        catch
        {
            Write-Output "`nException :" $Error
            Write-Host "`nException :" $Error -ForegroundColor Red
        }
	}
	else
	{
		Write-Host "Exception: " $Error -ForegroundColor Red
        Write-Output "Exception: " $Error
	}
}

# =================================================================================
#
# FUNC: SiteCOlumnToAdd
# DESC: Create site column in existing site collection
#
# =================================================================================
function SiteColumnToAdd([object] $column)
{
    try
    {
        $error.Clear()
        $site = new-object Microsoft.SharePoint.SPSite $column.SiteUrl       
        $ActionType=$column.ActionType
        if(![string]::IsNullOrEmpty($column.PushChanges))
        {
            $PushChanges=[System.Convert]::ToBoolean($column.PushChanges)
        }
        else
        {
            $PushChanges=$false
        }
        if($ActionType -eq "Add" -or $ActionType -eq "Update")
        {
			$web = $site.RootWeb
			
            # Get the type of the field
            $type = $column.Type
			$columnAlreadyExist = $false;
            if(($type -eq "TaxonomyFieldType") -or ($type -eq "TaxonomyFieldTypeMulti"))
            {
                # Get the Taxonomy session of your site collection
                $session = new-object Microsoft.SharePoint.Taxonomy.TaxonomySession($site)
                $termStore = $session.TermStores[$column.TermStore]
                # Get the term store group which stores the term sets you want to retrieve.
                $taxGroup=$column.TaxonomyGroup
                #if($column.TaxonomyGroup -eq "M&E Ad Sales")
                #{
                #    $taxGroup="M＆E Ad Sales"
                #}
				if($taxGroup.contains("&"))
				{
					$taxGroup = $taxGroup -replace "&","＆"
				}
                $termStoreGroup = $termStore.Groups[$taxGroup]

				  # Get the term set you want to associate with this field. 
				  $termSet = $termStoreGroup.TermSets[$column.TermSet]
				  # In most cases, the anchor on the managed metadata field is set to a lower level under the root term of the term set. In such cases, specify the term in the spreadsheet and do the following
				  $termID=""
				  if(($column.TermSet -ne $column.Term) -and ($column.Term -ne "") -and ($column.Term -ne $null))
				  {
						#Get all terms under term set
						$terms = $termSet.GetAllTerms()

						$termToMap =$column.Term
						if($termToMap.Contains(";"))
						{
							$multiLevelTaxonomyTerm=$termToMap.Split(";")
							$m=0;
							foreach($termLabel in $multiLevelTaxonomyTerm)
							{
							   $term = $terms | Where-Object {$_.Name -eq $termLabel}   
							   $terms = $term.Terms    
							}
						}
						else
						{
							 #Get the term to map the column to
							 $term = $terms | Where-Object {$_.Name -eq $column.Term}
						}


						#Get the GUID of the term to map the metadata column anchor to
						$termID = $term.Id
					}
				 else # In cases when you want to set the anchor at the root of the term set, leave the  value as blank. Empty guids will error out when you run the script but will accomplish what you need to do i.e. set the anchor at the root of the termset
				   {                                
						$termID = [System.GUID]::empty
				   } 
                     # Create the new managed metadata field
                     if($ActionType -eq "Add")
                     {
						#Warn and continue if the site column already exists
                        $newSiteColumn = $null
						$columnAlreadyExist = $false;
						if(-not($web.Fields.ContainsField($column.InternalName)))
						{
							 if([string]::IsNullOrEmpty($column.Guid))
							 {
								$newSiteColumn = $web.Fields.CreateNewField("TaxonomyFieldType", $column.InternalName)
								$web.Fields.Add($newSiteColumn) | Out-Null
								$web.Update()
							 }
							 else
							 {
								$fieldXML=$null
								$fieldXML = '<Field Type="TaxonomyFieldType" SourceID="http://schemas.microsoft.com/sharepoint/v3" Name="'+$column.InternalName+
											 '" ID="{' + $column.Guid+'}'+
											 '" DisplayName="'+$column.ColumnName+
											 '"></Field>' 
								$web.Fields.AddFieldAsXml($fieldXML) | Out-Null
								$web.update()
							 }
						}
						else
						{
							$newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)
							$columnAlreadyExist = $true
							
							if (($newSiteColumn.InternalName -eq $column.InternalName) -and ($newSiteColumn.TypeAsString -eq "TaxonomyFieldType" -or $newSiteColumn.TypeAsString -eq "TaxonomyFieldTypeMulti"))
							{
								Write-Host "`n Exception: Column " $column.ColumnName " already exists`n" -ForegroundColor Red 
								Write-Host "`n Exception: Column " $column.ColumnName " already exists`n"
							}
							else
							{
								Write-Host "`n Warning: Column " $column.ColumnName " already exists`n" -ForegroundColor Yellow 
								Write-Host "`n Warning: Column " $column.ColumnName " already exists`n"
							}
						}
                    }
					 $newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)

                     # Update the properties of the new field.
                     if(($ActionType -eq "Add" -and $columnAlreadyExist -eq $false) -or $ActionType -eq "Update")
                     {
                         $newSiteColumn.SspId = $termSet.TermStore.ID
                         $newSiteColumn.TermSetId = $termSet.Id 
                         $newSiteColumn.AnchorId = $termID
                         if($type -eq "TaxonomyFieldTypeMulti")
                         {
                            $newSiteColumn.AllowMultipleValues = $true
                         }
                         # Add the the new column to the Site collection's Root web's Fields Collection
                         $newSiteColumn.Update($PushChanges)
                         $web.Update()
                     } 
			}
             elseif($type -eq "Choice" -or $type -eq "MultiChoice" -or $type -eq "OutcomeChoice")
             {
                    # Build a string array with the choice values separating the values at ","
                    $choiceFieldChoices = @($column.Choices.choice)

                    # Declare a new empty String collection
                    $stringColl = new-Object System.Collections.Specialized.StringCollection

                    # Add the choice fields from array to the string collection
                    $stringColl.AddRange($choiceFieldChoices)

                    # Create a new choice field and add it to the web using overload method
                    if($ActionType -eq "Add")
                    {
                        $newSiteColumn = $null
						$columnAlreadyExist = $false;
						if(-not($web.Fields.ContainsField($column.InternalName)))
						{
							if([string]::IsNullOrEmpty($column.Guid))
							{
								$newSiteColumn = $web.Fields.Add($column.InternalName,[Microsoft.SharePoint.SPFieldType]::$type, $siteColumn.Required, $false, $stringColl)
								$web.Update()
							}
							else
							{
							   $fieldXML=$null
							   $fieldXML = '<Field Type="' + [Microsoft.SharePoint.SPFieldType]::$type + '" SourceID="http://schemas.microsoft.com/sharepoint/v3" FillInChoice="FALSE" Name="'+$column.InternalName+
											 '" DisplayName="'+$column.ColumnName+
											 '" ID="{' + $column.Guid+'}"'+
											 '>'
								if($column.Choices.choice.Count -gt 0)
								{
								   $fieldXML=$fieldXML+"<CHOICES>"
								   foreach($choice in $choiceFieldChoices)
								   {
										$fieldXML=$fieldXML+'<CHOICE>'+ [System.Web.HttpUtility]::HtmlDecode($choice.Trim()) +'</CHOICE>'
								   }
								   $fieldXML=$fieldXML+"</CHOICES>"
								}
							   $fieldXML=$fieldXML+ '</Field>'
                               $fieldXML=$fieldXML.Replace("&","&amp;")
							   $web.Fields.AddFieldAsXml($fieldXML)
							   $web.update()
							}
						}
						else
						{
							$newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)
							$columnAlreadyExist = $true
							if (($newSiteColumn.InternalName -eq $column.InternalName) -and ($newSiteColumn.TypeAsString -eq "Choice" -or $newSiteColumn.TypeAsString -eq "MultiChoice" -or $newSiteColumn.TypeAsString -eq "OutcomeChoice"))
							{
								Write-Host "`n Exception: Column " $column.ColumnName " already exists`n" -ForegroundColor Red 
								Write-Host "`n Exception: Column " $column.ColumnName " already exists`n"
							}
							else
							{
								Write-Host "`n Warning: Column " $column.ColumnName " already exists`n" -ForegroundColor Yellow 
								Write-Host "`n Warning: Column " $column.ColumnName " already exists`n"
							}
						}
                    }
                    $newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)
                    if($ActionType -eq "Update")
                    {
                        $newSiteColumn.choices.clear()
                        $newSiteColumn.update($PushChanges)
                        $web.update()
                        $newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)
                        $newSiteColumn.choices.addrange($choiceFieldChoices)
                        $newSiteColumn.update($PushChanges)
                        $web.update()
                    }
             }
			 elseif($type -eq "Number")
             {
                if($ActionType -eq "Add")
                {
					$newSiteColumn = $null
					$columnAlreadyExist = $false;

					if(-not($web.Fields.ContainsField($column.InternalName)))
					{
						if([string]::IsNullOrEmpty($column.Guid))
						{
							$newSiteColumn = $web.Fields.CreateNewField([Microsoft.SharePoint.SPFieldType]::$type, $column.InternalName)                        
							$web.Fields.Add($newSiteColumn)
							$web.update()
						}
						else
						{
						 $fieldXML=$null
							$fieldXML = '<Field Type="' + [Microsoft.SharePoint.SPFieldType]::$type + '"'+' SourceID="http://schemas.microsoft.com/sharepoint/v3" Name="'+$column.InternalName+
										 '" ID="{' + $column.Guid+'}'+
										 '" DisplayName="'+$column.ColumnName + '"></Field>'
										 
							$web.Fields.AddFieldAsXml($fieldXML)
							$web.update()
						}
					}
					else
					{
						$newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)
						$columnAlreadyExist = $true
						if (($newSiteColumn.InternalName -eq $column.InternalName) -and ($newSiteColumn.TypeAsString -eq $column.Type))
						{
							Write-Host "`n Exception: Column " $column.ColumnName " already exists`n" -ForegroundColor Red 
							Write-Host "`n Exception: Column " $column.ColumnName " already exists`n"
						}
						else
						{
							Write-Host "`n Warning: Column " $column.ColumnName " already exists`n" -ForegroundColor Yellow 
							Write-Host "`n Warning: Column " $column.ColumnName " already exists`n"
						}
					}
                }
                $newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)			 
             }
             elseif($type -eq "Image")
             {
                if($ActionType -eq "Add")
                {
				   $newSiteColumn = $null
				   $columnAlreadyExist = $false;

				   if(-not($web.Fields.ContainsField($column.InternalName)))
				   {
						if([string]::IsNullOrEmpty($column.Guid))
						{
							$newSiteColumn = $web.Fields.CreateNewField("Image", $column.InternalName)
							$web.Fields.Add($newSiteColumn)
							$web.update()
						}
						else
						{
							$fieldXML=$null
							$fieldXML = '<Field Type="' + "Image" + '"'+' SourceID="http://schemas.microsoft.com/sharepoint/v3" Name="'+$column.InternalName+
										 '" ID="{' + $column.Guid+'}'+
										 '" DisplayName="'+$column.ColumnName + '"></Field>'
							$web.Fields.AddFieldAsXml($fieldXML)
							$web.update()
						}
					}
					else
					{
						$newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)
						$columnAlreadyExist = $true
						if (($newSiteColumn.InternalName -eq $column.InternalName) -and ($newSiteColumn.TypeAsString -eq $column.Type))
						{
							Write-Host "`n Exception: Column " $column.ColumnName " already exists`n" -ForegroundColor Red 
							Write-Host "`n Exception: Column " $column.ColumnName " already exists`n"
						}
						else
						{
							Write-Host "`n Warning: Column " $column.ColumnName " already exists`n" -ForegroundColor Yellow 
							Write-Host "`n Warning: Column " $column.ColumnName " already exists`n"
						}
					}
                }
                $newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)
             }
             elseif($type -eq "Calculated")
             {
                $formula ="=" + $column.Formula
                if($ActionType -eq "Add")
                {		  
					$newSiteColumn = $null
					$columnAlreadyExist = $false;

					if(-not($web.Fields.ContainsField($column.InternalName)))
					{
						$choiceFieldChoices = @($column.CalculatedFields.Field)					
						if([string]::IsNullOrEmpty($column.Guid))
						{                      
						  # Add the the new column to the Site collection's Root web's Fields Collection  
						  $web.Fields.Add($column.InternalName,$type,$false)
						  $newSiteColumn=$web.Fields.GetField($column.InternalName)             
						  $newSiteColumn.Formula = $formula
						  $newSiteColumn.Update($PushChanges)
						  $web.Update()
						}
						else
						{
							$fieldXML=$null
							$fieldXML = '<Field Type="' + [Microsoft.SharePoint.SPFieldType]::$type + '"'+' SourceID="http://schemas.microsoft.com/sharepoint/v3" Name="'+$column.InternalName+
										 '" ID="{' + $column.Guid+'}'+
										 '" DisplayName="'+$column.ColumnName + '">'
							
							$fieldXML=$fieldXML+"<Formula>=Formula</Formula>" 

							if($column.CalculatedFields.Field.Count -gt 0)
							{
							   $fieldXML=$fieldXML+"<FieldRefs>"
							   foreach($choice in $choiceFieldChoices)
							   {
									$fieldXML=$fieldXML+ "<FieldRef Name='" + $choice + "'/>"
							   }
							   $fieldXML=$fieldXML+"</FieldRefs>"
							}
							$fieldXML=$fieldXML+ '</Field>'   						 

							$web.Fields.AddFieldAsXml($fieldXML)
							$web.update()
						}
					}
					else
					{
						$newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)
						$columnAlreadyExist = $true
						if (($newSiteColumn.InternalName -eq $column.InternalName) -and ($newSiteColumn.TypeAsString -eq $column.Type))
						{
							Write-Host "`n Exception: Column " $column.ColumnName " already exists`n" -ForegroundColor Red 
							Write-Host "`n Exception: Column " $column.ColumnName " already exists`n"
						}
						else
						{
							Write-Host "`n Warning: Column " $column.ColumnName " already exists`n" -ForegroundColor Yellow 
							Write-Host "`n Warning: Column " $column.ColumnName " already exists`n"
						}
					}
                }

                $newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)
                
                $newSiteColumn.Formula = $formula
                $newSiteColumn.Update()
                $web.Update()
                	
             }
			 elseif($type -eq "Lookup" -or $type -eq "LookupMulti")
             {
                if($ActionType -eq "Add")
                {
                    $LookupList = $web.Lists[$column.LookupList]
                    $lookupListGuid = $LookupList.ID.Guid
                    $columnAlreadyExist = $false;
					if(![string]::IsNullOrEmpty($column.LookupField))
                    {
                        $lookupColumn = $Lookuplist.Fields.GetFieldByInternalName($column.LookupField)
						#$lookupfld = $null
						if(-not($web.Fields.ContainsField($column.InternalName)))
						{
							if([string]::IsNullOrEmpty($column.Guid))
							{                      
								$addLookupResult = $web.Fields.AddLookup($column.InternalName, $lookupListGuid, $false);
								$addLookupField = $web.Fields.GetFieldByInternalName($addLookupResult);
								$addLookupField.Title = $column.ColumnName
								$addLookupField.LookupField = $LookupList.Fields.GetFieldByInternalName($lookupColumn).InternalName;
								$addLookupField.Update();
							}
							else
							{
								$lookupXMLString = '<Field Type="' + [Microsoft.SharePoint.SPFieldType]::$type + '"'+' SourceID="http://schemas.microsoft.com/sharepoint/v3" DisplayName="' + $column.ColumnName + 
								'" ID="{' + $column.Guid+'}" ShowField="' + $lookupColumn.InternalName + '" List="{' + $lookupListGuid+'}" Name="' + $column.InternalName + '"></Field>'
								$web.Fields.AddFieldAsXml($lookupXMLString)
								$web.update()
							}
						}
						else
						{	
							$newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)
							$columnAlreadyExist = $true
							if (($newSiteColumn.InternalName -eq $column.InternalName) -and ($newSiteColumn.TypeAsString -eq "Lookup" -or $newSiteColumn.TypeAsString -eq "LookupMulti"))
							{
								$columnAlreadyExist = $true
								Write-Host "`n Exception: Column " $column.ColumnName " already exists`n" -ForegroundColor Red 
								Write-Host "`n Exception: Column " $column.ColumnName " already exists`n"
							}
							else
							{
								Write-Host "`n Warning: Column " $column.ColumnName " already exists`n" -ForegroundColor Yellow 
								Write-Host "`n Warning: Column " $column.ColumnName " already exists`n"
							}
						}
                    }
                }               
             }
             else
             {
                # Create the new field and add it to the web
                if($ActionType -eq "Add")
                {
                    $newSiteColumn=$null
                    if($type -eq "UserMulti")
                    {
                        $type="User"
                    }
                    $newSiteColumn = $null
					$columnAlreadyExist = $false;

					if(-not($web.Fields.ContainsField($column.InternalName)))
					{
						if([string]::IsNullOrEmpty($column.Guid))
						{
							$newSiteColumn = $web.Fields.CreateNewField([Microsoft.SharePoint.SPFieldType]::$type, $column.InternalName)
							$web.Fields.Add($newSiteColumn) | Out-Null
							$web.update()
						}
						else
						{
							$fieldXML=$null
							$fieldXML = '<Field Type="' + [Microsoft.SharePoint.SPFieldType]::$type + '"'+' SourceID="http://schemas.microsoft.com/sharepoint/v3" Name="'+$column.InternalName+
										 '" ID="{' + $column.Guid+'}'+
										 '" DisplayName="'+$column.ColumnName + '"></Field>'
							$web.Fields.AddFieldAsXml($fieldXML) | Out-Null
							$web.update()
						}
					}
					else
					{
						$newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)
						$columnAlreadyExist = $true
						if (($newSiteColumn.InternalName -eq $column.InternalName) -and ($newSiteColumn.TypeAsString -eq $column.Type -or $newSiteColumn.TypeAsString -eq "UserMulti"))
						{
							Write-Host "`n Exception: Column " $column.ColumnName " already exists`n" -ForegroundColor Red 
							Write-Host "`n Exception: Column " $column.ColumnName " already exists`n"
						}
						else
						{
							Write-Host "`n Warning: Column " $column.ColumnName " already exists`n" -ForegroundColor Yellow 
							Write-Host "`n Warning: Column " $column.ColumnName " already exists`n"
						}
					}
                }
                $newSiteColumn=$web.Fields.GetFieldByInternalName($column.InternalName)
             }
			 
				if($columnAlreadyExist -eq $false)
				{
					Write-Host "Site column $($column.ColumnName) with action type $($ActionType) operation completed successfully at web:- $($column.SiteUrl)" -ForegroundColor Green
					Write-Output "Site column $($column.ColumnName) with action type $($ActionType) operation completed successfully at web:- $($column.SiteUrl)"
				}
              if(($ActionType -eq "Add" -and $columnAlreadyExist -eq $false) -or $ActionType -eq "Update")
              {
					UpdateColumnProperites $column $web $PushChanges
              }
        }
		elseif($ActionType -eq "Delete")
		{
			DeleteColumn $column $site $PushChanges
		}		
    }
    catch
    {
        Write-Host "`nException for site column :" $column.ColumnName " with action type $($column.ActionType)`n" $Error -ForegroundColor Red 
        Write-Output "`nException for site column :" $column.ColumnName "with action type $($column.ActionType)`n" $Error
    }
	
	if($web -ne $null)
	{
		$web.Dispose()
    }
	
	if($site -ne $null)
	{
		$site.Dispose()
    }
}

# =================================================================================
#
# FUNC: UpdateColumnProperites
# DESC: Update site column properties in existing site collection
#
# =================================================================================
function UpdateColumnProperites([object] $column, $web, $PushChanges)
{
    try
    {
        $error.Clear()
        SetDefaultValue $column $PushChanges
        $web=Get-SPWeb $column.SiteUrl
        $clm = $null
        $clm = $web.Fields.GetFieldByInternalName($column.InternalName)

        # Add or remove any properties here

        if(![string]::IsNullOrEmpty($column.ColumnName))
        {
             $clm.Title = $column.ColumnName
        }

        if(![string]::IsNullOrEmpty($column.Description))
        {
            $clm.Description = $column.Description
        }
        if(![string]::IsNullOrEmpty($column.GroupName))
        {
            $clm.Group = $column.GroupName
        }
       
		if(![string]::IsNullOrEmpty($column.ColumnValidation))
        {
            $columnValidation=$column.ColumnValidation
            if($columnValidation -eq "Empty")
            {
                $columnValidation = "";    
            } 
             $clm.ValidationFormula= $columnValidation      
        }

        if(![string]::IsNullOrEmpty($column.JSLink))
		{
			$clm.JSLink=$column.JSLink
		} 
		
		if(![string]::IsNullOrEmpty($column.CalculatedValueFormula))
		{
			if($column.CalculatedValueFormula.StartsWith("="))
			{
				$clm.DefaultFormula=$column.CalculatedValueFormula;
			}
			else
			{
				$clm.DefaultFormula="=" + $column.CalculatedValueFormula
			}
		} 
		
        # Boolean values must be converted before they are assigned in PowerShell.
        if(![string]::IsNullOrEmpty($column.ShowInNewForm))
        {
            [boolean]$clm.ShowInNewForm = [System.Convert]::ToBoolean($column.ShowInNewForm)
        }
        if(![string]::IsNullOrEmpty($column.ShowInDisplayForm))
        {
            [boolean]$clm.ShowInDisplayForm = [System.Convert]::ToBoolean($column.ShowInDisplayForm)
        }
        if(![string]::IsNullOrEmpty($column.ShowInEditForm))
        {
            [boolean]$clm.ShowInEditForm = [System.Convert]::ToBoolean($column.ShowInEditForm)
        }
        if(![string]::IsNullOrEmpty($column.Hidden))
        {			
			$type = $clm.GetType()
            $bindingFlags  = $type.GetMethod("SetFieldBoolValue",[System.Reflection.BindingFlags]$([System.Reflection.BindingFlags]::NonPublic -bor [System.Reflection.BindingFlags]::Instance))
			$bindingFlags.Invoke($clm, @("CanToggleHidden",$true))
			$clm.Update($true)
			$clm = $web.Fields.GetFieldByInternalName($column.InternalName)
			$clm.Hidden = $true		
            [boolean]$clm.Hidden = [System.Convert]::ToBoolean($column.Hidden)
        }
        if(![string]::IsNullOrEmpty($column.Required))
        {
            [boolean]$clm.Required = [System.Convert]::ToBoolean($column.Required)
        }
         if(![string]::IsNullOrEmpty($column.ReadOnlyField))
        {
            [boolean]$clm.ReadOnlyField = [System.Convert]::ToBoolean($column.ReadOnlyField)
        }
        if($type -eq "Note")
        {
             if($column.Format -eq "PlainText")
            {
                [boolean]$clm.RichText = $false
            }
            elseif($column.Format -eq "EnhanceRichText")
            {
				 [boolean]$clm.RichText = $true
				 $Field.RichTextMode = "FullHtml"
            }
            else
            {
                [boolean]$clm.RichText = $true
            }
        }
		elseif($column.Type -like 'User*')
		{
            $format = $column.Format
			if(![string]::IsNullOrEmpty($format))
			{
				$clm.SelectionMode=[Microsoft.SharePoint.SPFieldUserSelectionMode]::$format
			}
            
            if($column.Type -eq "UserMulti")
            {
                $clm.AllowMultipleValues = $true
            }

            if(![string]::IsNullOrEmpty($column.UserGroupName))
            {
                
                $spGroup=$web.SiteGroups[$column.UserGroupName]
                if($spGroup)
                {
                    $clm.SelectionGroup=$spGroup.ID
                }
                else
                {
                    Write-Host "Exception: Group $($column.UserGroupName) does not exists at web:- $($column.SiteUrl) for updating column $($column.InternalName)" -ForegroundColor Red
                    Write-Output "Exception: Group $($column.UserGroupName) does not exists at web:- $($column.SiteUrl) for updating column $($column.InternalName)" 
                }
            }
            else
            {
                $clm.SelectionGroup = 0; 
            }

		}
        elseif($column.Type -like "Number")
        {
            if(![string]::IsNullOrEmpty($column.Max))
            {
                $clm.MaximumValue = $column.Max
            }
            if(![string]::IsNullOrEmpty($column.Min ))
            {
                $clm.MinimumValue = $column.Min 
            } 
			if(![string]::IsNullOrEmpty($column.Format))
			{
				$clm.DisplayFormat=$column.Format;
			}
        }
		elseif($column.Type -like "Choice")
        {
			if(![string]::IsNullOrEmpty($column.Format))
            {
				if($column.Format -eq "RadioButtons")
				{
					$clm.EditFormat=[Microsoft.SharePoint.SPChoiceFormatType]::RadioButtons
				}
            }          
        }
        elseif($column.Type -like "DateTime")
        {
			if(![string]::IsNullOrEmpty($column.Format))
            {
				$clm.DisplayFormat=$column.Format
				
            }          
        } 
        elseif($column.Type -like "Calculated")
        {
            $clm.OutputType=$column.Format;
        }
		elseif($column.Type -like "Lookup*")
        {
            if(![string]::IsNullOrEmpty($column.LookupField))
			{
			    $LookupList = $web.Lists[$column.LookupList]
                $lookupListGuid = $LookupList.ID.Guid                    
                $lookupColumn = $Lookuplist.Fields.GetFieldByInternalName($column.LookupField)
                $clm.LookupField = $LookupList.Fields.GetFieldByInternalName($lookupColumn).InternalName;                     
			}
            if(![string]::IsNullOrEmpty($column.LookupUnlimitedLength))
			{
			    $clm.UnlimitedLengthInDocumentLibrary = $column.LookupUnlimitedLength                     
			}
            if($column.Type -eq "LookupMulti")
            {
                $clm.AllowMultipleValues = $true
            }
        }  

        # Update the site column
        $clm.Update($PushChanges)
        $web.Update()
    }
    catch
    {
         Write-Host "`nException for site column :" $column.ColumnName "`n" $Error -ForegroundColor Red 
         Write-Output "`nException for site column :" $column.ColumnName "`n" $Error 
    }
}

# =================================================================================
#
# FUNC: SetDeafultValue
# DESC: Set default value for column at site collection level
#
# =================================================================================
function SetDefaultValue([object] $column,$PushChanges)
{
    try
    {
        $error.Clear()
        $type=$column.Type
        if(($type -eq "TaxonomyFieldType") -or ($type -eq "TaxonomyFieldTypeMulti"))
            {
                # Get the Taxonomy session of your site collection
                $site = new-object Microsoft.SharePoint.SPSite $column.SiteUrl
                $session = new-object Microsoft.SharePoint.Taxonomy.TaxonomySession($site)
                $termStore = $session.TermStores[$column.TermStore]
                $taxGroup=$column.TaxonomyGroup
				if($taxGroup.contains("&"))
				{
					$taxGroup = $taxGroup -replace "&","＆"
				}
                #if($column.TaxonomyGroup -eq "M&E Ad Sales")
                #{
                #    $taxGroup="M＆E Ad Sales"
                #}
                $termStoreGroup = $termStore.Groups[$taxGroup]
				if($column.TermSet.contains("&"))
				{
					$column.TermSet = $column.TermSet -replace "&","＆"
				}
                $termSet = $termStoreGroup.TermSets[$column.TermSet]
				$web=$site.RootWeb
                $field=$web.Fields.GetFieldByInternalName($column.InternalName)
                $setDefVal=""
                if($type -eq "TaxonomyFieldTypeMulti")
                {
                    $defaultValues = @($column.DefaultValues.Split(","))
                    foreach($value in $defaultValues)
                    {
                        if(($value -ne $null) -and ($value -ne ""))
                        {
                            $term=$termSet.Terms[$value]
							$wssIDToSet = AddTaxonomyHiddenListItem $web $term 
                            $setDefVal=[string]$setDefVal+$wssIDToSet + ";#" + $term.GetPath() + [Microsoft.SharePoint.Taxonomy.TaxonomyField]::TaxonomyGuidLabelDelimiter + $term.Id.ToString()+";#"
                        }
                    }
                    $setDefVal=$setDefVal.TrimEnd(";#")
                }
                elseif($type -eq "TaxonomyFieldType")
                {
                    $defaultValues = $column.DefaultValues
                    if(($defaultValues -ne $null) -and ($defaultValues -ne ""))
                    {
                        $term=$termSet.Terms[$defaultValues]
						$wssIDToSet = AddTaxonomyHiddenListItem $web $term 
                        $setDefVal=[string]$wssIDToSet + ";#" + $term.GetPath() + [Microsoft.SharePoint.Taxonomy.TaxonomyField]::TaxonomyGuidLabelDelimiter + $term.Id.ToString()
                    }
                }
                $field.DefaultValue=$setDefVal
                $field.Update($PushChanges)
            }
            elseif(($type -ne "UserMulti") -and ($type -ne "User") -and ($type -ne "Note"))
            {
                $defaultValues = $column.DefaultValues
                if(($defaultValues -ne $null) -and ($defaultValues -ne ""))
                {
                    $field=$web.Fields.GetFieldByInternalName($column.InternalName)
                    $field.DefaultValue=$defaultValues
                    $field.Update($PushChanges)
                }
            }
    }
    catch
    {
        Write-Host "`nException for site column :" $column.ColumnName "`n" $Error -ForegroundColor Red
        Write-Output "`nException for site column :" $column.ColumnName "`n" $Error 
    }
}

# =================================================================================
#
# FUNC: AddTaxonomyHiddenListItem
# DESC: Add term to taxonomy hidden list.
#
# =================================================================================
function AddTaxonomyHiddenListItem($w,$termToAdd)
{
      $wssid = $null; #return value
      $count = 0;     
      $l = $w.Lists["TaxonomyHiddenList"]; 
      #check if Hidden List Item already exists
      foreach($item in $l.Items){
        $xml = [xml]$item.xml;#cast the xml TaxonomyHiddenList item values
        $temID = $xml.row.ows_IdForTerm #get the IdForTerm, this is the key that unlocks all the doors
        if($temID -eq $termToAdd.ID){ #compare the IdForTerm in the TaxonomyHiddenList item to the term in the termstore
            Write-Host $item.Name "Taxonomy Hidden List Item already exists" -ForegroundColor Yellow
            $wssid =  $item.ID; #get and return the WSSID needed to set the default clumn value
            return $wssid;
        } 
      }
      $newListItem = $l.Items.ADD();
      $newListItem["Title"] = $termToAdd.Name;
      $newListItem["IdForTermStore"] = $termToAdd.TermStore.ID;
      $newListItem["IdForTerm"] = $termToAdd.ID;
      $newListItem["IdForTermSet"] = $termToAdd.TermSet.ID;
      $newListItem["Term"] = $termToAdd.Name;
      $newListItem["Path"] = $divTerm.Name + ":" + $termToAdd.Name;
      $newListItem["CatchAllDataLabel"] = $termToAdd.Name + "#Љ|";  #"Љ" special char
      $newListItem["Term1033"] = $termToAdd.Name;
      $newListItem["Path1033"] = $divTerm.Name + ":" + $termToAdd.Name;
      $newListItem.Update();
      foreach($item in $l.Items){
        $xml = [xml]$item.xml;
        $temID = $xml.row.ows_IdForTerm
        if($temID -eq $termToAdd.ID){
            $wssid =  $item.ID; #get and return the WSSID needed to set the default clumn value
        } 
      }     
	  return $wssid;      
}


#===================================================================================
#
# Function: DeleteColumn
# DESC: Delete Site column from site collection
#
#===================================================================================
function DeleteColumn([object] $column, $site)
{
	$fieldName=$column.InternalName
	$web = $site.RootWeb
	
	#Delete field from all content types
    foreach($ct in $web.ContentTypes) {
        $fieldInUse = $ct.FieldLinks | Where {$_.Name -eq $fieldName }
        if($fieldInUse) {            
            $ct.FieldLinks.Delete($fieldName)
            $ct.Update()
			Write-Host "Field $($fieldName) remove successfully from ContentType: $($ct.Name) at web:- $($column.SiteUrl)" -ForegroundColor Green
			Write-Output "Field $($fieldName) remove successfully from ContentType: $($ct.Name) at web:- $($column.SiteUrl)"
        }
    }

    #Delete column from all lists in all sites of a site collection
    $site | Get-SPWeb | ForEach-Object {
	   #Specify list which contains the column
		$numberOfLists = $_.Lists.Count
		for($i=0; $i -lt $_.Lists.Count ; $i++) {
			$list = $_.Lists[$i]
			#Specify column to be deleted
			if($list.Fields.ContainsField($fieldName)) {
				$fieldInList = $list.Fields.GetFieldByInternalName($fieldName)

				if($fieldInList) {					
					#Allow column to be deleted
					$fieldInList.AllowDeletion = $true
					#Delete the column
					$fieldInList.Delete()
					 
					#Update the list
					$list.Update()
					Write-Host "Field $($fieldName) deleted successfully from list:- $($list.Title) at web:- $($_.URL )" -ForegroundColor Green
					Write-Output "Field $($fieldName) deleted successfully from list:- $($list.Title) at web:- $($_.URL)"
				}
			}
		}
    }
    

    # Remove the field itself
    if($web.Fields.ContainsField($fieldName)) {       
		$siteColumn = $web.Fields.GetFieldByInternalName($fieldName)
		$noteFieldInternalName = $null        

        #If it is a taxonomy field, get the id of the hidden note field
        if($siteColumn.GetType().Name -eq "TaxonomyField")
        {
            #TaxtField holds the Id of the hidden notes field            
            $noteFieldInternalName = $siteColumn.Id -ireplace "-",""
        }
        
        #Delete the Site Column
        $siteColumn.Delete()
        #Write-Host $noteFieldId
         
        #If notes field is there, find it and delete it as well
        if($noteFieldInternalName -ne $null)
        {
            $noteField = $web.Fields.GetFieldByInternalName($noteFieldInternalName)
            if($noteField -ne $null)
            {
                $noteField.Delete()				
                Write-Host "Note field $($noteField.Title) for taxonomy field $($fieldName) deleted successfully at web:- $($web.Url)"  -ForegroundColor Green
				Write-Output "Note field $($noteField.Title) for taxonomy field $($fieldName) deleted successfully at web:- $($web.Url)"  
            }
			else
			{
				Write-Host "Note field not found for taxonomy field $($fieldName) at web:- $($web.Url)"  -ForegroundColor Yelllow
				Write-Output "Note field not found for taxonomy field $($fieldName) at web:- $($web.Url)"  
			}
        }
		Write-Host "Site column $($fieldName) Deleted successfully at web:- $($web.Url)" -ForegroundColor Green
		Write-Output "Site column $($fieldName) Deleted successfully at web:- $($web.Url)"
    }
}