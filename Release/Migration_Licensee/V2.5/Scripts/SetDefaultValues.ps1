﻿# =================================================================================
#
#Main Function to set default values at library level.#
# =================================================================================
function LibDefaultValues([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..."
	}
	
	Write-Host "Successfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Successfully read config file $($ConfigPath) file"
   
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
            foreach($lst in $cfg.Libraries.Library)
            {
                SetLibraryDefaultValue $lst
            }
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host "Error: " $Error -ForegroundColor Red
        Write-Output "Error: " $Error
	}
}

# =================================================================================
#
# FUNC: SetLibraryDefaultValue
# DESC: Set default value for column at library level
#
# =================================================================================
function SetLibraryDefaultValue([object] $defVal)
{
    try
    {
        $error.Clear()
        $web =Get-SPWeb $defVal.SiteUrl
        $field=$null
        $lib=$null
        if(![string]::IsNullOrEmpty($defVal.LibraryName))
        {
            $lib=$web.Lists.TryGetList($defVal.LibraryName)
            if($lib)
            {
                if(![string]::IsNullOrEmpty($defVal.FieldInternalName))
                {
                    $field=$lib.Fields.GetFieldByInternalName($defVal.FieldInternalName)
                }
                elseif(![string]::IsNullOrEmpty($defVal.FieldName))
                { 
                    $field=$lib.Fields[$defVal.FieldName]
                }
                else
                {
                    Write-Host "`Field name is empty!" -ForegroundColor Yellow
                    Write-Output "`Field name is empty!`n"
                }
            }
            else
            {
                Write-Host "`nLibrary $($defVal.LibraryName) does not exist in web $($web.Url)" -ForegroundColor Yellow
                Write-Output "Library $($defVal.LibraryName) does not exist in web $($web.Url)"
            }
        }
        else
        {
            Write-Host "`nLibrary name $($defVal.LibraryName) is empty" -ForegroundColor Yellow
            Write-Output "Library name $($defVal.LibraryName) is empty"
        }
        if($field)
        {
            $type=$defVal.Type
			$siteCol=$web.Site
			$rootWeb=$siteCol.RootWeb
            if(($type -eq "TaxonomyFieldType") -or ($type -eq "TaxonomyFieldTypeMulti"))
            {
                $setDefVal=""
                $termSetId=$field.TermSetId.ToString()
                $centralAdmin = Get-SPWebApplication -IncludeCentralAdministration | Where {$_.IsAdministrationWebApplication} | Get-SPSite
                $session = new-object Microsoft.SharePoint.Taxonomy.TaxonomySession($centralAdmin)
                $termStore = $session.TermStores[$defVal.TermStore]
                $termSet = $termStore.GetTermSet([GUID]$termSetId)               
                
                $defaultValues = $defVal.DefaultValues

				if($type -eq "TaxonomyFieldTypeMulti")
				{
					$defaultValues = @($defVal.DefaultValues.Split(","))
					foreach($value in $defaultValues)
					{
						if(($value -ne $null) -and ($value -ne ""))
						{
							$term=$termSet.Terms[$value]
							if($term -eq $null)
							{
								if($value.Contains('&'))
								{
									$value=$value -replace '&','＆'
									$term=$termSet.Terms[$value]
								}
							}
							$wssIDToSet = AddTaxonomyHiddenListItem $rootWeb $term 
							$setDefVal=$setDefVal + ";#" +  [string]$wssIDToSet + ";#" + $term.Name + [Microsoft.SharePoint.Taxonomy.TaxonomyField]::TaxonomyGuidLabelDelimiter + $term.Id.ToString()
						}
					}
					$setDefVal=$setDefVal.TrimEnd(";#")
				}
				elseif($type -eq "TaxonomyFieldType")
				{
					$defaultValues = $defVal.DefaultValues
					if(($defaultValues -ne $null) -and ($defaultValues -ne ""))
					{
						#Get all terms under term set
						$terms = $termSet.GetAllTerms()

						$termToMap =$defVal.DefaultValues
						if($termToMap.Contains(";"))
						{
							$multiLevelTaxonomyTerm=$termToMap.Split(";")
							$m=0;
							foreach($termLabel in $multiLevelTaxonomyTerm)
							{
								$term = $terms | Where-Object {$_.Name -eq $termLabel}   
								$terms = $term.Terms    
							}
						}
						else
						{
							#Get the term to map the column to
							$term=$termSet.Terms[$defaultValues]
						}
						
						if($term -eq $null)
						{
							if($defaultValues.Contains('&'))
							{
								$defaultValues=$defaultValues -replace '&','＆'
								$term=$termSet.Terms[$defaultValues]
							}
						}
						$wssIDToSet = AddTaxonomyHiddenListItem $rootWeb $term 
						$setDefVal=[string]$wssIDToSet + ";#" + $term.Name + [Microsoft.SharePoint.Taxonomy.TaxonomyField]::TaxonomyGuidLabelDelimiter + $term.Id.ToString()
					}
					$setDefVal=$setDefVal.TrimEnd(";#")
				}
                if(![string]::IsNullOrEmpty($defVal.FolderUrl))
                {
                    $folder = $web.GetFolder($defVal.SiteURL+$defVal.FolderUrl)
                    $ColumnDefaults = New-Object -TypeName Microsoft.Office.DocumentManagement.MetadataDefaults $lib
                    if ($folder.Exists)
                        {
                           $ColumnDefaults.SetFieldDefault($folder,$field.InternalName,$setDefVal) | Out-Null
                           $ColumnDefaults.Update()   
                           Write-Host "`nDefault value for folder $($defVal.FolderUrl) in library $($defVal.LibraryName) set successfully" -ForegroundColor Green
                           Write-Output "Default value for folder $($defVal.FolderUrl) in library $($defVal.LibraryName) set successfully"                    
                        }
                        else
                        {
                           $mainFolderName = $defVal.FolderUrl.LastIndexOf("/")
                           $folderName = $defVal.FolderUrl.Substring($mainFolderName + 1)
                           $newFolder = $lib.AddItem("", [Microsoft.SharePoint.SPFileSystemObjectType]::Folder, $folderName)
                           $newFolder.Update()
                           Write-Host "`n$($defVal.FolderUrl) folder created" -ForegroundColor Green
                           Write-Output "$($defVal.FolderUrl) folder created"
                           $ColumnDefaults.SetFieldDefault($folder,$field.InternalName,$setDefVal) | Out-Null
                           $ColumnDefaults.Update()
                           Write-Host "`nDefault value for folder $($defVal.FolderUrl) in library $($defVal.LibraryName) set successfully" -ForegroundColor Green
                           Write-Output "Default value for folder $($defVal.FolderUrl) in library $($defVal.LibraryName) set successfully"
                        }
                }
                else
                {
                    $ColumnDefaults = New-Object -TypeName Microsoft.Office.DocumentManagement.MetadataDefaults $lib
                    $ColumnDefaults.SetFieldDefault($lib.RootFolder,$field.InternalName,$setDefVal) | Out-Null
                    $ColumnDefaults.Update()
                    Write-Host "`nDefault value for field $($defVal.FieldName) in library $($defVal.LibraryName) set successfully" -ForegroundColor Green
                    Write-Output "Default value for field $($defVal.FieldName) in library $($defVal.LibraryName) set successfully"
                }
            }
            elseif(($type -ne "UserMulti") -and ($type -ne "User") -and ($type -ne "Note"))
            {
                $defaultValues = $defVal.DefaultValues
                if(![string]::IsNullOrEmpty($defaultValues))
                {
                    if(![string]::IsNullOrEmpty($defVal.FolderUrl))
                    {
                        $folder = $web.GetFolder($defVal.SiteURL+ $defVal.FolderUrl)
                        $ColumnDefaults = New-Object -TypeName Microsoft.Office.DocumentManagement.MetadataDefaults $lib

                        if ($folder.Exists)
                        {
                           $ColumnDefaults.SetFieldDefault($folder,$field.InternalName,$defaultValues)
                           $ColumnDefaults.Update()   
                           Write-Host "`nDefault value for folder $($defVal.FolderUrl) in library $($defVal.LibraryName) set successfully" -ForegroundColor Green
                           Write-Output "Default value for folder $($defVal.FolderUrl) in library $($defVal.LibraryName) set successfully"                    
                        }
                        else
                        {
                           $mainFolderName = $defVal.FolderUrl.LastIndexOf("/")
                           $folderName = $defVal.FolderUrl.Substring($mainFolderName + 1)
                           $newFolder = $lib.AddItem("", [Microsoft.SharePoint.SPFileSystemObjectType]::Folder, $folderName)
                           $newFolder.Update()
                           Write-Host "`n$($defVal.FolderUrl) folder created" -ForegroundColor Green
                           Write-Output "$($defVal.FolderUrl) folder created"
                           $ColumnDefaults.SetFieldDefault($folder,$field.InternalName,$defaultValues)
                           $ColumnDefaults.Update()
                           Write-Host "`nDefault value for folder $($defVal.FolderUrl) in library $($defVal.LibraryName) set successfully" -ForegroundColor Green
                           Write-Output "Default value for folder $($defVal.FolderUrl) in library $($defVal.LibraryName) set successfully"
                        }
                    
                    }
                    else
                    {
                        $ColumnDefaults = New-Object -TypeName Microsoft.Office.DocumentManagement.MetadataDefaults $lib
                        $ColumnDefaults.SetFieldDefault($lib.RootFolder,$field.InternalName,$defaultValues)
                        $ColumnDefaults.Update()
                        Write-Host "`nDefault value for field $($defVal.FieldName) in library $($defVal.LibraryName) set successfully" -ForegroundColor Green
                        Write-Output "Default value for field $($defVal.FieldName) in library $($defVal.LibraryName) set successfully"
                    }
                    
                }
            }
        }
        else
        {
            Write-Host "`nField with name $($defVal.ColumnName) not exist in Library $($defVal.LibraryName)" -ForegroundColor Yellow
            Write-Output "Field with name $($defVal.ColumnName) not exist in Library $($defVal.LibraryName) `n"
        }
    }
    catch
    {
        Write-Host "`nException for site column :" $column.ColumnName "`n" $Error -ForegroundColor Red
        Write-Output "`nException for site column :" $column.ColumnName "`n" $Error 
    }
}

# =================================================================================
#
# FUNC: AddTaxonomyHiddenListItem
# DESC: Add term to taxonomy hidden list.
#
# =================================================================================
function AddTaxonomyHiddenListItem($w,$termToAdd)
{
      $wssid = $null; #return value
      $count = 0;     
      $l = $w.Lists["TaxonomyHiddenList"]; 
      #check if Hidden List Item already exists
      foreach($item in $l.Items){
        $xml = [xml]$item.xml;#cast the xml TaxonomyHiddenList item values
        $temID = $xml.row.ows_IdForTerm #get the IdForTerm, this is the key that unlocks all the doors
        if($temID -eq $termToAdd.ID){ #compare the IdForTerm in the TaxonomyHiddenList item to the term in the termstore
           #Write-Host $item.Name "Taxonomy Hidden List Item already exists" -ForegroundColor Yellow
            $wssid =  $item.ID; #get and return the WSSID needed to set the default clumn value
            return $wssid;
        } 
      }
      $newListItem = $l.Items.ADD();
      $newListItem["Title"] = $termToAdd.Name;
      $newListItem["IdForTermStore"] = $termToAdd.TermStore.ID;
      $newListItem["IdForTerm"] = $termToAdd.ID;
      $newListItem["IdForTermSet"] = $termToAdd.TermSet.ID;
      $newListItem["Term"] = $termToAdd.Name;
      $newListItem["Path"] = $divTerm.Name + ":" + $termToAdd.Name;
      $newListItem["CatchAllDataLabel"] = $termToAdd.Name + "#Љ|";  #"Љ" special char
      $newListItem["Term1033"] = $termToAdd.Name;
      $newListItem["Path1033"] = $divTerm.Name + ":" + $termToAdd.Name;
      $newListItem.Update();
      foreach($item in $l.Items){
        $xml = [xml]$item.xml;
        $temID = $xml.row.ows_IdForTerm
        if($temID -eq $termToAdd.ID){
            $wssid =  $item.ID; #get and return the WSSID needed to set the default clumn value
        } 
      }     
	  return $wssid;      
}

# =================================================================================
#
#Main Function to set default values at library level.#
# =================================================================================
function DeleteFoldersFromList([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..."
	}
	
	Write-Host "Successfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Successfully read config file $($ConfigPath) file"
   
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
            foreach($lst in $cfg.Libraries.Library)
            {
                DeleteFolder $lst
            }
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host "Error: " $Error -ForegroundColor Red
        Write-Output "Error: " $Error
	}
}
# =================================================================================
#
# FUNC: DeleteFolder
# 
# =================================================================================
function DeleteFolder([object] $lst)
{
    try
    {
        $error.Clear()
        $web =Get-SPWeb $lst.SiteUrl
        $field=$null
        $lib=$null
        if(![string]::IsNullOrEmpty($lst.LibraryName))
        {
            $lib=$web.Lists.TryGetList($lst.LibraryName)
            if($lib)
            {
				$folder = $web.GetFolder($lst.SiteURL+ $lst.FolderUrl)				

				if ($folder.Exists)
				{				  
				   $folder.Delete()  
				   Write-Host "`nFolder $($lst.FolderUrl) in library $($lst.LibraryName) deleted successfully" -ForegroundColor Green
				   Write-Output "Folder $($lst.FolderUrl) in library $($lst.LibraryName) deleted successfully"                    
				}
				else
				{
				   Write-Host "`nFolder $($lst.FolderUrl) in library $($lst.LibraryName) does not exists for $($lst.SiteUrl)" -ForegroundColor Yellow
				   Write-Output "`nFolder $($lst.FolderUrl) in library $($lst.LibraryName) does not exists for $($lst.SiteUrl)" 
				}
			}
        }
		else
		{
		   Write-Host "`nLibrary $($lst.LibraryName) does not exists for $lst.SiteUrl" -ForegroundColor Green
		   Write-Output "`nLibrary $($lst.LibraryName) does not exists for $lst.SiteUrl" 
		}
    }
    catch
    {
        Write-Host "`nException for site column :$($lst.LibraryName) `n" $Error -ForegroundColor Red
        Write-Output "`nException for site column :$($lst.LibraryName) `n" $Error
    }
}