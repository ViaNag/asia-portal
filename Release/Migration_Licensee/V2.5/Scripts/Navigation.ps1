﻿# =================================================================================
#
#Main Function to create Global Navigation
#
# =================================================================================
function CreateGlobalNavigation([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file"
	if($Error.Count -eq 0)
	{
        try
        {
            $error.Clear()
            foreach($webNav in $cfg.GlobalNavigations.WebNavigation)
            {                
                $siteUrl=$webNav.Url
                $web =  Get-SPWeb $siteUrl
                $nodes=$null
                if([Microsoft.SharePoint.Publishing.PublishingWeb]::IsPublishingWeb($web))
                {
                    [Microsoft.SharePoint.Publishing.PublishingWeb]$pubWeb=[Microsoft.SharePoint.Publishing.PublishingWeb]::GetPublishingWeb($web)
                    $pubWeb.Navigation.GlobalIncludePages=[System.Convert]::ToBoolean(@{$true="false";$false=$webNav.ShowPages}[$webNav.ShowPages -eq ""])
					$pubWeb.Navigation.GlobalIncludeSubSites=[System.Convert]::ToBoolean(@{$true="false";$false=$webNav.ShowSubsites}[$webNav.ShowSubsites -eq ""])
					$pubWeb.Update()
                    $nodes  = $pubWeb.Navigation.GlobalNavigationNodes
                    if($nodes -ne $null)
                    {
                        ClearNavigationNodes $nodes
                        if($pubWeb.IsRoot)
                        {
                            ConfigureNavigationType $siteUrl $webNav.NavigationType $null $true
                        }
                        else
                        {
                             ConfigureNavigationType $siteUrl $webNav.NavigationType $null $false
                        }
                        foreach($gNav in $webNav.GlobalNavigation)
                        {
                            GlobalNavigationToAdd $gNav $nodes $siteUrl $webNav.NavigationType $webNav
                        }
                    }
                    else
                    {
                        if($nodes.Count -ge 0)
                        {
                            if($pubWeb.IsRoot)
                            {
                                ConfigureNavigationType $siteUrl $webNav.NavigationType $null $true
                            }
                            else
                            {
                                 ConfigureNavigationType $siteUrl $webNav.NavigationType $null $false
                            }
                            foreach($gNav in $webNav.GlobalNavigation)
                            {
                                GlobalNavigationToAdd $gNav $nodes $siteUrl $webNav.NavigationType $webNav
                            }
                        }
                        else
                        {
                            Write-Host "Global navigation node object is null due to which navigation cannot be set at url $($webNav.Url)" -ForegroundColor Yellow
                            Write-Output "Global navigation node object is null due to which navigation cannot be set at url $($webNav.Url)"
                        }
                    }
                }
                SortDeleteGlobalNavigation $webNav				
				$web.Dispose()
            }             
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host $Error -ForegroundColor Red
        Write-Output $Error
	}
}

# =================================================================================
#
# FUNC: ConfigureNavigationType
# DESC: Set navigation type on web
#
# =================================================================================
function ConfigureNavigationType($webUrl,$glbNav,$leftNav, [bool]$isSiteColl)
{
    try
    {
        $web=Get-SPWeb $webUrl
        $WebNavSettings = New-Object Microsoft.SharePoint.Publishing.Navigation.WebNavigationSettings($web)
        if($isSiteColl)
        {
            $WebNavSettings.GlobalNavigation.Source = "PortalProvider"
            $WebNavSettings.CurrentNavigation.Source = "PortalProvider"
        }
        else
        {
            if(![string]::IsNullOrEmpty($glbNav))
            {
                if($glbNav -eq "Inherit from Parent Site")
                {
                    $WebNavSettings.GlobalNavigation.Source = "InheritFromParentWeb"
                }
                else
                {
                    $WebNavSettings.GlobalNavigation.Source = "PortalProvider"
                }
            }
            if(![string]::IsNullOrEmpty($leftNav))
            {
                if($leftNav -eq "Inherit from Parent Site")
                {
                    $WebNavSettings.CurrentNavigation.Source = "InheritFromParentWeb"
                }
                else
                {
                    $WebNavSettings.CurrentNavigation.Source = "PortalProvider"
                }
            }
        }
        $WebNavSettings.Update()
        $web.Update()
    }
    catch
    {
        Write-Host "`nException in setting configuration values on web :" $Error -ForegroundColor Red
        Write-Output "`nException in setting configuration values on web :" $Error
    }
}


# =================================================================================
#
# FUNC: ClearNavigationNodes
# DESC: Delete navigation nodes.
# =================================================================================
function ClearNavigationNodes($nodes)
{
    try
    {
        $error.Clear()
        $count=$nodes.Count
        for( $i=$count ;$i -gt 0;$i--)
		{   
			$node = $nodes[$i-1]
			if($node -ne $null)
			{
				$nodes.delete($node)
			}
		}
    }
    catch
    {
        Write-Host "Exception while deleting nodes " $Error -ForegroundColor Red
        Write-Output "Exception while deleting nodes " $Error
    }
}


# =================================================================================
#
# FUNC: GlobalNavigationToAdd
# DESC: Create global navigation.
# =================================================================================
function GlobalNavigationToAdd([object] $gNav, $nodes, $SiteURL, $navType, $navXml)
{
    try
    {
        $error.Clear()
        $spWeb = Get-SPWeb $SiteURL
        
        if($navType -ne "Inherit from Parent Site")
        {
			if([string]::isNullOrEmpty($gNav.LinkName))
			{
				$CreateSPNavigationNode = [Microsoft.SharePoint.Publishing.Navigation.SPNavigationSiteMapNode]::CreateSPNavigationNode
				$newNode = $CreateSPNavigationNode.Invoke($gNav.HeaderName, $gNav.HeaderURL, [Microsoft.SharePoint.Publishing.NodeTypes]::Heading, $nodes)
				$newNode.Properties["Audience"] = ";;;;" + $gNav.HeaderTargetAudience
				$newNode.Update()
				Write-Host "$($newNode.Title): heading  Added Successfully." -ForegroundColor Green
				Write-Output "$($newNode.Title): heading  Added Successfully."   

				$linksForHeader=$null
				$linksForHeader= $navXml.GlobalNavigation | Where-object{($_.HeaderName -eq $gNav.HeaderName) -and ($_.LinkName -ne [string]::Empty)}
				$newNodeCollection=$null
				$newNodeCollection = $newNode.Children 
				foreach($link in $linksForHeader)
				{
					$linkNode = $CreateSPNavigationNode.Invoke($link.LinkName, $link.LinkURL, [Microsoft.SharePoint.Publishing.NodeTypes]::AuthoredLinkPlain, $newNodeCollection)
					$linkNode.Properties["Audience"] = ";;;;" + $link.LinkTargetAudience
					$linkNode.Update() 
				}
			}
        }
        $spWeb.Update()
		$spWeb.Dispose()  
	}
    catch
    {
        Write-Host "`nException :" $gNav.LinkName "`n" $Error -ForegroundColor Red
        Write-Output "`nException :" $gNav.LinkName "`n" $Error
    }
}

# =================================================================================
#
#Main Function to create Left Navigation
#
# =================================================================================
function CreateLeftNavigation([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file"
	if($Error.Count -eq 0)
	{
        try
        {
            $error.Clear()
            foreach($webNav in $cfg.LeftNavigations.WebNavigation)
            {
                $siteUrl=$webNav.Url
                $site = new-object Microsoft.SharePoint.SPSite $siteUrl
                $web = Get-SPWeb $siteUrl
                $nodes=$null
                if([Microsoft.SharePoint.Publishing.PublishingWeb]::IsPublishingWeb($web))
                {
                    [Microsoft.SharePoint.Publishing.PublishingWeb]$pubWeb=[Microsoft.SharePoint.Publishing.PublishingWeb]::GetPublishingWeb($web)
                    $pubWeb.Navigation.CurrentIncludePages=[System.Convert]::ToBoolean(@{$true="false";$false=$webNav.ShowPages}[$webNav.ShowPages -eq ""])
					$pubWeb.Navigation.CurrentIncludeSubSites=[System.Convert]::ToBoolean(@{$true="false";$false=$webNav.ShowSubsites}[$webNav.ShowSubsites -eq ""])
					$pubWeb.Update()
                    $nodes  = $pubWeb.Navigation.CurrentNavigationNodes
                    if($nodes -ne $null)
                    {
                        ClearNavigationNodes $nodes
                        if($pubWeb.IsRoot)
                        {
                            ConfigureNavigationType $siteUrl $null $webNav.NavigationType $true
                        }
                        else
                        {
                             ConfigureNavigationType $siteUrl $null $webNav.NavigationType $false
                        }
                        foreach($lNav in $webNav.LeftNavigation)
                        {
                            LeftNavigationToAdd $lNav $nodes $siteUrl $webNav.NavigationType
                        }
                    }
                    else
                    {
                        if($nodes.Count -ge 0)
                        {
                            if($pubWeb.IsRoot)
                            {
                                ConfigureNavigationType $siteUrl $webNav.NavigationType $null $true
                            }
                            else
                            {
                                 ConfigureNavigationType $siteUrl $webNav.NavigationType $null $false
                            }
                            foreach($lNav in $webNav.LeftNavigation)
                            {
                                LeftNavigationToAdd $lNav $nodes $siteUrl $webNav.NavigationType
                            }   
                        }
                        else
                        {
                            Write-Host "Left navigation node object is null due to which navigation cannot be set at url $($webNav.Url)" -ForegroundColor Yellow
                            Write-Output "Left navigation node object is null due to which navigation cannot be set at url $($webNav.Url)"
                        }
                    }
                }
				SortLeftNavigation $webNav 
				$web.Dispose()
            }
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host $Error -ForegroundColor Red
        Write-Output $Error
	}
}


# =================================================================================
#
# FUNC: LeftNavigationToAdd
# DESC: Create left navigation.
# =================================================================================
function LeftNavigationToAdd([object] $lNav, $nodes,$SiteURL, $navType)
{
    try
    {
        $error.Clear()
        $spWeb = Get-SPWeb $SiteURL
		
        if($navType -ne "Inherit from Parent Site")
        {
            if(![string]::IsNullOrEmpty($lNav.HeaderName))
            {
                $headerNode=$nodes | where { $_.Title -eq $lNav.HeaderName}
                if(!$headerNode)
                {
                    $CreateSPNavigationNode = [Microsoft.SharePoint.Publishing.Navigation.SPNavigationSiteMapNode]::CreateSPNavigationNode
                    $headerNode = $CreateSPNavigationNode.Invoke($lNav.HeaderName, $lNav.HeaderURL, [Microsoft.SharePoint.Publishing.NodeTypes]::Heading, $nodes)
                    $headerNode.Properties["Audience"] = ";;;;" + $lNav.HeaderTargetAudience
			        $headerNode.Update()
			        Write-Host "$($headerNode.Title): heading  Added Successfully." -ForegroundColor Green
                    Write-Output "$($headerNode.Title): heading  Added Successfully."
                }
                else
                {
                    if(![string]::IsNullOrEmpty($lNav.HeaderTargetAudience))
                    {
                        $headerNode.Properties["Audience"] = ";;;;" + $lNav.HeaderTargetAudience
                    }
                    if(![string]::IsNullOrEmpty($lNav.HeaderURL))
                    {
                        $headerNode.Properties["Url"] = $lNav.HeaderURL
                    }
                    
			        $headerNode.Update()
                    Write-Host "$($headerNode.Title): heading  Updated  Successfully." -ForegroundColor Green
                    Write-Output "$($headerNode.Title): heading Updated Successfully."
                }
            }   
            if(![string]::IsNullOrEmpty($lNav.LinkName))
            {
                $headerNode=$nodes | where { $_.Title -eq $lNav.HeaderName}
				if($lNav.LinkURL -like "*/_layouts/15/people.aspx*")
				{
					$urlSplitString=$lNav.LinkURL.Split('=')
					if($urlSplitString.Length -eq 2)
					{	 
						$spGroup=$web.SiteGroups[$urlSplitString[1]]
						if($spGroup)
						{
							$lNav.LinkURL= $urlSplitString[0] + "=" + $spGroup.Id
						}
					}
					
				}
                if($headerNode)
                {
                    $headingCollection = $headerNode.Children  
                    $CreateSPNavigationNode = [Microsoft.SharePoint.Publishing.Navigation.SPNavigationSiteMapNode]::CreateSPNavigationNode
			        $linkNode = $CreateSPNavigationNode.Invoke($lNav.LinkName, $lNav.LinkURL, [Microsoft.SharePoint.Publishing.NodeTypes]::AuthoredLinkPlain, $headingCollection)
                    
                    if(![string]::IsNullOrEmpty($lNav.TargetAudience))
                    {
                        $linkNode.Properties["Audience"] = ";;;;" + $lNav.TargetAudience
                    }
                    
			        $linkNode.Update()
                    Write-Host "$($linkNode.Title): link  added successfully under header $($lNav.HeaderName)" -ForegroundColor Green
                    Write-Output "$($linkNode.Title): link  added successfully under header $($lNav.HeaderName)"
                }
            }                           
        }
        $spWeb.Update()
		$spWeb.Dispose()
	}
    catch
    {
        Write-Host "`nException :" $lNav.LinkName "`n" $Error -ForegroundColor Red
        Write-Output "`nException :" $lNav.LinkName "`n" $Error
    }
}

# =================================================================================
#
# FUNC: SortLeftNavigation
# DESC: Sort left navigation.
# =================================================================================
function SortLeftNavigation([object]$webNav)
{
    try
    {
        $error.Clear()
		$spWebObj = Get-SPWeb $webNav.Url
		$nodes=$null
		if([Microsoft.SharePoint.Publishing.PublishingWeb]::IsPublishingWeb($spWebObj))
		{
			[Microsoft.SharePoint.Publishing.PublishingWeb]$pubWeb=[Microsoft.SharePoint.Publishing.PublishingWeb]::GetPublishingWeb($spWebObj)
			$qlNav  = $pubWeb.Navigation.CurrentNavigationNodes
			if($qlNav -ne $null)
			{
				foreach($lNav in $webNav.LeftNavigation)
				{
					if(![string]::IsNullOrEmpty($lNav.HeaderName))
					{
						$qlHeading = $qlNav | where { $_.Title -eq $lNav.HeaderName }
						$qlHeading.MoveToLast($qlNav)
					}
					if(![string]::IsNullOrEmpty($lNav.LinkName))
					{
						$qlLink = $qlHeading.Children | where { $_.Title -eq $lNav.LinkName }
						$qlLink.MoveToLast($qlHeading.Children)
					}
				}
			}
		}		
		$spWebObj.Dispose()
	}
	catch
    {
        Write-Host "`nException : in sorting left navigation`n" $Error -ForegroundColor Red
        Write-Output "`nException : in sorting left navigation`n" $Error
    }
}

# =================================================================================
#
# FUNC: SortDeleteGlobalNavigation
# DESC: Sort th existing link and delete unwanted link from global navigation.
# =================================================================================
function SortDeleteGlobalNavigation([object]$webNav)
{
    try
    {
        $error.Clear()
		$spWebObj = Get-SPWeb $webNav.Url
		if([Microsoft.SharePoint.Publishing.PublishingWeb]::IsPublishingWeb($spWebObj))
		{
			[Microsoft.SharePoint.Publishing.PublishingWeb]$pubWeb=[Microsoft.SharePoint.Publishing.PublishingWeb]::GetPublishingWeb($spWebObj)
			$globalNav  =  $pubWeb.Navigation.GlobalNavigationNodes
			if($globalNav -ne $null)
			{
                [System.Collections.ArrayList]$globalNavArr=@()
				foreach($gNav in $webNav.GlobalNavigation)
				{
					if([string]::IsNullOrEmpty($gNav.LinkName))
					{
						$gLink = $globalNav | where { $_.Title -eq $gNav.HeaderName }
                        if($gLink -ne $null)
                        {
                            $globalNavArr.Add($gNav.HeaderName) | Out-Null
						    $gLink.MoveToLast($globalNav)
                        }
					}
				}
                foreach($gNode in $globalNav)
                {
                    $gNavExist=$globalNavArr -contains $gNode.Title
                    if(!$gNavExist)
                    {
                        $globalNav.Delete($gNode)
                    }
                }
			}
		}		
		$spWebObj.Dispose()
	}
	catch
    {
        Write-Host "`nException : in sorting global navigation`n" $Error -ForegroundColor Red
        Write-Output "`nException : in sorting global navigation`n" $Error
    }
}