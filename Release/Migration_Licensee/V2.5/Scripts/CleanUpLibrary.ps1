﻿#Usage: Run ./CleanUpLibrary and pass arguments "Site URL" "List1_Name" List2_Name" "List3_Name"...so on

#Load SharePoint Snapin if not present
$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'}
if ($snapin -eq $null) 
{
    Write-Host "Loading SharePoint Powershell Snapin" -ForegroundColor Green
    Add-PSSnapin "Microsoft.SharePoint.Powershell"
}

function DeleteFromLibrary([string] $listName) 
{
       

$web = Get-SPWeb $webUrl

$list=$web.Lists[$listName] 
# Delete root files
DeleteFiles $list.RootFolder.Url

# Delete files in folders
foreach ($folder in $list.Folders) {
    DeleteFiles $folder.Url
}

# Delete folders
foreach ($folder in $list.Folders) {
    try {
        Write-Host("DELETED FOLDER: " + $folder.name)
        $list.Folders.DeleteItemById($folder.ID)
    }
    catch {
        # Deletion of parent folder already deleted this folder
    }
}

}
function DeleteFiles([string]$folderUrl)  
{
    $folder = $web.GetFolder($folderUrl)
    foreach($file in $folder.Files) 
{
    if($file)
    {
        # Delete file by deleting parent SPListItem
        Write-Host("DELETED FILE: " + $file.name)
                $list.Items.DeleteItemById($file.Item.Id)
    }
}
}
#check the parameters
if($args.Count -eq 0)
{ 
    Write-Host "Site Url and Library url paramaters missing" -ForegroundColor Red
    ErrorExit
}
else
{
$webUrl=$args[0]
}
if($args.Count -gt 1)
{
$i =1
	foreach($item in $args)
	{
	if($args.IndexOf($item) -eq 0)
{
continue
}
Write-Host $args[$i]
DeleteFromLibrary $args[$i]
$i++
	}
}
else
{
Write-Host "Missing library name parameters" -ForegroundColor Red
    ErrorExit
}
 
