﻿
function global:CreateListFromSchema(
	[string]$siteURL,
	[string]$title,
	[string]$description,
	[string]$url,
	[string]$featureId,
	[string]$templateType,
	[string]$docTemplateType,
	[string]$listSchemaXmlFile,
	[string]$quickLaunchOptions,
	[string]$listInstanceFeatureDefintion) {


	#Start of script
	$cfg = [string](get-content $listSchemaXmlFile)
	
	# Exit if config file is invalid
	if( $? -eq $false ) 
	{
viaco		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..."
	}
	Write-Host "Sucessfully read config file $listSchemaXmlFile file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $($listSchemaXmlFile) file"
	
	$web = Get-SPWeb $siteURL
	$features = [Microsoft.SharePoint.Administration.SPFarm]::Local
	 
	$id = [Guid]("$listInstanceFeatureDefintion") 
	 
	$feature = $features[$id]

	$listid=$web.Lists.Add($title, $description, $url, $featureId, [int]$templateType, $docTemplateType, $cfg, $feature, [int]$quickLaunchOptions)
	#End of script
}

 

function CreateListsUsingSchemaXML([string]$ConfigPath = "")
{	
	$Error.Clear();
    
	$config = [xml](get-content $ConfigPath)
   
    if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
	{    
		Add-PsSnapin Microsoft.SharePoint.PowerShell
	}
	
	# Exit if config file is invalid
	if( $? -eq $false ) 
	{
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..."
	}

	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $($ConfigPath) file"
	if($Error.Count -eq 0)
	{
		try
        {
			$Error.Clear()
			if( $? -eq $false ) 
			{
				Write-Host "Error: " $ScriptDir + $List.listSchemaXmlFileName -ForegroundColor Red
			}
			
			$ListInfo = $config.ListInfo
			$error.clear()
			
			foreach($Webinfo in $ListInfo.Web)	
			{
				foreach($List in $Webinfo.List)	
				{
					[string]$siteURL = $Webinfo.URL
					[string]$title = $List.title
					[string]$description = $List.description
					[string]$url = $List.url
					[string]$featureId = $List.featureId
					[string]$templateType = $List.templateType
					[string]$docTemplateType = $List.docTemplateType
					[string]$currentLocation = Get-Location
					[string]$listSchemaXmlPath = $currentLocation + "\ListSchemaXML\" + $List.listSchemaXmlFileName
					[string]$listInstanceFeatureDefintion = $List.listInstanceFeatureDefintion
					[string]$quickLaunchOptions = $List.quickLaunchOptions

					CreateListFromSchema $siteURL  $title  $description  $url  $featureId  $templateType $docTemplateType  $listSchemaXmlPath  $quickLaunchOptions   $listInstanceFeatureDefintion 
					Write-Host "List created - $($List.title) successfully" -ForegroundColor Green
					Write-Output "List created - $($List.title) successfully"
				}
				Write-Host "All List created for web url- $($Webinfo.URL) successfully" -ForegroundColor Green
				Write-Output  "All List created for web url- $($Webinfo.URL) successfully"
			}
		}
		catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host "Error: " $Error -ForegroundColor Red
        Write-Output "Error: " $Error
	}
}
