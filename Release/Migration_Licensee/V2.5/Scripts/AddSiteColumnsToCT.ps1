##################################################################################################
# ================================================================================================
# Add Site columns to content type
# ================================================================================================
##################################################################################################

if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

function CallAddSiteColumnsToCT([string]$ConfigPath = "")
{
$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
	if($Error.Count -eq 0)
	{
		$configXml = $cfg
		if( $? -eq $false ) 
		{
			Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
		}

		if ($configXml.Sites)
		{
			foreach ($SiteCol in $configXml.Sites.Site)
			{
				try
				{
					$site = Get-SPSite -Identity $SiteCol.Path
					$web = $site.RootWeb 
					try
					{
						foreach($ContentType in $SiteCol.ContentType)
						{
							$ct=$web.ContentTypes[$ContentType.Name]; 
							try
							{
								foreach($siteColumn in $ContentType.SiteColumn)
								{
								$fieldAdd=$web.Fields.GetFieldByInternalName($siteColumn.Name)
								$fieldLink= New-Object Microsoft.SharePoint.SPFieldLink $fieldAdd
								if( $siteColumn.readonly -eq "1")
								{
									$fieldLink.ReadOnly=$true
								} 
								$ct.FieldLinks.Add($fieldLink);
								Write-Host "Site Column $($siteColumn.Name) added to content type  $($ContentType.Name) successfully"  -ForegroundColor Green       
								}
							 }
							 catch
							 {
								Write-Host "Exception while fecthing site column $($siteColumn.Name)" $Error -ForegroundColor Red
							 }
							$ct.Update($true)
							Write-Host "Content Type $($ContentType.Name) updated successfully" -ForegroundColor Green
						}
					}
					catch
					{
						Write-Host "Exception while fetching content type $($ContentType.Name) from site collection: " $Error -ForegroundColor Red
					}
					$web.Dispose()
				}
				catch
				{
					Write-Host "Exception while fetching site collection: " $siteCollection.RootWeb.Title $Error -ForegroundColor Red
				}
			}
		}
		else
		{
			Write-Host "Configuration xml does not contain sites node" $Error -ForegroundColor Red
		}
	}
}
