function CreateRecordsContentTypes([string]$ConfigPath = "")
{
	$Error.Clear();
	$config = [xml](get-content $ConfigPath)
    if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
	{    
		  Add-PsSnapin Microsoft.SharePoint.PowerShell
	}
    
	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
        $msg = "Error: Could not read config file: " + $ConfigPath + ". Exiting ..."
		Logging $msg "Red"
	}
	
	$msg = "Sucessfully read config file: " + $ConfigPath
	Logging $msg "Green"
	
	if($Error.Count -eq 0)
	{
        try
        {	
			$Error.Clear()
			$recordCTConfig = $config.RecordConfigurations.RecordContentTypes
			foreach($recordCT in $recordCTConfig.RecordContentType)	
			{
				$ctHubSiteUrl = $recordCT.CTSrcUrl				
				$ctHubWeb = Get-SPWeb $ctHubSiteUrl
				$contentTypeCSVFilePath = $currentLocation + "\configuration\" +  $operationFolder + "\" + $recordCT.ConfigFileName
				
				$ctList = Import-Csv $contentTypeCSVFilePath
				$msg = "Sucessfully read contentType CSV file and file path is " + $contentTypeCSVFilePath 
				Logging $msg "Green"
				
				foreach($ctItem in $ctList)
				{
					$web = Get-SPWeb $ctItem.SiteURL -ErrorVariable err -ErrorAction SilentlyContinue -AssignmentCollection $assignmentCollection

					if ($err)
					{
					   $msg = "Site URL " + $ctItem.SiteURL + " is invalid"
					   Logging $msg "Magenta"
					}
					else
					{
					   if($web.ContentTypes[$ctItem.CTName] -eq $null)
					   {
							#Create Content Type
							$ctParent = $web.ContentTypes[$ctItem.ParentCT]
							$ctype = new-object Microsoft.SharePoint.SPContentType($ctParent, $web.contenttypes, $ctItem.CTName)
							$ctype.Group = $ctItem.CTGroup
							$a = $web.ContentTypes.add($ctype)
							$web.Update()
							$msg = "Content Type " + $ctItem.CTName + " created in " + $web.Title
							Logging $msg "Green"
							$newCt = $web.ContentTypes[$ctItem.CTName]
							
							#-------------------------------------------------------
								$internalNameList = @()
								$fieldsInNewCt = $newCt.Fields
								foreach($newCtField in $fieldsInNewCt)
								{
								  $internalNameList += $newCtField.InternalName
								}
							#-------------------------------------------------------


							#Create Columns
							$sourceCt = $ctHubWeb.ContentTypes[$ctItem.SourceCT]
							if($sourceCt -eq $null)
							{
								$msg = "Source Content Type " + $ctItem.SourceCT + " not found in Content Type Hub"
								Logging $msg "Magenta"
							}
							else
							{
								foreach($field in $sourceCt.Fields)
								{   
									$msg = "Adding Field " + $field.Title
									Logging $msg "Cyan"
									Write-Host $field.TypeDisplayName -ForegroundColor Blue
									Write-Host $field.InternalName -ForegroundColor Blue
									$fInTargetSite = $null
									if($web.Fields.ContainsField($field.InternalName))
									{
										$fInTargetSite = $web.Fields.GetFieldByInternalName($field.InternalName)
										if($fInTargetSite -eq $null)
										{
											$msg = $field.Title + " not found in site " + $web.Title + ". It will not be added."
											Logging $msg "DarkRed"
										}
										else
										{
											#if(!$newCt.Fields.ContainsField($field.InternalName))
											if(!($internalNameList -contains $field.InternalName))
											{       
												#Write-Host "Adding " $field.Title " to Content Type " $newCt.Name -ForegroundColor Cyan
												$fieldLink=New-Object Microsoft.SharePoint.SPFieldLink($fInTargetSite)
												if($field.Hidden -eq $true)
												{
													#$fieldLink.Hidden = $true
												}
												if($field.Required -eq $true)
												{
													$fieldLink.Required = $true
												}
												$newCt.FieldLinks.Add($fieldLink)
												$msg = "Added " + $field.Title + " to Content Type " + $newCt.Name
												Logging $msg "Green"
												$newCt.Update()
											}
											else
											{
												$msg = $newCt.Name + " contains " + $field.Title + ", it wont be added"
												Logging $msg "Magenta"
											}
									   }

									}
									else
									{
										$msg = $field.Title + " not found in site " + $web.Title + ". It will not be added."
										Logging $msg "DarkRed"
									}
								}
							}		
							$web.Update()
					   }
					   else
					   {
							$msg = "Content Type " + $ctItem.CTName + " exists in site " + $ctItem.SiteURL
							Logging $msg "DarkRed"
					   }
					}
					$web.Dispose()
				}
				$ctHubWeb.Dispose()
			}
		}
		catch
        {
            $msg = "Exception :" + $Error 
			Logging $msg "Red"
        }
	}
	else
	{
		$msg = "Exception :" + $Error 
		Logging $msg "Red"
	}
}

