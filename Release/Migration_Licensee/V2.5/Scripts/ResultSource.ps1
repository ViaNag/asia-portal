﻿# =================================================================================
#
# FUNC: CreateResultSource
# DESC: Function to create result source at site/web and central admin level
# =================================================================================
function CreateResultSource($rs,$sspApp,$site)
{
    try
    {
        $resultSourceName = $rs.ResultSourceName
		$scope = $rs.SearchScope
        $query=$rs.ResultSourceQuery
		$query=$query -replace "&lt;", "<" -replace "&gt;", ">"
		
       # create manager instances
       $fedManager = New-Object Microsoft.Office.Server.Search.Administration.Query.FederationManager($sspApp)
       $web=$site.OpenWeb()
       $searchOwner = New-Object Microsoft.Office.Server.Search.Administration.SearchObjectOwner([Microsoft.Office.Server.Search.Administration.SearchObjectLevel]::$scope, $web)

       # define the required QUERY
       $queryProperties = New-Object Microsoft.Office.Server.Search.Query.Rules.QueryTransformProperties
   
       # define custom sorting
       <#$sortCollection = New-Object Microsoft.Office.Server.Search.Query.SortCollection
       $sortCollection.Add("LastModifiedTime", [Microsoft.Office.Server.Search.Query.SortDirection]::Descending)
       $queryProperties["SortList"] = [Microsoft.Office.Server.Search.Query.SortCollection]$sortCollection#>
 
   
       $source = $fedManager.GetSourceByName($resultSourceName, $searchOwner)
       if ($source -eq $null)
       {
	     # create result source
	     $resultSource = $fedManager.CreateSource($searchOwner)
	     $resultSource.Name = $resultSourceName
	     $resultSource.ProviderId = $fedManager.ListProviders()['Local SharePoint Provider'].Id
	     $resultSource.CreateQueryTransform($queryProperties, $query)
	     $resultSource.Commit()
         Write-Host "Result source: $($resultSourceName) created successfully" -ForegroundColor "Green"
         Write-Output "Result source: $($resultSourceName) created successfully"
       }
       else
       {
			$source.CreateQueryTransform($queryProperties, $query)
			$source.Commit()
			Write-Host "Result Source $($source) already exists and query updated successfully" -f Yellow
			Write-Output "Result Source $($source) already exists and query updated successfully"
       }
   }
   catch
   {
    Write-Host "Error while creating result source: $($resultSourceName)" $Error -ForegroundColor "Red"
    Write-Output "Error while creating result source: $($resultSourceName)" $Error
   }
}

# =================================================================================
#
# FUNC: CallCreateResultSource
# DESC: Main function to create result source
# =================================================================================
function CallCreateResultSource([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)
	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
	if($Error.Count -eq 0)
	{
        try
        {
		    $webAppFile = $cfg
		    $error.clear()
		    foreach($resultsource in $webAppFile.ResultSources.ResultSource)	
		    {
		        # Get the list for operation to perform
		        $SearchServiceName= $resultsource.SearchServiceName
		        $siteUrl= $resultsource.SiteUrl
		   
		        # load Search assembly
		        [void] [Reflection.Assembly]::LoadWithPartialName("Microsoft.Office.Server.Search")

		        # get current search service aplication
		        #**** Give the Search Service Name -> $SearchServiceName ****

		        $sspApp = Get-SPEnterpriseSearchServiceApplicationProxy $SearchServiceName

		        #****Give the site url where one want to create the result source***** 

		        $web = get-spweb $siteUrl -WarningAction SilentlyContinue
                $site=$web.Site
                if($site -ne $null)
                {
		            ###Calling function
		            CreateResultSource $resultsource $sspApp $site
                }
                else
                {
                    Write-Host "Error: site does not exist" $Error -ForegroundColor "Red"
                    Write-Output "Error: site does not exist" $Error
                }
		    }
        }
        catch
        {
            Write-Host "Error while creating result source: $($resultsource.ResultSourceName)" $Error -ForegroundColor "Red"
            Write-Output "Error while creating result source: $($resultsource.ResultSourceName)" $Error
        }
	}
}






