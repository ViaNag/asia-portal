# =================================================================================
#
#Main Function
#
# =================================================================================
function CreateRecordLibrary([string]$ConfigPath = "")
{
	$libList = Import-Csv $ConfigPath

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file"
	#load assembly in case library will be a CS Record Library
	[Reflection.Assembly]::Load("GimmalSoft.ComplianceSuite.WorkingWithRecords, Version=1.0.0.0, Culture=neutral, PublicKeyToken=d56cc47ac40ed024") 
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
			
				foreach($libItem in $libList)
				{
					$msg = "Checking library " + $libItem.LibName + " in site " + $web.Title + "....."
					Write-Host $msg -ForegroundColor Green 
					Write-Output $msg 
					$web = Get-SPWeb $libItem.SiteURL -ErrorVariable $err -ErrorAction SilentlyContinue -AssignmentCollection $assignmentCollection

					if ($err)
					{
					   #do something with error like Write-Error/Write-Warning or maybe just create a new web with New-SPWeb
					   $msg = "Error: Site URL " + $libItem.SiteURL + " is invalid"
					   Write-Host $msg -ForegroundColor Red 
					   Write-Output $msg 
					}
					else
					{
						$listCollection = $web.Lists
						$spLibrary = $listCollection.TrygetList($libItem.LibName)
						if($spLibrary -ne $null)
						{
							$msg = "Library " + $libItem.LibName + " exists in site " + $web.Title
							Write-Host $msg -ForegroundColor Green 
							Write-Output $msg 
							#Calling function to add associate workflow
							AssociateWorkflow $libItem.SiteURL $libItem.LibName "Disposition Tasks" "Workflow History" "Compliance Suite Delete Record and Metadata" "Compliance Suite Delete Record and Metadata" $true $false $false $libItem.DCLocation
						}     
						else
						{
							$listTemplate = $web.ListTemplates["Record Library"]
							$listGuid = $listCollection.Add($libItem.URL, "", $listTemplate)
							$listCreated = $listCollection.GetList($listGuid,$true)
						   
							if($listCreated -ne $null)
							{
								$listCreated.Title = $libItem.LibName
								$listCreated.Update()
								$msg = "Library " + $libItem.LibName + " with URL " + $libItem.URL + " created in site " + $web.Title
								Write-Host $msg -ForegroundColor Green 
								Write-Output $msg 
								#Calling function to add content types
								InstallDefaultRecordSettings $libItem.SiteURL $listCreated
								
								#Calling function to add associate workflow
								AssociateWorkflow $libItem.SiteURL $libItem.LibName "Disposition Tasks" "Workflow History" "Compliance Suite Delete Record and Metadata" "Compliance Suite Delete Record and Metadata" $true $false $false $libItem.DCLocation
							}
							else
							{
								$msg = "Library " + $libItem.LibName + " with URL " + $libItem.URL + " not exists in site " + $web.Title
								Write-Host $msg -ForegroundColor Green 
								Write-Output $msg 
							}
						}
					}
					$web.Dispose()
				}
			
            Write-Host "End : Addition of content type:- `n" -ForegroundColor Green
            Write-Output "End : Addition of content type:- `n"
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host $Error -ForegroundColor Red
        Write-Output $Error
	}
}


# =================================================================================
#
# FUNC: AssociateWorkflow
# DESC: Attach worflow to the library
#
# =================================================================================
function AssociateWorkflow([string]$siteURL, [string]$listName, [string]$taskListName, [string]$historyListName,
    [string]$workflowTemplateName, [string]$workflowName, [bool]$allowManualStart, [bool]$autoStartChange, [bool]$autoStartCreate ,[string]$dcLocation)
 {
	 try
	 {
	 
		$msg = 'Start WF association for ' + $workflowName + ' on list ' + $listName + ' and site ' + $siteURL
		Write-Host $msg -ForegroundColor Green 
		Write-Output $msg 
		#get culture
		$culture= Get-Culture
	 
		#Write-Host 'Get web'
		$hrWeb = Get-SPWeb $siteURL
	 
		#Write-Host 'Get Lists'
		$list = $hrWeb.Lists[$listName]
		$taskList = $hrWeb.Lists[$taskListName]
		$workflowHistoryList = $hrWeb.Lists[$historyListName]
	 
		#Write-Host 'Get base template'
		$basetemplate = $hrWeb.WorkflowTemplates.GetTemplateByName($workflowTemplateName,$culture);
		#Write-Host $basetemplate.Id
	 
		#set up variable to hold workflow instance if we find it
		$existingWorkflow = $null
	 
		#Write-Host 'Get workflow association'
		#loop through all associations on list
		foreach($tempWorkflowAssociation in $list.WorkflowAssociations)
		{
			#check if the base template id matches the base template of the current WF associaton
			#in additon check the name of the current WF association against the one we are interested in
			if($tempWorkflowAssociation.BaseTemplate.Id -eq $basetemplate.Id -and $tempWorkflowAssociation.Name -like $workflowName +"*" )
			{
				$existingWorkflow = $tempWorkflowAssociation
				break
			}
		}
		#check we have a workflow
		if($existingWorkflow -ne $null)
		{
			#Write-Host 'Got workflow associated with list'
			if($existingWorkflow.RunningInstances -gt 0)
			{
				Write-Host 'There are running instances so set to allow no new running instances'
				Write-Output 'There are running instances so set to allow no new running instances'
				$existingWorkflow.set_Enabled($false)
				$list.UpdateWorkflowAssociation($existingWorkflow)
			}
			else
			{
				Write-Host 'There are running instances so set to allow no new running instances' 'No running instances so remove'
				Write-Output 'There are running instances so set to allow no new running instances' 'No running instances so remove'
				$list.RemoveWorkflowAssociation($existingWorkflow)
			}
		}
		else
		{
			Write-Host 'There are running instances so set to allow no new running instances' 'No workflow associated with list'
			Write-Output 'There are running instances so set to allow no new running instances' 'No workflow associated with list'
		}
	 
		#Write-Host 'Create workflow association details'
		$date = Get-Date
		# $workflowName = $workflowName + " " + $date.ToShortDateString()
		$newWorkflow=[Microsoft.SharePoint.Workflow.SPWorkflowAssociation]::CreateListAssociation($basetemplate, $workflowName,$taskList,$workflowHistoryList)  
	 
		$newWorkflow.AllowManual = $allowManualStart
		$newWorkflow.AutoStartChange = $autoStartChange
		$newWorkflow.AutoStartCreate = $autoStartCreate
		
		#Write-Host 'Adding Destruction Certificate Settings'
		
		$newWorkflow.AssociationData = [string]::Format("<?xml version='1.0' encoding='utf-8' ?><AssociationDatas><DispositionTaskList>{0}</DispositionTaskList><TransferConfirmTaskList></TransferConfirmTaskList><Location><![CDATA[]]></Location><DestructionReceiptLibrary>$dcLocation</DestructionReceiptLibrary><ConfirmationReceiptList></ConfirmationReceiptList><RequireDeleteConfirmation>False</RequireDeleteConfirmation><SchemaMappingID>0</SchemaMappingID><rma:bulkProcessingFields xmlns:rma='http://rma.gimmal.com/2010/BulkProcessingSchema'><rma:steps><rma:step sequence='1' forDelete='true' completedField='Status' completedValue='Completed' confirmationRequired='false' label='Disposition Approval Task'><rma:field name='Body' value='' type='Text' label='Destruction Comments'/><rma:field name='Status' value='Completed' type='Text' label=''/><rma:field name='PercentComplete' value='1' type='Number' label=''/><rma:field name='_Deleted' value='' type='Boolean' label='Delete this record or category'/><rma:field name='_DestroyDate' value='$CurrentDate' type='DateTime' label=''/><rma:field name='_DestroyUser' value='$CurrentUser' type='Text' label=''/></rma:step></rma:steps></rma:bulkProcessingFields><SkipCaseBasedRetentionChecks>False</SkipCaseBasedRetentionChecks><SkipDispositionTasks>False</SkipDispositionTasks></AssociationDatas>", "Disposition Tasks", $destCertsList, $deleteConfirmationReqd)  
		
		 

		#Write-Host 'Add workflow to list'
		$a = $list.AddWorkflowAssociation($newWorkflow)
		$hrWeb.Dispose()
		Write-Host "Workflow Associated to List" -ForegroundColor Green
		Write-Output "Workflow Associated to List" 
	}
	catch [Exception]
	{
		 Logging "Workflow Association to list: FAILED"
        $msg = $_.Exception.Message + " - " + $_.Exception.Stacktrace
		Write-Host $msg -ForegroundColor Red
		Write-Output $msg
	}

}

# =================================================================================
#
# FUNC: InstallDefaultRecordSettings
# DESC: Add all content type to list/library
#
# =================================================================================

function InstallDefaultRecordSettings($SiteURL,$spList)
{    
   

    #Add Default Record Content Types To List or Library
    try
    {
        $success = [GimmalSoft.ComplianceSuite.WorkingWithRecords.Containers]::AddDefaultRecordContentTypesToList($SiteURL, $spList.ID)
        #$success = $containers.AddDefaultRecordContentTypesToList($webUrl, $spList.ID)
        if($success -eq $true)
        {
            Write-Host "Add default record content types to list: SUCCESS"
			Write-Output "Add default record content types to list: SUCCESS"
        }
        else
        {
            Write-Host "Add default record content types to list: FAILED."
            Write-Host "Check the ULS logs for details."
			Write-Output "Add default record content types to list: FAILED."
            Write-Output "Check the ULS logs for details."
        }
    }
    catch [Exception]
    {
        Write-Host "Add default record content types to list: FAILED"
		 Write-Output "Add default record content types to list: FAILED"
        $msg = $_.Exception.Message + " - " + $_.Exception.Stacktrace
        Write-Host $msg
		Write-Output $msg
    }
}

