﻿# ===================================================================================
# FUNC: UpdatePageLayout
# DESC: Update Page layouts
# ===================================================================================
 function UpdatePageLayout([String]$ConfigFileName = "")
 {
     if (-not $(Test-Path -Path $configFileName -Type Leaf))
	{
		Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
	}

	$configXml = [xml]$(get-content $configFileName)
	
	if( $? -eq $false ) 
	{
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..." | Out-Null
	}

	foreach($Page in $configXml.Configuration.Pages.Page)     
	{
		UpdateLayoutURL $configXml.Configuration.PublishingSite.URL $Page.LayoutName $Page.Name
	}
}
function UpdateLayoutURL([string]$WebURL, [string]$PageLayoutName,[string]$PublishingPageName)
{
     $Web = Get-SPWeb $WebURL
      
     #Get Publishing Site and Web
     $PublishingSite = New-Object Microsoft.SharePoint.Publishing.PublishingSite($Web.Site)
     $PublishingWeb = [Microsoft.SharePoint.Publishing.PublishingWeb]::GetPublishingWeb($web)
   
     #Get New Page Layout
     $SitePageLayouts = $PublishingSite.GetPageLayouts($false)
     $NewPageLayout = $SitePageLayouts | ? {$_.Name -eq $PageLayoutName}
  
     #Get Pages Library
     $publishingPages = $PublishingWeb.GetPublishingPages()

     $PublishingPage = $PublishingWeb.GetPublishingPage($WebURL + "/Pages/" + $PublishingPageName) 
     
    
     
     $PublishingPage.CheckOut()
     $PublishingPage.Layout = $NewPageLayout
     $PublishingPage.ListItem.Update();
     $PublishingPage.CheckIn("Page layout Updated via PowerShell")
     $PublishingPage.ListItem.File.Publish("Published via PowerShell")
     if ($PublishingPage.ListItem.ParentList.EnableModeration)
     {
         $PublishingPage.ListItem.File.Approve("Publishing Page Layout Updated!");
     }
   
     write-host "Updated Page layout on: "$PublishingPage.url -ForegroundColor Green
 }