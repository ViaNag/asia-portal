# =================================================================================
#
#Main Function
#
# =================================================================================
function createSendTo([string]$ConfigPath = "")
{

	$rowcounter = 1
    $ContentOrganizerStatus = $false
    $sendtoOverwrite = $false
    $sendtoShow = $false
    $sendtoCSV = $null
    $sendtoName = $null
    $sendtoURL = $null
    $sendtoAction = $null
    $sendtoExplanation = $null
	$activateContentOrganizerFeature=$false
	
	$sendtoCSV = Import-Csv $ConfigPath

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file"
	
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
			  #process each row of CSV file
        foreach ($row in $sendtoCSV)
		{
            #get contents of csv file
        
            $sendtoName = $row.Name
            $sendtoURL = $row.TargetURL
			$activateContentOrganizerFeature = $row.ActivateContentOrganizerFeature

            #verify $sendtoURL is a valid site and the Content Organizer feature is activated
            $exists = (Get-SPWeb $sendtoURL -ErrorAction SilentlyContinue) -ne $null
            if ($exists)
			{ #web exists, verify content organizer features is activated
                $ContentOrganizer = Get-SPFeature -identity documentrouting -web $sendtoURL -ErrorAction SilentlyContinue
                if ($ContentOrganizer -eq $null)
				{
                    Write-Host "The Content Organizer feature is not activated in the site specified by the TargetURL: $($sendtoURL)." -ForegroundColor Red
					Write-Output "The Content Organizer feature is not activated in the site specified by the TargetURL: $($sendtoURL)."

                    if ($activateContentOrganizerFeature)
					{
                        Enable-SPFeature -identity DocumentRouting -URL $sendtoURL
                        Write-Host "Content Organizer feature enabled at $($sendtoURL)" -ForegroundColor Green
						Write-Output "Content Organizer feature enabled at $($sendtoURL)"
                        $ContentOrganizerStatus = $true
                    } 
					else 
					{
                        Write-Host "Error: Content Organizer feature not enabled at $($sendtoURL) - skipping this connection" -ForegroundColor Red
						Write-Output "Error: Content Organizer feature not enabled at $($sendtoURL) - skipping this connection" 
                    }

                } 
				else
				{
                    $ContentOrganizerStatus = $true
                }
            } 
			else 
			{
                Write-Host "Error: The TargetURL provided, $($sendtoURL), is not a valid SharePoint web." -ForegroundColor Red
				Write-Output "Error: The TargetURL provided, $($sendtoURL), is not a valid SharePoint web."
            }

            #proceeding only if TargetURL is valid, and Content Organizer is activated in that site
            if($ContentOrganizerStatus){
                if($row.ShowOnSendToMenu -eq "TRUE")
				{
                    $sendtoShow = $true
                } 
				else 
				{
                    $sendtoShow = $false
                }
                if($row.Action -eq "Copy")
				{
                    $sendtoAction = "Copy"
                } 
				elseif ($row.Action -eq "Move")
				{
                    $sendtoAction = "Move"
                } 
				elseif ($row.Action -eq "Move and Leave a Link")
				{
                    $sendtoAction = "Link"
                }

                $sendtoExplanation = $row.Explanation
                $sendtoWebApp = $row.WebApp

                if($row.OverwriteExistingConnection -eq "TRUE")
				{
                    $sendtoOverwrite = $true
                } 
				else 
				{
                    $sendtoOverwrite = $false
                }

                $result = CreateSendToConnection $sendtoName $sendtoURL $sendtoShow $sendtoAction $sendtoExplanation $sendtoWebApp $sendtoOverwrite

                if ($result["status"] -eq "OK")
				{
                    Write-Host "The Send To Connection, $($sendtoName), was successfully created in web app: $($sendtoWebApp)" -ForegroundColor Green
					Write-Output "The Send To Connection, $($sendtoName), was successfully created in web app: $($sendtoWebApp)"
                } 
				else
				{
                    Write-Host "Error: " $result["status"]  -ForegroundColor Red
					Write-Output "Error: " $result["status"] 
                }
            }
            $rowcounter += 1
        }
				
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host "Error: " $Error -ForegroundColor Red
        Write-Output "Error: " $Error
	}
}


# =================================================================================
#
# FUNC: CreateSendToConnection
# DESC: To create sendtoconnection
#
# =================================================================================

function CreateSendToConnection($Name, $URL, $Show, $Action, $Explanation, $WebAppURL, $Overwrite){
    $response = @{}
    $response["result"] = $null
    $response["status"] = $null

    if($Name -eq $null -or $URL -eq $null -or $Show -eq $null -or $Action -eq $null -or $WebAppURL -eq $null){
        $response["status"] = "A required field is missing for the Send To Connection"
        return $response
    }

    $tempfilehost = $null
    
    $fullURL = $URL + "/_vti_bin/officialfile.asmx"
    Write-Host "Constructing complete Send To URL: $fullURL" -ForegroundColor Cyan
	Write-Output "Constructing complete Send To URL: $fullURL"
    #create new rules
    $site = Get-SPSite $URL
    #Get Web Application
    $WebApp = Get-SPWebApplication $WebAppURL

    #Check if Send To Connection already exists. If Overwrite is $true then delete the existing connection and create a new one.
    $tempfilehost = $WebApp.OfficialFileHosts | ? {$_.OfficialFileName -eq $Name}

    if ($tempfilehost -eq $null)
	{ #Send To Connection does already not exist, create new connection
        Write-Host "Creating new Send To Connection: $($Name)" "Cyan"
		Write-Output "Creating new Send To Connection: $($Name)"
        $SPOfficialFileHost = New-Object Microsoft.SharePoint.SPOfficialFileHost
        $SPOfficialFileHost.OfficialFileName = $Name
        Write-Host "Setting name of connection:"
        Write-Host $SPOfficialFileHost.OfficialFileName
		Write-Output "Setting name of connection:"
        Write-Output $SPOfficialFileHost.OfficialFileName

        $SPOfficialFileHost.OfficialFileUrl = $fullURL
        Write-Host "Setting URL of connection:"
        Write-Host $SPOfficialFileHost.OfficialFileUrl
		Write-Output "Setting URL of connection:"
        Write-Output $SPOfficialFileHost.OfficialFileUrl

        $SPOfficialFileHost.ShowOnSendToMenu = $Show
        Write-Host "Setting Show on menu value of connection:"
        Write-Host $SPOfficialFileHost.ShowOnSendToMenu
		Write-Output "Setting Show on menu value of connection:"
        Write-Output $SPOfficialFileHost.ShowOnSendToMenu

        $SPOfficialFileHost.Action = [Microsoft.SharePoint.SpOfficialFileAction]::$Action
        Write-Host "Setting Action of connection:"
        Write-Host $SPOfficialFileHost.Action
		Write-Output "Setting Action of connection:"
        Write-Output $SPOfficialFileHost.Action

        $SPOfficialFileHost.Explanation = $Explanation
        $WebApp.OfficialFileHosts.Add($SPOfficialFileHost)
        
        $WebApp.Update()
        $response["result"] = $WebApp.OfficialFileHosts | ? {$_.OfficialFileName -eq $Name} #verify that newly created rule exists

        #Write-Host $createstatus
        if($response["result"] -ne $null)
		{
            #$webapp.Update
            $response["status"] = "OK"
            return $response
        } 
		else 
		{
            $response["status"] = "There was an error creating connection: $($Name)"
            return $response
        }
	} 
	else 
	{ 		
		#Send To Connection exists, delete/create it only if $Overwrite is $true
		if($Overwrite)
		{
			Write-Host "Send To Connection, $($Name), already exists and overwrite is set to TRUE." -ForegroundColor Cyan
			Write-Host "Preparing to delete existing Send To Connection: $($Name)" -ForegroundColor Cyan
			Write-Output "Send To Connection, $($Name), already exists and overwrite is set to TRUE."
			Write-Output "Preparing to delete existing Send To Connection: $($Name)"
		
			$deleteconnection = $WebApp.OfficialFileHosts.Remove($tempfilehost)
			if($deleteconnection)
			{
				Write-Host "Deleted existing Send To Connection, $($Name), successfully." -ForegroundColor Green
				Write-Output "Deleted existing Send To Connection, $($Name), successfully."
			}
			$WebApp.Update()
			$SPOfficialFileHost = New-Object Microsoft.SharePoint.SPOfficialFileHost
			if ($SPOfficialFileHost -ne $null){
				$SPOfficialFileHost.OfficialFileName = $Name
				Write-Host "Setting name of connection:"
				Write-Host $SPOfficialFileHost.OfficialFileName
				Write-Output "Setting name of connection:"
				Write-Output $SPOfficialFileHost.OfficialFileName

				$SPOfficialFileHost.OfficialFileUrl = $fullURL
				Write-Host "Setting URL of connection:"
				Write-Host $SPOfficialFileHost.OfficialFileUrl
				Write-Output "Setting URL of connection:"
				Write-Output $SPOfficialFileHost.OfficialFileUrl
			
				$SPOfficialFileHost.ShowOnSendToMenu = $Show
				Write-Host "Setting Show on menu value of connection:"
				Write-Host $SPOfficialFileHost.ShowOnSendToMenu
				Write-Output "Setting Show on menu value of connection:"
				Write-Output $SPOfficialFileHost.ShowOnSendToMenu

				$SPOfficialFileHost.Action = [Microsoft.SharePoint.SpOfficialFileAction]::$Action
				Write-Host "Setting Action of connection:"
				Write-Host $SPOfficialFileHost.Action
				Write-Output "Setting Action of connection:"
				Write-Output $SPOfficialFileHost.Action

				$SPOfficialFileHost.Explanation = $Explanation
				$testvalue = $WebApp.OfficialFileHosts.Add($SPOfficialFileHost)
				$WebApp.Update()

				$response["result"] = $WebApp.OfficialFileHosts | ? {$_.OfficialFileName -eq $Name} #verify that newly created rule exists
				if ($response["result"] -ne $null){
					$response["status"] = "OK"
					return $response
				} else {
					$response["status"] = "There was a problem overwriting the Send To Connection: $($Name)"
					return $response
				}
			} 
			else 
			{
				$response["status"] = "There was a problem creating the new Send To Connection: $($Name)"
			}
		}			
		else 
		{ 	
			#connection exists and $Overwrite is $false
			#$webapp.Update()
			Write-Host "Error: Send To Connection,  $($Name), already exists and overwrite is set to FALSE - skipping." -ForegroundColor Red
			$response["status"] = "Error: Send To Connection,  $($Name), already exists and overwrite is set to FALSE - skipping."
			return $response
		}
    }
}


# =================================================================================
#
# FUNC: deleteSendTo
# DESC: Verifies that deletion file exists, reads in contents
#
# =================================================================================
function deleteSendTo([string]$ConfigPath = ""){
	$sendtoDelCSV = Import-CSV $ConfigPath
    if( $? -eq $true ) 
    {
	   Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
	   Write-Output "Sucessfully read config file $ConfigPath file"
	
	    #file exists - read in contents and delete send to connections
        foreach ($row in $sendtoDelCSV){
            $deleteName = $sendtoDelCSV.Name
            $deleteWebApp = $sendtoDelCSV.WebApp

            $result = deleteSendToConnection $deleteName $deleteWebApp
            
            if($result["status"] -eq "OK"){
                Write-Host "The Send To Connection, $($deleteName), was successfully removed from the web app: $($deletewebapp)" -ForegroundColor Green
				Write-Output "The Send To Connection, $($deleteName), was successfully removed from the web app: $($deletewebapp)"
            } else {
                Write-Host "Error: " $result["status"] -ForegroundColor Red
				Write-Output "Error: " $result["status"]
                
            }
        }
    } 
	else 
	{
       	Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..."
    }
}

# =================================================================================
#
# FUNC: deleteSentToConnection
# DESC: Delete Send To Connection from a given web application
# $SendToConnectionName - text string matching the name of the Send To Connection to delete
# $SendToConnectionWebApp - text string for the web app URL
#
# =================================================================================
function deleteSendToConnection($SendToConnectionName, $SendToConnectionWebApp){
    $response = @{}
    $response["result"] = $null
    $response["status"] = $null

    #get web application object
    $WebAppObject = Get-SPWebApplication $SendToConnectionWebApp

    #Verify that the send to connection exists in the specified web app
    $response["result"] = $WebAppObject.OfficialFileHosts | ? {$_.OfficialFileName -eq $SendToConnectionName}

    if ($WebAppObject -eq $null) {#connection exists - attempt to delete
        $response["status"] = "Web Application, $SendToConnectionWebApp,  does not exist. Make sure it is specified correctly in $global:sendtoconnectionsdel, and try again."
        return $response
    }

    if ($response["result"] -eq $null){#connection does not exist in specified web app
        $response["status"] = "The Send To Connection, $SendToConnectionName, does not exist in the specified web app: $SendToConnectionWebApp."
        return $response
    }
    
    #WebApp and Send To Connection both exist, delete
    Write-Host "Send To Connection found - preparing to delete." -ForegroundColor Cyan
	Write-Output "Send To Connection found - preparing to delete."
    $deleteStatus = $WebAppObject.OfficialFileHosts.Remove($response["result"])
    $WebAppObject.Update()
    if ($deleteStatus){
        $response["status"] = "OK"
        return $response 
    } else {
        $response["status"] = "There was a problem removing the Send To Connection, $SendToConnectionName, from the web app: $SendToConnectionWebApp."
        return $response
    }
}
