﻿# =================================================================================
#
#Main Function to create Left Navigation
#
# =================================================================================
function AddIndexColumns([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..."
	}
	
	Write-Host "Successfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Successfully read config file $ConfigPath file`n"
	if($Error.Count -eq 0)
	{
        try
        {
            $error.Clear()
            foreach($col in $cfg.IndexColumns.IndexColumn)
            {
                $web=Get-SPWeb $col.SiteURL
                $list=$web.Lists[$col.ListName]
                $fldToIndex =$list.Fields.GetFieldByInternalName($col.FieldInternalName); 
                $fldToIndex.Indexed = $true;     
                $list.FieldIndexes.Add($fldToIndex); 

				Write-Host "$($col.FieldInternalName): field  indexed Successfully." -ForegroundColor Green
				Write-Output "$($col.FieldInternalName): field indexed Successfully." 

				$web.Update()
                $web.Dispose()    
            }
        }
        catch
        {
            Write-Host "`nException in AddIndexColumns method :" $Error -ForegroundColor Red
            Write-Output "`nException in AddIndexColumns method :" $Error
        }
	}
	else
	{
		Write-Host $Error -ForegroundColor Red
        Write-Output $Error
	}
}