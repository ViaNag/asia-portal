
if ((Get-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue) -eq $null) 
{
    Add-PSSnapin "Microsoft.SharePoint.PowerShell"
}
# =================================================================================
#
#Main Function to set masterpage
#
# =================================================================================
function SetMasterPage([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
	if($Error.Count -eq 0)
	{
		foreach($siteCollectionConfiguration in $cfg.Sites.Site)
		{
            if(![string]::IsNullOrEmpty($siteCollectionConfiguration.SiteURL))
            {
			    $site = Get-SPSite $siteCollectionConfiguration.SiteURL
				if(![string]::IsNullOrEmpty($siteCollectionConfiguration.MasterPageUrl))
				{
					foreach($web in $site.AllWebs) 
					{      
						$web.AllowUnsafeUpdates = $true 
					
						if ($web.IsRootWeb) 
						{                  
							Write-Host "Setting Master Page for " $siteCollectionConfiguration.Title "(" $siteCollectionConfiguration.Path ")." -ForegroundColor Green
							$rootweb = $site.RootWeb 
							if($web.ServerRelativeUrl -eq "/")
							{
								$rootweb.CustomMasterUrl = "/_catalogs/masterpage/"+$siteCollectionConfiguration.MasterPageUrl
								$rootweb.MasterUrl = "/_catalogs/masterpage/"+$siteCollectionConfiguration.MasterPageUrl
							}
							else{
							   $rootweb.CustomMasterUrl = $web.ServerRelativeUrl +"/_catalogs/masterpage/"+$siteCollectionConfiguration.MasterPageUrl
							   $rootweb.MasterUrl = $web.ServerRelativeUrl +"/_catalogs/masterpage/"+$siteCollectionConfiguration.MasterPageUrl 
							}
							if($Error.Count -eq 0)
							{
								Write-Host "Success: Master Page for Site Collection:" $siteCollectionConfiguration.Title "(" $siteCollectionConfiguration.Path ")." -ForegroundColor Green
							}
							else
							{
								Write-Host "Error: Master Page for Site Collection:" $siteCollectionConfiguration.Title "(" $siteCollectionConfiguration.Path "). Exiting script..." -foregroundcolor Red
								Write-Host $Error -ForegroundColor Red  
								$error.clear()
							}
							$rootweb.Update()
						} 
						else 
						{ 
							$web.CustomMasterUrl=$site.RootWeb.CustomMasterUrl 
							$web.MasterUrl = $site.RootWeb.MasterUrl 
						}
						if(![string]::IsNullOrEmpty( $siteCollectionConfiguration.SiteLogoUrl))
						{
							if ($web.ServerRelativeUrl -eq "/") 
							{
								$web.SiteLogoUrl = "/"+$siteCollectionConfiguration.SiteLogoUrl
							}
							else
							{
								$web.SiteLogoUrl = $web.ServerRelativeUrl +"/"+$siteCollectionConfiguration.SiteLogoUrl 
							}
						}
						$web.Update();             
						$web.AllowUnsafeUpdates = $false 
						$web.Dispose(); 
					}
				}
            }
		}
	}
}
