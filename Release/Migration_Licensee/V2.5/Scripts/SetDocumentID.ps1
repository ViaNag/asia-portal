# =================================================================================
#
# FUNC: SetDocumentId
# DESC: funtion to set document id at site collection level.
#
# =================================================================================
function SetDocumentId([object] $ConfigPath)
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file"
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
            $siteCollectionConfig=$cfg.SelectNodes("/Webapplication/Site") | Where-Object {$_.IsSiteCollection -eq "True"}
            foreach($site in $siteCollectionConfig)
            {
                if(![string]::IsNullOrEmpty($site.DocumentIdPrefix))
                {
                    $docSite=Get-SPSite $site.SiteURL
                    if($docSite)
                    {
                        [Microsoft.Office.DocumentManagement.DocumentId]::SetDefaultProvider($docSite)
                        [Microsoft.Office.DocumentManagement.DocumentId]::EnableAssignment($docSite,$site.DocumentIdPrefix,$true,$true,$true,$false)
                        $docSite.Dispose()
                        Write-Host "`nDocument Id updated for web :$($site.SiteURL)" -ForegroundColor Green
                        Write-Output "`nDocument Id updated for web :$($site.SiteURL)"
                    }
                    else
                    {
                        Write-Host "`nWeb not found at url :" $site.SiteURL -ForegroundColor Yellow
                        Write-Output "`nWeb not found at url : $($site.SiteURL)"
                    }
                }
                else
                {
                    Write-Host "`nDocument id not given for site :" $site.SiteURL -ForegroundColor Yellow
                    Write-Output "`nDocument id not given for site : $($site.SiteURL)"
                }
            }
        }
        catch
        {
            Write-Host "`nException in setting document id at url $($site.SiteURL) :" $Error -ForegroundColor Red
            Write-Output "`nException in setting document id at url $($site.SiteURL) :" $Error
        }
    }
}