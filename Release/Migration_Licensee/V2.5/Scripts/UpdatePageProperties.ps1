﻿# ===================================================================================
# FUNC: UpdatePageLayout
# DESC: Update Page layouts
# ===================================================================================
 function UpdatePageMetadata([String]$ConfigFileName = "")
 {
     if (-not $(Test-Path -Path $configFileName -Type Leaf))
	{
		Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
	}

	$configXml = [xml]$(get-content $configFileName)
	
	if( $? -eq $false ) 
	{
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..." | Out-Null
	}

	foreach($Page in $configXml.Configuration.Pages.Page)     
	{
		UpdatePageProperties $configXml.Configuration.PublishingSite.URL $Page.PropertyName $Page.Name
	}
}
function UpdatePageProperties([string]$WebURL, [string]$PropertyName,[string]$PublishingPageName)
{
     $Web = Get-SPWeb $WebURL
      
     #Get Publishing Site and Web
     $PublishingSite = New-Object Microsoft.SharePoint.Publishing.PublishingSite($Web.Site)
     $PublishingWeb = [Microsoft.SharePoint.Publishing.PublishingWeb]::GetPublishingWeb($web)
   
     #Get Pages Library
     $publishingPages = $PublishingWeb.GetPublishingPages()
     $PublishingPage = $PublishingWeb.GetPublishingPage($WebURL + "/Pages/" + $PublishingPageName) 
     $file = $web.GetFile($PublishingPage.Uri.ToString())

     [Microsoft.SharePoint.SPListItem]$spListItem = $file.Item
     
    $file.CheckOut()
	 $items = $PropertyName.split(",")
	 foreach($item in $items)
	 {
		$spListItem[$item] = $null
	 }
	 $spListItem.Update()
     $file.CheckIn("Properties updated")

     $file.Publish("Publish via powershell!")
   
     write-host "Updated Page properties on: "$PublishingPage.url -ForegroundColor Green
 }