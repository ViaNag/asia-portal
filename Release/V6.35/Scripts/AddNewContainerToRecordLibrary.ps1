
# ===================================================================================
# FUNC: AddNewContainerToRecordLibrary
# DESC: Create new contaioner to record library
# ===================================================================================
function AddNewContainerToRecordLibrary([String]$ConfigFileName = "")
{

	# Check that the config file exists.
	if (-not $(Test-Path -Path $configFileName -Type Leaf))
	{
		Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
	}

	$configXml = [xml]$(get-content $configFileName)
	if( $? -eq $false ) 
	{
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
        Write-Output "Could not read config file. Exiting ..."
	}

	if ($configXml.Items)
	{
		foreach ($Item in $configXml.Items.Item)
		{
			try
			{
				write-host "started adding items List-" $Item.ListName -ForegroundColor Green

				if($Item.ListName)
				{
					# Break role inheritance of the list
					$web = Get-SPWeb $Item.WebUrl
					
					$ListToAddItem = $web.Lists[$Item.ListName]
					$selectedContentType= $ListToAddItem.ContentTypes[$Item.ContentTypeName];
					if($ListToAddItem)
					{
                        #Create a new item
                        $newItem = $ListToAddItem.Items.Add("", [Microsoft.SharePoint.SPFileSystemObjectType]::Folder,$Item.FolderName)

						if($selectedContentType)
						{
							$newitem["ContentTypeId"] = $selectedContentType.Id;
						}
                        foreach ($ItemColumns in $Item.Columns)
		                {
							if(![string]::IsNullOrEmpty( $ItemColumns.Value))
							{
								write-host "started adding column to item-" $ItemColumns.Name -ForegroundColor Green
								write-output "started adding column to item- $($ItemColumns.Name)"
								$newItem[$ItemColumns.Name] = $ItemColumns.Value
							}
                        }
                        $newItem.Update()
					}
					else
					{
						write-host "List-" $Item.ListName ", not found on " $web.Title -ForegroundColor Green
                        write-output "List- $($Item.ListName), not found on $($web.Title)"
					}
				}
				if($Error.Count -gt 0)
				{
					Write-Host "Error adding items. Cause : " $Error -ForegroundColor Red
                    Write-Output "Error adding items. Cause : "$Error
					$Error.Clear()	
				}
				else
				{
					Write-Host "Process Completed." -ForegroundColor Green
                    Write-output "Process Completed."
				}
			}
			catch
			{
				Write-Host "Exception while adding items." $Error -ForegroundColor Red
                Write-output "Exception while adding items." $Error
			}
		}
	}
}