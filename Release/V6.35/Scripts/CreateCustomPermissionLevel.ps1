﻿# =================================================================================
#
#Main Function to apply security
#
# =================================================================================
function CreateCustomPermissionLevels([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..."  -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file`n"
	if($Error.Count -eq 0)
	{
        try 
        {
            $error.clear()
            foreach($PermissionLevel in $cfg.PermissionLevels.PermissionLevel)
            {
                CreateCustomPermissionLevel $PermissionLevel
            }
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host $Error -ForegroundColor "Red"
        Write-Output $Error
	}
}

# =================================================================================
#
# FUNC: CreateCustomPermissionLevel
# DESC: Create custom permission level
# =================================================================================
function CreateCustomPermissionLevel([object] $PermissionLevel)
{
    try
    {
        $error.Clear()
        $site = Get-SPSite $PermissionLevel.SiteURL
        $spWeb=$site.RootWeb
        $spRoleDefinition =$spWeb.RoleDefinitions[$PermissionLevel.Name]
        if($spRoleDefinition -eq $null)
        {
            $spRoleDefinition = New-Object Microsoft.SharePoint.SPRoleDefinition
            $spRoleDefinition.Name = $PermissionLevel.Name
            $spRoleDefinition.Description = $PermissionLevel.Description
            # .Type is a ReadOnly property, hence it'll remain on "None".
 
            # Use the command [System.Enum]::GetNames("Microsoft.SharePoint.SPBasePermissions") to get a list of possible BasePermission values
            # For this Permission Level, we'll add four base permissions:
            $spRoleDefinition.BasePermissions = $PermissionLevel.BasePermissions
            $spWeb.RoleDefinitions.Add($spRoleDefinition) 
            Write-Host "Permission level $($PermissionLevel.Name) added successfully." -ForegroundColor Green
            Write-Output "Permission level $($PermissionLevel.Name) added successfully."
        }
        else
        {
            $spRoleDefinition.BasePermissions = $PermissionLevel.BasePermissions
            $spRoleDefinition.Update()  
            Write-Host "Permission level $($PermissionLevel.Name) updated successfully." -ForegroundColor Green
            Write-Output "Permission level $($PermissionLevel.Name) updated successfully."
        }
 
        #Display the properties for our new Permission level
        #$spWeb.RoleDefinitions[$PermissionLevel.Name] | Out-Host
 
        $spWeb.Dispose()
        $site.Dispose()
        
 	}
    catch
    {
        Write-Host "`nException to create permission level : $($PermissionLevel.Name)" $Error -ForegroundColor Red
        Write-Output "`nException to create permission level : $($PermissionLevel.Name)`n" $Error
    }
}