﻿# =================================================================================
#
#Main function to create action
#
# =================================================================================

Add-PSSnapin "Microsoft.SharePoint.Powershell"

function CreateCustomActions([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file"
	if($Error.Count -eq 0)
	{
        try
        {
            foreach($oCustomXml in $cfg.Sites.Site)
            {
                 $error.clear()
                 $actionScope=$oCustomXml.Scope
                 $oSiteUrl=$oCustomXml.Url
                 $oActionDest=$null
                 if($actionScope -eq "Site")
                 {
                    $oActionDest=Get-SPSite $oSiteUrl
                 }
                 elseif($actionScope -eq "Web")
                 {
                    $oActionDest=Get-SPWeb $oSiteUrl
                 }
                 elseif($actionScope -eq "List")
                 {
                    $oWeb=Get-SPWeb $oSiteUrl
                    $oActionDest=$oWeb.Lists[$oCustomXml.List]
                 }
                 if($oActionDest)
                 {
                    CreateCustomAction $oCustomXml $oActionDest
                 }
            }
        }
        catch
        {
            Write-Output "`nException :" $Error
            Write-Host "`nException :" $Error -ForegroundColor Red
        }
	}
	else
	{
		Write-Host $Error -ForegroundColor Red
        Write-Output $Error
	}
}

# =================================================================================
#
#Function to create custom action at various scope levels
#
# =================================================================================
function CreateCustomAction([object]$oCustomXml,$oScope)
{
    try
        {
            $existingAction=$oScope.UserCustomActions | where {$_.Title -eq $oCustomXml.Title}
            if($existingAction)
            {
                if($existingAction.Count -eq 1)
                {
                    $existingAction.Delete()
                    Write-Host "`nExisting Custom Action $($oCustomXml.Title) deleted successfully" -ForegroundColor Yellow
                    Write-Output "Existing Custom Action $($oCustomXml.Title) deleted successfully)"
                }
            }
            $action = $oScope.UserCustomActions.Add();
            if(![string]::IsNullOrEmpty($oCustomXml.RegistrationType))
            {
                $action.RegistrationType = $oCustomXml.RegistrationType;
            }
            if(![string]::IsNullOrEmpty($oCustomXml.RegistrationId))
            {
                $action.RegistrationId = $oCustomXml.RegistrationId;
            }
            if(![string]::IsNullOrEmpty($oCustomXml.Location))
            {
                $action.Location = $oCustomXml.Location;
            }
            if(![string]::IsNullOrEmpty($oCustomXml.Title))
            {
                $action.Title = $oCustomXml.Title;
            }
            if(![string]::IsNullOrEmpty($oCustomXml.Sequence))
            {
                $action.Sequence = $oCustomXml.Sequence;
            }
            if(![string]::IsNullOrEmpty($oCustomXml.Rights))
            {
                $action.Rights = $oCustomXml.Rights;
            }
            if(![string]::IsNullOrEmpty($oCustomXml.CommanUIExtensionFileUrl))
            {
				$commandUIExtensionFileUrl=[System.Web.HttpUtility]::HtmlDecode($oCustomXml.CommanUIExtensionFileUrl)
                $action.CommandUIExtension=[string]$commandUIExtensionFileUrl.Replace("<![CDATA[",[string]::Empty).Replace("]]>",[string]::Empty)
            }
            if(![string]::IsNullOrEmpty($oCustomXml.ScriptSrc))
            {
                $action.ScriptSrc = $oCustomXml.ScriptSrc;
            }

            if(![string]::IsNullOrEmpty($oCustomXml.UrlAction))
            {
               $action.Url=$oCustomXml.UrlAction
            }
             
            $action.Update();
            if($oCustomXml.Scope -ne "List")
            {
                $oScope.Dispose();
            }
            Write-Host "`nCustom Action created $($oCustomXml.Title)" -ForegroundColor Yellow
            Write-Output "Custom Action created $($oCustomXml.Title)"
            
        }
    catch
        {
            Write-Output "`nException :" $Error
            Write-Host "`nException :" $Error -ForegroundColor Red
        }

}

function DeleteCustomAction([object]$oCustomXml,$oScope)
{
    try
        {
            $existingAction=$oScope.UserCustomActions | where {$_.Title -eq $oCustomXml.Title}
            if($existingAction)
            {
                if($existingAction.Count -eq 1)
                {
                    $existingAction.Delete()
                    Write-Host "`Custom Action $($oCustomXml.Title) deleted successfully" -ForegroundColor Green
                    Write-Output "Custom Action $($oCustomXml.Title) deleted successfully)"
                }
            }
        }
    catch
        {
            Write-Output "`nException :" $Error
            Write-Host "`nException :" $Error -ForegroundColor Red
        }
}