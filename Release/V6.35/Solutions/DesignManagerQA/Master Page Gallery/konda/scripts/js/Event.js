var eventTagging = {
    clientcontext: null,
    web: null,
    oList: null,
    camlQuery: null,
    choiceField: null,
    businessclientcontext: null,
    oBusinessList: null,
    businessWeb: null,
    currentUser: null,
    currentUserEmail: null,
    EventTitle: null,
    EventDesc: null,
    EventLocation: null,
    EventStartDate: null,
    EventEndDate: null,
    EventOutlookStartDate: null,
    EventOutlookEndDate: null,
    startDate: null,
    endDate: null,
    localEventContact: null,
    businessEventContact: null,
    eventContactDisplayName: null,
    calCategory: null,

    displayCalendarEvents: function () {
        var subsiteName = (((document.URL.split(_spPageContextInfo.siteAbsoluteUrl))[1]).split('/'))[1];
        eventTagging.clientContext = new SP.ClientContext.get_current();
        eventTagging.web = eventTagging.clientContext.get_web();
        eventTagging.oList = eventTagging.web.get_lists().getByTitle('cal');
        var grSiteUrl = _spPageContextInfo.siteAbsoluteUrl.toLowerCase().replace(_spPageContextInfo.siteServerRelativeUrl.toLowerCase(), "");
        eventTagging.businessClientContext = new SP.ClientContext(grSiteUrl + "/Company");
        //eventTagging.businessClientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl+"/Company");
        eventTagging.businessWeb = eventTagging.businessClientContext.get_web();
        eventTagging.oBusinessList = eventTagging.businessWeb.get_lists().getByTitle('Business Calendar');
        eventTagging.clientContext.load(eventTagging.oList);
        eventTagging.businessClientContext.load(eventTagging.oBusinessList);
        var field = eventTagging.oBusinessList.get_fields().getByInternalNameOrTitle('Category');
        eventTagging.choiceField = eventTagging.businessClientContext.castTo(field, SP.FieldChoice);
        eventTagging.businessClientContext.load(field);
        eventTagging.camlQuery = new SP.CamlQuery();
        if (_spPageContextInfo.webServerRelativeUrl == "/") {
            eventTagging.camlQuery.set_viewXml('<View><ViewFields><FieldRef Name="fAllDayEvent"/><FieldRef Name="HideAddToOutlook"/><FieldRef Name="PublishingContact"/><FieldRef Name="EventDate"/><FieldRef Name="EndDate"/><FieldRef Name="Location"/><FieldRef Name="Title"/><FieldRef Name="Attire"/><FieldRef Name="Honorees"/><FieldRef Name="Description"/><FieldRef Name="Category"/></ViewFields><Query><Where><Eq><FieldRef Name="TaggedUrl" /><Value Type="Text">' + _spPageContextInfo.webServerRelativeUrl + 'Pages/' + articleTagging.pageName + '</Value></Eq></Where></Query><RowLimit>1</RowLimit></View>');
        }
        else {
            eventTagging.camlQuery.set_viewXml('<View><ViewFields><FieldRef Name="fAllDayEvent"/><FieldRef Name="HideAddToOutlook"/><FieldRef Name="PublishingContact"/><FieldRef Name="EventDate"/><FieldRef Name="EndDate"/><FieldRef Name="Location"/><FieldRef Name="Title"/><FieldRef Name="Attire"/><FieldRef Name="Honorees"/><FieldRef Name="Description"/><FieldRef Name="Category"/></ViewFields><Query><Where><Eq><FieldRef Name="TaggedUrl" /><Value Type="Text">' + _spPageContextInfo.webServerRelativeUrl + '/Pages/' + articleTagging.pageName + '</Value></Eq></Where></Query><RowLimit>1</RowLimit></View>');
        }

        eventCollListItem = eventTagging.oList.getItems(eventTagging.camlQuery);
        eventTagging.clientContext.load(eventCollListItem);
        eventTagging.businessClientContext.executeQueryAsync(eventTagging.successBusinessCalendarCategories, eventTagging.failure);
        eventTagging.clientContext.executeQueryAsync(eventTagging.successDisplayCalendarEvents, eventTagging.failure);
    },

    successBusinessCalendarCategories: function () {
        eventTagging.populateDropDown();
        if (eventTagging.calCategory != undefined && eventTagging.calCategory != null) {
            $('#drpdwnCategory').val(eventTagging.calCategory);// written here because the populateDropDown function was overriding this statement	
        }
    },

    successDisplayCalendarEvents: function () {
        var evnetlistItemEnumerator = eventCollListItem.getEnumerator();
        while (evnetlistItemEnumerator.moveNext()) {

            var oEventListItem = evnetlistItemEnumerator.get_current();
            var calStartDate = oEventListItem.get_item('EventDate');
            var calFormattedStartDate = calStartDate.format("dd MMMM yyyy-H-mm");
            var formattedDate = calFormattedStartDate.split("-");
            var formattedTitleStartDate = formattedDate[0];
            var formattedHour = formattedDate[1];
            //it is pm if hours from 12 onwards
            var startDateSuffex = (formattedHour >= 12) ? 'pm' : 'am';

            //only -12 from hours if it is greater than 12
            formattedHour = (formattedHour > 12) ? formattedHour - 12 : formattedHour;
            var formattedMins = formattedDate[2];
            var calEndDate = oEventListItem.get_item('EndDate');
            var formattedEndHour, formattedEndMins, endDateSuffex;
            if (calEndDate != null && calEndDate != undefined) {
                var calFormattedEndDate = calEndDate.format("dd MMMM yyyy-H-mm");
                var formattedEndDate = calFormattedEndDate.split("-");
                formattedEndHour = formattedEndDate[1];
                //it is pm if hours from 12 onwards
                endDateSuffex = (formattedEndHour >= 12) ? 'pm' : 'am';

                //only -12 from hours if it is greater than 12
                formattedEndHour = (formattedEndHour > 12) ? formattedEndHour - 12 : formattedEndHour;
                formattedEndMins = formattedEndDate[2];
            }
            var calLocation = oEventListItem.get_item('Location');
            var calTitle = oEventListItem.get_item('Title');
            var calAttire = oEventListItem.get_item('Attire');
            var calHonorees = oEventListItem.get_item('Honorees');
            var calDesc = oEventListItem.get_item('Description');
            eventTagging.calCategory = oEventListItem.get_item('Category');
            var eventContact = oEventListItem.get_item('PublishingContact');
            var hideAddToOutlook = oEventListItem.get_item('HideAddToOutlook');
            $("#hideAddToOutlook").prop("checked", hideAddToOutlook);
            var allDayEvent = oEventListItem.get_item('fAllDayEvent');
            $("#allDayEvent").prop("checked", allDayEvent);
            eventTagging.EventTitle = calTitle;
            eventTagging.EventDesc = calDesc;
            eventTagging.EventLocation = calLocation;
            eventTagging.EventStartDate = calStartDate;
            eventTagging.EventEndDate = calEndDate;
            if (hideAddToOutlook == true) {
                //$("#EventAddtoOutlook").hide();
                $(".outlook-box").hide();
            }
            else {
                //$("#EventAddtoOutlook").show();
                $(".outlook-box").show();
            }
            if (eventContact != null && eventContact != undefined && eventContact != '') {
                eventTagging.eventContactDisplayName = eventContact.$2e_1;
                // This property gives the display name of the event contact.
            }
            if (eventTagging.eventContactDisplayName != null && eventTagging.eventContactDisplayName != undefined && eventTagging.eventContactDisplayName != '') {
                $("#greenroom-article-event-contact").show();
                $("#EventContact").append(eventTagging.eventContactDisplayName);
            }
            if (calAttire != null && calAttire != '') {
                $('#txtAttire').val(calAttire);
            }
            if (calHonorees != null && calHonorees != '') //populating textbox with multiple line of text column type
            {
                var tmp = document.getElementById("txtHonorees");
                if (tmp != null && tmp != '') {
                    tmp.innerHTML = calHonorees;
                    calHonorees = tmp.textContent || tmp.innerText;
                    $('#txtHonorees').val(calHonorees);
                }
            }
            if (calDesc != null && calDesc != '') //populating textbox with multiple line of text column type
            {
                var tmpDesc = document.getElementById("txtDescription");
                if (tmpDesc != null && tmpDesc != '') {
                    tmpDesc.innerHTML = calDesc;
                    calDesc = tmpDesc.textContent || tmp.innerText;
                    $('#txtDescription').val(calDesc);
                }
            }
            $('#EventAddressDisp').append(calLocation);
            if (calLocation != undefined && calLocation != "undefined" && calLocation != null && calLocation != "") {
                $("#greenroom-article-event-loc").show();
            }
            if (calStartDate != undefined && calStartDate != "undefined" && calStartDate != null && calStartDate != "") {
                $("#greenroom-article-event-start-time").show();
            }

            var timeZone = "";
            if (calStartDate.toString().indexOf("(") > -1) {
                timeZone = calStartDate.toString().substr(calStartDate.toString().indexOf("("));
            }
            if (calEndDate == null || calEndDate == undefined || calEndDate == "") {
                $('#EventStartDateDisp').append(formattedHour + ":" + formattedMins + " " + startDateSuffex + " " + timeZone);
            }
            else {
                $('#EventStartDateDisp').append(formattedHour + ":" + formattedMins + " " + startDateSuffex + " - " + formattedEndHour + ":" + formattedEndMins + " " + endDateSuffex + " " + timeZone);
            }
            //$('#EventEndDate').append();
            $('#EventTitleDisp').append(formattedTitleStartDate);
            $('#EventDesc').append(calDesc);
        }
        if (eventTagging.calCategory != undefined && eventTagging.calCategory != null) {
            $('#drpdwnCategory').val(eventTagging.calCategory);// written here because the populateDropDown function was overriding this statement
        }
    },

    populateDropDown: function () {
        var distinctChoices = eventTagging.choiceField.get_choices();
        var select = document.getElementById("drpdwnCategory");
        if (select != null && select != undefined) {
            for (var i = 0; i < distinctChoices.length; i++) {
                var option = document.createElement('option');
                option.text = option.value = distinctChoices[i];
                select.add(option, 0);
            }
        }
    },

    pushToOutlook: function (type, showAlert, startDt, endDt) {
        var subsiteName = (((document.URL.split(_spPageContextInfo.siteAbsoluteUrl))[1]).split('/'))[1];
        //As per discussion with Thao Push to local also save in local cal at company
        /*  if(type=='LocalCalendar' && subsiteName!="company"){		
              eventTagging.clientContext = new SP.ClientContext.get_current();
              eventTagging.web = eventTagging.clientContext.get_web(); 
              eventTagging.oList= eventTagging.web.get_lists().getByTitle('cal');
          
          }
          else{
              eventTagging.clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl+"/Company");
              eventTagging.web = eventTagging.clientContext.get_web(); 
              eventTagging.oList= eventTagging.web.get_lists().getByTitle('Business Calendar');
          
          }*/
        eventTagging.clientContext = new SP.ClientContext.get_current();
        eventTagging.web = eventTagging.clientContext.get_web();
        eventTagging.oList = eventTagging.web.get_lists().getByTitle('cal');

        var EnsureUser = eventTagging.getEventContactLogin();
        if (EnsureUser != null && EnsureUser != undefined && EnsureUser != '') {
            eventTagging.localEventContact = eventTagging.web.ensureUser(EnsureUser);
        }
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml('<View><Query><Where><Eq><FieldRef Name="TaggedUrl" /><Value Type="Text">' + _spPageContextInfo.webServerRelativeUrl + '/Pages/' + articleTagging.pageName + '</Value></Eq></Where></Query><RowLimit>1</RowLimit></View>');
        collListItemOutlook = eventTagging.oList.getItems(camlQuery);
        eventTagging.clientContext.load(collListItemOutlook);
        if (eventTagging.localEventContact != null && eventTagging.localEventContact != undefined && eventTagging.localEventContact != "") {
            eventTagging.clientContext.load(eventTagging.localEventContact);
        }
        eventTagging.clientContext.executeQueryAsync(function () { eventTagging.successpushToOutlook(type, showAlert, startDt, endDt) }, eventTagging.failure);
    },

    successpushToOutlook: function (type, showAlert, startDt, endDt) {

        var location = $("input[title='Event Address']").val();
        var eventTitle = $("input[title='Event Title']").val();
        var category = $("select[title='category']").val();
        var description = $('#txtDescription').val();
        var attire = $('#txtAttire').val();
        var honorees = $('#txtHonorees').val();
        var allDayEvent = $("#allDayEvent").prop("checked");
        var hideAddToOutlook = $("#hideAddToOutlook").prop("checked");
        if (collListItemOutlook.get_count() > 0) {
            var listItemEnumerator = collListItemOutlook.getEnumerator();
            while (listItemEnumerator.moveNext()) {
                oListItem = listItemEnumerator.get_current();

                oListItem.set_item('EventDate', startDt);
                oListItem.set_item('EndDate', endDt);
                oListItem.set_item('Location', location);
                oListItem.set_item('Title', eventTitle);
                oListItem.set_item('Category0', category);
                oListItem.set_item('Description', description);
                if (eventTagging.localEventContact != null && eventTagging.localEventContact != undefined && eventTagging.localEventContact != "") {
                    oListItem.set_item('PublishingContact', eventTagging.localEventContact.get_id());
                }
                oListItem.set_item('Attire', attire);
                oListItem.set_item('Honorees', honorees);
                oListItem.set_item('fAllDayEvent', allDayEvent);
                oListItem.set_item('HideAddToOutlook', hideAddToOutlook);
                var taggedUrl = oListItem.get_item('TaggedUrl');
                if (_spPageContextInfo.webServerRelativeUrl == "/")
                    oListItem.set_item('TaggedUrl', _spPageContextInfo.webServerRelativeUrl + 'Pages/' + articleTagging.pageName);
                else
                    oListItem.set_item('TaggedUrl', _spPageContextInfo.webServerRelativeUrl + '/Pages/' + articleTagging.pageName);

                oListItem.update();
            }
        }
        else {
            var itemCreateInfo = new SP.ListItemCreationInformation();
            createItem = eventTagging.oList.addItem(itemCreateInfo);
            createItem.set_item('EventDate', startDt);
            createItem.set_item('EndDate', endDt);
            createItem.set_item('Location', location);
            createItem.set_item('Title', eventTitle);
            createItem.set_item('Description', description);
            if (eventTagging.localEventContact != null && eventTagging.localEventContact != undefined && eventTagging.localEventContact != "") {
                createItem.set_item('PublishingContact', eventTagging.localEventContact.get_id());
            }
            createItem.set_item('Attire', attire);
            createItem.set_item('Honorees', honorees);
            createItem.set_item('Category0', category);
            createItem.set_item('fAllDayEvent', allDayEvent);
            //changed oListItem to createItem
            createItem.set_item('HideAddToOutlook', hideAddToOutlook);
            createItem.set_item('TaggedUrl', _spPageContextInfo.webServerRelativeUrl + '/Pages/' + articleTagging.pageName);
            createItem.update();
        }
        eventTagging.clientContext.executeQueryAsync(function () { eventTagging.success(showAlert) }, eventTagging.failure);
    },

    success: function (showAlert) {
        if (showAlert == true) {
            alert('Values Updated!!');
        }
    },

    failure: function (sender, args) {
        //console.log(args.get_message() + '\n' + args.get_stackTrace());
    },
    pushToCompnayOutlook: function (startDt, endDt) {
        eventTagging.pushToOutlook('LocalCalendar', false, startDt, endDt);
        eventTagging.pushToBusinessOutlook('CompanyCalendar', true, startDt, endDt);
    },

    pushToBusinessOutlook: function (type, showAlert, startDt, endDt) {
		var grSiteUrl = _spPageContextInfo.siteAbsoluteUrl.toLowerCase().replace(_spPageContextInfo.siteServerRelativeUrl.toLowerCase(), "");
		eventTagging.businessclientcontext = new SP.ClientContext(grSiteUrl + "/Company");
        //eventTagging.businessclientcontext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl + "/Company");
        eventTagging.businessWeb = eventTagging.businessclientcontext.get_web();
        eventTagging.oBusinessList = eventTagging.businessWeb.get_lists().getByTitle('Business Calendar');
        var EnsureUser = eventTagging.getEventContactLogin();
        if (EnsureUser != null && EnsureUser != undefined && EnsureUser != '') {
            eventTagging.businessEventContact = eventTagging.businessWeb.ensureUser(EnsureUser);
        }
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml('<View><Query><Where><Eq><FieldRef Name="TaggedUrl" /><Value Type="Text">' + _spPageContextInfo.webServerRelativeUrl + '/Pages/' + articleTagging.pageName + '</Value></Eq></Where></Query><RowLimit>1</RowLimit></View>');
        collListItemBusinessOutlook = eventTagging.oBusinessList.getItems(camlQuery);
        eventTagging.businessclientcontext.load(collListItemBusinessOutlook);
        if (eventTagging.businessEventContact != null && eventTagging.businessEventContact != undefined && eventTagging.businessEventContact != "") {
            eventTagging.businessclientcontext.load(eventTagging.businessEventContact);
        }
        eventTagging.businessclientcontext.executeQueryAsync(function () { eventTagging.successpushToBusinessOutlook(type, showAlert, startDt, endDt) }, eventTagging.failure);
    },

    successpushToBusinessOutlook: function (type, showAlert, startDt, endDt) {
        var location = $("input[title='Event Address']").val();
        var eventTitle = $("input[title='Event Title']").val();
        var category = $("select[title='category']").val();
        var description = $('#txtDescription').val();
        var attire = $('#txtAttire').val();
        var honorees = $('#txtHonorees').val();
        var allDayEvent = $("#allDayEvent").prop("checked");
        if (collListItemBusinessOutlook.get_count() > 0) {
            var listItemBusinessEnumerator = collListItemBusinessOutlook.getEnumerator();
            while (listItemBusinessEnumerator.moveNext()) {
                oListItemBusiness = listItemBusinessEnumerator.get_current();

                oListItemBusiness.set_item('EventDate', startDt);
                oListItemBusiness.set_item('EndDate', endDt);
                oListItemBusiness.set_item('Location', location);
                oListItemBusiness.set_item('Title', eventTitle);
                oListItemBusiness.set_item('Category', category);
                oListItemBusiness.set_item('Description', description);
                if (eventTagging.businessEventContact != null && eventTagging.businessEventContact != undefined && eventTagging.businessEventContact != "") {
                    oListItemBusiness.set_item('EventContact', eventTagging.businessEventContact.get_id());
                }
                oListItemBusiness.set_item('Attire', attire);
                oListItemBusiness.set_item('Honorees', honorees);
                oListItemBusiness.set_item('fAllDayEvent', allDayEvent);
                var taggedUrl = oListItemBusiness.get_item('TaggedUrl');
                if (_spPageContextInfo.webServerRelativeUrl == "/")
                    oListItemBusiness.set_item('TaggedUrl', _spPageContextInfo.webServerRelativeUrl + 'Pages/' + articleTagging.pageName);
                else
                    oListItemBusiness.set_item('TaggedUrl', _spPageContextInfo.webServerRelativeUrl + '/Pages/' + articleTagging.pageName);

                oListItemBusiness.update();
            }
        }
        else {
            var businessItemCreateInfo = new SP.ListItemCreationInformation();
            createBusinessItem = eventTagging.oBusinessList.addItem(businessItemCreateInfo);
            createBusinessItem.set_item('EventDate', startDt);
            createBusinessItem.set_item('EndDate', endDt);
            createBusinessItem.set_item('Location', location);
            createBusinessItem.set_item('Title', eventTitle);
            createBusinessItem.set_item('Description', description);
            if (eventTagging.businessEventContact != null && eventTagging.businessEventContact != undefined && eventTagging.businessEventContact != "") {
                createBusinessItem.set_item('EventContact', eventTagging.businessEventContact.get_id());
            }
            createBusinessItem.set_item('Attire', attire);
            createBusinessItem.set_item('Honorees', honorees);
            createBusinessItem.set_item('Category', category);
            createBusinessItem.set_item('fAllDayEvent', allDayEvent);
            createBusinessItem.set_item('TaggedUrl', _spPageContextInfo.webServerRelativeUrl + '/Pages/' + articleTagging.pageName);
            createBusinessItem.update();
        }
        eventTagging.businessclientcontext.executeQueryAsync(function () { eventTagging.success(showAlert) }, eventTagging.failure);
    },


    sendMailToUsers: function () {

        var startDate = new Date(eventTagging.EventStartDate.getUTCFullYear(), eventTagging.EventStartDate.getUTCMonth(), eventTagging.EventStartDate.getUTCDate(), eventTagging.EventStartDate.getUTCHours(), eventTagging.EventStartDate.getUTCMinutes(), eventTagging.EventStartDate.getUTCSeconds());
        startDate = startDate.format("MM dd yyyy, HH:mm:ss");
        var endDate = new Date(eventTagging.EventEndDate.getUTCFullYear(), eventTagging.EventEndDate.getUTCMonth(), eventTagging.EventEndDate.getUTCDate(), eventTagging.EventEndDate.getUTCHours(), eventTagging.EventEndDate.getUTCMinutes(), eventTagging.EventEndDate.getUTCSeconds());
        endDate = endDate.format("MM dd yyyy, HH:mm:ss");
        var emailIDs = [];
        emailIDs.push(eventTagging.currentUserEmail);
        var meetingData = {
            "Subject": eventTagging.EventTitle,
            "body": eventTagging.EventDesc,
            "MailTo": emailIDs,
            "MailFrom": "noreply@viacom.com",
            "startDt": startDate,
            "endDt": endDate,
            "location": eventTagging.EventLocation,
            "siteUrl": _spPageContextInfo.siteAbsoluteUrl
        };
        $.support.cors = true;
        $.ajax({
            type: "POST",
            url: _spPageContextInfo.webServerRelativeUrl + "/_vti_bin/MeetingService.svc/CreateAppointment",
            data: JSON.stringify(meetingData),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            processData: true,
            success: function (data) {
                var d = data;
                alert('You will receive an email shortly allowing you to add this to your Outlook calendar.');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var a = jqXHR;
                //console.log(jqXHR + '---' + textStatus + '---' + errorThrown);
            }
        })
    },
    getUserEmail: function () {
        eventTagging.clientContext = new SP.ClientContext.get_current();
        eventTagging.web = eventTagging.clientContext.get_web();
        eventTagging.currentUser = eventTagging.web.get_currentUser();
        eventTagging.clientContext.load(eventTagging.currentUser);
        eventTagging.clientContext.executeQueryAsync(eventTagging.successUserEmail, eventTagging.failure);
    },

    successUserEmail: function () {
        eventTagging.currentUserEmail = eventTagging.currentUser.get_email();
    },
    getUTCDateAndSaveToLocal: function () {
        var startTime = $($("input[title='Event Start Date']").parent().siblings()[2]).find("select:eq(0)").val().split(" ")[0];
        var startHours = $($("input[title='Event Start Date']").parent().siblings()[2]).find("select:eq(0)").val().split(" ")[1];
        var startMinutes = $($("input[title='Event Start Date']").parent().siblings()[2]).find("select:eq(1)").val();
        var startDate = $("input[title='Event Start Date']").val();
        var startDateArr = startDate.split('/')
        var nowStartDate = new Date(startDateArr[0] + '/' + startDateArr[1] + '/' + startDateArr[2] + '/' + ' ' + startTime + ':' + startMinutes + ' ' + startHours);
        var endTime = $($("input[title='Event End Date']").parent().siblings()[2]).find("select:eq(0)").val().split(" ")[0];
        var endHours = $($("input[title='Event End Date']").parent().siblings()[2]).find("select:eq(0)").val().split(" ")[1];
        var endMinutes = $($("input[title='Event End Date']").parent().siblings()[2]).find("select:eq(1)").val();
        var endDate = $("input[title='Event End Date']").val();
        var endDateArr = endDate.split('/');
        var nowEndDate = new Date(endDateArr[0] + '/' + endDateArr[1] + '/' + endDateArr[2] + '/' + ' ' + endTime + ':' + endMinutes + ' ' + endHours);
        var arrDates = [];
        if ($("input[title='Event Start Date']").val() != "") {
            arrDates[0] = nowStartDate;
        }
        if ($("input[title='Event End Date']").val() != "") {
            arrDates[1] = nowEndDate;
        }
        this.clientContext = new SP.ClientContext();
        var isoDate = [];

        for (var i = 0; i < arrDates.length; i++) {
            var date = new Date(arrDates[i]);
            isoDate[i] = this.clientContext.get_web().get_regionalSettings().get_timeZone().localTimeToUTC(date.toISOString());
        }


        this.clientContext.executeQueryAsync(
            function () {

                if ($("input[title='Event Start Date']").val() != "") {
                    eventTagging.startDate = eventTagging.addBrowserUtcOffset(isoDate[0].get_value());
                }

                if ($("input[title='Event End Date']").val() != "") {
                    eventTagging.endDate = eventTagging.addBrowserUtcOffset(isoDate[1].get_value());
                }
                eventTagging.pushToOutlook('LocalCalendar', true, eventTagging.startDate, eventTagging.endDate);
            },
                function (sender, args) {
                    alert("Exception");
                }
            );

    },
    getUTCDateAndSaveToBusiness: function () {
        var startTime = $($("input[title='Event Start Date']").parent().siblings()[2]).find("select:eq(0)").val().split(" ")[0];
        var startHours = $($("input[title='Event Start Date']").parent().siblings()[2]).find("select:eq(0)").val().split(" ")[1];
        var startMinutes = $($("input[title='Event Start Date']").parent().siblings()[2]).find("select:eq(1)").val();
        var startDate = $("input[title='Event Start Date']").val();
        var startDateArr = startDate.split('/')
        var nowStartDate = new Date(startDateArr[0] + '/' + startDateArr[1] + '/' + startDateArr[2] + '/' + ' ' + startTime + ':' + startMinutes + ' ' + startHours);
        var endTime = $($("input[title='Event End Date']").parent().siblings()[2]).find("select:eq(0)").val().split(" ")[0];
        var endHours = $($("input[title='Event End Date']").parent().siblings()[2]).find("select:eq(0)").val().split(" ")[1];
        var endMinutes = $($("input[title='Event End Date']").parent().siblings()[2]).find("select:eq(1)").val();
        var endDate = $("input[title='Event End Date']").val();
        var endDateArr = endDate.split('/');
        var nowEndDate = new Date(endDateArr[0] + '/' + endDateArr[1] + '/' + endDateArr[2] + '/' + ' ' + endTime + ':' + endMinutes + ' ' + endHours);
        var arrDates = [];
        if ($("input[title='Event Start Date']").val() != "") {
            arrDates[0] = nowStartDate;
        }
        if ($("input[title='Event End Date']").val() != "") {
            arrDates[1] = nowEndDate;
        }
        this.clientContext = new SP.ClientContext();
        var isoDate = [];

        for (var i = 0; i < arrDates.length; i++) {
            var date = new Date(arrDates[i]);
            isoDate[i] = this.clientContext.get_web().get_regionalSettings().get_timeZone().localTimeToUTC(date.toISOString());
        }


        this.clientContext.executeQueryAsync(
            function () {

                if ($("input[title='Event Start Date']").val() != "") {
                    eventTagging.startDate = eventTagging.addBrowserUtcOffset(isoDate[0].get_value());
                }

                if ($("input[title='Event End Date']").val() != "") {
                    eventTagging.endDate = eventTagging.addBrowserUtcOffset(isoDate[1].get_value());
                }
                eventTagging.pushToCompnayOutlook(eventTagging.startDate, eventTagging.endDate);
            },
                function (sender, args) {
                    alert("Exception");
                }
            );

    },
    sendMail: function () {

        var arrDates = [];
        if (eventTagging.EventStartDate != "") {
            arrDates[0] = eventTagging.EventStartDate;
        }
        if (eventTagging.EventEndDate != "") {
            arrDates[1] = eventTagging.EventEndDate;
        }
        this.clientContext = new SP.ClientContext();
        var isoDate = [];

        for (var i = 0; i < arrDates.length; i++) {
            var date = new Date(arrDates[i]);
            isoDate[i] = this.clientContext.get_web().get_regionalSettings().get_timeZone().localTimeToUTC(date.toISOString());
        }


        this.clientContext.executeQueryAsync(
            function () {

                if (eventTagging.EventStartDate != "") {
                    eventTagging.EventOutlookStartDate = eventTagging.addBrowserUtcOffset(isoDate[0].get_value());
                }

                if (eventTagging.EventEndDate != "") {
                    eventTagging.EventOutlookEndDate = eventTagging.addBrowserUtcOffset(isoDate[1].get_value());
                }
                eventTagging.sendMail();
            },
                function (sender, args) {
                    //console.log("Exception");
                }
            );

    },
    addBrowserUtcOffset: function (date) {
        var utcOffsetMinutes = date.getTimezoneOffset();
        var newDate = new Date(date.getTime());
        newDate.setTime(newDate.getTime() - (utcOffsetMinutes * 60 * 1000));
        return newDate;
    },

    getEventContactLogin: function () {
        var eventContactDiv = $('#EventContact');
        var eventContactUsername = null;
        if (eventContactDiv != null && eventContactDiv != undefined) {
            var eventContactSpan = eventContactDiv.find('span[class="ms-usereditor"]');
            if (eventContactSpan != null && eventContactSpan != undefined) {
                var eventContactSpanChildren = eventContactSpan.children();
                if (eventContactSpanChildren.length > 0) {
                    var eventContactInput = eventContactSpanChildren[0].value;
                    if (eventContactInput != null && eventContactInput != undefined && eventContactInput.length > 7) {
                        var eventContactInputSpan;
                        if (eventContactInput.indexOf("160") > -1) {
                            eventContactInputSpan = $(eventContactInput.substring(6));
                        }
                        else {
                            eventContactInputSpan = $(eventContactInput);
                        }
                        if (eventContactInputSpan != null && eventContactInputSpan != undefined && eventContactInputSpan.length > 0) {
                            eventContactUsername = eventContactInputSpan[0].title;
                            if (eventContactUsername.indexOf("|") > -1) {
                                eventContactUsername = eventContactUsername.split('|')[1];
                            }
                        }
                    }
                }
            }
        }
        return eventContactUsername;
    }

}

$(document).ready(function () {
    articleTagging.pageName = getPageName();
    eventTagging.getUserEmail();
    eventTagging.displayCalendarEvents();
});
