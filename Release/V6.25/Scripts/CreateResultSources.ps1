﻿# =================================================================================
# FUNC: CreateResultSources
# DESC: Main function to create result souurce
# Please handle below code in deploy.ps1 file
#	$ver = $host | select version 
#	if($Ver.version.major -gt 1) {$Host.Runspace.ThreadOptions = "ReuseThread"} 
# Load all required assemby in deploy.ps1 file
# =================================================================================
function CreateResultSources([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $($ConfigPath) file"
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
			$searchServiceProxyName=$cfg.ResultSources.SearchServiceProxyName
			
			foreach($resultSource in $cfg.ResultSources.ResultSource)
            {
                CreateResultSource $resultSource $searchServiceProxyName
            }
        }
        catch
        {
            Write-Host "`nException in CreateResultSources :" $Error -ForegroundColor Red
            Write-Output "`nException in CreateResultSources:" $Error
        }
	}
	else
	{
		Write-Host "Error in CreateResultSources:" $Error -ForegroundColor Red
        Write-Output "Error in CreateResultSources:" $Error
	}
}

# =================================================================================
# FUNC: CreateResultSource
# DESC: Create result sources  
# =================================================================================
function CreateResultSource([object] $resultSource, $searchServiceProxyName)
{
	try
	{
		# Get the list for operation to perform
		$resultSourceName = $resultsource.ResultSourceName
		$scope = $resultsource.ScopeName
		$siteUrl= $resultsource.SiteUrl
		$query=$resultsource.Query

		#get Search Service application proxy
		$sspApp = Get-SPEnterpriseSearchServiceApplicationProxy $searchServiceProxyName
		 

		$site = get-spsite $siteUrl -WarningAction SilentlyContinue
		# create manager instances
		$fedManager = New-Object Microsoft.Office.Server.Search.Administration.Query.FederationManager($sspApp)
		$searchOwner = New-Object Microsoft.Office.Server.Search.Administration.SearchObjectOwner([Microsoft.Office.Server.Search.Administration.SearchObjectLevel]::$scope, $site.RootWeb)

		# define the required QUERY
		$queryProperties = New-Object Microsoft.Office.Server.Search.Query.Rules.QueryTransformProperties

		# define custom sorting
		$sortCollection = New-Object Microsoft.Office.Server.Search.Query.SortCollection
		$sortCollection.Add("LastModifiedTime", [Microsoft.Office.Server.Search.Query.SortDirection]::Descending)
		$queryProperties["SortList"] = [Microsoft.Office.Server.Search.Query.SortCollection]$sortCollection


		$resultSource = $fedManager.GetSourceByName($resultSourceName, $searchOwner)
		if ($resultSource -eq $null)
		{
			# create result source
			$resultSource = $fedManager.CreateSource($searchOwner)
			$resultSource.Name = $resultSourceName
			$resultSource.ProviderId = $fedManager.ListProviders()['Local SharePoint Provider'].Id
			$resultSource.CreateQueryTransform($queryProperties, $query)
			$resultSource.Commit()
			Write-Host "Result source $($resultSourceName) created successfuly" -ForegroundColor Green
			Write-Output "Result source $($resultSourceName) created successfuly" 
		}
		else
		{
			$resultSource.CreateQueryTransform($queryProperties, $query)
			$resultSource.Commit()
			
			Write-Host "Result Source $($resultSourceName) already exists and query updated successfully" -f Yellow
			Write-Output "Result Source $($resultSourceName) already exists and query updated successfully"
		}
	}
	catch
	{
		Write-Host "(ERROR in CreateResultSource: $($_.Exception.Message))" -ForegroundColor Red
		Write-Output "(ERROR in CreateResultSource: $($_.Exception.Message))" 
	} 
}







