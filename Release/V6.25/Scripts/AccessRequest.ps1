﻿# ===================================================================================
# Change acces request email ID function
# ===================================================================================

function ChangeAccessReqEmail([string]$ConfigPath = "")
{
    $error.Clear()
    $cfg = [xml](get-content $ConfigPath)

    # Exit if config file is invalid
    if( $? -eq $false ) 
    {
        Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
    }
    Write-Output "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    if($Error.Count -eq 0)
	{
        if( $? -eq $false ) 
	    {
            Write-Output "Could not read config file. Exiting ..."
            Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
	    }
        if(![string]::IsNullOrEmpty($cfg.Webapplication.Site))
        {
			try
			{
				foreach($Site in $cfg.Webapplication.Site)
				{
                    $ErrorActionPreference = "SilentlyContinue"
                    $siteCol=$null
                    $webCol=$null
                    $rootWeb=$null
					if($Site.isSiteCollection -eq "True")
					{
						$siteCol=Get-SPSite $Site.SiteURL               
						$webCol=$siteCol.AllWebs                     
					}
					elseif($Site.isSiteCollection -eq "False")
					{	            
						$rootWeb=Get-SPWeb $Site.SiteURL
					}
                    $error.Clear()
                    $ErrorActionPreference = "Continue"
                    $accessRequestEmail = $Site.AccessRequestEmailId
                    if(($webCol -ne $null) -and ($Site.isSiteCollection -eq "True"))
                    {
                        foreach($web in $webCol)
                        {
                           try
			               {
                               if ($web.HasUniqueRoleAssignments)
                               {
                                    $web.RequestAccessEmail = $accessRequestEmail
                                    $web.Update()
			                        Write-Host "Access Request Email set at " $web.Url -ForegroundColor Cyan
                               } 
                            }
			                catch
			                {
				                Write-Output "Exception for $($web.Url)"|out-null    
				                Write-Host "Exception for $($web.Url)" -ForegroundColor Yellow
			                }      
                        }
                    }
                    elseif($Site.isSiteCollection -eq "False")
                    {
                        try
			            {
                            if ($rootWeb.HasUniqueRoleAssignments)
                            {
                                $rootWeb.RequestAccessEmail = $accessRequestEmail
                                $rootWeb.Update()
			                    Write-Host "Access Request Email set at " $rootWeb.Url -ForegroundColor Cyan
                            } 
                        }
			            catch
			            {
				            Write-Output "Exception for $($rootWeb.Url)"|out-null    
				            Write-Host "Exception for $($rootWeb.Url)" -ForegroundColor Yellow
			            } 
                    }
				}
			}
			catch
			{
				Write-Output "Exception for $($Site.Url)"|out-null    
				Write-Host "Exception for $($Site.Url)" -ForegroundColor Yellow
			}
        }
		else
		{
			Write-Output "SiteUrl is empty for $($Site.Url)"|out-null    
            Write-Host "SiteUrl is empty for $($Site.Url)" -ForegroundColor Yellow
		}
    }
}
