# =================================================================================
#
#Main Function to Add web Parts to publishing page
#
# =================================================================================
function GetWebpartDetails([string]$ConfigPath = "")
{
    $Error.Clear();
    
                $cfg = [xml](get-content $ConfigPath)
   
    if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
                {    
                                  Add-PsSnapin Microsoft.SharePoint.PowerShell
                }
    
                # Exit if config file is invalid
                if( $? -eq $false ) 
    {
                                Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
                }
                
                Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $($ConfigPath) file"
                if($Error.Count -eq 0)
                {
        try
        {
            $Error.Clear()            
            foreach($webpart in $cfg.Webparts.Webpart)
            {
                AddWebPartToPage $webpart
            }            
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
                }
                else
                {
                                Write-Host $Error -ForegroundColor Red
        Write-Output $Error
                }

} 

# =================================================================================
#
# FUNC: AddWebPartPublish
# DESC: Add webparts on to the page.
# =================================================================================
function  AddWebPartToPage([object] $webpart)
{
    [string]$loc=Get-Location
    $loc +=$webpart.HtmlFilePath
    $htmlContent = get-content -Path $loc 
    $webPartProperty_Visible = $true 
    $web = get-spweb $webpart.SiteURL 
    $page = $web.GetFile($webpart.PagePath);
    if ($page.Level -eq [Microsoft.SharePoint.SPFileLevel]::Checkout)
    { 
        if ($page.CheckedOutBy.UserLogin -eq $web.CurrentUser.UserLogin) 
        {
            Write-Host "Page has already been checked out " -ForegroundColor Yellow
            Write-Output "Page has already been checked out "                           
        } 
        else 
        {
            $web.CurrentUser.LoginName
            $page.UndoCheckOut()
            $page.CheckOut()
            Write-Host "Check out the page override" -ForegroundColor Yellow
            Write-Output "Check out the page override"
        }        
    }
    else
    {
        $page.CheckOut() 
        Write-Host "Check out the page" -ForegroundColor Green
        Write-Output "Check out the page"
    }
    $removeWPcollection = @()
    # Get the LimitedWebPartManager 
    $webpartmanager=$web.GetLimitedWebPartManager($webpart.SiteURL+$webpart.PagePath, [System.Web.UI.WebControls.WebParts.PersonalizationScope]::Shared)
    
    
        #Create GUID 
        $createNewGuid = [System.Guid]::NewGuid().ToString() 
        $guidForWebPart = "g_" + $createNewGuid.Replace("-","_") 
    
        # Instantiate wp 
        $webpartToAdd =New-Object Microsoft.SharePoint.WebPartPages.ScriptEditorWebPart  
        $webpartToAdd.ID = $guidForWebPart 
        $code = "$" 
        $webpartToAdd.Content =  $htmlContent
        if(![string]::IsNullOrEmpty($webpart.Title))
        {
            $webpartToAdd.Title = $webpart.Title
        }
        if(![string]::IsNullOrEmpty($webPartProperty_Visible))
        {
            $webpartToAdd.Visible = $webPartProperty_Visible
        }  
        if(![string]::IsNullOrEmpty($webpart.ChromeType))
        {
            $webpartToAdd.ChromeType = $webpart.ChromeType
        }  
        if(![string]::IsNullOrEmpty($webpart.HorizontalAlign))
        {
           $webpartToAdd.HorizontalAlign = $webpart.HorizontalAlign
        }  
          
        # Add the web part 
        $webpartmanager.AddWebPart($webpartToAdd, $webpart.WebpartZone, $webpart.Index) 
        Write-Host "Webpart added to the page" -ForegroundColor Green
        Write-Output "Webpart added to the page"| Out-Null
  
        # Check in the Page with Comments
        $page.CheckIn("Checkin by PowerShell")
          
        # Publish the Page With Comments
        $page.Publish("Published by PowerShell")
        Write-Host "Page has been checked in" -ForegroundColor Green
        Write-Output "Page has been checked in"| Out-Null   
    
        # Update the web 
        $web.Update(); 
     
         
        $web.Dispose()   
        
}

