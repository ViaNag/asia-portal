/*Modified by Singh Rohit Start*/

var userProfilePanel = {
    clientContext: null,
    web: null,
    collListItem: null,
    UserProperties: new Array(),
    userProfile: null,
    currentUser: null,
    peopleManager: null,
    LoginName: null,
    UserPropertiesValue: new Array(),
    _getToKnowIntranet: null,
    _PictureUrl: null,
    percentComplete: 0,
    intranetQuickLinks: null,
    collGroup: null,
    listItemInfo: null,
    userProfileInitialized: false,
    onUserProfilePanelLoad: function () {
        var aboutMeTag = $("#ID_AboutMe").attr("onmenuclick");
        if (aboutMeTag != null && aboutMeTag != undefined) {
            var myHost = aboutMeTag.substring(aboutMeTag.indexOf('http'), aboutMeTag.length - 2);
            /* Changes for Defect ID 371 */
            $("#viewprofile").append("<a href='" + myHost + "'><span class='profile-sprite'></span> My profile</a>");
        }
        //userProfilePanel.CheckIfCurrentUserIsNewHire();

    },
    CheckIfCurrentUserIsNewHire: function () {

        userProfilePanel.clientContext = new SP.ClientContext.get_current();
        userProfilePanel.collGroup = userProfilePanel.clientContext.get_web().get_siteGroups();
        userProfilePanel.clientContext.load(userProfilePanel.collGroup);
        userProfilePanel.clientContext.load(userProfilePanel.collGroup, 'Include(Users)');
        userProfilePanel.currentUser = userProfilePanel.clientContext.get_web().get_currentUser();
        userProfilePanel.clientContext.load(userProfilePanel.currentUser);
        userProfilePanel.clientContext.executeQueryAsync(Function.createDelegate(this, userProfilePanel.onQuerySucceeded), Function.createDelegate(this, userProfilePanel.onQueryFailed));
    },

    onQuerySucceeded: function (sender, args) {

        var UserExistInGroup = false;
        var groupEnumerator = userProfilePanel.collGroup.getEnumerator();
        while (groupEnumerator.moveNext()) {
            var oGroup = groupEnumerator.get_current();
            if (oGroup.get_title() == 'Greenroom Pilot') {
                var collUser = oGroup.get_users();
                var userEnumerator = collUser.getEnumerator();
                while (userEnumerator.moveNext()) {
                    var oUser = userEnumerator.get_current();
                    if (oUser.get_loginName() == userProfilePanel.currentUser.get_loginName()) {
                        UserExistInGroup = true;
                        break;
                    }
                }
            }

            if (UserExistInGroup) {
                break;
            }
        }
        if (UserExistInGroup) {
            $('#greenroom-panel-onboarding-container').show();
        }
    },

    onQueryFailed: function (sender, args) {

    },
    getUserPropertiesFromProfile: function () {
        var getQuestionsContext = new SP.ClientContext.get_current();
        var peopleManager = new SP.UserProfiles.PeopleManager(getQuestionsContext);
        quickLinks.LoginName = quickLinks.currentUser.get_loginName();
        //Code update for ADFS
        if (quickLinks.LoginName.split('|').length == 2) {
            quickLinks.LoginName = quickLinks.LoginName.split('|')[1];
        }
        var userProfilePropertiesForUser = new SP.UserProfiles.UserProfilePropertiesForUser(quickLinks.clientContext, quickLinks.LoginName, "IntranetGetToKnowStatus");
        getQuestionsUserProfileProperties = peopleManager.getUserProfilePropertiesFor(userProfilePropertiesForUser);
        getQuestionsContext.load(userProfilePropertiesForUser);
        getQuestionsContext.executeQueryAsync(userProfilePanel.onSuccess, userProfilePanel.onQueryFailed);
    },
    onSuccess: function () {
        quickLinks._getToKnowIntranet = getQuestionsUserProfileProperties[0];
        userProfilePanel.GetQuestions();
    },

    GetQuestions: function () {
        userProfilePanel.clientContext = new SP.ClientContext.get_current();
        userProfilePanel.web = userProfilePanel.clientContext.get_web();
        var oListMasterItems = userProfilePanel.clientContext.get_site().get_rootWeb().get_lists().getByTitle('MasterItems');
        var camlQuery = new SP.CamlQuery();
        userProfilePanel.collListItem = oListMasterItems.getItems(camlQuery);
        userProfilePanel.clientContext.load(userProfilePanel.collListItem);
        userProfilePanel.clientContext.executeQueryAsync(Function.createDelegate(this, userProfilePanel.onQuestionSucceeded), Function.createDelegate(this, userProfilePanel.onQuestionFailed));
    },

    onQuestionSucceeded: function () {
        var listItemEnumerator = userProfilePanel.collListItem.getEnumerator();
        $("#questions").html(""); //Clear out the questions div on updating the questions.
        while (listItemEnumerator.moveNext()) {
            var oListItem = listItemEnumerator.get_current();
            userProfilePanel.listItemInfo = oListItem.get_item('Title');
            var listItemid = oListItem.get_item('ID');
            var listItemURL = oListItem.get_item('Url');
            var formatValues = quickLinks._getToKnowIntranet + ":" + listItemid;
            if (listItemURL == '' || listItemURL == null) {
                listItemURL = '#';
            }

            //Check if profile picture exists add check image in user profile completion section
            if (quickLinks._getToKnowIntranet != undefined && quickLinks._getToKnowIntranet != null && quickLinks._getToKnowIntranet.indexOf(listItemid) > -1 && listItemid != 3) {
                $("#questions").append('<li><a href="' + listItemURL + '" target="_blank" onclick="userProfilePanel.updateGetToKnowIntranetStatus(\'' + listItemURL + '\',0,\'' + listItemid + '\');">' + userProfilePanel.listItemInfo + '</a><span><small class="check"></small></span></li>');
            }
            else if (userProfilePanel.listItemInfo.toLowerCase() == "update your profile photo" && quickLinks._PictureUrl != "_layouts/15/images/Person.gif") {
                $("#questions").append('<li><a href="' + listItemURL + '" target="_blank">' + userProfilePanel.listItemInfo + '</a><span><small class="check"></small></span></li>');
            }
            else if (quickLinks._getToKnowIntranet != undefined && quickLinks._getToKnowIntranet != null && quickLinks._getToKnowIntranet.indexOf(listItemid) > -1 && listItemid == 3) {
                $("#questions").append('<li><a href="' + listItemURL + '" target="_blank">' + userProfilePanel.listItemInfo + '</a><span id="Check' + listItemid + '"></span><small class="uncheck"></small></li>');
            }
            else {
                $("#questions").append('<li><a href="' + listItemURL + '" target="_blank" onclick="userProfilePanel.updateGetToKnowIntranetStatus(\'' + listItemURL + '\',\'' + formatValues + '\',\'' + listItemid + '\');">' + userProfilePanel.listItemInfo + '</a><span id="Check' + listItemid + '"></span><small class="uncheck"></small></li>');
            }
        }
        userProfilePanel.PercentComplete();
        $("#ifProfileCompleteHtml").append('<div id="get-started-preloader"><img id="get-started-preloader-img" src="/SiteCollectionImages/icons/preloader.gif"><div>');
    },

    onQuestionFailed: function () {
        //console.log("Unable to fetch Questions. Please try again");
    },

    updateUserProfile: function (LoginName, propertyName, propertyValue) {
        propertyValue = propertyValue.replace(/\|/g, '');
        propertyValue = propertyValue.replace(/\&/g, '&amp;');
        var domain, userName;

        if (LoginName != undefined && LoginName != null && LoginName != "") {
            // code Updated for ADFS
            if (LoginName.split('\\').length == 2) {
                domain = LoginName.split("\\")[0];
                userName = LoginName.split("\\")[1];
            }
            else {
                domain = "";
                userName = LoginName;
            }
        }
        var userprofileServiceURL = GetUserProfileUrlFromLocalStarageJson();
        var siteUrl = GetMySiteUrlFromLocalStarageJson();
        $.support.cors = true;
        var jsonDataType = "json";
        $.ajax({
            url: userprofileServiceURL,
            dataType: jsonDataType,
            type: "GET",
            data: { domain: domain, userName: userName, propertyName: propertyName, value: propertyValue, siteUrl: siteUrl },
            success: function (data) {
                //Grab our data from Ground Control
                if (data.indexOf("Commit Success") > -1) {
                    //location.reload();
                    userProfilePanel.getUserPropertiesFromProfile();
                }
                else {
                    if (console != undefined && console != null) {
                        //console.log(data);
                        $("#get-started-preloader").hide();
                    }
                }
            },
            error: function (data) {
                /* Added code to check the status code and status text in case of IE9
                as IE couldn't parse the response XML and falls into the error function
                with all the successful processing at the back end and statusText=success and status=200 */
                if (data.statusText.toLowerCase() == "success" && data.status == 200) {
                    //location.reload();
                    userProfilePanel.getUserPropertiesFromProfile();
                }
                    //If any errors occurred - detail them here
                else if (console != undefined && console != null) {
                    //console.log(data);
                    $("#get-started-preloader").hide();
                }
            }
        });
    },

    PercentComplete: function () {
        if (quickLinks._getToKnowIntranet != undefined && quickLinks._getToKnowIntranet != null) {
            //if(quickLinks._PictureUrl  ==	"_layouts/15/images/Person.gif")
            //{
            quickLinks._getToKnowIntranet = quickLinks._getToKnowIntranet.split(':');
            //}
            //else
            //{
            //	quickLinks._getToKnowIntranet = quickLinks._getToKnowIntranet.split(':2');
            //}

            if (quickLinks._getToKnowIntranet.length > -1 && quickLinks._getToKnowIntranet[0] == null || quickLinks._getToKnowIntranet[0] == "") {
                for (var j = 0; j < quickLinks._getToKnowIntranet.length; j++) {
                    quickLinks._getToKnowIntranet[j] = quickLinks._getToKnowIntranet[j + 1];
                }
                quickLinks._getToKnowIntranet.length = (quickLinks._getToKnowIntranet.length) - 1;
            }
            if (quickLinks._getToKnowIntranet.length > -1) {
                var propertiesPercentage = 0;
                for (var i = 0; i < quickLinks._getToKnowIntranet.length; i++) {
                    if (quickLinks._getToKnowIntranet[i] == 3) //3 is the ID of the Upload Your profile photo item from the MasterItems list at the root site
                    {
                        propertiesPercentage = 20;
                    }
                }
                userProfilePanel.percentComplete = 20 * (quickLinks._getToKnowIntranet.length) - propertiesPercentage;

            }
            //Check if profile picture exists add 20% for profile completion
            if (quickLinks._PictureUrl != "_layouts/15/images/Person.gif") {
                userProfilePanel.percentComplete = userProfilePanel.percentComplete + 20;
            }
        }
        if (userProfilePanel.percentComplete <= 100) {
            $("#bar").html("<div id='complete' style='width:" + userProfilePanel.percentComplete + "%!important;'>" + userProfilePanel.percentComplete + "%</div>");
        }

        if (userProfilePanel.percentComplete == 100) {
            $("#ifProfileCompleteHtml").hide();
            $("#bar").hide();
            $("#greenroom-panel-getstarted-container").hide();
        }
        $("#get-started-preloader").hide();
    },

    updateGetToKnowIntranetStatus: function (url, IntranetStatus, listItemid) {
        var customUserField = 'IntranetGetToKnowStatus';
        if (IntranetStatus != '0') {
            $("#get-started-preloader").show();
            userProfilePanel.updateUserProfile(quickLinks.LoginName, customUserField, IntranetStatus);
            //userProfilePanel.percentComplete = userProfilePanel.percentComplete+20;
            //$('#Check'+listItemid+'').html('<img class="check" src="/SiteCollectionImages/icons/greenroom-panel-icon-circle-check.png" alt="check"/>');
            //$('#bar').html('<div class="complete-new" style=width:'+userProfilePanel.percentComplete+'%!important;height:100%!important;background-color:#0d8bb5!important;>'+userProfilePanel.percentComplete +'%</div>');
            //$('#Check'+listItemid+'').parent('li').children('.uncheck').hide();	
        }
    },
    GetMyQuickLinks: function (userName, domain) {
        var userprofileServiceURL = userProfilePanel.GetConfigValueLocalStarageJson("MyLinksServiceUrl");
        var mySiteUrl = userProfilePanel.GetConfigValueLocalStarageJson("QuickLinksUserProfileUrl");
        var serviceURL = userprofileServiceURL + '?siteUrl=' + mySiteUrl + '&username=' + userName + '&domain=' + domain;
        userProfileCrossDomainAjax(serviceURL, function (data) {
            userProfilePanel.BindMyQuickLinks(data);
        });
    },
    GetCurrentUserLoginName: function () {
        userProfilePanel.clientContext = new SP.ClientContext.get_current();
        userProfilePanel.web = userProfilePanel.clientContext.get_web();
        userProfilePanel.currentUser = userProfilePanel.web.get_currentUser();
        userProfilePanel.clientContext.load(userProfilePanel.currentUser);
        userProfilePanel.clientContext.executeQueryAsync(userProfilePanel.onGetCurrentUserSuccess, userProfilePanel.onGetCurrentFail);
    },

    onGetCurrentUserSuccess: function () {
        userProfilePanel.LoginName = userProfilePanel.currentUser.get_loginName();
        //Code update for ADFS
        var domain, userName;
        if (userProfilePanel.LoginName.split('|').length == 2) {
            userProfilePanel.LoginName = userProfilePanel.LoginName.split('|')[1];
            domain = userProfilePanel.LoginName.split("\\")[0];
            userName = userProfilePanel.LoginName.split("\\")[1];
        }
        else {
            domain = "";
            userName = userProfilePanel.LoginName;
        }


        userProfilePanel.GetMyQuickLinks(userName, domain);
    },

    onGetCurrentFail: function () {

    },
    GetConfigValueLocalStarageJson: function (configKey) {
        var retrievedObject = localStorage.getItem(GetLocalStorageSuffixKey('ConfigListDataJson'));
        ConfigListData = JSON.parse(retrievedObject);
        if (ConfigListData != null && ConfigListData.results != null && ConfigListData.results.length > 0) {
            try {
                for (var congigItemInex = 0; congigItemInex < ConfigListData.results.length; congigItemInex++) {
                    var ItemTitle = ConfigListData.results[congigItemInex].Title;
                    if (ItemTitle.toLowerCase() === configKey.toLowerCase()) {
                        return ConfigListData.results[congigItemInex].sitepath;
                    }
                }
            }
            catch (e) {
            }
        }
    },

    GetMySiteUrlFromLocalStarageJson: function () {
        var retrievedObject = localStorage.getItem(GetLocalStorageSuffixKey('ConfigListDataJson'));
        ConfigListData = JSON.parse(retrievedObject);
        if (ConfigListData != null && ConfigListData.results != null && ConfigListData.results.length > 0) {
            try {
                for (var congigItemInex = 0; congigItemInex < ConfigListData.results.length; congigItemInex++) {
                    var ItemTitle = ConfigListData.results[congigItemInex].Title;
                    if (ItemTitle.toLowerCase() === "mysiteurl") {
                        return ConfigListData.results[congigItemInex].sitepath;
                    }
                }
            }
            catch (e) {
            }
        }
    },
    BindMyQuickLinks: function (data) {
        var myQuickLinksHtml = "";
        var myQuickLinks = data.split("@#@");
        if (myQuickLinks != null && myQuickLinks != undefined && myQuickLinks != "") {
            var myLinksLength = 0;
            if (myQuickLinks.length > 3) {
                myLinksLength = 3;
            }
            else {
                myLinksLength = myQuickLinks.length;
            }
            for (var i = 0; i < myLinksLength; i++) {
                var myLinkHrefParts = myQuickLinks[i].split(",");
                if (myLinkHrefParts != null && myLinkHrefParts != undefined && myLinkHrefParts != "") {
                    var myLinkHref = myLinkHrefParts[1];
                    var myLinkDesc = myLinkHrefParts[0];
                    myQuickLinksHtml += '<li><a href="' + myLinkHref + '"><span class="book-vacation-icon"></span>' + myLinkDesc + '</a></li>';

                }
            }
            if (myQuickLinks.length > 0) {
                var mysiteConfigUrl = userProfilePanel.GetConfigValueLocalStarageJson("mysiteurl");
                myQuickLinksHtml += '<li><a href="' + mysiteConfigUrl + '/_layouts/15/MyQuickLinks.aspx">View more links > </a></li>';
            }
        }
        $("#greenroom-panel-default-links ul").append(myQuickLinksHtml);
    }
}
// call with your url (with parameters) 
// 2nd param is your callback function (which will be passed the json DATA back)
function userProfileCrossDomainAjax(url, userProfileSuccessCallback) {

    $.ajax({
        url: url,
        cache: false,
        dataType: 'json',
        type: 'GET',
        async: false, // must be set to false
        success: function (data, success) {
            userProfileSuccessCallback(data);
        },
        error: function (data) {
            userProfileSuccessCallback(data);
        }
    });

}
$(document).ready(function () {
    if (userProfilePanel.userProfileInitialized) return;
    else {
        userProfilePanel.userProfileInitialized = true;
        ExecuteOrDelayUntilScriptLoaded(userProfilePanel.onUserProfilePanelLoad, "SP.UserProfiles.js");
    }
});
$(window).load(function () {
    setTimeout(function () { userProfilePanel.GetCurrentUserLoginName(); }, 1000);
});
