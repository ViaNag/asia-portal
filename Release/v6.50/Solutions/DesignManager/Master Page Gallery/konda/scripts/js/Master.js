var ConfigListData;
var StorageSuffixKey="";
var SubSiteColorsListData;
var IsKaleoInjctorLoaded = false;
/*----------------Variables to handle profile picture issue---------------------------*/
var userProfileImageLoaded = false;
var checkPPStatusCount = 0;
var mySiteHostName = "me.viacom.com";
var checkPPStatusMaxCount = 5;
/*------------------Variables to handle profile picture issue-------------------------*/
$(document).ready(function () {
    /*----------------Methods to handle profile picture issue---------------------------*/
    setInterval(function () { if (!userProfileImageLoaded && checkPPStatusCount++ <= checkPPStatusMaxCount) { checkUserProfilePicStatus(true); } }, 10000);
    /*------------------Methods to handle profile picture issue-------------------------*/
    GetConfigListDataJson();
    GetSubSiteColorsListDataJson();
	var footerPortalObject = localStorage.getItem(GetLocalStorageSuffixKey('footerPortalListItem'));
	/*----------------Methods to handle footer portal links---------------------------*/
	var footerPortalObject = localStorage.getItem(GetLocalStorageSuffixKey('footerPortalListItem'));
	if(!footerPortalObject)
	{
		GetnSetFooterPortalDetails();
	}
	else
	{
		setFooterPortalLinks(footerPortalObject);
	}
	/*----------------Methods to handle footer portal links---------------------------*/
	ExecuteOrDelayUntilScriptLoaded(CurrentUserInfo.GetCurrentUserPropertiesFromProfileJson, 'sp.js');
    KaleoWidget = {};
    KaleoWidget.config = [
		//[DOM ID, Host URL, Widget Token]
		["kw-widget", "https://viacom.kaleosoftware.com", "026df204-3b38-4c54-88d6-c7e5b55981f8"]
    ];
    AskQuestion();
    var taggedUrlPageImage = "";
    var arr11 = [];
    /* display viacom logo in nav bar start */
    $('<div id="greenroom-logo-container"><a href="/sites/Asia/" alt="Asia"><img id="greenroom-logo" src="/sites/Asia/SiteCollectionImages/asiaportal_logo.png" /></a></div>').insertBefore('#suiteBar');
    $('#NavBarContainer').prepend('<div id="greenroom-logo-container-scroll"><a alt="Asia" href="/sites/Asia/"><img src="/sites/Asia/SiteCollectionImages/asiaportal_logo_scroll.png" id="greenroom-logo"></a></div>');
    /* display viacom logo in nav bar end */
    $("#DeltaPageStatusBar").insertAfter("#BreadCrumbNavigation"); //to move status bar after black share bar
    $("#globalNavBox").insertAfter("#DeltaPageStatusBar"); //to move editing/share/follow controls below top nav
    $('#ribbonBox').addClass('container'); //adds container class to center share/follow buttons

    /* removes suitebar links: OneDrive/SkyDrive, Sites start */
    $("ul.ms-core-suiteLinkList li:contains('Drive')").remove(); //removes li if it contains Drive
    $("ul.ms-core-suiteLinkList li:contains('Sites')").remove(); //removes li if it contains Sites
    $("ul.ms-core-suiteLinkList").show(); // display remaining links in suitebar
    /* remove suitebar links: OneDrive/SkyDrive, Sites end */
    /* Commented below line to append kaleo fish image as the RootAspMenu tag is generating different for different types of users. Moved this functionality in the AskQuestion function.
	$('#zz13_RootAspMenu').find('li').find('ul').find('li:eq(6)').find('.menu-item-text').append('<img class="greenroom-topnav-icon-kaleo" src="/SiteCollectionImages/icons/greenroom-topnav-icon-kaleo.png" />');
	 */
    /*For Top nav scroll start*/

    if (window.location.href.indexOf("/Pages/homepage.aspx") === -1) { //quicklinks will display on subsites
        $('#QuickLinks').show();
    }
    //Override the picture of the author with the complementee for viacompliments
    //TG: commenting ViaCompliment code block
    /* if(window.location.href.toLowerCase().indexOf("company/viacompliments/sitepages/topic.aspx") > -1){
	$(".ms-secondaryCommandLink").each(function(){
	var liketText = $(this).text();
	if(liketText.toLowerCase() == "unlike"){
	$(this).addClass("active");
	}
	});
	setLike();
	bindClickOnLike();
	$(".ms-comm-postExpander").hide();
	$($(".ms-comm-postBodyThreaded .ms-rtestate-field")[0]).html($(".ms-comm-postBodyThreaded .ms-rtestate-field")[0].textContent.replace(/\n/g,'<br\>'));
	}   */

    /*** fix for User Story 48684 14June 2018 ***/
    $('#s4-workspace').addClass('s4-workspace-scroll'); //add class on page load
    // 
    
    jQuery('#s4-workspace').bind('scroll', function () {

        if ($('#s4-workspace').scrollTop() >= 50) {
            //showing the static menu
            $('#TopBar').addClass('fixed');
            $('#TopBar').css({
                top: 0
            });
            $('#s4-workspace').removeClass('s4-workspace-scroll');
            $('#greenroom-logo-container-scroll').css("display", "block");
            $('#ms-designer-ribbon').removeClass('mainRibbonHide');
            $('#ms-designer-ribbon').addClass('mainRibbonDisplay');
            $("#greenroom-logo-container").hide();
            $("#TopBar #SearchBox .ms-srch-sb").animate({
                width: '215px'
            }, 500);
            $("#SerachBoxConatiner input").animate({
                width: '170px'
            }, 500);
            $('#QuickLinks').show();
            // $("#QuickLinksPlaceholder").hide();
        }
        if ($('#s4-workspace').scrollTop() < 50) {
            $('#TopBar').removeClass('fixed');
            $('#s4-workspace').addClass('s4-workspace-scroll');
            $('#greenroom-logo-container-scroll').css("display", "none");
            $('#ms-designer-ribbon').addClass('mainRibbonHide');
            $('#ms-designer-ribbon').removeClass('mainRibbonDisplay');
            $('#ms-designer-ribbon').css('visibility', 'visible');
            $("#greenroom-logo-container").show();
            $('#QuickLinks').show();
            $('#s4-workspace').css('margin-top', '25px')
        }

        /*----------------------------------------------------------------------------------------------------------------*/
        /*----------------------- SMOOTH SCROLL ------------------------*/
        /*
		$('#s4-workspace a[href*=#]').click(
		function (e) {

		// Disable default click event and scrolling
		e.preventDefault();
		var hash = $(this).attr('href');
		hash = hash.slice(hash.indexOf('#') + 1);

		// Scroll to
		$("#s4-workspace").scrollTo(hash == 'top' ? 0 : 'a[name='+hash+']', hash == 'top' ? 0 : 'a[name='+hash+']');

		window.location.hash = '#' + hash;
		}
		);
		 */
        /*----------------------------------------------------------------------------------------------------------------*/

    });
    /*For Top nav scroll end*/

    /* display comment message start */
    $('#CommentArea').text('Type your comment here...');
    var standard_message = $('#CommentArea').val();
    $('#CommentArea').focus(
		function () {
		    if ($(this).val() == standard_message)
		        $(this).val("");
		});
    $('#CommentArea').blur(
		function () {
		    if ($(this).val() == "")
		        $(this).val(standard_message);
		});
    /* display comment message end */

    $("#ms-newsfeedpartdiv").insertBefore($(".greenroom-post-comment-headline"));

    /** welcome text swap last name with first name start **/
    var currentWelcomeText = $('#welcomeMenuBox').find('span').eq(1).find('a').text(); //$('span#zz4_Menu_t.ms-menu-althov').text();
    var arrayText = currentWelcomeText.split(',');
    var FirstName = '';
    var newWelcomeText = '';
    if (arrayText.length == 2) {
        var LastName = arrayText[0];
        FirstName = arrayText[1].substr(0, arrayText[1].indexOf('Use'));
        newWelcomeText = '<span class="welcome-txt">Welcome, </span>' + FirstName + ' ' + LastName;
        $('#welcomeMenuBox').find('span').eq(1).find('a').html(newWelcomeText);
    }
    if (arrayText.length == 1) {
        FirstName = arrayText[0].substr(0, arrayText[0].indexOf('Use'));
        newWelcomeText = '<span class="welcome-txt">Welcome, </span>' + FirstName;
        $('#welcomeMenuBox').find('span').eq(1).find('a').html(newWelcomeText);
    }
    var welcomeIcon = "/SiteCollectionImages/icon_welcome.png";
    /** welcome text swap last name with first name end **/
    /* Commented here and added this code in userProfilePanel.js for Defect ID 371 */
    //$('#DeltaSuiteBarRight').prepend('<img id="WelcomeIcon" src="/SiteCollectionImages/icon_welcome.png" />');
    /*$("#TopBar").append("#s4-ribbonrow");
	$('#TopBar').prepend( $('#suiteBar') );
	$('#TopBar').detach().prependTo('#ms-designer-ribbon');*/
    //$("#TopBar").insertBefore($("#s4-ribbonrow"));
    $("#TopBar").insertAfter($("#greenroom-ribbon-container .container"));

    /************* Search box focus / blur functionality***********/

    /*    $("[id*='_csr_sbox']").focus(function() {
	$("#QuickLinks").animate({
	display: "none"
	});
	$(this).parent().animate({
	width: "279px"
	});
	});
	$("[id*='_csr_sbox']").blur(function() {
	$("#QuickLinks").animate({
	display: "block"
	});
	$(this).parent().animate({
	width: "137px"
	});
	});
	 */
    /****************************************************************/

    /*Show KaleoWidget on ask a question hover*/
    $('.askQuestion').on('click', function (event) {
        $('.askQuestion').addClass('active');
        $('body').addClass('kw-opened');
        $("#s4-workspace").animate({
            scrollTop: 0
        }, 500, function () {
            //	$("#QuickLinksPlaceholder").slideToggle();
        });
        if (IsKaleoInjctorLoaded == false) {
            var kwWidgetIframe = '<iframe width="1" height="1" style="visibility:hidden" src="https://viacom.kaleosoftware.com/users/auth/saml?redirect_to=/widgets/saml_status"> </iframe>';
            $("#iFrameForAsk-a-Question").html(kwWidgetIframe);
            var head = document.getElementsByTagName('head')[0];
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.onload = AskQuestionAfterInjectorLoad;
            script.src = 'https://viacom.kaleosoftware.com/assets/v4/widgets/injector.js';
            head.appendChild(script);
            IsKaleoInjctorLoaded = true;
        } else {
            AskQuestionAfterInjectorLoad();
        }

    });

    $('.kw-button').on('click', function (event) {
        $(".kw-popover").attr("style", "display:block");
    });

    /*Hide KaleoWidget on click out side KaleoWidget */
    $(document).click(function (e) {
        var container = $('#kw-widget');
        var askQuestionContainer = $('#ask-Question');
        var askKaleoSidebarContainer = $('#btn-greenroom-ask-kaleo');
        var askQuestionResponsiveContainer = $('.askQuestion-responsive');

        if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0 && !askQuestionContainer.is(e.target) && askQuestionContainer.has(e.target).length === 0
         && !askKaleoSidebarContainer.is(e.target) && askKaleoSidebarContainer.has(e.target).length === 0
         && !askQuestionResponsiveContainer.is(e.target) && askQuestionResponsiveContainer.has(e.target).length === 0)
            // ... nor a descendant of the container
        {
            $("#kw-widget").removeClass("kw-open");
            $(".kw-iframe-container").html("");
            $("#kw-widget").attr("classname", "kw-container");
            $('.askQuestion').removeClass('active');
            $('body').removeClass('kw-opened');
            container.hide();

        }
    });

    $("#s4-workspace").one("scroll", function () {
        var s4_height = $('#s4-workspace').height();
        var new_s4_height = s4_height + 40;
        $('#s4-workspace').height(new_s4_height);
        new_s4_height = s4_height;
    });

});


//Get and Set Portals data in local storage.

function GetnSetFooterPortalDetails() {
    var callConfig = $.ajax({
        url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('GR Footer Portals')/items?$filter=PortalLinkIsActive eq 1&$select=PortalLinkUrl,Title&$orderby=PortalLinkOrder",
        type: "GET",
        dataType: "json",
        headers: {
            Accept: "application/json;odata=verbose"
        }
    });
    callConfig.done(function (data, textStatus, jqXHR) {
        try {
            portalsListData = data.d.results;
			localStorage.setItem(GetLocalStorageSuffixKey('footerPortalListItem'), JSON.stringify(portalsListData));	
			setFooterPortalLinks(JSON.stringify(portalsListData));
            
        }
        catch (e) {
            //console.log(e);
        }
        
    });
    callConfig.fail(function (jqXHR, textStatus, errorThrown) {
        
    });
}


//Set Portals data in footer.
function setFooterPortalLinks(_portalsListData)
{
	try {
			_portalsListData = JSON.parse(_portalsListData);
            $.each(_portalsListData, function (index, data) {
			
			var portalLI = document.createElement('li');
			var portalA = document.createElement('a');
            portalA.setAttribute('href',data.PortalLinkUrl);
			portalA.setAttribute('target','_blank');
			portalA.innerHTML = data.Title;
			portalLI.appendChild(portalA);
			$("#intranet-sites-list")[0].appendChild(portalLI);

			});
        }
        catch (e) {
            //console.log(e);
        }
}

//Commenting bindClickOnLike: TG
/* function bindClickOnLike(){
$(".ms-comm-forumCmdList").click(function(){
var anchor = $($($(this).children()[0]).find("a"));
var anchorText = anchor.text();
if(anchorText.toLowerCase() == "like"){
setTimeout(function(){
var id = anchor.attr("id");
$("#"+id).addClass("active");
},1000);
}
if(anchorText.toLowerCase() == "unlike"){
setTimeout(function(){
var id = anchor.attr("id");
$("#"+id).removeClass("active");
},1000);
}
});
} */
//Commenting setAjaxClickforLike: TG
/* function setAjaxClickforLike()
{
$(".ms-pivotControl-container a").click(function (event) {
setTimeout(function () {setLike();}, 1000);
});
} */
//Commenting SetLike: TG
/* function setLike(){
$(".ms-secondaryCommandLink").each(function(){
var liketText = $(this).text();
if(liketText.toLowerCase() == "unlike"){
$(this).addClass("active");
}
});
setAjaxClickforLike();
} */


//Get Local Storage key Suffix
function GetLocalStorageSuffixKey(localStorageKey)
{
	if(!StorageSuffixKey)
	{
		StorageSuffixKey = _spPageContextInfo.siteServerRelativeUrl.replace(/\//g,"_");
	}
	return localStorageKey + StorageSuffixKey;
}

/*This is created to wait for injector.js loading */
function AskQuestionAfterInjectorLoad() {
    $(".kw-iframe-container").html("");
    $("#kw-widget").attr("data-sitemap", "/homepage");
    $("#kw-widget").insertAfter("#ask-Question");
    $(".kw-button").hide();
    $("#kw-widget").show();
    $(".kw-popover").show();
    $(".kw-button").click();
}

function AskQuestion() {
    var kwWidget = '<div id="kw-widget" class="kw-container"><div class="kw-button"></div><div class="kw-popover"><div class="kw-popover-content"><div class="kw-iframe-container"><!-- iframe will be injected here when icon is clicked --></div></div></div>';
    var parentLi = $("span:contains('Ask A Question')")[0].parentNode;
    parentLi.className = "static askQuestion";
    parentLi.href = "#";
    parentLi.id = "ask-Question"
    $("span:contains('Ask A Question')")[0].innerHTML = '<span class="menu-item-text">Ask A Question<small class="greenroom-topnav-icon-kaleo askQuestion sprite-new"></small></span>';
    $("#kw-widget").insertAfter("#ask-Question");
    /*InjectIFrame(window,document,'https://viacom.kaleosoftware.com/assets/v4/widgets/injector.js');*/
}
function InjectIFrame(w, d, u) {
    a = d.createElement('script'),
	m = d.getElementsByTagName('script')[0];
    a.async = 0;
    a.src = u;
    m.parentNode.insertBefore(a, m)
}
function hideshowQuickLinksPanel() {
    $("#s4-workspace").animate({
        scrollTop: 0
    }, 500, function () {
        //	$("#QuickLinksPlaceholder").slideToggle();
    });

    //$("#QuickLinksPlaceholder").toggle("slow");
    //Removing Recommended Links
    /*
	var recomendedLinksLength = $('.RecomendedLinks-homepage ul').find('li').length;
	var quickLinksLength = $('#QuickLinksContainer ul').find('li').length;
	var loopStart = 12 - quickLinksLength;
	for (var i = recomendedLinksLength; i > loopStart - 1; i--) {
	$('.RecomendedLinks-homepage ul li').eq(i).remove();
	}
	 */
}

/****************************************************************/

/*----------------------------------------------------------------------------------------------------------------*/
/*------------------------------------ DYNAMIC 'LIKE' FUNCTIONALITY ----------------------------------------------*/

/*----------------------------------------------------------------------------------------------------------------*/
/*---------------------------DISPLAY TEMPLATE CODE FOR RELATED ARTICLES ( BOTTOM )--------------------------------*/

function DisplayTemplateGetLikeCount(relativeURL, listID, listItemID, sitename, path, formatlistID) {
    getSubsiteColorRASection(listID, listItemID, path, 'RASection', relativeURL, sitename, formatlistID, 'DisplayTemplateGetLikeCount');
}

function NewsDisplayTemplateGetLikeCount(relativeURL, listID, listItemID, sitename, path, formatlistID, region) {
    if (region.value != undefined) {
        region = region.value;
    } else {
        region = region;
    }
    getSubsiteColorNewsSection(listID, listItemID, path, 'RASection', relativeURL, sitename, formatlistID, 'NewsDisplayTemplateGetLikeCount', region);
}

function DisplayTemplateGetLikeCountRASection(relativeURL, listItemID, sitename, formatlistID) {
    var contextURL = relativeURL.toString().split('/Pages')[0];

    var context = new SP.ClientContext(contextURL);
    var list = context.get_web().get_lists().getByTitle('Pages');
    var item = list.getItemById(listItemID);

    context.load(item, "LikedBy", "ID", "LikesCount");
    context.executeQueryAsync(Function.createDelegate(this, function (success) {
        // Check if the user id of the current users is in the collection LikedBy.
        var likeDisplay = true;
        var $v_0 = item.get_item('LikedBy');
        var itemc = item.get_item('LikesCount');
        if (!SP.ScriptHelpers.isNullOrUndefined($v_0)) {
            for (var $v_1 = 0, $v_2 = $v_0.length; $v_1 < $v_2; $v_1++) {
                var $v_3 = $v_0[$v_1];
                if ($v_3.$1E_1 === _spPageContextInfo.userId) {
                    //cb(true, item.get_item('LikesCount'));
                    //alert("Liked by me");
                    likeDisplay = false;
                }
            }
        }
        DTChangeLikeText(likeDisplay, itemc, sitename, listItemID, formatlistID);

    }), Function.createDelegate(this, function (sender, args) {
        //console.log(args.get_message());
    }));
}

function DisplayTemplateGetLikeCountNewsSection(relativeURL, listItemID, sitename, formatlistID) {
    var contextURL = relativeURL.toString().split('/Pages')[0];

    var context = new SP.ClientContext(contextURL);
    var list = context.get_web().get_lists().getByTitle('Pages');
    var item = list.getItemById(listItemID);
    var likeContextUser = context.get_web().get_currentUser();
    context.load(likeContextUser);
    context.load(item, "LikedBy", "ID", "LikesCount");
    context.executeQueryAsync(Function.createDelegate(this, function (success) {
        // Check if the user id of the current users is in the collection LikedBy.
        var likeDisplay = true;
        var likedContextUserId = likeContextUser.get_id();
        var $v_0 = item.get_item('LikedBy');
        var itemc = item.get_item('LikesCount');
        if (!SP.ScriptHelpers.isNullOrUndefined($v_0)) {
            for (var $v_1 = 0, $v_2 = $v_0.length; $v_1 < $v_2; $v_1++) {
                var $v_3 = $v_0[$v_1];
                if ($v_3.$1E_1 === likedContextUserId) {
                    //cb(true, item.get_item('LikesCount'));
                    //alert("Liked by me");
                    likeDisplay = false;
                }
            }
        }
        DTChangeLikeText(likeDisplay, itemc, sitename, listItemID, formatlistID);

    }), Function.createDelegate(this, function (sender, args) {
        //console.log(args.get_message());
    }));
}

function GetConfigListDataJson() {
    var retrievedObject = localStorage.getItem(GetLocalStorageSuffixKey('ConfigListDataJson'));
    ConfigListData = JSON.parse(retrievedObject);
    var callConfig = $.ajax({
        url: _spPageContextInfo.siteAbsoluteUrl + "/_api/lists/getbytitle('config')/items?" +
        "&$select=Title,sitepath,FetchProfileProperties",
        type: "GET",
        dataType: "json",
        headers: {
            Accept: "application/json;odata=verbose"
        }
    });
    callConfig.done(function (data, textStatus, jqXHR) {
        try {
            ConfigListData = data.d;
            localStorage.setItem(GetLocalStorageSuffixKey('ConfigListDataJson'), JSON.stringify(ConfigListData));
            SetWorkPlaceLogo(ConfigListData);
        } catch (e) {
            //console.log(e);
        }
    });
    callConfig.fail(function (jqXHR, textStatus, errorThrown) {
        localStorage.setItem(GetLocalStorageSuffixKey('ConfigListDataJson'), '{"results":[]}');
        //console.log("Error getting Site Color" + jqXHR.responseText);
    });
}
function SetWorkPlaceLogo(configListData) {
    if (configListData != null && configListData.results != null && configListData.results.length > 0) {
        for (var congigItemInex = 0; congigItemInex < ConfigListData.results.length; congigItemInex++) {
            var itemTitle = ConfigListData.results[congigItemInex].Title;
            if (itemTitle.toLowerCase() === "workplacelink") {
                var workplace = ConfigListData.results[congigItemInex];
                var workplaceLink = workplace.FetchProfileProperties;
                if (workplaceLink) {
                    $(".workspace-links-quickLinks").html(workplaceLink);
                }
                break;
            }

        }
    }
}
function GetSubSiteColorsListDataJson() {
    var subsiteColorsRetrievedObject = localStorage.getItem(GetLocalStorageSuffixKey('SubSiteColorDataJson'));
    SubSiteColorsListData = JSON.parse(subsiteColorsRetrievedObject);
    var subSiteColorsCallConfig = $.ajax({
        url: _spPageContextInfo.siteAbsoluteUrl + "/_api/lists/getbytitle('SubsiteColors')/items?" +
        "&$select=Title,sitepath,color,rollover",
        type: "GET",
        dataType: "json",
        headers: {
            Accept: "application/json;odata=verbose"
        }
    });
    subSiteColorsCallConfig.done(function (data, textStatus, jqXHR) {
        try {
            SubSiteColorsListData = data.d;
            localStorage.setItem(GetLocalStorageSuffixKey('SubSiteColorDataJson'), JSON.stringify(SubSiteColorsListData));
        } catch (e) { }
    });
    subSiteColorsCallConfig.fail(function (jqXHR, textStatus, errorThrown) {
        localStorage.setItem(GetLocalStorageSuffixKey('SubSiteColorDataJson'), '{"results":[]}');
    });
}

function GetUserProfileUrlFromLocalStarageJson() {
    var retrievedObject = localStorage.getItem(GetLocalStorageSuffixKey('ConfigListDataJson'));
    ConfigListData = JSON.parse(retrievedObject);
    if (ConfigListData != null && ConfigListData.results != null && ConfigListData.results.length > 0) {
        try {
            for (var congigItemInex = 0; congigItemInex < ConfigListData.results.length; congigItemInex++) {
                var ItemTitle = ConfigListData.results[congigItemInex].Title;
                if (ItemTitle.toLowerCase() === "userprofileserviceurl") {
                    return ConfigListData.results[congigItemInex].sitepath;
                }

            }

        } catch (e) {
            //console.log(e);
        }
    }
}
function GetMySiteUrlFromLocalStarageJson() {
    var retrievedObject = localStorage.getItem(GetLocalStorageSuffixKey('ConfigListDataJson'));
    ConfigListData = JSON.parse(retrievedObject);
    if (ConfigListData != null && ConfigListData.results != null && ConfigListData.results.length > 0) {
        try {
            for (var congigItemInex = 0; congigItemInex < ConfigListData.results.length; congigItemInex++) {
                var ItemTitle = ConfigListData.results[congigItemInex].Title;
                if (ItemTitle.toLowerCase() === "mysiteurl") {
                    return ConfigListData.results[congigItemInex].sitepath;
                }

            }

        } catch (e) {
            //console.log(e);
        }
    }
}
var CurrentUserInfo = {
    clientContext: null,
    web: null,
    LoginName: null,
    GetCurrentUserPropertiesFromProfileJson: function () {
        CurrentUserInfo.clientContext = new SP.ClientContext.get_current();
        CurrentUserInfo.web = CurrentUserInfo.clientContext.get_web();
        currentUser = CurrentUserInfo.web.get_currentUser();
        CurrentUserInfo.clientContext.load(currentUser);
        CurrentUserInfo.clientContext.executeQueryAsync(CurrentUserInfo.currentUserSuccess, CurrentUserInfo.failure);
    },
    currentUserSuccess: function () {
        LoginName = currentUser.get_loginName();
        LoginName = LoginName.split('|')[1];
        $.ajax({
            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/SP.UserProfiles.PeopleManager/GetPropertiesFor(accountName=@v)?@v='" + LoginName + "'",
            headers: {
                Accept: "application/json;odata=verbose"
            },
            success: function (data, textStatus, jqXHR) {
                var getUserPropertiesFromProfileJsonVar = data.d.UserProfileProperties;
                localStorage.setItem('CurrentUserPropertiesFromProfileJson', JSON.stringify(getUserPropertiesFromProfileJsonVar));
            },
            error: function (jQxhr, errorCode, errorThrown) {
                localStorage.setItem('CurrentUserPropertiesFromProfileJson', '{"results":[]}');
                //console.log(errorThrown);
            }
        });
    },
    failure: function (sender, args) {
        //console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
    }
}
/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

function DTChangeLikeText(like, count, sitename, listItemID, formatlistID) {

    /*----------------------WORKING CODE FOR LIKES with TEXT---------------------------
	if (like) {
	$(".DTLikeButton"+formatlistID+listItemID).text('Like');
	}
	else {
	$(".DTLikeButton"+formatlistID+listItemID).text('Unlike');
	}
	var htmlstring = String(count) + ' ' +'<img alt="" src="/SiteCollectionImages/pagelayouts/greenroom-tile-like-active.png" />';
	if (count > 0)
	$(".DTlikecount"+formatlistID+listItemID).html(htmlstring)
	else
	$(".DTlikecount"+formatlistID+listItemID).html("");

	---------------------------------------------------------------------------------*/

    if (like) {
        $(".DTLikeButton" + formatlistID + listItemID).html('<div id="cswp-like-text">Like</div>');
        $(".BGC" + formatlistID + "Likes" + listItemID).removeClass("activestate");
        $(".DTLikeButton" + formatlistID + listItemID).parent().parent().removeClass("activestate");
    } else {
        $(".DTLikeButton" + formatlistID + listItemID).html('<div id="cswp-like-text">Unlike</div>');
        $(".BGC" + formatlistID + "Likes" + listItemID).addClass("activestate");
        $(".DTLikeButton" + formatlistID + listItemID).parent().parent().addClass("activestate");
    }
    var htmlstring = String(count) + ' ' + '<img alt="" src="/SiteCollectionImages/pagelayouts/greenroom-tile-like.png" />';
    if (count > 0) {
        $(".DTlikecount" + formatlistID + listItemID).html(htmlstring);
    } else {
        $(".DTlikecount" + formatlistID + listItemID).html(String(0) + ' ' + '<img alt="" src="/SiteCollectionImages/pagelayouts/greenroom-tile-like.png" />');
    }
}

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

function DTLikePage(relativeURL, listID, listItemID, sitename, path, formatlistID, region) {

    var contextURL = relativeURL.toString().split('/Pages')[0];

    var like = false;
    //var likeButtonText = $(".DTLikeButton"+formatlistID+listItemID).text();	Working Like functionality with Text as 'Like'/'Unlike'...
    var likeButtonText = $(".DTLikeButton" + formatlistID + listItemID + " div").text();

    if (likeButtonText != "") {
        if (likeButtonText == "Like")
            like = true;

        var aContextObject = new SP.ClientContext(contextURL);
        EnsureScriptFunc('reputation.js', 'Microsoft.Office.Server.ReputationModel.Reputation', function () {
            Microsoft.Office.Server.ReputationModel.
			Reputation.setLike(aContextObject,
				listID.substring(1, 37),
				listItemID, like);

            aContextObject.executeQueryAsync(
				function () {
				    //alert(String(like));
				    NewsDisplayTemplateGetLikeCount(relativeURL, listID, listItemID, sitename, path, formatlistID, region);
				},
				function (sender, args) {
				    //alert('F0');
				});
        });
    }
    var ev = (this.event) ? this.event : this.Event;
    (ev.preventDefault) ? ev.preventDefault() : ev.returnValue = false;
}

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------------------------------------------*/
/*-----------------------DISPLAY TEMPLATE CODE FOR SIDEBAR RELATED ARTICLES ( RIGHT SIDE )------------------------*/

function SBDisplayTemplateGetLikeCount(relativeURL, listID, listItemID, sitename, path, formatlistID, section) {
    getSubsiteColorRASection(listID, listItemID, path, section, relativeURL, sitename, formatlistID, 'SBDisplayTemplateGetLikeCount');
}
/*konda*/
function RegionSBDisplayTemplateGetLikeCount(relativeURL, listID, listItemID, sitename, path, formatlistID, section, region) {
    regionGetSubsiteColorRASection(listID, listItemID, path, section, relativeURL, sitename, formatlistID, 'RegionSBDisplayTemplateGetLikeCount', region.value);
}

/*Get subsite Color for related article section */
function regionGetSubsiteColorRASection(listID, listItemID, path, section, relativeURL, sitename, formatlistID, callingMethod, region) {
    var getformatlistID = listID.toString().substring(1, 37);

    var filter;
    var parentsite = '';
    var urlParts = path.split('/');

    var firstSiteTitle = path.split('/')[3]; //.toUpperCase()
    if (firstSiteTitle == 'PAGES') {
        for (var i = 2; i < 3; i++) {
            parentsite += urlParts[i] + '/';
        }

        filter = "sitepath eq '" + parentsite + "'";
    } else {
        for (var i = 2; i < 4; i++) {
            parentsite += urlParts[i] + '/';
        }

        filter = "substringof('" + parentsite + "',sitepath)";
    }
    var defaultSubsiteColor = '';
    var defaultLikesColor = '';
    var retrievedObject = localStorage.getItem(GetLocalStorageSuffixKey('SubSiteColorDataJson'));
    ConfigListData = JSON.parse(retrievedObject);
    if (ConfigListData != null && ConfigListData.results != null && ConfigListData.results.length > 0) {
        try {
            var colorFound = false;
            var defaultColorFound = false;
            for (var congigItemInex = 0; congigItemInex < ConfigListData.results.length; congigItemInex++) {

                var SubsiteColor = ConfigListData.results[congigItemInex].color;
                var LikesColor = ConfigListData.results[congigItemInex].rollover;
                var ParentSiteTitle = ConfigListData.results[congigItemInex].Title;
                var Parentsitepath = ConfigListData.results[congigItemInex].sitepath;
                if (ParentSiteTitle === "DEFAULT") {
                    defaultSubsiteColor = ConfigListData.results[congigItemInex].color;
                    defaultLikesColor = ConfigListData.results[congigItemInex].rollover;
                    defaultColorFound = true;
                }

                if (ParentSiteTitle.toUpperCase() == region.toUpperCase()) {
                    colorFound = true;
                    if (section == 'RASection') {
                        $('.BGC' + getformatlistID + listItemID).css('background-color', SubsiteColor);
                        $('.BGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>');

                        $('.BGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                    } else if (section == 'FeaturingNow') {
                        if ($(window).width() > 1024) {
                            $('.FN' + getformatlistID + 'Title' + listItemID + '0').css('background-color', SubsiteColor);
                            $('.FN' + getformatlistID + 'Title' + listItemID + '0').html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>');

                            $('.FN' + getformatlistID + 'Title' + listItemID + '1').css('background-color', SubsiteColor);
                            $('.FN' + getformatlistID + 'Title' + listItemID + '1').html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>');

                            $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                            //Fix to fill in like row with section color for tiny tiles
                            $('.SBBGC' + getformatlistID + 'Likes' + listItemID).parent().css('background-color', SubsiteColor);
                            //Fix to display site title from the Config list
                            var tinyTileTitle = $('.SBBGC' + getformatlistID + 'Likes' + listItemID).parent().parent().find("#cswp-tile-subsite-tiny");
                            if (tinyTileTitle != null && tinyTileTitle != undefined && tinyTileTitle.length > 0) {
                                tinyTileTitle[0].textContent = ParentSiteTitle;
                            }
                            $('.FMRT' + getformatlistID + 'Title' + listItemID).html(ParentSiteTitle);

                        } else {
                            $('.FN' + getformatlistID + 'Title' + listItemID).css('background-color', SubsiteColor);
                            $('.FN' + getformatlistID + 'Title' + listItemID).html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>');

                            $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                        }

                    } else {
                        $('.SBBGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>');
                        //Fix to fill in like row with section color for tiny tiles
                        $('.SBBGC' + getformatlistID + 'Likes' + listItemID).parent().css('background-color', SubsiteColor);
                        $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                    }
                    //Exit loop once desired operation complete
                    if (defaultColorFound == true) {
                        break;
                    }
                }

            }
            if (colorFound == false) {
                if (section == 'RASection') {
                    $('.BGC' + getformatlistID + listItemID).css('background-color', defaultSubsiteColor);
                    $('.BGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + path + '">' + region + '</a>');

                    $('.BGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                } else if (section == 'FeaturingNow') {
                    if ($(window).width() > 1024) {
                        $('.FN' + getformatlistID + 'Title' + listItemID + '0').css('background-color', defaultSubsiteColor);
                        $('.FN' + getformatlistID + 'Title' + listItemID + '0').html('<a class="cswp-tile-subsite-link" href="' + path + '">' + region + '</a>');

                        $('.FN' + getformatlistID + 'Title' + listItemID + '1').css('background-color', defaultSubsiteColor);
                        $('.FN' + getformatlistID + 'Title' + listItemID + '1').html('<a class="cswp-tile-subsite-link" href="' + path + '">' + region + '</a>');

                        $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                        $('.SBBGC' + getformatlistID + 'Likes' + listItemID).parent().css('background-color', defaultSubsiteColor);
                    } else {
                        $('.FN' + getformatlistID + 'Title' + listItemID).css('background-color', defaultSubsiteColor);
                        $('.FN' + getformatlistID + 'Title' + listItemID).html('<a class="cswp-tile-subsite-link" href="' + path + '">' + region + '</a>');

                        $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                        $('.SBBGC' + getformatlistID + 'Likes' + listItemID).parent().css('background-color', defaultSubsiteColor);
                    }
                } else {
                    $('.SBBGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + path + '">' + region + '</a>');

                    $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                }
            }
            if (callingMethod == 'DisplayTemplateGetLikeCount') {
                DisplayTemplateGetLikeCountRASection(relativeURL, listItemID, sitename, formatlistID);
            }
            if (callingMethod == 'RegionSBDisplayTemplateGetLikeCount') {
                SBDisplayTemplateGetLikeCountSction(relativeURL, listItemID, sitename, formatlistID);
            }
            if (callingMethod == 'CalendarAgendaDisplayTemplateGetLikeCount') {
                CalendarAgendaDisplayTemplateGetLikeCountSction(relativeURL, listItemID, sitename, formatlistID);
            }
        } catch (e) {
            //console.log(e);
        }
    } else {
        var callConfig = $.ajax({
            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/lists/getbytitle('SubsiteColors')/items?" +
            "&$select=Title,sitepath,color,rollover",
            type: "GET",
            dataType: "json",
            headers: {
                Accept: "application/json;odata=verbose"
            }
        });
        callConfig.done(function (data, textStatus, jqXHR) {
            try {
                var colorFound = false;
                var defaultColorFound = false;
                ConfigListData = data.d;
                localStorage.setItem(GetLocalStorageSuffixKey('SubSiteColorDataJson'), JSON.stringify(ConfigListData));
                if (ConfigListData.results != null) {
                    for (var congigItemInex = 0; congigItemInex < ConfigListData.results.length; congigItemInex++) {
                        var SubsiteColor = ConfigListData.results[congigItemInex].color;
                        var LikesColor = ConfigListData.results[congigItemInex].rollover;
                        var ParentSiteTitle = ConfigListData.results[congigItemInex].Title;
                        var Parentsitepath = ConfigListData.results[congigItemInex].sitepath;
                        if (ParentSiteTitle === "DEFAULT") {
                            defaultSubsiteColor = ConfigListData.results[congigItemInex].color;
                            defaultLikesColor = ConfigListData.results[congigItemInex].rollover;
                            defaultColorFound = true;
                        }

                        if (ParentSiteTitle.toUpperCase() == region.toUpperCase()) {
                            colorFound = true;
                            if (section == 'RASection') {
                                $('.BGC' + getformatlistID + listItemID).css('background-color', SubsiteColor);
                                $('.BGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>');

                                $('.BGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                            } else if (section == 'FeaturingNow') {
                                $('.FN' + getformatlistID + 'Title' + listItemID + '0').css('background-color', SubsiteColor);
                                /* $('.FN' + getformatlistID + 'Title' + listItemID + '0').html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>'); */

                                $('.FN' + getformatlistID + 'Title' + listItemID + '1').css('background-color', SubsiteColor);
                                /* 	$('.FN' + getformatlistID + 'Title' + listItemID + '1').html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>'); */

                                $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                                $('.SBBGC' + getformatlistID + 'Likes' + listItemID).parent().css('background-color', SubsiteColor);
                            } else {
                                $('.SBBGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>');

                                $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                            }
                            //Exit loop once desired operation complete
                            if (defaultColorFound == true) {
                                break;
                            }
                        }
                    }
                    if (colorFound == false) {
                        if (section == 'RASection') {
                            $('.BGC' + getformatlistID + listItemID).css('background-color', defaultSubsiteColor);
                            $('.BGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + path + '">' + region + '</a>');

                            $('.BGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                        } else if (section == 'FeaturingNow') {
                            $('.FN' + getformatlistID + 'Title' + listItemID + '0').css('background-color', defaultSubsiteColor);
                            /* 	$('.FN' + getformatlistID + 'Title' + listItemID + '0').html('<a class="cswp-tile-subsite-link" href="' + path + '">' + region + '</a>'); */

                            $('.FN' + getformatlistID + 'Title' + listItemID + '1').css('background-color', defaultSubsiteColor);
                            /* $('.FN' + getformatlistID + 'Title' + listItemID + '1').html('<a class="cswp-tile-subsite-link" href="' + path + '">' + region + '</a>'); */

                            $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                            $('.SBBGC' + getformatlistID + 'Likes' + listItemID).parent().css('background-color', defaultSubsiteColor);
                        } else {
                            $('.SBBGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + path + '">' + region + '</a>');

                            $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                        }
                    }
                }
                if (callingMethod == 'DisplayTemplateGetLikeCount') {
                    DisplayTemplateGetLikeCountRASection(relativeURL, listItemID, sitename, formatlistID);
                }
                if (callingMethod == 'RegionSBDisplayTemplateGetLikeCount') {
                    SBDisplayTemplateGetLikeCountSction(relativeURL, listItemID, sitename, formatlistID);
                }
                if (callingMethod == 'CalendarAgendaDisplayTemplateGetLikeCount') {
                    CalendarAgendaDisplayTemplateGetLikeCountSction(relativeURL, listItemID, sitename, formatlistID);
                }
            } catch (e) {
                //console.log(e);
            }
        });
        callConfig.fail(function (jqXHR, textStatus, errorThrown) {
            //console.log("Error getting Site Color" + jqXHR.responseText);
        });
    }

}

/*End Konda*/
function SBDisplayTemplateGetLikeCountSction(relativeURL, listItemID, sitename, formatlistID) {
    var contextURL = relativeURL.toString().split('/Pages')[0];

    var context = new SP.ClientContext(contextURL);
    var list = context.get_web().get_lists().getByTitle('Pages');
    var item = list.getItemById(listItemID);
    var likeContextUser = context.get_web().get_currentUser();
    context.load(likeContextUser);
    context.load(item, "LikedBy", "ID", "LikesCount");
    context.executeQueryAsync(Function.createDelegate(this, function (success) {
        // Check if the user id of the current users is in the collection LikedBy.
        var likeDisplay = true;
        var likedContextUserId = likeContextUser.get_id();
        var $v_0 = item.get_item('LikedBy');
        var itemc = item.get_item('LikesCount');
        if (itemc == null || itemc == undefined || itemc == "undefined") {
            itemc = 0;
        }
        if (!SP.ScriptHelpers.isNullOrUndefined($v_0)) {
            for (var $v_1 = 0, $v_2 = $v_0.length; $v_1 < $v_2; $v_1++) {
                var $v_3 = $v_0[$v_1];
                if ($v_3.$1E_1 === likedContextUserId) {
                    //cb(true, item.get_item('LikesCount'));
                    //alert("Liked by me");
                    likeDisplay = false;
                }
            }
        }
        SBDTChangeLikeText(likeDisplay, itemc, sitename, listItemID, formatlistID);

    }), Function.createDelegate(this, function (sender, args) {
        //console.log(args.get_message());
    }));
}
/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

function SBDTChangeLikeText(like, count, sitename, listItemID, formatlistID) {
    /*----------------------WORKING CODE FOR LIKES with TEXT---------------------------

	if (like) {
	$(".SBDTLikeButton"+formatlistID+listItemID).text('Like');
	}
	else {
	$(".SBDTLikeButton"+formatlistID+listItemID).text('Unlike');
	}
	var htmlstring = String(count) + ' ' +'<img alt="" src="/SiteCollectionImages/pagelayouts/greenroom-count-likes.png" />';
	if (count > 0)
	$(".SBDTlikecount"+formatlistID+listItemID).html(htmlstring)
	else
	$(".SBDTlikecount"+formatlistID+listItemID).html("");

	---------------------------------------------------------------------------------*/
    var htmlstring = String(count) + ' ' + '<img alt="" src="/SiteCollectionImages/pagelayouts/greenroom-tile-like.png" />';

    $(".SBDTlikecount" + formatlistID + listItemID).html(htmlstring);

    if (like) {
        $(".SBDTLikeButton" + formatlistID + listItemID).html('<div id="cswp-like-text">Like</div>');
        $(".SBBGC" + formatlistID + "Likes" + listItemID).removeClass("activestate");
        $(".SBDTLikeButton" + formatlistID + listItemID).parent().parent().removeClass("activestate");
    } else {
        $(".SBDTLikeButton" + formatlistID + listItemID).html('<div id="cswp-like-text">Unlike</div>');
        $(".SBBGC" + formatlistID + "Likes" + listItemID).addClass("activestate");
        $(".SBDTLikeButton" + formatlistID + listItemID).parent().parent().addClass("activestate");

    }

    //if (like){
    //    $(".SBDTlikecount"+formatlistID+listItemID).html(htmlstring);
    //}
    //else{
    //    $(".SBDTlikecount"+formatlistID+listItemID).html(String(0) + ' ' + '<img alt="" src="/SiteCollectionImages/pagelayouts/greenroom-tile-like.png" />');
    //}
}

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

function SBDTLikePage(relativeURL, listID, listItemID, sitename, path, formatlistID) {

    var contextURL = relativeURL.toString().split('/Pages')[0];

    var like = false;
    //var likeButtonText = $(".SBDTLikeButton"+formatlistID+listItemID).text();    Working Like functionality with Text as 'Like'/'Unlike'...
    var likeButtonText = $(".SBDTLikeButton" + formatlistID + listItemID + " div").text();
    if (likeButtonText != "") {
        if (likeButtonText == "Like" || likeButtonText == "LikeLike" || likeButtonText == "LikeLikeLike")
            like = true;

        var aContextObject = new SP.ClientContext(contextURL);
        EnsureScriptFunc('reputation.js', 'Microsoft.Office.Server.ReputationModel.Reputation', function () {
            Microsoft.Office.Server.ReputationModel.
			Reputation.setLike(aContextObject,
				listID.substring(1, 37),
				listItemID, like);

            aContextObject.executeQueryAsync(
				function () {
				    //alert(String(like));
				    SBDisplayTemplateGetLikeCount(relativeURL, listID, listItemID, sitename, path, formatlistID);
				},
				function (sender, args) {
				    //alert('F0');
				});
        });
    }
}

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------------------------------------------*/
/*--------------------------------- DISPLAY TEMPLATE CODE FOR CALENDAR AGENDA ------------------------------------*/

function CalendarAgendaDisplayTemplateGetLikeCount(relativeURL, listID, listItemID, sitename, path, formatlistID, section) {
    getSubsiteColorRASection(listID, listItemID, path, section, relativeURL, sitename, formatlistID, 'CalendarAgendaDisplayTemplateGetLikeCount');

}
function CalendarAgendaDisplayTemplateGetLikeCountSction(relativeURL, listItemID, sitename, formatlistID) {
    var contextURL = relativeURL.toString().split('/Lists')[0];

    var context = new SP.ClientContext(contextURL);
    var list = context.get_web().get_lists().getByTitle('cal');
    var item = list.getItemById(listItemID);

    context.load(item, "LikedBy", "ID", "LikesCount");
    context.executeQueryAsync(Function.createDelegate(this, function (success) {
        // Check if the user id of the current users is in the collection LikedBy.
        var likeDisplay = true;
        var $v_0 = item.get_item('LikedBy');
        var itemc = item.get_item('LikesCount');
        if (itemc == null || itemc == undefined || itemc == "undefined") {
            itemc = 0;
        }
        if (!SP.ScriptHelpers.isNullOrUndefined($v_0)) {
            for (var $v_1 = 0, $v_2 = $v_0.length; $v_1 < $v_2; $v_1++) {
                var $v_3 = $v_0[$v_1];
                if ($v_3.$1E_1 === _spPageContextInfo.userId) {
                    //cb(true, item.get_item('LikesCount'));
                    //alert("Liked by me");
                    likeDisplay = false;
                }
            }
        }
        CalendarAgendaChangeLikeText(likeDisplay, itemc, sitename, listItemID, formatlistID);

    }), Function.createDelegate(this, function (sender, args) {
        //console.log(args.get_message());
    }));
}
/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

function CalendarAgendaChangeLikeText(like, count, sitename, listItemID, formatlistID) {
    var htmlstring = String(count) + ' ' + '<img alt="" src="/SiteCollectionImages/pagelayouts/greenroom-tile-like.png" />';

    $(".SBDTlikecount" + formatlistID + listItemID).html(htmlstring);

    if (like) {
        $(".SBDTLikeButton" + formatlistID + listItemID).html('<div id="cswp-like-text">Like</div>');
        $(".SBBGC" + formatlistID + "Likes" + listItemID).removeClass("activestate");
        $(".SBDTLikeButton" + formatlistID + listItemID).parent().parent().removeClass("activestate");
    } else {
        $(".SBDTLikeButton" + formatlistID + listItemID).html('<div id="cswp-like-text">Unlike</div>');
        $(".SBBGC" + formatlistID + "Likes" + listItemID).addClass("activestate");
        $(".SBDTLikeButton" + formatlistID + listItemID).parent().parent().addClass("activestate");
    }
}

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

function CalendarAgendaLikeEvent(relativeURL, listID, listItemID, sitename, path, formatlistID) {

    var contextURL = relativeURL.toString().split('/Lists')[0];
    var like = false;
    //var likeButtonText = $(".SBDTLikeButton"+formatlistID+listItemID).text();    Working Like functionality with Text as 'Like'/'Unlike'...
    var likeButtonText = $(".SBDTLikeButton" + formatlistID + listItemID + " div").text();
    if (likeButtonText != "") {
        if (likeButtonText == "Like" || likeButtonText == "LikeLike" || likeButtonText == "LikeLikeLike")
            like = true;

        var aContextObject = new SP.ClientContext(contextURL);
        EnsureScriptFunc('reputation.js', 'Microsoft.Office.Server.ReputationModel.Reputation', function () {
            Microsoft.Office.Server.ReputationModel.
			Reputation.setLike(aContextObject,
				listID.substring(1, 37),
				listItemID, like);

            aContextObject.executeQueryAsync(
				function () {
				    //alert(String(like));
				    CalendarAgendaTaggedUrl(listItemID, formatlistID, like);
				    CalendarAgendaDisplayTemplateGetLikeCount(relativeURL, listID, listItemID, sitename, path, formatlistID);
				},
				function (sender, args) {
				    //alert('F0');
				});
        });
    }
}
//------------------------------------Liking the page in the Pages library----------------------------//
function CalendarAgendaTaggedUrl(listItemID, formatlistID, like) {
    var call = $.ajax({
        url: _spPageContextInfo.webAbsoluteUrl + "/_api/lists/getbytitle('cal')/items?" +
        "$select=Title,TaggedUrl,ID" +
        "&$filter=(ID eq '" + listItemID + "')",
        type: "GET",
        async: false,
        dataType: "json",
        headers: {
            Accept: "application/json;odata=verbose"
        }
    });

    call.done(function (data, textStatus, jqXHR) {
        try {
            var taggedUrl = data.d.results[0].TaggedUrl;
            CalendarAgendaPageName(taggedUrl, formatlistID, listItemID, like);
        } catch (e) { }
    });

    call.fail(function (jqXHR, textStatus, errorThrown) {
        //alert("Error generating Report: " + jqXHR.responseText);
    });

}

function CalendarAgendaPageName(taggedUrl, formatlistID, listItemID, like) {
    var formatLikedPage = taggedUrl.split('/Pages/')[1];
    formatLikedPage = _spPageContextInfo.siteAbsoluteUrl + _spPageContextInfo.webServerRelativeUrl + '/Pages/' + formatLikedPage + '.aspx';
    $.ajax({
        url: _spPageContextInfo.siteAbsoluteUrl + _spPageContextInfo.webServerRelativeUrl + "/_api/lists/getbytitle('Pages')/items?" +
		"$select=ID,Title,EncodedAbsUrl",
        type: "GET",
        async: false,
        dataType: "json",
        headers: {
            Accept: "application/json;odata=verbose"
        },
        success: function (data) {
            var dataResults = data.d.results;
            for (var i = 0; i < dataResults.length; i++) {
                if (formatLikedPage == dataResults[i].EncodedAbsUrl) {
                    var itemID = dataResults[i].ID;
                    var aContextObjectChange = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl + _spPageContextInfo.webServerRelativeUrl);
                    EnsureScriptFunc('reputation.js', 'Microsoft.Office.Server.ReputationModel.Reputation', function () {
                        Microsoft.Office.Server.ReputationModel.
						Reputation.setLike(aContextObjectChange,
							_spPageContextInfo.pageListId.substring(1, 37),
							itemID, like);
                        aContextObjectChange.executeQueryAsync(
							function () {
							    //alert(String(like));
							    //alert("liked");
							},
							function (sender, args) {
							    //alert('F0');
							});
                    });

                }
            }

        },
        error: function (jQxhr, errorCode, errorThrown) {
            /*ADFS Alert Issue*/console.log(errorThrown);
        }

    });

}
//------------------------------------Liking the page in the Pages library Ends----------------------------//


/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------CODE TO FETCH COLORs OF SITE/SUBSITES ('SubsiteColors' List)-------------------------------*/

/*
var arrayColors = [];

function sortFunction(a, b) {
if (a[0] === b[0]) {
return 0;
}
else {
return (a[0] < b[0]) ? -1 : 1;
}
}
 */

/*Get subsite Color for related article section */
function getSubsiteColorRASection(listID, listItemID, path, section, relativeURL, sitename, formatlistID, callingMethod) {
    var getformatlistID = listID.toString().substring(1, 37);

    var filter;
    var parentsite = '';
    var urlParts = path.split('/');

    var firstSiteTitle = path.split('/')[3]; //.toUpperCase()
    if (firstSiteTitle == 'PAGES') {
        for (var i = 2; i < 3; i++) {
            parentsite += urlParts[i] + '/';
        }

        filter = "sitepath eq '" + parentsite + "'";
    } else {
        for (var i = 2; i < 4; i++) {
            parentsite += urlParts[i] + '/';
        }

        filter = "substringof('" + parentsite + "',sitepath)";
    }
    var defaultSubsiteColor = '';
    var defaultLikesColor = '';
    var retrievedObject = localStorage.getItem(GetLocalStorageSuffixKey('SubSiteColorDataJson'));
    ConfigListData = JSON.parse(retrievedObject);
    if (ConfigListData != null && ConfigListData.results != null && ConfigListData.results.length > 0) {
        try {
            var colorFound = false;
            var defaultColorFound = false;
            for (var congigItemInex = 0; congigItemInex < ConfigListData.results.length; congigItemInex++) {

                var SubsiteColor = ConfigListData.results[congigItemInex].color;
                var LikesColor = ConfigListData.results[congigItemInex].rollover;
                var ParentSiteTitle = ConfigListData.results[congigItemInex].Title;
                var Parentsitepath = ConfigListData.results[congigItemInex].sitepath;
                if (ParentSiteTitle === "DEFAULT") {
                    defaultSubsiteColor = ConfigListData.results[congigItemInex].color;
                    defaultLikesColor = ConfigListData.results[congigItemInex].rollover;
                    defaultColorFound = true;
                }
                if (Parentsitepath != null) {
                    var resultParentsitepath = Parentsitepath.replace(/.*?:\/\//g, "");
                    if (resultParentsitepath === parentsite) {
                        colorFound = true;
                        if (section == 'RASection') {
                            $('.BGC' + getformatlistID + listItemID).css('background-color', SubsiteColor);
                            $('.BGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>');

                            $('.BGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                        } else if (section == 'FeaturingNow') {
                            if ($(window).width() > 1024) {
                                $('.FN' + getformatlistID + 'Title' + listItemID + '0').css('background-color', SubsiteColor);
                                $('.FN' + getformatlistID + 'Title' + listItemID + '0').html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>');

                                $('.FN' + getformatlistID + 'Title' + listItemID + '1').css('background-color', SubsiteColor);
                                $('.FN' + getformatlistID + 'Title' + listItemID + '1').html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>');

                                $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                                //Fix to fill in like row with section color for tiny tiles
                                $('.SBBGC' + getformatlistID + 'Likes' + listItemID).parent().css('background-color', SubsiteColor);
                                //Fix to display site title from the Config list
                                var tinyTileTitle = $('.SBBGC' + getformatlistID + 'Likes' + listItemID).parent().parent().find("#cswp-tile-subsite-tiny");
                                if (tinyTileTitle != null && tinyTileTitle != undefined && tinyTileTitle.length > 0) {
                                    tinyTileTitle[0].textContent = ParentSiteTitle;
                                }
                                $('.FMRT' + getformatlistID + 'Title' + listItemID).html(ParentSiteTitle);

                            } else {
                                $('.FN' + getformatlistID + 'Title' + listItemID).css('background-color', SubsiteColor);
                                $('.FN' + getformatlistID + 'Title' + listItemID).html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>');

                                $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                            }
                        } else {
                            if ($(window).width() <= 1024 && section == 'SBRASection') {
                                $('.FNS' + getformatlistID + 'Title' + listItemID).css('background-color', SubsiteColor);
                                $('.FNS' + getformatlistID + 'Title' + listItemID).html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>');

                                $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);

                            } else {
                                $('.SBBGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>');
                                //Fix to fill in like row with section color for tiny tiles
                                $('.SBBGC' + getformatlistID + 'Likes' + listItemID).parent().css('background-color', SubsiteColor);
                                $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                            }
                        }
                        //Exit loop once desired operation complete
                        if (defaultColorFound == true) {
                            break;
                        }
                    }
                }
            }
            if (colorFound == false) {
                if (section == 'RASection') {
                    $('.BGC' + getformatlistID + listItemID).css('background-color', defaultSubsiteColor);
                    $('.BGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>');

                    $('.BGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                } else if (section == 'FeaturingNow') {
                    $('.FN' + getformatlistID + 'Title' + listItemID + '0').css('background-color', defaultSubsiteColor);
                    $('.FN' + getformatlistID + 'Title' + listItemID + '0').html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>');

                    $('.FN' + getformatlistID + 'Title' + listItemID + '1').css('background-color', defaultSubsiteColor);
                    $('.FN' + getformatlistID + 'Title' + listItemID + '1').html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>');

                    $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                } else {
                    if ($(window).width() <= 1024 && section == 'SBRASection') {
                        $('.FNS' + getformatlistID + 'Title' + listItemID).css('background-color', defaultSubsiteColor);
                        $('.FNS' + getformatlistID + 'Title' + listItemID).html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>');
                        $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                    } else {
                        $('.SBBGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>');

                        $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                    }
                }
            }
            if (callingMethod == 'DisplayTemplateGetLikeCount') {
                DisplayTemplateGetLikeCountRASection(relativeURL, listItemID, sitename, formatlistID);
            }
            if (callingMethod == 'SBDisplayTemplateGetLikeCount') {
                SBDisplayTemplateGetLikeCountSction(relativeURL, listItemID, sitename, formatlistID);
            }
            if (callingMethod == 'CalendarAgendaDisplayTemplateGetLikeCount') {
                CalendarAgendaDisplayTemplateGetLikeCountSction(relativeURL, listItemID, sitename, formatlistID);
            }
        } catch (e) {
            //console.log(e);
        }
    } else {
        var callConfig = $.ajax({
            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/lists/getbytitle('SubsiteColors')/items?" +
            "&$select=Title,sitepath,color,rollover",
            type: "GET",
            dataType: "json",
            headers: {
                Accept: "application/json;odata=verbose"
            }
        });
        callConfig.done(function (data, textStatus, jqXHR) {
            try {
                var colorFound = false;
                var defaultColorFound = false;
                ConfigListData = data.d;
                localStorage.setItem(GetLocalStorageSuffixKey('SubSiteColorDataJson'), JSON.stringify(ConfigListData));
                if (ConfigListData.results != null) {
                    for (var congigItemInex = 0; congigItemInex < ConfigListData.results.length; congigItemInex++) {
                        var SubsiteColor = ConfigListData.results[congigItemInex].color;
                        var LikesColor = ConfigListData.results[congigItemInex].rollover;
                        var ParentSiteTitle = ConfigListData.results[congigItemInex].Title;
                        var Parentsitepath = ConfigListData.results[congigItemInex].sitepath;
                        if (ParentSiteTitle === "DEFAULT") {
                            defaultSubsiteColor = ConfigListData.results[congigItemInex].color;
                            defaultLikesColor = ConfigListData.results[congigItemInex].rollover;
                            defaultColorFound = true;
                        }
                        if (Parentsitepath != null) {
                            var resultParentsitepath = Parentsitepath.replace(/.*?:\/\//g, "");

                            if (resultParentsitepath === parentsite) {
                                colorFound = true;
                                if (section == 'RASection') {
                                    $('.BGC' + getformatlistID + listItemID).css('background-color', SubsiteColor);
                                    $('.BGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>');

                                    $('.BGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                                } else if (section == 'FeaturingNow') {
                                    $('.FN' + getformatlistID + 'Title' + listItemID + '0').css('background-color', SubsiteColor);
                                    /* $('.FN' + getformatlistID + 'Title' + listItemID + '0').html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>'); */

                                    $('.FN' + getformatlistID + 'Title' + listItemID + '1').css('background-color', SubsiteColor);
                                    /* 	$('.FN' + getformatlistID + 'Title' + listItemID + '1').html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>'); */

                                    $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                                } else {
                                    $('.SBBGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>');

                                    $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                                }
                                //Exit loop once desired operation complete
                                if (defaultColorFound == true) {
                                    break;
                                }
                            }
                        }
                    }
                    if (colorFound == false) {
                        if (section == 'RASection') {
                            $('.BGC' + getformatlistID + listItemID).css('background-color', defaultSubsiteColor);
                            $('.BGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>');

                            $('.BGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                        } else if (section == 'FeaturingNow') {
                            $('.FN' + getformatlistID + 'Title' + listItemID + '0').css('background-color', defaultSubsiteColor);
                            /* 	$('.FN' + getformatlistID + 'Title' + listItemID + '0').html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>'); */

                            $('.FN' + getformatlistID + 'Title' + listItemID + '1').css('background-color', defaultSubsiteColor);
                            /* $('.FN' + getformatlistID + 'Title' + listItemID + '1').html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>'); */

                            $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                        } else {
                            $('.SBBGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>');

                            $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                        }
                    }
                }
                if (callingMethod == 'DisplayTemplateGetLikeCount') {
                    DisplayTemplateGetLikeCountRASection(relativeURL, listItemID, sitename, formatlistID);
                }
                if (callingMethod == 'SBDisplayTemplateGetLikeCount') {
                    SBDisplayTemplateGetLikeCountSction(relativeURL, listItemID, sitename, formatlistID);
                }
                if (callingMethod == 'CalendarAgendaDisplayTemplateGetLikeCount') {
                    CalendarAgendaDisplayTemplateGetLikeCountSction(relativeURL, listItemID, sitename, formatlistID);
                }
            } catch (e) {
                //console.log(e);
            }
        });
        callConfig.fail(function (jqXHR, textStatus, errorThrown) {
            //console.log("Error getting Site Color" + jqXHR.responseText);
        });
    }

}

/*Get subsite Color for News section */
function getSubsiteColorNewsSection(listID, listItemID, path, section, relativeURL, sitename, formatlistID, callingMethod, region) {
    var getformatlistID = listID.toString().substring(1, 37);

    var filter;
    var parentsite = '';
    var urlParts = path.split('/');

    var firstSiteTitle = path.split('/')[3]; //.toUpperCase()
    if (firstSiteTitle == 'PAGES') {
        for (var i = 2; i < 3; i++) {
            parentsite += urlParts[i] + '/';
        }

        filter = "sitepath eq '" + parentsite + "'";
    } else {
        for (var i = 2; i < 4; i++) {
            parentsite += urlParts[i] + '/';
        }

        filter = "substringof('" + parentsite + "',sitepath)";
    }
    var defaultSubsiteColor = '';
    var defaultLikesColor = '';
    var retrievedObject = localStorage.getItem(GetLocalStorageSuffixKey('SubSiteColorDataJson'));
    ConfigListData = JSON.parse(retrievedObject);
    if (ConfigListData != null && ConfigListData.results != null && ConfigListData.results.length > 0) {
        try {
            var colorFound = false;
            var defaultColorFound = false;
            for (var congigItemInex = 0; congigItemInex < ConfigListData.results.length; congigItemInex++) {

                var SubsiteColor = ConfigListData.results[congigItemInex].color;
                var LikesColor = ConfigListData.results[congigItemInex].rollover;
                var ParentSiteTitle = ConfigListData.results[congigItemInex].Title;
                var Parentsitepath = ConfigListData.results[congigItemInex].sitepath;
                if (ParentSiteTitle === "DEFAULT") {
                    defaultSubsiteColor = ConfigListData.results[congigItemInex].color;
                    defaultLikesColor = ConfigListData.results[congigItemInex].rollover;
                    defaultColorFound = true;
                }
                if (ParentSiteTitle.toUpperCase() == region.toUpperCase()) {
                    colorFound = true;
                    if (section == 'RASection') {
                        $('.BGC' + getformatlistID + listItemID).css('background-color', SubsiteColor);
                        $('.BGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" title="' + region + '" href="' + path + '">' + region + '</a>');

                        $('.BGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                    } else if (section == 'FeaturingNow') {
                        if ($(window).width() > 1024) {
                            $('.FN' + getformatlistID + 'Title' + listItemID + '0').css('background-color', SubsiteColor);
                            $('.FN' + getformatlistID + 'Title' + listItemID + '0').html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>');

                            $('.FN' + getformatlistID + 'Title' + listItemID + '1').css('background-color', SubsiteColor);
                            $('.FN' + getformatlistID + 'Title' + listItemID + '1').html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>');

                            $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                            //Fix to fill in like row with section color for tiny tiles
                            $('.SBBGC' + getformatlistID + 'Likes' + listItemID).parent().css('background-color', SubsiteColor);
                            //Fix to display site title from the Config list
                            var tinyTileTitle = $('.SBBGC' + getformatlistID + 'Likes' + listItemID).parent().parent().find("#cswp-tile-subsite-tiny");
                            if (tinyTileTitle != null && tinyTileTitle != undefined && tinyTileTitle.length > 0) {
                                tinyTileTitle[0].textContent = ParentSiteTitle;
                            }
                            $('.FMRT' + getformatlistID + 'Title' + listItemID).html(ParentSiteTitle);

                        } else {
                            $('.FN' + getformatlistID + 'Title' + listItemID).css('background-color', SubsiteColor);
                            $('.FN' + getformatlistID + 'Title' + listItemID).html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>');
                            $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);

                        }
                    } else {
                        $('.SBBGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" title="' + region + '" href="' + path + '">' + region + '</a>');
                        //Fix to fill in like row with section color for tiny tiles
                        $('.SBBGC' + getformatlistID + 'Likes' + listItemID).parent().css('background-color', SubsiteColor);
                        $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                    }
                    //Exit loop once desired operation complete
                    if (defaultColorFound == true) {
                        break;
                    }
                }
            }
            if (colorFound == false) {
                if (section == 'RASection') {
                    $('.BGC' + getformatlistID + listItemID).css('background-color', defaultSubsiteColor);
                    $('.BGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" title="' + region + '" href="' + path + '">' + region + '</a>');

                    $('.BGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                } else if (section == 'FeaturingNow') {
                    if ($(window).width() > 1024) {
                        $('.FN' + getformatlistID + 'Title' + listItemID + '0').css('background-color', defaultSubsiteColor);
                        $('.FN' + getformatlistID + 'Title' + listItemID + '0').html('<a class="cswp-tile-subsite-link" title="' + region + '" href="' + path + '">' + region + '</a>');

                        $('.FN' + getformatlistID + 'Title' + listItemID + '1').css('background-color', defaultSubsiteColor);
                        $('.FN' + getformatlistID + 'Title' + listItemID + '1').html('<a class="cswp-tile-subsite-link" title="' + region + '" href="' + path + '">' + region + '</a>');

                        $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                    } else {
                        $('.FN' + getformatlistID + 'Title' + listItemID).css('background-color', defaultSubsiteColor);
                        $('.FN' + getformatlistID + 'Title' + listItemID).html('<a class="cswp-tile-subsite-link" title="' + region + '" href="' + path + '">' + region + '</a>');

                        $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                    }
                } else {
                    $('.SBBGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" title="' + region + '" href="' + path + '">' + region + '</a>');

                    $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                }
            }
            if (callingMethod == 'DisplayTemplateGetLikeCount') {
                DisplayTemplateGetLikeCountRASection(relativeURL, listItemID, sitename, formatlistID);
            }
            if (callingMethod == 'SBDisplayTemplateGetLikeCount') {
                SBDisplayTemplateGetLikeCountSction(relativeURL, listItemID, sitename, formatlistID);
            }
            if (callingMethod == 'NewsDisplayTemplateGetLikeCount') {
                DisplayTemplateGetLikeCountNewsSection(relativeURL, listItemID, sitename, formatlistID);
            }
            if (callingMethod == 'CalendarAgendaDisplayTemplateGetLikeCount') {
                CalendarAgendaDisplayTemplateGetLikeCountSction(relativeURL, listItemID, sitename, formatlistID);
            }
        } catch (e) {
            //console.log(e);
        }
    } else {
        var callConfig = $.ajax({
            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/lists/getbytitle('SubsiteColors')/items?" +
            "&$select=Title,sitepath,color,rollover",
            type: "GET",
            dataType: "json",
            headers: {
                Accept: "application/json;odata=verbose"
            }
        });
        callConfig.done(function (data, textStatus, jqXHR) {
            try {
                var colorFound = false;
                var defaultColorFound = false;
                ConfigListData = data.d;
                localStorage.setItem(GetLocalStorageSuffixKey('SubSiteColorDataJson'), JSON.stringify(ConfigListData));
                if (ConfigListData.results != null) {
                    for (var congigItemInex = 0; congigItemInex < ConfigListData.results.length; congigItemInex++) {
                        var SubsiteColor = ConfigListData.results[congigItemInex].color;
                        var LikesColor = ConfigListData.results[congigItemInex].rollover;
                        var ParentSiteTitle = ConfigListData.results[congigItemInex].Title;
                        var Parentsitepath = ConfigListData.results[congigItemInex].sitepath;
                        if (ParentSiteTitle === "DEFAULT") {
                            defaultSubsiteColor = ConfigListData.results[congigItemInex].color;
                            defaultLikesColor = ConfigListData.results[congigItemInex].rollover;
                            defaultColorFound = true;
                        }
                        if (ParentSiteTitle.toUpperCase() == region.toUpperCase()) {
                            colorFound = true;
                            if (section == 'RASection') {
                                $('.BGC' + getformatlistID + listItemID).css('background-color', SubsiteColor);
                                $('.BGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" title="' + region + '" href="' + path + '">' + region + '</a>');

                                $('.BGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                            } else if (section == 'FeaturingNow') {
                                if ($(window).width() > 1024) {
                                    $('.FN' + getformatlistID + 'Title' + listItemID + '0').css('background-color', SubsiteColor);
                                    /* $('.FN' + getformatlistID + 'Title' + listItemID + '0').html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>'); */

                                    $('.FN' + getformatlistID + 'Title' + listItemID + '1').css('background-color', SubsiteColor);
                                    /* 	$('.FN' + getformatlistID + 'Title' + listItemID + '1').html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>'); */

                                    $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                                } else {
                                    $('.FN' + getformatlistID + 'Title' + listItemID).css('background-color', SubsiteColor);

                                    $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                                }
                            } else {
                                $('.SBBGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>');

                                $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                            }
                            //Exit loop once desired operation complete
                            if (defaultColorFound == true) {
                                break;
                            }
                        }
                    }
                    if (colorFound == false) {
                        if (section == 'RASection') {
                            $('.BGC' + getformatlistID + listItemID).css('background-color', defaultSubsiteColor);
                            $('.BGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + path + '">' + region + '</a>');

                            $('.BGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                        } else if (section == 'FeaturingNow') {
                            if ($(window).width() > 1024) {
                                $('.FN' + getformatlistID + 'Title' + listItemID + '0').css('background-color', defaultSubsiteColor);
                                /* 	$('.FN' + getformatlistID + 'Title' + listItemID + '0').html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>'); */

                                $('.FN' + getformatlistID + 'Title' + listItemID + '1').css('background-color', defaultSubsiteColor);
                                /* $('.FN' + getformatlistID + 'Title' + listItemID + '1').html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>'); */

                                $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                            } else {
                                $('.FN' + getformatlistID + 'Title' + listItemID).css('background-color', defaultSubsiteColor);

                                $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                            }
                        } else {
                            $('.SBBGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + path + '">' + region + '</a>');

                            $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                        }
                    }
                }
                if (callingMethod == 'DisplayTemplateGetLikeCount') {
                    DisplayTemplateGetLikeCountRASection(relativeURL, listItemID, sitename, formatlistID);
                }
                if (callingMethod == 'SBDisplayTemplateGetLikeCount') {
                    SBDisplayTemplateGetLikeCountSction(relativeURL, listItemID, sitename, formatlistID);
                }
                if (callingMethod == 'NewsDisplayTemplateGetLikeCount') {
                    DisplayTemplateGetLikeCountNewsSection(relativeURL, listItemID, sitename, formatlistID);
                }
                if (callingMethod == 'CalendarAgendaDisplayTemplateGetLikeCount') {
                    CalendarAgendaDisplayTemplateGetLikeCountSction(relativeURL, listItemID, sitename, formatlistID);
                }
            } catch (e) {
                //console.log(e);
            }
        });
        callConfig.fail(function (jqXHR, textStatus, errorThrown) {
            //console.log("Error getting Site Color" + jqXHR.responseText);
        });
    }

}

function getSubsiteColor(listID, listItemID, path, section) {
    var getformatlistID = listID.toString().substring(1, 37);

    var filter;
    var parentsite = '';
    var urlParts = path.split('/');

    var firstSiteTitle = path.split('/')[3]; //.toUpperCase()
    if (firstSiteTitle == 'PAGES') {
        for (var i = 2; i < 3; i++) {
            parentsite += urlParts[i] + '/';
        }

        filter = "sitepath eq '" + parentsite + "'";
    } else {
        for (var i = 2; i < 4; i++) {
            parentsite += urlParts[i] + '/';
        }

        filter = "substringof('" + parentsite + "',sitepath)";
    }
    var defaultSubsiteColor = '';
    var defaultLikesColor = '';
    var retrievedObject = localStorage.getItem(GetLocalStorageSuffixKey('SubSiteColorDataJson'));
    ConfigListData = JSON.parse(retrievedObject);
    if (ConfigListData != null && ConfigListData.results != null && ConfigListData.results.length > 0) {
        try {
            var colorFound = false;
            var defaultColorFound = false;
            for (var congigItemInex = 0; congigItemInex < ConfigListData.results.length; congigItemInex++) {

                var SubsiteColor = ConfigListData.results[congigItemInex].color;
                var LikesColor = ConfigListData.results[congigItemInex].rollover;
                var ParentSiteTitle = ConfigListData.results[congigItemInex].Title;
                var Parentsitepath = ConfigListData.results[congigItemInex].sitepath;
                if (ParentSiteTitle === "DEFAULT") {
                    defaultSubsiteColor = ConfigListData.results[congigItemInex].color;
                    defaultLikesColor = ConfigListData.results[congigItemInex].rollover;
                    defaultColorFound = true;
                }
                var resultParentsitepath = Parentsitepath.replace(/.*?:\/\//g, "");
                if (resultParentsitepath === parentsite) {
                    colorFound = true;
                    if (section == 'RASection') {
                        $('.BGC' + getformatlistID + listItemID).css('background-color', SubsiteColor);
                        $('.BGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>');

                        $('.BGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                    } else if (section == 'FeaturingNow') {
                        $('.FN' + getformatlistID + 'Title' + listItemID + '0').css('background-color', SubsiteColor);
                        /* 	$('.FN' + getformatlistID + 'Title' + listItemID + '0').html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>'); 	*/

                        $('.FN' + getformatlistID + 'Title' + listItemID + '1').css('background-color', SubsiteColor);
                        /* 	$('.FN' + getformatlistID + 'Title' + listItemID + '1').html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>'); */

                        $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                    } else {
                        $('.SBBGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>');

                        $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                    }
                    //Exit loop once desired operation complete
                    if (defaultColorFound == true) {
                        break;
                    }
                }
            }
            if (colorFound == false) {
                if (section == 'RASection') {
                    $('.BGC' + getformatlistID + listItemID).css('background-color', defaultSubsiteColor);
                    $('.BGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>');

                    $('.BGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                } else if (section == 'FeaturingNow') {
                    $('.FN' + getformatlistID + 'Title' + listItemID + '0').css('background-color', defaultSubsiteColor);
                    /*		$('.FN' + getformatlistID + 'Title' + listItemID + '0').html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>'); */

                    $('.FN' + getformatlistID + 'Title' + listItemID + '1').css('background-color', defaultSubsiteColor);
                    /*	$('.FN' + getformatlistID + 'Title' + listItemID + '1').html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>'); */

                    $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                } else {
                    $('.SBBGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>');

                    $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                }
            }
        } catch (e) {
            //console.log(e);
        }
    } else {
        var callConfig = $.ajax({
            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/lists/getbytitle('SubsiteColors')/items?" +
            "&$select=Title,sitepath,color,rollover",
            type: "GET",
            dataType: "json",
            headers: {
                Accept: "application/json;odata=verbose"
            }
        });
        callConfig.done(function (data, textStatus, jqXHR) {
            try {
                var colorFound = false;
                var defaultColorFound = false;
                ConfigListData = data.d;
                localStorage.setItem(GetLocalStorageSuffixKey('SubSiteColorDataJson'), JSON.stringify(ConfigListData));
                if (ConfigListData.results != null) {
                    for (var congigItemInex = 0; congigItemInex < ConfigListData.results.length; congigItemInex++) {
                        var SubsiteColor = ConfigListData.results[congigItemInex].color;
                        var LikesColor = ConfigListData.results[congigItemInex].rollover;
                        var ParentSiteTitle = ConfigListData.results[congigItemInex].Title;
                        var Parentsitepath = ConfigListData.results[congigItemInex].sitepath;
                        if (ParentSiteTitle === "DEFAULT") {
                            defaultSubsiteColor = ConfigListData.results[congigItemInex].color;
                            defaultLikesColor = ConfigListData.results[congigItemInex].rollover;
                            defaultColorFound = true;
                        }
                        var resultParentsitepath = Parentsitepath.replace(/.*?:\/\//g, "");
                        if (resultParentsitepath === parentsite) {
                            colorFound = true;
                            if (section == 'RASection') {
                                $('.BGC' + getformatlistID + listItemID).css('background-color', SubsiteColor);
                                $('.BGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>');

                                $('.BGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                            } else if (section == 'FeaturingNow') {
                                $('.FN' + getformatlistID + 'Title' + listItemID + '0').css('background-color', SubsiteColor);
                                /*	$('.FN' + getformatlistID + 'Title' + listItemID + '0').html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>'); */

                                $('.FN' + getformatlistID + 'Title' + listItemID + '1').css('background-color', SubsiteColor);
                                /*	$('.FN' + getformatlistID + 'Title' + listItemID + '1').html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>'); */

                                $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                            } else {
                                $('.SBBGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + Parentsitepath + '">' + ParentSiteTitle + '</a>');

                                $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', LikesColor);
                            }
                            //Exit loop once desired operation complete
                            if (defaultColorFound == true) {
                                break;
                            }
                        }

                    }
                    if (colorFound == false) {
                        if (section == 'RASection') {
                            $('.BGC' + getformatlistID + listItemID).css('background-color', defaultSubsiteColor);
                            $('.BGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>');

                            $('.BGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                        } else if (section == 'FeaturingNow') {
                            $('.FN' + getformatlistID + 'Title' + listItemID + '0').css('background-color', defaultSubsiteColor);
                            /*	$('.FN' + getformatlistID + 'Title' + listItemID + '0').html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>'); */

                            $('.FN' + getformatlistID + 'Title' + listItemID + '1').css('background-color', defaultSubsiteColor);
                            /*	$('.FN' + getformatlistID + 'Title' + listItemID + '1').html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>'); */

                            $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                        } else {
                            $('.SBBGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>');

                            $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
                        }
                    }
                }
            } catch (e) {
                //console.log(e);
            }
        });
        callConfig.fail(function (jqXHR, textStatus, errorThrown) {
            //console.log("Error getting Site Color" + jqXHR.responseText);
        });
    }

}

/*----------------------------------------------------------------------------------------------------------------*/
/*------------------------------ CODE TO GET DEFAULT COLOR FOR A SUB-SITE ----------------------------------------*/

function getDefaultColor(getformatlistID, listItemID, path, section) {
    var ParentSiteTitle = path.split('/')[3].toUpperCase();

    var call = $.ajax({
        url: _spPageContextInfo.siteAbsoluteUrl + "/_api/lists/getbytitle('SubsiteColors')/items?" +
        "&$select=Title,sitepath,color,rollover" +
        "&$filter=Title eq 'DEFAULT'", //+
        //"&$top=20",
        //async:false,
        type: "GET",
        dataType: "json",
        headers: {
            Accept: "application/json;odata=verbose"
        }
    });

    call.done(function (data, textStatus, jqXHR) {
        try {
            var defaultSubsiteColor = data.d.results[0].color;
            var defaultLikesColor = data.d.results[0].rollover;
            //var ParentSiteTitle = data.d.results[0].Title;
            //var Parentsitepath = data.d.results[0].sitepath;

            if (section == 'RASection') {
                $('.BGC' + getformatlistID + listItemID).css('background-color', defaultSubsiteColor);
                $('.BGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>');

                $('.BGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
            } else if (section == 'FeaturingNow') {
                $('.FN' + getformatlistID + 'Title' + listItemID + '0').css('background-color', defaultSubsiteColor);
                /*    $('.FN' + getformatlistID + 'Title' + listItemID + '0').html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>'); */

                $('.FN' + getformatlistID + 'Title' + listItemID + '1').css('background-color', defaultSubsiteColor);
                /*   $('.FN' + getformatlistID + 'Title' + listItemID + '1').html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>'); */
                $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
            } else {
                $('.SBBGC' + getformatlistID + listItemID).html('<a class="cswp-tile-subsite-link" href="' + path + '">' + ParentSiteTitle + '</a>');

                $('.SBBGC' + getformatlistID + 'Likes' + listItemID).css('background-color', defaultLikesColor);
            }

        } catch (e) {
            //alert(e);
        }
    });

    call.fail(function (jqXHR, textStatus, errorThrown) {
        //alert("Error getting Site Color" + jqXHR.responseText);
    });

}

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

/*---------------------------Method to bind picture Urls to viacompliments topic.aspx page------------------------*/
function getQueryStringParameter(urlParameterKey) {
    var queryString = document.URL.split('?');
    if (queryString.length >= 2) {
        var params = document.URL.split('?')[1].split('&');
        var strParams = '';
        for (var i = 0; i < params.length; i = i + 1) {
            var singleParam = params[i].split('=');
            if (singleParam[0].toLowerCase() == urlParameterKey.toLowerCase())
                return singleParam[1];
        }
    }
}


//Method to Check if user is authenticated.
function checkUserProfilePicStatus(isAsync) {
    var apiURL = _spPageContextInfo.webAbsoluteUrl + "/_api/web/currentuser";
    try {
        $.ajax({
            url: apiURL,
            type: "GET",
            headers: {
                "Accept": "application/json;odata=verbose"
            },
            async: isAsync,
            success: function (data, textStatus, xhr) {
                userProfileImageLoaded = true;
                $('img[src*="' + mySiteHostName + '"]').each(function () {
                    var img_src = $(this).attr('src');
                    $(this).attr('src', img_src + '?' + Math.random());
                });
            },
            error: function (error) {
                userProfileImageLoaded = false;
            }
        });
    } catch (e) {
        userProfileImageLoaded = false;
    }
}