$(window).load(function() {

getUserProperties()
});

$('#createListItem').click(function(){

	createListItem();
	
});
var siteUrl2 = '/benefits/MovieScreening/';
var siteUrl = window.location.protocol + "//" + window.location.host + siteUrl2;


function createListItem() {
	
var fname = $('#firstname').val();

var lname = $('#lastname').val();
var email = $('#workemail').val();
var phone = $('#workphone').val();
var department = $('#department').val();

if (fname=='' || lname =='' || email== '' ||  department=='')
{
	alert('Please fill in all required field');
	return false;
}
    var clientContext = new SP.ClientContext(siteUrl);
    var oList = clientContext.get_web().get_lists().getByTitle('Movie_Screening');
        
    var itemCreateInfo = new SP.ListItemCreationInformation();
    this.oListItem = oList.addItem(itemCreateInfo);
	

        
    oListItem.set_item('Title', fname);
    oListItem.set_item('Last_x0020_Name', lname);
	oListItem.set_item('Work_x0020_Email', email);
	oListItem.set_item('Work_x0020_Phone', phone);
	oListItem.set_item('Department', department);
        
    oListItem.update();

    clientContext.load(oListItem);
        
    clientContext.executeQueryAsync(Function.createDelegate(this, this.onQuerySucceeded), Function.createDelegate(this, this.onQueryFailed));
}

function onQuerySucceeded() {
	 // alert('Item created: ' + oListItem.get_id());

window.location.href = "https://greenroom.viacom.com/benefits/MovieScreening/Pages/ThankYouMS.aspx";
  
}

function onQueryFailed(sender, args) {

    /*ADFS Alert Issue*/console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
}




function getUserProperties(){
 var userid= _spPageContextInfo.userId;

 //var requestUri = _spPageContextInfo.webAbsoluteUrl + "/_api/web/getuserbyid(" + userid + ")";
 var requestUri = _spPageContextInfo.webAbsoluteUrl + "/_api/SP.UserProfiles.PeopleManager/GetMyProperties";
  var requestHeaders = { "accept" : "application/json;odata=verbose" };
  $.ajax({
    url : requestUri,
    contentType : "application/json;odata=verbose",
    headers : requestHeaders,
    success : onSuccess,
    error : onQueryFailed
  });
}

  function onSuccess(data, request){
    //var loginName = data.d.DisplayName;
	var lastname = data.d.UserProfileProperties.results[6].Value;
	var firstname = data.d.UserProfileProperties.results[4].Value;
	var workemail = data.d.Email;
	var workphone = data.d.UserProfileProperties.results[53].Value;
	var dept = data.d.UserProfileProperties.results[10].Value;

    $('#firstname').val(firstname);
	$('#lastname').val(lastname);
	$('#workemail').val(workemail);
	$('#workphone').val(workphone);
	$('#department').val(dept);
	
	
	}  

 /* function onError(error) {
    alert("error");
  }
*/