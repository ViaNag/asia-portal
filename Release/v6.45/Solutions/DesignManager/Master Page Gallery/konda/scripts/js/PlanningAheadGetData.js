$(document).ready(function() {
	PlanningAheadGetData.GetItemsFromCalendar(_spPageContextInfo.webAbsoluteUrl,'Cal','0x010200583E76F5E872426E858939A5AC096179');//GREEN ROOM REMINDER
});

var PlanningAheadGetData={
 oListPageImageCalAgenda : null,
 collListItemPageImageCalAgenda : null,
 allCalItems:new Array(),
 allEventsImagesAndLikes:new Array(),
 allEventsTaggedURL:new Array(),
 itemLimit:6, //Limit to be displayed in calendar Agenda Webpart
/*Get calendar items using SOAP*/
GetItemsFromCalendar: function(webUrl, calendarName,contentType) {
    wsURL = webUrl + "/_vti_bin/Lists.asmx";
    var xmlCall =
        "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'> <soap:Body>" +
        "<GetListItems xmlns='http://schemas.microsoft.com/sharepoint/soap/'>" +
        "<listName>" + calendarName + "</listName>" +
		"<rowLimit>" + PlanningAheadGetData.itemLimit + "</rowLimit>"+
	     "<query>" +
        "<Query>" +
        "<Where>" +
		 "<And>" +
						"<BeginsWith>" +
						  "<FieldRef Name='ContentTypeId'/>" +
						  "<Value Type='Text'>"+contentType+"</Value>" +
						"</BeginsWith>"+					
			   "<DateRangesOverlap>" +
			   "<FieldRef Name=\"EventDate\" />" +
			   "<FieldRef Name=\"EndDate\" />" +
			   "<FieldRef Name=\"RecurrenceID\" />" +
			   "<Value Type='DateTime'><Now/></Value>" +
			   "</DateRangesOverlap>" +
			"</And>" +
        "</Where>" +
        "</Query>" +
        "</query>" +
        "<queryOptions>" +
        "<QueryOptions>" +
            "<ExpandRecurrence>TRUE</ExpandRecurrence>" +
        "</QueryOptions>" +
        "</queryOptions>" +
        "</GetListItems>" +
        "</soap:Body></soap:Envelope>";
     $.ajax({
        url: wsURL,
        type: "POST",
        dataType: "xml",
        async: false,
        data: xmlCall,
        complete: function (xData, status) {
		//console.log(status);
            if (status === "success") {
                var root = $(xData.responseText);
			//	console.log(xData);
                root.find("listitems").children().children().each(function () {
                    $this = $(this);
                    var ids = $this.attr("ows_UniqueId").split(";");
                    var rec = $this.attr("ows_fRecurrence");
                    var EventDate=new Date($this.attr("ows_EventDate"));  
					PlanningAheadGetData.allCalItems.push({
							"StartTime": $this.attr("ows_EventDate"),
							"EndTime": $this.attr("ows_EndDate"),
							"ContentTypeId": contentType,
							"Title": $htmlEncode($this.attr("ows_Title")),
							"Recurrence": (rec === "1" ? true : false),
							"Description":  $htmlEncode($this.attr("ows_Description")),
							"Guid": ids[1],
							"Id": ids[0],
							"ItemLinkRef":webUrl+"/Lists/cal/DispForm.aspx?ID="+ids[0]
						});                                                       
                });
            }
			
			if(contentType==='0x0102007ED29E69F64E47DF827B5239039962BE')//GREEN ROOM EVENT
			{
			PlanningAheadGetData.allCalItems.sort(PlanningAheadGetData.compareDate);//Sort calendar item in ascending order
			PlanningAheadGetData.getTaggedUrl(contentType);//Get Tagged URL for  GREEN ROOM EVENT calendar
			}
			else{
			//Get Green room event Data
			PlanningAheadGetData.GetItemsFromCalendar(_spPageContextInfo.webAbsoluteUrl,'Cal','0x0102007ED29E69F64E47DF827B5239039962BE');//GREEN ROOM EVENT
			}
        },
        contentType: "text/xml; charset=\"utf-8\""
    });
   
},
//Compare function to sort date
compareDate:function(a,b) {
var EventDateA=new Date(a.StartTime.substring(0,10));
var EventDateB=new Date(b.StartTime.substring(0,10));
  if (EventDateA < EventDateB)
     return -1;
  if (EventDateA > EventDateB)
    return 1;
  return 0;
},

getTaggedUrl: function(contentType)
{
var itemCount=PlanningAheadGetData.allCalItems.length;
var filterValue="";
var filterCount=0;
	for(var itemIndex=0;itemIndex<itemCount ;itemIndex++ )
	{
		if(PlanningAheadGetData.allCalItems[itemIndex].ContentTypeId===contentType)//GREEN ROOM EVENT
		{
			if(filterCount>0)
			{
			filterCount++;
			filterValue=filterValue+" or(ID eq '" +PlanningAheadGetData.allCalItems[itemIndex].Id+ "')"
			}
			else{
				filterCount++;
				filterValue="(ID eq '" +PlanningAheadGetData.allCalItems[itemIndex].Id+ "')"
			}
		}
	}
	if(filterCount>0)
	{
	filterValue="("+filterValue+")";
	//console.log("ID :" +filterValue);
	PlanningAheadGetData.getTaggedUrlForAll(filterValue);
	}
	else{
	PlanningAheadGetData.getCalendarAgendaSubsiteColor() ;
	}
},

getTaggedUrlForAll: function(filterValue)
{
var taggedUrlPageQueyCondition="";
var serviceUrl = _spPageContextInfo.webAbsoluteUrl + "/_api/lists/getbytitle('cal')/items?" +
        	"$select=TaggedUrl,ID" +      
            "&$filter=" +filterValue;
 var call = $.ajax({
        url:serviceUrl,
        type: "GET",
        async: false,
        dataType: "json",
        headers: {
            Accept: "application/json;odata=verbose"
        }
    });

    call.done(function(data, textStatus, jqXHR) {
        try {
		//console.log("length :"+data.d.results.length);
		var taggedImageCount=0;
		for(var itemIndex=0;itemIndex<data.d.results.length ;itemIndex++ )
		{
		//	console.log("TaggedUrl :"+data.d.results[itemIndex].ID);
			if(data.d.results[itemIndex].TaggedUrl)
			{			
				var taggedUrlPage = data.d.results[itemIndex].TaggedUrl; 
				var taggedUrlParts=taggedUrlPage.toLowerCase().split("pages");
				if(taggedUrlParts.length>1)
				{
				taggedUrlPage=taggedUrlParts[1].slice(1);
				taggedUrlPage=taggedUrlPage+".aspx";
				}
				taggedUrlPageQueyCondition=taggedUrlPageQueyCondition+"<Value Type='Text'>"+taggedUrlPage+"</Value>";
				taggedImageCount++;
					PlanningAheadGetData.allEventsTaggedURL.push({
							"TaggedPage": taggedUrlPage,
							"TaggedUrlPage": data.d.results[itemIndex].TaggedUrl,
							"CalItemID": data.d.results[itemIndex].ID
					});               
			}
			
		}
	    if(taggedImageCount>0)
		{
				//console.log("taggedUrlPage :"+taggedUrlPageQueyCondition);				
				PlanningAheadGetData.CalendarAgendaPageImage(taggedUrlPageQueyCondition);
		}
		else
		{
		PlanningAheadGetData.getCalendarAgendaSubsiteColor() ;
		}		
        } catch (e) {}
    });

    call.fail(function(jqXHR, textStatus, errorThrown) {
		PlanningAheadGetData.getCalendarAgendaSubsiteColor() ;
        /*ADFS Alert Issue*/console.log("Error generating Report: " + jqXHR.responseText);
    });
 },

CalendarAgendaPageImage: function(taggedUrlPageQueyCondition){

	var clientContextPageImage = new SP.ClientContext.get_current(); 		
	oListPageImageCalAgenda = clientContextPageImage .get_web().get_lists().getByTitle('Pages');	
    var camlQueryPageImage = new SP.CamlQuery();
	camlQueryPageImage.set_viewXml("<View Scope='RecursiveAll'><Query><Where><In><FieldRef Name='FileLeafRef' /><Values>"+taggedUrlPageQueyCondition+"</Values></In></Where></Query><ViewFields><FieldRef Name='FileLeafRef' /><FieldRef Name='PublishingPageImage' /><FieldRef Name='ID' /><FieldRef Name='LikedBy' /><FieldRef Name='LikesCount' /></ViewFields></View>"); 
	collListItemPageImageCalAgenda = oListPageImageCalAgenda.getItems(camlQueryPageImage);
	clientContextPageImage.load(oListPageImageCalAgenda);
	clientContextPageImage.load(collListItemPageImageCalAgenda);
    clientContextPageImage.executeQueryAsync(Function.createDelegate(this, function(success) {
     var listItemEnumeratorPageImage = collListItemPageImageCalAgenda.getEnumerator();
	 var listGuid=oListPageImageCalAgenda.get_id().toString();
		while (listItemEnumeratorPageImage.moveNext())
		{
			var oListItemPageImage = listItemEnumeratorPageImage.get_current();	
			var pageUrl =  oListItemPageImage.get_item('FileLeafRef');
			var pageImage =  oListItemPageImage.get_item('PublishingPageImage');
			var id= oListItemPageImage.get_item('ID');
			var likeDisplay = true;
			var $v_0 = oListItemPageImage.get_item('LikedBy');
			var itemc = oListItemPageImage.get_item('LikesCount');
			if (itemc == null || itemc == undefined || itemc == "undefined") {
				itemc = 0;
			}
			if (!SP.ScriptHelpers.isNullOrUndefined($v_0)) {
				for (var $v_1 = 0, $v_2 = $v_0.length; $v_1 < $v_2; $v_1++) {
					var $v_3 = $v_0[$v_1];
					if ($v_3.$1E_1 === _spPageContextInfo.userId) {
						likeDisplay = false;
					}
				}
			}
			
			PlanningAheadGetData.allEventsImagesAndLikes.push({
							"FileLeafRef": pageUrl.toLowerCase(),
							"PublishingPageImage": pageImage,
							"ID": id,
							"LikeDisplay": likeDisplay,
							"PageURL": pageUrl,
							"ListGuid": listGuid,
							"LikesCount":  itemc
			});                     
		}
		PlanningAheadGetData.getCalendarAgendaSubsiteColor() ;
    }), Function.createDelegate(this, function(sender, args) {
       PlanningAheadGetData.getCalendarAgendaSubsiteColor() ;
    }));	
},
getCalendarAgendaSubsiteColor : function() {
var taggedUrlPageQueyCondition="";
var defaultSubsiteColor='';
var defaultLikesColor ='';
var SubsiteColor ='';
var LikesColor = '';
var parentsite=_spPageContextInfo.siteAbsoluteUrl + "/" +_spPageContextInfo.webServerRelativeUrl.split('/')[1];
var getColorUrl=_spPageContextInfo.siteAbsoluteUrl + "/_api/lists/getbytitle('SubsiteColors')/items?&$select=Title,sitepath,color,rollover&$filter=(( startswith(sitepath , '"+parentsite+"' )) or (Title eq 'DEFAULT' ))";
	 var callConfig = $.ajax({
			url: getColorUrl, 
			type: "GET",
			dataType: "json",
			headers: {
				Accept: "application/json;odata=verbose"
			}
		});
		callConfig.done(function(data, textStatus, jqXHR) {
			try 
			{
				var colorFound=false;
				var defaultColorFound=false;
				ConfigListData=data.d;
				if (ConfigListData.results != null) {
					for(var congigItemInex=0;congigItemInex<ConfigListData.results.length;congigItemInex++)
					{

						SubsiteColor = ConfigListData.results[congigItemInex].color;
						LikesColor = ConfigListData.results[congigItemInex].rollover;
						var Parentsitepath = ConfigListData.results[congigItemInex].sitepath;
						if(ParentSiteTitle ==="DEFAULT")
						{
						   defaultSubsiteColor = ConfigListData.results[congigItemInex].color;
						   defaultLikesColor = ConfigListData.results[congigItemInex].rollover;
						   defaultColorFound=true;
						   	if(colorFound==true)
							{
									break;
							}
						}
						if(resultParentsitepath ===parentsite)
						{
							colorFound=true;
							break;

						}
						
					}
				}			
			
				if(colorFound==false)
				{
				SubsiteColor=defaultSubsiteColor;
				LikesColor=defaultLikesColor;
				}
			}	
			catch (e) {
			//console.log(e);
			}
			$("#greenroom-Planning-Agenda").html(PlanningAheadGetDisplay.createHTML(SubsiteColor));
		});
		callConfig.fail(function(jqXHR, textStatus, errorThrown) {
       $("#greenroom-Planning-Agenda").html(PlanningAheadGetDisplay.createHTML(SubsiteColor));
		});
}
}