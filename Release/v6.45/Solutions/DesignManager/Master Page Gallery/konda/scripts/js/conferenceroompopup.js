var itemId="";
var details=[];
$(document).ready(function(){
itemId = getParameterByName('ID');
getItemDetailsById(itemId);

});

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function getItemDetailsById(itemId)
{
$.ajax({
url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/GetByTitle('Conference%20Rooms')/items("+itemId+")",
                    type: "GET",
                    headers: {
                        "accept": "application/json;odata=verbose",
                    },
                    success: function (data) {
					 
					 var item =data.d;
					 getPublishingImage(item);
					 
					},
					error: function (error) {
                        /*ADFS Alert Issue*/console.log(JSON.stringify(error));
                    }
					});
}



function getPublishingImage(item){
    var publishingImage = '';
            // List item retrieved. Fetch the Publishing Image.
         var publishingImageUri = item.FieldValuesAsHtml.__deferred.uri;

            // Query SharePoint
            $.ajax({
                url:        publishingImageUri,
                method:     'GET',
                headers:    {
                    Accept:     'application/json; odata=verbose'
                },
                success:    function(data, request){
 				   publishingImage = data.d.Picture1;
				   details.push({Title:item.Title,Location:item.Location,Capacity:item.Capacity,Features:item.Features,Img:data.d.Picture1});
				   ShowDetails();
                },
				error: function (error) {
                        /*ADFS Alert Issue*/console.log(JSON.stringify(error));
                    }
            });
       
    // Return the Publishing Image html
    return publishingImage;
}

function ShowDetails()
{
$('#lblTitle').html(details[0].Title);
$('#lblLoc').html(details[0].Location);
$('#lblCapacity').html(details[0].Capacity);
$('#divFeaturesHtml').html(details[0].Features);
$('#divImage').html(details[0].Img);
}