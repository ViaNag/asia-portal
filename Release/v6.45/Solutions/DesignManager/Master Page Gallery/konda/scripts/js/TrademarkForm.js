﻿var viacomTrademarkForm = {

    clientContext: null,
    web: null,
    camlQuery: null,
    oList: null,
    firstName: null,
    mtvOwned: null,
    insertValues: function() {
		
        viacomTrademarkForm.clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl + "/Services/Bala");
        viacomTrademarkForm.web = viacomTrademarkForm.clientContext.get_web();
        viacomTrademarkForm.oList = viacomTrademarkForm.web.get_lists().getByTitle('TrademarkForm');
        viacomTrademarkForm.clientContext.load(viacomTrademarkForm.oList);
        viacomTrademarkForm.clientContext.executeQueryAsync(viacomTrademarkForm.checkValidations, viacomTrademarkForm.failure);
    },

    failure: function(sender, args) {
        /*ADFS Alert Issue*/console.log(args.get_message() + '\n' + args.get_stackTrace());
    },
    successInsertValues: function() {


        var itemCreateInfo = new SP.ListItemCreationInformation();
        createItem = viacomTrademarkForm.oList.addItem(itemCreateInfo);
        createItem.set_item('LastName', viacomTrademarkForm.lastName);
        createItem.set_item('FirstName', viacomTrademarkForm.firstName);
        createItem.set_item('Phone', viacomTrademarkForm.phone);
        createItem.set_item('Fax', viacomTrademarkForm.fax);
        createItem.set_item('Email', viacomTrademarkForm.email);
        createItem.set_item('NameToBeCleared', viacomTrademarkForm.nameCleared);
        createItem.set_item('MTVNOwnedProperty', viacomTrademarkForm.mtvOwned);
        createItem.set_item('OutsideParties', viacomTrademarkForm.outsideParties);
        createItem.set_item('AnticipatedWindowofUse', viacomTrademarkForm.anticipatedWindowofUse);
        createItem.set_item('ReplacementTitle', viacomTrademarkForm.replacementTitle);
        createItem.set_item('ChannelAttorney', viacomTrademarkForm.channelAttorney);
        createItem.set_item('AnticipatedWindowofUse', viacomTrademarkForm.anticipatedWindowofUse);
        createItem.set_item('BudgetCode', viacomTrademarkForm.budgetCode);
        createItem.set_item('OnAirProgramming', viacomTrademarkForm.onAirProgramming);
        createItem.set_item('NatureofTvShow', viacomTrademarkForm.natureofTvshows);
        createItem.set_item('OneSheet', viacomTrademarkForm.oneSheet);
        createItem.set_item('Merchandise', viacomTrademarkForm.merchandiseValue);
        createItem.set_item('Channel', viacomTrademarkForm.channel);
        createItem.set_item('AnticipatedDateofUse', viacomTrademarkForm.DateOfFirstUse);

        createItem.update();
        viacomTrademarkForm.clientContext.load(createItem);
        viacomTrademarkForm.clientContext.executeQueryAsync(viacomTrademarkForm.Success, viacomTrademarkForm.failure);



    },
    Success: function() {
        alert("Thank You. Your form has been submitted successfully.");
    },
    checkValidations: function() {
        viacomTrademarkForm.merchandiseValue = '';
        viacomTrademarkForm.merchandiseValues = '';
        var flag = 0;
        /*---------Fetching controls value-------------*/
        viacomTrademarkForm.firstName = $('#FirstName').val();
        viacomTrademarkForm.lastName = $('#LastName').val();
        viacomTrademarkForm.phone = $('#Phone').val();
        viacomTrademarkForm.fax = $('#Fax').val();
        viacomTrademarkForm.email = $('#Email').val();
        viacomTrademarkForm.nameCleared = $('#NameCleared').val();
        viacomTrademarkForm.replacementTitle = $('#ReplacementTitle').val();
        viacomTrademarkForm.channelAttorney = $('#ChannelAttorney').val();
        viacomTrademarkForm.DateOfFirstUse = $('#AnticipatedDateofUse').val();
        viacomTrademarkForm.anticipatedWindowofUse = $('#AnticipatedWindowofUse').val();
        viacomTrademarkForm.natureofTvShow = $('#NatureofTvShow').val();
        viacomTrademarkForm.budgetCode = $('#BudgetCode').val();
        viacomTrademarkForm.natureofTvshows = $('#NatureofTvShow').val();
        viacomTrademarkForm.channel = $("#Channel").val()
        //viacomTrademarkForm.oneSheet = $('#uploadFile').val().replace("C:\\fakepath\\", "")
        viacomTrademarkForm.mtvOwned = $("input[type='radio'][name='MTVNOwnedProperty']:checked");
        viacomTrademarkForm.mtvOwned = viacomTrademarkForm.mtvOwned.val();

        if (viacomTrademarkForm.mtvOwned == "Yes") {
            viacomTrademarkForm.mtvOwned =="1";
        } else {
            viacomTrademarkForm.mtvOwned ="0";
        }
        viacomTrademarkForm.outsideParties = $("input[type='radio'][name='OutsideParties']:checked");
        viacomTrademarkForm.outsideParties = viacomTrademarkForm.outsideParties.val();
        alert(viacomTrademarkForm.mtvOwned);
        if (viacomTrademarkForm.outsideParties == "Yes") {
            viacomTrademarkForm.outsideParties = "1";
        } else {
            viacomTrademarkForm.outsideParties = "0"
        }
        viacomTrademarkForm.onAirProgramming = $("input[type='radio'][name='group2']:checked");
        viacomTrademarkForm.onAirProgramming = viacomTrademarkForm.onAirProgramming.val();
        // update the choice column with checkbox
        $("input:checkbox[name=Merchandise]:checked").each(function() {
            viacomTrademarkForm.merchandiseValue += $(this).val() + ';#';

        });
        viacomTrademarkForm.merchandiseValue = viacomTrademarkForm.merchandiseValue.replace(/;#$/, '');
        /*----------------------fetching controls value ends-----------------*/

        /*----------- Validations Begin-------------- */
         if (viacomTrademarkForm.checkNullValidation(viacomTrademarkForm.firstName, 'FirstName') == true) {
            flag += 1;
        }

        if (viacomTrademarkForm.checkNullValidation(viacomTrademarkForm.lastName, 'LastName') == true) {
            flag += 1;
        }
        if (viacomTrademarkForm.checkNullValidation(viacomTrademarkForm.email, 'Email') == true) {
            flag += 1;

        }
        if (viacomTrademarkForm.checkNullValidation(viacomTrademarkForm.replacementTitle, 'ReplacementTitle') == true) {
             flag += 1;
         }
         if (viacomTrademarkForm.checkNullValidation(viacomTrademarkForm.budgetCode, 'BudgetCode') == true) {
             flag += 1;
         }
         if (viacomTrademarkForm.checkNullValidation(viacomTrademarkForm.channelAttorney, 'ChannelAttorney') == true) {
             flag += 1;
         }
         if (viacomTrademarkForm.checkNullValidation(viacomTrademarkForm.anticipatedWindowofUse, 'AnticipatedWindowofUse') == true) {
             flag += 1;
         }
         if (viacomTrademarkForm.checkNullValidation(viacomTrademarkForm.nameCleared, 'NameCleared') == true) {
             flag += 1;
         }
        //email expression validation        
        if(viacomTrademarkForm.email!= ""||viacomTrademarkForm.email!= null){ 
			var atpos = viacomTrademarkForm.email.indexOf("@");
	    	var dotpos = viacomTrademarkForm.email.lastIndexOf(".");
	    	if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=viacomTrademarkForm.email.length) {
	        $('#SpanEmail').append("Not a valid e-mail address");
	        return false;    }	
		}
		
        if (viacomTrademarkForm.DateOfFirstUse != null && viacomTrademarkForm.DateOfFirstUse != "") {
            var currVal = viacomTrademarkForm.DateOfFirstUse;
            if (currVal == '')
                return false;            
            var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
            var dtArray = currVal.match(rxDatePattern); // is format OK?
            if (dtArray == null) {
                $("#SpanAnticipatedDateofUse").append('Correct Date format required');
                return false;
            }
            //Checks for mm/dd/yyyy format.
            dtMonth = dtArray[1];
            dtDay = dtArray[3];
            dtYear = dtArray[5];

            if (dtMonth < 1 || dtMonth > 12) {
                $("#SpanAnticipatedDateofUse").append('Correct Date format required');
                return false;
            } else if (dtDay < 1 || dtDay > 31) {
                $("#SpanAnticipatedDateofUse").append('Correct Date format required');
                return false;
            } else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31) {
                $("#SpanAnticipatedDateofUse").append('Correct Date format required');
                return false;
            } else if (dtMonth == 2) {
                var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
                if (dtDay > 29 || (dtDay == 29 && !isleap))
                    return false;
            }
            // return true;  
        }
         
        /*-------------------validation ends-----------------*/
         if (flag == 0)
        viacomTrademarkForm.clientContext.executeQueryAsync(viacomTrademarkForm.successInsertValues, viacomTrademarkForm.failure);
    },

    checkNullValidation: function(param, span) {
        if (param == null || param == "") {
        	var validationMessage="<span style='padding:0px; margin:0px; position:absolute; display: block'>*</span>";
            $('#Span' + span).append(validationMessage);
            return true;

        }
        return false;
    },
    show: function(paramvalue) {
        if (paramvalue.id == "AnticipatedDateofUse") {
            var span = "<span id ='clearSpan' style='width:215px; height:15px; display: block; padding:0px; margin:0px; position:absolute;'>Date format required in mm/dd/yyyy</span>";
        } else {
            var span = "<span id ='clearSpan' style='width:115px; height:15px; display: block; padding:0px; margin:0px; position:absolute; float:right;'>Cannot be blank</span>";
        }
        $("#Span" + paramvalue.id).append(span);

    },
    hide: function(param) {
        $('#clearSpan').remove();
    },
    populateDropDown:function(dropDownId){
    	viacomTrademarkForm.clientContextOnLoad = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl);
        viacomTrademarkForm.webOnLoad = viacomTrademarkForm.clientContextOnLoad.get_web();
        viacomTrademarkForm.oListOnLoad = viacomTrademarkForm.webOnLoad.get_lists().getByTitle('List Channel');
        viacomTrademarkForm.clientContextOnLoad.load(viacomTrademarkForm.oListOnLoad);
        viacomTrademarkForm.camlQuery = new SP.CamlQuery();
		collListItem = viacomTrademarkForm.oListOnLoad .getItems(viacomTrademarkForm.camlQuery);
		viacomTrademarkForm.clientContextOnLoad.load(collListItem);
        viacomTrademarkForm.clientContextOnLoad.executeQueryAsync(function() {viacomTrademarkForm.successPopulateDropDown(dropDownId)}, viacomTrademarkForm.failure);
    },
    successPopulateDropDown:function(dropDownId){
    	var listItemEnumerator = collListItem.getEnumerator();
    	var titleListChannel=[];	
		while (listItemEnumerator.moveNext()) 
		{	
			oListItem = listItemEnumerator.get_current();			 
			titleListChannel.push(oListItem.get_item('Title'));
			
		}		
		var select = document.getElementById(dropDownId);		
		for(var i =0; i <titleListChannel.length; i++) {
        var option = document.createElement('option');
        option.text = option.value = titleListChannel[i];
        select.add(option);
    }
		
    }
}

$(document).ready(function(){
	viacomTrademarkForm.populateDropDown("Channel");	
	viacomTrademarkForm.populateDropDown("ddlChannel");
});

/*---------------------Trademark Registrations form begins---------------------*/
viacomRegistrationForm={
	clientContext: null,
    web: null,
    camlQuery: null,
    oList: null,
    insertValues:function(){    
		viacomRegistrationForm.clientContext = new SP.ClientContext(_spPageContextInfo.siteAbsoluteUrl + "/Services/Bala");
        viacomRegistrationForm.web = viacomRegistrationForm.clientContext.get_web();
        viacomRegistrationForm.oList = viacomRegistrationForm.web.get_lists().getByTitle('TrademarkRegistrations');
        viacomRegistrationForm.clientContext.load(viacomRegistrationForm.oList);
        viacomRegistrationForm.clientContext.executeQueryAsync(viacomRegistrationForm.checkValidations, viacomRegistrationForm.failure);
        },
        
     checkValidations:function(){
     	viacomRegistrationForm.geoTerritory='';     
     	var flag=0;
 		viacomRegistrationForm.firstName = $('#FirstName').val();
        viacomRegistrationForm.lastName = $('#LastName').val();
        viacomRegistrationForm.phone = $('#Phone').val();
        viacomRegistrationForm.fax = $('#Fax').val();
        viacomRegistrationForm.email = $('#Email').val();
        viacomRegistrationForm.channel= $('#ddlChannel').val();
        viacomRegistrationForm.mark= $('#Mark').val();
        viacomRegistrationForm.correspondingTrademarkSearchResult = $('#ddlCorrespondingTrademarkSearchResult').val();
		viacomRegistrationForm.dateofFirstUse= $('#DateofFirstUse').val();
		$("input:checkbox[name=geoTerritory]:checked").each(function() {
            viacomRegistrationForm.geoTerritory+= $(this).val() + ';#';

        });
		viacomRegistrationForm.description= $('#Description').val();

       /*  if (viacomTrademarkForm.checkNullValidation(viacomRegistrationForm.firstName, 'FirstName') == true) {
            flag += 1;
        }

        if (viacomTrademarkForm.checkNullValidation(viacomRegistrationForm.lastName, 'LastName') == true) {
            flag += 1;
        }
        if (viacomTrademarkForm.checkNullValidation(viacomRegistrationForm.email, 'Email') == true) {
            flag += 1;

        }
		if (viacomTrademarkForm.checkNullValidation(viacomRegistrationForm.phone, 'Phone') == true) {
            flag += 1;

        }
		if (viacomTrademarkForm.checkNullValidation(viacomRegistrationForm.channel, 'Channel') == true) {
            flag += 1;

        }
		if (viacomTrademarkForm.checkNullValidation(viacomRegistrationForm.mark, 'Mark') == true) {
		            flag += 1;
		
		 }
		if (viacomTrademarkForm.checkNullValidation(viacomRegistrationForm.description, 'Description') == true) {
		            flag += 1;
		
		 }
		if (viacomTrademarkForm.checkNullValidation(viacomRegistrationForm.dateofFirstUse, 'DateofFirstUse') == true) {
		            flag += 1;
		
		 }
		 if(viacomRegistrationForm.email!= ""||viacomRegistrationForm.email!= null){ 
			var atpos = viacomRegistrationForm.email.indexOf("@");
	    	var dotpos = viacomRegistrationForm.email.lastIndexOf(".");
	    	if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=viacomRegistrationForm.email.length) {
	        $('#SpanEmail').append("Not a valid e-mail address");
	        return false;    }	
		}
		
        if (viacomRegistrationForm.dateofFirstUse!= null && viacomRegistrationForm.dateofFirstUse!= "") {
            var currVal = viacomRegistrationForm.dateofFirstUse;
            if (currVal == '')
                return false;            
            var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
            var dtArray = currVal.match(rxDatePattern); // is format OK?
            if (dtArray == null) {
                $("#SpanDateofFirstUse").append('Correct Date format required');
                return false;
            }
            //Checks for mm/dd/yyyy format.
            dtMonth = dtArray[1];
            dtDay = dtArray[3];
            dtYear = dtArray[5];

            if (dtMonth < 1 || dtMonth > 12) {
                $("#SpanDateofFirstUse").append('Correct Date format required');
                return false;
            } else if (dtDay < 1 || dtDay > 31) {
                $("#SpanDateofFirstUse").append('Correct Date format required');
                return false;
            } else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31) {
                $("#SpanDateofFirstUse").append('Correct Date format required');
                return false;
            } else if (dtMonth == 2) {
                var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
                if (dtDay > 29 || (dtDay == 29 && !isleap))
                    return false;
            }
            // return true;  
        }
         
        /*-------------------validation ends-----------------*/
       // if (flag == 0)
        viacomRegistrationForm.clientContext.executeQueryAsync(viacomRegistrationForm.successInsertValues, viacomTrademarkForm.failure);

   },
   successInsertValues:function(){
   	 	var itemCreateInfoTrademarkRegistrations = new SP.ListItemCreationInformation();
        createItem = viacomRegistrationForm.oList.addItem(itemCreateInfoTrademarkRegistrations);
        createItem.set_item('LastName', viacomRegistrationForm.lastName);
        createItem.set_item('FirstName', viacomRegistrationForm.firstName);
        createItem.set_item('Phone', viacomRegistrationForm.phone);
        createItem.set_item('Fax', viacomRegistrationForm.fax);
        createItem.set_item('Email', viacomRegistrationForm.email);
        createItem.set_item('Channel', viacomRegistrationForm.channel);
        createItem.set_item('Mark', viacomRegistrationForm.mark);
        createItem.set_item('DescriptionOfUse', viacomRegistrationForm.description);
        createItem.set_item('GeographicalTerritoryOfUse', viacomRegistrationForm.geoTerritory);
        createItem.set_item('DateOfFirstUse', viacomRegistrationForm.dateofFirstUse);
        createItem.set_item('TrademarkSearchResultsReceivedFr',viacomRegistrationForm.correspondingTrademarkSearchResult);

        createItem.update();
        viacomRegistrationForm.clientContext.load(createItem);
        viacomRegistrationForm.clientContext.executeQueryAsync(viacomRegistrationForm.success, viacomTrademarkForm.failure);

   },
   success:function()
   {
   alert("Thank You. Your form has been submitted successfully.");
   }

}
