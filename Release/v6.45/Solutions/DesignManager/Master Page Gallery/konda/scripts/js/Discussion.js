﻿(function () {
    var favoriteColorContext = {};

    // You can provide templates for:
    // View, DisplayForm, EditForm and NewForm
    favoriteColorContext.Templates = {};
    //itemCtx.Templates.Header = “<div><b>Announcements</b></div><table>”;
favoriteColorContext.Templates.Item = favoriteColorViewTemplate;
//itemCtx.Templates.Footer = “</table>”;



    SPClientTemplates.TemplateManager.RegisterTemplateOverrides(
        favoriteColorContext
        );
})();

// The favoriteColorViewTemplate provides the rendering logic
// the custom field type when it is displayed in the view form.
function favoriteColorViewTemplate(ctx) {
/*ADFS Alert Issue*/console.log(JSON.stringify(ctx.CurrentItem));
var html="";
if(ctx.CurrentItem.ContentType == "Discussion")
{
html = 	"<div>"+
				"Comments :"+ctx.CurrentItem.ItemChildCount+
			"</div>"+
			"<div>"+
				"Likes :"+ctx.CurrentItem.LikesCount+
			"</div>"+
			"<div>"+
				"Body :"+ctx.CurrentItem.Body+
			"</div>"+
			"<div>"+
				ctx.CurrentItem.Modified+
			"</div>";
}
if(ctx.CurrentItem.ContentType == "Message")
{
html = 	"<div>"+
			ctx.CurrentItem.Body	
	    "</div>";
}

    return html;
}

