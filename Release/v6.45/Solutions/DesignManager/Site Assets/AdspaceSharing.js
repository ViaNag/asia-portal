	
var listID;
var listItemIDs;
var fileItem;
var fileCountCheck = 0;
var FileItems = [];
var web;
var loginName;
var clientContext = new SP.ClientContext();
var siteColl;
var contentnameresult = false;
var ExternalSharingInterface = {};
ExternalSharingInterface.query = '';
var listitemids = getParameterValues('SPListItemId');
// create an empty array
var currentUser = '';
 function concatval() {
            alert("Content Group already exists , Please select from dropdown or else enter a new group name.");
            return false;
        }
		
		  function dia() {
            $("#termscond").dialog({
                modal: true,
                minHeight: 'auto',
                maxHeight: 'auto',
                width: 400,
                resizable: false,
                draggable: false,
                buttons: {
                    Submit: function (event, n) {
                        if (!$("#field_terms").prop("checked")) {
                            
                            alert("Please accept terms and conditions");
                            event.preventDefault();
                            $("#lblcheck").css("color", "red");
                        } else {
                            $(this).dialog("close");
                            $("#termsbtn").click();
                        }
                    },
                    Cancel: function () {
                        $(this).dialog('destroy');
                    }
                },
            });
            $(".ui-dialog-buttonpane button:contains('Cancel')").attr("disabled", false)
            $(".ui-dialog-buttonpane button:contains('Submit')").attr("disabled", false);
            $(".ui-dialog-titlebar").hide();
        }
        $(function () {
            $('#field_terms').change(function () {
                //$('#terms').toggle(this.checked);
                $('#lblerrormsg').hide();
                $("#lblcheck").css("color", "black");
            }).change(); //ensure visible state matches initially
        });
        var linkBtnAddMoreClk = '';
        var dtCh = "/";
        var minYear = 1900;
        var maxYear = 2100;

        function isInteger(s) {
            var i;
            for (i = 0; i < s.length; i++) {
                // Check that current character is number.
                var c = s.charAt(i);
                if (((c < "0") || (c > "9"))) return false;
            }
            // All characters are numbers.
            return true;
        }

        function stripCharsInBag(s, bag) {
            var i;
            var returnString = "";
            // Search through string's characters one by one.
            // If character is not in bag, append to returnString.
            for (i = 0; i < s.length; i++) {
                var c = s.charAt(i);
                if (bag.indexOf(c) == -1) returnString += c;
            }
            return returnString;
        }

        function daysInFebruary(year) {
            // February has 29 days in any year evenly divisible by four,
            // EXCEPT for centurial years which are not also divisible by 400.
            return (((year % 4 == 0) && ((!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28);
        }

        function DaysArray(n) {
            for (var i = 1; i <= n; i++) {
                this[i] = 31
                if (i == 4 || i == 6 || i == 9 || i == 11) {
                    this[i] = 30
                }
                if (i == 2) {
                    this[i] = 29
                }
            }
            return this
        }

        function isDate(dtStr) {
            var daysInMonth = DaysArray(12)
            var pos1 = dtStr.indexOf(dtCh)
            var pos2 = dtStr.indexOf(dtCh, pos1 + 1)
            var strMonth = dtStr.substring(0, pos1)
            var strDay = dtStr.substring(pos1 + 1, pos2)
            var strYear = dtStr.substring(pos2 + 1)
            strYr = strYear
            if (strDay.charAt(0) == "0" && strDay.length > 1) strDay = strDay.substring(1)
            if (strMonth.charAt(0) == "0" && strMonth.length > 1) strMonth = strMonth.substring(1)
            for (var i = 1; i <= 3; i++) {
                if (strYr.charAt(0) == "0" && strYr.length > 1) strYr = strYr.substring(1)
            }
            month = parseInt(strMonth)
            day = parseInt(strDay)
            year = parseInt(strYr)
            if (pos1 == -1 || pos2 == -1) {
                alert("The date format should be : mm/dd/yyyy")
                return false
            }
            if (strMonth.length < 1 || month < 1 || month > 12) {
                alert("Please enter a valid month")
                return false
            }
            if (strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year)) || day > daysInMonth[month]) {
                alert("Please enter a valid day")
                return false
            }
            if (strYear.length != 4 || year == 0 || year < minYear || year > maxYear) {
                alert("Please enter a valid 4 digit year between " + minYear + " and " + maxYear)
                return false
            }
            if (dtStr.indexOf(dtCh, pos2 + 1) != -1 || isInteger(stripCharsInBag(dtStr, dtCh)) == false) {
                alert("Please enter a valid date")
                return false
            }
            return true
        }

        function CheckUnCheckChildern(id) {
            var grid;
            var cell;
            var cellIndex = 0;
            if (grid.rows.length > 0) {
                for (i = 1; i < grid.rows.length; i++) {
                    cell = grid.rows[i].cells[cellIndex];
                    for (j = 0; j < cell.childNodes.length; j++) {
                        if (cell.childNodes[j].type == "checkbox") {
                            cell.childNodes[j].checked = document.getElementById(id).checked;
                        }
                    }
                }
            }
        }

        function checkall(bx) {
            $('#grdSharingItems').find('input:checkbox').each(function () {
                
                cbs[i].checked = bx.checked;
            });
        }

        function echeck(str) {
            var at = "@"
            var dot = "."
            var lat = str.indexOf(at)
            var lstr = str.length
            var ldot = str.indexOf(dot)
            if (str.indexOf(at) == -1) {
                alert("Invalid E-mail ID : " + str)
                return false
            }
            if (str.indexOf(at) == -1 || str.indexOf(at) == 0 || str.indexOf(at) == lstr) {
                alert("Invalid E-mail ID : '" + str + "'")
                return false
            }
            if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0 || str.indexOf(dot) == lstr) {
                alert("Invalid E-mail ID : '" + str + "'")
                return false
            }
            if (str.indexOf(at, (lat + 1)) != -1) {
                alert("Invalid E-mail ID : '" + str + "'")
                return false
            }
            if (str.substring(lat - 1, lat) == dot || str.substring(lat + 1, lat + 2) == dot) {
                alert("Invalid E-mail ID : '" + str + "'")
                return false
            }
            if (str.indexOf(dot, (lat + 2)) == -1) {
                alert("Invalid E-mail ID : '" + str + "'")
                return false
            }
            if (str.indexOf(" ") != -1) {
                alert("Invalid E-mail ID : '" + str + "'")
                return false
            }
            return true
        }
		
		
        function validate_field() {
            var recepient = $("#PeoplePicker").val();
            var recepientclear = recepient.replace(/ /g, '');
            var recepientcheck = [];
            recepientcheck = recepientclear.split(',');
            var set_title = $("#ExSsetTitleTxt").val();
            var con_cat = $("#ExSContentGrpDrpDwn option:selected").text();
            var expdate = $("#datepicker").datepicker().val();
            var grdshares = $("#grdSharingItems").val();
			var selectedCG='';
            /*if (isDate(expdate) == false) {
                return false;
            }*/
			
            if (recepient == "") {
                alert("Please enter the recepients");
                return false;
            }
            if (set_title == "" && con_cat == "select") {
                alert("Please Enter A Title or Select a Content Category");
                return false;
            }
			if($("#datepicker").val() == "")
			{
			alert("Please select expiry date");
                return false;
			}
			 if ($('#ExSsetTitleTxt').val() != '') 
			 {
                    selectedCG = $('#ExSsetTitleTxt').val();
             }
            else 
			{
                    selectedCG = $('#ExSContentGrpDrpDwn').val();
            }
			
			if(selectedCG.trim()==""  || selectedCG=="Select")
			{
				alert("Please create or select a content group");
				return false;
			}
            for (i = 0; i <= recepientcheck.length; i++) {
                if (recepientcheck[i] != "" && recepientcheck[i] != null) {
                    if (echeck(recepientcheck[i]) == false) {
                        return false
                    }
                }
            }
            
			dia();
            var isOpen = $("#termscond").dialog("isOpen");
             if (isOpen) {
                return false;
             }
			
			ExternalSharingInterface.ShareItem ();
        }

        function closeParentDialog(refresh) {
            var target = parent.postMessage ? parent : (parent.document.postMessage ? parent.document : undefined);
            if (refresh) target.postMessage('CloseCustomActionDialogRefresh', '*');
            else target.postMessage('CloseCustomActionDialogNoRefresh', '*');
        }

      

        function CheckAllEmp(Checkbox) {
            var GridVwHeaderChckbox = document.getElementById("grdSharingItems");
            for (i = 1; i < GridVwHeaderChckbox.rows.length; i++) {
                GridVwHeaderChckbox.rows[i].cells[1].getElementsByTagName("INPUT")[0].checked = Checkbox.checked;
            }
        }
$(document).ready(function(){


	if ($("#ExSsetTitleTxt").val() == "" && $("#ExSContentGrpDrpDwn").val() == "Select")
    {	
        $("#hladdmore").prop('disabled', true);
    }
  
	listItemIDs = GetUrlKeyValue("SPListItemId");
	listID = GetUrlKeyValue("SPListId");
	SharingItemPopulation(listID,listItemIDs);
	FindCurrentUser();
  
	$( "#datepicker" ).datepicker(); 

	
	$(".deleterow").on("click", function(){
		var $killrow = $(this).parent('tr');
			$killrow.addClass("danger");
			$killrow.fadeOut(2000, function(){
				$(this).remove();
			});
	});
	
	$('#ExSContentGrpDrpDwn').change(function() {
		 //alert($(this).find(":selected").text());
		 var selectedGroup = $(this).find(":selected").text();
		 PopulateContentGrpView(selectedGroup)
	});
	
	$("#ExSsetTitleTxt").blur(function () {

        //alert("Handler for .blur() called.");
        onContentGroupTextChanged();
    });
	$("#termsbtn").on("click", function(){
		ExternalSharingInterface.ShareItem ();
	});
	
		$('#cancelBtn').click(function() {	
			
    		RedirectToSource();
		});
	
});

function RedirectToSource()
{
		$.ajax({		
					url: _spPageContextInfo.webAbsoluteUrl + "/_api/Web/Lists/GetByTitle('" + listTitle + "')/Forms?$select=ServerRelativeUrl&$filter=FormType eq 4",		
					type: "GET",		
					headers: {		
						"accept": "application/json;odata=verbose",		
					},		
					success: function (data) {		
						$.each(data.d.results, function (index, item) {		
							var docURL = data.d.results[0].ServerRelativeUrl;		
							var lengthurl=docURL.split('/', 3).join('/').length;		
							docURL=docURL.substring(0, lengthurl);		
							window.location=_spPageContextInfo.siteAbsoluteUrl + docURL;		
						});		
								
					},		
					error: function (error) { }		
				});
}

function onContentGroupTextChanged() {
    try {
        //***** check whether content group exists or not *****
        contentnameresult = false;
        $.ajax({
            url: _spPageContextInfo.webAbsoluteUrl + "/_api/Web/Lists/GetByTitle('Externally shared Content')/Items",
            type: "GET",
            headers: {
                "accept": "application/json;odata=verbose",
            },
            success: function (data) {
                $.each(data.d.results, function (index, item) {
                    if (item.ContentGroup != null) {           
                        contentnameresult = item.ContentGroup.toUpperCase() === $('#ExSsetTitleTxt').val().toUpperCase();
                        if (contentnameresult == true) {
                            return false;
                        }
                        else {
                            contentnameresult = false;
                        }
                    }
                });
                if ($('#ExSsetTitleTxt').val() != "" || $('#ExSsetTitleTxt') != null) {
                    $("#hladdmore").prop('disabled', false);
                    $('#ExSContentGrpDrpDwn').val('Select');
                    $('#ExSContentGrpDrpDwn').change();
                    // ExSContentGrpDrpDwn_SelectedIndexChanged(sender, e);
                }
            },
            error: function (error) { }
        });

       

    }
    catch (ex) { /*ADFS Alert Issue*/console.log(ex); }

}




function FindCurrentUser()
{
    var userid = _spPageContextInfo.userId;
  var requestUri = _spPageContextInfo.webAbsoluteUrl + "/_api/web/getuserbyid(" + userid + ")";
  var requestHeaders = { "accept" : "application/json;odata=verbose" };
  $.ajax({
    url : requestUri,
    contentType : "application/json;odata=verbose",
    headers : requestHeaders,
    success : onSuccess,
    error : onError
  });

  
  function onSuccess(data, request){
    loginName = data.d.Title;
    //alert(loginName);
	PopulateDropDownContol();
  }

  function onError(error) {
    /*ADFS Alert Issue*/console.log("error");
  }

}

function PopulateContentGrpView(selectedGroup)
{
				$("#Messagelbl").text("");
                if ($("#ExSContentGrpDrpDwn").val() != "Select")
                {
					$("#ExSsetTitleTxt").val("");
					$("#Messagelbl").text("")
                }

                //***Give add more link*******
                if ($("#ExSsetTitleTxt").val() == "" && $("#ExSContentGrpDrpDwn").val() != "Select")
                {
                    $("#hladdmore").prop('disabled', false);
					$("#Messagelbl").text("The notification email will be sent to all members of the selected content group, as well as to any users listed in 'TO' field");
                }


var allValues1=[];
	grddocviewers.textContent = ""
	var listName = "Externally shared Content";
	    $.ajax({
        url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('" + listName + "')/items"+"?$filter=((ContentGroup eq '"+selectedGroup+"') and (ExternallySharedContentStatus ne 'ToBeDeleted'))",
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {	
		//alert("Found Filtered Data");	
		//	console.log(data.d.results.length);
			var collLength = data.d.results.length;
			for (var i = 0; i < data.d.results.length; i++) {
			var filename = data.d.results[i].Name;
			var filePath = data.d.results[i].FilePath;
			
			if ((filename != undefined || filename != null) && (filePath != undefined || filePath != null))
			{
				SharingGridItems(filename,filePath)
			}
			
			var ModifiedDate = data.d.results[i].Modified;
			fileArray.push({
								FileName:filename,
								FileUrl: filePath,
								ModifiedDate:ModifiedDate
							});
			
			var userNames = data.d.results[i].SharedWith;
			
				
				if(userNames.indexOf(",") > 0)
				{
					var userArray = userNames.split(",");
					for (var k = 0; k < userArray.length; k++) 
					{
						if(!containsString(allValues1,userArray[k]))
						{
						allValues1.push(userArray[k]);
						var row = $("<tr />")
						$("#grddocviewers").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
						row.append($("<td>" + userArray[k] + "</td>"));
						row.append($("<td class='deleterow'>" + "<input title='Delete' class='Docdltbtn' id='Docdltbtn'  type='button' value='Remove User Sharing' onclick='SharedUserDeletion(this)'/>" + "</td>"));
						}
					}
				}
				else
				{
					if(!containsString(allValues1,data.d.results[i].SharedWith))
					{
						allValues1.push(data.d.results[i].SharedWith);
						var row = $("<tr />")
						$("#grddocviewers").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
						row.append($("<td>" + data.d.results[i].SharedWith + "</td>"));
						row.append($("<td class='deleterow'>" + "<input title='Delete' class='Docdltbtn' id='Docdltbtn'  type='button' value='Remove User Sharing' onclick='SharedUserDeletion(this)'/>" + "</td>"));
					}
				}
			
			
        }
        },
        error: function (data) {
		alert("Failed to fatch the data");
		           
        }
    });
}

var collListItem;
function PopulateDropDownContol()
{
var context = SP.ClientContext.get_current();
 var web = context.get_web();	
       var oList = web.get_lists().getByTitle('Externally shared Content');
       var camlQuery = new SP.CamlQuery();
	   camlQuery.set_viewXml("<View><Query><Where><Eq><FieldRef Name='SharedBy' /><Value Type='User'>" + loginName + "</Value></Eq></Where></Query></View>");
       collListItem = oList.getItems(camlQuery); 
       context.load(collListItem);
	   context.executeQueryAsync(Function.createDelegate(this, function () {
						//alert("success recieved");
						var listItemInfo = '';
						var allValues = [];
						var listItemEnumerator = collListItem.getEnumerator();
							
						while (listItemEnumerator.moveNext()) {
							var oListItem = listItemEnumerator.get_current();
							var ContentGroup = oListItem.get_item('ContentGroup');
							if(!containsString(allValues,oListItem.get_item('ContentGroup')))
							{
							
							
							allValues.push(ContentGroup);
							$("#ExSContentGrpDrpDwn").append("<option value='"+ContentGroup+"'>"+ContentGroup+"</option>");
							}
							
						}
						UpdateTermAndConditions();
			}), onQueryFailed);


}

function UpdateTermAndConditions()
{
	var listName = "TermsAndConditions";
	    $.ajax({
        url: location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '') + "/_api/web/lists/getbytitle('" + listName + "')/items",
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {	
		//alert("Terms and conditions recieved");	
		var collLength = data.d.results.length;
			for (var i = 0; i < data.d.results.length; i++) {
			
				if (data.d.results[i].Title == "Terms")
								{
									$("#lblterms").html(data.d.results[i].custommessage);
								}
								if (data.d.results[i].Title == "ErrorMessage")
								{
									$("#lblerrormsg").html(data.d.results[i].custommessage);
								}
			}
        },
        error: function (data) {
		alert("Failed to fatch the data for term and conditions");
		           
        }
    });
}

var fileArray=[];
var listTitle;
function SharingItemPopulation(listID,listItemIDs)
{
        //siteColl = clientContext.get_web();
        this.myweb = clientContext.get_web();
        this.list = myweb.get_lists().getById(listID); //Edit the ID
        clientContext.load(list);
		clientContext.load(myweb);
		clientContext.executeQueryAsync(Function.createDelegate(this, function () {						
			
			var itemidArray = listItemIDs.split(",");
            for(var i = 0; i < itemidArray.length;i++) {
                        itemID = itemidArray[i];
						listTitle = list.get_title();	
						fileItem = list.getItemById(itemID);
						FileItems[i] = fileItem;
						//clientContext.load(FileItems[i],�Include(ContentType, File)�);
						clientContext.load(FileItems[i]);
			 }	
			clientContext.executeQueryAsync(Function.createDelegate(this, function () {
						for (var j = 0; j < FileItems.length; j++) {
							var filename = FileItems[j].get_item('FileLeafRef');
							var itemID = FileItems[j].get_item('ID');
							var fileurl = FileItems[j].get_fieldValues().FileRef;
							var isShareable = FileItems[j].get_item('Shareable');
							var ModifiedDate = FileItems[j].get_item('Modified');
							
							fileArray.push({
								FileName:filename,
								FileUrl: fileurl,
								ModifiedDate:ModifiedDate
							});
							console.log(fileArray);
							//alert("file name is =" + filename + "   and file url is = "+fileurl);
							SharingGridItems(filename, fileurl, formatDate(ModifiedDate), itemID, 'Pending',isShareable)
						}
			}), onQueryFailed);	
			
		}), onQueryFailed);
}

	function SharingGridItems(filename,fileurl,ModifiedDate,itemID,status,isShareable) {
		var ErrMsg="Not Shareable";
        var fileanchor = "<a id='"+itemID+"' href='"+ fileurl+"' target='_blank'>"+ filename +"</a>";
		var row = $("<tr />")
			$("#personDataTable").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
			row.append($("<td class='code'>" + "<img src=\"" + GetIcon(filename) + "\"/>" + "</td>"));
			row.append($("<td class='code'>" + fileanchor + "</td>"));
			row.append($("<td class='code'>" + ModifiedDate + "</td>"));
			row.append($("<td class='code'>" + status + "</td>"));
			row.append($("<td class='checkboxvalue'>" + "<input type='checkbox' name='Download' value='Download'>" + "</td>"));
			if(isShareable)
			{
			row.append($("<td class='deleterow'>" + "<input title='Delete' class='dltbtn' id='dltbtn'  type='button' value='DeleteFile' onclick='docViewItemDeleting(this)'/>" + "</td>"));
			}
			else
			{
			row.append($("<td class='code'>" + ErrMsg + "</td>"));
			}
		
    }

function onQueryFailed(sender, args) {
        if(console != undefined && console != null){
            console.log('Error :' + args.get_message());}
    }
function oneventFailure()
{
/*ADFS Alert Issue*/console.log("failed");
}
	
 function getFailed(sender, args) {
        /*ADFS Alert Issue*/console.log('Failed = '+args.get_message());
    } 	



 function drawTable(data) {
        for (var i = 0; i < data.d.results.length; i++) {
            //drawRow(data.d.results.length[i]);
			var row = $("<tr />")
			$("#personDataTable").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
			row.append($("<td>" + data.d.results[i].AdSpaceName + "</td>"));
			row.append($("<td>" + data.d.results[i].ContentGroup + "</td>"));
			row.append($("<td>" + data.d.results[i].EditExpiry + "</td>"));
        }
    }
	


		var counter =0;
ExternalSharingInterface.ShareItem = function () {
//alert("in");
  
    var i;
    var fName = "";
    var fPath = "";
    var contentcat = "";
    var peopleUpdate;
    var shareds = "";
    var selPeopletext = "";
    var nameresult;
    var finaluser = "";
    var newusers = "";
    var date = "";
    var ip;
    var clientContext;
    var listitemids = getParameterValues("SPListItemId");
    var listid = getParameterValues("SPListId");
	var table;
	var numberOfFile;
   
    try {
        contentcat = $("#ExSContentGrpDrpDwn").val();
        i = 0;
	
        var listitemid = listitemids.split(',');
        	table = $("#personDataTable");
			var MyRows = $('table#personDataTable').find('tbody').find('tr');
			numberOfFile = MyRows.length;
			if(contentnameresult==false)
			{
			   for (var k = 1; k < MyRows.length; k++) 
			   {
					var MyIndexValue = $(MyRows[k]).find('td:eq(1)').html();
					fPath = $(MyIndexValue).attr('href');
					fName = $(MyIndexValue).text();
					var chk = $(MyRows[k]).find('input:checkbox')[0].checked;
					var itemID = $(MyIndexValue).attr('id');
					//alert(fName + "       " + fPath + "      " + itemID + "   "+chk)
						var actionColumn=	$(MyRows[k]).find('td:eq(5)');
						if($(actionColumn).html()==="Not Shareable")
						{
						alert(fName +" can't be shared!",true);
						}
						else
						{
						if (contentcat != "Select") {
							//*****If content Group is selected*****
							//*****If content Group is selected*****
							$.ajax({
							async: false,
								url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/GetByTitle('Externally shared Content')/Items?$filter=((AdSpaceName eq " + "'"+fName+"') and (ContentGroup eq '"+contentcat+"'))",
								type: "GET",
								headers: {
									"accept": "application/json;odata=verbose",
								},
								success: function (data) {
								//alert("success to find data with length =." + data.d.results.length);
									if (data.d.results.length > 0) {
										//CreateAccountForExternalUsers();                      commented by sanjay for dublicacy test
										peopleUpdate = false;
										$.each(data.d.results, function (index, item) { 
										if (listitemid[i] == item.ItemID) {
													i++;
												}
										 shareds = item.SharedWith;
												selPeopletext = $('#PeoplePicker').val();
												selPeopletext = selPeopletext.replace(" ", "");
												var selPeople = selPeopletext.split(',');
												//   var selPeoples = selPeople.Distinct().ToArray();
												selPeople = $.unique(selPeople);
												selPeople.sort();
											 
											  $.each(selPeople, function (index, sname) {

													//var str = shareds.match("\\b" + sname + "\\b" + "/gi");
													
													var sharedItemArray = shareds.split(',');
													if ($.inArray(sname, sharedItemArray) == -1)
													{
														
															if (shareds == "") {
																shareds = sname;
															}
															else {
																shareds = shareds + "," + sname;
															}
														
													}
													
												});
												
												finaluser = shareds;								
												var allowDwnld = chk;
												var message = $('#MessageTA').val();
												var Expirydate = $("#datepicker").datepicker().val();
												//alert("values are  "+ message + "   -----  " + Expirydate + "--------"+allowDwnld);
										
											$.ajax({
												url: _spPageContextInfo.webAbsoluteUrl + "/_api/Web/Lists/GetByTitle('Externally shared Content')/getItemById(" + item.ID + ")",
											   
													type: "POST",
													data: JSON.stringify({
														'__metadata': { 'type': 'SP.Data.Externally_x0020_shared_x0020_ContentListItem' }, 
														'SharedWith': finaluser,
														'ExternallySharedContentStatus': 'Pending',
														'AllowDownload': allowDwnld,
														'Message': message,
														'ExpiresOn' : Expirydate
													}),
													
																  
													headers: {
														"accept": "application/json;odata=verbose",
														"content-type": "application/json;odata=verbose",
														"X-RequestDigest": $("#__REQUESTDIGEST").val(),
														"X-HTTP-Method": "MERGE",
														"IF-MATCH": "*"
													},
													success: function (data) {
														RedirectToSource();
													/*	if(k = numberOfFile-1)
														{
															alert("successfully updated");															
														}	*/		
													},
													error: function (error) { }

												});
												peopleUpdate = true;
										});
										 if (!peopleUpdate) {

											/****access the shared content list to save values taken from the form***/
											CreateItemInExtSharingList(fName, fPath, itemID, chk,k);
											i++;
										}
									}
									else {
										/****access the shared content list to save values taken from the form***/
										//CreateAccountForExternalUsers();                   // commented by sanjay for duplicasy identification
										
										CreateItemInExtSharingList(fName, fPath, itemID, chk , k);
									   
									}
								},
								error: function (error) {
									/*ADFS Alert Issue*/console.log(JSON.stringify(error));
								}
							});
							
							}
							else
							{
								CreateItemInExtSharingList(fName, fPath, itemID, chk , k);
							}
							
							}
				
			}
			
			}
			else{
	concatval();
        
			}
    }//try
    catch (ex) {
        /*ADFS Alert Issue*/console.log(ex);
    }
}

function getParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) {
        var urlparam = url[i].split('=');
        if (urlparam[0] == param) {
            return urlparam[1];
        }
    }
}

function CreateAccountForExternalUsers() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/_layouts/15/SharePointProject3/ApplicationPage1.aspx/CreateAccountForExternalUsers",
        data: "{'extUserEmails':'" + $('#PeoplePicker').val() + "'}",
        dataType: "json",
        success: function (data) {
            var errormsg6 = "Username already exists. Please enter a different user name";
            var errormsg7 = "Email id already exists. Please enter a different Email id";
            var errormsgelse = "An unknown error occurred.";
            var errorid = data.d;
            if (errorid == 0) {
                ErrorLbl.Text += "User Account Creation Succesful  ||  ";
            }
            else if (errorid == 6) {

                alert(errormsg6 + " exists");
            }
            else if (errorid == 7) {
                alert(errormsg7 + " exists");

            }
            else {
                alert(errormsgelse + " exists");

            }
        },
        error: function (result) {
            /*ADFS Alert Issue*/console.log("Error");
        }
    });
}

function CreateItemInExtSharingList(fName, fPath, listItemID, chk , counter) {
    var newusers = "";
    var date = "";
    var ip;
    var clientContext;
    var listitemids = getParameterValues("SPListItemId");
    var listid = getParameterValues("SPListId");
    var sdSharedWith = [];

    try {
        getCurrentUser();
        var today = new Date();
        var thisDay = today.getDate();
        finaluser = "";
        newusers = $('#PeoplePicker').val();
        newusers = newusers.replace(" ", "");
        var selPeople = newusers.split(',');
        newuserarr = $.unique(selPeople);
        newuserarr.sort();

        $.ajax({
            url: _spPageContextInfo.webAbsoluteUrl + "/_api/Web/Lists/GetByTitle('Externally shared Content')/Items?$filter=ContentGroup eq '"+$('#ExSContentGrpDrpDwn').val()+"'",
            type: "GET",
            headers: {
                "accept": "application/json;odata=verbose",
            },

            success: function (data) {
                this.col1 = data.d.results.length;
                if (data.d.results.length > 0) {
                    $.each(data.d.results, function (index, item) {
                        var anames = item.SharedWith.split(',');
                        var lnames = anames.concat(newuserarr);
                        //  anames = email_accounts.ToArray();
                        $.each(lnames, function (index, aname) {
                            if (sdSharedWith[aname] === undefined) {
                                sdSharedWith.push({
                                    key: aname,
                                    value: aname
                                });
                            }
                        });
                    });
                }
                $.each(newuserarr, function (index, newusername) {
				var finaluserArray = finaluser.split(',');
				if ($.inArray(newusername, finaluserArray) == -1)
				{
                    if (finaluser == "") {
                        finaluser = newusername;
                    }
                    else {
                        finaluser = finaluser + "," + newusername;
                    }
				}
                });

                if (finaluser.endsWith(',')) {
                    ip = finaluser.lastIndexOf(',');
                    if (ip != -1) {
                        finaluser = finaluser.substring(0, ip - 1) + finaluser.substring(ip, finaluser.length);
                    }
                }

                if (this.col1 != 0) {
                    finaluser = "";
                    $.each(sdSharedWith, function (index, p) {

                        if (finaluser == "") {
                            finaluser = p.key;
                        }
                        else {

                            finaluser = finaluser + "," + p.key;
                        }

                    });

                    if (finaluser.endsWith(",")) {
                        ip = finaluser.lastIndexOf(",");
                        if (ip != -1) {
                            finaluser = finaluser.substring(0, ip - 1) + finaluser.substring(ip, finaluser.length);
                        }
                    }
                }


                finaluser = finaluser.replace(" ", "");
                var contentGroup = '';
                if ($('#ExSsetTitleTxt').val() != '') {
                    contentGroup = $('#ExSsetTitleTxt').val();
                }
                else {
                    contentGroup = $('#ExSContentGrpDrpDwn').val();
                }
                var allowDwnld = chk;
                /*
				if ($("#isAgeSelected").is(':checked')) {
                    allowDwnld = "Yes";
                }
				*/
				var message = $('#MessageTA').val();
				var Expirydate = $("#datepicker").datepicker().val();
				//alert("values are  "+ message + "   -----  " + Expirydate + "--------"+contentgroup);
				
                $.ajax({
                    url: _spPageContextInfo.webAbsoluteUrl + "/_api/Web/Lists/GetByTitle('Externally shared Content')/Items",
                    type: "POST",
                    contentType: "application/json;odata=verbose",
                    data: JSON.stringify({
                        '__metadata': { 'type': 'SP.Data.Externally_x0020_shared_x0020_ContentListItem' },

                        'Title' : fName,	
						'ContentGroup' : contentGroup,
						'SharedWith': finaluser,
                        'Message': message,
                        'AdSpaceName': fName,               
                        'ItemID': listItemID,
                        'ListName': listTitle,
						'FilePath' : fPath,
						'ExpiresOn' : Expirydate,
						'AllowDownload' : allowDwnld,
						'Source' : "Source Sample Data",
						'SharedById': _spPageContextInfo.userId,
						'ExternallySharedContentStatus' : 'Pending'
						}),

                    headers: {

                        "accept": "application/json;odata=verbose",
                        "X-RequestDigest": $("#__REQUESTDIGEST").val()
                    },
                    success: function (data) {
					RedirectToSource();
					
					/*if(counter = MyRows.length - 1)
					{
						alert("item created successfully");						
					}*/
					
					
					},
                    error: function (error) { }
                });
            },
            error: function (error) { /*ADFS Alert Issue*/console.log(Error); }

        });
    }
    catch (ex) {
        console.log(ex);
    }

}


function getCurrentUser() {
    var userid = _spPageContextInfo.userId;
    var requestUri = _spPageContextInfo.webAbsoluteUrl + "/_api/web/getuserbyid(" + userid + ")";

    var requestHeaders = { "accept": "application/json;odata=verbose" };

    $.ajax({
        url: requestUri,
        contentType: "application/json;odata=verbose",
        headers: requestHeaders,
        success: onSuccess,
        error: onError
    });
}

function onSuccess(data, request) {
    currentUser = data.d.LoginName;
    //alert(currentUser);
}

function onError(error) {
    /*ADFS Alert Issue*/console.log(error);
}		
	
$(".deleterow").on("click", function(){
	//alert("in");
	var $killrow = $(this).parent('tr');
    $killrow.addClass("danger");
	$killrow.fadeOut(2000, function(){
    $(this).remove();
});});

function docViewItemDeleting(btnObject) {

    var index;
    var fName = "";
    var deletedoc = false;

    try {
        
          
          var row = $(btnObject).parent().parent();
                          var MyIndexValue = $(row).find('td:eq(1)').html();
                              fName = $(MyIndexValue).text();
                
        //alert(fName + " removed ");

        $.ajax({
			
            url: _spPageContextInfo.webAbsoluteUrl + "/_api/Web/Lists/GetByTitle('Externally shared Content')/Items?$filter=(ContentGroup eq '"+$('#ExSContentGrpDrpDwn').val()+"') and (AdSpaceName eq '" + fName + "')",

            type: "GET",
            headers: {
                "accept": "application/json;odata=verbose",
            },

            success: function (data) {


                if (data.d.results.length != 0) {
                    deletedoc = false;
                    $.each(data.d.results, function (index, item) {
                        $.ajax({

                            url: _spPageContextInfo.webAbsoluteUrl + "/_api/Web/Lists/GetByTitle('Externally shared Content')/getItemById(" + item.ID + ")",
                            type: "POST",

                            data: JSON.stringify({
                                '__metadata': { 'type': 'SP.Data.Externally_x0020_shared_x0020_ContentListItem' }, 
								'ExternallySharedContentStatus': 'ToBeDeleted'
                            }),

                            headers: {

                                "accept": "application/json;odata=verbose",
                                "content-type": "application/json;odata=verbose",
                                "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                                "X-HTTP-Method": "MERGE",
                                "IF-MATCH": "*"
                            },

                            success: function (data) {
                                alert("soft delete successfull");
                                if (!deletedoc)
                                {

                                 $(row).remove();            
                            }},

                            error: function (error) { }

                        });
                    });

                }
            },
            error: function (error) { }
        });
    }
    catch (ex) { /*ADFS Alert Issue*/console.log(ex); }
}

function SharedUserDeletion(btnObject) {
	var collListItem;
    var index;
    var uName = "";
    var deleteuser;
    var shrdWithNames;
    try {
        uName = "test";
        alert("Are you sure you want to remove " + uName + " ?", true);
		
		var row = $(btnObject).parent().parent();
        var uName = $(row).find('td:eq(0)').html();
		
		var context = SP.ClientContext.get_current();
		var web = context.get_web();	
       var oList = web.get_lists().getByTitle('Externally shared Content');
       var camlQuery = new SP.CamlQuery();
	   camlQuery.set_viewXml("<View><Query><Where><And><Eq><FieldRef Name='ContentGroup'/><Value Type='Text'>" + $('#ExSContentGrpDrpDwn').val() + "</Value></Eq><Contains><FieldRef Name='SharedWith'/><Value Type='Text'>" + uName + "</Value></Contains></And></Where></Query></View>");
       collListItem = oList.getItems(camlQuery); 
       context.load(collListItem);
	   context.executeQueryAsync(Function.createDelegate(this, function () {
						//alert("success recieved");
						var listItemInfo = '';
						var listItemEnumerator = collListItem.getEnumerator();
						if(collListItem.get_count() != 0)
						{
								while (listItemEnumerator.moveNext()) {
								var oListItem = listItemEnumerator.get_current();
								var names = oListItem.get_item('SharedWith').split(',');
								
								 if (names.length == 1) {
										alert("Cannot remove the user as a document is currenty being shared only with this particular user", true);
										deleteuser = true;
									}
								else
								{
											if (oListItem.get_item('SharedWith').includes(uName)) {
												shrdWithNames = oListItem.get_item('SharedWith');
												shrdWithNames = oListItem.get_item('SharedWith').replace(uName + ",", "");
												shrdWithNames = shrdWithNames.replace("," + uName, "");
												shrdWithNames = shrdWithNames.replace(uName, "");
												//oListItem.get_item('SharedWith') = shrdWithNames;
												var itemID = oListItem.get_item('ID');
												//var cListItem = cList.getItemById(eventId);
												oListItem.set_item('SharedWith', shrdWithNames);
												oListItem.update();
												context.executeQueryAsync(Function.createDelegate(this, function () {
											    alert("updated successfully.");
												}), getFailed);
									}
								}
							}
						}
							
						
			}), getFailed);
		
		
		
		
       
    }
    catch (ex) {

    }

}
	function hladdmore_Click()
	{
	
	var contentGroup=$('#ExSContentGrpDrpDwn').val();
	$.ajax({
			
            url: _spPageContextInfo.webAbsoluteUrl + "/_api/Web/Lists/GetByTitle('Externally shared Content')/Items?$filter=ContentGroup eq '"+$('#ExSContentGrpDrpDwn').val()+"'",

            type: "GET",
            headers: {
                "accept": "application/json;odata=verbose",
            },

            success: function (data) {
$.each(data.d.results,function(index,item){

var selectedItemArray = listitemids.split(',');
                    if ($.inArray(item.ItemID, selectedItemArray) == -1)
                    {
$.ajax({
			
            url: _spPageContextInfo.webAbsoluteUrl + "/_api/Web/Lists/GetByTitle('"+item.ListName+"')/Items?$filter=ID eq '"+item.ItemID+"'",
async: false,
            type: "GET",
            headers: {
                "accept": "application/json;odata=verbose",
            },

            success: function (data) {
			var file=data.d.results[0];
			SharingGridItems(item.AdSpaceName,item.FilePath,file.Modified,file.ID,"Pending",item.Shareable);
			},
			error: function (data){}
			});
			
			}
			});


}
	,
	error: function(data){}
	});
	}

	
	function GetIcon(filename) {
	    var imgpath = '';
	    var file_extension = filename.substring(filename.lastIndexOf('.'));


	    switch (file_extension) {

	        case ".doc":
	        case ".docx":
	            imgpath = _spPageContextInfo.webAbsoluteUrl + "/_layouts/15/images/icdocx.png";
	            break;
	        case ".xls":
	        case ".xlsx":
	            imgpath = _spPageContextInfo.webAbsoluteUrl + "/_layouts/15/images/icxlsx.png";
	            break;
	        case ".txt":
	            imgpath = _spPageContextInfo.webAbsoluteUrl + "/_layouts/15/images/ictxt.gif";
	            break;

	        case ".zip":
	            imgpath = _spPageContextInfo.webAbsoluteUrl + "/_layouts/15/images/iczip.png";
	            break;
	        case ".ppt":
	        case ".pptx":
	            imgpath = _spPageContextInfo.webAbsoluteUrl + "/_layouts/15/images/icpptx.png";
	            break;
	        default:

	            imgpath = _spPageContextInfo.webAbsoluteUrl + "/_layouts/15/images/CNSPUB16.png";
	            break;
	    }
	    return imgpath;
	}

	function formatDate(d)
	{
	    date = new Date(d)
	    var dd = date.getDate(); 
	    var mm = date.getMonth()+1;
	    var yyyy = date.getFullYear(); 
	    if(dd<10){dd='0'+dd} 
	    if(mm<10){mm='0'+mm};
	    return d = dd+'/'+mm+'/'+yyyy
	}
	function containsString(strArray, text){
          var contains = false;
          for (var i=0; i<strArray.length; i++)
		  {
               if(strArray[i]==text)
			   {
			   contains = true;
			   break;
			   }
          
     }
	 return contains;
	 }
	

	
	

	



	
	
