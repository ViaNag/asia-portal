﻿var PlanonServiceURL;
$(window).load(function() {
		GetPlanlocationAndURL();
});

var OfficeLocations = {
		GetPlanOnLocationsStringifyData: function() {
			 var serviceURL=PlanonServiceURL+'PlanonRetrieveLocations?strDefault=default';
					crossDomainAjax(serviceURL, function (data) {
					OfficeLocations.GetLocations(JSON.parse(data));
				});
		},

        GetLocations: function (data) {
           try {

                 var size = data.length;
                 var USdata = "<div></div>";
                 var globalData = "<div></div>";
                 for (var i = 0; i < size; i++) {

                            var currentItem = data[i];

                            var country = currentItem.Country;
                            var address = currentItem.Address;
                            var state = currentItem.State;
                            var city = currentItem.City;
                            var zip = currentItem.Zip;
                            var fulladdress;
                            var flag = false;
                            var temp;

                            if (country == "United States") {
                                if (zip != " ")
                                    zip = ' - ' + zip;
                                fulladdress = '<li>' + '<strong>' + address + '</strong>, ' + city + zip + '</li>';
                                if ($(USdata).text() != "") {

                                    $(USdata).find('h3').each(function () {

                                        if ($(this).text() == state) {
                                            temp = $(USdata).html()
                                            flag = true;
                                            return false;
                                        }
                                    });


                                }
                                if (flag == true) {

                                    var divLength = temp.length;
                                    temp = temp.substring(0, divLength - 10);
                                    USdata = '<div>' + temp + fulladdress + '<ul></div>';

                                }
                                else {
                                    var header = "<h3 class='locationHeader'>" + state + "</h3>";
                                    fulladdress = '<ul>' + fulladdress + '</ul>';

                                    var divLength = USdata.length;
                                    USdata = USdata.substring(0, divLength - 6);
                                    USdata = USdata + header + fulladdress + "</div>";

                                }

                            }
                            else {
                                if (zip != " ")
                                    zip = ' - ' + zip;
                                fulladdress = '<li>' + '<strong>' + address + '</strong>, ' + city + state + zip + '</li>';
                                if ($(globalData).text() != "") {

                                    $(globalData).find('h3').each(function () {

                                        if ($(this).text() == country) {
                                            temp = $(globalData).html()
                                            flag = true;
                                            return false;

                                        }
                                    });

                                }
                                if (flag == true) {

                                    var divLength = temp.length;
                                    temp = temp.substring(0, divLength - 10);
                                    globalData = '<div>' + temp + fulladdress + '<ul></div>';

                                }
                                else {
                                    var header = "<h3 class='locationHeader'>" + country + "</h3>";
                                    fulladdress = '<ul>' + fulladdress + '</ul>';
                                    //fulladdress = '<ul><li>' + '<strong>' + address + '</strong>, ' + city + state + zip + '</li></ul>';

                                    var divLength = globalData.length;
                                    globalData = globalData.substring(0, divLength - 6);
                                    globalData = globalData + header + fulladdress + "</div>";

                                }

                            }

                    }

                        $('.USAddrContainer').append(USdata);
                        $('.globalAddrContainer').append(globalData);

                 }
				 catch (e) {
                 
			}
		}
    }
	// call with your url (with parameters) 
// 2nd param is your callback function (which will be passed the json DATA back)
function crossDomainAjax (url, successCallback) {

    // IE8 & 9 only Cross domain JSON GET request
    // if ('XDomainRequest' in window && window.XDomainRequest !== null) {

        // var xdr = new XDomainRequest(); // Use Microsoft XDR
        // xdr.open('get', url);
        // xdr.onload = function () {
            // var dom  = new ActiveXObject('Microsoft.XMLDOM'),
                // JSON = $.parseJSON(xdr.responseText);

            // dom.async = false;

            // if (JSON == null || typeof (JSON) == 'undefined') {
                // JSON = $.parseJSON(data.firstChild.textContent);
            // }

            // successCallback(JSON); // internal function
        // };
		  // xdr.ontimeout = function () { 
			// alert("ontimeout");
		  // };

        // xdr.onerror = function() {
			// alert("onerror");
            // _result = false;  
        // };

         // setTimeout(function () {    xdr.send();  }, 100);
    // } 
    // Do normal jQuery AJAX for everything else          
    // else {
        $.ajax({
            url: url,
            cache: false,
            dataType: 'json',
            type: 'GET',
            async: false, // must be set to false
            success: function (data, success) {
                successCallback(data);
            },
			error: function (data) {
				successCallback(data);
			}
        });
    // }
}

function GetPlanlocationAndURL() {
        var callConfig = $.ajax({
            url: "/_api/lists/getbytitle('config')/items?&$select=Title,sitepath&$filter=Title eq 'PlanonServiceUrl'",
            type: "GET",
            dataType: "json",
            headers: {
                Accept: "application/json;odata=verbose"
            }
        });
        callConfig.done(function (data, textStatus, jqXHR) {
            try {
                if (data.d.results.length > 0) {
                    PlanonServiceURL = data.d.results[0].sitepath;
					OfficeLocations.GetPlanOnLocationsStringifyData();
                }
            }
            catch (e) {
                /*ADFS Alert Issue*/console.log(e);
            }
        });
        callConfig.fail(function (jqXHR, textStatus, errorThrown) {
            /*ADFS Alert Issue*/console.log("Error getting Site Color" + jqXHR.responseText);
        });
    }