if ((Get-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue) -eq $null) {
    Add-PSSnapin "Microsoft.SharePoint.PowerShell"
}

# Traverses all files in a folder and publish
function GetFiles($Folder,$list)
{ 
   Write-Host "+"$Folder.Name

    foreach($listitemfile in $Folder.files)
	{	
		try
		{
			
           # $listitemfile.unpublish("");

			if($listitemfile.Level -ne "Published")
			{
				Write-Host "`t" $listitemfile.Name
				$listitemfile.publish("");
			}			
			
		}
		catch
		{
			Write-Host $_.Exception.Message
			Stop-Transcript
			Stop-SPAssignment -Global		
			Exit -1
		}
    
	 }
	 #Loop through all subfolders and call the function recursively
     foreach ($SubFolder in $Folder.SubFolders)
        {
		    if($SubFolder.Name -ne "Forms")
		    {  
			    Write-Host "`t" -NoNewline
				GetFiles($Subfolder,$list)
				 
			}
		}
 }

 # gets the list from site
function approveContent ($w, $listName,$folderName ) {
  $list = $w.Lists |? {$_.Title -eq $listName}
  if($folderName -eq $null -or $folderName -eq "")
  {
    $Folder = $list.RootFolder
  }
  else
  {
    $Folder = $list.RootFolder.SubFolders[$folderName]
  }
    
    GetFiles $Folder $list
 
}
 

# reads the configuration xml for publishing file
function PublishFile([String]$ConfigFileName = "")
{
	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$publishXML =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green 
	   try
		{
		$siteurl=$publishXML.Publish.Site
		$site=  Get-SPSite -Identity $siteurl
		$web= $site.RootWeb 

		Write-Host "Processing site: " $web.Url -ForegroundColor Yellow
		$publishXML.Publish.List|
		ForEach-Object {
		$ListName= $_.Name
		$FolderName=$_.Folder
		approveContent $web $ListName $FolderName

		}

		Write-Host $ListName  "Files Published Succesully" -ForegroundColor Green 
		}
		catch
		{
		Write-Host "Exception. Exiting script..." -ForegroundColor Red        
		Stop-Transcript
		Stop-SPAssignment -Global		
		Exit -1
		}

}