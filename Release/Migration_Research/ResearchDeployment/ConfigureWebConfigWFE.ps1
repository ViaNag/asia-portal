﻿# =================================================================================
#
# FUNC: 	Get-ScriptLogFileName
# DESC:	Gets the log file nme based on invocation script name
#		Suffice the script with execution date and returns filename string 
#		in format <<Log File Name>>_<<ddMMMyyyy>>.log
#
# =================================================================================
function Get-ScriptLogFileName
{
	if($myInvocation.ScriptName) 
	{ 
		[string]$scriptName = $myInvocation.ScriptName
		$scriptName = $scriptName.Substring($scriptName.LastIndexOf('\')+1)
		$currentDate = Get-Date -Format "ddMMMyyyy"
		$logFileName = $scriptName.Replace(".ps1","_" + $args[0] + "_" + $currentDate  + ".log")

		Return $logFileName
	}
	else 
	{ 
		[string]$scriptName = $myInvocation.MyCommand.Definition 
		$scriptName = $scriptName.Substring($scriptName.LastIndexOf('\')+1)
		$currentDate = Get-Date -Format "ddMMMyyyy"
		$logFileName = $scriptName.Replace(".ps1","_" + $currentDate  + ".log")

		Return $logFileName
	}
}

#check the parameters
if($args.Count -lt 2)
{ 
    Write-Host "Usage: .\ConfigureWebConfigWFE.PS1 <<Folder name containing configuration for Webapplication config file in configuration folder>><<Default Web Application URL>>" 
    Exit
}

$foldername = $args[0]
$webApplicationURL=$args[1]

$logFileName = Get-ScriptLogFileName
$logFileName = ".\Logs\" + $logFileName
$Error.Clear()

Start-Transcript -Path $logFileName 


#check the parameters
if($args.Count -lt 2)
{ 
    Write-Host "Usage: .\ConfigureWebConfigWFE.PS1 <<Folder name containing configuration for Webapplication config file in configuration folder>><<Default Web Application URL>>" 
    Exit
}

[string] $currentLocation = Get-Location

try
{
	$assemblyPath = $currentLocation + "\Assembly\Deployment.dll"
	Add-type -Path $assemblyPath

	#Process Web Config file for Web Application
	Write-Host "Starting processing Web Config file for Web Application." -ForegroundColor Green
	$settingsPath = $currentLocation + "\Configuration\" + $foldername + "\WebAppFunc.xml"
	$settings = Get-Content $settingsPath
	[WebConfigHelper]::Configure($settings,$webApplicationURL)
	Write-Host "Success: processing Web Config file for Web Application." -ForegroundColor Green
}
Catch
{
	Write-Host "Error: processing Web Config file for Web Application." -ForegroundColor Red
	Write-Host "Error Message : " $_.Exception.ToString()
	Stop-Transcript
	Exit
}

Stop-Transcript
