<script type = "text/javascript">

var worldClock ={
	 citiesCount:0,
	 userProfileProperty:"WorldClockKonda",
	 ifarmeSrc:null,		
	openPopup: function(){
			worldClock.openDialog(_spPageContextInfo.webAbsoluteUrl+"/Pages/WorldClockCustomization.aspx?isDlg=1","Customize World Clock",'1175','550');
			},
	SetWorldClock: function()
	{ 
			var WordClockKondaTestValue = "";   
			$.ajax({  
	  
				url: _spPageContextInfo.webAbsoluteUrl + "/_api/SP.UserProfiles.PeopleManager/GetMyProperties",  
				headers: { Accept: "application/json;odata=verbose" },  
				success: function (data) {  
					try {  
						var properties = data.d.UserProfileProperties.results;
						for (var i = 0; i < properties.length; i++) {
							var property=properties[i];
							if (property.Key == worldClock.userProfileProperty) {  
							 WordClockKondaTestValue = property.Value;
												 
							 var label="";
							 $.each(WordClockKondaTestValue.split(","),function(i,val){
							 
							 if(val!="")
							 {
							label+=val.split("@#@")[0]+","
							 }
							 });
							  if(label.charAt(label.length-1)==",")
									 {
									 label = label.substring(0, label.length - 1);
									 } 
									 WordClockKondaTestValue=label;
							}  
						  
						}
						
						if(WordClockKondaTestValue=="")
	{
	 
	  var listName="Master Cities";
	  $.ajax({  
				url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('" + listName + "')/GetItems(query=@v1)?@v1={'ViewXml':'<View><Query></Query></View>'}",  
		 type: "POST",
			   headers: {
			"Accept": "application/json;odata=verbose",
			"X-RequestDigest": $("#__REQUESTDIGEST").val()
		},  
				success: function (data) {  
					try {  
						 
				worldClock.citiesCount = data.d.results.length; 
						for (var i = 0; i < data.d.results.length; i++) {
							var item=data.d.results[i];
							
							WordClockKondaTestValue += item.LocationClockCode+",";
									 }
									 if(WordClockKondaTestValue.charAt(WordClockKondaTestValue.length-1)==",")
									 {
									 WordClockKondaTestValue = WordClockKondaTestValue.substring(0, WordClockKondaTestValue.length - 1);
									 }
									 worldClock.ifarmeSrc="http://localtimes.info/timediff.php?lcid="+WordClockKondaTestValue+ "&cp=000000,FFFFFF&uc=0";
									   worldClock.ShowCurrentTime();
						
					} catch (err2) {  
					   console.log(JSON.stringify(err2));  
					}  
				},  
				error: function (jQxhr, errorCode, errorThrown) {  
					console.log(errorThrown);  
				}  
			}); 

	}
	else{
	var cities=[];
							 $.each(WordClockKondaTestValue.split(","),function(i,city){
							 if(city!="")
							 {
							 cities.push(city);
							 }
							 });
							 worldClock.citiesCount = cities.length;
							 worldClock.ifarmeSrc="http://localtimes.info/timediff.php?lcid="+WordClockKondaTestValue +"&cp=000000,FFFFFF&uc=0";
							 worldClock.ShowCurrentTime();
								
	}

					} catch (err2) {  
					   console.log(JSON.stringify(err2));  
					}  
				},  
				error: function (jQxhr, errorCode, errorThrown) {  
					console.log(errorThrown);  
				}  
			});  
	  },
	  
	openDialog: function(navUrl,pageTitle,objWidth,objHeight)
	{
	var myvar = navUrl;
	var options =
	{
	url: myvar,
	width: objWidth,
	height: objHeight,
	title: pageTitle,
	dialogReturnValueCallback: RefreshOnDialogClose  
	};
	SP.UI.ModalDialog.showModalDialog(options);
	},
	callBackFunction: function ()
	{

	},

	ShowCurrentTime:function() {
		$.ajax({
			type: "POST",
			url: _spPageContextInfo.siteAbsoluteUrl+"/_layouts/15/worldclock/worldclock.aspx/GetCurrentTime",
			data: '{URL: "' +worldClock.ifarmeSrc +'"}',
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: worldClock.OnSuccess,
			failure: function(response) {
				alert(response.d);
			}
		});
	},
	
	OnSuccess: function(response) {
		var html_string= response.d;
		html_string=html_string.replace("/js_difference/clock1.min.js", _spPageContextInfo.siteAbsoluteUrl+"/_catalogs/masterpage/Research/js/UpdateTime.js");
		html_string=html_string.replace("/css_difference/difference_widget.css",_spPageContextInfo.siteAbsoluteUrl+"/_catalogs/masterpage/Research/css/WorldClockWidgets.css");
		
		document.getElementById('iWorldClock').contentWindow.document.write(html_string);
		$("#WordClockKondaTest iframe").attr('height',worldClock.citiesCount*38);
	},

}


 $(document).ready(function(){
           worldClock.SetWorldClock();
		  
        });

		
</script>


<div id="WordClockKondaTest" style="width : 216px"> 

<table>
<tr><td><div style="width: 108px;margin-left:3px;margin-top:20px"><b>World Clock</b></div></td><td><div style="margin-top:20px"><a href="javascript:void(0)" onclick="worldClock.openPopup()">Customize</a></div></td></tr>

</table>
<div style="border-radius: 10px; overflow: auto;padding:0px 3px;padding-top:3px; width:214px; margin:auto; align:center; text-align:center; height:auto"><iframe id="iWorldClock" width="200"></iframe> 
</div>
</div>
</div>





