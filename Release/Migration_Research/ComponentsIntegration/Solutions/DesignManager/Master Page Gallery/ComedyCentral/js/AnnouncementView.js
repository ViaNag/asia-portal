var Viacom = Viacom || {};
Viacom.IAN = Viacom.IAN || {};

Viacom.IAN.customItemHtml= function (ctx) {
	
        var oTitle = ctx.CurrentItem.Title;		
		var url = String.format('{0}&ID={1}', ctx.displayFormUrl, ctx.CurrentItem.ID);
		//var oBody=ctx.CurrentItem.Body;
		var showChar = 100;  // How many characters are shown by default
		var ellipsestext = "...";
		//var parser = new DOMParser();
		//var doc = parser.parseFromString(oBody, "text/xml");
		//var content = doc.firstChild.textContent
		var value = ctx.CurrentItem.Body;
		var content = value.replace(/<(?:.|\n)*?>/gm, '');
		 if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(0, content.length);
 
            content = c + '<span title="'+h+'">'+ellipsestext+'</span>';
 
        }
		
		var accordionItemHtml='<ul class="webpart-list"><li style="list-style:none;"><h4><a href="'+url+'">'+oTitle+'</a></h4><div class="webpart-content">'+content+'</div></li></ul>'
        return accordionItemHtml;
    
};

Viacom.IAN.customHeaderHtml = function (ctx) {
		var accordionHeaderHtml="<div class='webpart-heading'><h3>Announcements <a href='"+ctx.listUrlDir+"' class='view-all-link'>View All</a></h3></div>";
        return accordionHeaderHtml;
 
};


(function () {
    var overrideCtx = {};
    overrideCtx.Templates = {};

    overrideCtx.Templates.Header = Viacom.IAN.customHeaderHtml;
    overrideCtx.Templates.Item = Viacom.IAN.customItemHtml;
    overrideCtx.BaseViewID = 1;
    overrideCtx.ListTemplateType = 104;

    SPClientTemplates.TemplateManager.RegisterTemplateOverrides(overrideCtx);
})();