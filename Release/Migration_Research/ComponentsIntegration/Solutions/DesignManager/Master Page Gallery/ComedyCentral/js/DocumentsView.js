var Viacom = Viacom || {};
Viacom.IAN = Viacom.IAN || {};

Viacom.IAN.customItemHtml = function (ctx) {

        var oTitle = ctx.CurrentItem.FileLeafRef.substring(0,ctx.CurrentItem.FileLeafRef.length-(ctx.CurrentItem.File_x0020_Type.length+1));
		var url = String.format('{0}&ID={1}', ctx.displayFormUrl, ctx.CurrentItem.ID);
		var accordionItemHtml='<ul class="webpart-list"><li style="list-style:none;"><h4><a href="'+url+'">'+oTitle+'</a></h4></li></ul>'
        return accordionItemHtml;
 
};

Viacom.IAN.customHeaderHtml = function (ctx) {
		var accordionHeaderHtml="<div class='webpart-heading'><h3>Documents <a href='"+ctx.listUrlDir+"' class='view-all-link'>View All</a></h3></div>";
        return accordionHeaderHtml;
 
};

(function () {
    var overrideCtx = {};
    overrideCtx.Templates = {};

    overrideCtx.Templates.Header = Viacom.IAN.customHeaderHtml;
    overrideCtx.Templates.Item = Viacom.IAN.customItemHtml;
    overrideCtx.BaseViewID = 1;
    overrideCtx.ListTemplateType = 101;

    SPClientTemplates.TemplateManager.RegisterTemplateOverrides(overrideCtx);
})();
