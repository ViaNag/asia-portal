var Viacom = Viacom || {};
Viacom.IAN = Viacom.IAN || {};

Viacom.IAN.customItemHtml = function (ctx) {

        var oTitle = ctx.CurrentItem.URLwMenu;
		var accordionItemHtml='<ul class="webpart-list"><li style="list-style:none;"><h4><a target="_blank" href='+ctx.CurrentItem["URL"]+'>'+ctx.CurrentItem["URL.desc"]+'</h4></a></li></ul>';
        return accordionItemHtml;
}

Viacom.IAN.customHeaderHtml = function (ctx) {
		var accordionHeaderHtml="<div class='webpart-heading'><h3>Quick Links <a href='"+ctx.listUrlDir+"' class='view-all-link'>View All</a></h3></div>";
        return accordionHeaderHtml;
 
};

(function () {
    var overrideCtx = {};
    overrideCtx.Templates = {};

    overrideCtx.Templates.Header = Viacom.IAN.customHeaderHtml;
    overrideCtx.Templates.Item = Viacom.IAN.customItemHtml;
    overrideCtx.BaseViewID = 1;
    overrideCtx.ListTemplateType = 103;

    SPClientTemplates.TemplateManager.RegisterTemplateOverrides(overrideCtx);
})();
