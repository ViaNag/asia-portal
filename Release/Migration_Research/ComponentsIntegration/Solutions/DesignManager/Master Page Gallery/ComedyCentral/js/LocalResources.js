var Viacom = Viacom || {};
Viacom.IAN = Viacom.IAN || {};

Viacom.IAN.customItemHtml = function (ctx) {
		if(ctx.ListTitle=='Local Resources'){
				var oTitle = ctx.CurrentItem.Title;
				var url = String.format('{0}&ID={1}', ctx.displayFormUrl, ctx.CurrentItem.ID);
				var accordionItemHtml='<ul class="webpart-list"><li style="list-style:none;"><h4><a href="'+url+'">'+oTitle+'</a></h4></li></ul>'
				return accordionItemHtml;
		}
		else{
			return RenderItemTemplate(ctx);
		}
		
};


Viacom.IAN.customHeaderHtml = function (ctx) {
		if(ctx.ListTitle=='Local Resources'){
				var accordionItemHtml="<div class='webpart-heading'><h3>Local Resources <a href='"+ctx.listUrlDir+"' class='view-all-link'>View All</a></h3></div>";
				return accordionItemHtml;
		}
		else if(ctx.ListTitle=='Contacts'){
			var accordionItemHtml="<div class='webpart-heading'><h3>Contacts <a href='"+ctx.listUrlDir+"' class='view-all-link'>View All</a></h3></div>";
			return accordionItemHtml;
		}
		else{
			return '';
		}
  
};


(function () {
    var overrideCtx = {};
    overrideCtx.Templates = {};

    overrideCtx.Templates.Header = Viacom.IAN.customHeaderHtml;
    overrideCtx.Templates.Item = Viacom.IAN.customItemHtml;
    overrideCtx.BaseViewID = 1;
    overrideCtx.ListTemplateType = 100;
	

    SPClientTemplates.TemplateManager.RegisterTemplateOverrides(overrideCtx);
})();


