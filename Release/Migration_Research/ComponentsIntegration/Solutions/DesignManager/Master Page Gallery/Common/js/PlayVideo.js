// Create a namespace for our functions so we don't collide with anything else
var Viacom = Viacom || {};



function mediaSiloModal(assetID, vTitle) {
    $('#mediasilo-overlay,.mediasilo-modal').toggle();
    $('.ms-dlgTitleBtns').click(function () {
        $('#mediasilo-overlay,.mediasilo-modal').hide();
        jwplayer("mediasilo-video").remove();
        $('#mediasilo-video').text('Loading the Player...');
    });
    $('#videoTitle').text(vTitle);
    Viacom.GetVideoProperties(assetID);

}

Viacom.GetVideoProperties = function (astID) {
    try {
        $.ajax({
            type: "POST",
            url: _spPageContextInfo.siteAbsoluteUrl + "/_catalogs/masterpage/MediaSiloUploadVideo.aspx/" + "AssetDetails",
            data: JSON.stringify({ assetID: '"' + astID + '"', siteCollectionURL: '"' + _spPageContextInfo.siteAbsoluteUrl + '"' }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d != null) {
                    var obj = jQuery.parseJSON(data.d);
                    var assetDetails = obj[0];
                    var derivative = assetDetails.derivatives[1];

                    vURL = derivative.url;
                    iURL = derivative.thumbnail;
                    jwplayer("mediasilo-video").setup({
                        autostart: true,
                        controlbar: "bottom",
                        file: vURL,
                        image: iURL,
                        duration: 26,
                        title: 'Nickelodeon\'s Kids\' Choice Sports 2014',
                        flashplayer: "/_catalogs/masterpage/JwPlayer6/jwplayer.flash.swf",
                        volume: 80,
                        width: 640,
                        height: 480
                    });
                    jwplayer().onReady(function (event) {
                        $('#mediasilo-video button').on('click', function (event) { return false; });
                    });
                    // Save video details
                    var IsDownload = "false";
                    // Viacom.SaveVideoData(intVideoUrl, IsDownload);
                }
            }
            ,
            error: function (err) {
                console.log('Asset Details Process failed');
            }
        });
    }
    catch (err) {
        console.log(err.message + 'Something went wrong during creation of detailsAsset');
    }
}