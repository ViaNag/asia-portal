// JavaScript Document
(function ($, sr) {
    // debouncing function from John Hann
    // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
    var debounce = function (func, threshold, execAsap) {
        var timeout;

        return function debounced() {
            var obj = this, args = arguments;
            function delayed() {
                if (!execAsap)
                    func.apply(obj, args);
                timeout = null;
            };

            if (timeout)
                clearTimeout(timeout);
            else if (execAsap)
                func.apply(obj, args);

            timeout = setTimeout(delayed, threshold || 100);
        };
    }
    // smartresize 
    jQuery.fn[sr] = function (fn) {
        return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr);
    };

})(jQuery, 'smartresize');

// JavaScript Document
$(document).ready(function (e) {
    if ($(window).width() > 767) {
        $('.carousel-indicators li').hover(
                function () {
                    var getWidth = $(this).width();
                    $(this).children('figure').fadeIn('slow').css('width', getWidth);
                    $(this).children('figure').children('span').css('left', (getWidth / 2) - 5);
                },
                function () {
                    $(this).children('figure').fadeOut(1).css('width', '');
                    $(this).children('figure').children('span').css('left', '');
                }
        );
    }

    $('.quick-links-nav > li').click(function (e) {
        var getIndex = $(this).index();
        //alert(getIndex);
        $(this).addClass('selected').siblings().removeClass('selected');
        $('.quick-links-result > li').eq(getIndex).show().siblings().hide();
		if($(this).text() == 'My Links')
		{
			$('#divMangeMyLinks').show();
		}
		else
			$('#divMangeMyLinks').hide();
    });

    $('#deviceNav').appendTo('#headerContainer');
    $('#dvCarousel div.item span img').attr('width', '100%');
    $('#ms-microbloginputbox').addClass('form-control');
    $('#dispImgDiv > img').attr('width', '100%').addClass('img-responsive');
    $('#sideNavBox').prependTo('.container.vl-container').wrap('<div class="row"><div class="col-md-2 sideNav"></div></div>');

    
    $('textarea.ms-comm-postReplyTextBox.ms-fullWidth').css({'width': '100%'});
    $('#DeltaPlaceHolderMain').appendTo('.container.vl-container > .row').wrap('<div class="col-md-10 blogContainer"></div>');
    $('#imageForBlog').children('img').attr('width', '100%');
	
	/*$('.item-search-result li h3').removeClass('ms-srch-ellipsis');*/
	
	/*$('.peopleSearchBox').find('#SearchBox').children('div.ms-srch-sbLarge').addClass('input-group');*/
	/*$('.peopleSearchBox #SearchBox > div').addClass('input-group').children('input').addClass('form-control').next('a').addClas('input-group-btn');*/
	/*setTimeout( function(){
       $('.peopleSearchBox').find('#SearchBox').children('div.ms-srch-sbLarge').removeClass('ms-srch-sb-border').addClass('input-group').next('a').addClas('input-group-btn');
	   $('.peopleSearchBox').find('#SearchBox').children('div.ms-srch-sbLarge').children('input').addClass('form-control');
	   
    }, 1500);*/
	
    $('.groupDocument .ms-webpart-titleText').children('a').attr('href', 'javascript:void(0);');
	

    var syncHeight = function(winWidth) {
        var threshold = 1023,
            $container = $('.main-container');

        if( winWidth <= threshold ) {
            $container.children('div').removeAttr('style');
        } else {
            var minHeight = parseInt($('.main-container').outerHeight());
            $container.children('div').css('min-height', minHeight);
            $container.attr('data-min-height', minHeight);
            
            var timeInterval = setInterval(function(){ 
                var minHeight = parseInt($container.attr('data-min-height')),
                    containerHeight = parseInt($('.main-container').outerHeight());

                if( minHeight === containerHeight ) {
                    clearInterval(timeInterval);
                } else {
                    $container.children('div').css('min-height', containerHeight);
                    $container.attr('data-min-height', containerHeight);
                }
            }, 3000);
        }
    };
	
	var v1_height2 = $('.container.vl-container').height() - 1200;
	
	$('.peopleleftNav').css('min-height',v1_height2);
	$('.main-container > .col-md-2').css('height', v1_height2);
	$('.main-container > .col-md-4').css('height', v1_height2 );
    
    var syncLeftSidebarHeight = function(winWidth) {
        var threshold = 990,
            getBannerHeight = $('#dispImgDiv.group-img').height(),
            $container = $('#sideNavBox .carousel-nav');
        
        if( getBannerHeight > 0 ) {
            if( winWidth > threshold ) {
                $container.css({'height': getBannerHeight, 'background': '#1a1a1a'});
            }
        } else {
            setTimeout( function(){
                $(document).trigger('domupdate');
            }, 1000);
        }
    };
    
    $(window).on('smartresize', function () {
        var windowWidth = $(window).width();
        syncHeight(windowWidth);
        syncLeftSidebarHeight(windowWidth);
    });
    
    setTimeout( function(){
        $(window).trigger('smartresize');
    }, 100);
    
    $(document).on('domupdate orientationchange', function(){
        var windowWidth = $(window).width();
        syncLeftSidebarHeight(windowWidth);
    });
    
    $(document).on('syncSidebarHeight', function(){
        var windowWidth = $(window).width();
        syncHeight(windowWidth);
    });

   
   
});
var QuickLinksArray = [];
//var QuickLinks = [];
function GetLinks(QuickLinkUrl){

	var clientContext = new SP.ClientContext(QuickLinkUrl);
    var oList = clientContext.get_web().get_lists().getByTitle('QuickLinks');
	var camlQueryforQuickLinks = new SP.CamlQuery();
    camlQueryforQuickLinks.set_viewXml("<View><Query><Where><Eq><FieldRef Name='IsActive' /><Value Type='Boolean'>1</Value></Eq></Where><OrderBy><FieldRef Name='LinkOrder' Ascending='True' /></OrderBy></Query><RowLimit>10</RowLimit></View>");
   	var collListItem = oList.getItems(camlQueryforQuickLinks);  
    clientContext.load(collListItem, 'Include(Title, QuickLinkURL)');
    clientContext.executeQueryAsync(
		function(sender, args)
		{
			
			 var listItemEnumerator = collListItem.getEnumerator();
			 var counter =0;
			 while (listItemEnumerator.moveNext()) {
                var oListItem = listItemEnumerator.get_current();
				QuickLinksArray[counter] = {quickLinkIitle: oListItem.get_item('Title'), quickLinkURL: oListItem.get_item('QuickLinkURL').get_url() };
				counter ++;			
			}			
			if(QuickLinksArray.length > 0)
			{
				$("#QuickLinks").tmpl(QuickLinksArray).appendTo("#ulQuickLinks");
				$("#noQuickLinks").css("display","none");
			}
			else{
				$("#noQuickLinks").css("display","block");
			}
		},
		function(sender, args)
		{
			//console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
		}
	); 
};

function retTileColor(colorSelected) {
    var color = '';
    switch (colorSelected)
    {
       
        case 'Green':
            color = '#a6d72d';
            break;
        case 'Red': color = '#f82828';
            break;
        case 'Blue': color = '#2daed8';
            break;
        case 'Orange': color = '#fea734';
            break;
        case 'Purple': color = '#c32cd9';
            break;
        case 'Sky Blue': color = '#1ec8bb';
            break;
        case 'Bittersweet': color = '#fe6f5e';
            break;
        case 'Bottle Green': color = '#006a4e';
            break;
        case 'Brilliant Rose': color = '#ff55a3';
            break;
        case 'Byzantium': color = '#702963';
            break;
        case 'Meat Brown': color = '#e5b73b';
            break;
        case 'Mint': color = '#3eb489';
            break;
        case 'Pansy Purple': color = '#78184a';
            break;
        case 'Dark Midnight Blue': color = '#003366';
            break;
        case 'Tangerine Yellow': color = '#ffcc00';
            break;
        case 'Voilet': color = '#ee82ee';
            break;
        case 'Cadet Blue': color = '#5F9EA0';
            break;
        case 'Light Sea Green': color = '#20B2AA';
            break;
        case 'Dark Cyan': color = '#008B8B';
            break;
		case 'Pink': color = '#FF69B4';		
        break;
    }
    return color;
}
