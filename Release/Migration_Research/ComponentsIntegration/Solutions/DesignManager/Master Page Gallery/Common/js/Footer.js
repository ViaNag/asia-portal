var Viacom = Viacom || {};
Viacom.LegalPortal = Viacom.LegalPortal || {};

Viacom.LegalPortal.Constants = function () {
    return {
        FooterLinks: null,
        OurBrands: null,
        requestUri: null,
        tdWidth: null,
        brandImages: null,
        AdministrationSite: null,
        RootSite: null
    }
};

Viacom.LegalPortal.Initialize = function () {
    Viacom.LegalPortal.Constants.requestUri = _spPageContextInfo.webAbsoluteUrl;
    Viacom.LegalPortal.Constants.AdministrationSite = _spPageContextInfo.siteServerRelativeUrl + "/Administration";
    Viacom.LegalPortal.Constants.RootSite = _spPageContextInfo.siteServerRelativeUrl;
};

$(document).ready(
	function () {
	    Viacom.LegalPortal.Initialize();
	   // SP.SOD.executeFunc('SP.js', 'SP.ClientContext', Viacom.LegalPortal.GetData);
	   
	   //Commented the below call as this is not required in Research and this is causing errors in console
	   //SP.SOD.executeFunc('SP.js', 'SP.ClientContext', Viacom.LegalPortal.GetBrandImages);
	}
);

Viacom.LegalPortal.GetData = function () {
    var clientContext = new SP.ClientContext(Viacom.LegalPortal.Constants.AdministrationSite);
    var oList = clientContext.get_web().get_lists().getByTitle('Footer Links');
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml("<View><Query><OrderBy><FieldRef Name='Order1' Ascending='True'/></OrderBy><GroupBy><FieldRef Name='Category1' /></GroupBy></Query></View>");
    this.collListItemFooterLinks = oList.getItems(camlQuery);

    //Include isActive & State column from OOF list in case of edit
    clientContext.load(collListItemFooterLinks, 'Include(Title, Url, Category1 , Order1, CategoryOrder)');

    clientContext.executeQueryAsync(
		function (sender, args) {
		    Viacom.LegalPortal.Constants.FooterLinks = [];
		    var listItemEnumerator = collListItemFooterLinks.getEnumerator();
		    try {
		        while (listItemEnumerator.moveNext()) {
		            var oListItem = listItemEnumerator.get_current();
		            var linkUrl = '#';
		            if (oListItem.get_item('Url') != undefined) {
		                linkUrl = oListItem.get_item('Url').get_url();
		            }
		            Viacom.LegalPortal.Constants.FooterLinks[Viacom.LegalPortal.Constants.FooterLinks.length] = {
		                Title: oListItem.get_item('Title'),
		                Url: linkUrl,
		                Category: oListItem.get_item('Category1'),
		                CategoryOrder: oListItem.get_item('CategoryOrder'),
		                Order: oListItem.get_item('Order1')
		            };
		        }
		        Viacom.LegalPortal.BindData();
		    }
		    catch (err) {
		       /* if (console != undefined && console != null) {
		            console.log("There was some error in processing..");
		        } */
		        return false;
		    }
		},

		function (sender, args) {
		  /*  if (console != undefined && console != null) {
		        console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
		    } */
		}
	);
};

Viacom.LegalPortal.sortFunction = function (a, b) {
    return a.CategoryOrder > b.CategoryOrder ? 1 : -1;
};
Viacom.LegalPortal.sortOrderFunction = function (a, b) {
    return a.Order1 < b.Order1 ? 1 : -1;
};

Viacom.LegalPortal.BindData = function () {
    if (Viacom.LegalPortal.Constants.FooterLinks != null) {
        var categorised = [];
        var noOfCategories = 0;
        //Sort the footer links array by category order
        Viacom.LegalPortal.Constants.FooterLinks.sort(Viacom.LegalPortal.sortFunction);
        for (var i = 0; i < Viacom.LegalPortal.Constants.FooterLinks.length; i++) {
            if (i == 0) {

                categorised[noOfCategories] = {
                    Category: Viacom.LegalPortal.Constants.FooterLinks[i].Category
                };
                categorised[noOfCategories]["Links"] = [];
                categorised[noOfCategories]["Links"][categorised[noOfCategories]["Links"].length] = {
                    Title: Viacom.LegalPortal.Constants.FooterLinks[i].Title,
                    Url: Viacom.LegalPortal.Constants.FooterLinks[i].Url,
                    Order: Viacom.LegalPortal.Constants.FooterLinks[i].Order
                };
            }
            else {
                if (Viacom.LegalPortal.Constants.FooterLinks[i].Category != Viacom.LegalPortal.Constants.FooterLinks[i - 1].Category) {
                    noOfCategories++;

                    categorised[noOfCategories] = {
                        Category: Viacom.LegalPortal.Constants.FooterLinks[i].Category
                    };
                    categorised[noOfCategories]["Links"] = [];
                    categorised[noOfCategories]["Links"][categorised[noOfCategories]["Links"].length] = {
                        Title: Viacom.LegalPortal.Constants.FooterLinks[i].Title,
                        Url: Viacom.LegalPortal.Constants.FooterLinks[i].Url,
                        Order: Viacom.LegalPortal.Constants.FooterLinks[i].Order
                    };
                }
                else {

                    categorised[noOfCategories]["Links"][categorised[noOfCategories]["Links"].length] = {
                        Title: Viacom.LegalPortal.Constants.FooterLinks[i].Title,
                        Url: Viacom.LegalPortal.Constants.FooterLinks[i].Url,
                        Order: Viacom.LegalPortal.Constants.FooterLinks[i].Order
                    };
                }
            }

        }
        //categorised.sort(Viacom.LegalPortal.sortOrderFunction);
        Viacom.LegalPortal.Constants.tdWidth = (100 / (noOfCategories + 1));
        $.tmpl('<div class="col-md-2"><h4>${Category}</h4><ul>{{each Links}}<li><a href="${Url}">${Title}</a></li>{{/each}}</ul></div>', categorised).appendTo("#divFooterLinks");

        SP.SOD.executeFunc('SP.js', 'SP.ClientContext', Viacom.LegalPortal.GetBrandImages);
    }
};

Viacom.LegalPortal.GetBrandImages = function () {
    var clientContext = new SP.ClientContext(Viacom.LegalPortal.Constants.RootSite);
    var oList = clientContext.get_web().get_lists().getByTitle('Our Brands');

    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml("<View><Query><OrderBy><FieldRef Name='Order0' Ascending='True' /></OrderBy></Query></View>");
    this.collListItemBrandImages = oList.getItems(camlQuery);

    //Include isActive & State column from OOF list in case of edit
    clientContext.load(collListItemBrandImages, 'Include(IconURL, Order0, BrandLink)');

    clientContext.executeQueryAsync(
		function (sender, args) {
		    Viacom.LegalPortal.Constants.brandImages = [];
		    var listItemEnumerator = collListItemBrandImages.getEnumerator();
		    try {
		        while (listItemEnumerator.moveNext()) {
		            var oListItem = listItemEnumerator.get_current();
		            Viacom.LegalPortal.Constants.brandImages[Viacom.LegalPortal.Constants.brandImages.length] = {
		                IconURL: oListItem.get_item('IconURL') + "</img>",
		                Order: oListItem.get_item('Order0'),
		                BrandLink: oListItem.get_item('BrandLink').get_url()
		            };
		        }
		        Viacom.LegalPortal.BindImages();
		    }
		    catch (err) {
		      if (console != undefined && console != null) {
		            console.log("There was some error in processing..");
		        } 
		        return false;
		    }
		},

		function (sender, args) {
		    if (console != undefined && console != null) {
		        console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
		    } 
		}
	);
};

Viacom.LegalPortal.BindImages = function () {
    if (Viacom.LegalPortal.Constants.brandImages != null) {
        $.tmpl('<li><a href="${BrandLink}" target="_blank"><figure>{{html IconURL}}</figure></a></li>', Viacom.LegalPortal.Constants.brandImages).appendTo("#listIcons");

    }
    Viacom.LegalPortal.GetDisclaimerText();
};

Viacom.LegalPortal.GetDisclaimerText = function () {
    var clientContext = new SP.ClientContext(Viacom.LegalPortal.Constants.RootSite);
    var oList = clientContext.get_web().get_lists().getByTitle('Disclaimer');
    var disclaimerText = '';
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml("<View><Query><OrderBy><FieldRef Name='Created' Ascending='True' /></OrderBy></Query><RowLimit>1</RowLimit></View>");
    var collListItemDisclaimer = oList.getItems(camlQuery);

    //Include isActive & State column from OOF list in case of edit
    clientContext.load(collListItemDisclaimer, 'Include(Title, DisclaimerText)');

    clientContext.executeQueryAsync(
		function (sender, args) {
		    var listItemEnumerator = collListItemDisclaimer.getEnumerator();
		    try {
		        while (listItemEnumerator.moveNext()) {
		            var oListItem = listItemEnumerator.get_current();
		            disclaimerText = oListItem.get_item('DisclaimerText');
		            $("#disclaimerText").html(disclaimerText);
		        }

		    }
		    catch (err) {
		     /*   if (console != undefined && console != null) {
		            console.log("There was some error in processing  in fetching disclaimer text..");
		        } */
		        return false;
		    }
		},

		function (sender, args) {
		  /*  if (console != undefined && console != null) {
		        console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
		    } */
		}
	);
};

