(function(){
var $;
window.Kaleo||(window.Kaleo={}
);
$=jQuery;
Kaleo.API=(function(){
function API(opts){
this.host=opts.host;
this.widgetToken=opts.widgetToken;
this.sitemap_token=opts.sitemap_token;
//this.board_ids = opts.board_ids;
this.sitemap = opts.sitemap;
this.baseUrlParams={sitemap:this.sitemap};
this.useEasyXDM=opts.useEasyXDM!=null?opts.useEasyXDM:true;
this.debug=opts.debug||false;
this._log("Creating Kaleo API Object")
}
API.prototype.search=function(data,success_cb,error_cb){
var data_ext,url;

url = "/v4/search.json?sitemap="+this.sitemap +"&sitemap_token="+this.sitemap_token+"&restrict_to_sitemap=true";
data_ext=$.extend({},data,{_:jQuery.now()},this.baseUrlParams);
success_cb=success_cb||function(){};
error_cb=error_cb||function(){};
if(this.useEasyXDM){
return this._xhrRequestEasyXDM(url,data_ext,success_cb,error_cb)
}
else{
return this._xhrRequestJQuery(url,data_ext,success_cb,error_cb)}
};
API.prototype._log=function(msg){if((typeof console!=="undefined"&&console!==null)&&this.debug){msg=typeof msg==="string"?msg:JSON.stringify(msg);return console.log(msg)}};
API.prototype._xhrRequestJQuery=function(url,data,success_cb,error_cb){return $.ajax({url:url,data:data,dataType:"json",method:"GET",success:success_cb,error:error_cb})};
API.prototype._xhrRequestEasyXDM=function(url,data,success_cb,error_cb){
var endpoint;
endpoint=this.debug?""+this.host+"/easyxdm/cors/index_debug.html":""+this.host+"/easyxdm/cors/index.html";
this.xhr||(this.xhr=new easyXDM.Rpc({local:"/easyxdm/name.html",remote:endpoint,remoteHelper:""+this.host+"/easyxdm/name.html",swf:""+this.host+"/easyxdm/easyxdm.swf"},{remote:{request:{}}}));
return this.xhr.request({url:url,method:"GET",data:data},(function(_this){return function(response){data=response.data||response;if(typeof data==="string"){data=JSON.parse(data)}return success_cb(data)}})(this),error_cb)};return API})()}).call(this);