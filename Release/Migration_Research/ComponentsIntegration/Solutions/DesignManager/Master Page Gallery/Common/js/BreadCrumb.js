//// function to process breadcrumbs on pages
var Viacom = Viacom || {};
Viacom.LegalPortal = Viacom.LegalPortal || {};
Viacom.LegalPortal.BreadCrumb = {};
Viacom.LegalPortal.BreadCrumb.Constants = function(){
	return{
		requestUri : null
	}
};


$(document).ready(function(){
    SP.SOD.executeFunc('SP.js', 'SP.ClientContext', Viacom.LegalPortal.BreadCrumb.SetUpData);
});


Viacom.LegalPortal.BreadCrumb.SetUpData = function(){
    var context = SP.ClientContext.get_current();
    var web = context.get_web(); 
   
    context.load(web);
    context.executeQueryAsync(
     function () {  
       
         var url =  document.URL;
		 var i=0;
		 var currentPage = "";
		 var breadCrumbStructure = [];
         //Viacom.LegalPortal.BreadCrumb.Constants.requestUri = web.get_url();
         if(url.indexOf("Groups") > -1)
         {
			if(url.indexOf("GroupPage") > -1 || url.indexOf(".") <= -1)
			{
				currentPage = web.get_title();
			}
			else
			{
				breadCrumbStructure[i] = {
				    breadCrumbUrl: "/sites/Legal/Groups/Pages/GroupPage.aspx",
						BreadCrumbTitle: "Groups"
					};
				i++;
				
				if(url.indexOf("VMNServicesTiles") > -1) //VMN Services
				{
					currentPage = "VMN Services";
				}
				//currentPage = web.get_title();
				if(url.indexOf("GroupsHomePage") > -1) //Group sub sites A-G
				{
					currentPage = web.get_title();
				}
				if(url.indexOf("GroupsTeamMembers") > -1) // groups sub site all team members page
				{
					currentPage = web.get_title();
				}
				if (url.indexOf("GroupCalendar") > -1) //Group sub sites A-G
				{
				    currentPage = web.get_title();
				}
				if (url.indexOf("GroupTasks") > -1) // groups sub site all team members page
				{
				    currentPage = web.get_title();
				}
				if (url.indexOf("GroupDiscussions") > -1) //Group sub sites A-G
				{
				    currentPage = web.get_title();
				}
				if (url.indexOf("GroupDocuments") > -1) // groups sub site all team members page
				{
				    currentPage = web.get_title();
				}
				if(url.indexOf("PeerLeadership") > -1)  // peer leadership page
				{
					currentPage = web.get_title();
				}
				if(url.indexOf("TaskForceHomePage") > -1)   // Task forces site
				{
				   currentPage = web.get_title();
				}
				if (url.indexOf("TaskForcesSubSiteHomePage") > -1) // groups sub site all team members page
				{
				    breadCrumbStructure[i] = {
				        breadCrumbUrl: "/sites/Legal/Groups/TaskForces/Pages/TaskForceHomePage.aspx",
				        BreadCrumbTitle: "Task Forces"
				    };
				    i++;
				    currentPage = web.get_title();
				}
				if (url.indexOf("TaskForcesTeamMembers") > -1)   // Task forces site
				{
				    breadCrumbStructure[i] = {
				        breadCrumbUrl: "/sites/Legal/Groups/TaskForces/Pages/TaskForceHomePage.aspx",
				        BreadCrumbTitle: "Task Forces"
				    };
				    i++;
				    currentPage = web.get_title();
				}
				if (url.indexOf("TaskForcesTasks") > -1) // groups sub site all team members page
				{
				    breadCrumbStructure[i] = {
				        breadCrumbUrl: "/sites/Legal/Groups/TaskForces/Pages/TaskForceHomePage.aspx",
				        BreadCrumbTitle: "Task Forces"
				    };
				    i++;
				    currentPage = web.get_title();
				}
				if (url.indexOf("TaskForcesDocuments") > -1)   // Task forces site
				{
				    breadCrumbStructure[i] = {
				        breadCrumbUrl: "/sites/Legal/Groups/TaskForces/Pages/TaskForceHomePage.aspx",
				        BreadCrumbTitle: "Task Forces"
				    };
				    i++;
				    currentPage = web.get_title();
				}

				if (url.indexOf("TaskForcesCalendar") > -1) // groups sub site all team members page
				{
				    breadCrumbStructure[i] = {
				        breadCrumbUrl: "/sites/Legal/Groups/TaskForces/Pages/TaskForceHomePage.aspx",
				        BreadCrumbTitle: "Task Forces"
				    };
				    i++;
				    currentPage = web.get_title();
				}
				if (url.indexOf("TaskForcesDiscussions") > -1)   // Task forces site
				{
				    breadCrumbStructure[i] = {
				        breadCrumbUrl: "/sites/Legal/Groups/TaskForces/Pages/TaskForceHomePage.aspx",
				        BreadCrumbTitle: "Task Forces"
				    };
				    i++;
				    currentPage = web.get_title();
				}

				if (url.indexOf("ProfessionalDevelopment") > -1) // PD site
				{
					if(url.indexOf("PDHomePage") > -1)
					{
						currentPage = web.get_title();
						
					}
					else 
					{
						if(url.indexOf("PDCoursesHome") > -1)// BALAU etc. sites
						{
							breadCrumbStructure[i] = {
							    breadCrumbUrl: "/sites/Legal/Groups/ProfessionalDevelopment/Pages/PDHomePage.aspx",
								BreadCrumbTitle: "Professional Development"
							};
							i++;
							//currentPage = web.get_title();

							if (web.get_title() == "BALAU") {

							    currentPage = 'BALA U!';
							}
							else {
							    currentPage = web.get_title();
							}



						}
						if(url.indexOf("ACPHomePage") > -1)  // ACP site
						{
							breadCrumbStructure[i] = {
							    breadCrumbUrl: "/sites/Legal/Groups/ProfessionalDevelopment/Pages/PDHomePage.aspx",
								BreadCrumbTitle: "Professional Development"
							};
							i++;
							currentPage = web.get_title();
						}
						if(url.indexOf("LegalJobPostings") > -1)  // Legal Job Postings Library
						{
							breadCrumbStructure[i] = {
							    breadCrumbUrl: "/sites/Legal/Groups/ProfessionalDevelopment/Pages/PDHomePage.aspx",
								BreadCrumbTitle: "Professional Development"
							};
							i++;
							currentPage = "Legal Job Postings";
						}
						
					}
				
				}
			}
				
         }
		 else if (url.indexOf("Resources") > -1) // PD site
         {
            
                 currentPage = web.get_title();

             
             
                 if (url.indexOf("BusinessAndAdministrativeDocuments") > -1)
                 {
                     breadCrumbStructure[i] = {
                         breadCrumbUrl: "/sites/Legal/Resources/",
                         BreadCrumbTitle: "Resources"
                     };
                     i++;
                     currentPage = "Business & Administrative Documents";

                 }
				 else if (url.indexOf("FinancialReferences") > -1)
                 {
                     breadCrumbStructure[i] = {
                         breadCrumbUrl: "/sites/Legal/Resources/",
                         BreadCrumbTitle: "Resources"
                     };
                     i++;
                     currentPage = "Financial References";

                 }
				 else if (url.indexOf("ClientApplications") > -1)
                 {
                     breadCrumbStructure[i] = {
                         breadCrumbUrl: "/sites/Legal/Resources/",
                         BreadCrumbTitle: "Resources"
                     };
                     i++;
                     currentPage = "Client Applications";

                 }
                 else if (url.indexOf("BusinessAndAdministrative") > -1)
                 {
                     breadCrumbStructure[i] = {
                         breadCrumbUrl: "/sites/Legal/Resources/",
                         BreadCrumbTitle: "Resources"
                     };
                     i++;
                     currentPage = "Business and Administrative";

                 }
                 else if (url.indexOf("FormsLibrary") > -1)
                 {
                     breadCrumbStructure[i] = {
                         breadCrumbUrl: "/sites/Legal/Resources/",
                         BreadCrumbTitle: "Resources"
                     };
                     i++;
                     currentPage = "Forms Library";

                 }
                 else if (url.indexOf("LegalApplications") > -1)
                 {
                     breadCrumbStructure[i] = {
                         breadCrumbUrl: "/sites/Legal/Resources/",
                         BreadCrumbTitle: "Resources"
                     };
                     i++;
                     currentPage = "Legal Applications";

                 }
                 else if (url.indexOf("ViacomResources") > -1)
                 {
                     breadCrumbStructure[i] = {
                         breadCrumbUrl: "/sites/Legal/Resources/",
                         BreadCrumbTitle: "Resources"
                     };
                     i++;
                     currentPage = "Viacom Resources";

                 }
                 else if (url.indexOf("VMNViacomAPForms") > -1)
                 {
                     breadCrumbStructure[i] = {
                         breadCrumbUrl: "/sites/Legal/Resources/",
                         BreadCrumbTitle: "Resources"
                     };
                     i++;
                     currentPage = "VMN/Viacom AP Forms";

                 }
				 else if (url.indexOf("BALAInternationalResourcesTiles") > -1)
                 {
                     breadCrumbStructure[i] = {
                         breadCrumbUrl: "/sites/Legal/Resources/",
                         BreadCrumbTitle: "Resources"
                     };
                     i++;
                     currentPage = "BALA International Resources";

                 }
				 else if (url.indexOf("Americas") > -1)
                 {
                     breadCrumbStructure[i] = {
                         breadCrumbUrl: "/sites/Legal/Resources/",
                         BreadCrumbTitle: "Resources"
                     };
                     i++;
                     currentPage = "Americas";

                 }
				 else if (url.indexOf("Asia") > -1)
                 {
                     breadCrumbStructure[i] = {
                         breadCrumbUrl: "/sites/Legal/Resources/",
                         BreadCrumbTitle: "Resources"
                     };
                     i++;
                     currentPage = "Asia";

                 }
				  else if (url.indexOf("North") > -1)
                 {
                     breadCrumbStructure[i] = {
                         breadCrumbUrl: "/sites/Legal/Resources/",
                         BreadCrumbTitle: "Resources"
                     };
                     i++;
                     currentPage = "North";

                 }
				  else if (url.indexOf("South") > -1)
                 {
                     breadCrumbStructure[i] = {
                         breadCrumbUrl: "/sites/Legal/Resources/",
                         BreadCrumbTitle: "Resources"
                     };
                     i++;
                     currentPage = "South";

                 }
				  else if (url.indexOf("UK") > -1)
                 {
                     breadCrumbStructure[i] = {
                         breadCrumbUrl: "/sites/Legal/Resources/",
                         BreadCrumbTitle: "Resources"
                     };
                     i++;
                     currentPage = "UK";

                 }
				  else if (url.indexOf("VIMNHQ") > -1)
                 {
                     breadCrumbStructure[i] = {
                         breadCrumbUrl: "/sites/Legal/Resources/",
                         BreadCrumbTitle: "Resources"
                     };
                     i++;
                     currentPage = "VIMN HQ";

                 }
                 else if (url.indexOf("BALAInternationalResources") > -1)
                 {
                     breadCrumbStructure[i] = {
                         breadCrumbUrl: "/sites/Legal/Resources/",
                         BreadCrumbTitle: "Resources"
                     };
                     i++;
                     currentPage = "VIMN International Resources";

                 }
				  else if (url.indexOf("Notaries") > -1)
                 {
                     breadCrumbStructure[i] = {
                         breadCrumbUrl: "/sites/Legal/Resources/",
                         BreadCrumbTitle: "Resources"
                     };
                     i++;
                     currentPage = "Notaries";

                 }
				else if (url.indexOf("AuthSignatories") > -1)
                 {
                     breadCrumbStructure[i] = {
                         breadCrumbUrl: "/sites/Legal/Resources/",
                         BreadCrumbTitle: "Resources"
                     };
                     i++;
                     currentPage = "Authorized Signatories";

                 }
                 else if (url.indexOf("NotariesAuthSignatories") > -1)
                 {
                     breadCrumbStructure[i] = {
                         breadCrumbUrl: "/sites/Legal/Resources/",
                         BreadCrumbTitle: "Resources"
                     };
                     i++;
                     currentPage = "Notaries and Authorized Signatories";

                 }
				 else if (url.indexOf("OutsideCounsel") > -1)
                 {
                     breadCrumbStructure[i] = {
                         breadCrumbUrl: "/sites/Legal/Resources/",
                         BreadCrumbTitle: "Resources"
                     };
                     i++;
                     currentPage = "Outside Counsel";

                 }
            
             

         }
		 else if(url.indexOf("PeopleDirectory")  > -1)
		 {
			breadCrumbStructure[i] = {
                         breadCrumbUrl: "/sites/Legal/Pages/PeopleDirectory.aspx",
                         BreadCrumbTitle: "People Directory"
                     };
                     i++;
			currentPage = "Home";
		 }
		 else if(url.indexOf("LegalSearchResults")  > -1)
		 {
			currentPage = "Search Results";
		 }
		 else if(url.indexOf("ViewAllAnnouncements")  > -1)
		 {
			currentPage = "All Announcements";
		 }
		 else if (url.indexOf("ViewAnnouncementDetails") > -1) {
		     currentPage = "Announcement Details";
		 }
         if($.tmpl)
			{ 
			$.tmpl('<li> <a href="${breadCrumbUrl}">${BreadCrumbTitle}</a></li>', breadCrumbStructure).appendTo("#ulBreadCrumb");
			$.tmpl('<li> <a href="${breadCrumbUrl}">${BreadCrumbTitle}</a></li>', breadCrumbStructure).appendTo("#ulBreadCrumbPL");
			}
         $("#dvBreadCrumb ul").append("<li>" + currentPage + "</li>");
         $("#dvBreadCrumbPL ul").append("<li>" + currentPage + "</li>");
	 },
     function (sender, args) {
         //console.log(args.get_message());
     }
    ); 

}

