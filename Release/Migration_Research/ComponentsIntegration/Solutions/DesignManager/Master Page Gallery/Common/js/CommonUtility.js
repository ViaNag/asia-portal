/*
 * This javascript is use to open create show page
 * Page will redirect to new site welcome page once show is created successfully
 */
var Viacom = Viacom || {};
Viacom.ShowModelCheck = Viacom.ShowModelCheck || {};

Viacom.ErrorDialog={
	OpenErrorDialog:function(message){
	var html = document.createElement('div');
	html.innerHTML = '<h1>Hi! Welcome to SharePoint</h1>\
	<a href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Close Dialog</a>\
	<a href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">OK</a>';
	OpenPopUpPageWithDialogOptions({
	 title: "HTML Content",
	 html:html,
	 dialogReturnValueCallback: function(dialogResult){
	  alert(dialogResult); //Add your custom code here.
	 }
	});
}