﻿/*
 * This javascript is use to open create show page
 * Page will redirect to new site welcome page once show is created successfully
 */
var Viacom = Viacom || {};
Viacom.ShowModelCheck = Viacom.ShowModelCheck || {};

Viacom.ShowModelCheck={
	OpenDialog:function(pageURL) {
		var options = SP.UI.$create_DialogOptions();
		options.url = _spPageContextInfo.siteAbsoluteUrl + pageURL;
		options.width = 800;
		options.height = 800;
		options.dialogReturnValueCallback = function (res, retVal) {
			if (res== SP.UI.DialogResult.OK) {
				 if (retVal[0]) {
					var RedirectUrl = window.location.protocol + "//" + window.location.host + retVal[0];
					//Viacom.ShowModelCheck.ShowDialogMessage(RedirectUrl);
					window.location.href = RedirectUrl;
				}
			}
			if (res== SP.UI.DialogResult.cancel) {
					if (retVal[0]) {
				}
			}
		  }  
	SP.UI.ModalDialog.showModalDialog(options);
	},
	ShowDialogMessage:function(popUpDialogHeader,message){
		var html = document.createElement('div');
		html.innerHTML = '<div><h1>'+message+'</h1></div><div>\
		<a href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Close</a>\
		<a href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">OK</a></div>';
		OpenPopUpPageWithDialogOptions({
		 title: popUpDialogHeader,
		 html:html,
			 dialogReturnValueCallback: function(dialogResult){
			 
			 }
		});
	}
}



































