﻿/*
 * This javascript is use to create Comedy central site using web template
 * CreateWebTemplate function will be called at page laod
 */
var Viacom = Viacom || {};
Viacom.CreateComedyCentralShows = Viacom.CreateComedyCentralShows || {};


Viacom.CreateComedyCentralShows={
	CloseDialog:function(showSubsiteUrl){
		var returnValue = [];
		returnValue[0] = showSubsiteUrl;
		SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK, returnValue);
	},
	//Function to call web method to create site using web template
	CreateWebTemplate:function () {
	var webMethodURL="/_layouts/15/WebMethods.aspx/CreateComedyCentralShowSubsite";
	var showSubsiteUrl = '';
	var hiddenValue = $("#hidItemID").val();
	  if (hiddenValue>-1) {
		$('#loading').show();
		$('.lblSaveMessage').hide();
			$.ajax({
			   async: true,
			   type: "POST",
			   url: _spPageContextInfo.siteAbsoluteUrl + webMethodURL,           
			   data: "{'itemId':'" + hiddenValue  + "'}",
			   contentType: "application/json",
			   dataType: "json",
			   success: function (data) {
					   try { 
					 
					   showSubsiteUrl = data.d;              
						}
					   catch (e) {
						   console.log(e);
						}
			   },
			   error: function (e) {
				 console.log(e);
			   },
			   complete: function(){
				$('#loading').hide();
				$('.lblSaveMessage').show();
				Viacom.CreateComedyCentralShows.CloseDialog(showSubsiteUrl);
				}
		   })
		}
	}
}

//function will be called on page load and call create web method for site creation
$(document).ready(function(){
      Viacom.CreateComedyCentralShows.CreateWebTemplate();    
       $('#loading').bind('ajaxStart',
	   function(){
			$(this).show();
		}).bind('ajaxStop',
		function(){
			$(this).hide();
		});
     
});










