var WorldClock = {
    LoginName: null,
	quickLinksItems:null,
	userProfileProperty:"WorldClockKonda",
	uProfileClockValues:'',
    successgetQuickLinks: function() {
		
        var imgUrl = "";
        $.ajax({
			url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('Master Cities')/GetItems(query=@v1)?@v1={'ViewXml':'<View><Query></Query></View>'}",
			type: "POST",
            headers: {
                "Accept": "application/json;odata=verbose",
				"X-RequestDigest": $("#__REQUESTDIGEST").val()
            },
            async: false,
            success: function(data, textStatus, xhr) {				
				WorldClock.quickLinksItems = data.d.results;
					for(var j=0;j<data.d.results.length;j++)
					{
						var isRecommendedItemInQuickLinks = false;
						var LocationClockCode = data.d.results[j].LocationClockCode;
						var CityName = data.d.results[j].CityName.Label;
						var uProperty = CityName.replace(/\,/g, "$");
						uProperty =LocationClockCode+"@#@"+uProperty;
						var clockValues = WorldClock.uProfileClockValues;
						var qucikString='';
						if(clockValues.indexOf(uProperty)>-1)
						{
							qucikString ='<li class="quick-link-pages quick-link-pages-remove" style="padding:0 5px;margin-bottom: 5px; height:20px;width:100%"><span><span>' + CityName + '</span><img src="../SiteCollectionImages/icons/greenroom-panel-icon-circle-check.png" style="height:10px;width:10px;"></span><a href="#" id="btnRem" class="btnRemove"><input type="hidden" name="uProperty" value="'+uProperty+'"/><span style="padding:0 5px;float:right;">Remove</span></a></li>';
						}
						else
						{
							qucikString = '<li class="quick-link-pages" style="padding:0 5px; background-color: white; margin-bottom: 5px; height:20px;width:100%"><span>' + CityName + '</span>' +
								'<a href="#" id="btnAdd" class="btnAdd"><input type="hidden" name="uProperty" value="'+uProperty+'"/><span class="hover" style="padding:0 5px; display: none"><img src="../SiteCollectionImages/icons/greenroom-panel-icon-plus.png">Add to my cities</span></a>' + '<span class="quick-links-full" style="padding:0 5px; display: none"><img src="../SiteCollectionImages/icons/greenroom-panel-icon-delete.png" style="width:10px;height:10px"><span style="padding:0 5px;float:right">Quick links full</span></span>' + '</li>';
						}

						$('#AddQuickLinksContainer ul').append(qucikString);
					}
			},
            error: function(xhr, textStatus, errorThrown) {				
                console.log("error 1:"+JSON.stringify(xhr));
            }
        });

     setTimeout(function(){$(".btnRemove").click(function() {
            var uProperty = $(this).find('input').val();
			var action='Remove';
            WorldClock.updateUserProfileValue(WorldClock.LoginName, WorldClock.userProfileProperty, uProperty, false,action);
		});}, 500);

    setTimeout(function(){$(".btnAdd").click(function() {
		   var uProperty = $(this).find('input').val();
		   var action='Add';
		   WorldClock.updateUserProfileValue(WorldClock.LoginName, WorldClock.userProfileProperty, uProperty,false, action);
        });}, 500);
		
	$('.quick-link-pages').hover(function() {
		var myQuickLinksLength = $('#EditQuickLinksContainer').find('li').length;
		var myCustomLinksLength	= $('#CustomQuickLinksContainer').find('li').length;
		var totalLinksLength = myQuickLinksLength + myCustomLinksLength;
		if (totalLinksLength < 13) {
			$(this).find("span.hover").toggle();
		} else {
			$(this).find("span.quick-links-full").toggle();
		}
		});
	},
	loadClockValuesFromUserProfile : function()
	{
		$.ajax({  
            url: _spPageContextInfo.webAbsoluteUrl + "/_api/SP.UserProfiles.PeopleManager/GetMyProperties",  
            headers: { Accept: "application/json;odata=verbose" },  
            success: function (data) { 
                try {  
                      var properties = data.d.UserProfileProperties.results;  
						for (var i = 0; i < properties.length; i++) {
							var property=properties[i];
							if (property.Key == WorldClock.userProfileProperty) {  
								$.each(property.Value.split(","),function(i,val){
								var label="";
								if(val!="")
								{
									WorldClock.uProfileClockValues = WorldClock.uProfileClockValues + "," +val;
									
									label=val.split("@#@")[1]
									
									if (label.indexOf("$")!=-1)
									{
									label= label.replace(/\$/g, ",");
									}
									var qucikString  = '<li style="float: left; padding:0 5px;"><a href="#" style="display:block;font-weight:bold;padding-top:10px;min-height:90px;"><span>' + label + '</span></a><a href="#" style="display:block" id="btnRem" class="btnRemove"><input type="hidden" name="uProperty" value="'+val+'"/><span>(remove)</span></a></li>';
									$('#EditQuickLinksContainer ul').append(qucikString);
								}
							});							
						}                        
                    }					
					WorldClock.successgetQuickLinks();
                } catch (err2) {  
                   alert(JSON.stringify(err2));  
                }  
            },  
            error: function (jQxhr, errorCode, errorThrown) {  
                alert(errorThrown);  
            }  
        });
	},

    WorldClock: function() {
        location.href = 'EditQuickLinks.aspx';
    },

    failure: function(sender, args) {
		
        //console.log(args.get_message() + '\n' + args.get_stackTrace());
    },
	failureGetArticle: function(sender, args) {
        //console.log(args.get_message() + '\n' + args.get_stackTrace());
		location.reload();
    },
	
	updateClockValue :function(uProperty)
	{   
		$.ajax({    
            url: _spPageContextInfo.webAbsoluteUrl + "/_api/SP.UserProfiles.PeopleManager/GetMyProperties",  
            headers: { Accept: "application/json;odata=verbose" },  
            success: function (data) { 
			WorldClock.LoginName = data.d.AccountName;			
			console.log(WorldClock.LoginName);
			},
			error: function (jQxhr, errorCode, errorThrown) {  
                alert(errorThrown);  
			}  
        }); 
	},
	updateUserProfileValue: function(LoginName, propertyName, propertyValue, isMultiValue,action) {
        propertyValue = propertyValue.replace(/\|/g, '');
        propertyValue = propertyValue.replace(/\&/g, '&amp;');		
		var propertyValues = propertyValue.split('@#@');
		var myLinks = "";
		var clockValues = WorldClock.uProfileClockValues;
		if(action=='Remove')
		{
			clockValues = clockValues.replace(propertyValue,"");
		}
		else
		{
			clockValues = clockValues + "," + propertyValue;
		}
		var domain, userName;
	
		if(LoginName != undefined && LoginName != null && LoginName !="")
		{
			domain = LoginName.split("\\")[0];
			userName = LoginName.split("\\")[1];
		}
		var userprofileServiceURL=GetUserProfileUrlFromLocalStarageJson();
		var siteUrl = GetMySiteUrlFromLocalStarageJson();
		$.support.cors = true;
		var jsonDataType = "json";
		if(isMultiValue){
		$.ajax({
               url: userprofileServiceURL,
			   dataType: jsonDataType,
               type: "GET",
			   data: { domain : domain, userName: userName, propertyName: propertyName, value: clockValues, siteUrl: siteUrl, isMultiValued:"true" },
			   success: function (data) {
                   if(data.indexOf("Commit Success") > -1)
				   {
					   location.reload();
				   }
				   else
				   {
					   if(console != undefined && console != null)
					   {
							console.log(data);
					   }
				   }
               },
			   error: function (data) {
                   /* Added code to check the status code and status text in case of IE9
				   as IE couldn't parse the response XML and falls into the error function
				   with all the successful processing at the back end and statusText=success and status=200 */
				   if(data.statusText.toLowerCase() == "success" && data.status == 200)
				   {
					   location.reload();
				   }
				   //If any errors occurred - detail them here
				   else if(console != undefined && console != null)
				   {
						//console.log(data);
				   }
               }               
           });
		}
		else{
			$.ajax({
               url: userprofileServiceURL,
			   dataType: jsonDataType,
               type: "GET",
			   data: { domain : domain, userName: userName, propertyName: propertyName, value: clockValues, siteUrl: siteUrl},
			   success: function (data) {
                   //Grab our data from Ground Control
                   if(data.indexOf("Commit Success") > -1)
				   {
					   location.reload();
				   }
				   else
				   {
					   if(console != undefined && console != null)
					   {
					   }
				   }
               },
			   error: function (data) {
                   /* Added code to check the status code and status text in case of IE9
				   as IE couldn't parse the response XML and falls into the error function
				   with all the successful processing at the back end and statusText=success and status=200 */
				   if(data.statusText.toLowerCase() == "success" && data.status == 200)
				   {
					   location.reload();
				   }
				   //If any errors occurred - detail them here
				   else if(console != undefined && console != null)
				   {
				   }
               }               
           });
	   }
    }
}

$(document).ready(function() {	
	//SP.SOD.executeFunc('sp.js', null, function(){
	WorldClock.updateClockValue();
    WorldClock.loadClockValuesFromUserProfile();
	//})	
});
