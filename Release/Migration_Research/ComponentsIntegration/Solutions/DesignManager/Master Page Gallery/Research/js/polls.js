google.load("visualization", "1", {packages:["corechart"]});

var Viacom = Viacom || {};
Viacom.Adspace = Viacom.Adspace || {};
Viacom.Adspace.Poll = {};

Viacom.Adspace.Poll.Constants = function(){
	return{
		requestUri : null,
	}
};

var selectedAnswer;
var currentUser;
var answerListTitle = "PollAnswers";
var pollConfigTitle = "PollConfig";
var activeClass="";
var firstPollID = null;
var selectedPoll;
var poleItemArr = [];


$(document).ready(
	function(){
	
	SP.SOD.executeFunc('SP.js', 'SP.ClientContext', Viacom.Adspace.Poll.FindPollItems);
	
	}
);



Viacom.Adspace.Poll.FindPollItems = function()
{

//	$("#pollLoading").show();
var caml = "<View><Query><Where><And><And><Eq><FieldRef Name='PollIsActive' /><Value Type='Boolean'>1</Value></Eq><Leq><FieldRef Name='PollStartDate' /><Value Type='DateTime'><Today /></Value></Leq></And><Geq><FieldRef Name='PollsEndDate' /><Value Type='DateTime'><Today /></Value></Geq></And></Where></Query></View>";
	
	getListItems(_spPageContextInfo.siteAbsoluteUrl,caml,pollConfigTitle)
			.done(function(data)
			{
				var myAnswer = 'NAN';
				var index = 'NAN';
				var IsCurrentUserPolled = false;
				var isActiveAssigned= true;
				if(data.d.results.length > 0)
				{
			   		var items = data.d.results;
			   					   		
			  		for(var i = 0; i < items.length;i++) {
			   			
			   			if(i==0)
			   			{
			   				firstPollID = items[i].ID;
			   			}
						
			           var html="";
			           var showResult = items[i].ShowResults;
			           var ChartType= items[i].ChartType;
			           var itemID= items[i].ID;
			           var pollID = 'poll' + items[i].ID;
			           
			           poleItemArr[poleItemArr.length] = {
			                ItemID: items[i].ID,
			                PoleName: items[i].PollName,
			                Question: items[i].PollQuestion,
			                Answers: items[i].PollAnswers,
			                showResult: items[i].ShowResults,
			                ChartType: items[i].ChartType
			                };

			           
						
			           $.ajax({
			           async: true,
			           type: "POST",
			           url: _spPageContextInfo.siteAbsoluteUrl + "/Pages/WebMethods.aspx/GetPollUsersInfo",
			           data: "{'siteURL':'" + _spPageContextInfo.siteAbsoluteUrl + "','PollLookupValue':'"+items[i].PollName+"','listname':'"+answerListTitle+"','userID':'"+ _spPageContextInfo.userId +"','indexCounter':'"+i+ "'}",
			           contentType: "application/json",
			           dataType: "json",
			           success: function (e) {
			           
			          	try {
			          	
			          		if(JSON.parse(e.d)[0].MyResponse != "No item found.")
			          		{
			          			myAnswer = JSON.parse(e.d)[0].MyResponse;
			          			IsCurrentUserPolled = true;	
								index = JSON.parse(e.d)[0].Index;
			          		}
			          		else
			          		{
			          			myAnswer = 'NAN';	
			          			index = JSON.parse(e.d)[0].Index;
	
			          		}
			          		
			          		if(JSON.parse(e.d)[0].MyResponse == "No item found." == "" || myAnswer == 'NAN')
			          		{
			          			if(activeClass == "")
			          			{
			          			  activeClass = 'poll' + items[index].ID;
			          			}
			          		}
			          		
			                  
					                if(myAnswer != 'NAN')
					                {
				               			if(items[index].ShowResults == true)
						               		{
						               			var chartID = 'chart' + items[index].ID;
						               		    

						               			if(index == 0 && isActiveAssigned== true)
												{     
													 html = "<div class='item active' ID='"+'poll' + items[index].ID+"'><h4>"+items[index].PollQuestion+"</h4>";
													 html = html + "<div id='"+ chartID +"' ></div>";
													 html = html + "<h5>My Response :  "+ myAnswer  + "</h5>";

													 $('.carousel-inner').append(html);										         
												}
												else
												{
													  html = "<div class='item' ID='"+'poll' + items[index].ID+"'><h4>"+items[index].PollQuestion+"</h4>";
													  html = html + "<div id='"+chartID +"' ></div>";
													  html = html + "<h5>My Response :  "+ myAnswer  + "</h5>";
													  $('.carousel-inner').append(html);						
												}
												
												    var ChartArr = [['Answers','Count']];
										           if(e.d != "")
									          		{
									          			var ansItems = JSON.parse(e.d);
													     for(var k = 0; k < ansItems.length;k++) {
													     	ChartArr.push([ansItems[k].AnswerChoice,parseInt(ansItems[k].Count)]);

													     }
									          		}
									          		
													var choices =  items[index].PollAnswers.split("\n");												  
													for(var j = 0; j < choices.length;j++) {
													
													var IsExist = $.grep(ChartArr, function(v) {
																return v[0] == choices[j];
															});
														if(IsExist.length == 0)
														{
														   ChartArr.push([choices[j],0])
														}
													} 
												    
											        var data = google.visualization.arrayToDataTable(ChartArr);										
											        
													if(items[index].ChartType === "Pie")
													{
													var options = {
											          title: '' };
														var chart = new google.visualization.PieChart(document.getElementById(chartID));											
												        chart.draw(data, options);
													}
													else if(items[index].ChartType === "Bar")
													{
														var options = {
											          title: '',
													  legend: 'none'
											        };
														 var chart = new google.visualization.BarChart(document.getElementById(chartID ));											
												         chart.draw(data, options);
													}
													else if(items[index].ChartType === "Line")
													{
														var options = {
											          title: '',
													  legend: 'none',
													  pointSize: 10,
													  pointShape: 'circle'
											        };
														 var chart = new google.visualization.LineChart(document.getElementById(chartID));											
												         chart.draw(data, options);
													}											
											       
													
						               		}
						               		else
						               		{
							               		if(index == 0 && isActiveAssigned== true)
												{
												  html = "<div class='item active' ID='"+'poll' + items[index].ID+"'><h4>"+items[index].PollQuestion+"</h4>";
												  var choices =  items[index].PollAnswers.split("\n");
												  var itemID  = items[index].ID + "#" + items[index].ShowResults + "#" + items[index].ChartType;
												  
												  html = html + "<h5>My Response :  "+ myAnswer  + "</h5>";
												  html   = html   + "<input type='button' id="+itemID+" class='btn' onclick='Viacom.Adspace.Poll.NewPoleEntry(this);' title='"+items[index].PollName+";#;"+myAnswer+"' value='Vote' disabled></div>";
													
												  $('.carousel-inner').append(html);
												 
												}
												else
												{
												  html = "<div class='item' ID='"+'poll' + items[index].ID+"'><h4>"+items[index].PollQuestion+"</h4>";
												  var choices =  items[index].PollAnswers.split("\n");
												  var itemID  = items[index].ID + "#" + items[index].ShowResults + "#" + items[index].ChartType;			          
												  
													html = html + "<h5>My Response :  "+ myAnswer  + "</h5>";
													html   = html   + "<input type='button' id="+itemID+" class='btn' onclick='Viacom.Adspace.Poll.NewPoleEntry(this);' title='"+items[index].PollName+";#;"+myAnswer+"' value='Vote' disabled></div>";
													
													$('.carousel-inner').append(html);						
												}
						               		}
					                }
									else
									{
										if( isActiveAssigned== true)
												{     
												  //Remove active class from other poll.
												   $("div.active").removeClass("active");
												   isActiveAssigned= false;
												  
												  html = "<div class='item active' ID='"+'poll' + items[index].ID+"'><h4>"+items[index].PollQuestion+"</h4><div>";
												  var choices =  items[index].PollAnswers.split("\n");
												   var itemID  = items[index].ID + "#" + items[index].ShowResults + "#" + items[index].ChartType;
											
												  
													for(var j = 0; j < choices.length;j++) {
													var name = "poll" + items[index].ID;
													html  = html  + "<label><input type='radio' value='"+ choices[j] +"' name='"+name+"'>"+choices[j]+"</label>"; 
													} 
													
													if(e.d != "")
													{
														html   = html   + "<input type='button' id="+itemID+" class='btn' onclick='Viacom.Adspace.Poll.NewPoleEntry("+'this,'+ e.d +");' title='"+items[index].PollName+";#;"+myAnswer+"' value='Vote'></div>";
													}
													else
													{
														html   = html   + "<input type='button' id="+itemID+" class='btn' onclick='Viacom.Adspace.Poll.NewPoleEntry(this);' title='"+items[index].PollName+";#;"+myAnswer+"' value='Vote'></div>";
													}
													
													$('.carousel-inner').append(html);												 
												}
												else
												{
												  html = "<div class='item' ID='"+'poll' + items[index].ID+"'><h4>"+items[index].PollQuestion+"</h4>";

												  var choices =  items[index].PollAnswers.split("\n");
												  var itemID  = items[index].ID + "#" + items[index].ShowResults + "#" + items[index].ChartType;			          
												  
													for(var j = 0; j < choices.length;j++) {
													var name = "poll" + items[index].ID;
													html  = html  + "<label><input type='radio' value='"+ choices[j] +"' name='"+name+"'>"+choices[j]+"</label>"; 
													} 
													
													if(e.d != "")
													{
														html   = html   + "<input type='button' id="+itemID+" class='btn' onclick='Viacom.Adspace.Poll.NewPoleEntry("+'this,'+ e.d +");' title='"+items[index].PollName+";#;"+myAnswer+"' value='Vote'></div>";
													}
													else
													{
														html   = html   + "<input type='button' id="+itemID+" class='btn' onclick='Viacom.Adspace.Poll.NewPoleEntry(this);' title='"+items[index].PollName+";#;"+myAnswer+"' value='Vote'></div>";
													}
																										
													$('.carousel-inner').append(html);
						
												}
				               			
									}

			               }
			               catch (t) {	
							// $("#pollLoading").hide();
							  console.log("exception while rendering poll html. " + t.message, "Poll","polls.js", "getListItems")
			               }
						   
			            if(index == items.length-1)
						 {
						 		if(activeClass != "")
						 		{
								//	$("#poll"+firstPollID).removeClass();
								//	$("#" + 'poll' + items[index].ID + "").addClass("item active");
								//	$("#poll"+firstPollID).addClass("item");
								}	
								
							//$("#pollLoading").hide();
						 }
						 
			           }, 
			           error : function(data){
					 //  $("#pollLoading").hide();
					      console.log("Exception while fetching data from poll annswer list. " + data.message, "Poll","polls.js", "getListItems")
					    }
			       });	
					}  
					
					$("#pollLoading").hide();
					// Hide next previous button for single pole
					if(data.d.results.length == 1){
						$("a.carousel-prev").hide();
						$("a.carousel-next").hide();
					
					}
					
					
			    }
				else
				{
				//	$("#pollLoading").hide();
					$("#pollHeading").hide();
					$("#polls").hide();
					$("a.carousel-prev").hide();
					$("a.carousel-next").hide();
				}
			})
			.fail(
			function(error){
		//	$("#pollLoading").hide();
			
			console.log(JSON.stringify(error), "Poll","polls.js", "getListItems")
			});
    
};


Viacom.Adspace.Poll.NewPoleEntry = function(e,data)
{

        selectedPoll = $(".item.active").attr("ID");
        $("#carousel").on('slide.bs.carousel', function (evt) {
            selectedPoll = $(evt.relatedTarget)[0].id;
        });

        selectedAnswer = $("input:radio[name=" + selectedPoll + "]:checked").val();

		if(selectedAnswer != undefined || selectedAnswer != null)
		{
		var ControlIDValue = $(e).attr('id');
		var ID = ControlIDValue.split("#")[0];
		var showResult = ControlIDValue.split("#")[1];
		var ChartType = ControlIDValue.split("#")[2];
		var chartID = "chart" + ID;
		var currentPollData = data;
		
		var currentPollItem = $.grep(poleItemArr, function(v) {
									return v.ItemID == ID;
								});
		
		
		var ansChoices = currentPollItem[0].Answers;
		var PollQuestion = currentPollItem[0].Question;
		var siteUrl=_spPageContextInfo.siteAbsoluteUrl;
		var clientContext = new SP.ClientContext(siteUrl);
        var oList = clientContext.get_web().get_lists().getByTitle('PollAnswers');
        var itemCreateInfo = new SP.ListItemCreationInformation();
        var newListItem = oList.addItem(itemCreateInfo);
		newListItem.set_item('PollUserResponse',selectedAnswer);
        var CurrentpoleName = new SP.FieldLookupValue();
        CurrentpoleName.set_lookupId(ID); //â€1â€ is the other list item ID
        newListItem.set_item('PollNameLookUp', CurrentpoleName);
        newListItem.set_item('PollQuestion', PollQuestion);

        newListItem.update();
        clientContext.load(newListItem);
		clientContext.executeQueryAsync(Function.createDelegate(this, function () {
		
						alert("Vote submitted!  Thanks for participating in this poll");						
						$("#poll" +ID+ "").empty();	
						if(currentPollData != undefined)
						{	
							var html ="";							
							if(showResult == 'true')
							{						
								html = html + "<h4>"+ PollQuestion  +"</h4>";		
								html = html + "<div id='chart"+ ID +"' ></div>";
								html = html + "<h5>My Response :  "+ selectedAnswer + "</h5>";
								$("#poll"+ID).append(html);			
							
									

								var maxCountArr = [];
								var ChartArr = [['Answers','Count']];
								 for(var k = 0; k < currentPollData.length;k++) {
										ChartArr.push([currentPollData[k].AnswerChoice,parseInt(currentPollData[k].Count)]);
										maxCountArr.push(parseInt(currentPollData[k].Count));
									}
								 
								var foundAns = 'No';
								 for(var k = 1; k < ChartArr.length;k++) {								 
										if(ChartArr[k][0] == selectedAnswer)
										{
											foundAns = 'Yes';
											ChartArr[k][1] = ChartArr[k][1] + 1;
										}										     	
									 }
									 
									 if(foundAns == 'No')
									 {
										ChartArr.push([selectedAnswer,1]);
									 }								 
									 
									 var choices =  ansChoices.split("\n");												  
													for(var j = 0; j < choices.length;j++) {
													
													var IsExist = $.grep(ChartArr, function(v) {
																return v[0] == choices[j];
															});
														if(IsExist.length == 0)
														{
														   ChartArr.push([choices[j],0])
														}
													} 
									 
									 var data = google.visualization.arrayToDataTable(ChartArr);							
										
										if(ChartType === "Pie")
													{
														var options = { title: '' };
														var chart = new google.visualization.PieChart(document.getElementById(chartID));											
												        chart.draw(data, options);
													}
													else if(ChartType === "Bar")
													{
														var options = { title: '',
														legend: 'none'
											        };
														 var chart = new google.visualization.BarChart(document.getElementById(chartID ));											
												         chart.draw(data, options);
													}
													else if(ChartType === "Line")
													{
														var options = {
											          title: '',
													  legend: 'none',
													  pointSize: 10,
													  pointShape: 'circle'
											        };
														 var chart = new google.visualization.LineChart(document.getElementById(chartID));											
												         chart.draw(data, options);
													}
										
							}
							else
							{			
									html = html + "<h4>"+ PollQuestion  +"</h4>";														 
									html = html + "<h5>My Response :  "+ selectedAnswer + "</h5>";
									$("#poll"+ID).append(html);
							}
						
							 	
						}
						else{
							if(showResult == 'true')
							{
							
								   var html ="";
								    html = html + "<h4>"+ PollQuestion  +"</h4>";
								    html = html + "<div id='chart"+ ID +"' ></div>";
									html = html + "<h5>My Response :  "+ selectedAnswer + "</h5>";
									$("#poll"+ID).append(html);
								var ChartArr = [['Answers','Count']];
								ChartArr.push([selectedAnswer,1]);
								
								 var choices =  ansChoices.split("\n");												  
													for(var j = 0; j < choices.length;j++) {
													
													var IsExist = $.grep(ChartArr, function(v) {
																return v[0] == choices[j];
															});
														if(IsExist.length == 0)
														{
														   ChartArr.push([choices[j],0])
														}
													} 

								
								
								
								
								var data = google.visualization.arrayToDataTable(ChartArr);							
										
										if(ChartType === "Pie")
													{
														var options = {
											          title: '' };
														var chart = new google.visualization.PieChart(document.getElementById(chartID));											
												        chart.draw(data, options);
													}
													else if(ChartType === "Bar")
													{
														var options = {
											          title: '',
													  legend: 'none'
											        };
														 var chart = new google.visualization.BarChart(document.getElementById(chartID ));											
												         chart.draw(data, options);
													}
													else if(ChartType === "Line")
													{
														var options = {
											          title: '',
													  legend: 'none',
													  pointSize: 10,
													  pointShape: 'circle'
											        };
														 var chart = new google.visualization.LineChart(document.getElementById(chartID));											
												         chart.draw(data, options);
													}
										
							}
							else{
									var html ="";
									html = html + "<h4>"+ PollQuestion  +"</h4>";
									html = html + "<h5>My Response :  "+ selectedAnswer + "</h5>";
									$("#poll"+ID).append(html);	
									
							}
							
						}	
						
						
			}), onQueryFailed);	
		}
		else
		{
			alert("Please select an answer before voting");
		}
		

};


function onQueryFailed(args)
{
//$("#pollLoading").hide();
console.log("Unable to insert poll answer into list." + args.message, "Poll", "Polls.js", "onQueryFailed");
}


function getListItems(webUrl,caml,listTitle ) {	
	
	//$("#pollLoading").show();
	var urltest = webUrl + "/_api/Web/lists/getByTitle('"+listTitle +"')/getitems(query=@v1)?@v1={\"ViewXml\":\"" + caml + "\" }}";         
			 	return	$.ajax({
                url: urltest,
                async : true,
                type: "POST",
                headers: {
                    "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                    "Accept": "application/json; odata=verbose",
                    "Content-Type": "application/json; odata=verbose"
                }, 
			  complete: function (xhr,status){
			//	$("#pollLoading").hide();
			  }
            });
}

 
