var Viacom = Viacom || {};
Viacom.Asia = Viacom.Asia || {};
Viacom.Asia.Count = 0;
Viacom.Asia.getActiveAnnouncements = function () {
if(ctx.ListTitle === "Announcements")
	{
    Viacom.Asia.Count++;
    var Items = "";
    var annTitle = ctx.CurrentItem.Title;
	var annCategory;
	if(ctx.CurrentItem.AnnouncementCategory != undefined)
	 annCategory = ctx.CurrentItem.AnnouncementCategory[0].lookupValue;
	var annModified = ctx.CurrentItem.Modified;
  	var annEditor;
	if(ctx.CurrentItem.Editor!= undefined)
	annEditor = ctx.CurrentItem.Editor[0].title;

	var url = _spPageContextInfo.webAbsoluteUrl+"/Lists/Announcements/DispForm.aspx?ID="+ctx.CurrentItem.ID  + "&Source=" +_spPageContextInfo.siteAbsoluteUrl + _spPageContextInfo.serverRequestPath + "&IsDlg=1";
    Items += '<tr><td><div class="announcement-box subsiteAnnouncements"><h3><a style="cursor: pointer;" src='+url +' onclick="openInDialog(this)">' + annTitle + '</a></h3></div></td><td><div class="announcement-box subsiteAnnouncements"><h3>' +annCategory +'</h3></div></td><td><div class="announcement-box subsiteAnnouncements"><h3>' +annEditor+ '</h3></div></td><td><div class="announcement-box subsiteAnnouncements"><h3>' +annModified+ '</h3></div></td></tr>';
	return Items
	}
	else
	return RenderItemTemplate(ctx);
};
Viacom.Asia.getHeader = function () {
	if(ctx.ListTitle === "Announcements")
    {
		var header = '<div class="content-padding"><div class="top-announcement"><table><tbody><tr><th>Title</th><th>Category</th><th>Modified By</th><th>Last Modified</th></tr>';
		return header
	}
	else
	return RenderHeaderTemplate(ctx);
};

Viacom.Asia.getFooter = function () {
	if(ctx.ListTitle === "Announcements")
    {
		var footerHtml = "";
        var firstRow = ctx.ListData.FirstRow;
        var lastRow = ctx.ListData.LastRow;
        var prev = ctx.ListData.PrevHref;
        var next = ctx.ListData.NextHref;

           footerHtml += "<table class=\"ms-bottompaging\"><tr><td>";
            if (prev)
                footerHtml += "<a class='ms-commandLink ms-promlink-button ms-promlink-button-enabled' href='" + prev + "'><span class='ms-promlink-button-image'><img class='ms-promlink-button-left' src='/_layouts/15/images/spcommon.png?rev=23' /></span></a>";
            else
                footerHtml += "<a class='ms-commandLink ms-promlink-button ms-promlink-button-disabled' href='javascript:void(0);'><span class='ms-promlink-button-image'><img class='ms-promlink-button-left-disabled' src='/_layouts/15/images/spcommon.png?rev=23' /></span></a>";

            footerHtml += "<span class=\"ms-promlink-button-inner\"></span>";

            if (next)
                footerHtml += "<a class='ms-commandLink ms-promlink-button ms-promlink-button-enabled' href='javascript:void(0);' onclick=\"RefreshPageTo(event, &quot;" + next + "&quot;);return false;\"><span class='ms-promlink-button-image'><img class='ms-promlink-button-right' src='/_layouts/15/images/spcommon.png?rev=23'/></span></a>";
            else
                footerHtml += "<a class='ms-commandLink ms-promlink-button ms-promlink-button-disabled' href='javascript:void(0);'><span class='ms-promlink-button-image'><img class='ms-promlink-button-right-disabled' src='/_layouts/15/images/spcommon.png?rev=23' /></span></a>";

            footerHtml += "</td></tr></table>";

        return footerHtml;
	}
	else
	return RenderFooterTemplate(ctx);
};

(function () {
var overrideCtx = {};
overrideCtx.Templates = {};

overrideCtx.Templates.Header = Viacom.Asia.getHeader;
overrideCtx.Templates.Item = Viacom.Asia.getActiveAnnouncements; + '</tbody></table></div></div>';
//overrideCtx.Templates.Footer = Viacom.Asia.getFooter; 

 
SPClientTemplates.TemplateManager.RegisterTemplateOverrides(overrideCtx);
})();

function openInDialog(that) {
		var modal_url = $(that).attr('src');
         var options = {
             url: modal_url,
			 title:'',
			 height:385 
         };
       SP.SOD.execute('sp.ui.dialog.js', 'SP.UI.ModalDialog.showModalDialog', options);
	   $('#dialogTitleSpan').hide();
}
