var IsSBDTLikePageDisabled =false;
var IsDTLikePageDisabled=false;
var ConfigListData;
var SubSiteColorsListData;
var IsKaleoInjctorLoaded=false;
$(document).ready(function() {
	GetConfigListDataJson();
});


function GetConfigListDataJson(){		
		var retrievedObject = localStorage.getItem('ConfigListDataJson');
		ConfigListData=JSON.parse(retrievedObject);
		var callConfig = $.ajax({
				url: _spPageContextInfo.webAbsoluteUrl + "/_api/lists/getbytitle('config')/items?" +
					"&$select=Title,sitepath,FetchProfileProperties", 
				type: "GET",
				dataType: "json",
				headers: {
					Accept: "application/json;odata=verbose"
				}
			});
			callConfig.done(function(data, textStatus, jqXHR) {
			try 
			{
				ConfigListData=data.d;
				localStorage.setItem('ConfigListDataJson', JSON.stringify(ConfigListData));
			}
			catch (e) {
			//console.log(e);
			}
			});
			callConfig.fail(function(jqXHR, textStatus, errorThrown) {
			localStorage.setItem('ConfigListDataJson', '{"results":[]}');
			//console.log("Error getting Site Color" + jqXHR.responseText);
			});
	}
	
function GetUserProfileUrlFromLocalStarageJson(){
	var retrievedObject = localStorage.getItem('ConfigListDataJson');
	ConfigListData=JSON.parse(retrievedObject);
	if( ConfigListData !=null && ConfigListData.results !=null && ConfigListData.results.length >0)
	{
	try {
			for(var congigItemInex=0;congigItemInex<ConfigListData.results.length;congigItemInex++)
			{
				var ItemTitle = ConfigListData.results[congigItemInex].Title;
				if(ItemTitle.toLowerCase()==="userprofileserviceurl")
				{
					return ConfigListData.results[congigItemInex].sitepath;
				}
						
			}
					
		}
		catch (e) {
		//console.log(e);
		}
	}
}
function GetMySiteUrlFromLocalStarageJson(){
	var retrievedObject = localStorage.getItem('ConfigListDataJson');
	ConfigListData=JSON.parse(retrievedObject);
	if( ConfigListData !=null && ConfigListData.results !=null && ConfigListData.results.length >0)
	{
	try {
			for(var congigItemInex=0;congigItemInex<ConfigListData.results.length;congigItemInex++)
			{
				var ItemTitle = ConfigListData.results[congigItemInex].Title;
				if(ItemTitle.toLowerCase()==="mysiteurl")
				{
					return ConfigListData.results[congigItemInex].sitepath;
				}
						
			}
					
		}
		catch (e) {
		//console.log(e);
		}
	}
}
