var j=0;
var Order=50;
var SaveFinalOrder=0;
var finalsaveCount=0;
var getWebCountBefore=0;
var getWebCountAfter=0;
var getArticleCountBefore=0;
var getArticleCountAfter=0;
var IsDisplayArticleNotCompleted=true;
var displayloopcount=0;
var saveloopcount=0;
var arr = new Array();
var arrStatus = new Array();
var FinalOrder = new Array();
var AllSubwebs = new Array();
var nonzeroArr = new Array();
var zeroArr = new Array();
var flag=0;
var PromotionOrderRows;
var sections = [];
var GRflag=false;
var GRUrl = "";
var AsiaUrl = "";
var GetInvolvedSite = "/getinvolved/";
var RelativeUrl = (_spPageContextInfo.webServerRelativeUrl.substring(1)).concat('/');

var Constants = {
      
   //Page names
   'SITE_RELATIVE_URL': "sites/Asia/",
   
};

$(document).ready(function () {
GetSiteUrls();
getGreenRoomArticles();
});

function getGreenRoomArticles()
{
	var todaysDate=new Date();
	todaysDate.setDate(todaysDate.getDate() - 1);
	var tomorrowtodaysDate=new Date();
	tomorrowtodaysDate.setDate(tomorrowtodaysDate.getDate() + 1);
	
	//query used == http://sp2013-dev-12:8081/_api/search/query?querytext='((contenttype:" Green Room Gallery Article Page") OR (contenttype:"Green Room Article Page") OR (contenttype:"Green Room Video Article Page") OR (contenttype:"Green Room Survey Article Page") OR (contenttype:"Green Room Article Page") OR (contenttype:"Green Room Event Article Page")) AND PromotionRequestOWSCHCM:Homepage AND IsaFeaturedArticleOWSBOOL:true AND ShownOnHomepageOrderOWSNMBR:"1*" AND PromotionStartDate<=Today AND PromotionEndDate>=Today' &selectproperties='AuthorOWSUSER,Title,ID,FileRef,Created,PromotionEndDate,PromotionStartDate,ShownOnHomepageOrderOWSNMBR,PromotionRequestOWSCHCM,Author,IsaFeaturedArticleOWSBOOL,Path,AuthorOWSTEXT,ListItemID'&trimduplicates=false &rowlimit=2

	var greenRoomCall = $.ajax({
			async: false,
			url:GRUrl+ "/_api/search/query?querytext=%27((contenttype:%22%20Green%20Room%20Gallery%20Article%20Page%22)%20OR%20(contenttype:%22Green%20Room%20Article%20Page%22)%20OR%20(contenttype:%22Green%20Room%20Video%20Article%20Page%22)%20OR%20(contenttype:%22Green%20Room%20Survey%20Article%20Page%22)%20OR%20(contenttype:%22Green%20Room%20Article%20Page%22)%20OR%20(contenttype:%22Green%20Room%20Event%20Article%20Page%22))%20AND%20PromotionRequestOWSCHCM:Homepage%20AND%20IsaFeaturedArticleOWSBOOL:true%20AND%20ShownOnHomepageOrderOWSNMBR:%221*%22%20AND%20PromotionStartDate%3C=Today%20AND%20PromotionEndDate%3E=Today%27%20&selectproperties=%27AuthorOWSUSER,Title,ID,FileRef,Created,PromotionEndDate,PromotionStartDate,ShownOnHomepageOrderOWSNMBR,PromotionRequestOWSCHCM,Author,IsaFeaturedArticleOWSBOOL,Path,AuthorOWSTEXT,ListItemID%27&sortlist=%27ShownOnHomepageOrderOWSNMBR%3Aascending%27&trimduplicates=false%20&rowlimit=2",

			type: "GET",
			dataType: "json",
			headers: {
			Accept: "application/json;odata=verbose"
			}
		});
	
	greenRoomCall.done(function (data,textStatus, jqXHR){
		 try {
            var arrSiteCollection = data.d.query.PrimaryQueryResult.RelevantResults.Table.Rows.results;
              for (var i = 0 ; i < arrSiteCollection.length ; i++) {
                var currItem = arrSiteCollection[i].Cells.results;
				var promotionReq = GetValueFormSearchResult(currItem, "PromotionRequestOWSCHCM");
				if(IsValidForFeaturingNow(promotionReq) )
				{
                var showOrder = GetValueFormSearchResult(currItem, "ShownOnHomepageOrderOWSNMBR");
                var author = GetValueFormSearchResult(currItem, "Author");
                var title = GetValueFormSearchResult(currItem, "Title");
                var docId = GetValueFormSearchResult(currItem, "ListItemID");
                var path = GetValueFormSearchResult(currItem, "Path");
                var isaFeaturedArticleOWSBOOL = GetValueFormSearchResult(currItem, "IsaFeaturedArticleOWSBOOL");
                var promotionStartDate = GetValueFormSearchResult(currItem, "PromotionStartDate");
                var promotionEndDate = GetValueFormSearchResult(currItem, "PromotionEndDate");   
				
                arr.push({ID: docId, Title: title, PSite: "GREENROOM", ShowOrder: showOrder, FileRef: path, IsaFeaturedArticle: isaFeaturedArticleOWSBOOL, PromotionStartDate: promotionStartDate, PromotionEndDate: promotionEndDate, PromotionRequest: promotionReq, CreatedBy: author});
                }
				}
		getArticles();
		splitArray();
		nonzeroArr.sort(compare); 
		combineArray();
		display();
		}
		catch(e){
		getArticleCountAfter++;
		}
	});
	
	greenRoomCall.fail(function (jqXHR,textStatus,errorThrown){
		getArticleCountAfter++;
	});
}

function getArticles()
{
	var todaysDate=new Date();
	todaysDate.setDate(todaysDate.getDate() - 1);
	var tomorrowtodaysDate=new Date();
	tomorrowtodaysDate.setDate(tomorrowtodaysDate.getDate() + 1);
	getArticleCountBefore++;
	var call = $.ajax({
			async: false,
			url: AsiaUrl+"_api/lists/getbytitle('Pages')/items?"+
					"$select=Title,ID,FileRef,Created,PublishingPageLayout,ShownOnHomepageOrder,IsaFeaturedArticle,PromotionEndDate,PromotionStartDate,PromotionRequest,Author/Title&$expand=Author"+
					"&$filter=((ContentType eq 'Konda Article Page'"+
					"or ContentType eq 'Konda Event Article Page'"+
					"or ContentType eq 'Konda Gallery Article Page'"+
					"or ContentType eq 'Konda Survey Article Page'"+
					"or ContentType eq 'Konda Video Article Page') and (PromotionEndDate gt datetime'" + todaysDate.toISOString() + "' ) and (PromotionStartDate lt datetime'" + tomorrowtodaysDate.toISOString() + "' ) and (IsaFeaturedArticle eq 1) and (substringof('Homepage',  PromotionRequest)))"
					+"&$orderby=ShownOnHomepageOrder&$top=8", 
					
			type: "GET",
			dataType: "json",
			headers: {
			Accept: "application/json;odata=verbose"
			}
		});
	
	call.done(function (data,textStatus, jqXHR){
		try{
			 
			for(i=0;i<data.d.results.length;i++)
			{
				if(data.d.results[i].PromotionRequest)
				{
				var AuthorTitle="";
				if(data.d.results[i].Author)
				{
				 AuthorTitle=data.d.results[i].Author.Title;
				}
				j++;
				
				var ParentSite = data.d.results[i].FileRef.split('/')[3].toUpperCase();
				
				var Path = data.d.results[i].FileRef.split('Pages')[0];
				Path = Path + 'Pages/Forms/EditForm.aspx?ID=' + data.d.results[i].ID;
				if(ParentSite !='PAGES')
				{
					if(IsValidForFeaturingNow(data.d.results[i].PromotionRequest) )
					{
					arr.push({
							ID: data.d.results[i].ID,
							Title: data.d.results[i].Title,
							PSite: ParentSite,
							ShowOrder: data.d.results[i].ShownOnHomepageOrder,
							FileRef: data.d.results[i].FileRef,
							IsaFeaturedArticle: data.d.results[i].IsaFeaturedArticle,
							PromotionEndDate: data.d.results[i].PromotionEndDate,
							PromotionStartDate: data.d.results[i].PromotionStartDate,
							PromotionRequest: data.d.results[i].PromotionRequest,
							Created: data.d.results[i].Created,
							CreatedBy:AuthorTitle,
							URLPath: Path
						});
					}	
				}
			}
			}	
			getArticleCountAfter++;
		}
		
		catch(e){
		getArticleCountAfter++;
		}
	});
	
	call.fail(function (jqXHR,textStatus,errorThrown){
		getArticleCountAfter++;
	});
}


function IsValidForFeaturingNow(promoRequest) {
	var isValidatedForFeaturingNowResult= false;
	if(promoRequest)
	{var promotionRequestCoice=promoRequest.split(";#").filter(function(v){return v!==''}).join(";");
		promotionRequestCoice=promotionRequestCoice.split("\n").filter(function(v){return v!==''}).join(";");
		var promotionRequestCoiceList=promotionRequestCoice.split(";");
		for( var choiceIndex=0;choiceIndex<promotionRequestCoiceList.length;choiceIndex++)
		{
			var choice=promotionRequestCoiceList[choiceIndex];
			if(choice.toLowerCase()=='homepage')
			{
			isValidatedForFeaturingNowResult=true;
			break;
			}
			
		}
		}
	return 	isValidatedForFeaturingNowResult;
}

function compare(a,b) {
  if (a.ShowOrder < b.ShowOrder)
     return -1; 
  if (a.ShowOrder > b.ShowOrder)
    return 1;
  return 0;
}

function GetSiteUrls() {
	AsiaUrl = _spPageContextInfo.siteAbsoluteUrl;
	GRUrl = AsiaUrl.split(_spPageContextInfo.siteServerRelativeUrl)[0];
	AsiaUrl = AsiaUrl + GetInvolvedSite;
}

function display()
{
	$('#PromotionOrder').css('position','relative');
	$('#PromotionOrder').prepend('<div id="loadingAnimation" style="display:none;position: absolute;top: 60%;left: 50%;margin-left: -22px;"><img id="loadingAnimationImg"  src="/SiteAssets/loadingAnimation.gif" /></div>');
	for(var i=0;i<arr.length;i++)
	{
	var EndDate;
	var StartDate;
	if(arr[i].PromotionStartDate)
	 {
		StartDate = new Date(arr[i].PromotionStartDate);
		StartDate = StartDate.format('MMM, dd yyyy');
	 }
	 else{
	 StartDate="";
	 }
	 if(arr[i].PromotionEndDate)
	 {
		EndDate = new Date(arr[i].PromotionEndDate);
		EndDate = EndDate.format('MMM, dd yyyy');
	 }
	 else{
	 EndDate="";
	 }
	 var showOrder;
	 if(arr[i].ShowOrder)
	 {
		showOrder=arr[i].ShowOrder;
	 }
	 else{
	 showOrder="";
	 }
	var promoteInputId=arr[i].FileRef.trim().replace(/[^a-z0-9]+/gi, '-');	
	$('#table-1').append('<tr id="'+arr[i].ID+":"+arr[i].PSite+":"+arr[i].FileRef+'" style="cursor: move;"><td><a target="_blank" href="'+arr[i].FileRef+'">'+arr[i].Title+'</a></td><td>'+arr[i].PSite+'</td><td>'+showOrder+'</td><td>'+StartDate+'</td><td>'+EndDate+'</td><td>'+arr[i].CreatedBy+'</td></tr>');
	}
	$("#table-1").tableDnD({
			        onDragClass: "myDragClass",
			        onDrop: function(table, row) {
			       	while(FinalOrder.length > 0)
			        	{
			        		FinalOrder.pop();
			        	}
			            var rows = table.tBodies[0].rows;
			            var debugStr = "<br /> Row dropped was "+row.id+"<br /> New order: ";
			            for (var i=0; i<rows.length; i++) {
			                debugStr += rows[i].id+"" ; 
							FinalOrder.push(rows[i].id);
			                debugStr += breaktag="<br />";			               
			            }
			            $('#debugArea').html(debugStr);
			        },
			        onDragStart: function(table, row) {
			            $('#debugArea').html("Started dragging row "+row.id);
			        }
			    });

}

function splitArray()
{
	for (var i=0; i<arr.length; i++) {
			if(arr[i].ShowOrder == 0 || arr[i].ShowOrder == null || arr[i].ShowOrder == undefined)
			{
			  zeroArr.push(arr[i]);
			}
			else
			{
			  nonzeroArr.push(arr[i]);
			}
	}
}

function combineArray()
{
	arr.length = 0;
	for (var i=0; i<nonzeroArr.length; i++) {
			  arr.push(nonzeroArr[i]);
	}
	for (var i=0; i<zeroArr.length; i++) {
			  arr.push(zeroArr[i]);
	}
}

function SavePomotionOrderProperties()
{
	$("#loadingAnimation").css("display", "block");
	for(i=0;i<arr.length;i++)
	{
					var Contents = FinalOrder[i+1];
					if(Contents)
					{
					var Data = Contents.split(":");
					var sitePath=Data[2].split('Pages')[0].replace(/^\/|\/$/g, '');
					//remove sites/Asia from sitepath
					var sitepathLower = sitePath.toLowerCase();
					var index = sitepathLower.indexOf(RelativeUrl.toLowerCase());
					if(index!=-1)
						{
							sitePath = sitepathLower.replace(RelativeUrl.toLowerCase(), "");
							var articleUrl=Data[2].split("'").join("''");
							SaveItems(sitePath,Data[0],articleUrl);
							finalsaveCount++;
							if(!GRflag)
							{
								Order++;
							}
						}
						else
						{
							GRflag = true;
							Order=0;
						}
					}			
	}
	IsSaveOrderCompleted();
}
function SaveOrder()
{
	SavePomotionOrderProperties();
	setTimeout(function () {
    alert("Some error occurred this page will be reloaded");
	location.reload();
    }, 500000);
}

function IsSaveOrderCompleted()
{
	saveloopcount++;
	if(SaveFinalOrder==finalsaveCount && saveloopcount<300)
	{
		$("#loadingAnimation").css("display", "none");
		location.reload();
	}
	else{
		setTimeout(function () {IsSaveOrderCompleted();}, 1000);
	}
}

function SaveItems(sitename,ItemID,URL)
{	
try{
var finalPromotionOrder=Order;
	
	$.ajax({url: _spPageContextInfo.webAbsoluteUrl+"/"+sitename+"/_api/web/GetFileByServerRelativeUrl('"+ URL +"')/checkOutType",
	    headers: { "Accept": "application/json; odata=verbose" }, 
        success: function(data) {
				  
                  if(data.d.CheckOutType == 0) {
					SaveFinalOrder++;						
					return;
                  }
            $.ajax({
			url: _spPageContextInfo.webAbsoluteUrl+"/"+sitename+"/_api/web/GetFileByServerRelativeUrl('"+ URL +"')/CheckOut()",
			async:false,
			method: "POST",
			headers: {
			"X-RequestDigest":  $("#__REQUESTDIGEST").val() 
			},
			success: function(data) {
			
			$.ajax({ 
			url:_spPageContextInfo.webAbsoluteUrl+"/"+sitename+ "/_api/web/lists/getbytitle('Pages')/items("+ItemID+")",
			 method: "POST",
				async: false,
				data:  JSON.stringify({"__metadata": {"type": "SP.Data.PagesItem"},"ShownOnHomepageOrder": finalPromotionOrder}),
				headers: { 
					  "X-HTTP-Method":"MERGE",
					"accept": "application/json;odata=verbose",
					"content-type": "application/json;odata=verbose",
					"X-RequestDigest": $("#__REQUESTDIGEST").val(),
					"IF-MATCH": "*"
				},
				success: function(data) {
			   
				
				$.ajax({
					url: _spPageContextInfo.webAbsoluteUrl+"/"+sitename+"/_api/web/GetFileByServerRelativeUrl('"+ URL +"')/CheckIn(comment='Comment',checkintype=1)",
					async:false,
					method: "POST",
					headers: {
						"X-RequestDigest":  $("#__REQUESTDIGEST").val() 
						},
					success: function(data) {
						SaveFinalOrder++;
					},
					 error: function (xhr) { 
						  SaveFinalOrder++;
					   }
				});
								},
				   error: function (xhr) { 
				   SaveFinalOrder++;
			   }
			});

			},
			onerror: function (xhr) { 
				  SaveFinalOrder++;
			  }

			 });
		},
			onerror: function (xhr) { 
				  SaveFinalOrder++;
			  }
	});	 
 }
 catch(e){
 SaveFinalOrder++;
 }
}

function GetValueFormSearchResult(currItem, paramkey) {

        for (var index = 0; index < currItem.length; index++) {
            if (currItem[index].Key == paramkey) {
                return currItem[index].Value;
            }
        }
        return "";
    }
