# ===================================================================================
# EXTERNAL FUNCTIONS
# ===================================================================================


# ===================================================================================
# FUNC: ActivateWebFeatures
# DESC: Given some xml containing features, activate those features
# ===================================================================================
function ActivateWebFeatures([String]$ConfigFileName = "")
{

	# Check that the config file exists.
	if (-not $(Test-Path -Path $configFileName -Type Leaf))
	{
		Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
	}

	$configXml = [xml]$(get-content $configFileName)
	if( $? -eq $false ) 
	{
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
	}

	$webApplication = $Configuration.Configuration.Farm.WebApplications.WebApplication | 
			Where-Object {$_.Name.value -match $configXml.Root.Configuration.Sites.WebApp}
	                  
	$Error.Clear()
	
	foreach ($Site in $configXml.Root.Configuration.Sites.Site)
        {

		if ($Site.Features -ne $NULL)
		{
        	$url = $webApplication.url.value + $Site.url
			Write-Host "Activating features at web: $url" -ForegroundColor Green
		
			foreach ($feature in $Site.Features.Feature)
			{
				
				if((Get-SPFeature -Web $url | ? { $_.id -eq $feature.id }) -eq $null )
				{
		    		Write-Host "Activating feature $($feature.Name)" -ForegroundColor Green
		    		Enable-SPFeature -Identity $feature.id -Url $url -Force

					if($Error.Count -gt 0)
					{
						Write-Host "Error: Activating feature $($feature.Name)" -ForegroundColor Red
						break
					}
					Write-Host "Success: Activating feature $($feature.Name)" -ForegroundColor Green
				}
				else
				{
					Write-Host "Warning: Feature already activated $($feature.Name)" -ForegroundColor Green
				}
		}
	        Write-Host "Completed activating features at web: $url" -ForegroundColor Green
	}
	}
}

# ===================================================================================
# FUNC: ActivatSiteFeatures
# DESC: Given some xml containing features, activate those features
# ===================================================================================
function ActivateSiteFeatures([String]$ConfigFileName = "")
{

	# Check that the config file exists.
	if (-not $(Test-Path -Path $configFileName -Type Leaf))
	{
		Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
	}

	$configXml = [xml]$(get-content $configFileName)
	if( $? -eq $false ) 
	{
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
	}
	$Error.Clear()

	$webApplication = $Configuration.Configuration.Farm.WebApplications.WebApplication | Where-Object {$_.name -match $configXml.Webapplication.name}
	if($webApplication -ne $null)
	{
		foreach ($site in $configxml.Webapplication.Sites.Site)
		{
			#$siteConfiguration = $webApplication.Sites.Site | Where-Object {$_.Title -match $site.Title}
			#$url = $siteConfiguration.Path
            $url = $site.Path
            
	    	Write-Host "Activating features at Site: $url" -ForegroundColor Green
			foreach ($feature in $site.SiteFeatures.Feature)
			{
                if($feature.isSite -eq "false")
                {
                    if((Get-SPFeature -Web $url | ? { $_.id -eq $feature.id }) -eq $null )
				    {
		    		    Write-Host "Activating feature $($feature.Name)" -ForegroundColor Green
		    		    Enable-SPFeature -Identity $feature.id -Url $url -Force

					    if($Error.Count -gt 0)
					    {
						    Write-Host "Error: Activating feature $($feature.Name)" -ForegroundColor Red
						    break
					    }
					    Write-Host "Success: Activating feature $($feature.Name)" -ForegroundColor Green
				    }
				    else
				    {
					    Write-Host "Warning: Feature already activated $($feature.Name)" -ForegroundColor Yellow

                        Write-Host "Deactivating Feature $($feature.Name)" -ForegroundColor Yellow
                        Disable-SPFeature -Identity $feature.id -Url $url -Force -Confirm: $false

                        Write-Host "Feature deactivated $($feature.Name)" -ForegroundColor Green

                        Write-Host "Activating feature $($feature.Name)" -ForegroundColor Green
		    		    Enable-SPFeature -Identity $feature.id -Url $url -Force
					    if($Error.Count -gt 0)
					    {
						    Write-Host "Error: Activating feature $($feature.Name)" -ForegroundColor Red
						    break
					    }
					    Write-Host "Success: Activating feature $($feature.Name)" -ForegroundColor Green
				    }
                }
                else
                {
				    if((Get-SPFeature -Site $url | ? { $_.id -eq $feature.id }) -eq $null )
				    {
		    		    Write-Host "Activating feature $($feature.Name)" -ForegroundColor Green
		    		    Enable-SPFeature -Identity $feature.id -Url $url -Force
					    if($Error.Count -gt 0)
					    {
						    Write-Host "Error: Activating feature $($feature.Name)" -ForegroundColor Red
						    break
					    }
					    Write-Host "Success: Activating feature $($feature.Name)" -ForegroundColor Green
				    }
				    else
				    {
					    Write-Host "Warning: Feature already activated $($feature.Name)" -ForegroundColor Yellow

                        Write-Host "Deactivating Feature $($feature.Name)" -ForegroundColor Yellow
                        Disable-SPFeature -Identity $feature.id -Url $url -Force -Confirm: $false

                        Write-Host "Feature deactivated $($feature.Name)" -ForegroundColor Green

                        Write-Host "Activating feature $($feature.Name)" -ForegroundColor Green
		    		    Enable-SPFeature -Identity $feature.id -Url $url -Force
					    if($Error.Count -gt 0)
					    {
						    Write-Host "Error: Activating feature $($feature.Name)" -ForegroundColor Red
						    break
					    }
					    Write-Host "Success: Activating feature $($feature.Name)" -ForegroundColor Green
				    }
                }
			}
		}
		Write-Host "Completed activating features at at Site: $url" -ForegroundColor Green
	}
	else
	{
		Write-Host "Error: Web Application not Found" -ForegroundColor Red
	}
}


# ===================================================================================
# FUNC: ActivateFarmFeatures
# DESC: Given some xml containing farm features, activate those features
# ===================================================================================
function ActivateFarmFeatures([xml]$inputXml)
{
    Write-Host "Activating farm features" -ForegroundColor Green
	if ($configXml.Configuration.FarmFeatures)
	{
		foreach ($feature in $configXml.Configuration.FarmFeatures.Feature)
		{
		    Write-Host "Activating feature $($feature.Name)" -ForegroundColor Green
		    Enable-SPFeature -Identity $feature.Name -force
			if($Error.Count -gt 0)
			{
				Write-Host "Error: Activating feature $($feature.Name)" -ForegroundColor Red
				break
			}
			Write-Host "Success: Activating feature $($feature.Name)" -ForegroundColor Green
		}
	}

	Write-Host "Completed activating features" -ForegroundColor Green
}

# ===================================================================================
# FUNC: UninstallFeatures
# DESC: Given some xml containing farm features, uninstalls those features
# ===================================================================================
function UninstallFeatures([xml]$inputXml)
{
    Write-Host "Deactivating farm features" -ForegroundColor Green
	if ($configXml.Configuration.ForceFeatureUninstall)
	{
		foreach ($feature in $configXml.Configuration.ForceFeatureUninstall.Feature)
		{
		    Write-Host "Deactivating feature $($feature.Name)" -ForegroundColor Green
		    # Disable-SPFeature -Identity $feature.Name -force
			Uninstall-SPFeature -Identity $feature.Name -force
		}
	}

	Write-Host "Completed deactivating features" -ForegroundColor Green
}


# ===================================================================================
# FUNC: InstallWSPSolutions
# DESC: Install WSP Package files
# ===================================================================================
function InstallWSPSolutions([String]$ConfigFileName = "")
{

	# Check that the config file exists.
	if (-not $(Test-Path -Path $configFileName -Type Leaf))
	{
		Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
	}

	$configXml = [xml]$(get-content $configFileName)
	if( $? -eq $false ) 
	{
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
	}

	Write-Host "Installing WSP solutions" -ForegroundColor Green
    
	if ($configXml.Configuration.Solutions)
	{
		foreach ($solutionConfig in $configXml.Configuration.Solutions.Solution)
		{
			if($solutionConfig.Action -eq "Install")
			{
        		$solution = Get-SPSolution -Identity $solutionConfig.name -ErrorAction SilentlyContinue 
				$Error.Clear()
				if ($solution -eq $null)
				{
            		[string] $solutionFileName = Get-Location
            		$solutionFileName = $solutionFileName + "\" + [string]$solutionConfig.path
            		$solutionFileName = $solutionFileName -replace "\\.\\", "\"
			
					$solution = Add-SPSolution -LiteralPath $solutionFileName
            	}
				else
				{
					Write-Host "Warning: Solution Already Exist..." -ForegroundColor DarkYellow
				}
				
				if (($solution.ContainsWebApplicationResource -eq $false) -and ($solution.Deployed -eq $false))
        		{
            		Write-Host "Globally Deploying Solution: $($solutionConfig.name)" -ForegroundColor Green
            		if ($solutionConfig.allowGacDeployment -eq $null)
		    		{
		        		Install-SPSolution -Identity $solutionConfig.name -Force 
		    		}
		    		else 
		    		{
		        		Install-SPSolution -Identity $solutionConfig.name -Force -GacDeployment 
		    		}
					if($Error.Count -gt 0)
					{
						Write-Host "Error: Globally Deployed Solution: $($solutionConfig.name)" -ForegroundColor Red
						Break
					}
                	WaitForJobToFinish $solutionConfig.name 
	            	Write-Host "Success: Globally Deployed Solution: $($solutionConfig.name)" -ForegroundColor Green
            	}
	        	else
    	    	{
					if ($solutionConfig.WebApplications -ne $null)
					{
						foreach ($webAppforDeployment in $solutionConfig.WebApplications.WebApplication)
						{

							$webApplication = $webAppforDeployment
							Write-Host "Deploying Solution: $($solutionConfig.name) for Web Application $($webApplication)" -ForegroundColor Green
							
							$webApp = Get-SPWebApplication -Identity $webApplication
		                	if ($solutionConfig.allowGacDeployment -eq $null)
					    	{
					        	Install-SPSolution -Identity $solutionConfig.name -Force -WebApplication $webApp
					    	}
				    		else 
				    		{
				        		Install-SPSolution -Identity $solutionConfig.name -Force -GacDeployment -WebApplication $webApp -FullTrustBinDeployment
				    		}
							if($Error.Count -gt 0)
							{
								Write-Host "Error: Deploying Solution: $($solutionConfig.name) for Web Application $($solutionConfig.webAppURL)" -ForegroundColor Red
								Break;
							}
							WaitForJobToFinish $solutionConfig.name 
			            	Write-Host "Success: Deploying Solution: $($solutionConfig.name) for Web Application $($solutionConfig.webAppURL)" -ForegroundColor Green
						}
					}
					else
					{
						#Install-SPSolution -Identity $solutionConfig.name -Force -GacDeployment -AllWebApplications
					}
    	    	}           	
			}
		}
	}

	Write-Host "Completed installing global solutions" -ForegroundColor Green
}

# ===================================================================================
# FUNC: UninstallGlobalSolutions
# DESC: Uninnstalls global solutions speecified in the config file
# ===================================================================================
function UninstallWSPSolutions([string]$configFileName)
{
	# Check that the config file exists.
	if (-not $(Test-Path -Path $configFileName -Type Leaf))
	{
		Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
	}

	$configXml = [xml]$(get-content $configFileName)
	if( $? -eq $false ) 
	{
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
	}

	Write-Host "Uninstalling WSP solutions" -ForegroundColor Green
	if ($configXml.Configuration.Solutions)
	{
		foreach ($solutionConfig in $configXml.Configuration.Solutions.Solution)
		{
			$solution = Get-SPSolution -Identity $solutionConfig.name -ErrorAction SilentlyContinue 
			$Error.Clear()
			if ($solution -ne $null)
			{
				UnInstall-SPSolution -Identity $solutionConfig.name -ErrorVariable err -ErrorAction SilentlyContinue -AllWebApplications -Confirm:$false
				WaitForJobToFinish $solutionConfig.name
				Remove-SPSolution -Identity $solutionConfig.name -force -Confirm:$false
			}
		}
	}

	Write-Host "Completed uninstalling global solutions" -ForegroundColor Green 	
}


# ===================================================================================
# FUNC: UpgradeGlobalSolutions
# DESC: UPgrade WSP Package files
# ===================================================================================
function UpgradeWSPSolutions([string]$ConfigFileName)
{
	# Check that the config file exists.
	if (-not $(Test-Path -Path $ConfigFileName -Type Leaf))
	{
		Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
	}

	$configXml = [xml]$(get-content $ConfigFileName)
	
	if( $? -eq $false ) 
	{
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
	}
	
	Write-Host "Upgrading global solutions" -ForegroundColor Green
    
	if ($configXml.Configuration.Solutions)
	{
		foreach ($solutionConfig in $configXml.Configuration.Solutions.Solution)
		{
			if($solutionConfig.Action -eq "Upgrade")
			{
	        	$solution = Get-SPSolution -Identity $solutionConfig.name -ErrorAction SilentlyContinue 
				$Error.Clear()
				if ($solution -ne $null)
				{
	            	[string] $solutionFileName = Get-Location
	            	$solutionFileName = $solutionFileName + "\" + [string]$solutionConfig.path
	            	$solutionFileName = $solutionFileName -replace "\\.\\", "\"
		          
					if ($solutionConfig.allowGacDeployment -eq $null)
	            	{
	                	Write-Host "Upgrade Solution: $($solutionConfig.name)" -ForegroundColor Green
						Update-SPSolution –Identity $solutionConfig.name –LiteralPath $solutionFileName
	                			    	
						if($Error.Count -gt 0)
						{
							Write-Host "Error: Upgrade Solution: $($solutionConfig.name)" -ForegroundColor Red
							Break
						}
		                WaitForJobToFinish $solutionConfig.name
	    	            Write-Host "Success: Upgrade Solution: $($solutionConfig.name)" -ForegroundColor Green
		            }
	    	        else
	        	    {
	            	    Write-Host "Upgrade Solution: $($solutionConfig.name)" -ForegroundColor Green
	                	Update-SPSolution –Identity $solutionConfig.name –LiteralPath $solutionFileName -Force -GacDeployment -FullTrustBinDeployment
					    
						if($Error.Count -gt 0)
						{
							Write-Host "Error: Upgrade Solution: $($solutionConfig.name)" -ForegroundColor Red
							Break;
						}

						WaitForJobToFinish $solutionConfig.name 
	                	Write-Host "Success: Upgrade Solution: $($solutionConfig.name) for Web Application $($solutionConfig.webAppURL)" -ForegroundColor Green
	            	}
					
	            }
				else
				{
					Write-Host "Warning: Solution dose not exists. Skipping Upgradation..." -ForegroundColor DarkYellow
				}
			}
		}
	}

	Write-Host "Completed Upgrade solutions" -ForegroundColor Green
}



function WaitForJobToFinish ([string]$SolutionFileName)
{ 
    $JobName = "*solution-deployment*$SolutionFileName*"
    $job = Get-SPTimerJob | ?{ $_.Name -like $JobName }
    if ($job -eq $null) 
    {
        Write-Host "Timer job not found" -ForegroundColor Green
    }
    else
    {
        $JobFullName = $job.Name
        Write-Host -NoNewLine "Waiting to finish job $JobFullName" -ForegroundColor DarkBlue
        
        while ((Get-SPTimerJob $JobFullName) -ne $null) 
        {
            Write-Host -NoNewLine . -ForegroundColor DarkBlue
            Start-Sleep -Seconds 2
        }
        Write-Host  "Finished waiting for job.." -ForegroundColor Green
    }
}

# ===================================================================================
# FUNC: AddUserSolutions
# DESC: Adds user solutions (wsps) to a given site collection
# ===================================================================================
function AddUserSolutions([System.Xml.XmlElement]$siteCollection)
{
	Write-Host "Adding user solutions" -ForegroundColor blue

	if ($siteCollection.UserSolutions)
	{
	    foreach ($userSolution in $siteCollection.UserSolutions.UserSolution)
		{
			Write-Host "Adding user solutions PATH: $($userSolution.Path), NAME: $($userSolution.Name), URL $($siteCollection.siteUrl) " -ForegroundColor blue
	 	   	Add-SPUserSolution -LiteralPath $userSolution.Path -Site $siteCollection.siteUrl -ErrorVariable err -ErrorAction SilentlyContinue 
			ProcessSPAdminJobs $err
           	Install-SPUserSolution -Identity $userSolution.Name -Site $siteCollection.siteUrl
		}
	}

	Write-Host "Completed adding user solutions" -ForegroundColor blue
}


# ===================================================================================
# FUNC: DeleteWebApps
# DESC: Deletes web apps speecified in the config file
# ===================================================================================
function DeleteWebApps([string]$ConfigFileName)
{
	# Check that the config file exists.
	if (-not $(Test-Path -Path $ConfigFileName -Type Leaf))
	{
		Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
	}

	$configXml = [xml]$(get-content $ConfigFileName)
	
	if( $? -eq $false ) 
	{
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
	}

	Write-Host "Deleting web applications" -ForegroundColor Green 
	if ($configXml.Configuration.Farm.WebApplications)
	{
  	    foreach ($webApplicationConfig in $configXml.Configuration.Farm.WebApplications.WebApplication)
	    {
			if($webApplicationConfig.Type.ToLower() -eq "new")
			{
   		    	$webAppURL = $webApplicationConfig.LoadBalancerURL
				$webApp = Get-SPWebApplication -Identity $webAppURL -ErrorAction SilentlyContinue
				$Error.Clear()
				if($webApp -ne $null)
				{
					Remove-SPWebApplication -Identity $webAppURL -DeleteIISSite -RemoveContentDatabases -Confirm:$false -ErrorAction Continue
				}
			}
		}
    }

	Write-Host "Completed deleting web applications" -ForegroundColor Green 
}

# ===================================================================================
# FUNC: SetTerms
# DESC: Function to Create First Level terms within a termset
# ===================================================================================
function SetTerms ([Microsoft.SharePoint.Taxonomy.TermSetItem] $termsetitem, [System.Xml.XmlElement]$parentnode)
{
 $parentnode.term |
 ForEach-Object {
  ## create the term
  if($termsetitem.Terms[$_.name.Replace("&", "＆")] -eq $null)
  {
  	if($_.Action -eq "New")
	{
  		$newterm = $termsetitem.CreateTerm($_.name, 1033);
		Write-Host -ForegroundColor DarkCyan "Added term " $_.name;
	}
  }
  else
  {
  	$newterm = $termsetitem.Terms[$_.name.Replace("&", "＆")]
  }
  if($_.isAvailableForTagging -eq "false")
  {
	$newterm.IsAvailableForTagging = $false;	
  }
  else
  {
	$newterm.IsAvailableForTagging = $true;	
  }
	
  if ($_.HasChildNodes -eq $true)
  {
  	SetTermsRecursive $newterm $_;
  }
  if($_.Action -eq "Rename")
  {
	$newterm.Name = $_.NewName
	Write-Host "Rename Term " $_.name " to " $_.NewName -ForegroundColor Green
  }
  
  if($_.Action -eq "Delete")
  {
  	$newterm.Delete()
	Write-Host "Delteing Term " $_.name -ForegroundColor Green
  }
  

 }
}
# ===================================================================================
# FUNC: SetTermsRecursive
# DESC: Function to Terms at nth level starting from 2nd level
# ===================================================================================
function SetTermsRecursive ([Microsoft.SharePoint.Taxonomy.Term] $termitem, [System.Xml.XmlElement]$parentnode)
{
	$parentnode.term |
 	ForEach-Object {
  		## create the term
		if($termitem.Terms[$_.name.Replace("&", "＆")] -eq $null)
		{	
			if($_.Action -eq "New")
			{
				$newterm = $termitem.CreateTerm($_.name, 1033);
				Write-Host -ForegroundColor DarkCyan "Added term " $_.name;
			}
		}
		else
		{
			$newterm = $termitem.Terms[$_.name.Replace("&", "＆")];
		}
		if($_.Action -eq "Rename")
		{
			$newterm.Name = $_.NewName
			Write-Host "Rename Term " $_.name " to " $_.NewName -ForegroundColor Green
		}
		
		if ($_.HasChildNodes -eq $true)
  		{
  			SetTermsRecursive $newterm $_;
  		}
  		if($_.isAvailableForTagging -eq "false")
  		{
			$newterm.IsAvailableForTagging = $false;	
  		}
  		else
  		{	
			$newterm.IsAvailableForTagging = $true;	
  		}
		if($_.Action -eq "Delete")
  		{
			$newterm.Delete()
			Write-Host "Delteing Term " $_.name -ForegroundColor Green
  		}

 	}
}


function UpgradeFeatures($features, [string]$url)
{    
	if ($features -ne $NULL)
	{
        foreach ($feature in $features)
		{
			Write-Host "Accessing feature $($feature.Name)" -ForegroundColor Green
			if($feature.Scope -eq "Farm")
			{
				$farmService = [Microsoft.SharePoint.Administration.SPWebService]::AdministrationService
				$featureUpgrade = $farmService.Features.Item($feature.id)
			}
			elseif($feature.Scope -eq "Web Application")
			{
				$webURL = $feature.Url + $url
				$webApplication = Get-SPWebApplication -Identity $webURL
				$featureUpgrade = $webApplication.Features.Item($feature.id)
			}
			elseif($feature.Scope -eq "Site")
			{
				$siteURL = $url + $feature.Url
				$site = Get-SpSite -Identity $siteURL
				$featureUpgrade = $site.Features.Item($feature.id)				
			}
			elseif($feature.Scope -eq "Web")
			{
				$webURL = $url + $feature.Url
				$web = Get-SPWeb -Identity $webURL
				$featureUpgrade = $web.Features.Item($feature.id)				
			}
			if($Error.Count -gt 0)
			{
				Write-Host "Error: Accessing feature..."
				Write-Host "Error: $Error"
				$Error.Clear()
				Break
			}
			
			if($featureUpgrade -ne $null )
			{
				$oldVersion = $featureUpgrade.Version
				Write-Host "Upgrading feature $($feature.Name) from version ($oldVersion)" -ForegroundColor Green
				$featureUpgrade.Upgrade($true)	
				if($Error.Count -gt 0)
				{
					Write-Host "Error: Upgrading feature $($feature.Name) from version ($oldVersion)" -ForegroundColor Green
					Write-Host "Error Message: $Error"
					$Error.Clear()
				}
				
				$newVersion = $featureUpgrade.Version
				
				Write-Host "Success: Upgrading feature $($feature.Name) from version ($oldVersion) to Version ($newVersion)" -ForegroundColor Green
			}
		}
	}
}                                                                                                                                                                        


# ===================================================================================
# FUNC: Disable Global features
# DESC: Disables global features specified in the config file
# ===================================================================================
function DisableGlobalFeatures([string]$configFileName)
{
	# Check that the config file exists.
	if (-not $(Test-Path -Path $configFileName -Type Leaf))
	{
		Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
	}

	$configXml = [xml]$(get-content $configFileName)
	if( $? -eq $false ) 
	{
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
	}

	$Error.Clear()
	
	foreach ($Site in $configXml.Webapplication.Sites.Site)
        {
		
		if ($Site.Features -ne $NULL)
		{
        	$url = $Site.url
			Write-Host "Deactivating features at web: $url" -ForegroundColor Green
		
			foreach ($feature in $Site.Features.Feature)
			{
				if($feature.isSite -eq "false")
                {
                    if((Get-SPFeature -Web $url | ? { $_.id -eq $feature.id }) -eq $null )
				    {
		    		    try
						{
							Write-Host "Deactivating feature $($feature.Name)" -ForegroundColor Yellow
							Disable-SPFeature -identity $feature.id -Url $url -Force -Confirm:$False

							Write-Host "Success: Deactivating feature $($feature.Name)" -ForegroundColor Green
							
						}
						catch
						{
							Write-Host "Exception while Deactivating feature. $($feature.Name)" $Error  -ForegroundColor Red
							$Error.Clear()  
						}
				    }
				    else
				    {
						Write-Host "Warning: Feature already deactivated $($feature.Name)" -ForegroundColor Green
					}
                }
				else
				{
					if((Get-SPFeature -Web $url | ? { $_.id -eq $feature.id }) -eq $null )
					{
						try
						{
							Write-Host "Deactivating feature $($feature.Name)" -ForegroundColor Yellow
							Disable-SPFeature -identity $feature.id -Url $url -Force -Confirm:$False

							Write-Host "Success: Deactivating feature $($feature.Name)" -ForegroundColor Green
							
						}
						catch
						{
							Write-Host "Exception while Deactivating feature. $($feature.Name)" $Error  -ForegroundColor Red
							$Error.Clear()  
						}
					}
					else
					{
						Write-Host "Warning: Feature already deactivated $($feature.Name)" -ForegroundColor Green
					}
				}
			}
	        Write-Host "Completed Deactivating features at web: $url" -ForegroundColor Green
			$Error.Clear() 
		}
	} 	
}


# =================================================================================
# FUNC: Create site collection
# DESC: Create site collection specified in the config file
# =================================================================================
function CreateSiteCollections([string]$configFileName)
{

    # Check that the config file exists.
	if (-not $(Test-Path -Path $configFileName -Type Leaf))
	{
		Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
	}

	$configXml = [xml]$(get-content $configFileName)
	if( $? -eq $false ) 
	{
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
	}

	Write-Host "Creating site collections" -ForegroundColor Green
    
	if ($configXml.Configuration.SiteCollections)
	{
		foreach ($siteCollectionConfiguration in $configXml.Configuration.SiteCollections.SiteCollection)
		{
            if(!($siteCollectionConfiguration.Title.ToString() -eq ""))
		    {
			    Write-Host "Creating Site Collection:" $siteCollectionConfiguration.Title "(" $siteCollectionConfiguration.Path ")." -ForegroundColor Green
			    $SiteCollection = New-SPSite -Url $siteCollectionConfiguration.Path -Name $siteCollectionConfiguration.Title -Description $siteCollectionConfiguration.Description -Language 1033 -Template $siteCollectionConfiguration.Template -OwnerAlias $siteCollectionConfiguration.AdminAccount
			    if($Error.Count -eq 0)
			    {
				    Write-Host "Success: Creating Site Collection:" $siteCollectionConfiguration.Title "(" $siteCollectionConfiguration.Path ")." -ForegroundColor Green
			    }
			    else
			    {
				    Write-Host "Error: Creating Site Collection:" $siteCollectionConfiguration.Title "(" $siteCollectionConfiguration.Path "). Exiting script..." -foregroundcolor Red
				    Write-Host $Error -ForegroundColor Red
                  
                    $error.clear()
			    }
			
			    $primaryOwner = ""
 			    $secondaryOwner = ""
  			    if ($siteCollectionConfiguration.CreateDefaultGroups -eq "true")
			    {
	 			    Write-Host "Removing any existing visitors group for Site collection $($siteCollectionConfiguration.Path)" -foregroundcolor blue
	 			    #This is here to fix the situation where a visitors group has already been assigned
	 			    #$SiteCollection.RootWeb.AssociatedVisitorGroup = $null;
	 			    $SiteCollection.RootWeb.Update();
				    Write-Host "Creating Owners group for Site collection $($siteCollectionConfiguration.Path)" -foregroundcolor blue
	 			    $SiteCollection.RootWeb.CreateDefaultAssociatedGroups($primaryOwner, $secondaryOwner, $siteCollectionConfiguration.Title)
	 			    $SiteCollection.RootWeb.Update();
                  
                    $error.clear()
			    }
			
			    # This section is added only to enable creation of search center subsite underneath Root site collection for AdSales
			    if(!($siteCollectionConfiguration.Title.ToString() -eq "Root"))
			    {
				    Enable-SPFeature –identity "f6924d36-2fa8-4f0b-b16d-06b7250180fa" -URL $siteCollectionConfiguration.Path
			    }
			
 		    }
        }
    }
}


# =================================================================================
# FUNC: CreateSite
# DESC: Create sites specified in the config file
# =================================================================================
function CreateSites([string]$configFileName)
{
    # Check that the config file exists.
	if (-not $(Test-Path -Path $configFileName -Type Leaf))
	{
		Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
	}

	$configXml = [xml]$(get-content $configFileName)
	if( $? -eq $false ) 
	{
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
	}

	Write-Host "Creating sites" -ForegroundColor Green
    
	if ($configXml.Configuration.Sites)
	{
		foreach ($siteConfiguration in $configXml.Configuration.Sites.Site)
		{
            if(!($siteConfiguration.Path.ToString() -eq ""))
		    {
			    Write-Host "Creating Sites :" $siteConfiguration.Title "(" $siteConfiguration.Path ")." -ForegroundColor Green
			    $Web = New-SPWeb -Url $siteConfiguration.Path -Name $siteConfiguration.Title -Description $siteConfiguration.Description -Language 1033 -Template $siteConfiguration.Template 
			    if($Error.Count -eq 0)
			    {
				    Write-Host "Success: Creating Sites :" $siteConfiguration.Title "(" $siteConfiguration.Path ")." -ForegroundColor Green
			    }
			    else
			    {
				    Write-Host "Error: Creating Sites :" $siteConfiguration.Title "(" $siteConfiguration.Path "). Exiting script..." -foregroundcolor Red
				    Write-Host $Error -ForegroundColor Red
  
                     $error.clear()
			    }
			
 		    }
        }
    }
}
