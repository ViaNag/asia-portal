﻿# =================================================================================
#
#Main Function to create list views
#
# =================================================================================
function DeleteFieldFromView([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file`n"

    $error.clear()
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
			$global:isAllDocumentsView=$false;
            foreach($VW in $cfg.Views.View)
            {
                DeleteField $VW
            }
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host $Error -ForegroundColor Red
        Write-Output $Error
	}
}

function DeleteField([object] $VW)
{
	try
		{
			$siteUrl= $VW.SiteURL
					
			#Get web object
			$web = get-spWeb $siteUrl

			#Get list and column
			$list = $web.Lists[$VW.LibraryName]
						
			$Column = $list.Fields["Title"]
			#Set column to optional and hidden
			$Column.Required = $false
			$Column.Hidden = $true

			#Update column
			$Column.Update()
			
			$view = $list.Views[$VW.ViewName]
			#remove column From view
			if($view.ViewFields.Exists($VW.Fields)) 
				{
				   Write-Host "Title Exists"
				   $view.ViewFields.Delete($VW.Fields);
				   $view.Update();
				   Write-Host "Field has deleted from View successfully"
				} 
			
			#Update  list
			$list.Update()

			#Dispose of Web object
			$web.Dispose()
		}
	catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
}
