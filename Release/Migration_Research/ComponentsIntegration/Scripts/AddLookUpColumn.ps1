if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
{    
      Add-PSSnapin Microsoft.SharePoint.PowerShell
}

function AddLookUpColumn()
{
$sourceList = $spWeb.Lists[$listName]
 
#Create a site column so that subwebs can access it.  
#This is a "Lookup" field, and its data is provided by the "parent" list.
if($spWeb.Fields.ContainsField($internalName)){
    Write-host "Deleting existing site column..." + $displayName
    $spWeb.Fields.Delete($internalName)
    $spWeb.Update();
}
$addLookupResult = $spWeb.Fields.AddLookup($internalName, $sourceList.ID, $false);
$addLookupField = $spWeb.Fields.GetFieldByInternalName($addLookupResult);
$addLookupField.Title = $displayName
$addLookupField.LookupField = $sourceList.Fields.GetFieldByInternalName($targetFieldInternalName).InternalName;
$addLookupField.Required = $true
$addLookupField.Group = $groupName
$addLookupField.Update();
Write-Host New site column added.
Write-Host
$spWeb.Dispose()
}


function LookUpColumnDetails([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)
	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
	if($Error.Count -eq 0)
	{
		$webAppFile = $cfg
		$error.clear()
		$sites = $webAppFile.Sites
		foreach($site in $Sites.Site)	
		{
		# Get the list for operation to perform
		$siteUrl= $site.SiteUrl
		$displayName = $site.DisplayName
		$internalName= $site.InternalName
		$listName = $site.ListName
		$targetFieldInternalName= $site.TargetLookupField
		$groupName = $site.GroupName

		#****Give the site url where one want to update page properties***** 

		$spWeb = get-spWeb $siteUrl

		###Calling function
		AddLookUpColumn
		}
	}
}