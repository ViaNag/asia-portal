var quickLinks = {
    clientContext: null,
    web: null,
    oListLinks: null,
    currentUser: null,
    LoginName: null,
    collListItem: null,
    UserProperties: new Array(),
    userProfile: null,
    peopleManager: null,
    UserPropertiesValue: new Array(),
    _getToKnowIntranet: null,
    _PictureUrl: null,
    percentComplete: null,
    intranetQuickLinks: null,
    webApplicationQuickLinks: null,
    quickLinksInitialized: false,
    isEditQuickLinksPage: false,
    intranetCustomQuickLinks: null,
    resultItem: [],
    quicklinkhtml1: null,
    quicklinkhtml2: null,
    searchProperties: { Title: "Title", ID: "ListItemID", LinkLoc: "LinkLocationOWSURLH", BackGrndImgLoc: "BackgroundImageLocationOWSURLH", DefaultQuickLink: "DefaultQuickLinksOWSBOOL", SiteUrl: "SPSiteURL" },
    /* Pulls the Quick Links from Enterprise list if the custom user profile property is not null*/
    getArticle: function () {
        if (localStorage.getItem("searchResults") == null) {
            quickLinks.getDefaultQuickLinksResults();
        }
        else {
            quickLinks.processArticle();
        }
    },

    processArticle: function () {
        var propertyValue = "";
        quickLinks.createInput();
        var delim = _spPageContextInfo.webServerRelativeUrl.substring(_spPageContextInfo.webServerRelativeUrl.lastIndexOf("/") + 1);
        $.each(JSON.parse(localStorage.getItem("searchResults")), function (i, val) {
            if (quickLinks.getResultItem(i, quickLinks.searchProperties.DefaultQuickLink) == "1") {
                //oListItemLinks = listItemEnumeratorLinks.get_current();
                linkID = quickLinks.getResultItem(i, quickLinks.searchProperties.ID);
                linkName = quickLinks.getResultItem(i, quickLinks.searchProperties.Title);
                linkLocation = quickLinks.getResultItem(i, quickLinks.searchProperties.LinkLoc);
                linkUrl = quickLinks.getResultItem(i, quickLinks.searchProperties.BackGrndImgLoc);
                var qucikString = '<li style="float: left;margin-right: 10px;"><a href="' + linkLocation + '"><img src="' + linkUrl + '" style="width:40px;height:40px;display:block"><span>' + linkName + '</span></a></li>';
                $('#QuickLinksContainer ul').append(qucikString);
                if (delim != "") {
                    propertyValue += linkID + "$$" + delim + '@#@';
                }
                else {
                    propertyValue += linkID + '@#@';
                }
            }
        });
        var qucikStringHtml = $('#QuickLinksContainer ul').html().trim();
        if ($(window).width() >= 1200 && qucikStringHtml) {
            quickLinks.setQuickLinkHtml(qucikStringHtml);
        }
        quickLinks.calculateQuickLinks();
        propertyValue = propertyValue.substring(0, propertyValue.length - 3);
        var propertyName = 'IntranetQuickLinks';
        quickLinks.updateUserProfile(quickLinks.LoginName, propertyName, propertyValue);
    },
    setQuickLinkHtml: function (qucikString) {
        quickLinks.quicklinkhtml1 = "";
        quickLinks.quicklinkhtml2 = "";
        var propertiesDetails = [];
        if (qucikString) {
            var propertyName = 'quicklinkhtml1';
            if (qucikString.length < 3600) {
                quickLinks.quicklinkhtml1 = qucikString;
                quickLinks.quicklinkhtml2 = "";
            }
            else if (qucikString.length > 3600 && qucikString.length < 7200) {
                quickLinks.quicklinkhtml1 = qucikString.substr(0, 3600);
                quickLinks.quicklinkhtml2 = qucikString.substr(3600);

            }
        }
        propertiesDetails.push({
            "IsMultiValued": false,
            "Name": "quicklinkhtml1",
            "Value": quickLinks.quicklinkhtml1
        });
        propertiesDetails.push({
            "IsMultiValued": false,
            "Name": "quicklinkhtml2",
            "Value": quickLinks.quicklinkhtml2
        });
        var domain, userName;

        if (quickLinks.LoginName != undefined && quickLinks.LoginName != null && quickLinks.LoginName != "") {
            // code Updated for ADFS
            if (quickLinks.LoginName.split('\\').length == 2) {
                domain = quickLinks.LoginName.split("\\")[0];
                userName = quickLinks.LoginName.split("\\")[1];
            }
            else {
                domain = "";
                userName = quickLinks.LoginName;
            }
        }
        var siteUrl = GetMySiteUrlFromLocalStarageJson();
        var profileProperties = {
            "UserName": userName,
            "Domain": domain,
            "SiteUrl": siteUrl,
            "PropertiesDetails": propertiesDetails
        };
        quickLinks.saveProfileProperties(profileProperties);
    },

    /* Post Function to update user profile custom property*/

    saveProfileProperties: function (profileProperties) {
        var result;
        $.ajax({
            url: _spPageContextInfo.siteAbsoluteUrl + "/_vti_bin/Viacom.Intranet.WCFService/IntranetService.svc/SaveProfileProperties",
            type: "POST",
            data: JSON.stringify(profileProperties),
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
               // console.log(data);
            },
            error: function (data) {
                console.log(data);
            }
        });

    },
    /*Function to update user profile custom property*/

    updateUserProfile: function (LoginName, propertyName, propertyValue) {

        propertyValue = propertyValue.replace(/\|/g, '');
        propertyValue = propertyValue.replace(/\&/g, '&amp;');

        var domain, userName;

        if (LoginName != undefined && LoginName != null && LoginName != "") {
            // code Updated for ADFS
            if (LoginName.split('\\').length == 2) {
                domain = LoginName.split("\\")[0];
                userName = LoginName.split("\\")[1];
            }
            else {
                domain = "";
                userName = LoginName;
            }
        }
        var userprofileServiceURL = GetUserProfileUrlFromLocalStarageJson();
        var siteUrl = GetMySiteUrlFromLocalStarageJson();
        $.support.cors = true;
        var jsonDataType = "json";

        $.ajax({
            url: userprofileServiceURL,
            dataType: jsonDataType,
            type: "GET",
            data: { domain: domain, userName: userName, propertyName: propertyName, value: propertyValue, siteUrl: siteUrl },
            success: function (data) {
                //Grab our data from Ground Control
                if (console != undefined && console != null) {
                    //console.log(data);
                }
            },
            error: function (data) {

                //If any errors occurred - detail them here
                if (console != undefined && console != null) {
                    //console.log(data);
                }
            }
        });

    },
    /* Get quick links will fetch the Quick links if the custom user profile property is not null*/

    getQuickLinks: function () {
        quickLinks.createInput();
        // clear existing quicklinks HTML so that it will create again when window has resized 
        $('#QuickLinksContainer ul').html('');
        $('#QuickLinksContainer ul').html('');
        if ($(window).width() >= 1200 && (quickLinks.quicklinkhtml1 || quickLinks.quicklinkhtml2)) {

            var quicklinkhtml = "";
            if (quickLinks.quicklinkhtml1) {
                quicklinkhtml = quickLinks.quicklinkhtml1;
            }
            if (quickLinks.quicklinkhtml2) {
                quicklinkhtml += quickLinks.quicklinkhtml2;
            }
            $('#QuickLinksContainer ul').append(quicklinkhtml);
            quickLinks.calculateQuickLinks();
        }
        else {
            quickLinks.clientContext = new SP.ClientContext.get_current();
            if (localStorage.getItem("searchResults") == null) {
                quickLinks.getQuickLinksResults();
            }
            else {
                quickLinks.processQuickLinks();
            }
        }

    },

    processQuickLinks: function () {
        $('#QuickLinksContainer ul').html("");
        var temp = quickLinks.webApplicationQuickLinks.replace(/\|/g, '');
        var selectedLinks = temp.split('@#@');
        // Remove the empty entry from array
        if (selectedLinks[selectedLinks.length - 1] == null || selectedLinks[selectedLinks.length - 1] == '') {
            selectedLinks.pop();
        }
        var imgUrl = "";
        if (JSON.parse(localStorage.getItem("searchResults")) != null && JSON.parse(localStorage.getItem("searchResults")) != undefined) {

            var dataSource = JSON.parse(localStorage.getItem("searchResults"));
            var resultsCount = dataSource.length;

            for (var i = 0; i < resultsCount; i++) {

                if (quickLinks.getResultItem(i, quickLinks.searchProperties.Title) === 'DefaultQuickLinksImagePlaceholder') {
                    imgUrl = quickLinks.getResultItem(i, quickLinks.searchProperties.BackGrndImgLoc);
                    break;
                }
            }

            for (var i = 0; i < selectedLinks.length; i++) {

                var selectedId = selectedLinks[i].split("$$")[0];

                if (!isNaN(selectedId)) {

                    for (var j = 0; j < resultsCount; j++) {
                        if (selectedLinks[i].split("$$").length == 1) {
                            var crntSiteUrl = _spPageContextInfo.siteAbsoluteUrl.indexOf("/sites") > -1 ? _spPageContextInfo.siteAbsoluteUrl.substring(0, _spPageContextInfo.siteAbsoluteUrl.indexOf("/sites")).toUpperCase() : _spPageContextInfo.siteAbsoluteUrl.toUpperCase();
                            if (quickLinks.getResultItem(j, quickLinks.searchProperties.ID).toString() == selectedLinks[i] && quickLinks.getResultItem(j, quickLinks.searchProperties.SiteUrl).toUpperCase() == crntSiteUrl) {
                                var linkName = quickLinks.getResultItem(j, quickLinks.searchProperties.Title);
                                var linkUrl = quickLinks.getResultItem(j, quickLinks.searchProperties.BackGrndImgLoc) != null ? quickLinks.getResultItem(j, quickLinks.searchProperties.BackGrndImgLoc) : "";
                                var linkLocation = quickLinks.getResultItem(j, quickLinks.searchProperties.LinkLoc) != null ? quickLinks.getResultItem(j, quickLinks.searchProperties.LinkLoc) : "";
                                var qucikString = '<li style="float: left;margin-right: 10px;"><a href="' + linkLocation + '"><img src="' + linkUrl + '" style="width:40px;height:40px;display:block"><span>' + linkName + '</span></a></li>';
                                $('#QuickLinksContainer ul').append(qucikString);
                                break;
                            }
                        }
                        else {
                            if (quickLinks.getResultItem(j, quickLinks.searchProperties.ID).toString() === selectedId && quickLinks.getResultItem(j, quickLinks.searchProperties.SiteUrl).indexOf(selectedLinks[i].split("$$")[1]) > -1) {
                                var linkName = quickLinks.getResultItem(j, quickLinks.searchProperties.Title);
                                var linkUrl = quickLinks.getResultItem(j, quickLinks.searchProperties.BackGrndImgLoc) != null ? quickLinks.getResultItem(j, quickLinks.searchProperties.BackGrndImgLoc) : "";
                                var linkLocation = quickLinks.getResultItem(j, quickLinks.searchProperties.LinkLoc) != null ? quickLinks.getResultItem(j, quickLinks.searchProperties.LinkLoc) : "";
                                var qucikString = '<li style="float: left;margin-right: 10px;"><a href="' + linkLocation + '"><img src="' + linkUrl + '" style="width:40px;height:40px;display:block"><span>' + linkName + '</span></a></li>';
                                $('#QuickLinksContainer ul').append(qucikString);
                                break;
                            }
                        }
                    }

                }
                else if (selectedId.indexOf(_spPageContextInfo.siteAbsoluteUrl) > -1) {
                    var LinkImgUrl = "";
                    var title = selectedLinks[i].split(',');
                    var linkLocation = title[1];
                    var nestedSite = title[1].split(_spPageContextInfo.siteAbsoluteUrl);
                    var sites = nestedSite[1].split('/');
                    for (var k = 0; k < sites.length; k++) {
                        if (sites[k] != "") {
                            if (LinkImgUrl == "") {
                                for (var j = 0; j < resultsCount; j++) {
                                    if (quickLinks.getResultItem(j, quickLinks.searchProperties.Title) === sites[k]) {
                                        LinkImgUrl = quickLinks.getResultItem(j, quickLinks.searchProperties.BackGrndImgLoc) != null ? quickLinks.getResultItem(j, quickLinks.searchProperties.BackGrndImgLoc) : "";
                                        break;
                                    }

                                }

                            }
                        }
                    }
                    if (LinkImgUrl == "")
                        LinkImgUrl = imgUrl;
                    var linkName = title[0];
                    var qucikString = '<li style="float: left;margin-right: 10px;"><a href="' + linkLocation + '"><img src="' + LinkImgUrl + '" style="width:40px;height:40px;display:block"><span>' + linkName + '</span></a></li>';
                    $('#QuickLinksContainer ul').append(qucikString);
                }
                else {
                    var LinkImgUrl = "";
                    var title = selectedLinks[i].split(',');
                    var linkLocation = title[1];
                    LinkImgUrl = imgUrl;
                    var linkName = title[0];
                    var qucikString = '<li style="float: left;margin-right: 10px;"><a href="' + linkLocation + '"><img src="' + LinkImgUrl + '" style="width:40px;height:40px;display:block"><span>' + linkName + '</span></a></li>';
                    $('#QuickLinksContainer ul').append(qucikString);
                }
            }
            /* Calculate quick links and hide extra items */
            quickLinks.calculateQuickLinks();
            var qucikStringHtml = $('#QuickLinksContainer ul').html().trim();
            if ($(window).width() >= 1200 && qucikStringHtml) {
                quickLinks.setQuickLinkHtml(qucikStringHtml);
            }
        }

    },
    getQuickLinksResults: function () {
        $.ajax({

            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/search/query?querytext='ContentType:CTQuickLinks'&selectproperties='" + quickLinks.searchProperties.Title + "%2c" + quickLinks.searchProperties.ID + "%2c" + quickLinks.searchProperties.LinkLoc + "%2c" + quickLinks.searchProperties.BackGrndImgLoc + "%2c" + quickLinks.searchProperties.SiteUrl + "%2c"
			+ quickLinks.searchProperties.DefaultQuickLink + "'&rowlimit=500&clienttype='ContentSearchRegular'",
            type: "GET",
            headers: {
                "Accept": "application/json;odata=verbose"
            },
            async: true,
            success: function (data, textStatus, xhr) {
                var query = data.d.query;
                quickLinks.insertResultItem(query.PrimaryQueryResult.RelevantResults.Table.Rows.results);
                quickLinks.getListQuickLinksResults();
            },
            error: function (data) {
                quickLinks.getListQuickLinksResults();
                //If any errors occurred - detail them here
                if (console != undefined && console != null) {

                }
            }
        });
    },
    getListQuickLinksResults: function () {
        $.ajax({
            url: _spPageContextInfo.siteAbsoluteUrl.indexOf("/sites") > -1 ? _spPageContextInfo.siteAbsoluteUrl.substring(0, _spPageContextInfo.siteAbsoluteUrl.indexOf("/sites")) + "/_api/web/lists/getbytitle('QuickLinks')/items?$top=5000" : _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('QuickLinks')/items?$top=5000",
            type: "GET",
            headers: {
                "Accept": "application/json;odata=verbose"
            },
            async: true,
            success: function (data, textStatus, xhr) {
                var resultItemLength = quickLinks.resultItem.length;
                for (var i = 0; i < data.d.results.length; i++) {
                    var resultItemIndex = resultItemLength + i;
                    quickLinks.resultItem[resultItemIndex] = {};
                    quickLinks.resultItem[resultItemIndex]["Item"] = [];
                    quickLinks.resultItem[resultItemIndex]["Item"].push({ Key: quickLinks.searchProperties.ID, Value: data.d.results[i].ID });
                    quickLinks.resultItem[resultItemIndex]["Item"].push({ Key: quickLinks.searchProperties.Title, Value: data.d.results[i].Title });
                    quickLinks.resultItem[resultItemIndex]["Item"].push({ Key: quickLinks.searchProperties.LinkLoc, Value: data.d.results[i].LinkLocation != null ? data.d.results[i].LinkLocation.Url : "" });
                    quickLinks.resultItem[resultItemIndex]["Item"].push({ Key: quickLinks.searchProperties.BackGrndImgLoc, Value: data.d.results[i].BackgroundImageLocation != null ? data.d.results[i].BackgroundImageLocation.Url : "" });
                    quickLinks.resultItem[resultItemIndex]["Item"].push({ Key: quickLinks.searchProperties.SiteUrl, Value: _spPageContextInfo.siteAbsoluteUrl.indexOf("/sites") > -1 ? _spPageContextInfo.siteAbsoluteUrl.substring(0, _spPageContextInfo.siteAbsoluteUrl.indexOf("/sites")) : _spPageContextInfo.siteAbsoluteUrl });
                    if (data.d.results[i].DefaultQuickLinks == true || data.d.results[i].DefaultQuickLinks == false) {
                        quickLinks.resultItem[resultItemIndex]["Item"].push({ Key: quickLinks.searchProperties.DefaultQuickLink, Value: data.d.results[i].DefaultQuickLinks ? "1" : "0" });
                    }
                    else {
                        quickLinks.resultItem[resultItemIndex]["Item"].push({ Key: quickLinks.searchProperties.DefaultQuickLink, Value: data.d.results[i].DefaultQuickLinks });
                    }
                }

                // VC: Remove this from the for loop
                localStorage.setItem("searchResults", JSON.stringify(quickLinks.resultItem));

                quickLinks.processQuickLinks();
            },
            error: function (data) {
                quickLinks.processQuickLinks();
                //If any errors occurred - detail them here
                if (console != undefined && console != null) {

                }
            }
        });
    },
    getDefaultQuickLinksResults: function () {
        $.ajax({
            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('QuickLinks')/items?$top=5000",
            type: "GET",
            headers: {
                "Accept": "application/json;odata=verbose"
            },
            success: function (data, textStatus, xhr) {
                for (var i = 0; i < data.d.results.length; i++) {
                    quickLinks.resultItem[i] = {};
                    quickLinks.resultItem[i]["Item"] = [];
                    quickLinks.resultItem[i]["Item"].push({ Key: quickLinks.searchProperties.ID, Value: data.d.results[i].ID });
                    quickLinks.resultItem[i]["Item"].push({ Key: quickLinks.searchProperties.Title, Value: data.d.results[i].Title });
                    quickLinks.resultItem[i]["Item"].push({ Key: quickLinks.searchProperties.LinkLoc, Value: data.d.results[i].LinkLocation != null ? data.d.results[i].LinkLocation.Url : "" });
                    quickLinks.resultItem[i]["Item"].push({ Key: quickLinks.searchProperties.BackGrndImgLoc, Value: data.d.results[i].BackgroundImageLocation != null ? data.d.results[i].BackgroundImageLocation.Url : "" });
                    quickLinks.resultItem[i]["Item"].push({ Key: quickLinks.searchProperties.SiteUrl, Value: _spPageContextInfo.siteAbsoluteUrl });
                    if (data.d.results[i].DefaultQuickLinks == true || data.d.results[i].DefaultQuickLinks == false) {
                        quickLinks.resultItem[i]["Item"].push({ Key: quickLinks.searchProperties.DefaultQuickLink, Value: data.d.results[i].DefaultQuickLinks ? "1" : "0" });
                    }
                    else {
                        quickLinks.resultItem[i]["Item"].push({ Key: quickLinks.searchProperties.DefaultQuickLink, Value: data.d.results[i].DefaultQuickLinks });
                    }
                    localStorage.setItem("searchResults", JSON.stringify(quickLinks.resultItem));

                }
                quickLinks.processArticle();
            },
            error: function (data) {
                quickLinks.processArticle();
                //If any errors occurred - detail them here
                if (console != undefined && console != null) {
                    //console.log(data);
                }
            }
        });
    },

    insertResultItem: function (data) {
        $.each(data, function (i, val) {
            quickLinks.resultItem[i] = {};
            quickLinks.resultItem[i]["Item"] = [];
            $.each(this.Cells.results, function () {
                if (this.Key == quickLinks.searchProperties.BackGrndImgLoc || this.Key == quickLinks.searchProperties.LinkLoc) {
                    if (this.Value != null) {
                        if (this.Value.indexOf(",") > -1) {
                            quickLinks.resultItem[i]["Item"].push({ Key: this.Key, Value: this.Value.split(",")[0] });
                        }
                        else {
                            quickLinks.resultItem[i]["Item"].push({ Key: this.Key, Value: this.Value });
                        }
                    }
                    else {
                        quickLinks.resultItem[i]["Item"].push({ Key: this.Key, Value: this.Value });
                    }
                }
                else {

                    quickLinks.resultItem[i]["Item"].push({ Key: this.Key, Value: this.Value });
                }

            });
            localStorage.setItem("searchResults", JSON.stringify(quickLinks.resultItem))
        });
    },
    getResultItem: function (index, key) {
        var result = JSON.parse(localStorage.getItem("searchResults"));
        var item = '';
        var item2 = '';
        // VC: change to grep

        item2 = $.grep(result[index]["Item"], function (e) { return e.Key == key; });

        if (item2.length == 1) {
            item = item2[0].Value;
        }

        return item;
    },
    createInput: function () {
        /* Added edit links button in a before the recommended links search web part to display it properly*/
        var btnId = document.getElementById('btnEdit');
        if (btnId == null) {
            $('<input id="btnEdit" type="button" value="Edit" onclick="quickLinks.editLinks();">').appendTo("#QuickLinksContainer h3");
        }


    },
    editLinks: function () {
        location.href = _spPageContextInfo.siteAbsoluteUrl + '/Pages/EditQuickLinks.aspx';

    },
    failure: function (sender, args) {

    },
    GetProperties: function () {
        quickLinks.clientContext = new SP.ClientContext.get_current();
        quickLinks.web = quickLinks.clientContext.get_web();
        quickLinks.currentUser = quickLinks.web.get_currentUser();
        var oList = quickLinks.clientContext.get_site().get_rootWeb().get_lists().getByTitle('Config');
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml('<View><Query><Where><Eq><FieldRef Name=\'Title\'/><Value Type=\'Text\'>Test</Value></Eq></Where></Query><ViewFields><FieldRef Name=\'FetchProfileProperties\'/><FieldRef Name=\'Title\'/></ViewFields></View>');
        quickLinks.collListItem = oList.getItems(camlQuery);
        quickLinks.clientContext.load(quickLinks.collListItem);
        quickLinks.clientContext.load(quickLinks.currentUser);
        quickLinks.clientContext.executeQueryAsync(Function.createDelegate(this, quickLinks.onProfileSucceeded), Function.createDelegate(this, quickLinks.onProfileFailed));
    },

    onProfileFailed: function (sender, args) {
        //console.log(args.get_message() + '\n' + args.get_stackTrace());
    },

    onProfileSucceeded: function (sender, args) {
        var listItemInfo = '';
        var listItemEnumerator = quickLinks.collListItem.getEnumerator();
        while (listItemEnumerator.moveNext()) {
            var oListItem = listItemEnumerator.get_current();
            listItemInfo = oListItem.get_item('FetchProfileProperties');
        }
        quickLinks.userProfile = listItemInfo;
        quickLinks.getUserPropertiesFromProfile();
    },

    getUserPropertiesFromProfile: function () {
        var userProfileInfo = quickLinks.userProfile.split(',');
        var peopleManager = new SP.UserProfiles.PeopleManager(quickLinks.clientContext);
        quickLinks.LoginName = quickLinks.currentUser.get_loginName();
        //Code update for ADFS
        if (quickLinks.LoginName.split('|').length == 2) {
            quickLinks.LoginName = quickLinks.LoginName.split('|')[1];
        }
        var userProfilePropertiesForUser = new SP.UserProfiles.UserProfilePropertiesForUser(quickLinks.clientContext, quickLinks.LoginName, userProfileInfo);
        quickLinks.userProfileProperties = peopleManager.getUserProfilePropertiesFor(userProfilePropertiesForUser);
        quickLinks.clientContext.load(userProfilePropertiesForUser);
        quickLinks.clientContext.executeQueryAsync(quickLinks.onSuccess, quickLinks.onProfileRequestFail);
    },

    onSuccess: function () {
        var _userProfilePictureURL = "";/* Changes for Defect ID 371 */
        var _Name = quickLinks.userProfileProperties[0] + " " + quickLinks.userProfileProperties[1];
        var _firstName = quickLinks.userProfileProperties[0];
        quickLinks._PictureUrl = quickLinks.userProfileProperties[3];
        _userProfilePictureURL = quickLinks._PictureUrl;/* Changes for Defect ID 371 */
        var _Department = quickLinks.userProfileProperties[8];
        quickLinks._getToKnowIntranet = quickLinks.userProfileProperties[9];
        quickLinks.webApplicationQuickLinks = quickLinks.userProfileProperties[10];
        quickLinks.quicklinkhtml1 = quickLinks.userProfileProperties[15];
        quickLinks.quicklinkhtml2 = quickLinks.userProfileProperties[16];
        quickLinks.intranetQuickLinks = quickLinks.webApplicationQuickLinks;
        var link = ""
        if (quickLinks.webApplicationQuickLinks != "" || quickLinks.webApplicationQuickLinks != null) {
            var delim = _spPageContextInfo.webServerRelativeUrl.substring(_spPageContextInfo.webServerRelativeUrl.lastIndexOf("/") + 1);
            if (delim != "") {
                $.each(quickLinks.webApplicationQuickLinks.split('@#@'), function (i, val) {
                    if (val != null && val != '') {
                        if (val.indexOf(delim) != -1) {
                            link += val.split("$$")[0] + "@#@";
                        }
                        else {
                            link += val + "@#@";
                        }
                    }
                });
                quickLinks.intranetQuickLinks = link;
            }
        }

        var _employeeType = quickLinks.userProfileProperties[13];
        var _newHire = quickLinks.userProfileProperties[12];
        var _topPages = quickLinks.userProfileProperties[11];
        quickLinks.intranetCustomQuickLinks = quickLinks.userProfileProperties[14];
        if (_topPages != undefined && _topPages != null && _topPages != "undefined") {
            _topPages = _topPages.replace(/\|/g, '');
            _topPages = _topPages.split('@#@');
            $('#number-of-pages').text("All " + (_topPages.length - 1) + " Pages >> ");
            var count = 0;
            if ((_topPages.length - 1) > 10)
                count = 10;
            else
                count = _topPages.length - 1;
            for (var k = 0; k < count; k++) {
                var topPagesTitle = _topPages[k].split(',');
                var divHtml = '<li><table><tr><td id="greenroom-panel-top-pages-left"><a href="' + topPagesTitle[1] + '" title="" target="_blank"><span class="glyphicon glyphicon-list-alt"></span></a></td> <td id="greenroom-panel-top-pages-right"><a href="' + topPagesTitle[1] + '" title="" target="_blank">' + topPagesTitle[0] + '</a></td></tr></table></li>';
                $('#greenroom-panel-top-pages-followed').append(divHtml);

            }
            if (count == 0) {
                var divHtml = '<div class="no-followed-site">If no links appear here, please click on  link "All 0 Pages >>".<br/><br/></div>';
                $('#greenroom-panel-top-pages-followed').html(divHtml);
            }
        }
        if (_employeeType != undefined && _employeeType != null) {
            if (_employeeType.indexOf("RF") > -1) /* RF indicates staff, if employeeType contains RF/staff then show employee links */ {
                $('.greenroom-panel-link-employee').show();
            }
            if (_employeeType.indexOf("RF") == -1) /* if employeeType does not contain RF show contractor links */ {
                $('#greenroom-panel-contractor-container').show();
                $('.greenroom-panel-link-contractor').show();
            }
        }
        if (quickLinks._PictureUrl == null || quickLinks._PictureUrl == "") {
            quickLinks._PictureUrl = "_layouts/15/images/Person.gif";
            _userProfilePictureURL = _spPageContextInfo.siteAbsoluteUrl + "/SiteCollectionImages/icon_welcome.png";	/* Changes for Defect ID 371 */
        }
        if (_Department == undefined && _Department == null || _Department == "") {
            _Department = "N\A";
        }
        /*Adding condition to check if this is edit quick links page*/
        var strpage = location.pathname.toLowerCase().split('/pages/')[1];
        if (strpage != null && strpage != undefined) {
            if (strpage.toLowerCase() == "editquicklinks.aspx") {
                quickLinks.isEditQuickLinksPage = true;
            }
        }
        if (!(quickLinks.webApplicationQuickLinks) && !(quickLinks.intranetCustomQuickLinks)) {
            quickLinks.getArticle();
            if (quickLinks.isEditQuickLinksPage) {
                editLinks.getQuickLinks();
            }
        }
        if ((quickLinks.webApplicationQuickLinks) || (quickLinks.intranetCustomQuickLinks)) {
            if (quickLinks.webApplicationQuickLinks) {
                quickLinks.getQuickLinks();
            }
            if (quickLinks.intranetCustomQuickLinks) {
                quickLinks.createInput();
                quickLinks.getCustomQuickLinks();
            }

            if (quickLinks.isEditQuickLinksPage) {
                if (quickLinks.intranetCustomQuickLinks) {
                    if (quickLinks.intranetCustomQuickLinks.length <= 0) {
                        $("#CustomQuickLinksContainer").hide();
                    }
                }
                else {
                    $("#CustomQuickLinksContainer").hide();
                }
                if (quickLinks.webApplicationQuickLinks.length <= 0) {
                    $("#EditQuickLinksContainer").hide();
                }
                editLinks.getQuickLinks();
                if (quickLinks.intranetCustomQuickLinks) {
                    editLinks.getCustomQuickLinks();
                }
            }
        }

        /************************** Hide Customized links section from Quick links top nav section if there are no custom links **********************/
        if (quickLinks.intranetCustomQuickLinks != null && quickLinks.intranetCustomQuickLinks != "" && quickLinks.intranetCustomQuickLinks != undefined) {
            if (quickLinks.intranetCustomQuickLinks.length <= 0) {
                $("#customized-intranet-quickLinks").hide();
            }
            else {
                $("#customized-intranet-quickLinks").show();
            }
            if (quickLinks.intranetQuickLinks.length <= 0) {
                $("#QuickLinksContainer").hide();
            }
            else {
                $("#QuickLinksContainer").show();
            }
        }
        else {
            $("#customized-intranet-quickLinks").hide();
        }

        /* Changes for Defect ID 371 */
        var welcomeiconId = document.getElementById('WelcomeIcon');
        if (welcomeiconId == null) {
            $('#DeltaSuiteBarRight').prepend("<img id='WelcomeIcon' src='" + _userProfilePictureURL + "' />");
            $("#greenroom-panel-default-image").append("<img class='circular'src='" + quickLinks._PictureUrl + "' alt='" + _Name + "'/>");
            $("#greenroom-panel-default-name").append(_Name);
            $(".greenroom-panel-default-first-name").append(_firstName);
            $("#greenroom-panel-default-dept").append(_Department);
        }
        userProfilePanel.GetQuestions();
    },
    onProfileRequestFail: function (sender, args) {

    },

    /* Calculate quick links and hide extra items */
    calculateQuickLinks: function () {
        $('#QuickLinks').hide();
        $("#QuickLinksPlaceholder").css("display", "block");
    },
    getCustomQuickLinks: function () {
        var intranetCustomQuickLinks = quickLinks.intranetCustomQuickLinks;
        var customIntranetQuickLinksHtml = "";
        intranetCustomQuickLinks = intranetCustomQuickLinks.replace(/\|/g, '');
        intranetCustomQuickLinks = intranetCustomQuickLinks.split("@#@");
        for (var iCount = 0; iCount < intranetCustomQuickLinks.length; iCount++) {
            if (intranetCustomQuickLinks[iCount] != "" && intranetCustomQuickLinks[iCount] != undefined && intranetCustomQuickLinks[iCount] != null) {
                var linkDescription = intranetCustomQuickLinks[iCount].split(",")[0];
                var linkUrl = intranetCustomQuickLinks[iCount].split(",")[1];
                customIntranetQuickLinksHtml = '<li style="float: left;margin-right: 10px;"><a href="' + linkUrl + '"><img src="/../../SiteCollectionImages/QLs/greenroom-default-ql-icon.png" style="width:40px;height:40px;display:block"><span>' + linkDescription + '</span></a></li>';
                $('#customized-intranet-quickLinks ul').append(customIntranetQuickLinksHtml);
            }
        }
    }
};
$(document).ready(function () {
    // localStorage.clear();

    width = $(window).width();
    if (quickLinks.quickLinksInitialized) return;
    else {
        /* Start code to fill newsfeed textarea from the share to newsfeed functionality */
        var getArticleUrl = window.location.href;
        var formatArticleUrl = getArticleUrl.split('ArticleUrl=');
        if (formatArticleUrl.length > 1) {
            var articleUrl = formatArticleUrl[1];
            if (articleUrl != undefined && articleUrl != null && articleUrl != '') {
                var ArticleLinkUrl = articleUrl.split('&')[0];
                $('#ms-microbloginputbox').val(ArticleLinkUrl);
            }
        }
        /* End code to fill newsfeed textarea from the share to newsfeed functionality */
        quickLinks.quickLinksInitialized = true;
        $('#RecommendedlinksSection').hide();
        if (getArticleUrl.toLowerCase().indexOf("newsfeed.aspx") < 0) {
            ExecuteOrDelayUntilScriptLoaded(function () { ExecuteOrDelayUntilScriptLoaded(quickLinks.GetProperties, "SP.UserProfiles.js"); }, "sp.js");
        }
        else {
            quickLinks.GetProperties();
        }
    }
});
