﻿[string] $currentLocation = Get-Location
$opDefFileName = "Operations.xml"
$configurationFileName = "Configuration.xml"
$configurationGlobalFileName = "Global.xml"
$tokensFileName = "Tokens.xml"

# =================================================================================
#
# FUNC:	Get-ScriptLogFileName
# DESC:	Gets the log file nme based on invocation script name
#		Suffice the script with execution date and returns filename string 
#		in format <<Log File Name>>_<<ddMMMyyyyHHmmss>>.log
#
# =================================================================================
function Get-ScriptLogFileName([string]$loggingAction)
{
		$currentDate = Get-Date -Format "ddMMMyyyyHHmmss"
		$logFileName = $loggingAction + $currentDate  + ".log"

		Return $logFileName
}

##Logging
##Display provided message
function global:Logging($msg, $textcolor="white")
{
    Write-Host $msg -ForegroundColor $textcolor
	Write-Output $msg
}

function script:ErrorExit()
{
    exit 1
}

#$args = {WAT}

#check the parameters
if($args.Count -ne 1 -and $args.Count -ne 2)
{ 
    Write-Host "Usage: .\Deploy.ps1 <<Folder name of webapplication in configuration folder>>" -ForegroundColor Red
    ErrorExit
}

if($args.Count -gt 1)
{
	$opDefFileName = $args[1]
}

$operationFolder=$args[0]

$opDefFileNamePath = $currentLocation + "\Configuration\" + $operationFolder + "\" + $opDefFileName
$configurationFileNameWithPath = $currentLocation + "\Configuration\" + $operationFolder + "\" + $configurationFileName
$configurationGlobalFileNameWithPath = $currentLocation + "\Configuration\" + $operationFolder + "\" + $configurationGlobalFileName

#Replace any tokens found in config folder. If no tokens file is found, then skip the operation as its not mandatory
$tokensFileNameWithPath = $currentLocation + "\Configuration\" + $operationFolder + "\" + $tokensFileName
if (-not $(Test-Path -Path $tokensFileNameWithPath -Type Leaf))
{
	Write-Host "Tokens file '$($tokensFileName)' does not exist at $($operationFolder) folder. So reading file from Global Configuration Folder" -ForegroundColor Yellow
	$tokensFileNameWithPath = $currentLocation + "\GlobalConfigurations\" + $tokensFileName
	if (-not $(Test-Path -Path $tokensFileNameWithPath -Type Leaf))
	{
		Write-Host "No tokens file found. Skipping the operation to replace tokens" -ForegroundColor Yellow
	}
	else
	{
		$tokens = [xml]$(get-content $tokensFileNameWithPath)
		$currentConfigsPath=$currentLocation + "\Configuration\" + $operationFolder
		$configFiles = Get-ChildItem $currentConfigsPath
		foreach ($file in $configFiles)
		{
			Write-Host "Replacing tokens in file $($file.Name)"
			$contents = Get-Content $file.PSPath
			
			foreach($token in $tokens.Tokens.Token)
			{
				$contents = $contents -replace $token.Name, $token.Value
			}
			
			#Write Contents
			Set-Content -Path $file.PSPath -Force $contents
		}
	}
}


if (-not $(Test-Path -Path $opDefFileNamePath -Type Leaf))
{
	Write-Host "Operations definition file '$($opDefFileName)' does not exist at $($operationFolder) folder. So reading file from COnfiguration Folder" -ForegroundColor Yellow
	$opDefFileNamePath = $currentLocation + "\Configuration\" + $opDefFileName
	if (-not $(Test-Path -Path $opDefFileNamePath -Type Leaf))
	{
		Write-Host "Operations definition file '$($opDefFileName)' does not exist at COnfiguration folder. So checking file from GlobalCOnfiguration Folder" -ForegroundColor Yellow
		$opDefFileNamePath = $currentLocation + "\GlobalConfigurations\" + $opDefFileName
		if (-not $(Test-Path -Path $opDefFileNamePath -Type Leaf))
		{
			Write-Error "Operations definition file $opDefFileName does not exist at GlobalConfigurations folder." 
			Exit        
		}
	}
}

$Operations = [xml]$(get-content $opDefFileNamePath)

if( $? -eq $false ) 
{
	Write-Host "Could not read Operation file. Exiting ..." -ForegroundColor Red`
	Exit
}

if (-not $(Test-Path -Path $configurationFileNameWithPath -Type Leaf))
{
	Write-Host "Configuration definition file '$($configurationFileName)' does not exist at $($operationFolder) folder. So reading file from Configuration Folder" -ForegroundColor Yellow
	$configurationFileNameWithPath = $currentLocation + "\Configuration\" + $configurationFileName
	if (-not $(Test-Path -Path $configurationFileNameWithPath -Type Leaf))
	{
		Write-Host "Configuration definition file '$($configurationFileName)' does not exist at Configuration folder. So reading file from GlobalCOnfiguration Folder" -ForegroundColor Yellow
		$configurationFileNameWithPath = $currentLocation + "\GlobalConfigurations\" + $configurationFileName
		if (-not $(Test-Path -Path $configurationFileNameWithPath -Type Leaf))
		{
			Write-Error "Configuration definition file $($configurationFileName) does not exist at GlobalConfigurations folder." 
			Exit        
		}
	}
}

$Configuration = [xml]$(get-content $configurationFileNameWithPath)

if( $? -eq $false ) 
{
	Write-Host "Could not read configuration definition file. Exiting ..." -ForegroundColor Red`
	Exit
}

if (-not $(Test-Path -Path $configurationGlobalFileNameWithPath -Type Leaf))
{
	Write-Host "Global definition file '$($configurationGlobalFileName)' does not exist at $($operationFolder) folder. So reading file from Configuration Folder" -ForegroundColor Yellow
	$configurationGlobalFileNameWithPath = $currentLocation + "\Configuration\" + $configurationGlobalFileName
	if (-not $(Test-Path -Path $configurationGlobalFileNameWithPath -Type Leaf))
	{
		Write-Host "Global definition file '$($configurationGlobalFileName)' does not exist at Configuration folder. So reading file from GlobalCOnfiguration Folder" -ForegroundColor Yellow
		$configurationGlobalFileNameWithPath = $currentLocation + "\GlobalConfigurations\" + $configurationGlobalFileName
		if (-not $(Test-Path -Path $configurationGlobalFileNameWithPath -Type Leaf))
		{
			Write-Error "Global definition file $($configurationGlobalFileName) does not exist at GlobalConfigurations folder." 
			Exit        
		}
	}
}

$GlobalConfiguration = [xml]$(get-content $configurationGlobalFileNameWithPath)

if( $? -eq $false ) 
{
	Write-Host "Could not read global configuration file. Exiting ..." -ForegroundColor Red`
	Exit
}

$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'}
if ($snapin -eq $null) 
{
    Write-Host "Loading SharePoint Powershell Snapin" -ForegroundColor Green
    Add-PSSnapin "Microsoft.SharePoint.Powershell"
}

$Global = $GlobalConfiguration.GlobalConfiguration
$configPath=$currentLocation + "\configuration\"

#Looping though all directory present in configuration folder except folder for opration file and excute the operation accordingly

        # Check if the log subfolder exist if not create it 
        $destDir=$currentLocation + "\Logs\" +$operationFolder
        If (!(Test-Path $destDir)) {
           New-Item -Path $destDir -ItemType Directory
        }
        foreach ($Operation in $Operations.Operations.Operation)
        {
            $OperationGroup=$Operation.IsOperationGroup
            $OpName = $Operation.Name
            $OpCHildNodes=$false
            $Opgroup=$null
            $OperationsToLoop=$null
            if(![string]::IsNullOrEmpty($OperationGroup))
            {
                if($OperationGroup -eq "true")
                {
                    $Opgroup=$Operations.Operations.OperationGroup | where {$_.Name -eq $OpName}
                    if($Opgroup.HasChildNodes)
                    {
                        $OpCHildNodes=$true
                    }
                }
            }
	        if($OpCHildNodes -and ($Operation.Enabled -eq "true"))
            {
                foreach($subOperation in $Opgroup.Operation)
                {
                    $OpName = $subOperation.Name
                    if($OpName -ne "PauseWithMessage")
                    {
	                    $Enabled = $subOperation.Enabled
	                    $Method = $subOperation.Method
	                    $script = ". .\scripts\" + $subOperation.script
	                    $parameterfile = $currentLocation + "\configuration\" +  $operationFolder + "\" + $subOperation.parameterfile
	                    $executeasSeprateProcess = $subOperation.executeasSeprateProcess
	                    $endScriptOnFail = $subOperation.endScriptOnFail.ToString().ToLower()
	                    $Action = $subOperation.LogFileName
	
	                    if ($Enabled.ToLower() -eq "true")
	                    {
		                    $logFileName = Get-ScriptLogFileName($Action)
		                    $logFileName = ".\Logs\" + $operationFolder + "\" + $logFileName
		                    $Error.Clear()
		                    Start-SPAssignment -Global
		                    Start-Transcript -Path $logFileName

		                    $importScript = $script 
		                    Invoke-Expression $importScript 
		                    $command = $Method + " '" + $parameterfile + "'"
		                    Invoke-Expression -Command $command
		
		                    Stop-Transcript
		                    Stop-SPAssignment -Global
		
		                    if(($Error.Count -gt 0) -and ($endScriptOnFail -eq "true"))
		                    {
			                    Break
			                    Exit-PSSession
		                    }

                            $Operation.Enabled = "false"
                            #$Operations.Save($opDefFileNamePath)
	                    }
                    }
                    else
                    {
                        do
                        {
                            $msg=Read-Host $Operation.Message
                            if($msg -eq 'Y')
                            {
                                continue
                            }
                            elseif($msg -eq 'N')
                            {
                                exit
                            }
                        }
                        while($msg -ne 'Y')
                    }
                }
            }
            else
            {
                $OpName = $Operation.Name
                if($OpName -ne "PauseWithMessage")
                {
	                $Enabled = $Operation.Enabled
	                $Method = $Operation.Method
	                $script = ". .\scripts\" + $Operation.script
	                $parameterfile = $currentLocation + "\configuration\" +  $operationFolder + "\" + $Operation.parameterfile
	                $executeasSeprateProcess = $Operation.executeasSeprateProcess
	                $endScriptOnFail = $Operation.endScriptOnFail.ToString().ToLower()
	                $Action = $Operation.LogFileName
	
	                if ($Enabled.ToLower() -eq "true")
	                {
		                $logFileName = Get-ScriptLogFileName($Action)
		                $logFileName = ".\Logs\" + $operationFolder + "\" + $logFileName
		                $Error.Clear()
		                Start-SPAssignment -Global
		                Start-Transcript -Path $logFileName

		                $importScript = $script 
		                Invoke-Expression $importScript 
		                $command = $Method + " '" + $parameterfile + "'"
		                Invoke-Expression -Command $command
		
		                Stop-Transcript
		                Stop-SPAssignment -Global
		
		                if(($Error.Count -gt 0) -and ($endScriptOnFail -eq "true"))
		                {
			                Break
			                Exit-PSSession
		                }

                        $Operation.Enabled = "false"
                        #$Operations.Save($opDefFileNamePath)
	                }
                }
                else
                {
                    do
                    {
                        $msg=Read-Host $Operation.Message
                        if($msg -eq 'Y')
                        {
                            continue
                        }
                        elseif($msg -eq 'N')
                        {
                            exit
                        }
                    }
                    while($msg -ne 'Y')
                }
            }
            
        }

# =================================================================================
#
# FUNC:	Get-ScriptLogFileName
# DESC:	Gets the log file nme based on invocation script name
#		Suffice the script with execution date and returns filename string 
#		in format <<Log File Name>>_<<ddMMMyyyyHHmmss>>.log
#
# =================================================================================
function Get-ScriptLogFileName
{
	if($myInvocation.ScriptName) 
	{ 
		[string]$scriptName = $myInvocation.ScriptName 
		$scriptName = $scriptName.Substring($scriptName.LastIndexOf('\')+1)
		$currentDate = Get-Date -Format "ddMMMyyyyHHmmss"
		$logFileName = $scriptName.Replace(".ps1","_" + $currentDate  + ".log")

		Return $logFileName
	}
	else 
	{ 
		[string]$scriptName = $myInvocation.MyCommand.Definition 
		$scriptName = $scriptName.Substring($scriptName.LastIndexOf('\')+1)
		$currentDate = Get-Date -Format "ddMMMyyyyHHmmss"
		$logFileName = $scriptName.Replace(".ps1","_" + $currentDate  + ".log")

		Return $logFileName
	}
}