<%-- SPG:

This HTML file has been associated with a SharePoint Page Layout (.aspx file) carrying the same name.  While the files remain associated, you will not be allowed to edit the .aspx file, and any rename, move, or deletion operations will be reciprocated.

To build the page layout directly from this HTML file, simply fill in the contents of content placeholders.  Use the Snippet Generator at http://greenroomqa.viacom.com/sites/Konda/_layouts/15/ComponentHome.aspx?Url=http%3A%2F%2Fgreenroomqa%2Eviacom%2Ecom%2Fsites%2FKonda%2F%5Fcatalogs%2Fmasterpage%2Fkonda%2FLandingPage%2Easpx to create and customize additional content placeholders and other useful SharePoint entities, then copy and paste them as HTML snippets into your HTML code.   All updates to this file within content placeholders will automatically sync to the associated page layout.

 --%>
<%@Page language="C#" Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage, Microsoft.SharePoint.Publishing, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="PageFieldFieldValue" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="Publishing" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="PageFieldTextField" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="PageFieldRichImageField" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="PageFieldRichHtmlField" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<asp:Content runat="server" ContentPlaceHolderID="PlaceHolderPageTitle">
            <SharePoint:ProjectProperty Property="Title" runat="server" />
            
            
            <PageFieldFieldValue:FieldValue FieldName="fa564e0f-0c70-4ab9-b863-0177e6ddd247" runat="server">
            </PageFieldFieldValue:FieldValue>
            
        </asp:Content><asp:Content runat="server" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea">
            
            
            <PageFieldFieldValue:FieldValue FieldName="fa564e0f-0c70-4ab9-b863-0177e6ddd247" runat="server">
            </PageFieldFieldValue:FieldValue>
            
        </asp:Content><asp:Content runat="server" ContentPlaceHolderID="PlaceHolderAdditionalPageHead">
            
            
            
            <Publishing:EditModePanel runat="server" id="editmodestyles">
                <SharePoint:CssRegistration name="&lt;% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/editmode15.css %&gt;" After="&lt;% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/pagelayouts15.css %&gt;" runat="server">
                </SharePoint:CssRegistration>
            </Publishing:EditModePanel>
            
        </asp:Content><asp:Content runat="server" ContentPlaceHolderID="PlaceHolderMain">
            <link href="/sites/Australia/_catalogs/masterpage/konda/css/pagelayouts.css" rel="stylesheet" type="text/css" />
            <div id="RightDiv">
                <div id="PageHead" class="ms-emphasis">
                    <div id="PageTitle">
                        
                        
                        
                        <h1>
                            <PageFieldTextField:TextField FieldName="fa564e0f-0c70-4ab9-b863-0177e6ddd247" runat="server">
                            </PageFieldTextField:TextField>
                        </h1>
                        
                    </div>
                    <div id="dvShare">Follow 
                    
                    
                    </div>
                    <div id="dvFollow">Share
                    
                    
                    </div>
                </div>
                <div>
                </div>
                <div>
                    
                    
                    <PageFieldRichImageField:RichImageField FieldName="3de94b06-4120-41a5-b907-88773e493458" runat="server">
                        
                    </PageFieldRichImageField:RichImageField>
                    
                </div>
                <div>
                    
                    
                    <PageFieldRichHtmlField:RichHtmlField FieldName="f55c4d88-1f2e-4ad9-aaa8-819af4ee7ee8" runat="server">
                        
                    </PageFieldRichHtmlField:RichHtmlField>
                    
                </div>
                <div data-name="WebPartZone">
                    
                    
                    <div>
                        <WebPartPages:WebPartZone runat="server" ID="x958b7b14192444e0a83f199cff277619" LayoutOrientation="Horizontal" AllowPersonalization="False" FrameType="None" Orientation="Horizontal" PartChromeType="None">
                            <ZoneTemplate></ZoneTemplate>
                        </WebPartPages:WebPartZone>
                    </div>
                    
                </div>
                <div data-name="WebPartZone">
                    
                    
                    <div>
                        <WebPartPages:WebPartZone runat="server" ID="x97ce337279374c53b0a53488f89497e1" LayoutOrientation="Horizontal" AllowPersonalization="False" FrameType="None" Orientation="Horizontal" PartChromeType="None">
                            <ZoneTemplate></ZoneTemplate>
                        </WebPartPages:WebPartZone>
                    </div>
                    
                </div>
                <div data-name="WebPartZone" style="width:50%;float:left">
                    
                    
                    <div>
                        <WebPartPages:WebPartZone runat="server" ID="x088f44209fce41cd9bc2ded02b7de317" AllowPersonalization="False" FrameType="None" Orientation="Vertical" PartChromeType="None">
                            <ZoneTemplate></ZoneTemplate>
                        </WebPartPages:WebPartZone>
                    </div>
                    
                </div>
                <div data-name="WebPartZone" style="width:50%;float:left">
                    
                    
                    <div>
                        <WebPartPages:WebPartZone runat="server" ID="x3d9c7e2b0b0941e593be34f37cc4bc6a" AllowPersonalization="False" FrameType="None" Orientation="Vertical" PartChromeType="None">
                            <ZoneTemplate></ZoneTemplate>
                        </WebPartPages:WebPartZone>
                    </div>
                    
                </div>
            </div>
            <div id="Leftdiv">
                <div data-name="WebPartZone">
                    
                    
                    <div>
                        <WebPartPages:WebPartZone runat="server" ID="xf4121615019b4147b40fb843b570189d" AllowPersonalization="False" FrameType="None" Orientation="Vertical" PartChromeType="None">
                            <ZoneTemplate></ZoneTemplate>
                        </WebPartPages:WebPartZone>
                    </div>
                    
                </div>
            </div>
        </asp:Content>