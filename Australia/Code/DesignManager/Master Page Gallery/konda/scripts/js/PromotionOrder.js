﻿var j=0;
var Order=100;
var SaveFinalOrder=0;
var finalsaveCount=0;
var getWebCountBefore=0;
var getWebCountAfter=0;
var getArticleCountBefore=0;
var getArticleCountAfter=0;
var IsDisplayArticleNotCompleted=true;
var displayloopcount=0;
var saveloopcount=0;
var arr = new Array();
var arrStatus = new Array();
var FinalOrder = new Array();
var AllSubwebs = new Array();
var flag=0;
var PromotionOrderRows;
var sections = [];
$(document).ready(function () {

getSubsiteNames();


});

function getSubsiteNames()
{
	var call = $.ajax({
	    url: _spPageContextInfo.siteAbsoluteUrl + "/_api/lists/getbytitle('SubsiteColors')/items?"+
					"&$select=Title,Id,sitepath"+
					//"&$expand=CreatedBy"+
					//"&$filter=sitepath eq '"+path+"'",//+
					"&$orderby=Id asc",
					//"&$top=20",
			//async:false,
			type: "GET",
			dataType: "json",
			headers: {
			Accept: "application/json;odata=verbose"
			}
		});
	
	call.done(function (data,textStatus, jqXHR){
		try{
			for(i=0;i<data.d.results.length;i++)
			{
				var SubsitePaths = data.d.results[i].sitepath;
				sections.push(SubsitePaths);
				
			}
			getWebs();	
		   }
		catch(e){
			//alert(e);
		}
	});
	
	call.fail(function (jqXHR,textStatus,errorThrown){
		//alert("Error getting Site Color" + jqXHR.responseText);
	});
}

function getArticles(sitename)
{
	var todaysDate=new Date();
	todaysDate.setDate(todaysDate.getDate() - 1);
	var tomorrowtodaysDate=new Date();
	tomorrowtodaysDate.setDate(tomorrowtodaysDate.getDate() + 1);
	getArticleCountBefore++;
	var call = $.ajax({
			async: false,
			url: sitename+"_api/lists/getbytitle('Pages')/items?"+
					"$select=Title,ID,FileRef,Created,PublishingPageLayout,ShownOnHomepageOrder,IsaFeaturedArticle,PromotionEndDate,PromotionStartDate,PromotionRequest,Author/Title&$expand=Author"+
					"&$filter=((startswith(ContentTypeId,'0x010100C568DB52D9D0A14D9B2FDCC96666E9F2007948130EC3DB064584E219954237AF3900242457EFB8B24247815D688C526CD44D00D82A09EDFC42AA4FBA3089B892E0C3880700') "+
					"or startswith(ContentTypeId,'0x010100C568DB52D9D0A14D9B2FDCC96666E9F2007948130EC3DB064584E219954237AF3900242457EFB8B24247815D688C526CD44D00D82A09EDFC42AA4FBA3089B892E0C3880702') "+
					"or startswith(ContentTypeId,'0x010100C568DB52D9D0A14D9B2FDCC96666E9F2007948130EC3DB064584E219954237AF3900242457EFB8B24247815D688C526CD44D00D82A09EDFC42AA4FBA3089B892E0C3880704') "+
					"or startswith(ContentTypeId,'0x010100C568DB52D9D0A14D9B2FDCC96666E9F2007948130EC3DB064584E219954237AF3900242457EFB8B24247815D688C526CD44D00D82A09EDFC42AA4FBA3089B892E0C3880707') "+
					"or startswith (ContentTypeId,'0x010100C568DB52D9D0A14D9B2FDCC96666E9F2007948130EC3DB064584E219954237AF3900242457EFB8B24247815D688C526CD44D00D82A09EDFC42AA4FBA3089B892E0C3880703')) and (PromotionEndDate gt datetime'" + todaysDate.toISOString() + "' ) and (PromotionStartDate lt datetime'" + tomorrowtodaysDate.toISOString() + "' ) and (IsaFeaturedArticle eq 1) and (substringof('Homepage',  PromotionRequest)) )",
					//+
					//"&$top=20",
			type: "GET",
			dataType: "json",
			headers: {
			Accept: "application/json;odata=verbose"
			}
		});
	
	call.done(function (data,textStatus, jqXHR){
		try{
			 
			for(i=0;i<data.d.results.length;i++)
			{
				if(data.d.results[i].PromotionRequest)
				{
				var AuthorTitle="";
				if(data.d.results[i].Author)
				{
				 AuthorTitle=data.d.results[i].Author.Title;
				}
				j++;
				
				var ParentSite = data.d.results[i].FileRef.split('/')[1].toUpperCase();
				
				var Path = data.d.results[i].FileRef.split('Pages')[0];
				Path = Path + 'Pages/Forms/EditForm.aspx?ID=' + data.d.results[i].ID;
				if(ParentSite !='PAGES')
				{
					if(IsValidForFeaturingNow(data.d.results[i].PromotionRequest) )
					{
					arr.push({
							ID: data.d.results[i].ID,
							Title: data.d.results[i].Title,
							PSite: ParentSite,
							ShowOrder: data.d.results[i].ShownOnHomepageOrder,
							FileRef: data.d.results[i].FileRef,
							IsaFeaturedArticle: data.d.results[i].IsaFeaturedArticle,
							PromotionEndDate: data.d.results[i].PromotionEndDate,
							PromotionStartDate: data.d.results[i].PromotionStartDate,
							PromotionRequest: data.d.results[i].PromotionRequest,
							Created: data.d.results[i].Created,
							CreatedBy:AuthorTitle,
							URLPath: Path
						});
					}	
				}
			}
			}	
			getArticleCountAfter++;		
		}
		
		catch(e){
		getArticleCountAfter++;
			//alert(e);
		}
	});
	
	call.fail(function (jqXHR,textStatus,errorThrown){
		getArticleCountAfter++;
		//console.log("Error generating Report: " + jqXHR.responseText);
	});
}

function IsValidForFeaturingNow(promoRequest) {
	var isValidatedForFeaturingNowResult= false;
	var promotionRequestCoice=promoRequest.split(";#").filter(function(v){return v!==''}).join(";");
		promotionRequestCoice=promotionRequestCoice.split("\n").filter(function(v){return v!==''}).join(";");
		var promotionRequestCoiceList=promotionRequestCoice.split(";");
		for( var choiceIndex=0;choiceIndex<promotionRequestCoiceList.length;choiceIndex++)
		{
			var choice=promotionRequestCoiceList[choiceIndex];
			if(choice.toLowerCase()=='homepage')
			{
			isValidatedForFeaturingNowResult=true;
			break;
			}
			
		}
	return 	isValidatedForFeaturingNowResult;
}

function compare(a,b) {
  if (a.ShowOrder < b.ShowOrder)
     return -1;
  if (a.ShowOrder > b.ShowOrder)
    return 1;
  return 0;
}

function display()
{
 //console.log( " getWebCountBefore " +getWebCountBefore +" getWebCountAfter "+ getWebCountAfter+" getArticleCountBefore " +getArticleCountBefore+" getArticleCountAfter "+ getArticleCountAfter);

	$('#PromotionOrder').css('position','relative');
	$('#PromotionOrder').prepend('<div id="loadingAnimation" style="display:none;position: absolute;top: 60%;left: 50%;margin-left: -22px;"><img id="loadingAnimationImg"  src="/SiteAssets/loadingAnimation.gif" /></div>');
	/*$('#table-1').append('<tr id="TableHead"><th style"width":15%">Article</th><th style"width":10%">ParentSite</th><th style"width":10%">Order</th><th  style"width":10%">Promo StartDate</th><th  style"width":10%">Promo EndDate</th><th  style"width":10%">Created By</th></tr>');*/
	for(var i=0;i<arr.length;i++)
	{
	var EndDate;
	var StartDate;
	if(arr[i].PromotionStartDate)
	 {
		StartDate = new Date(arr[i].PromotionStartDate);
		StartDate = StartDate.format('MMM, dd yyyy');
	 }
	 else{
	 StartDate="";
	 }
	 if(arr[i].PromotionEndDate)
	 {
		EndDate = new Date(arr[i].PromotionEndDate);
		EndDate = EndDate.format('MMM, dd yyyy');
	 }
	 else{
	 EndDate="";
	 }
	 var showOrder;
	 if(arr[i].ShowOrder)
	 {
		showOrder=arr[i].ShowOrder;
	 }
	 else{
	 showOrder="";
	 }
	var promoteInputId=arr[i].FileRef.trim().replace(/[^a-z0-9]+/gi, '-');	/* Remove / from url*/
	// promoteInputId=promoteInputId.split(".").join("");	/* Remove / from url*/
	$('#table-1').append('<tr id="'+arr[i].ID+":"+arr[i].PSite+":"+arr[i].FileRef+'" style="cursor: move;"><td><a target="_blank" href="'+arr[i].FileRef+'">'+arr[i].Title+'</a></td><td>'+arr[i].PSite+'</td><td>'+showOrder+'</td><td>'+StartDate+'</td><td>'+EndDate+'</td><td>'+arr[i].CreatedBy+'</td></tr>');
	}
	$("#table-1").tableDnD({
			        onDragClass: "myDragClass",
			        onDrop: function(table, row) {
			       	while(FinalOrder.length > 0)
			        	{
			        		FinalOrder.pop();
			        	}
			            var rows = table.tBodies[0].rows;
			            var debugStr = "<br /> Row dropped was "+row.id+"<br /> New order: ";
			            for (var i=0; i<rows.length; i++) {
			                debugStr += rows[i].id+"" ; 
							FinalOrder.push(rows[i].id);
			                debugStr += breaktag="<br />";			               
			            }
			            $('#debugArea').html(debugStr);
			         //   $('#debugArea').hide();
			        },
			        onDragStart: function(table, row) {
			            $('#debugArea').html("Started dragging row "+row.id);
			        }
			    });

}



function PromoteRequestChanged()
{
	if(FinalOrder.length == 0)
	{
		 var rows = $("#table-1")[0].rows;
		 for (var i=0; i<rows.length; i++) {
			FinalOrder.push(rows[i].id);
         }
	}
}

function SavePomotionOrderProperties()
{
	$("#loadingAnimation").css("display", "block");
	for(i=0;i<arr.length;i++)
	{
	var Contents = FinalOrder[i+1];
		if(Contents)
		{
		var Data = Contents.split(":");
		/*var promoteInputId=Data[2].trim().replace(/[^a-z0-9]+/gi, '-');
		var IsPromoted=$("#"+promoteInputId).prop("checked");*/
		var sitePath=Data[2].split('Pages')[0].replace(/^\/|\/$/g, '');
		var articleUrl=Data[2].split("'").join("''");
		SaveItems(sitePath,Data[0],articleUrl);
		finalsaveCount++;
		Order++;
		}
	}
	IsSaveOrderCompleted();
}
function SaveOrder()
{
	SavePomotionOrderProperties();
	setTimeout(function () {
    alert("Some error occurred this page will be reloaded");
	location.reload();
    }, 500000);
}

function IsSaveOrderCompleted()
{
	saveloopcount++;
	//console.log( " SaveFinalOrder " +SaveFinalOrder +" finalsaveCount " +finalsaveCount +" saveloopcount " +saveloopcount);
	if(SaveFinalOrder==finalsaveCount && saveloopcount<300)
	{
		$("#loadingAnimation").css("display", "none");
		location.reload();
	}
	else{
		setTimeout(function () {IsSaveOrderCompleted();}, 1000);
	}
}

function SaveItems(sitename,ItemID,URL)
{
try{
var finalPromotionOrder=Order;
	$.ajax({url: _spPageContextInfo.webAbsoluteUrl+"/"+sitename+"/_api/web/GetFileByServerRelativeUrl('"+ URL +"')/checkOutType",
        headers: { "Accept": "application/json; odata=verbose" }, 
        success: function(data) {
				  
                  if(data.d.CheckOutType == 0) {
					SaveFinalOrder++;
                        //console.log('The file is checked out '+URL);
						
						return;
                  }
            $.ajax({
			url: _spPageContextInfo.webAbsoluteUrl+"/"+sitename+"/_api/web/GetFileByServerRelativeUrl('"+ URL +"')/CheckOut()",
			async:false,
			method: "POST",
			headers: {
			"X-RequestDigest":  $("#__REQUESTDIGEST").val() 
			},
			success: function(data) {
			
			$.ajax({ 
			url:_spPageContextInfo.webAbsoluteUrl+"/"+sitename+ "/_api/web/lists/getbytitle('Pages')/items("+ItemID+")",
			 method: "POST",
				async: false,
				data:  JSON.stringify({"__metadata": {"type": "SP.Data.PagesItem"},"ShownOnHomepageOrder": finalPromotionOrder}),
				headers: { 
					  "X-HTTP-Method":"MERGE",
					"accept": "application/json;odata=verbose",
					"content-type": "application/json;odata=verbose",
					"X-RequestDigest": $("#__REQUESTDIGEST").val(),
					"IF-MATCH": "*"
				},
				success: function(data) {
			   
				
				$.ajax({
					url: _spPageContextInfo.webAbsoluteUrl+"/"+sitename+"/_api/web/GetFileByServerRelativeUrl('"+ URL +"')/CheckIn(comment='Comment',checkintype=1)",
					async:false,
					method: "POST",
					headers: {
						"X-RequestDigest":  $("#__REQUESTDIGEST").val() 
						},
					success: function(data) {
						SaveFinalOrder++;
					},
					 error: function (xhr) { 
						  SaveFinalOrder++;
						  //console.log(xhr.status + ': ' + xhr.statusText); 
					   }
				});
								},
				   error: function (xhr) { 
				   SaveFinalOrder++;
				  //console.log(xhr.status + ': ' + xhr.statusText); 
			   }
			});

			},
			onerror: function (xhr) { 
				  SaveFinalOrder++;
				  //console.log(xhr.status + ': ' + xhr.statusText); 
			  }

			 });
		}
	});	 
 }
 catch(e){
 SaveFinalOrder++;
 //console.log(e);
 }
}


function RetriveSites()
{
currentcontext = new SP.ClientContext.get_current();
currentweb = currentcontext.get_web();
this.subsites = currentweb.getSubwebsForCurrentUser(null);
currentcontext.load(this.subsites);
currentcontext.executeQueryAsync(Function.createDelegate(this, this.ExecuteOnSuccess),
Function.createDelegate(this, this.ExecuteOnFailure));
}
function ExecuteOnSuccess(sender, args) {
var subsites = '';
var enum1 = this.subsites.getEnumerator();
var el = document.getElementById('LKMSubWebs');
while (enum1.moveNext())
{
var Site = enum1.get_current();
//alert('hi123');
subsites += '\n' + '<a class=subsitecls href='+ Site.get_serverRelativeUrl()+'>'+Site.get_title()+'</a> </br>'
//RootElement.append('<a class=subsitecls href='+ siteTitle+'>'+siteTitle+'</a> </br>');
//alert(subsites);
}
el.innerHTML = subsites;
}
function ExecuteOnFailure(sender, args) {
/*ADFS Alert Issue*/console.log("error");
//alert(args.get_message());
}

function getWebs()
{
   var context = SP.ClientContext.get_current();
   var web = context.get_web(); 
   var site = context.get_site();
   var rootWeb = site.get_rootWeb();
   var subWebs = rootWeb.getSubwebsForCurrentUser(null);
   getWebCountBefore++;
   context.load(subWebs, 'Include(Url)');
   context.executeQueryAsync(
    function () {  
		getWebCountAfter++;	
         for (var i = 0; i < subWebs.get_count(); i++) {
            var subWeb = subWebs.itemAt(i);
			var webUrl=subWeb.get_url()+"/";
			for(k=0;k<sections.length;k++)
			{ 	
				if(sections[k])
				{
					if (webUrl.toLowerCase()=== sections[k].toLowerCase())
					{
						getArticles(webUrl); 
						getChildWebs(subWeb);
						break;
					}	
				}		
			}  
		}
		IsAllGetWebNgetArticleCompleted();
		/*arr.sort(compare); 
		display();*/
    },
    function (sender, args) {
		getWebCountAfter++;	
       //console.log(args.get_message());
    }
   );   
}


function getChildWebs(web)
{
   var context = SP.ClientContext.get_current();
   var subWebs = web.getSubwebsForCurrentUser(null);
   getWebCountBefore++;
   context.load(subWebs, 'Include(Url)');
   context.executeQueryAsync(
    function () {
	getWebCountAfter++;	
         for (var i = 0; i < subWebs.get_count(); i++) {
            var subWeb = subWebs.itemAt(i);
			var webUrl=subWeb.get_url()+"/";
			for(k=0;k<sections.length;k++)
			{ 	
				if(sections[k])
				{
					if (webUrl.toLowerCase().indexOf(sections[k].toLowerCase())>= 0)
					{
						getArticles(webUrl); 
						break;
					}	
				}		
			}  
			getChildWebs(subWeb);
          }
    },
    function (sender, args) {
	getWebCountAfter++;	
       //console.log(args.get_message());
    }
   );   
}


function IsAllGetWebNgetArticleCompleted()
{
	displayloopcount++;
	//console.log("displayloopcount  "+displayloopcount);
	if(getWebCountAfter==getWebCountBefore && getArticleCountBefore==getArticleCountAfter && IsDisplayArticleNotCompleted && displayloopcount<300)
	{
			IsDisplayArticleNotCompleted=false;
			arr.sort(compare); 
			display();
	}
	else{
		setTimeout(function () {IsAllGetWebNgetArticleCompleted();}, 1000);
	}
}

function UniqueJoinRemoveWmpty(stringValue,splitValue,joinValue)
{
var joinedString="";
 var splitArray =stringValue.split(splitValue);//.join(";");
 	for(i=0;i<splitArray.length;i++)
	{
		 if(splitArray[i])
		 { 
			 if(joinedString)
			 {
			 joinedString=joinedString+joinValue;
			 }
			  joinedString=joinedString+splitArray[i];
		 }
	}
	
	return joinedString;
}