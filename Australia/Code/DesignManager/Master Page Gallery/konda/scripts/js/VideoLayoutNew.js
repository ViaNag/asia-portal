var url = "";

$(document).ready(function () {
    ExecuteOrDelayUntilScriptLoaded(CreateVideoSnippet, "sp.js");
    //articleTagging.pageName = getPageName();
});


/*
function UploadVid() {

    var ListID = "";

    $.ajax({
        url: _spPageContextInfo.webServerRelativeUrl + "/_api/web/lists/GetByTitle('Images')/",
        type: 'GET',

        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
            "content-type": "application/json;odata=verbose",
            'X-RequestDigest': $('#__REQUESTDIGEST').val()
        },

        success: function (responseData) {
            console.log(JSON.stringify(responseData.d.Id));
            ListID = responseData.d.Id;
        },
        error: function () {
            console.log("error");
        }

    });



    var options = SP.UI.$create_DialogOptions();
    options.url = _spPageContextInfo.webAbsoluteUrl + "/_layouts/15/NewVideoSet.aspx?List=" + ListID + "&ContentTypeId=0x0120D520A8080034D0AF1705D7F24994302EA8C70E07D1&";
    options.width = "650";
    options.height = "650";
    options.allowMaximize = false;
    options.dialogReturnValueCallback = Function.createDelegate(null, CloseCallback);
    SP.UI.ModalDialog.showModalDialog(options);
}

function CloseCallback(dialogResult, returnValue) {
    $("input[Title='Video Link']").val(returnValue["newFileUrl"])
    CreateVideoSnippet();


}

*/

function CreateVideoSnippet() {

	articleTagging.pageName = getPageName();
	
    //articleTagging.getPageIdValue();

    if ($("#dvVideoLink").text().trim() == "Video Link") {
        url = $("input[Title='Video Link']").val();
    } else {
        url = $("#dvVideoLink").text();
    }


    var context = new SP.ClientContext.get_current();
    this.Web = context.get_web();
    this.list = context.get_web().get_lists().getByTitle('Videos');
    this.site = context.get_site();
	var camlQuery = new SP.CamlQuery();
	
	/*
	camlQuery.set_viewXml('<View>'+
							'<Query><Where></Where><OrderBy><FieldRef Name=\'Created\' Ascending=\'False\'/></OrderBy></Query>'+
							//'<ViewFields><FieldRef Name="Title"/><FieldRef Name="FileRef"/><FieldRef Name="ArticleStartDate"/><FieldRef Name="PublishingPageContent"/></ViewFields>'+
							'<RowLimit>1</RowLimit></View>'); */
	camlQuery.set_folderServerRelativeUrl(_spPageContextInfo.webServerRelativeUrl+'/Lists/Videos/'+articleTagging.pageName ); 
	this.oVideoCollection = this.list.getItems(camlQuery);
    

    this.folder = this.Web.getFolderByServerRelativeUrl(url);
    context.load(this.Web);
    context.load(this.site);
    context.load(this.list);
    context.load(oVideoCollection);
    //context.load(this.folder, 'Properties');
    context.executeQueryAsync(Function.createDelegate(this, this.onSuccess),
	Function.createDelegate(this, this.onFail));
}

function onSuccess(sender, args) {
    //var x = this.folder.get_properties().get_fieldValues()["vti_rtag"].toString().split("rt:")[1].split("@")[0];
    
    //var AlternateThumbnailUrl = encodeURIComponent(this.folder.get_properties().get_fieldValues()["AlternateThumbnailUrl"].split(",")[0]);
	var embedCode;

    var playerUrl = this.Web.get_url() + "/_layouts/15/videoembedplayer.aspx";
    
    var siteID = this.site.get_id().toString().replace(/-/g, '');
    var webID = this.Web.get_id().toString().replace(/-/g, '');
    var listID = this.list.get_id().toString().replace(/-/g, '');

	var listItemEnumerator = oVideoCollection.getEnumerator();
	while (listItemEnumerator.moveNext()) 
	{
		var oListItem = listItemEnumerator.get_current();       
		var getEmbedCode = oListItem.get_fieldValues().VideoSetEmbedCode;
        var AlternateThumbnailUrl = oListItem.get_fieldValues().AlternateThumbnailUrl.$3_1;
		if ( getEmbedCode != null )
		{
			embedCode = getEmbedCode;
		}
		else if ( oListItem.get_fieldValues().VideoSetExternalLink != null )
		{
			var vidExtLink = oListItem.get_fieldValues().VideoSetExternalLink.get_url();
			embedCode = '<video controls="" name="media" poster="'+AlternateThumbnailUrl+'"><source src="'+vidExtLink+'" ></video>';
		}		
		else
		{
	        
	        var itemID = oListItem.get_fieldValues().ID;
	        itemID = itemID + 3;
	        
	        var autoplay = window.location.href;
			if (autoplay.indexOf('Auto') >= 0)
			{
		        embedCode = "<iframe type='text/html' width='640' height='360' src='" + playerUrl + "?site=" + siteID + "&amp;web=" + webID + "&amp;list=" + listID + "&amp;item=" + itemID + "&amp;img=" + AlternateThumbnailUrl + "&amp;auto=1&amp;&amp;title=1&amp;lHome=1&amp;lOwner=1'></iframe>";
			}
			else
			{
				embedCode = "<iframe type='text/html' width='640' height='360' src='" + playerUrl + "?site=" + siteID + "&amp;web=" + webID + "&amp;list=" + listID + "&amp;item=" + itemID + "&amp;img=" + AlternateThumbnailUrl + "&amp;title=1&amp;lHome=1&amp;lOwner=1'></iframe>";
			}
		}		
	}

    $("#video").html("");
    $("#video").html("<div>" + embedCode + "</div>");
}

function onFail(sender, args) {
    //console.log('Failed:' + args.get_message());
}