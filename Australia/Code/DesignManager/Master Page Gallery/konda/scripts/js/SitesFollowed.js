﻿$(document).ready(function () {
    ExecuteOrDelayUntilScriptLoaded(GetFollowedSites, "SP.UserProfiles.js");
});

function GetFollowedSites() {

    $.ajax({
        url: _spPageContextInfo.siteAbsoluteUrl +  "/_api/social.following/my/followed(types=4)",
        type: "GET",
        async: false,
        headers: {
            "accept": "application/json;odata=verbose"
        },
        success: function (data, textStatus, xhr) {
            for (i = 0; i < data.d.Followed.results.length; i++) {
                var followedSiteName = data.d.Followed.results[i].Name;
                var followedSiteUri = data.d.Followed.results[i].Uri;

                var followedSiteUriPart1="";
                var followedSiteUriPart2="";
                var followedSiteUriNoPort = followedSiteUri;
                
                var indexColon = followedSiteUri.indexOf(':','7');

                if (indexColon != -1) {
                    followedSiteUriPart1 = followedSiteUri.substring(0, indexColon );
                    followedSiteUriPart2 = followedSiteUri.substring(indexColon + 1, followedSiteUri.length);

                    var indexSlash = followedSiteUriPart2.indexOf("/");
                    if (indexSlash != -1) {
                        followedSiteUriPart2 = followedSiteUriPart2.substring(indexSlash, followedSiteUriPart2.length);
                        followedSiteUriNoPort = followedSiteUriPart1 + followedSiteUriPart2;
                    }
                }
                if (_spPageContextInfo.webAbsoluteUrl == followedSiteUriNoPort) {
                    $("div#greenroom-subsite-title-follow").addClass("SiteFollowed");
                    return false;
                }

            }

        },
        error: _funcRequestFailed
    });

}

function _funcRequestFailed(xhr, ajaxOptions, thrownError) {
    //console.log('Error: ' + xhr.status + ' ' + thrownError + ' ' + xhr.responseText);

}
