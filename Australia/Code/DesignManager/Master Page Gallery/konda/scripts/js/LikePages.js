﻿var likepage = {
    //Likes the current page. 
    LikePage: function () {
        likepage.getUserLikedPage(function(likedPage, likeCount) {
 
            var aContextObject = new SP.ClientContext();
            EnsureScriptFunc('reputation.js', 'Microsoft.Office.Server.ReputationModel.Reputation', function() {
                Microsoft.Office.Server.ReputationModel.
                    Reputation.setLike(aContextObject,
                        _spPageContextInfo.pageListId.substring(1, 37),
                        _spPageContextInfo.pageItemId, !likedPage);
 
                aContextObject.executeQueryAsync(
                    function() {
                        var elements = $(".likecount");
                   /*ADFS Alert Issue*/console.log(likedPage);
                        if (likedPage) {
                            likeCount--;
                        } else {
                            likeCount++;
                        }
                        for (var i = 0; i < elements.length;i++) {
                            elements[i].innerHTML = likeCount;
                        }
                    }, function(sender, args) {
                        // Custom error handling if needed
                        
                    });
            });
        });
 
    },
    // Checks if the user already liked the page, and returns the number of likes. 
    getUserLikedPage: function (cb) {
   
        var context = new SP.ClientContext(_spPageContextInfo.webServerRelativeUrl);
        var list = context.get_web().get_lists().getById(_spPageContextInfo.pageListId);
        var item = list.getItemById(_spPageContextInfo.pageItemId);
 
        context.load(item, "LikedBy", "ID", "LikesCount");
        context.executeQueryAsync(Function.createDelegate(this, function (success) {
            // Check if the user id of the current users is in the collection LikedBy. 
            var $v_0 = item.get_item('LikedBy');
   
            if (!SP.ScriptHelpers.isNullOrUndefined($v_0)) {
                for (var $v_1 = 0, $v_2 = $v_0.length; $v_1 < $v_2; $v_1++) {
                    var $v_3 = $v_0[$v_1];
                    if ($v_3.$1E_1 === _spPageContextInfo.userId) {
                        cb(true, item.get_item('LikesCount'));
                    }
                }
            }
            cb(false, item.get_item('LikesCount'));
        }),
            Function.createDelegate(this, function (sender, args) { 
            //Custom error handling if needed 
            }));
    },
    initialize: function () {
        var elements = $(".likecount");
        likepage.getUserLikedPage(function(likedPage, likesCount) {
            for (var i = 0; i < elements.length; i++) {
                elements[i].innerHTML = likesCount;
            }
        });
    }
};
$(document).ready(function () {

         SP.SOD.executeFunc('sp.js', 'SP.ClientContext', likepage.initialize); 
				 if($(".comments").length>0){
				 GetCommentsCount(); 
				 }
               });



function GetCommentsCount()
{
//alert(_spPageContextInfo.webServerRelativeUrl);
 $.ajax( {
        url: _spPageContextInfo.webServerRelativeUrl+"/_api/web/lists/getbytitle('Microfeed')/items" ,//
        type: "GET",
        async:false,
        headers: { 
            "accept":"application/json;odata=verbose",
            "content-type":"application/json;odata=verbose",
             'X-RequestDigest': $('#__REQUESTDIGEST').val()         
            },
        success: function (responseData) { 
       			//alert(responseData.d.results.length-2)
       	if(responseData.d.results.length === 0)
       		$(".comments").html(responseData.d.results.length);
       	else
       		$(".comments").html(responseData.d.results.length-2);
				},
        error: _funcRequestFailed
    });

}

function _funcRequestFailed (xhr, ajaxOptions, thrownError){
		//console.log('Error: ' + xhr.status + ' ' + thrownError + ' ' + xhr.responseText); 
		//var _orufusUtility = new _rufusUtility();  
    	//_orufusUtility._funcAddStatus('Error: ' + xhr.status + ' ' + thrownError + ' ' + xhr.responseText+' '+"Please contact you administrator!", "red");
}



