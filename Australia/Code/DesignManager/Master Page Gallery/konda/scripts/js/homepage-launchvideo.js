var modal = (function(){
				var 
				method = {},
				$overlay,
				$modal,
				$content,
				$close;

				
				method.center = function () {
					var top, left;
					$modal.css({
						top:'50%',
						left:'50%',
                        'width': '680px',
                        margin: '-250px 0 0 -280px',
                        background:'rgba(0,0,0,0.2)',
                        borderRadius:'14px',
                        padding:'8px'
					});
				};

				
				method.open = function (settings) {
					$content.empty().append(settings.content);

					$modal.css({
						width: settings.width || 'auto', 
						height: settings.height || 'auto'
					});

					method.center();
					$(window).bind('resize.modal', method.center);
					$modal.show();
					$overlay.show();
				};

				
				method.close = function () {
					$modal.hide();
					$overlay.hide();
					$content.empty();
					$(window).unbind('resize.modal');
				};

				
				$overlay = $('<div id="overlay"></div>');
				$modal = $('<div id="modal"></div>');
				$content = $('<div id="content"></div>');
				$close = $('<a id="close" href="#">close</a>');

				$modal.hide();
				$overlay.hide();
				$modal.append($content, $close);

				$(document).ready(function(){
					$('body').append($overlay, $modal);						
				});

				$close.click(function(e){
                    var now = new Date();
                    var expires = now.setTime(now.getTime() + 1000000 * 365 * 60 * 1000); //Expire in one hour
                    document.cookie = 'GreenroomWelcomeVideo=1;path=/;expires=' + new Date(expires) + ';';
				
					method.close();
					jwplayer('content').remove();
				});

				return method;
}());
/* popup video ends */

$(document).ready(function(){
/* popup video starts */		
				$('#openvideo1').click(function(e){
//alert("yodel2");  
				modal.open({content: "Loading the player..."});
					jwplayer("content").setup({
						controlbar:"bottom",
						file: "http://a836.g.akamaitech.net/7/836/123/e358f5db0045e9/intlscrnstor.download.akamai.com/101865/viacomcorporate/GreenroomLaunchVideo-050515.mp4",
						image: "/happeningnow/Lists/Videos/greenroom-video-still.gif",
                        flashplayer: "/_catalogs/masterpage/greenroom/scripts/js/jwplayer6/jwplayer.flash.swf",
						autostart: true,
                        width: "640",
   						height: "360",
				});
				
					e.preventDefault();
				});
	if (getCookie("GreenroomWelcomeVideo", "1") == false) {
		setTimeout(function() {$('#openvideo1').trigger('click');},5);	
	};	
});
/* popup video ends */

function getCookie(name, value) {
    if (document.cookie.indexOf(name) == 0) //Match without a ';' if its the firs
        return -1 < document.cookie.indexOf(value ? name + "=" + value + ";" : name + "=")
    else if (value && document.cookie.indexOf("; " + name + "=" + value) + name.length + value.length + 3 == document.cookie.length) //match without an ending ';' if its the last
        return true
    else { //match cookies in the middle with 2 ';' if you want to check for a value
        return -1 < document.cookie.indexOf("; " + (value ? name + "=" + value + ";" : name + "="))
    }
}
