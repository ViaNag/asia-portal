﻿var hostUrl = _spPageContextInfo.siteAbsoluteUrl;
var collListItem;

$(document).ready(function () {

    var scriptbase = hostUrl + "/_layouts/15/";
    $.getScript(scriptbase + 'SP.Runtime.js',
     function () {
         $.getScript(scriptbase + 'SP.js', execCrossDomainRequest);
     });
});

function execCrossDomainRequest() {
    var context = SP.ClientContext.get_current();

    var web = context.get_web();
    var lists = web.get_lists();


    this.web = context.get_web();
    mylist = this.web.get_lists().getByTitle('landingpagecontent');
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml('<View><Query><OrderBy><FieldRef Name="ID"/></OrderBy></Query><ViewFields><FieldRef Name=\'ID\'/><FieldRef Name=\'Inactive\'/><FieldRef Name=\'Title\'/><FieldRef Name=\'Linktoform\'/><FieldRef Name=\'Category\'/></ViewFields></View>');
    collListItem = mylist.getItems(camlQuery);
    context.load(mylist);
    context.load(collListItem, 'Include(ID, Title, Linktoform, Category, Inactive)');
    context.executeQueryAsync(
   Function.createDelegate(this, successHandler),
   Function.createDelegate(this, errorHandler)
  );


    function successHandler() {
        var listItemInfo = '';
        var listItemEnumerator = collListItem.getEnumerator();
        var count = 0;
        var row = '';
        var interneval = '';
        var needintern = '';
        var reqt = '';
        var refi='';

        row += "<form method='post'><select id='list'>";
        interneval += "<form method='post'><select id='list'>";
        needintern += "<form method='post'><select id='list'>";
        reqt += "<form method='post'><select id='list'>";
        refi += "<form method='post'><select id='list'>";



        while (listItemEnumerator.moveNext()) {

            var oListItem = listItemEnumerator.get_current();
            var id = oListItem.get_item('ID');
            //console.log("id : " + id);
            var title = oListItem.get_item('Title');
            var linktoform = oListItem.get_item('Linktoform');
            var inactive = oListItem.get_item('Inactive');
            var category = oListItem.get_item('Category');
            var completetraining = "Complete Training";
            var internevl = "Intern evaluation";
            var requirements = "Requirements";
            var rai = "Request an Intern";
            var refer = "Refer an Intern";






            if (category == "Request an Intern" && linktoform != "" && inactive != true) {
                row += "<option value='" + linktoform + "'>" + title + "</option>";
            }

            if (category == "Intern Evaluation" && linktoform != "" && inactive != true) {
                interneval += "<option value='" + linktoform + "'>" + title + "</option>";
            }

            if (category == "Need an Intern" && linktoform != "" && inactive != true) {


                needintern += "<option value='" + linktoform + "'>" + title + "</option>";

            }
            if (category == "Required Training" && linktoform != "" && inactive != true) {


                reqt += "<option value='" + linktoform + "'>" + title + "</option>";

            }

			 if (category == "Refer an Intern" && linktoform != "" && inactive != true) {

                refi+= "<option value='" + linktoform + "'>" + title + "</option>";

            }


            count++;



        }
        row += "</select></form><div class='btn btn-primary btn-lg'  id='rai' class='crbutton'><span class='ms-rteStyle-Accent2' style='color:#ffffff'>" + rai + "</span></div>";
        interneval += "</select></form><div class='btn btn-primary btn-lg'  id='internevl' class='crbutton'><span class='ms-rteStyle-Accent2' style='color:#ffffff'>" + internevl + "</span></div>";
        needintern += "</select></form><div class='btn btn-primary btn-lg'  id='completetraining' class='crbutton'><span class='ms-rteStyle-Accent2' style='color:#ffffff'>" + completetraining + "</span></div>";
        reqt += "</select></form><div class='btn btn-primary btn-lg'  id='requirements' class='crbutton'><span class='ms-rteStyle-Accent2' style='color:#ffffff'>" + requirements + "</span></div>";
		refi += "</select></form><div class='btn btn-primary btn-lg'  id='refer' class='crbutton'><span class='ms-rteStyle-Accent2' style='color:#ffffff'>" + refer + "</span></div>";

        document.getElementById("requestanintern").innerHTML = row.toString();
        document.getElementById("internevaluation").innerHTML = interneval.toString();
        document.getElementById("needanintern").innerHTML = needintern.toString();
        document.getElementById("requiredtraining").innerHTML = reqt.toString();
        document.getElementById("referanintern").innerHTML = refi.toString();

        LoadButton();
    }
    function errorHandler() {
        //console.log(args.get_message() + '\n' + args.get_stackTrace());
    }

}


function LoadButton() {

    $("#requestanintern #list").prepend("<option selected='true' value='0'>Select your city</option>");
    $("#requestanintern #list").val(0);

    $("#needanintern #list").prepend("<option selected='true' value='0'>Select your city</option>");
    $("#needanintern #list").val(0);


    $("#requiredtraining #list").prepend("<option selected='true' value='0'>Select your city</option>");
    $("#requiredtraining #list").val(0);


    $("#internevaluation #list").prepend("<option selected='true' value='0'>Select your city</option>");
    $("#internevaluation #list").val(0);
    
    

    $("#referanintern #list").prepend("<option selected='true' value='0'>Select your city</option>");
    $("#referanintern #list").val(0);









    $("#rai").click(function () {
        window.location.href = $("#requestanintern #list").val();
    });

    $("#completetraining").click(function () {
        window.location.href = $("#needanintern #list").val();
    });


    $("#requirements").click(function () {
        window.location.href = $("#requiredtraining #list").val();
    });

    $("#internevl").click(function () {

        window.location.href = $("#internevaluation #list").val();

    });
    
    
    $("#refer").click(function () {

        window.location.href = $("#referanintern #list").val();

    });




}
