var PrintLounge = {

clientContext:null,

checkPrintLoungeStatus : function(){

				var TodayDate = new Date();
				var getTodayDate = TodayDate.format('yyyy-MM-dd');
				
				var start = TodayDate.format('yyyy-MM-ddTHH:mm:ssZ');
			jQuery.noConflict();
			$=jQuery;
			$().SPServices({
			  operation: "GetListItems",
			  async: false,
			  listName: "Print Lounge Calendar",
			  CAMLViewFields: "<ViewFields>" +
			  		    "<FieldRef Name='Title' />" +
			  		    "<FieldRef Name='EventDate' />" +
			  		    "<FieldRef Name='EndDate' />" +
			  		    "<FieldRef Name='Location' />" +
			  		    "<FieldRef Name='PrintLoungeStatus' />" +
			  		    "<FieldRef Name='Description' />" +
			  		    "<FieldRef Name='fRecurrence' />" +
			  		    "<FieldRef Name='RecurrenceData' />" +
			  		    "<FieldRef Name='fAllDayEvent' />" +
			  		"</ViewFields>",
			  CAMLQuery: '<Query>' +
					'<CalendarDate>' + start + '</CalendarDate>' +
					'<Where>' +
					    '<DateRangesOverlap>' +
						'<FieldRef Name="EventDate" />' +
						'<FieldRef Name="EndDate" />' +
						'<FieldRef Name="RecurrenceID" />' +
						'<Value Type="DateTime">' +
						    '<Year />' +
						'</Value>' +
					    '</DateRangesOverlap>' +
					'</Where>' +			'<OrderBy>' +
					    '<FieldRef Name="EventDate" />' +
					'</OrderBy>' +
				    '</Query>',
			  CAMLQueryOptions: '<QueryOptions>' +
						'<CalendarDate>' + start + '</CalendarDate>' +
						'<RecurrencePatternXMLVersion>v3</RecurrencePatternXMLVersion>' +
						'<ExpandRecurrence>TRUE</ExpandRecurrence>' +
					'</QueryOptions>',
			  completefunc: function (xData, Status) {
		
				//var itemCount = $(xData.responseXML).SPFilterNode("rs:data").attr("ItemCount");
				
				$(xData.responseXML).SPFilterNode("z:row").each(function(){
				if ( $(this).attr("ows_EventDate").split(' ')[0] == getTodayDate )
				{
					if ( $(this).attr("ows_PrintLoungeStatus") == "Open" )
					{
						var getStartTime = new Date($(this).attr("ows_EventDate").replace(/\-/g, "/"));
						var startTime = PrintLounge.formatDateTime(getStartTime);
  
						var getEndTime = new Date($(this).attr("ows_EndDate").replace(/\-/g, "/"));
						var endTime = PrintLounge.formatDateTime(getEndTime);
						
						$('#printloungestatus').html('<div id="greenroom-featurecallout-large-headline"><h1>Print Lounge <br />is Open</h1></div><div id="greenroom-featurecallout-large-top-content"><p>We are open today from '+ startTime +' until '+ endTime +'</p></div> ');
					}
					else
					{
						$('#printloungestatus').html('<div id="greenroom-featurecallout-large-headline"><h1>Print Lounge <br />is Closed</h1></div><div id="greenroom-featurecallout-large-top-content"><p>You can start or continue your project by clicking on the &#39;Print Lounge Remote&#39; button to the right</p></div> ');
					}
				}
				});
			}
		});
	},
	
	formatDateTime : function(dateFormat){
	
		var hours = dateFormat.getHours();
		var minutes = dateFormat.getMinutes();
		var ampm = hours >= 12 ? 'PM' : 'AM';
		hours = hours % 12;
		hours = hours ? hours : 12; // the hour '0' should be '12'
		minutes = minutes < 10 ? '0'+minutes : minutes;
		var formattedTime = hours + ':' + minutes + ' ' + ampm;
		return formattedTime;
	}
}


$(window).load(function(){
jQuery.noConflict();
PrintLounge.checkPrintLoungeStatus();
});
