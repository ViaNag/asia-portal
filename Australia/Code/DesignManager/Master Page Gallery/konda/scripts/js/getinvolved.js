﻿(function () {
   
	var itemCtx = {};
	itemCtx.Templates = {};
	
        itemCtx.Templates.Header = "<div class='researchcontainer'>";
        itemCtx.Templates.Item = ItemOverrideFun;+"</div>";
        itemCtx.Templates.Footer = pagingControl; 


	itemCtx.BaseViewID = 1;
	itemCtx.ListTemplateType = 100;
	
	SPClientTemplates.TemplateManager.RegisterTemplateOverrides(itemCtx);

})();

function ItemOverrideFun(ctx) {
var liContent ="";

var downloadpdf="Download PDF";


var link = ctx.CurrentItem.Link;
var title = ctx.CurrentItem.Title;
var image = ctx.CurrentItem["PublishingRollupImage"];
var imageSplit = ctx.CurrentItem["PublishingRollupImage"].split("src=")[1];
var imageSrc = imageSplit.split("\"")[1];


if(ctx.CurrentItem.firstRow == true && ctx.ListData.FirstRow == 1)
{
	/*liContent += "<div class='sirdate'><font class='latestissue' color='#999999'>Latest Issue: </font>";
	liContent += date ;
	liContent += "<br><img class='latestissuehr' src='../../PublishingImages/latestentryhr.png' /></div><br/>";*/
	liContent +="<span class='greenroom-featured'><img src='"+imageSrc +"'/></span><span class='greenroom-col4'>"+title +"</span>";

//	liContent += "<div class='pdf' width='100%'><img src='../../PublishingImages/downloadicon.png'><a class='downloadpdftext' href='"+ download +"'> "+downloadpdf+" </a><br/><br/><img class='endhr' src='../../PublishingImages/hr.png'></div>";
	liContent += "<br/><div style='height:50px'></div>";
}
else
{

	liContent += "<span class='greenroom-col1'><img src='"+imageSrc +"'/></span><span class='greenroom-col2'><a href='"+link +"' target='_blank'>" + title+ "</a></span><span class='greenroom-col3'><a href='"+link +"' target='_blank'><img src='/PublishingImages/rightarrow.png'/></a></span><br/>";
	//liContent += "<a href='"+link+"')>"+title + "</a><br/><br/>";
}
return liContent ;

}

function pagingControl(ctx) {
       var footerHtml = "";
        var firstRow = ctx.ListData.FirstRow;
        var lastRow = ctx.ListData.LastRow;
        var prev = ctx.ListData.PrevHref;
        var next = ctx.ListData.NextHref;

           footerHtml += "<div align='right' style='padding-right:50px'><table class=\"ms-bottompaging\"><tr><td>";
            if (prev)
                footerHtml += "<a class='ms-commandLink ms-promlink-button ms-promlink-button-enabled' href='" + prev + "'><span class='ms-promlink-button-image'><img class='ms-promlink-button-left' src='/_layouts/15/images/spcommon.png?rev=23' /></span></a><a  href='" + prev + "'><span>&nbsp;&nbsp;&nbsp;Newer Issues&nbsp;&nbsp;</span></a>";
            else
               // footerHtml += "<a class='ms-commandLink ms-promlink-button ms-promlink-button-disabled' href='javascript:void(0);'><span class='ms-promlink-button-image'><img class='ms-promlink-button-left-disabled' src='/_layouts/15/images/spcommon.png?rev=23' /></span></a>";

            footerHtml += "<span class=\"ms-promlink-button-inner\"></span>";
            if (prev && next)
            	footerHtml += "|";


            if (next)
                footerHtml += "<a  href='" + next + "'><span>&nbsp;&nbsp;Older Issues&nbsp;&nbsp;&nbsp;</span></a><a class='ms-commandLink ms-promlink-button ms-promlink-button-enabled' href='javascript:void(0);' onclick=\"RefreshPageTo(event, &quot;" + next + "&quot;);return false;\"><span class='ms-promlink-button-image'><img class='ms-promlink-button-right' src='/_layouts/15/images/spcommon.png?rev=23'/></span></a>";
            else
               // footerHtml += "<a class='ms-commandLink ms-promlink-button ms-promlink-button-disabled' href='javascript:void(0);'><span class='ms-promlink-button-image'><img class='ms-promlink-button-right-disabled' src='/_layouts/15/images/spcommon.png?rev=23' /></span></a>";

            footerHtml += "</td></tr></table></div>";

        return footerHtml;    }