﻿_QuickLinksPanel = function (){

this._followingManagerEndpoint = _spPageContextInfo.siteAbsoluteUrl+ "/_api/social.following";
this._digest = $("#__REQUESTDIGEST").val();
this._myfollowedContent;
}

_QuickLinksPanel.prototype._funcFillQuickLinks = function (){


var _followedContent = new Array();
 var _orufusUtility = new _rufusUtility();
 $.ajax( {
        url: this._followingManagerEndpoint+ "/my/followed(types=2)",
        type: "GET",
        async:false,
        headers: { 
            "accept":"application/json;odata=verbose",
            "content-type":"application/json;odata=verbose",
            "X-RequestDigest":this._digest
        },
        success: function (responseData) {      
	        for(var i=0 ; i < responseData.d.Followed.results.length ; i++)
	        {      		
	        	_followedContent.push(responseData.d.Followed.results[i]);
	        }                   
        },
        error: _orufusUtility._funcRequestFailed
    } );
    $.ajax( {
        url: this._followingManagerEndpoint+ "/my/followed(types=4)",
        type: "GET",
        async:false,
        headers: { 
            "accept":"application/json;odata=verbose",
            "content-type":"application/json;odata=verbose",
            "X-RequestDigest":this._digest
        },
        success: function (responseData) {      
	        for(var i=0 ; i < responseData.d.Followed.results.length ; i++)
	        {      		
	        	_followedContent.push(responseData.d.Followed.results[i]);
	        }                   
        },
        error: _orufusUtility._funcRequestFailed
    } );


//console.log(JSON.stringify(_followedContent));
this._myfollowedContent =  _followedContent;


}

$(document).ready(function (){
	var _oQuickLinksPanel = new _QuickLinksPanel();
	_oQuickLinksPanel._funcFillQuickLinks();
	for(var i=0 ; i< _oQuickLinksPanel._myfollowedContent.length ; i++)
	{
		$(".pnl-quick-links").append("<div class='item-quick-link' onclick='javascript:{window.location.href=\""+_oQuickLinksPanel._myfollowedContent[i].Uri+"\"}' >"+_oQuickLinksPanel._myfollowedContent[i].Name+"</div>");
	}
	
	//clicking on edit links
	$(".btn-edit-quick-link").click(function (){
	$(".pnl-edit-quick-links").html("");
		$(".pnl-quick-links").hide();
		$(this).hide();
		for(var i=0 ; i< _oQuickLinksPanel._myfollowedContent.length ; i++)
		{
			$(".pnl-edit-quick-links").append("<div class='item-edit-quick-link'>"+_oQuickLinksPanel._myfollowedContent[i].Name+"</div>");
		}
		$(".wrap-edit-quick-links").show();
	});
	
	//cliking on close edit links
	$(".save-edit-quick-links").click(function (){
				$(".pnl-quick-links").show();
				$(".wrap-edit-quick-links").hide();
				$(".btn-edit-quick-link").show();
		});

});








