﻿function Task(data) {

    this.Title = ko.observable(data.Title);
    this.ID = ko.observable(data.ID);
    this.IsVisible = ko.observable(data.IsVisible);
    this.__metadata = ko.observable(data.__metadata);
    this.LinkLocation = ko.observable(data.LinkLocation);
    this.ExternalLink = ko.observable(data.ExternalLink);

    this.BackgroundImageLocation = ko.observable(data.BackgroundImageLocation);
    this.LaunchBehavior = ko.observable(data.LaunchBehavior);

}

function TaskListViewModel() {
    // Data
    var self = this;


    self._funcFillQuickLinks = function () {

        var userId = _spPageContextInfo.userId;
        //alert("http://newrufusdev.viacom.com/_api/web/lists/getbyTitle('My Promoted Links')/items?$orderby=TileOrder&$filter=IsVisible eq true and User eq " + userId);
        // Load initial state from server, convert it to Task instances, then populate self.tasks"http://newrufusdev.viacom.com/_api/web/lists/getbyTitle('My Promoted Links')/items"
        $.ajax({
            url: "http://newrufusdev.viacom.com/_api/web/lists/getbyTitle('My Promoted Links')/items?$orderby=TileOrder&$filter=IsVisible ne false and User eq " + userId, //
            type: "GET",
            async: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "content-type": "application/json;odata=verbose",
                'X-RequestDigest': $('#__REQUESTDIGEST').val()
            },
            success: function (responseData) {
                //alert(responseData.d.results.length);
                //self._myfollowedContent(responseData.d.results);
                /*if(responseData.d.results.length === 12)
       				{$('.save-internal-link').attr("disabled","disabled");$('.save-internal-link').css("color","gray");
       				$('.save-external-link').attr("disabled","disabled");$('.save-external-link').css("color","gray");}
       				else
       				{
       					$('.save-internal-link').removeAttr("disabled");$('.save-internal-link').css("color","black");
       					$('.save-external-link').removeAttr("disabled");$('.save-external-link').css("color","black");
       				}*/
               // var scriptbase = _spPageContextInfo.siteAbsoluteUrl + '/_layouts/15/';

                //load all appropriate scripts for the page to function

              SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {

//alert("inside SP.SOD");
                    if (responseData.d.results.length === 0) {
                        var clientContext = SP.ClientContext.get_current();
                        var oList = clientContext.get_web().get_lists().getByTitle('My Promoted Links');
                        var itemArray = new Array();

                        $.ajax({
                            url: "http://newrufusdev.viacom.com/_api/web/lists/getbyTitle('Enterprise Links')/items", //
                            type: "GET",
                            async: false,
                            headers: {
                                "accept": "application/json;odata=verbose",
                                "content-type": "application/json;odata=verbose",
                                'X-RequestDigest': $('#__REQUESTDIGEST').val()
                            },
                            success: function (responseData) {
                                itemArray.push(responseData.d.results);
                            },
                            error: _funcRequestFailed
                        });


                        for (var i = 0; i < 9; i++) {
                            // alert(JSON.stringify(itemArray[0][i]["LinkLocation"]["Url"]));
                             //alert(JSON.stringify(itemArray[0][i]["BackgroundImageLocation"]["Url"]));


                           // alert(JSON.stringify(itemArray[0][i]["Title"]));
                            var itemCreateInfo = new SP.ListItemCreationInformation();
                            var oListItem = oList.addItem(itemCreateInfo);
                            oListItem.set_item('Title', itemArray[0][i]["Title"]);
                            oListItem.set_item('LaunchBehavior', itemArray[0][i]["LaunchBehavior"]);
                            oListItem.set_item('BackgroundImageLocation', itemArray[0][i]["BackgroundImageLocation"]["Url"]);
                            oListItem.set_item('LinkLocation', itemArray[0][i]["LinkLocation"]["Url"]);
                            oListItem.set_item('User', _spPageContextInfo.userId);
                            oListItem.set_item('TileOrder', i);

                            oListItem.set_item('IsVisible', true);

                            oListItem.update();
                            
                        


                        }
                        for (var i = 9; i < 12; i++) {

                            var itemCreateInfo = new SP.ListItemCreationInformation();
                            var oListItem = oList.addItem(itemCreateInfo);
                            oListItem.set_item('Title', '');

                            oListItem.set_item('User', _spPageContextInfo.userId);
                            oListItem.set_item('TileOrder', i);

                            oListItem.set_item('IsVisible', false);

                            oListItem.update();


                        }
						clientContext.executeQueryAsync(function () {
						//alert("Success");
                            self._funcFillQuickLinks();
                        }, function (sender, args) {
    //console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
});

                    } 
					else {
                        var mappedTasks = $.map(responseData.d.results, function (item) {
                            return new Task(item)
                        });
                        self._myfollowedContent(mappedTasks);

                    }
                });




            },
            error: _funcRequestFailed
        });
    }
    self._myfollowedContent = ko.observableArray();
    self.selectedTask = ko.observable();
    self.clearTask = function (data, event) {

        if (data === self.selectedTask()) {
            self.selectedTask(null);
        }

        if (data.name() === "") {
            self._myfollowedContent.remove(data);
        }
    };
    self.Cancel = function () {

        self._funcFillQuickLinks();

    };
    self.AddLink = function () {

        var userId = _spPageContextInfo.userId;
        $.ajax({
            url: "http://newrufusdev.viacom.com/_api/web/lists/getbyTitle('My Promoted Links')/items?$orderby=TileOrder&$filter=IsVisible eq false and User eq " + userId + "&$top=1",
            method: "GET",
            async: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "content-type": "application/json;odata=verbose",
                'X-RequestDigest': $('#__REQUESTDIGEST').val()
            },
            success: function (responseData) {
                //	alert(JSON.stringify(responseData.d.results.length));
                if (responseData.d.results.length !== 0) {
                    var data = '{"Title":"' + _spPageContextInfo.webTitle + '","LaunchBehavior":"In page navigation","IsVisible":true,"__metadata":{"type":"SP.Data.My_x0020_Promoted_x0020_LinksListItem"},"LinkLocation":{"__metadata":{"type":"SP.FieldUrlValue"},"Description":"","Url":"' + _spPageContextInfo.webAbsoluteUrl + '"},"BackgroundImageLocation":{"__metadata":{"type":"SP.FieldUrlValue"},"Description":"","Url":"' + _spPageContextInfo.siteAbsoluteUrl + "/" + _spPageContextInfo.webLogoUrl + '"}}';
                    $.ajax({
                        url: "http://newrufusdev.viacom.com/_api/web/lists/getbyTitle('My Promoted Links')/items(" + responseData.d.results[0].ID + ")",
                        method: "POST",
                        async: false,
                        data: data,
                        headers: {
                            "X-HTTP-Method": "MERGE",
                            "IF-MATCH": "*",
                            "accept": "application/json;odata=verbose",
                            "content-type": "application/json;odata=verbose",
                            'X-RequestDigest': $('#__REQUESTDIGEST').val()
                        },
                        success: function (responseData) {

                        },
                        error: _funcRequestFailed
                    });
                } else {
                    SP.UI.Status.removeAllStatus(true);
                    var statusId;
                    statusId = SP.UI.Status.addStatus("You have reached limit to add quick links!" + "<a href='javascript:{SP.UI.Status.removeAllStatus(true);}' style='float:right'><img style='height:70px;width:70px;position:absolute;left:93%;top:5%' src='/SiteAssets/RufusImages/delete-icon.png'/><a>");
                    SP.UI.Status.setStatusPriColor(statusId, "red");

                }

            },
            error: _funcRequestFailed
        });
        self._funcFillQuickLinks();

    };
    self.AddLinkExternal = function () {

        var userId = _spPageContextInfo.userId;

        $.ajax({
            url: "http://newrufusdev.viacom.com/_api/web/lists/getbyTitle('My Promoted Links')/items?$orderby=TileOrder&$filter=IsVisible eq false and User eq " + userId + "&$top=1",
            method: "GET",
            async: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "content-type": "application/json;odata=verbose",
                'X-RequestDigest': $('#__REQUESTDIGEST').val()
            },
            success: function (responseData) {
                //	alert(JSON.stringify(responseData.d.results.length));
                if (responseData.d.results.length !== 0) {
                    var data = '{"Title":"' + $("#DeltaPlaceHolderPageTitleInTitleArea").text() + '","LaunchBehavior":"New tab","IsVisible":true,"__metadata":{"type":"SP.Data.My_x0020_Promoted_x0020_LinksListItem"},"LinkLocation":{"__metadata":{"type":"SP.FieldUrlValue"},"Description":"","Url":"' + $(".RedirectLink").text() + '"},"BackgroundImageLocation":{"__metadata":{"type":"SP.FieldUrlValue"},"Description":"","Url":"' + _spPageContextInfo.siteAbsoluteUrl + "/" + _spPageContextInfo.webLogoUrl + '"}}';
                    $.ajax({
                        url: "http://newrufusdev.viacom.com/_api/web/lists/getbyTitle('My Promoted Links')/items(" + responseData.d.results[0].ID + ")",
                        method: "POST",
                        async: false,
                        data: data,
                        headers: {
                            "X-HTTP-Method": "MERGE",
                            "IF-MATCH": "*",
                            "accept": "application/json;odata=verbose",
                            "content-type": "application/json;odata=verbose",
                            'X-RequestDigest': $('#__REQUESTDIGEST').val()
                        },
                        success: function (responseData) {

                        },
                        error: _funcRequestFailed
                    });
                } else {
                    SP.UI.Status.removeAllStatus(true);
                    var statusId;
                    statusId = SP.UI.Status.addStatus("You have reached limit to add quick links!" + "<a href='javascript:{SP.UI.Status.removeAllStatus(true);}' style='float:right'><img style='height:70px;width:70px;position:absolute;left:93%;top:5%' src='/SiteAssets/RufusImages/delete-icon.png'/><a>");
                    SP.UI.Status.setStatusPriColor(statusId, "red");

                }

            },
            error: _funcRequestFailed
        });
        self._funcFillQuickLinks();

    };

    self.remove = function (task) {

        task.IsVisible = false;
        self._myfollowedContent.remove(task);
        self._myfollowedContent.push(task);
    },

    self.addTask = function () {
        //var body = ko.toJSON(self._myfollowedContent());

        // alert(self._myfollowedContent().length);
        for (var i = 0; i < self._myfollowedContent().length; i++) {


            self._myfollowedContent()[i].TileOrder = i;
            //console.log(ko.toJSON(self._myfollowedContent()[i]));
            var body = JSON.parse(ko.toJSON(self._myfollowedContent()[i]));

            //alert(JSON.stringify(body));

            $.ajax({
                url: "http://newrufusdev.viacom.com/_api/web/lists/getbyTitle('My Promoted Links')/items(" + body.ID + ")",
                method: "POST",
                async: false,
                data: JSON.stringify(body),
                headers: {
                    "X-HTTP-Method": "MERGE",
                    "IF-MATCH": "*",
                    "accept": "application/json;odata=verbose",
                    "content-type": "application/json;odata=verbose",
                    'X-RequestDigest': $('#__REQUESTDIGEST').val()
                },
                success: function (responseData) {

                },
                error: _funcRequestFailed
            });

        }
        self._funcFillQuickLinks();

    };

    self.isTaskSelected = function (task) {

        return task === self.selectedTask();
    };
    self._isExternal = function () {
        var _oisExternal = false;
        jQuery.support.cors = true;
        $.ajax({
            url: 'http://nicsdev.viacom.com/api/values',
            type: 'GET',
            dataType: 'jsonp',
            success: function (data) {
                //console.log(data);
                _oisExternal = data
            },
            error: function (x, y, z) {
                //console.log(JSON.stringify(x) + '\n' + JSON.stringify(y) + '\n' + z);
            }
        });
        return _oisExternal;

    }

}
ko.bindingHandlers.visibleAndSelect = {
    update: function (element, valueAccessor) {

        ko.bindingHandlers.visible.update(element, valueAccessor);
        if (valueAccessor()) {
            setTimeout(function () {
                $(element).find("input").focus().select();
            }, 0); //new tasks are not in DOM yet
        }
    }
};