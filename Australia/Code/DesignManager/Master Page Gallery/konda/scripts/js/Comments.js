﻿var parentsite = '';
var relativeURL;
var clientContext;
var userComments;
var repliedComment='';
var listID;
var web;
var currentUser;
var loginUserId;
var commentedPictureURL;
var recursiveFlag = true;
var UserFollow=false;

$(document).ready(function() {

    relativeURL = window.location.href;
    if( relativeURL.indexOf("Auto") > -1 )
    {
    	relativeURL = relativeURL.replace("?Auto=1","");	// On click on Video play button, comments were not loaded due to incorrect URL
    }
	
    var urlParts = relativeURL.split('/');

    for (var i = 0; i < urlParts.length-2; i++) {
        parentsite += urlParts[i] + '/';
    }
	/*Modified by robin*/	
//	DisplayComments("All Comments", parentsite, "<View><Query><Where><And><Neq><FieldRef Name=\'ReportAbuse\'/><Value Type=\'Text\'>1</Value></Neq><Eq><FieldRef Name=\'Title\'/><Value Type=\'Text\'>" + window.location.href + "</Value></Eq></And></Where><OrderBy><FieldRef Name=\'Created\' Ascending=\'True\'/></OrderBy></Query></View>");
	if(relativeURL.toLowerCase().indexOf(_spPageContextInfo.siteAbsoluteUrl.toLowerCase()) > -1)
	{
		relativeURL = relativeURL.toLowerCase().split(_spPageContextInfo.siteAbsoluteUrl.toLowerCase())[1];
	}
});

/*Modified by robin*/	
$(window).load(function () {
		DisplayComments("All Comments", parentsite, "<View><Query><Where><And><Neq><FieldRef Name=\'ReportAbuse\'/><Value Type=\'Text\'>1</Value></Neq><And><Neq><FieldRef Name=\'Deleted\'/><Value Type=\'Text\'>1</Value></Neq><Eq><FieldRef Name=\'Title\'/><Value Type=\'Text\'>" + relativeURL + "</Value></Eq></And></And></Where><OrderBy><FieldRef Name=\'Created\' Ascending=\'True\'/></OrderBy></Query></View>");
});
	/*Modified by robin*/
/*----------------------------------------------------------------------------------------------------------------*/
/*---------------------------This function is used to Display the PAGINATION Control Panel------------------------*/

function PaginationPanel() {
    $('#pagination-demo').twbsPagination({
        totalPages: 5,
        visiblePages: 3,
        onPageClick: function(event, page) {

            $('.content-pagination').hide();
            $('.page-' + page + '').show();
        }
    });
}

/*----------------------------------------------------------------------------------------------------------------*/
/*--------------------This function is used to check validation if comment field is/is not empty------------------*/

function PostComment() {
    userComments = $('#CommentArea').val();

    if (userComments == null || userComments == '' || userComments == 'Type your comment here...') {
	alert("Comments field cannot be left blank!");
	} else {
        createComment(parentsite, relativeURL, userComments, null);
    }
}

/*----------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------This function is used to POST A COMMENT------------------------------------*/

function createComment(context, relativeURL, userComments, ReplyLookup) {
    clientContext = new SP.ClientContext(context);
    var oList = clientContext.get_web().get_lists().getByTitle('Comments');
    var itemCreateInfo = new SP.ListItemCreationInformation();
    this.oListItem = oList.addItem(itemCreateInfo);

    oListItem.set_item('Title', relativeURL);
    oListItem.set_item('_Comments', userComments);
    oListItem.set_item('ArticleTitle', $('.page-header h1').text().trim());
    oListItem.set_item('ThumbnailImage', $('#greenroom-page-content').find('img').attr('src'));
    oListItem.set_item('ReplyLookup', ReplyLookup);

    oListItem.update();
    clientContext.load(oListItem);
    clientContext.executeQueryAsync(Function.createDelegate(this, this.onCommentQuerySucceeded), Function.createDelegate(this, this.onCommentQueryFailed));
}

/*---------- ON SUCCESS --------------*/

function onCommentQuerySucceeded() {
    $('#CommentArea').val('');
    location.reload();
}

/*---------- ON FAILURE --------------*/

function onCommentQueryFailed(sender, args) {
}

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------This function is used to Display the COMMENTS---------------------------------*/

function DisplayComments(header, context, query) {

	if( query.indexOf("Auto") > -1 )
	{
		query = query.replace("?Auto=1","");	// On click on Video play button, comments were not loaded due to incorrect URL
	}
    clientContext = new SP.ClientContext(context);
    currentUser = clientContext.get_web().get_currentUser();
    oList = clientContext.get_web().get_lists().getByTitle('Comments');
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml(query);
    this.ShowNamecollListItem = oList.getItems(camlQuery);
    clientContext.load(oList);
    clientContext.load(ShowNamecollListItem);
    clientContext.load(currentUser);

    clientContext.executeQueryAsync(Function.createDelegate(this, function() {
        onDisplayCommentsSucceeded(header);
    }), Function.createDelegate(this, this.onDisplayCommentsFailed));
}

/*---------- ON SUCCESS --------------*/

function onDisplayCommentsSucceeded(header) {
    var i = 1;
    var j = 1;
    var serialNumber = 1;
    var CommentsSection = '';
	var commentCount ='';
	if (header == 'All Comments') {
		commentCount = ' (' + ShowNamecollListItem.get_count() + ')';
	}
    var topCommentsHeader = '<div id="greenroom-article-comments-header"><h1>' + header + commentCount + '</h1></div><br />';
	/*('#greenroom-top-comments-sidebar').hide();  *//*Commenting for bug 526 */

    var listItemEnumerator = ShowNamecollListItem.getEnumerator();
    listID = oList.get_id().toString();
    loginUserId = currentUser.get_id();

    while (listItemEnumerator.moveNext()) {
        var oListItem = listItemEnumerator.get_current();
        var userName = oListItem.get_fieldValues().Author.get_lookupValue();
        var formatUserName = '';
        formatUserName = userName;
        if( userName.indexOf(",") > -1 )	//This condition is added as the Usernames have ',' while test accounts do not...
        {
        	formatUserName = userName.split(', ')[1] + ' ' + userName.split(', ')[0];
        }
        var userId = oListItem.get_fieldValues().Author.$1E_1;

        getProfilePictureURL(userId);

        var listItemID = oListItem.get_fieldValues().ID;
        var userComment = oListItem.get_fieldValues()._Comments;
		userComment=	userComment.split('\n').join('<br />');
        var commentedDate;
        var localCreatedDate =new Date(oListItem.get_fieldValues().Created);
        var today = new Date();
        if(localCreatedDate.setHours(0,0,0,0) == today.setHours(0,0,0,0))
        {
            //Date equals today's date
            commentedDate = oListItem.get_fieldValues().Created.format('ddd, MMM dd, yyyy' + ' @ ' + 'HH:mm tt');
            var indexOfAt = commentedDate.toString().indexOf("@");
            commentedDate = commentedDate.toString().substring(indexOfAt, commentedDate.toString().length);
            commentedDate = "Today " + commentedDate;
        }
        else if(localCreatedDate.setDate(localCreatedDate.getDate()+1) == today.setHours(0,0,0,0)){
            commentedDate = oListItem.get_fieldValues().Created.format('ddd, MMM dd, yyyy' + ' @ ' + 'HH:mm tt');
            var indexOfAt = commentedDate.toString().indexOf("@");
            commentedDate = commentedDate.toString().substring(indexOfAt, commentedDate.toString().length);
            commentedDate = "Yesterday " + commentedDate;
        }
        else{
            commentedDate = oListItem.get_fieldValues().Created.format('ddd, MMM dd, yyyy' + ' @ ' + 'HH:mm tt');
        }

        var _pagingclass;
        var EditDeleteButtons = '';
        var allCommentsHeader = '';
        var followusername = '';

        if (loginUserId == userId) {
            EditDeleteButtons = '<div id="greenroom-article-comments-edit"><a title="Delete comment" onclick="DeleteComment(' + listItemID + ')"><img alt="Delete comment" height="15" width="15" src="/SiteCollectionImages/pagelayouts/delete.png" /></a></div>' +
                '<div id="greenroom-article-comments-edit"><a title="Edit comment" onclick="EditComment(' + listItemID + ')"><img alt="Edit comment" height="15" width="15" src="/SiteCollectionImages/pagelayouts/edit.png" /></a></div>';
        }
		
        if (header == 'All Comments') {
            if (j % 50 == 0) //Modify here to Display number of comments on pagination
            {
                i++;
                _pagingclass = "content-pagination page-" + i;
            } else if (j % 50 != 0) //Modify here to Display number of comments on pagination
            {
                _pagingclass = "content-pagination page-" + i;
            }
        } else {
            _pagingclass = "";
        }
		
		if (loginUserId != userId) {

			
			var formatPostAuthor = PostAuthor.replace("\\","\\\\");
			var followText=""
			IsFollowed(PostAuthor);
			if(UserFollow)
			{
				followText="Stop following ";
			}
			else
			{
				followText="Follow ";
			}
			followusername = '<a class="follow" style="cursor:pointer" onclick="follow.FollowPerson(\''+formatPostAuthor+'\');">'+followText + userName+'</a>';

		}
		var lookUpId="";
		if ( oListItem.get_fieldValues().ReplyLookup != null )
        {
			lookUpId = oListItem.get_fieldValues().ReplyLookup.$1E_1;
		}
        CommentsSection += '<div id="greenroom-article-comments-container" class="' + _pagingclass + '">' +

            '<div id="greenroom-article-comments-userimage"><img class="circular" src="' + commentedPictureURL + '" /></div>' +

            '<div id="greenroom-article-usercomments-container">' +
            '<div id="greenroom-comments-username">' + formatUserName + '</div><div id="greenroom-article-comments-number">#' + serialNumber + '</div>' +

            '<div id="Reply'+listItemID+'" class="ReplyId" lookUpId="'+lookUpId+'"></div><br /><div id="greenroom-article-comments-edit-container">' +
            '<div style="display:none" id="divCommentTextArea' + listItemID + '">' +
            '<textarea id="CommentTextArea' + listItemID + '" name="Comment" cols="40" rows="2">' + userComment + '</textarea>' +
            '<a onclick="saveModifiedComment(' + listItemID + ')"><input type="submit" value="Edit Post" class="btn" name="greenroom-article-comments-edit-post-button" id="greenroom-article-comments-edit-post-button"></a>' +
            '</div></div>' +

            '<div id="greenroom-article-comments-usercomment"><div id="divCommentDisplayArea' + listItemID + '">' + userComment + '</div></div>' +
            
            '<div style="display:none" id="divReplyTextArea' + listItemID + '">' +
            '<textarea id="ReplyTextArea' + listItemID + '" name="Reply" cols="40" rows="2"></textarea>' +
            '<a onclick="saveRepliedComment(' + listItemID + ')"><input type="button" value="Reply" class="btn" ></a>' +	//type="submit"
            '</div>' +

            EditDeleteButtons +

            '<div>' +
            '<div id="greenroom-article-comments-timestamp">' + commentedDate + ' &nbsp;| &nbsp;</div> <div id="greenroom-article-comments-follow"> '+followusername+' </div>' +
            '<div id="greenroom-article-comments-report">&nbsp;<a onclick="reportAbuse(\'' + listItemID + '\',\'ReportAbuse\',\'1\')">Report</a> &nbsp;&nbsp;|&nbsp;&nbsp; </div> ' +
            '<div id="greenroom-article-comments-report" class="checkReply'+listItemID+'">&nbsp;<a onclick="ReplyComment(' + listItemID +')">Reply</a> &nbsp;&nbsp;|&nbsp;&nbsp; </div> ' +
            '<div id="greenroom-article-comments-like"> <span class="CDTlikecount' + listID + listItemID + '"></span><a href="#" id="greenroom-article-comments-likes-text" onclick="CDTLikePage(' + listItemID + ')" class="CDTLikeButton' + listID + listItemID + '"><span id="cswp-like-text">Like</span></a></div>' +
            '</div><br /><hr/></div></div>';

        CommentsDisplayTemplateGetLikeCount(listItemID);
        
        var rightSideComment = header;
        //Moved this section below to handle the quoting in Mozilla
		//Changed the logic a bit as well.
        //if ( oListItem.get_fieldValues().ReplyLookup != null )
        //{
        //	getRepliedComment(listItemID,oListItem.get_fieldValues().ReplyLookup.$1E_1)
        //}
        
        if (( header == 'Top Comments' && j==1 ) || ( header == 'All Comments' && j == ShowNamecollListItem.get_count() ) )
        {
        	var section= ''
        	if ( header == 'All Comments' )
        	{
        		rightSideComment = 'Recent Comment';
        		section ='greenroom-all-comments';
        	}        	
        	else
        	{
        		rightSideComment = 'Top Comment';
				section ='greenroom-top-comments';
        	}
        	//Old Right Side Top Comment
        	//$('.greenroom-sidebar-top-comment').html('<div style="font-size:13pt;font-weight:bold;padding-bottom:5%">'+rightSideComment+'<span class="landing-page-view-all" style="float:right"><a href="#'+section+'">View All &gt;&gt;</a></span></div><div><div style="float:left" id="ProfilePicture"><img class="circular" src="' + commentedPictureURL + '" /></div><div style="padding-left:4%;"><div style="font-size:10pt;font-weight:bold">' + userName + '</div><div style="font-size:10pt">' + userComment + '</div><br /><div style="font-size:8pt;text-decoration:italics">' + commentedDate + '&nbsp;&nbsp; | &nbsp;&nbsp; <span class="CDTlikecount' + listID + listItemID + '"></span><a href="#" id="greenroom-article-comments-likes-text" onclick="CDTLikePage(' + listItemID + ')" class="CDTLikeButton' + listID + listItemID + '"></a>&nbsp;<img style="background-color:#4EB19F" src="/SiteCollectionImages/pagelayouts/greenroom-article-comments-likes.png" /></div></div></div>');
        	
        	//Updated Right Side Top Comment
        	//$('.greenroom-sidebar-top-comment').html('<div style="font-size:13pt;font-weight:bold;padding-bottom:5%">'+rightSideComment+'<span class="landing-page-view-all" style="float:right"><a href="#'+section+'">View All &gt;&gt;</a></span></div><div><div style="float:left; height: 165px;" id="ProfilePicture"><img class="circular" src="' + commentedPictureURL + '" /></div><div style="padding-left:4%;"><div style="font-size:10pt;font-weight:bold">' + userName + '</div><div style="font-size:10pt">' + userComment + '</div><br /><div id="greenroom-article-comments-sidebar-timestamp" style="font-size:7pt;text-decoration:italics">' + commentedDate + '&nbsp;&nbsp; | &nbsp;&nbsp; <span class="CDTlikecount' + listID + listItemID + '"></span><a href="#" id="greenroom-article-comments-likes-text" onclick="CDTLikePage(' + listItemID + ')" class="CDTLikeButton' + listID + listItemID + '"></a>&nbsp;<img src="/SiteCollectionImages/pagelayouts/greenroom-article-comments-likes.png" /></div><div id="greenroom-article-comments-follow">'+followusername+'</div></div></div>');
			//$('.greenroom-sidebar-top-comment').html('<div style="font-size:13pt;font-weight:bold;padding-bottom:5%"><h2 class="ms-webpart-titleText">'+rightSideComment+'</h2><span class="landing-page-view-all" style="float:right"><a href="#'+section+'">View All &gt;&gt;</a></span></div><div><div style="float:left; height: 165px;" id="ProfilePicture"><img class="circular" src="' + commentedPictureURL + '" /></div><div style="padding-left:4%;"><div style="font-size:10pt;font-weight:bold">' + userName + '</div><div style="font-size:10pt">' + userComment + '</div><br /><div id="greenroom-article-comments-sidebar-timestamp" style="font-size:7pt;text-decoration:italics">' + commentedDate + '&nbsp;&nbsp; | &nbsp;&nbsp; <span class="CDTlikecount' + listID + listItemID + '"></span><a href="#" id="greenroom-article-comments-likes-text" onclick="CDTLikePage(' + listItemID + ')" class="CDTLikeButton' + listID + listItemID + '"></a>&nbsp;<img src="/SiteCollectionImages/pagelayouts/greenroom-article-comments-likes.png" /></div><div id="greenroom-article-comments-follow">Follow Username</div></div></div>');        	
        	
			//Fixes for bug ID 336
			$('.greenroom-sidebar-top-comment').html('<div style="font-size:13pt;font-weight:bold;padding-bottom:5%"><span style="font: 20px ProximaNova-Light,Helvetica Neue,Helvetica,Arial !important">'+rightSideComment+'</span><span class="landing-page-view-all" style="float:right; margin-top: 6px;"><b>'+ShowNamecollListItem.get_count()+'</b> comments ></span></div><span style="float:left; height: 165px;" id="ProfilePicture"><img class="circular" src="' + commentedPictureURL + '" /></span><div style="padding-left:4%;"><span style="font-size:10pt;font-weight:bold">' + formatUserName + '</span><div class="comment-desc"><span style="font-size:10pt">' + userComment + '</span></div><div class="comment-btn"><a class="btn btn-primary" href="#ReplyTextArea' + listItemID + '" onclick="ReplyComment(' + listItemID +')">Reply</a></div><div class="comment-day"><span id="greenroom-article-comments-sidebar-timestamp" style="font-size:7pt;text-decoration:italics">' + commentedDate + '&nbsp;&nbsp; | &nbsp;&nbsp;</span><span class="CDTlikecount' + listID + listItemID + '"></span><a href="#" id="greenroom-article-comments-likes-text" onclick="CDTLikePage(' + listItemID + ')" class="CDTLikeButton' + listID + listItemID + '"></a></div><div id="greenroom-article-comments-follow">'+followusername+'</div></div>')
        }

        j++;
        serialNumber++;
    }
    var contentArea = 'DisplayTopComments';
    if (header == 'All Comments' && recursiveFlag == true) {
        recursiveFlag = false;
        contentArea = 'DisplayAllComments';
        
        if ( j > 50 ) // This number is used to Hide/Show Pagination panel on the basis of number of items... ( It should be same as the number of items displayed/pagination )
        {
        	PaginationPanel();
		}

        //----------------- Get current user Picture URL to be displayed on the 'POST COMMENT' section...
        getProfilePictureURL(loginUserId);
        $('#CommenterPictureURL').append('<img class="circular" style="width:60px!important;height:60px!important;border-radius:60px!important;-webkit-border-radius:60px!important;-moz-border-radius: 60px!important;margin:0px 10px 0px 0px;" src="' + commentedPictureURL + '" />');
        //-----------------	
        //DisplayComments("Top Comments", parentsite, "<View><Query><Where><And><Neq><FieldRef Name=\'ReportAbuse\'/><Value Type=\'Text\'>1</Value></Neq><Eq><FieldRef Name=\'Title\'/><Value Type=\'Text\'>" + window.location.href + "</Value></Eq></And></Where><OrderBy><FieldRef Name=\'LikesCount\' Ascending=\'False\'/></OrderBy></Query><RowLimit>3</RowLimit></View>");
        DisplayComments("Top Comments", parentsite, "<View><Query><Where><And><And><Neq><FieldRef Name=\'ReportAbuse\'/><Value Type=\'Text\'>1</Value></Neq><Gt><FieldRef Name=\'LikesCount\'/><Value Type=\'Text\'>0</Value></Gt></And><And><Neq><FieldRef Name=\'Deleted\'/><Value Type=\'Text\'>1</Value></Neq><Eq><FieldRef Name=\'Title\'/><Value Type=\'Text\'>" + relativeURL + "</Value></Eq></And></And></Where><OrderBy><FieldRef Name=\'LikesCount\' Ascending=\'False\'/></OrderBy></Query><RowLimit>3</RowLimit></View>");
    }

    if ( j > 1 ) // This number is used to Hide/Show comments sections
    {
	    $('#' + contentArea).append(topCommentsHeader + CommentsSection);
		$('#greenroom-top-comments-sidebar').show();
	}

	$('.content-pagination').hide();
	$('.page-1').show();  
	
	
    if( ( $('.greenroom-sidebar-top-comment').text().trim() == "" ) || ( $('.greenroom-sidebar-top-comment').text().trim() == null) )
    {
    	$('.greenroom-sidebar-top-comment').append('There are no comments to be displayed. To add, click on "Add A Comment"');
    }
    
     $('a[href*=#]').click(	    
		function (e) {					
		    // Disable default click event and scrolling
		    e.preventDefault();
		    var hash = $(this).attr('href');
		    // Scroll to
		   $("#s4-workspace").scrollTo(hash,{duration:'slow'});
		}
	)
	
	//Added this to handle quoting of replied comments in mozilla
	$('.ReplyId').each(function(){
		var replyItemId = $(this).attr("id");
		var itemLookUpId = $(this).attr("lookUpId");
		var listItemID;
		var replyItemIdParts = replyItemId.split("Reply");
		if(replyItemIdParts != null && replyItemIdParts != undefined)
		{
			if(replyItemIdParts.length > 1)
			{
				listItemID = replyItemIdParts[1];
			}
		}
		if(listItemID != null && listItemID != undefined && listItemID != "" && itemLookUpId != null && itemLookUpId != undefined && itemLookUpId != "")
		{
			getRepliedComment(listItemID,itemLookUpId);
		}
	});
	

}

/*---------- ON FAILURE --------------*/

function onDisplayCommentsFailed(sender, args) {
}

/*----------------------------------------------------------------------------------------------------------------*/
/*-----------------------------------This function is used to DELETE A COMMENT------------------------------------*/

function DeleteComment(itemId) {
    if (confirm('Are you sure you want to delete this comment?')) {
    	/*
        var clientContext = new SP.ClientContext(parentsite);
        var oList = clientContext.get_web().get_lists().getByTitle('Comments');
        this.oListItem = oList.getItemById(itemId);
		var replyQuery = "<View><Query><Where><Eq><FieldRef Name='ReplyLookup' LookupId='TRUE' /><Value Type='Lookup'>" + itemId + "</Value></Eq></View></Query></Where>";
		var replyCamlQuery = new SP.CamlQuery();
		replyCamlQuery.set_viewXml(replyQuery);
		this.replyListItems = oList.getItems(replyCamlQuery);
        clientContext.load(replyListItems);
        oListItem.deleteObject();
		
        clientContext.executeQueryAsync(Function.createDelegate(this, this.onDeleteCommentSucceeded), Function.createDelegate(this, this.onDeleteCommentFailed));
        */
        
		var clientContext = new SP.ClientContext(parentsite);
		var oList = clientContext.get_web().get_lists().getByTitle('Comments');
		this.oListItem = oList.getItemById(itemId);
		oListItem.set_item('Deleted', '1');
		oListItem.update();
		clientContext.executeQueryAsync(Function.createDelegate(this, this.onDeleteCommentSucceeded), Function.createDelegate(this, this.onDeleteCommentFailed));        
        
    } else {

    }
}

/*---------- ON SUCCESS --------------*/

function onDeleteCommentSucceeded() {
    alert('Your comment has been rolled up and thrown in the recycle bin.');
    location.reload();
}

/*---------- ON FAILURE --------------*/

function onDeleteCommentFailed(sender, args) {
}
/*----------------------------------------------------------------------------------------------------------------*/
/*-----------------------------------------------Delete Replies---------------------------------------------------*/

/*----------------------------------------------------------------------------------------------------------------*/
/*--------------------------------This function is used to EDIT/REPLY A COMMENT-----------------------------------*/

function EditComment(itemID) {
    $('#divReplyTextArea' + itemID + '').hide();
    $('#divCommentTextArea' + itemID + '').show();
    $('#divCommentDisplayArea' + itemID + '').hide();
}

function saveModifiedComment(itemID) {
    userComments = $('#CommentTextArea' + itemID + '').val();
    updateCommentListItem(itemID, '_Comments', userComments);

    $('#divCommentTextArea' + itemID + '').hide();
    $('#divCommentDisplayArea' + itemID + '').html(userComments);
    $('#divCommentDisplayArea' + itemID + '').show();
}

function ReplyComment(itemID) {
    $('#divCommentTextArea' + itemID + '').hide();
    $('#divCommentDisplayArea' + itemID + '').show();    
    $('#divReplyTextArea' + itemID + '').show();
}

function saveRepliedComment(currentItemID){
	repliedComment = $('#ReplyTextArea' + currentItemID + '').val();
	
	//createComment(parentsite, relativeURL, repliedComment, currentItemID);
    //$('#divCommentTextArea' + itemID + '').hide();
    
	if (repliedComment == null || repliedComment == '' ) {
	alert("Comments field cannot be left blank!");
	return false;
	} else {
        createComment(parentsite, relativeURL, repliedComment, currentItemID);
        $('#divCommentTextArea' + itemID + '').hide();
    }
}

function reportAbuse(itemID, fieldname, fieldvalue){

	ReportAbusedParentAndChildComments(parentsite,itemID);
}

/*----------------------------------------------------------------------------------------------------------------*/
/*---------------------------------This function is used to "UPDATE" a comment------------------------------------*/

function updateCommentListItem(itemID, fieldname, fieldvalue) {
    var clientContext = new SP.ClientContext(parentsite);
    var oList = clientContext.get_web().get_lists().getByTitle('Comments');
    this.oListItem = oList.getItemById(itemID);
    oListItem.set_item(fieldname, fieldvalue);
    oListItem.update();
    clientContext.executeQueryAsync(Function.createDelegate(this, this.onUpdateCommentListItemSucceeded), Function.createDelegate(this, this.onUpdateCommentListItemFailed));
     alert('Your comments has been modified.');

}

function onUpdateCommentListItemSucceeded() {
   // alert('Your comments has been modified.');
    //location.reload();
}

function onUpdateCommentListItemFailed(sender, args) {
}

/*----------------------------------------------------------------------------------------------------------------*/
/*------------------------------This function is used to "REPORT ABUSE" a comment---------------------------------*/

function ReportAbusedParentAndChildComments(context,itemID)
{
    clientContext = new SP.ClientContext(context);
    oList = clientContext.get_web().get_lists().getByTitle('Comments');
    var camlQuery = new SP.CamlQuery();
    
    //This query is used to fetch the parent comment as well as the child ( replied ) comments.
    //camlQuery.set_viewXml("<View><Query><ViewFields><FieldRef Name='ID' /><FieldRef Name='ReplyLookup' /><FieldRef Name='ReportAbuse' /></ViewFields><Where><Or><Eq><FieldRef Name='ID' /><Value Type='Counter'>"+itemID+"</Value></Eq><Eq><FieldRef Name='ReplyLookup' /><Value Type='Lookup'>"+itemID+"</Value></Eq></Or></Where></Query></View>");
    
    camlQuery.set_viewXml("<View><Query><ViewFields><FieldRef Name='ID' /><FieldRef Name='ReplyLookup' /><FieldRef Name='ReportAbuse' /></ViewFields><Where><Eq><FieldRef Name='ID' /><Value Type='Counter'>"+itemID+"</Value></Eq></Where></Query></View>");
    this.ShowNamecollListItem = oList.getItems(camlQuery);
    clientContext.load(oList);
    clientContext.load(ShowNamecollListItem);
	clientContext.executeQueryAsync(Function.createDelegate(this, this.onReportAbusedParentAndChildCommentsSucceeded), Function.createDelegate(this, this.onReportAbusedParentAndChildCommentsFailed));	
}

function onReportAbusedParentAndChildCommentsSucceeded()
{
    var listItemEnumerator = ShowNamecollListItem.getEnumerator();

    while (listItemEnumerator.moveNext()) {
    var oListItem = listItemEnumerator.get_current();
    var getChildItemID = oListItem.get_fieldValues().ID;
    	updateCommentListItem(getChildItemID, 'ReportAbuse', '1')
	}
	
	alert('Thank you for reporting this comment. It has been removed and is under review by the Greenroom editorial team.');
	location.reload();	
}

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------CUSTOM CODE FOR LIKING A COMMENT -----------------------------------------*/

function CommentsDisplayTemplateGetLikeCount(listItemID) {
    var context = new SP.ClientContext(parentsite);
    var list = context.get_web().get_lists().getByTitle('Comments');
    var item = list.getItemById(listItemID);

    context.load(item, "LikedBy", "ID", "LikesCount");
    context.executeQueryAsync(Function.createDelegate(this, function(success) {
        // Check if the user id of the current users is in the collection LikedBy. 
        var likeDisplay = true;
        var $v_0 = item.get_item('LikedBy');
        var itemc = item.get_item('LikesCount');
        if (!SP.ScriptHelpers.isNullOrUndefined($v_0)) {
            for (var $v_1 = 0, $v_2 = $v_0.length; $v_1 < $v_2; $v_1++) {
                var $v_3 = $v_0[$v_1];
                if ($v_3.$1E_1 === _spPageContextInfo.userId) {
                    //cb(true, item.get_item('LikesCount'));
                    //alert("Liked by me");
                    likeDisplay = false;
                }
            }
        }
        //CDTChangeLikeText(likeDisplay, itemc, listItemID);
        
        setTimeout(function(){         
        CDTChangeLikeText(likeDisplay, itemc, listItemID); }, 1000);

    }), Function.createDelegate(this, function(sender, args) {
    }));
}

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

function CDTChangeLikeText(like, count, listItemID) {
    if (like) {
        $(".CDTLikeButton" + listID + listItemID).html('<span id="cswp-like-text">Like</span>');
    } else {
        $(".CDTLikeButton" + listID + listItemID).html('<span id="cswp-like-text">Unlike</span>');
    }
    var htmlstring = String(count);
    if (count > 0)
        $(".CDTlikecount" + listID + listItemID).html(htmlstring )
    else
        $(".CDTlikecount" + listID + listItemID).html(String(0) + ' ' );
}

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

function CDTLikePage(listItemID) {

    var like = false;
    var likeButtonText = $(".CDTLikeButton" + listID + listItemID).text();
    if (likeButtonText != "") {
        if (likeButtonText == "Like" || likeButtonText == "LikeLike" || likeButtonText == "LikeLikeLike")
            like = true;

        var aContextObject = new SP.ClientContext(parentsite);
        EnsureScriptFunc('reputation.js', 'Microsoft.Office.Server.ReputationModel.Reputation', function() {
            Microsoft.Office.Server.ReputationModel.
            Reputation.setLike(aContextObject,
                //listID.substring(1, 37),
                listID,
                listItemID, like);

            aContextObject.executeQueryAsync(
                function() {
                    //alert(String(like));
                    CommentsDisplayTemplateGetLikeCount(listItemID);
                }, function(sender, args) {
                    //alert('F0');
                });
        });
    }
}

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/
var PostAuthor;

function getProfilePictureURL(userid) {
    var call = $.ajax({
        url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/GetUserById(" + userid + ")/LoginName", //+
        //"&$select=PictureUrl"+
        //"&$expand=CreatedBy"+
        //"&$top=20",
        async: false,
        type: "GET",
        dataType: "json",
        headers: {
            Accept: "application/json;odata=verbose"
        }
    });

    call.done(function(data, textStatus, jqXHR) {
        try {

            PostAuthor = data.d.LoginName.split('|')[1];
            getCommenterPictureURL(PostAuthor);
        } catch (e) {
        }
    });

    call.fail(function(jqXHR, textStatus, errorThrown) {
    });
}
/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

function getCommenterPictureURL(PostAuthor) {
    var call = $.ajax({
        url: _spPageContextInfo.siteAbsoluteUrl + "/_api/SP.UserProfiles.PeopleManager/GetPropertiesFor(accountName=@v)?@v='" + PostAuthor + "'" +
            "&$select=PictureUrl" +
            "&$filter=DirectReports", //+
        //"&$top=20",
        async: false,
        type: "GET",
        dataType: "json",
        headers: {
            Accept: "application/json;odata=verbose"
        }
    });

    call.done(function(data, textStatus, jqXHR) {
        try {
            var ProfilePicture = data.d.PictureUrl;

            if (ProfilePicture == null) {
                commentedPictureURL = '_layouts/15/images/Person.gif';
            } else {
                commentedPictureURL = ProfilePicture;
            }
        } catch (e) {
            //alert(e);
        }
    });

    call.fail(function(jqXHR, textStatus, errorThrown) {
        //alert("Error getting Profile Details" + jqXHR.responseText);
    });
}

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/
/* ------------ FIRST LEVEL OF REPLY ONLY --------------
var repliedComment;

function getRepliedComment(currentItemID,repliedCommentID)
{
	var call = $.ajax({
			url: _spPageContextInfo.webServerRelativeUrl+"/_api/lists/getbytitle('Comments')/items?"+
					"&$select=Id,OData__Comments,ReportAbuse"+
					"&$filter=Id eq '"+repliedCommentID+"'",//+
					//"&$top=20",
			//async:false,
			type: "GET",
			dataType: "json",
			headers: {
			Accept: "application/json;odata=verbose"
			}
		});
	
	call.done(function (data,textStatus, jqXHR){
		try{
			repliedComment = data.d.results[0].OData__Comments;
			var commentAvailable = data.d.results[0].ReportAbuse;
			if ( commentAvailable == true )
			{
				$('#Reply'+currentItemID).html('<div style="background-color:white;width:50%;height:100px;vertical-align:middle;text-align:left;" >" This comment is currently unavailable as it has been report abused."</div>')	
			}
			else
			{
				$('#Reply'+currentItemID).html('<div style="background-color:white;width:50%;height:100px;vertical-align:middle;text-align:left;" >"'+repliedComment+'"</div>')
			}
			$('.checkReply'+currentItemID).hide();
		   }
		catch(e){
			//alert(e);
		}
	});
	
	call.fail(function (jqXHR,textStatus,errorThrown){
		//alert("Error getting Site Color" + jqXHR.responseText);
	});
}
*/

var count = 0;

function getRepliedComment(currentItemID,repliedCommentID)
{
	var call = $.ajax({
			url: _spPageContextInfo.webServerRelativeUrl+"/_api/lists/getbytitle('Comments')/items?"+
					"&$select=Id,OData__Comments,ReportAbuse,ReplyLookupId,AuthorId,Deleted"+
					"&$filter=Id eq '"+repliedCommentID+"'",//+
					//"&$top=20",
			//async:false,
			type: "GET",
			dataType: "json",
			headers: {
			Accept: "application/json;odata=verbose"
			}
		});
	
	call.done(function (data,textStatus, jqXHR){
	if(data.d.results != null && data.d.results.length >0)
		{
		try{
		
		getOriginalCommenterName(data.d.results[0].AuthorId);
		
        if(data.d.results[0].ReplyLookupId == null)
        {
        	repliedComment = '';
        }	    
	
		if(repliedComment == '')
		{
			 repliedComment = "<strong>"+originalCommenterName+"</strong></br>"+data.d.results[0].OData__Comments;
	    }
	    else
	    {
			repliedComment += "</br><strong>"+originalCommenterName+"</strong></br>"+data.d.results[0].OData__Comments;
	    }
	    
	    var commentAvailable = data.d.results[0].ReportAbuse;
	    var commentDeleted = data.d.results[0].Deleted;
			
			if ( commentAvailable == true || commentDeleted == true )
			{
				$('#Reply'+currentItemID).html('<blockquote>" This comment is currently unavailable."</blockquote>')	
			}
			else
			{
				$('#Reply'+currentItemID).html('<blockquote>'+repliedComment+'</blockquote>')
			}
			if ( count < 1 && data.d.results[0].ReplyLookupId != null )
		   {
				getRepliedComment(repliedCommentID,data.d.results[0].ReplyLookupId)
		   }
		   }
		catch(e){
			//alert(e);
		}
		count++;
		}
	});
	
	call.fail(function (jqXHR,textStatus,errorThrown){
		//console.log("Error getting Site Color" + jqXHR.responseText);
	});
}

/*----------------------------------------------------------------------------------------------------------------*/
/*--------------------- Function to get Original Commented User's Name (Reply Functionality) ---------------------*/

var originalCommenterName = '';

function getOriginalCommenterName(userid) {
    var call = $.ajax({
        url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/GetUserById(" + userid + ")", //+
        async: false,
        type: "GET",
        dataType: "json",
        headers: {
            Accept: "application/json;odata=verbose"
        }
    });

    call.done(function(data, textStatus, jqXHR) {
        try {
	        	var getOriginalCommenterName = data.d.Title;
	        	originalCommenterName = getOriginalCommenterName;
	        	if( getOriginalCommenterName.indexOf(",") > -1 )	//This condition is added as the Usernames have ',' while test accounts do not...
	        	{
	        		originalCommenterName = getOriginalCommenterName.split(', ')[1] + ' ' + getOriginalCommenterName.split(', ')[0];
	        	}
        } catch (e) {
        }
    });

    call.fail(function(jqXHR, textStatus, errorThrown) {
        //console.log("Error getting Profile Details" + jqXHR.responseText);
    });
}

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

	/*Modified by robin*/
var follow={
	clientContext :null,
	followingManager :null,
	actorInfo :null,
	isFollowed :null,
	followedCount :null,
	FollowPerson:function(targetUser){
		follow.clientContext = SP.ClientContext.get_current();
		follow.followingManager = new SP.Social.SocialFollowingManager(follow.clientContext);
		follow.actorInfo = new SP.Social.SocialActorInfo();
        follow.actorInfo.set_accountName(targetUser);
        follow.isFollowed = follow.followingManager.isFollowed(follow.actorInfo);        
		follow.clientContext.executeQueryAsync(follow.sucessPersonFollowed, follow.Failure);
	},
	IsFollowed:function(targetUser){
		var f=false;
		follow.clientContext = SP.ClientContext.get_current();
		follow.followingManager = new SP.Social.SocialFollowingManager(follow.clientContext);
		follow.actorInfo = new SP.Social.SocialActorInfo();		
        follow.actorInfo.set_accountName(targetUser);
        follow.isFollowed = follow.followingManager.isFollowed(follow.actorInfo); 
        follow.clientContext.executeQueryAsync( function() 
		{
			f=  follow.isFollowed.get_value();			
			UserFollow=f;
			alert(UserFollow);
		}, follow.Failure);		 			
	},	
	sucessPersonFollowed:function(){
		if (follow.isFollowed.get_value() === false) {
	        follow.followingManager.follow(follow.actorInfo);
	    }
	    else if (follow.isFollowed.get_value() === true) {
	        follow.followingManager.stopFollowing(follow.actorInfo);
	    }
		follow.clientContext.executeQueryAsync(follow.successToggle, follow.Failure);		
	},
	successToggle:function(){
		alert('Your request is completed.');
		location.reload();
	},
	Failure:function(sender,args){
 	}
}
	/*Modified by robin*/
/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

	/*Modified by robin*/
function IsFollowed(targetUser){
	var call =$.ajax({
	    url: _spPageContextInfo.siteAbsoluteUrl + "/_api/sp.userprofiles.peoplemanager/amifollowing(@v)?@v='"+targetUser+"'",
  async: false,
  type: "GET",
   headers: {
            Accept: "application/json;odata=verbose"
        }
 });
 call.done(function(data, textStatus, jqXHR) {
        try {
          	UserFollow=data.d.AmIFollowing;
        } 
		catch (e) {
        }
    });

    call.fail(function(jqXHR, textStatus, errorThrown) {
    });

 
}
	/*Modified by robin*/
/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/
