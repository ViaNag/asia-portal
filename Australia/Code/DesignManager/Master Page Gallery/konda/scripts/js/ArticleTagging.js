﻿var articleTagging = {
clientContext:null,
web:null,
pageName:getPageName(),
IsFolderExists:null,
listRootFolder:null,
camlQuery:null,
pageID:null,
libraryName:null,
checkAndCreateFolder:function(libraryName){
	articleTagging.libraryName = libraryName;
	articleTagging.IsFolderExists = false;
	articleTagging.clientContext = new SP.ClientContext.get_current();
	articleTagging.web = articleTagging.clientContext.get_web(); 
	oList = articleTagging.web.get_lists().getByTitle(libraryName); 
	articleTagging.clientContext.load(oList);
	articleTagging.listRootFolder= oList.get_rootFolder();
	articleTagging.clientContext.load(articleTagging.listRootFolder);
	articleTagging.camlQuery = new SP.CamlQuery();
	articleTagging.camlQuery.set_viewXml('<View><Query><Where><Eq><FieldRef Name="Title" /><Value Type="Text">'+articleTagging.pageName+'</Value></Eq></Where></Query><RowLimit>1</RowLimit></View>');
	collListItem = oList.getItems(articleTagging.camlQuery);
	articleTagging.clientContext.load(collListItem,'Include(Id)');
	articleTagging.clientContext.executeQueryAsync(articleTagging.successCheckAndCreateFolder, articleTagging.failure);
},
	
failure:function(sender,args){
	//console.log(args.get_message() + '\n' + args.get_stackTrace());
},

successCheckAndCreateFolder:function(){
	var listItemEnumerator = collListItem.getEnumerator();
	while (listItemEnumerator.moveNext()) 
	{
		articleTagging.IsFolderExists = true;
		if(articleTagging.libraryName=="Images")
		{
		articleTagging.imageLibraryDetails();
		}
		else
		{
			articleTagging.videoLibraryDetails();			
		}
		break;
	}
	if(!articleTagging.IsFolderExists)
	{
		articleTagging.createFolder(oList);
	}	
},
	
createFolder:function(oList){		
	var itemCreation = new SP.ListItemCreationInformation();
    itemCreation.set_underlyingObjectType(SP.FileSystemObjectType.folder);
    itemCreation.set_leafName(articleTagging.pageName);
    folderItem = oList.addItem(itemCreation);
	folderItem.set_item('Title',articleTagging.pageName);
    folderItem.update();
    articleTagging.clientContext.load(folderItem);
    articleTagging.clientContext.executeQueryAsync(articleTagging.successCreateFolder, articleTagging.failure);

},

successCreateFolder:function(){		
	if(articleTagging.libraryName=="Images")
	{
	articleTagging.imageLibraryDetails();
	}
	else
	{
		articleTagging.videoLibraryDetails();			
	}
},
	
imageLibraryDetails:function(){
	var imageLibrary = oList.get_id().toString();
	var imageLibraryUrl=articleTagging.listRootFolder.get_serverRelativeUrl();
	articleTagging.uploadImages(imageLibrary,imageLibraryUrl);
},	

videoLibraryDetails:function(){
	var videoLibrary = oList.get_id().toString();
	var videoLibraryUrl=articleTagging.listRootFolder.get_serverRelativeUrl();
	articleTagging.uploadVideos(videoLibrary ,videoLibraryUrl);
},
	
uploadVideos:function(videoLibrary ,videoLibraryUrl){
	//var uploadVideoUrl = _spPageContextInfo.siteAbsoluteUrl+_spPageContextInfo.webServerRelativeUrl+"/_layouts/15/Upload.aspx?List={"+videoLibrary +"}&RootFolder="+videoLibraryUrl+"%2F"+articleTagging.pageName;
	var uploadVideoUrl = _spPageContextInfo.webAbsoluteUrl +"/_layouts/15/NewVideoSet.aspx?List=" + videoLibrary + "&ContentTypeId=0x0120D520A80800F91E907DEEF33C4981E526771DBA125C&RootFolder="+videoLibraryUrl+"%2F"+articleTagging.pageName;
	articleTagging.OpenDialog(uploadVideoUrl,"650","450",false);	
	//OpenPopUpPageWithTitle(_spPageContextInfo.webServerRelativeUrl+'/Videos/'+articleTagging.pageName,null,null,null,'Upload Videos  '+articleTagging.pageName);
},

uploadImages:function(imageLibrary,imageLibraryUrl){
	/*var uploadImageUrl = _spPageContextInfo.siteAbsoluteUrl+_spPageContextInfo.webServerRelativeUrl+"/_layouts/15/Upload.aspx?List={"+imageLibrary+"}&RootFolder="+imageLibraryUrl+"%2F"+articleTagging.pageName;
	articleTagging.OpenDialog(uploadImageUrl,"650","450",false);*/
	OpenPopUpPageWithTitle(_spPageContextInfo.webServerRelativeUrl+'/PublishingImages/'+articleTagging.pageName,null,null,null,'Upload Images  '+articleTagging.pageName);
},

OpenDialog:function(url,width,height,alloMaximize){
		
	var options = SP.UI.$create_DialogOptions();
	options.url = url;
	options.width = width;
	options.height = height;
	options.allowMaximize = alloMaximize;
	SP.UI.ModalDialog.showModalDialog(options);
},

TagImages:function(libraryName){
	articleTagging.clientContext = new SP.ClientContext.get_current();
    articleTagging.web = articleTagging.clientContext.get_web();
    oList= articleTagging.web.get_lists().getByTitle(libraryName); 
    articleTagging.clientContext.load(oList);
    articleTagging.camlQuery = new SP.CamlQuery();
    //camlQuery.set_viewXml('<View><Query><Where><Eq><FieldRef Name="Title" /><Value Type="Text">' + pageName + '</Value></Eq></Where></Query><RowLimit>1</RowLimit></View>');
    //articleTagging.camlQuery.set_folderServerRelativeUrl(_spPageContextInfo.webServerRelativeUrl+'/PublishingImages/'+articleTagging.pageName );         
	if(libraryName=="Videos")
	{
		articleTagging.camlQuery.set_folderServerRelativeUrl(_spPageContextInfo.webServerRelativeUrl+'/Lists/Videos/'+articleTagging.pageName ); 
	}
	else if (libraryName=="Images")
	{
	articleTagging.camlQuery.set_folderServerRelativeUrl(_spPageContextInfo.webServerRelativeUrl+'/PublishingImages/'+articleTagging.pageName ); 
	}
    collListItem= oList.getItems(articleTagging.camlQuery);
    articleTagging.clientContext .load(collListItem,'Include(TaggedPage)');
    articleTagging.clientContext .executeQueryAsync(articleTagging.successTagImages, articleTagging.failure);	
},
	
successTagImages:function(){
	//articleTagging.getPageIdValue();
	listItemEnumerator= collListItem.getEnumerator();		
	 while (listItemEnumerator.moveNext()) 
	{
	      	articleTagging.clientContext = new SP.ClientContext.get_current();
            oListItemImages = listItemEnumerator.get_current();
            var lookupValue = new SP.FieldLookupValue();
            lookupValue.set_lookupId(_spPageContextInfo.pageItemId);
            oListItemImages.set_item('TaggedPage', lookupValue);
            oListItemImages .update();		       	                              
   }
	articleTagging.clientContext.executeQueryAsync(articleTagging.successImage,articleTagging.failure);  
},
	
successImage:function()	{
	alert(" Tagged successfully!!");
	articleTagging.refreshPage();
},
	
refreshPage:function(){
   window.location = window.location;
}

/*pageIdValue:function(){
	clientContextPages = new SP.ClientContext.get_current();
	webPages = clientContextPages .get_web(); 
	oListPages = webPages .get_lists().getByTitle("Pages"); 
	clientContextPages.load(oListPages );
	camlQuery = new SP.CamlQuery();
	camlQuery.set_viewXml('<View><Query><Where><Eq><FieldRef Name="Title" /><Value Type="Text">'+articleTagging.pageName+'</Value></Eq></Where></Query><RowLimit>1</RowLimit></View>');
	collListItemPages = oListPages .getItems(camlQuery);
	clientContextPages .load(collListItemPages);
	clientContextPages.executeQueryAsync(articleTagging.successPageIdValue, articleTagging.failure);		
},
	
successPageIdValue:function(){
	var listItemInfoPages = '';
    var listItemEnumeratorPages = collListItemPages.getEnumerator();
    while (listItemEnumeratorPages .moveNext())
	{
		oListItem = listItemEnumeratorPages .get_current();
		listItemInfoPages =  oListItem.get_item('ID');			
	}
	 articleTagging.pageID = listItemInfoPages ;
},*/
/*
getPageIdValue:function(){	
	    var call = $.ajax({
	        url: _spPageContextInfo.webServerRelativeUrl+ "/_api/lists/getbytitle('Pages')/items?" +
	            "$select=Title,Id" +
	            "&$filter=Title eq '" + articleTagging.pageName + "'"+
	            "&$top=1",
	        async: false,
	        type: "GET",
	        dataType: "json",
	        headers: {
	            Accept: "application/json;odata=verbose"
	        }
	    });		
	    call.done(function(data, textStatus, jqXHR) {
	        try {
	            	articleTagging.pageID = data.d.results[0].ID;
	        } catch (e) {}
	    });		
	    call.fail(function(jqXHR, textStatus, errorThrown) {
	        //alert("Error generating Report: " + jqXHR.responseText);
	    });
	}*/
}

$(document).ready(function()
{
	//articleTagging.pageName = getPageName();
	
});

function getPageName() {
	var location = document.location.href;
	var indexOfLastSlash = location.lastIndexOf("/");
	var indexOfLastDot = location.lastIndexOf(".");
	return location.substring(indexOfLastSlash+1, indexOfLastDot);
}
