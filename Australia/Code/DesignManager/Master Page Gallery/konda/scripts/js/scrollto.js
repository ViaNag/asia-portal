/*!
 * jQuery.scrollTo
 * Copyright (c) 2007-2014 Ariel Flesler - aflesler<a>gmail<d>com | http://flesler.blogspot.com
 * Licensed under MIT
 * http://flesler.blogspot.com/2007/10/jqueryscrollto.html
 * @projectDescription Easy element scrolling using jQuery.
 * @author Ariel Flesler
 * @version 1.4.13
 */
;(function (define) {
	'use strict';

	define(['jquery'], function ($) {

		var $scrollTo = $.scrollTo = function( target, duration, settings ) {
			return $(window).scrollTo( target, duration, settings );
		};

		$scrollTo.defaults = {
			axis:'xy',
			duration: parseFloat($.fn.jquery) >= 1.3 ? 0 : 1,
			limit:true
		};

		// Returns the element that needs to be animated to scroll the window.
		// Kept for backwards compatibility (specially for localScroll & serialScroll)
		$scrollTo.window = function( scope ) {
			return $(window)._scrollable();
		};

		// Hack, hack, hack :)
		// Returns the real elements to scroll (supports window/iframes, documents and regular nodes)
		$.fn._scrollable = function() {
			return this.map(function() {
				var elem = this,
					isWin = !elem.nodeName || $.inArray( elem.nodeName.toLowerCase(), ['iframe','#document','html','body'] ) != -1;

					if (!isWin)
						return elem;

				var doc = (elem.contentWindow || elem).document || elem.ownerDocument || elem;

				return /webkit/i.test(navigator.userAgent) || doc.compatMode == 'BackCompat' ?
					doc.body :
					doc.documentElement;
			});
		};

		$.fn.scrollTo = function( target, duration, settings ) {
			if (typeof duration == 'object') {
				settings = duration;
				duration = 0;
			}
			if (typeof settings == 'function')
				settings = { onAfter:settings };

			if (target == 'max')
				target = 9e9;

			settings = $.extend( {}, $scrollTo.defaults, settings );
			// Speed is still recognized for backwards compatibility
			duration = duration || settings.duration;
			// Make sure the settings are given right
			settings.queue = settings.queue && settings.axis.length > 1;

			if (settings.queue)
				// Let's keep the overall duration
				duration /= 2;
			settings.offset = both( settings.offset );
			settings.over = both( settings.over );

			return this._scrollable().each(function() {
				// Null target yields nothing, just like jQuery does
				if (target == null) return;

				var elem = this,
					$elem = $(elem),
					targ = target, toff, attr = {},
					win = $elem.is('html,body');

				switch (typeof targ) {
					// A number will pass the regex
					case 'number':
					case 'string':
						if (/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(targ)) {
							targ = both( targ );
							// We are done
							break;
						}
						// Relative/Absolute selector, no break!
						targ = win ? $(targ) : $(targ, this);
						if (!targ.length) return;
					case 'object':
						// DOMElement / jQuery
						if (targ.is || targ.style)
							// Get the real position of the target
							toff = (targ = $(targ)).offset();
				}

				var offset = $.isFunction(settings.offset) && settings.offset(elem, targ) || settings.offset;

				$.each( settings.axis.split(''), function( i, axis ) {
					var Pos	= axis == 'x' ? 'Left' : 'Top',
						pos = Pos.toLowerCase(),
						key = 'scroll' + Pos,
						old = elem[key],
						max = $scrollTo.max(elem, axis);

					if (toff) {// jQuery / DOMElement
						attr[key] = toff[pos] + ( win ? 0 : old - $elem.offset()[pos] );

						// If it's a dom element, reduce the margin
						if (settings.margin) {
							attr[key] -= parseInt(targ.css('margin'+Pos)) || 0;
							attr[key] -= parseInt(targ.css('border'+Pos+'Width')) || 0;
						}

						attr[key] += offset[pos] || 0;

						if(settings.over[pos])
							// Scroll to a fraction of its width/height
							attr[key] += targ[axis=='x'?'width':'height']() * settings.over[pos];
					} else {
						var val = targ[pos];
						// Handle percentage values
						attr[key] = val.slice && val.slice(-1) == '%' ?
							parseFloat(val) / 100 * max
							: val;
					}

					// Number or 'number'
					if (settings.limit && /^\d+$/.test(attr[key]))
						// Check the limits
						attr[key] = attr[key] <= 0 ? 0 : Math.min( attr[key], max );

					// Queueing axes
					if (!i && settings.queue) {
						// Don't waste time animating, if there's no need.
						if (old != attr[key])
							// Intermediate animation
							animate( settings.onAfterFirst );
						// Don't animate this axis again in the next iteration.
						delete attr[key];
					}
				});

				animate( settings.onAfter );

				function animate( callback ) {
					$elem.animate( attr, duration, settings.easing, callback && function() {
						callback.call(this, targ, settings);
					});
				}
			}).end();
		};

		// Max scrolling position, works on quirks mode
		// It only fails (not too badly) on IE, quirks mode.
		$scrollTo.max = function( elem, axis ) {
			var Dim = axis == 'x' ? 'Width' : 'Height',
				scroll = 'scroll'+Dim;

			if (!$(elem).is('html,body'))
				return elem[scroll] - $(elem)[Dim.toLowerCase()]();

			var size = 'client' + Dim,
				html = elem.ownerDocument.documentElement,
				body = elem.ownerDocument.body;

			return Math.max( html[scroll], body[scroll] ) - Math.min( html[size]  , body[size]   );
		};

		function both( val ) {
			return $.isFunction(val) || typeof val == 'object' ? val : { top:val, left:val };
		}

		// AMD requirement
		return $scrollTo;
	})
}(typeof define === 'function' && define.amd ? define : function (deps, factory) {
	if (typeof module !== 'undefined' && module.exports) {
		// Node
		module.exports = factory(require('jquery'));
	} else {
		factory(jQuery);
	}
}));

$( document ).ready(function() {

/* Back to top button */ 
$('.back-to-top-btn,.quick-link-responsive').click(function(){
		$('html,body').animate({
			scrollTop: 0
		},500);
});

/* Anchor Links Code */ 
$('.anchor-links li a').click(function(){
     var target_id = $(this).attr('href');
    jQuery('html,body').animate({
        scrollTop: parseInt(jQuery(target_id).offset().top - 100)
    },500);	
});

$('.admin-links-sidebar #greenroom-admin-block').remove();
$('.greenroom-admin-block').clone().appendTo('.admin-links-sidebar');

/* Search input toggle */
$( "#SerachBoxConatiner .search-button").on( "click", function() {
	$(this).toggleClass('active');
	$('#searchInputBox').toggle();
});

/* Quick Links Dropdown */
$('.quick-dropdown .toggle-btn').click(function(){	
	 if(jQuery(this).hasClass('active')){
            $(this).removeClass('active');
            $(this).parent('.quick-dropdown').find('ul').hide();
        }
        else
        {
            $('.quick-dropdown .toggle-btn').removeClass('active');
            $(this).addClass('active');
            $('.quick-dropdown ul').hide();
            $(this).parent('.quick-dropdown').find('ul').show();            
        }	
});

/* Advance Search Page */
if ($('.SearchContentBox div').hasClass('ms-searchCenter-advanced')){
  $('body').addClass('advance-search-page');
}

$('#greenroom-homepage-sidebar #greenroom-btn-holiday-schedule').parent('.ms-rtestate-field').addClass('holiday-schedule-container');

/* Fixed Header Code */ 
function scrollResponsive(){	
	if($( window ).width() <= 1024){
		var $top1= $('body').offset().top + 40;   
		$(window).scroll(function()
		{   
			if ($(window).scrollTop()>$top1)   
			{
			 $('#TopBar').addClass('fixed');
			}
			else
			{
			 $('#TopBar').removeClass('fixed');
			}    
		});

/* Back to Top scroll */			
	$('#back-to-top-container').on('click',function () {
		$('html,body').animate({
			scrollTop: 0
		},500);
	});	
			
	$('#QuickLinks').on('click',function () {
		$('html,body').animate({
			scrollTop: 0
		},500);
	});	
	}
}
scrollResponsive();
$( window ).resize(function() {
			scrollResponsive();
});	

/* Search Filter Code */ 
setTimeout(function(){
			$('.SearchContentBox #SearchBox').before('<span class="filter-icon"></span>');
			$('.SearchContentBox .filter-icon').click(function(){	
				$('.search-refiners').toggle();
				$('#s4-workspace').css('overflow','visible');
			});
},2000);


$('.search-refiners .refiner-close').click(function(){	
	$('.search-refiners').hide();
	$('#s4-workspace').css('overflow','hidden');
});

/* add class image webpart */ 
$('img[id^="MSOImageWebPart"]').closest('table').addClass('image-webpart');

if($( window ).width() < 768){
	$('.via-compliment-container').closest('.ms-webpartzone-cell').addClass('viacompliment-webpart');
	$('.viacompliment-webpart').find('.ms-webpart-titleText').append('<span class="compliment-btn">Show</span>');
	
	$('.compliment-btn').click(function(){
		$('.via-compliment-container,.viacompliments-guidelines').toggle();
		$(this).toggleClass('active');
		if ($(this).text() == "Show")
		   {$(this).text("Collapse");}
		else
		   {$(this).text("Show");}
	});
}
	
if ($('#Discussions img').hasClass('circular')) {
    $('#greenroom-page-content-container').addClass('happening-class');
}

$( "#greenroom-bottom-zone-left .ms-webpart-chrome-title,#greenroom-bottom-zone-right .ms-webpart-chrome-title,#greenroom-top-zone-left-col6 .ms-webpart-chrome-title,#greenroom-top-zone-right-col6 .ms-webpart-chrome-title" ).on( "click", function() {
if ($(this).hasClass( "ms-webpart-chrome-title" )){
		$(this).parent().parent('.ms-webpart-cell-vertical').addClass('greenroom-webpart-bg-offset');
		$('.happening-class #greenroom-top-zone-left-col6 .ms-webpart-cell-vertical,.happening-class #greenroom-top-zone-right-col6 .ms-webpart-cell-vertical').removeClass('greenroom-webpart-bg-offset');
		$(this).parent().parent('.ms-webpart-cell-vertical').find('.dfwp-column').css({'margin-top':'0','padding-bottom':'0'});
	}
	
});
$( "#greenroom-bottom-zone-left .ms-webpart-chrome-title,#greenroom-bottom-zone-right .ms-webpart-chrome-title,#greenroom-top-zone-left-col6 .ms-webpart-chrome-title,#greenroom-top-zone-right-col6 .ms-webpart-chrome-title" ).trigger( "click" );

/* Landing Page Section Expand/Collapse */
$('.greenroom-webpart-bg-offset .ms-webpart-titleText').append('<strong class="show-btn">Hide</strong>');
/*$('.greenroom-webpart-bg-offset').addClass('active');*/
$('.show-btn').click(function(){
  $(this).parents('.greenroom-webpart-bg-offset').toggleClass('active');
  $(this).parents('.greenroom-webpart-bg-offset').find('.ms-wpContentDivSpace').toggle();
    if ($(this).text() == "Show")
       {$(this).text("Hide");}
    else
       {$(this).text("Show");}
});


/* Landing Page Lists Expand/Collapse */
if($( window ).width() <= 1024){	
	$('.greenroom-webpart-bg-offset .cbq-layout-main').each(function(){
		if ($(this).find('ul li').length > 3) {
			$(this).append('<div class="show-btn-block"><span class="list-show-btn">More</span></div>');
		};		
		$(this).find('ul li:gt(2)').hide();			
	});
	
	$('.list-show-btn').click(function() {
			$(this).parents('.cbq-layout-main').find('ul li:gt(2)').toggle();
			$(this).toggleClass('active');
			if ($(this).text() == "More")
			   {$(this).text("Less");}
			else
			   {$(this).text("More");}
	});		
}

if($( window ).width() <= 1024 && $( window ).width() >= 768){
		setTimeout(function(){
			$('.uniqueYears').click(function(){
				var target_id = $(this).attr('href');
				$('html,body').animate({
					scrollTop: parseInt($(target_id).offset().top - 70)
				},500);
			});
		},4000);
}


/* Move Timeline Years */
if($( window ).width() < 768){
	var timelineYears = $('#tdYears');
	$('#greenroom-landing-page-content').prepend('<div class="timeline-years"></div>');
	$('.timeline-years').append(timelineYears);
	
	setTimeout(function(){
	
	$('.timeline-years div').click(function(){
			var target_id = $(this).find('a').attr('href');
		$('html,body').animate({
			scrollTop: parseInt($(target_id).offset().top - 70)
		},500);
		});
	},5000);	
	
}

/* Landing Page About Section */
function aboutContent(){
		if($( window ).width() < 768){		
		var aboutContent =  $('#greenroom-about-container').closest('.ms-webpart-cell-vertical.ms-fullWidth').addClass('aboutContainer');
		$('#greenroom-landing-page-content').prepend(aboutContent);	
		var aboutContentHeight = $('#greenroom-about-intro p').height();		
		$('.aboutMore').remove();
		if(aboutContentHeight > 40){
			$('#greenroom-about-container').append('<div class="aboutMore"><span>More</span></div>');
			$('#greenroom-about-contact').hide();
		}		
		
		$('.aboutMore').click(function() {
		$(this).parents('#greenroom-about-container').toggleClass('active');
		$('#greenroom-about-contact').toggle();
		$(this).toggleClass('active');
		if ($(this).find('span').text() == "More")
		   {$(this).find('span').text("Less");}
			else
			{$(this).find('span').text("More");}
			});	
	}	
}
aboutContent();
var width = $(window).width();
$( window ).resize(function() {
		if($(window).width() != width)
		{
		width = $(window).width();
		aboutContent();
		}	
});




/* Article Button placement changes */
function articleButtons(){
	var likeBtn = $('#greenroom-page-content-count-likes');
	var shareBtn = $('#greenroom-subsite-title-share');
	var commentBtn = $('#greenroom-top-comments-sidebar #greenroom-add-comment');
	$('.article-buttons').append(likeBtn);
	$('.article-buttons').append(shareBtn);
	$('.article-buttons').append(commentBtn);
		
	$('#greenroom-article-content #greenroom-add-comment a,#greenroom-sidebar-add-comment #greenroom-add-comment a').click(function(){
		var target_id = $('#greenroom-post-comment');
		$('html,body').animate({
			scrollTop: parseInt($(target_id).offset().top - 50)
		},500);	
	});
}
if($( window ).width() <= 1024){		
	articleButtons();
}

if($( window ).width() <= 1024){	
	$('ul.dynamic').addClass('inactive-item');	
	$('.dynamic-children.menu-item').click(function(){
		$(this).next('ul.dynamic').toggleClass('active-item');
		return false;
	});
}


/* Article Recent Comment placement changes */
function articleRecentComment(){
var recentComment = $('.greenroom-sidebar-top-comment');
$('.greenroom-recent-comment').append(recentComment);

$('.greenroom-recent-comment .comment-btn a').click(function(){
		var target_id = $('#ReplyTextArea2');
		$('html,body').animate({
			scrollTop: parseInt($(target_id).offset().top - 50)
		},500);	
});
}
if($( window ).width() <= 767){		
	articleRecentComment();
}

/* Outlook Button placement changes */
function outlookButton(){
var outlookContent = $('.outlook-box');
$('#EventConatiner').append(outlookContent);
}

if($( window ).width() <= 1024){		
	outlookButton();
}

/* Custom tooltip for iPad and iPhone/Android */
function toolTip(){
$(".tool-tip-desc,.tool-tip-desc-Location").click(function () {
  $('.tooltip-title').remove();
  var tooltipTitle = $(this).find('img').attr('title'); 
  $(this).append('<span class="tooltip-title">' + tooltipTitle + '</span>');  
});
}
toolTip();
$(window).load(function(){
	toolTip();
});

$(document).mouseup(function (e)
{
    var container = $('.tooltip-title');
    if (!container.is(e.target)
        && container.has(e.target).length === 0)
        {
        container.hide();
    }
});

/* Tix Template */
$("#greenroom-selfServiceProfileForm").closest('table').addClass('responsive-tix-table');
$('.waitlist-div td').each(function() {
 var $this = $(this);
 if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
     $this.remove(); }); 
	

$( "table" ).on( "click", function() {
if ($("table").attr('summary','MeetTheTeam')) {
	$(this).addClass('greenroom-meettheteam');
   	$(this).find('thead tr').addClass('hide');  	
}
});

$( "table" ).trigger( "click" );

/*
$( ".js-webpart-titleCell" ).on( "click", function() {
	if ($(this).attr('title','Meet The Team')) {
		if ($(this).find('a').attr('href','#')) {
		$(this).find('.landing-page-view-all').hide();
		}	
	}	
});

$( ".js-webpart-titleCell" ).trigger( "click" );
*/

setTimeout(function(){
$('.js-webpart-titleCell').each(function() {
	if ($(this).find('a').attr('href') != '#' ) {
		$(this).find('h2.ms-webpart-titleText a .landing-page-view-all').show();
	}	
});
},10000);


$( ".ms-hide").on( "click", function() {
	$(this).parent('.ms-webpartzone-cell').addClass('ms-webpartzone-cell-no-margin');

});

$( ".ms-hide").trigger( "click" );

$('.page-field-image .ms-rtestate-field').each(function() {
    var $this = $(this);
    if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
        $this.html('');
});

$('.greenroom-article-byline').each(function() {
    var $this = $(this);
    if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
        $this.html('');
});

setTimeout(function(){$(".upcoming-event").each(function() {
	$(this).parent().addClass("upcoming-item");
});
},2000);

$('.galleria-caption:has(div)').addClass('galleria-caption-edit');
		

/*$('#QuickLinksPlaceholder').hide();*/
setTimeout(function(){
QuickRollOver();
},4000); 

function QuickRollOver(){
$("#QuickLinksContainer ul img,.RecomendedLinks-homepage ul img").mouseover(function(){
var arr = $(this).attr('src').split('.png');
$(this).attr('src',arr[0]+'-roll.png');
});

$("#QuickLinksContainer ul img,.RecomendedLinks-homepage ul img").mouseleave(function(){
var arr = $(this).attr('src').split('-roll.png');
$(this).attr('src',arr[0]+'.png');
});

$("#QuickLinksContainer ul a span,.RecomendedLinks-homepage ul a span").mouseover(function(){
var arr = $(this).parent('a').find('img').attr('src').split('.png');
$(this).parent('a').find('img').attr('src',arr[0]+'-roll.png');
});

$("#QuickLinksContainer ul a span,.RecomendedLinks-homepage ul a span").mouseleave(function(){
var arr = $(this).parent('a').find('img').attr('src').split('-roll.png');
$(this).parent('a').find('img').attr('src',arr[0]+'.png');
});
}

setTimeout(function(){	
	$(".cswp-like-text,.cswp-tile-event,#cswp-tile-like-tiny,#cswp-tile-like-med,.cswp-tile-like-lg,#cswp-tile-like-sm,#greenroom-page-content-count-likes").on('mousedown',function(){
        $(this).addClass("activestate");
	});
	$(".cswp-like-text,.cswp-tile-event,#cswp-tile-like-tiny,#cswp-tile-like-med,.cswp-tile-like-lg,#cswp-tile-like-sm,#greenroom-page-content-count-likes").on('mouseup',function(){
        $(this).removeClass("activestate");
	});
	$('#greenroom-next-holiday-box').parents('.cbq-layout-main').addClass('event-list');
},2000);
	
	
});

$( document ).load(function() {
	$('#greenroom-next-holiday-box').parents('.cbq-layout-main').addClass('event-list');
});