﻿

$(window).load(function(){
	 ExecuteOrDelayUntilScriptLoaded(GetProperties, "SP.UserProfiles.js");
	 RedirectToPageFromPagesLibrary();
	 RedirectToPageFromCommentsList();
});



var clientContext=null;
var web;
var currentUser=null;
var peopleManager;
var LoginName=null;
var UserPropertiesValue = new Array();
var _PictureUrl;
var userProfileProperties;
function GetProperties()
{

	clientContext = new SP.ClientContext.get_current();
 	web = clientContext.get_web();
 	currentUser = web.get_currentUser(); 
    clientContext.load(currentUser);
    clientContext.executeQueryAsync(Function.createDelegate(this, this.onProfileSucceeded), Function.createDelegate(this, this.onProfileFailed));       
}
function onProfileFailed(sender,args){
	//console.log(args.get_message() + '\n' + args.get_stackTrace());
}

function onProfileSucceeded(sender, args) 
{
	getUserPropertiesFromProfile();
}

function getUserPropertiesFromProfile()
{
	
	var userProfileInfo = ["FirstName", "LastName", "PictureURL"];
	clientContext = new SP.ClientContext.get_current();
	LoginName = currentUser.get_loginName();
	LoginName = LoginName.split('|')[1];
	
	var peopleManager = new SP.UserProfiles.PeopleManager(clientContext);		
	var userProfilePropertiesForUser = new SP.UserProfiles.UserProfilePropertiesForUser(clientContext,LoginName,userProfileInfo);		
	userProfileProperties = peopleManager.getUserProfilePropertiesFor(userProfilePropertiesForUser);
	clientContext.load(userProfilePropertiesForUser);
	clientContext.executeQueryAsync(onSuccess, onProfileRequestFail);
}

function onSuccess()
{	
	var _userProfilePictureURL="";
	var _Name= userProfileProperties[0]+" "+ userProfileProperties[1];
	_PictureUrl =  userProfileProperties[2];
	
  
	if(_PictureUrl == null || _PictureUrl == "")
	{
   
		_userProfilePictureURL="/SiteCollectionImages/icon_welcome.png";	
	}
	else
	{
		_userProfilePictureURL=_PictureUrl;
	}
	
	var newWelcomeText = null;
	if(_Name == null || _Name == "")
	{
   //copied from GreenRoom MasterPage, to display Name in the welcome Box, in case some problem occured in getting the same from User Profile
	  var currentWelcomeText = $('#welcomeMenuBox').find('span').eq(1).find('a').text(); 
	  var arrayText = currentWelcomeText.split(',');
               var FirstName ='';
               if(arrayText.length ==2)
               {
                              FirstName = arrayText[1].substr(0, arrayText[1].indexOf('Use'));
                              newWelcomeText = 'Welcome, ' + FirstName;
                              $('#welcomeMenuBox').find('span').eq(1).find('a').text(newWelcomeText);
               }
               if(arrayText.length ==1)
               {
                              FirstName = arrayText[0].substr(0, arrayText[0].indexOf('Use'));
                              newWelcomeText = 'Welcome, ' + FirstName;
                              $('#welcomeMenuBox').find('span').eq(1).find('a').text(newWelcomeText);
               }
               var welcomeIcon = "/SiteCollectionImages/icon_welcome.png";

	}
	else
	{
	 newWelcomeText = 'Welcome, ' + _Name;
	}

	//setting the new Welcome Text in the Welcome Box
	$('#welcomeMenuBox').find('span').eq(1).find('a').text(newWelcomeText);	
	
	//add user profile image in the welcome menu
	$('#welcomeMenuBox').prepend("<img id='WelcomeIcon' src='"+_userProfilePictureURL+"' />");  


}
 
function onProfileRequestFail(sender, args)
{
	//console.log('Unable to Fetch User Profile Values.' + args.get_message() + '\n' + args.get_stackTrace()); 
}

function getQueryStringParameter(urlParameterKey) {
    var queryString = document.URL.split('?');
    if(queryString.length>=2){
		var params = document.URL.split('?')[1].split('&');
		var strParams = '';
		for (var i = 0; i < params.length; i = i + 1) {
			var singleParam = params[i].split('=');
			if (singleParam[0].toLowerCase() == urlParameterKey.toLowerCase())
				return true;
		}
    }
	return false;

}

function RedirectToPageFromPagesLibrary()
{
	var isCancelRedirect = getQueryStringParameter("cancelRedirect");
	var isPagesDispForm = false ;
	var currentLocation = window.location.href;
	var displayFormUrl = "Pages/Forms/DispForm.aspx";
	if(currentLocation.toLowerCase().indexOf(displayFormUrl.toLowerCase()) > -1)
	{
		isPagesDispForm = true;
	}
	if(!isCancelRedirect && isPagesDispForm)
	{
		if($("#SPFieldFile") != undefined && $("#SPFieldFile") != null && $("#SPFieldFile").find("a").length > 0)
		{
			var redirectLocation = $("#SPFieldFile").find("a")[0].href;
			if(redirectLocation != null && redirectLocation != undefined && redirectLocation != "")
			{
				window.location = redirectLocation;
			}
		}
	}
}

function RedirectToPageFromCommentsList()
{
	var isCancelRedirect = getQueryStringParameter("cancelRedirect");
	var isPagesDispForm = false ;
	var currentLocation = window.location.href;
	var displayFormUrl = "/Lists/Comments/DispForm.aspx";
	if(currentLocation.toLowerCase().indexOf(displayFormUrl.toLowerCase()) > -1)
	{
		isPagesDispForm = true;
	}
	if(!isCancelRedirect && isPagesDispForm)
	{
		var titleFieldLabel = $('a[name="SPBookmark_Title"]');
		if(titleFieldLabel!= undefined && titleFieldLabel != null && titleFieldLabel.parent() != null && titleFieldLabel.parent() != undefined)
		{
			if(titleFieldLabel.parent().parent() != null && titleFieldLabel.parent().parent() != undefined)
			{
				if(titleFieldLabel.parent().parent().parent() != null && titleFieldLabel.parent().parent().parent() != undefined)
				{
					var titleRowChildren = titleFieldLabel.parent().parent().parent().children();
					if(titleRowChildren != null && titleRowChildren != undefined && titleRowChildren.length >= 2)
					{
						var commentsParentPage = titleRowChildren[1];
						var commentsParentPageUrl = commentsParentPage.textContent.trim();
						window.location = commentsParentPageUrl;
					}
				}
			}
		}
	}
}

