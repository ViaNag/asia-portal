<%-- SPG:

This HTML file has been associated with a SharePoint Page Layout (.aspx file) carrying the same name.  While the files remain associated, you will not be allowed to edit the .aspx file, and any rename, move, or deletion operations will be reciprocated.

To build the page layout directly from this HTML file, simply fill in the contents of content placeholders.  Use the Snippet Generator at https://greenroomqa.viacom.com/sites/Australia/_layouts/15/ComponentHome.aspx?Url=https%3A%2F%2Fgreenroomqa%2Eviacom%2Ecom%2Fsites%2FAustralia%2F%5Fcatalogs%2Fmasterpage%2Fkonda%2Fkonda%2Darticle%2Dvideo%2Easpx to create and customize additional content placeholders and other useful SharePoint entities, then copy and paste them as HTML snippets into your HTML code.   All updates to this file within content placeholders will automatically sync to the associated page layout.

 --%>
<%@Page language="C#" Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage, Microsoft.SharePoint.Publishing, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="PageFieldFieldValue" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="Publishing" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="PageFieldTextField" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="PageFieldRichHtmlField" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="spsswc" Namespace="Microsoft.Office.Server.Search.WebControls" Assembly="Microsoft.Office.Server.Search, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@Register TagPrefix="a2e8ead9d" Namespace="Microsoft.Office.Server.Search.WebControls" Assembly="Microsoft.Office.Server.Search, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@Register TagPrefix="PageFieldUserField" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="PageFieldTaxonomyFieldControl" Namespace="Microsoft.SharePoint.Taxonomy" Assembly="Microsoft.SharePoint.Taxonomy, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="PageFieldCheckBoxChoiceField" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="PageFieldDateTimeField" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="PageFieldRichImageField" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="PageFieldBooleanField" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<asp:Content runat="server" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea">
            
            
            <PageFieldFieldValue:FieldValue FieldName="fa564e0f-0c70-4ab9-b863-0177e6ddd247" runat="server">
            </PageFieldFieldValue:FieldValue>
            
        </asp:Content><asp:Content runat="server" ContentPlaceHolderID="PlaceHolderMain">
            <!-- <script type="text/javascript" src="scripts/js/jquery-1.10.2.min.js">//<![CDATA[
			
			//]]></script> -->
            <SharePoint:ScriptLink ID="ScriptLink4" name="~sitecollection/_catalogs/masterpage/konda/scripts/js/jquery.twbsPagination.min.js" OnDemand="false" runat="server" Localizable="false" />
            <SharePoint:ScriptLink ID="ScriptLink5" name="~sitecollection/_catalogs/masterpage/konda/scripts/js/Comments.js" OnDemand="false" runat="server" Localizable="false" />
            <SharePoint:ScriptLink ID="ScriptLink100" name="~sitecollection/_catalogs/masterpage/konda/scripts/js/pagelayouts.js" OnDemand="false" runat="server" Localizable="false" />
            <SharePoint:ScriptLink ID="ScriptLink15" name="~sitecollection/_catalogs/masterpage/konda/scripts/js/ArticleTagging.js" OnDemand="false" runat="server" Localizable="false" />
            <SharePoint:ScriptLink ID="ScriptLink10" name="~sitecollection/_catalogs/masterpage/konda/scripts/js/VideoLayoutNew.js" OnDemand="false" runat="server" Localizable="false" />
            <SharePoint:ScriptLink ID="ScriptLink7" name="~sitecollection/_catalogs/masterpage/konda/scripts/js/unslider.min.js" OnDemand="false" runat="server" Localizable="false" />
            <link href="/sites/Australia/_catalogs/masterpage/konda/css/PageLayouts.css" rel="stylesheet" type="text/css" />
            <SharePoint:ScriptLink ID="ScriptLink6" name="SP.js" runat="server" ondemand="false" localizable="false" loadafterui="true" />
            <SharePoint:ScriptLink ID="ScriptLink8" name="SP.Core.js" runat="server" ondemand="false" localizable="false" loadafterui="true" />
            <SharePoint:ScriptLink ID="ScriptLink9" name="Reputation.js" runat="server" ondemand="false" localizable="false" loadafterui="true" />
            <!-- Begin page content -->
            <div class="container" id="article-video">
                <div id="greenroom-subsite-title">
                </div>
                <div id="greenroom-subsite-title-follow">
                </div>
                <a href="mailto:hr.team@vimn.com?subject=Greenroom Australia Feedback">
                    <div id="greenroom-landing-page-btn-feedback">
                        <img src="/SiteCollectionImages/greenroom-subsite-feedback.png" />
                    </div>
                </a>
                <div class="container" id="greenroom-page-content-container">
                    <div class="col-md-9 col-sm-7 col-xs-12">
                        <div class="greenroom-article-info-report">
                            <div id="TimeStampCreated">
                            </div>
                        </div>
                        <div id="greenroom-article-content" class="col-md-9 col-sm-12">
                            <div class="page-header">
                                <h1>
                                    
                                    
                                    
                                    <PageFieldTextField:TextField FieldName="fa564e0f-0c70-4ab9-b863-0177e6ddd247" runat="server">
                                    </PageFieldTextField:TextField>
                                    
                                </h1>
                                <div class="article-buttons">
                                </div>
                                <div data-name="Page Field: Byline">
                                    
                                    
                                    <PageFieldTextField:TextField FieldName="d3429cc9-adc4-439b-84a8-5679070f84cb" runat="server">
                                        
                                    </PageFieldTextField:TextField>
                                    
                                </div>
                            </div>
                            <div>
                                <div id="video">
                                </div>
                            </div>
                            <div>
                                <div data-name="EditModePanelShowInEdit">
                                    
                                    
                                    <Publishing:EditModePanel runat="server" CssClass="edit-mode-panel">
                                        
                                        <!-- <input type="button" id="UploadVideos" onclick="javascript:{UploadVid();}" value="Uplaod Videos"/>-->
                                        <input type="button" onclick="articleTagging.checkAndCreateFolder(&quot;Videos&quot;);" value="UPLOAD VIDEOS" />
                                        <br />
                                        <input type="button" onclick="articleTagging.TagImages(&quot;Videos&quot;);" value="Tag Videos" />
                                        
                                    </Publishing:EditModePanel>
                                    
                                </div>
                            </div>
                            <div data-name="Page Field: Video Link" id="dvVideoLink">
                                
                                
                                <PageFieldTextField:TextField FieldName="eadaff62-1a42-4b9e-bdfb-765d1c97f9dc" runat="server">
                                    
                                </PageFieldTextField:TextField>
                                
                            </div>
                            <br />
                            <br />
                            <div id="greenroom-page-content">
                                
                                
                                <PageFieldRichHtmlField:RichHtmlField FieldName="f55c4d88-1f2e-4ad9-aaa8-819af4ee7ee8" runat="server">
                                    
                                </PageFieldRichHtmlField:RichHtmlField>
                                
                            </div>
                            <!-- id="greenroom-page-content" end-->
                        </div>
                        <!-- id="greenroom-article-content" end-->
                        <div id="greenroom-article-content-sidebar" class="col-md-3 col-sm-12">
                            <div id="greenroom-page-content-count-likes">
                                <span>
                                    <span class="mylikecount">
                                    </span>
                                    <a href="#" onclick="LikePage()" class="MyLikeButton">
                                    </a>
                                </span>
                            </div>
                            <!--<div id="greenroom-page-content-count-comments">
                                <div class="comments">
                                </div>
                                <img src="/SiteCollectionImages/pagelayouts/greenroom-count-comments.png"/>
                            </div>-->
                            <div id="greenroom-subsite-title-share">
                                <div class="ms-cui-TabRowRight s4-trc-container s4-notdlg ms-core-defaultFont" id="RibbonContainer-TabRowRight" unselectable="on" onmouseover="document.getElementById('share-container').style.display='block';" onmouseout="document.getElementById('share-container').style.display='none';">
                                    <a title="Share this article with people." class="ms-promotedActionButton">
                                        <span class="s4-clust ms-promotedActionButton-icon">
                                            <img alt="Share" src="/SiteCollectionImages/pagelayouts/greenroom-subsite-title-share.png" />
                                        </span>
                                    </a>
                                    <ul id="share-container" class="ms-core-menu-list">
                                        <li class="ms-core-menu-item">
                                            <a onclick="javascript:OnEmailClick();" class="ms-core-menu-link">Email
                                
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            </a>
                                        </li>
                                        <li class="ms-core-menu-item">
                                            <a class="ms-core-menu-link" href="javascript:void(0);" onclick="javascript:OpenNewsFeed();">Newsfeed
                                
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- id="greenroom-article-content-sidebar end-->
                        <hr />
                        <div id="" class="col-xs-12">
                            <div data-name="WebPartZone">
                                
                                
                                <div>
                                    <WebPartPages:WebPartZone runat="server" ID="xd7ee700c2f8244ae91a39adad0237e41" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Vertical" Title="greenroom-left-zone">
                                        <ZoneTemplate>
                                            
                                        </ZoneTemplate>
                                    </WebPartPages:WebPartZone>
                                </div>
                                
                            </div>
                        </div>
                        <div id="greenroom-top-comments" class="col-md-9 col-sm-12">
                            <a name="top-comments">
                            </a>
                            <div id="DisplayTopComments">
                            </div>
                        </div>
                        <!-- id="greenroom-top-comments" end-->
                        <div id="greenroom-top-comments-sidebar" class="col-md-3 col-sm-12">
                            <div id="greenroom-add-comment">
                                <a class="btn btn-primary btn-lg" href="#post-comment">ADD A COMMENT 
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    <span class="glyphicon glyphicon-plus-sign">
                                    </span>
                                </a>
                            </div>
                        </div>
                        <!-- id="greenroom-top-comments-sidebar" end-->
                        <div class="greenroom-recent-comment">
                        </div>
                        <div class="clearfix">
                        </div>
                        <div id="greenroom-all-comments" class="col-md-9 col-sm-12">
                            <div id="DisplayAllComments">
                            </div>
                            <div id="pagination-demo" class="pagination-sm">
                            </div>
                        </div>
                        <!-- id="greenroom-all-comments" end-->
                        <div id="greenroom-all-comments-sidebar" class="col-md-3 col-sm-12">
                        </div>
                        <!-- id="greenroom-all-comments-sidebar" end-->
                        <hr />
                        <div id="greenroom-post-comment" class="col-md-9 col-sm-12">
                            <a name="post-comment">
                            </a>
                            <h1>Post a Comment
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            </h1>
                            <div id="greenroom-article-postcomment-container">
                                <div id="CommenterPictureURL" class="greenroom-post-comment-user-image">
                                </div>
                                <div>
                                    <textarea id="CommentArea" name="Comment" cols="70" rows="6">
                                    </textarea>
                                    <div id="greenroom-post-comment-guidelines">Before you post your comment, please be sure it adheres to our 
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        <a href="/pages/community-guidelines.aspx" title="Community Guidelines" target="_blank">community guidelines
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        </a>









.                                                                                                                                                                                                                                                                                                                                                                        </div>
                                    <div id="greenroom-post-comment-button">
                                        <a class="btn btn-primary btn-lg" onclick="PostComment();">POST YOUR COMMENT 
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            <span class="glyphicon glyphicon-play">
                                            </span>
                                        </a>
                                    </div>
                                    <!--input class="btn btn-primary btn-lg" name="POST YOUR COMMENT" type="button" value="POST YOUR COMMENT" onclick="PostComment();"-->
                                </div>
                            </div>
                        </div>
                        <!-- id="greenroom-post-comments" end-->
                        <!-- id="greenroom-post-comment-sidebar" end-->
                        <hr />
                        <div id="greenroom-more-related-articles" class="col-xs-12">
                            
                            
                            
                            <spsswc:ContentBySearchWebPart runat="server" AlwaysRenderOnServer="False" ResultType="" NumberOfItems="4" DataProviderJSON="{&quot;QueryGroupName&quot;:&quot;Default&quot;,&quot;QueryPropertiesTemplateUrl&quot;:&quot;sitesearch://webroot&quot;,&quot;IgnoreQueryPropertiesTemplateUrl&quot;:false,&quot;SourceID&quot;:&quot;8413cd39-2156-4e00-b54d-11efd9abdb89&quot;,&quot;SourceName&quot;:&quot;Local SharePoint Results&quot;,&quot;SourceLevel&quot;:&quot;Ssa&quot;,&quot;CollapseSpecification&quot;:&quot;&quot;,&quot;QueryTemplate&quot;:&quot;path:{Site.URL} (ContentType:\&quot;Konda Video Article Page\&quot;*) AND (( Title:{Page.Title} OR owstaxidmetadataalltagsinfo:{Page.TaxKeyword} ) AND Path\u003c\u003e{Page.URL} ) AND ( -ContentType:\&quot;CommentReportAbuse\&quot;*)&quot;,&quot;FallbackSort&quot;:[],&quot;FallbackSortJson&quot;:&quot;[]&quot;,&quot;RankRules&quot;:[],&quot;RankRulesJson&quot;:&quot;[]&quot;,&quot;AsynchronousResultRetrieval&quot;:false,&quot;SendContentBeforeQuery&quot;:true,&quot;BatchClientQuery&quot;:true,&quot;FallbackLanguage&quot;:-1,&quot;FallbackRankingModelID&quot;:&quot;&quot;,&quot;EnableStemming&quot;:true,&quot;EnablePhonetic&quot;:false,&quot;EnableNicknames&quot;:false,&quot;EnableInterleaving&quot;:false,&quot;EnableQueryRules&quot;:true,&quot;EnableOrderingHitHighlightedProperty&quot;:false,&quot;HitHighlightedMultivaluePropertyLimit&quot;:-1,&quot;IgnoreContextualScope&quot;:true,&quot;ScopeResultsToCurrentSite&quot;:false,&quot;TrimDuplicates&quot;:false,&quot;Properties&quot;:{&quot;TryCache&quot;:true,&quot;Scope&quot;:&quot;{Site.URL}&quot;,&quot;ListId&quot;:&quot;00000000-0000-0000-0000-000000000000&quot;,&quot;UpdateLinksForCatalogItems&quot;:true,&quot;EnableStacking&quot;:true},&quot;PropertiesJson&quot;:&quot;{\&quot;TryCache\&quot;:true,\&quot;Scope\&quot;:\&quot;{Site.URL}\&quot;,\&quot;ListId\&quot;:\&quot;00000000-0000-0000-0000-000000000000\&quot;,\&quot;UpdateLinksForCatalogItems\&quot;:true,\&quot;EnableStacking\&quot;:true}&quot;,&quot;ClientType&quot;:&quot;ContentSearchRegular&quot;,&quot;UpdateAjaxNavigate&quot;:true,&quot;SummaryLength&quot;:180,&quot;DesiredSnippetLength&quot;:90,&quot;PersonalizedQuery&quot;:false,&quot;FallbackRefinementFilters&quot;:null,&quot;IgnoreStaleServerQuery&quot;:false,&quot;RenderTemplateId&quot;:&quot;DefaultDataProvider&quot;,&quot;AlternateErrorMessage&quot;:null,&quot;Title&quot;:&quot;&quot;}" BypassResultTypes="True" ItemTemplateId="~sitecollection/_catalogs/masterpage/Display Templates/Content Web Parts/Related_Articles.js" GroupTemplateId="~sitecollection/_catalogs/masterpage/Display Templates/Content Web Parts/Group_Content.js" ResultsPerPage="4" SelectedPropertiesJson="[&quot;PublishingImage&quot;,&quot;PictureURL&quot;,&quot;PictureThumbnailURL&quot;,&quot;Path&quot;,&quot;Title&quot;,&quot;SitePath&quot;,&quot;ModifiedOWSDATE&quot;,&quot;ListID&quot;,&quot;ListItemID&quot;,&quot;SiteTitle&quot;,&quot;PublishingPageContentOWSHTML&quot;,&quot;ContentTypeId&quot;,&quot;SecondaryFileExtension&quot;]" HitHighlightedPropertiesJson="[&quot;Title&quot;,&quot;Path&quot;,&quot;Author&quot;,&quot;SectionNames&quot;,&quot;SiteDescription&quot;]" AvailableSortsJson="null" ShowBestBets="False" ShowPersonalFavorites="False" ShowDefinitions="False" ShowDidYouMean="False" PreloadedItemTemplateIdsJson="null" QueryGroupName="Default" RenderTemplateId="~sitecollection/_catalogs/masterpage/Display Templates/Content Web Parts/Control_List_LandingPage_LatestArticles.js" StatesJson="{}" ServerIncludeScriptsJson="null" Title="More Related Articles" FrameType="TitleBarOnly" SuppressWebPartChrome="False" Description="" IsIncluded="True" ZoneID="ImportedPartZone" PartOrder="0" FrameState="Normal" AllowRemove="True" AllowZoneChange="True" AllowMinimize="True" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" DetailLink="" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="&lt;%$Resources:core,ImportErrorMessage;%&gt;" ImportErrorMessage="&lt;%$Resources:core,ImportErrorMessage;%&gt;" PartImageLarge="" IsIncludedFilter="" ExportControlledProperties="True" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_2835b02e_056b_4bf8_b6f0_c9c0c9825fd0" ChromeType="TitleOnly" ExportMode="All" __MarkupType="vsattributemarkup" __WebPartId="{2835b02e-056b-4bf8-b6f0-c9c0c9825fd0}" WebPart="true" Height="" Width="">
                            </spsswc:ContentBySearchWebPart>
                            
                            
                        </div>
                        <!-- id="greenroom-more-related-articles" end-->
                        <hr />
                    </div>
                    <div class="col-md-3 col-sm-5 col-xs-12" id="greenroom-sidebar">
                        <!-- adding page fields-->
                        <div data-name="EditModePanelShowInEdit">
                            
                            
                            <Publishing:EditModePanel runat="server" CssClass="edit-mode-panel">
                                
                                <div class="DefaultContentBlock" style="border: medium black solid; background: yellow; color: black; margin: 20px; padding: 10px;">
                                    <div>
                                        <strong>PLEASE NOTE, AFTER YOU PUBLISH THE PAGE, IT MAY TAKE UP TO 30MINS FOR YOUR ARTICLE TO APPEAR ON THE SITE. 
                                        
                                        
                                        
                                        
                                        
                                        
                                        </strong>
                                    </div>
                                    <div data-name="Page Field: Contact">
                                        
                                        
                                        <PageFieldUserField:UserField FieldName="aea1a4dd-0f19-417d-8721-95a1d28762ab" runat="server">
                                            
                                        </PageFieldUserField:UserField>
                                        
                                    </div>
                                    <div data-name="Page Field: Region">
                                        
                                        
                                        <PageFieldTaxonomyFieldControl:TaxonomyFieldControl FieldName="Region" runat="server">
                                            
                                        </PageFieldTaxonomyFieldControl:TaxonomyFieldControl>
                                        
                                    </div>
                                    <div data-name="Page Field: Enterprise Keywords">
                                        
                                        
                                        <PageFieldTaxonomyFieldControl:TaxonomyFieldControl FieldName="23f27201-bee3-471e-b2e7-b64fd8b7ca38" runat="server">
                                            
                                        </PageFieldTaxonomyFieldControl:TaxonomyFieldControl>
                                        
                                    </div>
                                    <div>
                                    </div>
                                    <div> Articles can be promoted in the subsites that they are created in, home page (featuring now) and happening now (if selected below).
                                  
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    </div>
                                    <div data-name="Page Field: Request to promote on the following pages">
                                        
                                        
                                        <PageFieldCheckBoxChoiceField:CheckBoxChoiceField FieldName="20658759-c3a9-4482-8078-fca681363bb4" runat="server">
                                            
                                        </PageFieldCheckBoxChoiceField:CheckBoxChoiceField>
                                        
                                    </div>
                                    <div data-name="Page Field: Promotion Start Date">
                                        
                                        
                                        <PageFieldDateTimeField:DateTimeField FieldName="a90e54fb-1ca6-4e9b-9124-d0824233d1c0" runat="server">
                                            
                                        </PageFieldDateTimeField:DateTimeField>
                                        
                                    </div>
                                    <div data-name="Page Field: Promotion End Date">
                                        
                                        
                                        <PageFieldDateTimeField:DateTimeField runat="server" FieldName="0a61bf5a-6146-4c25-be7f-31b22ea97c43">
                                            
                                        </PageFieldDateTimeField:DateTimeField>
                                        
                                    </div>
                                    <div>
                                        <strong>NOTE: IF REQUESTING TO PROMOTE AN ARTICLE, A THUMBNAIL 
						IMAGE IS REQUIRED IN 398x258px.
                                        
                                        
                                        
                                        
                                        
                                        
                                        </strong>
                                    </div>
                                    <div data-name="Page Field: Thumbnail Image">
                                        
                                        
                                        <PageFieldRichImageField:RichImageField FieldName="3de94b06-4120-41a5-b907-88773e493458" runat="server">
                                            
                                        </PageFieldRichImageField:RichImageField>
                                        
                                    </div>
                                </div>
                                <div class="DefaultContentBlock" id="greenroom-admin-block-blue">
                                    <div data-name="Page Field: Promote Request">
                                        
                                        
                                        <PageFieldBooleanField:BooleanField FieldName="d08270ab-fdbb-4e30-a6f5-91325d58a16b" runat="server">
                                            
                                        </PageFieldBooleanField:BooleanField>
                                        
                                    </div>
                                </div>
                                
                            </Publishing:EditModePanel>
                            
                        </div>
                        <div data-name="WebPartZone">
                            
                            
                            <div>
                                <WebPartPages:WebPartZone runat="server" ID="x3a0dfd72d75543d3916f18c057f336e5" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Vertical" Title="greenroom-sidebar-zone">
                                    <ZoneTemplate>
                                        
                                    </ZoneTemplate>
                                </WebPartPages:WebPartZone>
                            </div>
                            
                        </div>
                        <div class="greenroom-sidebar-top-comment-headline">
                            <h2 class="ms-webpart-titleText">
                                <a href="#top-comments">
                                </a>
                            </h2>
                        </div>
                        <div class="greenroom-sidebar-top-comment">
                        </div>
                        <div id="greenroom-sidebar-add-comment">
                            <div id="greenroom-add-comment">
                                <a href="#post-comment" class="btn btn-primary btn-lg">ADD A COMMENT 
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    <span class="glyphicon glyphicon-plus-sign">
                                    </span>
                                </a>
                            </div>
                        </div>
                        <hr />
                        <div class="greenroom-sidebar-more-videos">
                            
                            
                            
                            <spsswc:ContentBySearchWebPart runat="server" AlwaysRenderOnServer="False" ResultType="" DataProviderJSON="{&quot;QueryGroupName&quot;:&quot;Default&quot;,&quot;QueryPropertiesTemplateUrl&quot;:&quot;sitesearch://webroot&quot;,&quot;IgnoreQueryPropertiesTemplateUrl&quot;:false,&quot;SourceID&quot;:&quot;8413cd39-2156-4e00-b54d-11efd9abdb89&quot;,&quot;SourceName&quot;:&quot;Local SharePoint Results&quot;,&quot;SourceLevel&quot;:&quot;Ssa&quot;,&quot;CollapseSpecification&quot;:&quot;&quot;,&quot;QueryTemplate&quot;:&quot;path:{Site.URL} (ContentType:\&quot;Konda Video Article Page\&quot;)  AND Path\u003c\u003e{Page.URL}&quot;,&quot;FallbackSort&quot;:[],&quot;FallbackSortJson&quot;:&quot;[]&quot;,&quot;RankRules&quot;:[],&quot;RankRulesJson&quot;:&quot;[]&quot;,&quot;AsynchronousResultRetrieval&quot;:false,&quot;SendContentBeforeQuery&quot;:true,&quot;BatchClientQuery&quot;:true,&quot;FallbackLanguage&quot;:-1,&quot;FallbackRankingModelID&quot;:&quot;&quot;,&quot;EnableStemming&quot;:true,&quot;EnablePhonetic&quot;:false,&quot;EnableNicknames&quot;:false,&quot;EnableInterleaving&quot;:false,&quot;EnableQueryRules&quot;:true,&quot;EnableOrderingHitHighlightedProperty&quot;:false,&quot;HitHighlightedMultivaluePropertyLimit&quot;:-1,&quot;IgnoreContextualScope&quot;:true,&quot;ScopeResultsToCurrentSite&quot;:false,&quot;TrimDuplicates&quot;:false,&quot;Properties&quot;:{&quot;TryCache&quot;:true,&quot;Scope&quot;:&quot;{Site.URL}&quot;,&quot;ListId&quot;:&quot;00000000-0000-0000-0000-000000000000&quot;,&quot;UpdateLinksForCatalogItems&quot;:true,&quot;EnableStacking&quot;:true},&quot;PropertiesJson&quot;:&quot;{\&quot;TryCache\&quot;:true,\&quot;Scope\&quot;:\&quot;{Site.URL}\&quot;,\&quot;ListId\&quot;:\&quot;00000000-0000-0000-0000-000000000000\&quot;,\&quot;UpdateLinksForCatalogItems\&quot;:true,\&quot;EnableStacking\&quot;:true}&quot;,&quot;ClientType&quot;:&quot;ContentSearchRegular&quot;,&quot;UpdateAjaxNavigate&quot;:true,&quot;SummaryLength&quot;:180,&quot;DesiredSnippetLength&quot;:90,&quot;PersonalizedQuery&quot;:false,&quot;FallbackRefinementFilters&quot;:null,&quot;IgnoreStaleServerQuery&quot;:false,&quot;RenderTemplateId&quot;:&quot;DefaultDataProvider&quot;,&quot;AlternateErrorMessage&quot;:null,&quot;Title&quot;:&quot;&quot;}" BypassResultTypes="True" ItemTemplateId="~sitecollection/_catalogs/masterpage/Display Templates/Content Web Parts/Related_Articles_Sidebar.js" GroupTemplateId="~sitecollection/_catalogs/masterpage/Display Templates/Content Web Parts/Group_Content.js" ResultsPerPage="3" SelectedPropertiesJson="[&quot;PublishingImage&quot;,&quot;PictureURL&quot;,&quot;PictureThumbnailURL&quot;,&quot;Path&quot;,&quot;Title&quot;,&quot;SitePath&quot;,&quot;ModifiedOWSDATE&quot;,&quot;ListID&quot;,&quot;ListItemID&quot;,&quot;SiteTitle&quot;,&quot;PublishingPageContentOWSHTML&quot;,&quot;ContentTypeId&quot;,&quot;SecondaryFileExtension&quot;]" HitHighlightedPropertiesJson="[&quot;Title&quot;,&quot;Path&quot;,&quot;Author&quot;,&quot;SectionNames&quot;,&quot;SiteDescription&quot;]" AvailableSortsJson="null" ShowBestBets="False" ShowPersonalFavorites="False" ShowDefinitions="False" ShowDidYouMean="False" PreloadedItemTemplateIdsJson="null" QueryGroupName="Default" RenderTemplateId="~sitecollection/_catalogs/masterpage/Display Templates/Content Web Parts/Control_List_LandingPage_LatestArticles.js" StatesJson="{}" ServerIncludeScriptsJson="null" Title="More Videos" FrameType="TitleBarOnly" SuppressWebPartChrome="False" Description="" IsIncluded="True" ZoneID="ImportedPartZone" PartOrder="0" FrameState="Normal" AllowRemove="True" AllowZoneChange="True" AllowMinimize="True" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" DetailLink="" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="&lt;%$Resources:core,ImportErrorMessage;%&gt;" ImportErrorMessage="&lt;%$Resources:core,ImportErrorMessage;%&gt;" PartImageLarge="" IsIncludedFilter="" ExportControlledProperties="True" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_4ac220dd_ef85_441f_9bad_ce0fdd4f9a31" ChromeType="TitleOnly" ExportMode="All" __MarkupType="vsattributemarkup" __WebPartId="{4ac220dd-ef85-441f-9bad-ce0fdd4f9a31}" WebPart="true" Height="" Width="">
                            </spsswc:ContentBySearchWebPart>
                            
                            
                        </div>
                        <hr />
                        <div class="greenroom-sidebar-related">
                            
                            
                            
                            <spsswc:ContentBySearchWebPart runat="server" AlwaysRenderOnServer="False" ResultType="" NumberOfItems="5" DataProviderJSON="{&quot;QueryGroupName&quot;:&quot;Default&quot;,&quot;QueryPropertiesTemplateUrl&quot;:&quot;sitesearch://webroot&quot;,&quot;IgnoreQueryPropertiesTemplateUrl&quot;:false,&quot;SourceID&quot;:&quot;8413cd39-2156-4e00-b54d-11efd9abdb89&quot;,&quot;SourceName&quot;:&quot;Local SharePoint Results&quot;,&quot;SourceLevel&quot;:&quot;Ssa&quot;,&quot;CollapseSpecification&quot;:&quot;&quot;,&quot;QueryTemplate&quot;:&quot;path:{Site.URL} (ContentType:\&quot;Konda Video Article Page\&quot;*) AND (( Title:{Page.Title} OR owstaxidmetadataalltagsinfo:{Page.TaxKeyword} ) AND Path\u003c\u003e{Page.URL} ) AND ( -ContentType:\&quot;CommentReportAbuse\&quot;*)&quot;,&quot;FallbackSort&quot;:[],&quot;FallbackSortJson&quot;:&quot;[]&quot;,&quot;RankRules&quot;:[],&quot;RankRulesJson&quot;:&quot;[]&quot;,&quot;AsynchronousResultRetrieval&quot;:false,&quot;SendContentBeforeQuery&quot;:true,&quot;BatchClientQuery&quot;:true,&quot;FallbackLanguage&quot;:-1,&quot;FallbackRankingModelID&quot;:&quot;&quot;,&quot;EnableStemming&quot;:true,&quot;EnablePhonetic&quot;:false,&quot;EnableNicknames&quot;:false,&quot;EnableInterleaving&quot;:false,&quot;EnableQueryRules&quot;:true,&quot;EnableOrderingHitHighlightedProperty&quot;:false,&quot;HitHighlightedMultivaluePropertyLimit&quot;:-1,&quot;IgnoreContextualScope&quot;:true,&quot;ScopeResultsToCurrentSite&quot;:false,&quot;TrimDuplicates&quot;:false,&quot;Properties&quot;:{&quot;TryCache&quot;:true,&quot;Scope&quot;:&quot;{Site.URL}&quot;,&quot;ListId&quot;:&quot;00000000-0000-0000-0000-000000000000&quot;,&quot;UpdateLinksForCatalogItems&quot;:true,&quot;EnableStacking&quot;:true},&quot;PropertiesJson&quot;:&quot;{\&quot;TryCache\&quot;:true,\&quot;Scope\&quot;:\&quot;{Site.URL}\&quot;,\&quot;ListId\&quot;:\&quot;00000000-0000-0000-0000-000000000000\&quot;,\&quot;UpdateLinksForCatalogItems\&quot;:true,\&quot;EnableStacking\&quot;:true}&quot;,&quot;ClientType&quot;:&quot;ContentSearchRegular&quot;,&quot;UpdateAjaxNavigate&quot;:true,&quot;SummaryLength&quot;:180,&quot;DesiredSnippetLength&quot;:90,&quot;PersonalizedQuery&quot;:false,&quot;FallbackRefinementFilters&quot;:null,&quot;IgnoreStaleServerQuery&quot;:false,&quot;RenderTemplateId&quot;:&quot;DefaultDataProvider&quot;,&quot;AlternateErrorMessage&quot;:null,&quot;Title&quot;:&quot;&quot;}" BypassResultTypes="True" ItemTemplateId="~sitecollection/_catalogs/masterpage/Display Templates/Content Web Parts/Related_Articles_Sidebar.js" GroupTemplateId="~sitecollection/_catalogs/masterpage/Display Templates/Content Web Parts/Group_Content.js" ResultsPerPage="5" SelectedPropertiesJson="[&quot;PublishingImage&quot;,&quot;PictureURL&quot;,&quot;PictureThumbnailURL&quot;,&quot;Path&quot;,&quot;Title&quot;,&quot;SitePath&quot;,&quot;ModifiedOWSDATE&quot;,&quot;ListID&quot;,&quot;ListItemID&quot;,&quot;SiteTitle&quot;,&quot;PublishingPageContentOWSHTML&quot;,&quot;ContentTypeId&quot;,&quot;SecondaryFileExtension&quot;]" HitHighlightedPropertiesJson="[&quot;Title&quot;,&quot;Path&quot;,&quot;Author&quot;,&quot;SectionNames&quot;,&quot;SiteDescription&quot;]" AvailableSortsJson="null" ShowBestBets="False" ShowPersonalFavorites="False" ShowDefinitions="False" ShowDidYouMean="False" PreloadedItemTemplateIdsJson="null" QueryGroupName="Default" RenderTemplateId="~sitecollection/_catalogs/masterpage/Display Templates/Content Web Parts/Control_List_LandingPage_LatestArticles.js" StatesJson="{}" ServerIncludeScriptsJson="null" Title="Related Articles" FrameType="TitleBarOnly" SuppressWebPartChrome="False" Description="" IsIncluded="True" ZoneID="ImportedPartZone" PartOrder="0" FrameState="Normal" AllowRemove="True" AllowZoneChange="True" AllowMinimize="True" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" DetailLink="" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="&lt;%$Resources:core,ImportErrorMessage;%&gt;" ImportErrorMessage="&lt;%$Resources:core,ImportErrorMessage;%&gt;" PartImageLarge="" IsIncludedFilter="" ExportControlledProperties="True" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_c86d8420_8e0b_44ce_bada_6c1d805423c6" ChromeType="TitleOnly" ExportMode="All" __MarkupType="vsattributemarkup" __WebPartId="{c86d8420-8e0b-44ce-bada-6c1d805423c6}" WebPart="true" Height="" Width="">
                            </spsswc:ContentBySearchWebPart>
                            
                            
                        </div>
                        <!-- id="greenroom-related-articles" end-->
                        <hr />
                        <div class="greenroom-sidebar-latest-articles">
                            
                            
                            
                            <spsswc:ContentBySearchWebPart runat="server" AlwaysRenderOnServer="False" ResultType="" NumberOfItems="5" DataProviderJSON="{&quot;QueryGroupName&quot;:&quot;Default&quot;,&quot;QueryPropertiesTemplateUrl&quot;:&quot;sitesearch://webroot&quot;,&quot;IgnoreQueryPropertiesTemplateUrl&quot;:false,&quot;SourceID&quot;:&quot;8413cd39-2156-4e00-b54d-11efd9abdb89&quot;,&quot;SourceName&quot;:&quot;Local SharePoint Results&quot;,&quot;SourceLevel&quot;:&quot;Ssa&quot;,&quot;CollapseSpecification&quot;:&quot;&quot;,&quot;QueryTemplate&quot;:&quot;path:{Site.URL} (ContentType:\&quot;Konda Article Page\&quot;*) OR  (ContentType:\&quot;Konda Event Article Page\&quot;*) OR (ContentType:\&quot;Konda Gallery Article Page\&quot;*) OR (ContentType:\&quot;Konda Video Article Page\&quot;*) OR (ContentType:\&quot;Konda Survey Article Page\&quot;*) OR (ContentType:\&quot;Konda Text Article Page\&quot;*) AND Path\u003c\u003e{Page.URL}&quot;,&quot;FallbackSort&quot;:[],&quot;FallbackSortJson&quot;:&quot;[]&quot;,&quot;RankRules&quot;:[],&quot;RankRulesJson&quot;:&quot;[]&quot;,&quot;AsynchronousResultRetrieval&quot;:false,&quot;SendContentBeforeQuery&quot;:true,&quot;BatchClientQuery&quot;:true,&quot;FallbackLanguage&quot;:-1,&quot;FallbackRankingModelID&quot;:&quot;&quot;,&quot;EnableStemming&quot;:true,&quot;EnablePhonetic&quot;:false,&quot;EnableNicknames&quot;:false,&quot;EnableInterleaving&quot;:false,&quot;EnableQueryRules&quot;:true,&quot;EnableOrderingHitHighlightedProperty&quot;:false,&quot;HitHighlightedMultivaluePropertyLimit&quot;:-1,&quot;IgnoreContextualScope&quot;:true,&quot;ScopeResultsToCurrentSite&quot;:false,&quot;TrimDuplicates&quot;:false,&quot;Properties&quot;:{&quot;TryCache&quot;:true,&quot;Scope&quot;:&quot;{Site.URL}&quot;,&quot;ListId&quot;:&quot;00000000-0000-0000-0000-000000000000&quot;,&quot;UpdateLinksForCatalogItems&quot;:true,&quot;EnableStacking&quot;:true},&quot;PropertiesJson&quot;:&quot;{\&quot;TryCache\&quot;:true,\&quot;Scope\&quot;:\&quot;{Site.URL}\&quot;,\&quot;ListId\&quot;:\&quot;00000000-0000-0000-0000-000000000000\&quot;,\&quot;UpdateLinksForCatalogItems\&quot;:true,\&quot;EnableStacking\&quot;:true}&quot;,&quot;ClientType&quot;:&quot;ContentSearchRegular&quot;,&quot;UpdateAjaxNavigate&quot;:true,&quot;SummaryLength&quot;:180,&quot;DesiredSnippetLength&quot;:90,&quot;PersonalizedQuery&quot;:false,&quot;FallbackRefinementFilters&quot;:null,&quot;IgnoreStaleServerQuery&quot;:false,&quot;RenderTemplateId&quot;:&quot;DefaultDataProvider&quot;,&quot;AlternateErrorMessage&quot;:null,&quot;Title&quot;:&quot;&quot;}" BypassResultTypes="True" ItemTemplateId="~sitecollection/_catalogs/masterpage/Display Templates/Content Web Parts/Related_Articles_Sidebar.js" GroupTemplateId="~sitecollection/_catalogs/masterpage/Display Templates/Content Web Parts/Group_Content.js" ResultsPerPage="5" SelectedPropertiesJson="[&quot;PublishingImage&quot;,&quot;PictureURL&quot;,&quot;PictureThumbnailURL&quot;,&quot;Path&quot;,&quot;Title&quot;,&quot;SitePath&quot;,&quot;ModifiedOWSDATE&quot;,&quot;ListID&quot;,&quot;ListItemID&quot;,&quot;SiteTitle&quot;,&quot;PublishingPageContentOWSHTML&quot;,&quot;ContentTypeId&quot;,&quot;SecondaryFileExtension&quot;]" HitHighlightedPropertiesJson="[&quot;Title&quot;,&quot;Path&quot;,&quot;Author&quot;,&quot;SectionNames&quot;,&quot;SiteDescription&quot;]" AvailableSortsJson="null" ShowBestBets="False" ShowPersonalFavorites="False" ShowDefinitions="False" ShowDidYouMean="False" PreloadedItemTemplateIdsJson="null" QueryGroupName="Default" RenderTemplateId="~sitecollection/_catalogs/masterpage/Display Templates/Content Web Parts/Control_List_LandingPage_LatestArticles.js" StatesJson="{}" ServerIncludeScriptsJson="null" Title="More Articles You May Like" FrameType="TitleBarOnly" SuppressWebPartChrome="False" Description="" IsIncluded="True" ZoneID="ImportedPartZone" PartOrder="0" FrameState="Normal" AllowRemove="True" AllowZoneChange="True" AllowMinimize="True" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" DetailLink="" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="&lt;%$Resources:core,ImportErrorMessage;%&gt;" ImportErrorMessage="&lt;%$Resources:core,ImportErrorMessage;%&gt;" PartImageLarge="" IsIncludedFilter="" ExportControlledProperties="True" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_87045e35_b9c1_4de5_b258_c866ad148624" ChromeType="TitleOnly" ExportMode="All" __MarkupType="vsattributemarkup" __WebPartId="{87045e35-b9c1-4de5-b258-c866ad148624}" WebPart="true" Height="" Width="">
                            </spsswc:ContentBySearchWebPart>
                            
                            
                        </div>
                        <hr />
                    </div>
                </div>
                <div class="col-xs-12" id="greenroom-bottom-zone-full-width">
                    <div data-name="WebPartZone">
                    </div>
                </div>
            </div>
        </asp:Content><asp:Content runat="server" ContentPlaceHolderID="PlaceHolderPageTitle">
            <SharePoint:ProjectProperty Property="Title" runat="server" />
            
            
            <PageFieldFieldValue:FieldValue FieldName="fa564e0f-0c70-4ab9-b863-0177e6ddd247" runat="server">
            </PageFieldFieldValue:FieldValue>
            
        </asp:Content><asp:Content runat="server" ContentPlaceHolderID="PlaceHolderAdditionalPageHead">
            
            
            
            <Publishing:EditModePanel runat="server" id="editmodestyles">
                <SharePoint:CssRegistration name="&lt;% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/editmode15.css %&gt;" After="&lt;% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/pagelayouts15.css %&gt;" runat="server">
                </SharePoint:CssRegistration>
            </Publishing:EditModePanel>
            
        </asp:Content>