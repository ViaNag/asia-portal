﻿
# ===================================================================================
# FUNC: AddListItem
# DESC: Create Item In list
# ===================================================================================
function AddListItem([String]$ConfigFileName = "")
{

	# Check that the config file exists.
	if (-not $(Test-Path -Path $configFileName -Type Leaf))
	{
		Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
	}

	$configXml = [xml]$(get-content $configFileName)
	if( $? -eq $false ) 
	{
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..." | Out-Null
	}

	if ($configXml.Items)
	{
		foreach ($Item in $configXml.Items.Item)
		{
			try
			{
				if($Item.ListName)
				{
					# Break role inheritance of the list
					$web = Get-SPWeb $Item.WebUrl
					
					$ListToAddItem = $web.Lists[$Item.ListName]
					
					if($ListToAddItem)
					{
                        #Create a new item
                        $newItem = $ListToAddItem.Items.Add()

                        foreach ($ItemColumns in $Item.Columns)
		                {
                            
                            switch ($ItemColumns.Type) 
                            { 
                            'Number'         { 
                                                $newItem[$ItemColumns.Key] = $ItemColumns.Value.Trim()
                                                break
                                             }
                            'Note'           { 
                                                $newItem[$ItemColumns.Key] = [System.Web.HttpUtility]::HtmlDecode($ItemColumns.Value.Trim())
                                                break 
                                             } 
                            'User'           { 
                                                $userValue=$null
                                                if(![string]::IsNullOrEmpty($ItemColumns.GroupValue.Trim()))
                                                {
                                                    $groupToAdd=$null
                                                    $groupToAdd = $web.SiteGroups[$ItemColumns.GroupValue.Trim()]
		                                            $userValue = new-object Microsoft.SharePoint.SPFieldUserValue($web, $groupToAdd.ID, $groupToAdd.Name)
                                                }
                                                elseif(![string]::IsNullOrEmpty($ItemColumns.UserValue.Trim()))
                                                {
                                                     $user=$null
                                                     $user = $web.EnsureUser($ItemColumns.UserValue.Trim())
		                                             $userValue = new-object Microsoft.SharePoint.SPFieldUserValue($web, $user.ID, $user.LoginName)
                                                }
                                                $newItem[$ItemColumns.Key] = $userValue
                                                break
                                             }  
                            'Boolean'        { 
                                                if(![string]::IsNullOrEmpty($ItemColumns.Value.Trim()))
                                                {
                                                    $newItem[$ItemColumns.Key] = [System.Convert]::ToBoolean([System.Convert]::ToInt32($ItemColumns.Value.Trim()))
                                                }
                                                break 
                                             }
                            'Hyperlink'      { 
                                                if(![string]::IsNullOrEmpty($ItemColumns.LinkValue.Trim()))
                                                {
                                                    [Microsoft.SharePoint.SPFieldUrlValue] $hyperlinkValue = new-object Microsoft.SharePoint.SPFieldUrlValue
                                                    $hyperlinkValue.Url=$ItemColumns.LinkValue.Trim()
                                                    if(![string]::IsNullOrEmpty($ItemColumns.LinkText.Trim()))
                                                    {
                                                        $hyperlinkValue.Description=$ItemColumns.LinkText.Trim()
                                                    }
                                                    else
                                                    {
                                                        $hyperlinkValue.Description=$ItemColumns.LinkValue.Trim()
                                                    }
                                                    $newItem[$ItemColumns.Key] = $hyperlinkValue
                                                }
                                                break
                                             }
                            'Taxonomy'       { 
                                                if(![string]::IsNullOrEmpty($ItemColumns.Value.Trim()))
                                                {
                                                    $taxField=$null
                                                    $termSetId=$null
                                                    $taxField=$ListToAddItem.Fields[$ItemColumns.Key.trim()]
                                                    $termSetId=$taxField.TermSetId
                                                    if($termSetId -ne $null)
                                                    {
                                                        $site=$null
                                                        $termToLink=$null
                                                        $termValue=$null
                                                        $taxonomyField = [Microsoft.SharePoint.Taxonomy.TaxonomyField]$newItem.Fields[$ItemColumns.Key.trim()]
                                                        $site = $web.Site
                                                        $session = New-Object Microsoft.SharePoint.Taxonomy.TaxonomySession($site)
                                                        $termStore = $session.TermStores[ $Item.MMDServiceName]
                                                        $termSet=$termStore.GetTermSet($termSetId)
                                                        $termToLink=$ItemColumns.Value.Trim() -split ";"
                                                        foreach($term in  $termToLink)
                                                        {
                                                            if($term.contains("&"))
                                                            {
                                                                $term = $term -replace "&","＆"
                                                            }                                                           
                                                            if(![string]::IsNullOrEmpty($term))
                                                            {
                                                                $termValue=$termSet.Terms[$term]
                                                            }
                                                        }
                                                        [Microsoft.SharePoint.Taxonomy.TaxonomyFieldValue]$taxonomyFieldValue = New-Object Microsoft.SharePoint.Taxonomy.TaxonomyFieldValue($taxonomyField)
                                                        $taxonomyFieldValue.Label=$termValue.Labels.ToString()
                                                        $taxonomyFieldValue.TermGuid=$termValue.Id
                                                        $taxonomyFieldValue.WssId=-1
                                                        $newItem[$ItemColumns.Key] = $taxonomyFieldValue
                                                    }
                                                }
                                                break 
                                             }
                            'MultiUser'      { 
                                                $userList=$null
                                                [Microsoft.SharePoint.SPFieldUserValueCollection] $userList = new-object Microsoft.SharePoint.SPFieldUserValueCollection
                                                if(![string]::IsNullOrEmpty($ItemColumns.GroupValue.Trim()))
                                                {
                                                    $allGroupsToAdd=$null
                                                    $allGroupsToAdd=$ItemColumns.GroupValue.Trim() -split ";"
                                                    foreach($spGroup in $allGroupsToAdd)
                                                    {
                                                        $groupToAdd=$null
                                                        $groupToAdd = $web.SiteGroups[$spGroup]
		                                                $userValue = new-object Microsoft.SharePoint.SPFieldUserValue($web, $groupToAdd.ID, $groupToAdd.Name)
                                                        $userList.Add($userValue)
                                                    }
                                                }
                                                if(![string]::IsNullOrEmpty($ItemColumns.UserValue.Trim()))
                                                {
                                                     $allusersToAdd=$null
                                                     $allusersToAdd=$ItemColumns.UserValue.Trim() -split ";"
                                                     foreach($spUser in $allusersToAdd)
                                                     {
                                                         $user=$null
                                                         $user = $web.EnsureUser($spUser)
		                                                 $userValue = new-object Microsoft.SharePoint.SPFieldUserValue($web, $user.ID, $user.LoginName)
                                                         $userList.Add($userValue)
                                                     }
                                                }
                                                $newItem[$ItemColumns.Key] = $userList
                                                break
                                             }
                            'DateTime'       {
                                                if(![string]::IsNullOrEmpty($ItemColumns.Value.Trim()))
                                                {
                                                    $dateValue = Get-Date $ItemColumns.Value.Trim()
                                                    $newItem[$ItemColumns.Key] = $dateValue
                                                }
                                                break
                                             }
                            default          {
                                                #if([string]$ItemColumns.Value.Trim() -eq "M&E Ad Sales")
							                    #{
							                    #   $ItemColumns.Value = "M＆E Ad Sales"
							                    #}
												
                                                $newItem[$ItemColumns.Key] = [System.Web.HttpUtility]::HtmlDecode($ItemColumns.Value.Trim())
                                             }
                            }
                            write-host "Item '$($ItemColumns.Value)' addedd for column '$($ItemColumns.Key)' in list $($Item.ListName) at web $($Item.WebUrl) " -ForegroundColor Green
                            write-output "Item '$($ItemColumns.Value)' addedd for column '$($ItemColumns.Key)' in list $($Item.ListName) at web $($Item.WebUrl) " | Out-Null
                        }
                        $newItem.Update()
                        
					}
					else
					{
						write-host "List-" $Item.ListName ", not found on $($Item.WebUrl)" -ForegroundColor Yellow
                        write-output "List- $($Item.ListName), not found on $($Item.WebUrl)" | Out-Null
					}
				}
				if($Error.Count -gt 0)
				{
					Write-Host "Error adding items. Cause : " $Error -ForegroundColor Red
                    Write-Output "Error adding items. Cause : "$Error | Out-Null
					$Error.Clear()	
				}				
			}
			catch
			{
				Write-Host "Exception while adding items." $Error -ForegroundColor Red
                Write-output "Exception while adding items." $Error | Out-Null
			}
		}
	}
}