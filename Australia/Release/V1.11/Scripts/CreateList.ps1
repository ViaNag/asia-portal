﻿$global:isAllDocumentsView
$global:isAllItemsView
$global:libraryName
# =================================================================================
#
#Main Function to create lists
#
# =================================================================================
function CreateLists([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..."
	}
	
	Write-Host "Successfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Successfully read config file $($ConfigPath) file`n"
    
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
            foreach($lst in $cfg.Libraries.Library)
            {
                ListToAdd $lst
            }
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host "Error: " $Error -ForegroundColor Red
        Write-Output "Error: " $Error
	}
}

# =================================================================================
#
# FUNC: ListToAdd
# DESC: Create list if not exist else associate content type and fields to it.
# =================================================================================
function ListToAdd([object] $lst)
{
    try
    {
        $error.Clear()
        
        $web = Get-SPWeb $lst.SiteURL
		$actionType=$lst.ActionType
		
		$listCompleteUrl=$null
		
		$listTemplate=$web.ListTemplates[$lst.Template]
		if($listTemplate.BaseType -eq "DocumentLibrary")
		{
			$listCompleteUrl=$web.Url +"/"+$lst.LibraryUrl
		}
		else
		{			
			$listCompleteUrl=$web.Url +"/Lists/"+$lst.LibraryUrl
		}
		
		if($actionType -eq "Add" -or $actionType -eq "Update")
		{			
			$listAdded=$null
			if($actionType -eq "Add")
			{
				try
				{
					$web.Lists.Add($lst.LibraryUrl,$lst.Description,$listTemplate) | Out-Null
					$web.update()
					Write-Host "`nList $($lst.LibraryName) created successfully at web : $($lst.SiteURL)" -ForegroundColor Green
					Write-Output "List $($lst.LibraryName) created successfully at web : $($lst.SiteURL)"
					
				}
				catch
				{
					Write-Host "`nList $($lst.LibraryName) already exist at web : $($lst.SiteURL)" -ForegroundColor Yellow
					Write-Output "List $($lst.LibraryName) already exist at web : $($lst.SiteURL)"
				}				
			}
			#Update List
			$listAdded=$web.GetList($listCompleteUrl)
			
			if(![string]::IsNullOrEmpty($lst.ContentType))
			{
				$ct=$web.AvailableContentTypes[$lst.ContentType]
				if(!$listAdded.ContentTypes[$ct.Name])
				{
					$listAdded.ContentTypes.Add($ct) | Out-Null
					Write-Host "ContentType $($lst.ContentType) added to list $($lst.LibraryName) at web : $($lst.SiteURL)" -ForegroundColor Green
					Write-Output "ContentType $($lst.ContentType) added to list $($lst.LibraryName) at web : $($lst.SiteURL)"
				}
				else
				{
					Write-Host "ContentType $($lst.ContentType) already exist in list $($lst.LibraryName) at web : $($lst.SiteURL)"  -ForegroundColor Yellow
					Write-Output "ContentType $($lst.ContentType) already exist in list $($lst.LibraryName) at web : $($lst.SiteURL)" 
				}
				$docSet=$listAdded.ContentTypes[$ct.Name]
				$docSetId=[string]$docSet.Id
				if($docSetId.StartsWith("0x0120D520"))
				{
					
					[Microsoft.Office.DocumentManagement.DocumentSets.DocumentSetTemplate]$docSetCT = [Microsoft.Office.DocumentManagement.DocumentSets.DocumentSetTemplate]::GetDocumentSetTemplate($docSet);
					$LstSharedColumn=$null;
					$AllowContentTypes=$null
					if(![string]::IsNullOrEmpty($lst.ListSharedColumns))
					{
						$LstSharedColumn=[string]$lst.ListSharedColumns
						$LstSharedColumn = $LstSharedColumn.Split(",")
						foreach($addLstSharedCol in $LstSharedColumn)
						{
							if(![string]::IsNullOrEmpty($addLstSharedCol))
							{
								$docSetCT.SharedFields.Add($docSet.Fields.GetFieldByInternalName($addLstSharedCol));
							}
						}
					}

					if(![string]::IsNullOrEmpty($lst.AllowedContentTypes))
					{
						$AllowContentTypes = [string]$lst.AllowedContentTypes
						$AllowContentTypes=$AllowContentTypes.Split(",")
						foreach($addCT in $AllowContentTypes)
						{
							if(![string]::IsNullOrEmpty($addCT))
							{
								$docSetCT.AllowedContentTypes.Add($web.Site.RootWeb.ContentTypes[$addCT].Id);
							}
						}
					}
		
					if(![string]::IsNullOrEmpty($lst.RemoveContentTypesFromDocSet))
					{
						$RemoveContentTypesFromDocSet = [string]$lst.RemoveContentTypesFromDocSet
						$RemoveContentTypesFromDocSet=$RemoveContentTypesFromDocSet.Split(",")
						foreach($removeCT in $RemoveContentTypesFromDocSet)
						{
							if(![string]::IsNullOrEmpty($removeCT))
							{
								$docSetCT.AllowedContentTypes.Remove($web.ContentTypes[$removeCT].Id);
							}
						}
					}

					#set  welcome page fields for Document set content type
					if(![string]::IsNullOrEmpty($lst.WelcomePageFields))
					{
						$welcomePageFields=([string]$lst.WelcomePageFields).Split(",")
						$docSetCT.WelcomePageFields.Clear()
						foreach($wcField in $welcomePageFields)
						{
							if(![string]::IsNullOrEmpty($wcField))
							{
								$docSetCT.WelcomePageFields.Add($docSet.Fields.GetFieldByInternalName($wcField));
							}
						}
					}
					$docSetCT.Update($true);
				}
			}
					
			if(![string]::IsNullOrEmpty($lst.ListFields))
			{
				try
				{
					$field=$web.AvailableFields.GetFieldByInternalName($lst.ListFields)
                          
					if(!$listAdded.Fields.Contains($field.Id))
					{
						$listAdded.Fields.Add($field) | Out-Null
						Write-Host "Field $($lst.ListFields) added to list $($lst.LibraryName) at web : $($lst.SiteURL)" -ForegroundColor Green
						Write-Output "Field $($lst.ListFields) added to list $($lst.LibraryName) at web : $($lst.SiteURL)"
					}
					else
					{
						Write-Host "Field $($lst.ListFields) already exist in list $($lst.LibraryName) at web : $($lst.SiteURL)"  -ForegroundColor Yellow
						Write-Output "Field $($lst.ListFields) already exist in list $($lst.LibraryName) at web :$($lst.SiteURL)" 
					}
				}
				catch
				{
					Write-Host "Field $($lst.ListFields) not exist on web : $($lst.SiteURL)"  -ForegroundColor Yellow
					Write-Output "Field $($lst.ListFields) not exist on web : $($lst.SiteURL)" 
					$error.Clear()
				}
			}

		   #Set site column optional for this list
			$optionalSiteColumns=$lst.OptionalSiteColumns
			foreach($columnInternalName in $optionalSiteColumns.ColumnInternalName)
			{			
				foreach($listCT in $listAdded.ContentTypes)
				{
					$old_ErrorActionPreference = $ErrorActionPreference
					$ErrorActionPreference = 'SilentlyContinue'
					$field=$listCT.FieldLinks[$columnInternalName]
					$ErrorActionPreference = $old_ErrorActionPreference
					if($field)
					{                    
						[boolean]$listCT.FieldLinks[$columnInternalName].Required = $false
						$listCT.Update()
						Write-Host "Field $($columnInternalName) set optional field for contentType: $($listCT.Name) in list $($lst.LibraryName) at web $($lst.SiteURL)" -ForegroundColor Green
						Write-Output "Field $($columnInternalName) set optional field for contentType: $($listCT.Name) in list $($lst.LibraryName) at web $($lst.SiteURL)" 
					}
				}		
			}			
            
            if(![string]::IsNullOrEmpty($lst.Description))
            {
			    $listAdded.Description=$lst.Description
            }

            if(![string]::IsNullOrEmpty($lst.LibraryName))
            {
			    $listAdded.Title=$lst.LibraryName
            }
			$listAdded.Update()
			
			UpdateListProperties $lst $web $listAdded
			if(![string]::IsNullOrEmpty($lst.ContentTypeOrder))
			{
				SetContentTypeOrder $lst $web $listAdded
			}
			
			if(![string]::IsNullOrEmpty($lst.RemoveContentTypes))
			{
				RemoveContentType $lst $web $listAdded
			}
			
			if(![string]::IsNullOrEmpty($lst.KeyFilters))
			{
				$keys = @($lst.KeyFilters.Split(","))
				if($keys)
				{
					ApplyKeyFilterToLibrary $lst.LibraryName $lst.SiteURL $web $keys
				}
			}
			
			if(![string]::IsNullOrEmpty($lst.NavigationKeys))
			{
				$Navigationkeys = @($lst.NavigationKeys.Split(","))
				if($Navigationkeys)
				{
					CreateNavigationHierachyInLibrary $lst.LibraryName $lst.SiteURL $web $Navigationkeys
				}
			}			
		}
		else
		{
			try
			{
				$listToDelete=$web.GetList($listCompleteUrl)
				if(!$listToDelete.AllowDeletion)
				{
					$listToDelete.AllowDeletion=$True
					$listToDelete.update()
				}
				
				$listToDelete.Delete()
				Write-Host "List $($lst.LibraryName) deleted successfully at web : $($lst.SiteURL)" -ForegroundColor Green
				Write-Output "List $($lst.LibraryName) deleted successfully at web : $($lst.SiteURL)"
			}
			catch
			{
				Write-Host "List $($lst.LibraryName) to be deleted does not exists at web : $($lst.SiteURL)" -ForegroundColor Green
				Write-Output "List $($lst.LibraryName) to be deleted does not exists at web : $($lst.SiteURL)"
			}
		}
        $web.Dispose()                
	}
    catch
    {
        Write-Host "`nException for Creating or updating List:" $lst.LibraryName "`n" $Error -ForegroundColor Red
        Write-Output "`nException for Creating or updating List :" $lst.LibraryName "`n" $Error
    }
}
# =================================================================================
#
#Main Function to create list views
#
# =================================================================================
function UpdateListProperties([object] $lstProp,$web,$list)
{
     try
     {
        # Check if the list already exist or not
        if($list)
        {
			#Attaching taxonomy event reciever to list
			$spEventReceiverAdding = $list.EventReceivers.Add()
			$spEventReceiverAdding.Assembly = "Microsoft.SharePoint.Taxonomy, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"
			$spEventReceiverAdding.Class = "Microsoft.SharePoint.Taxonomy.TaxonomyItemEventReceiver"
			$spEventReceiverAdding.Type = "ItemAdding"
			$spEventReceiverAdding.SequenceNumber = 1001
			$spEventReceiverAdding.Update()
			$spEventReceiverUpdating = $list.EventReceivers.Add()
			$spEventReceiverUpdating.Assembly = "Microsoft.SharePoint.Taxonomy, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"
			$spEventReceiverUpdating.Class = "Microsoft.SharePoint.Taxonomy.TaxonomyItemEventReceiver"
			$spEventReceiverUpdating.Type = "ItemUpdating"
			$spEventReceiverUpdating.SequenceNumber = 1002
			$spEventReceiverUpdating.Update()
			$ErrorActionPreference= "silentlycontinue"
            if(![string]::IsNullOrEmpty($lstProp.EnableVersioning))
            {
				[boolean]$list.EnableVersioning=[System.Convert]::ToBoolean(@{$true="false";$false=$lstProp.EnableVersioning}[$lstProp.EnableVersioning -eq ""])
            }
            if(![string]::IsNullOrEmpty($lstProp.ForceCheckout))
            {
				[boolean]$list.ForceCheckout=[System.Convert]::ToBoolean(@{$true="false";$false=$lstProp.ForceCheckout}[$lstProp.ForceCheckout -eq ""])
            }
            if(![string]::IsNullOrEmpty($lstProp.EnableFolderCreation))
            {
				[boolean]$list.EnableFolderCreation=[System.Convert]::ToBoolean(@{$true="false";$false=$lstProp.EnableFolderCreation}[$lstProp.EnableFolderCreation -eq ""])
            }
            if(![string]::IsNullOrEmpty($lstProp.EnableContentType))
            {
				[boolean]$list.ContentTypesEnabled=[System.Convert]::ToBoolean(@{$true="false";$false=$lstProp.EnableContentType}[$lstProp.EnableContentType -eq ""])
            }
			if(![string]::IsNullOrEmpty($lstProp.Hidden))
            {
				[boolean]$list.Hidden=[System.Convert]::ToBoolean(@{$true="false";$false=$lstProp.Hidden}[$lstProp.Hidden -eq ""])
            }	        $list.Update()
			$ErrorActionPreference= "Continue"
			$error.clear()
            Write-Host "List $($lstProp.LibraryName) property updated successfully at web : $($lstProp.SiteURL)"  -ForegroundColor Green
            Write-Output "List $($lstProp.LibraryName) property updated successfully at web : $($lstProp.SiteURL)"
        }
     }
    catch
    {
        Write-Host "`nException on List $($lstProp.LibraryName) while updating property`n" $Error -ForegroundColor Red
        Write-Output "`nException on List $($lstProp.LibraryName) while updating property`n" $Error
    }
}

# =================================================================================
#
# FUNC: SetContentTypeOrder
# DESC: SetContentTypeOrder in list.
# =================================================================================
function SetContentTypeOrder([object] $lstProp,$web,$list)
{
# check if list contains the first content type of the $CTOrder variable (we assume that they have the other content types as well)
    try
    {
		$error.clear()
		if($lstProp.ContentTypeOrder)
		{
			if(($list -ne $null) -and (![string]::IsNullOrEmpty($lstProp.ContentTypeOrder)))
			{
				$CTOrder = $lstProp.ContentTypeOrder -split ","
				if(![string]::IsNullOrEmpty($CTOrder[0]))
				{
					$CTOrder[0]= ([string]$CTOrder[0]).Trim()
					if($list.ContentTypes[$CTOrder[0]] -ne $null)
					{
						# get the current ContentTypeOrder which is a List equivalent in C#
						$currentListOrder = $list.RootFolder.ContentTypeOrder
						# delete the current order - we cannot delete the first one as we need to have one
						for($i = $currentListOrder.Count; $i -gt 0; $i--)
						{
							$lct = $currentListOrder[$i]
							$disableoutput = $currentListOrder.Remove($lct)
						}
						# add the first contenttype to the order so we can remove the old left over content type
						$currentListOrder.Add($list.ContentTypes[$CTOrder[0]])
						# delete the left over content type
						$disableoutput = $currentListOrder.Remove($currentListOrder[0])
						# add the other content types to the order list
						foreach($ctToAdd in $CTOrder | where { $_ -ne $CTOrder[0]})
						{
							$ctToAdd=([string]$ctToAdd).Trim()
							if(![string]::IsNullOrEmpty($ctToAdd))
							{
								if($list.ContentTypes[$ctToAdd] -ne $null)
								{
									$currentListOrder.Add($list.ContentTypes[$ctToAdd])
								}
								else
								{
									Write-host "Content type $($ctToAdd) not exist in list $($list.Title)" -ForegroundColor Yellow
									Write-Output "Content type $($ctToAdd) not exist in list $($list.Title)"
								}
							}
						}
						# update the order list
						$list.RootFolder.UniqueContentTypeOrder = $currentListOrder
						$list.RootFolder.Update()
						# output the list URL of which the list is updated
						Write-Host "List $($lstProp.LibraryName) content type order updated" -ForegroundColor Green
						Write-Output "List $($lstProp.LibraryName) content type order updated"
					}
				}
			}
		}
		
    }
    catch
    {
         Write-Host "`nException : List $($lstProp.LibraryName) while updating content type order`n" -ForegroundColor Red
         Write-Output "`nException : List $($lstProp.LibraryName) while updating content type order`n"
    }
}

# =================================================================================
#
# FUNC: RemoveContentType
# DESC: Remove content type form list.
# =================================================================================
function RemoveContentType([object] $lstProp,$web,$list)
{
    try
    {
		$error.clear()
		if($lstProp.RemoveContentTypes)
		{
			if(($list -ne $null) -and (![string]::IsNullOrEmpty($lstProp.RemoveContentTypes)))
			{
				$CTRemove = $lstProp.RemoveContentTypes -split ","
				if($CTRemove -ne $null)
				{
					if($CTRemove.length -gt 0)
					{
						foreach($ct in $CTRemove)
						{
							$ct=([string]$ct).Trim()
							if(![string]::IsNullOrEmpty($ct))
							{
								$ctToRemove = $list.ContentTypes[$ct]
								if($ctToRemove -ne $null)
								{
									if($list.ContentTypes.Count -gt 1)
									{
										Write-host "Removing content type $($ctToRemove.Name) from list $($list.Title)" -ForegroundColor Green
										Write-Output "Removing content type $($ctToRemove.Name) from list $($list.Title)"
										$list.ContentTypes.Delete($ctToRemove.Id)
										$list.Update()
									}
									else
									{
										Write-host "`nContent type $($ctToRemove.Name) cannot be deleted from list $($list.Title) as list doesnot have more than one content type" -ForegroundColor Yellow
										Write-Output "Content type $($ctToRemove.Name) cannot be deleted from list $($list.Title) as list doesnot have more than one content type`n"
									}
								}
								else
								{
									Write-host "Content type $($ct) not found in list $($list.Title)" -ForegroundColor Yellow
									Write-Output "Content type $($ct) not found in list $($list.Title)"
								}
							}
						}
					}
				}
			}
		}
		
    }
    catch
    {
         Write-Host "`nException : List $($lstProp.LibraryName) while removing content type order`n" -ForegroundColor Red
         Write-Output "`nException : List $($lstProp.LibraryName) while removing content type order`n"
    }
}

# =================================================================================
#
#Main Function to create list views
#
# =================================================================================
function CreateListViews([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $ConfigPath file`n"

	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
			$global:isAllDocumentsView=$false;
			$global:isAllItemsView=$false;
            foreach($VW in $cfg.Views.View)
            {
                ViewsToAdd $VW
            }
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host "Error: " $Error -ForegroundColor Red
        Write-Output "Error: " $Error
	}
}

# =================================================================================
#
# FUNC: ViewsToAdd
# DESC: Create view if not exist else modify it.
# =================================================================================
function ViewsToAdd([object] $VW)
{
    try
    {
        $error.Clear()		
		$site = new-object Microsoft.SharePoint.SPSite $VW.SiteURL
		[boolean]$isPersonalView = [System.Convert]::ToBoolean(@{$true="false";$false=$VW.IsPersonalView}[$VW.IsPersonalView -eq ""])
		if($isPersonalView)
		{
			$rootWeb = $site.rootweb
			$user = $rootWeb.EnsureUser($VW.PersonalViewUser)
			if ($user -eq $null) 
			{
				Write-Host "Personal View User $($VW.PersonalViewUser) not exist at web : $($VW.SiteURL)" -ForegroundColor Green
				Write-Output "Personal View User $($VW.PersonalViewUser) not exist at web : $($VW.SiteURL)" 
			}
			$token = $user.usertoken
			$site = new-object microsoft.sharepoint.spsite($VW.SiteURL, $token)
		}
        
        $web = Get-SPWeb $VW.SiteURL
		
        # Get list
        $list=$web.Lists[$VW.LibraryName]
        if(!$list)
        {
            if($isPersonalView)
            {
                Write-Host "List $($lst.LibraryName) does not exist Or Personal View User $($VW.PersonalViewUser) does not have permission on given list at web : $($VW.SiteURL)" -ForegroundColor Green
                Write-Output "List $($lst.LibraryName) does not exist Or Personal View User $($VW.PersonalViewUser) does not have permission on given list at web : $($VW.SiteURL)"
            }
            else
            {
                Write-Host "List $($lst.LibraryName) not exist at web : " $VW.SiteURL -ForegroundColor Green
                Write-Output "List $($lst.LibraryName) not exist at web : " $VW.SiteURL
            }
        }
        else
        {
            #Query property
            $grpQuery=""
            $sortQuery=""
			$filterQuery=""
			if(![string]::IsNullOrEmpty($VW.FilterByQuery))
            {
				$filterQuery=[System.Web.HttpUtility]::HtmlDecode($VW.FilterByQuery)
            }
            if(![string]::IsNullOrEmpty($VW.GroupBy))
            {
                    $grpQuery="<GroupBy Collapse='TRUE' GroupLimit='30'>"
                    $grpFields = @($VW.GroupBy.Split(","))
                    foreach($field in $grpFields)
                    {
                        if(![string]::IsNullOrEmpty($field))
						{
							$grpQuery= $grpQuery + "<FieldRef Name='" + $field + "' />"
						}
                    }
                     $grpQuery= $grpQuery + "</GroupBy>"
            }
            if(![string]::IsNullOrEmpty($VW.SortingBy))
            {
                    $sortQuery="<OrderBy>"
                    $sortFields = @($VW.SortingBy.Split(","))
                    foreach($field in $sortFields)
                    {
                        if(![string]::IsNullOrEmpty($field))
						{
							$sortQuery= $sortQuery + "<FieldRef Name='" + $field + "' />"
						}
                    }
                     $sortQuery= $sortQuery + "</OrderBy>"
            }
			$viewQuery = $filterQuery
			
			if($filterQuery -NotLike "*<GroupBy>*" -And $filterQuery -NotLike "*</GroupBy>*")
			{
				$viewQuery =$viewQuery + $grpQuery
			}
			
			if($filterQuery -NotLike "*<OrderBy>*" -And $filterQuery -NotLike "*</OrderBy>*")
			{
				$viewQuery =$viewQuery + $sortQuery
			}
            
            $existingView=$list.Views[$VW.ViewName]
            $viewRowLimit = 30
            $viewPaged = $true
            [boolean]$viewDefaultView = [System.Convert]::ToBoolean(@{$true="false";$false=$VW.IsDefaultView}[$VW.IsDefaultView -eq ""])
            [boolean]$viewRecursive=[System.Convert]::ToBoolean(@{$true="false";$false=$VW.Recursive}[$VW.Recursive -eq ""])
            if(!$existingView)
            {				
                $viewFields = New-Object System.Collections.Specialized.StringCollection
                $viewFields.Add($VW.Fields) > $null

                #Create the view in the destination list
				$newview =$null
				if($isPersonalView)
				{
					$newview = $list.Views.Add($VW.ViewName, $viewFields, $viewQuery, $viewRowLimit, $viewPaged, $viewDefaultView,"Invalid",$isPersonalView)
				}
				else
				{
					$newview = $list.Views.Add($VW.ViewName, $viewFields, $viewQuery, $viewRowLimit, $viewPaged, $viewDefaultView)
				}
                Write-Host ("View '" + $newview.Title + "' created in list '" + $list.Title + "' for list:$($VW.LibraryName) on site " + $web.Url) -ForegroundColor Green
                Write-Output ("View '" + $newview.Title + "' created in list '" + $list.Title + "' for list:$($VW.LibraryName) on site " + $web.Url)
                $existingView=$newview
            }
            else
            {
				if(![string]::IsNullOrEmpty($VW.Fields))
				{					
					$viewFields = $existingView.ViewFields.ToStringCollection()
					if($global:libraryName -ne $VW.LibraryName)
					{
						$global:isAllItemsView=$false
					}
					if($VW.ViewName -eq  "All Documents")
					{
						if(!$global:isAllDocumentsView)
						{
							$global:libraryName=$VW.LibraryName
							foreach($field in $viewFields)
							{
								$existingView.ViewFields.Delete($field)
								$existingView.Update()
							}						
						}
						$global:isAllDocumentsView=$true
					}				
					elseif($VW.ViewName -eq  "All Items")
					{						
						if(!$global:isAllItemsView)
						{
							$global:libraryName=$VW.LibraryName
							foreach($field in $viewFields)
							{
								$existingView.ViewFields.Delete($field)
								$existingView.Update()
							}						
						}
						$global:isAllItemsView=$true
					}
					
					
					$viewFields = $existingView.ViewFields.ToStringCollection()
					if($viewFields.Contains($VW.Fields))
					{
						Write-Host ("Field '" + $VW.Fields + "' already added in view '" + $VW.ViewName + "' for list:$($VW.LibraryName) on site " + $web.Url) -ForegroundColor Green
						Write-Output ("Field '" + $VW.Fields + "' already added in view '" + $VW.ViewName + "' for list:$($VW.LibraryName) for list:$($VW.LibraryName) on site " + $web.Url)
						$existingView.ViewFields.MoveFieldTo($VW.Fields,$existingView.ViewFields.Count)
						$existingView.Update()   
					}
					else
					{
						$existingView.ViewFields.add($VW.Fields)
						$existingView.Update()
						Write-Host ("Field '" + $VW.Fields + "' added in view '" + $VW.ViewName + "' for list:$($VW.LibraryName) on site " + $web.Url) -ForegroundColor Green
						Write-Output ("Field '" + $VW.Fields + "' added in view '" + $VW.ViewName + "' for list:$($VW.LibraryName) on site " + $web.Url)
					}
				}
			}
			
            if(![string]::IsNullOrEmpty($VW.IsDefaultView))
            {
                $existingView.DefaultView=$viewDefaultView
            }
            if(![string]::IsNullOrEmpty($VW.Recursive))
            {
				if($viewRecursive)
				{
                    $existingView.Scope=[Microsoft.SharePoint.SPViewScope]::Recursive
                }
                else
                {
                    $existingView.Scope=[Microsoft.SharePoint.SPViewScope]::Default
                }
            }

			if(![string]::IsNullOrEmpty($viewQuery))
			{
				$existingView.Query=$viewQuery
			}

            if(![string]::IsNullOrEmpty($viewPaged))
            {
				$existingView.Paged=$viewPaged
            }
             
			if(![string]::IsNullOrEmpty($viewRowLimit))
			{
				$existingView.RowLimit=$viewRowLimit
			}  

			if(![string]::IsNullOrEmpty($VW.JSLink))
			{
				$existingView.JSLink=$VW.JSLink
			}
             $existingView.Update()
        }
		$web.Dispose() 
        $site.Dispose()         
	}
    catch
    {
        Write-Host "`nException for View : $($VW.ViewName) for list: $($VW.LibraryName) " $Error -ForegroundColor Red
        Write-Output "`nException for View : $($VW.ViewName) for list: $($VW.LibraryName) `n" $Error
    }		
}

# ===================================================================================
# FUNC: ApplyKeyFilterToLibrary
# DESC: Given some xml containing library and keys to set filters on it
# ===================================================================================
function ApplyKeyFilterToLibrary($library,$url,$web,$keys)
{	
    try
    {
        $Error.Clear()
        $list = $web.Lists[$library]
        try
        {
            $listNavSettings = [Microsoft.Office.DocumentManagement.MetadataNavigation.MetadataNavigationSettings]::GetMetadataNavigationSettings($list)
            $listNavSettings.ClearConfiguredHierarchies()
            $listNavSettings.ClearConfiguredKeyFilters()

            #Configure key filters by adding columns
            foreach($key in $keys)
            {
                if(![string]::IsNullOrEmpty($key))
                {
                    $listNavSettings.AddConfiguredKeyFilter($list.Fields.GetFieldByInternalName($key))
                }
            }
            [Microsoft.Office.DocumentManagement.MetadataNavigation.MetadataNavigationSettings]::SetMetadataNavigationSettings($list, $listNavSettings, $true)
            $list.RootFolder.Update()
            Write-Host "Success : Key filters for library $($library) set."  -ForegroundColor Green
            Write-Output "Success : Key filters for library $($library) set." 
        }
        catch
        {
            Write-Host "Exception: Configuring key filters for library $($library) for web :$($url)"  $Error -ForegroundColor Red
            Write-Output "Exception: Configuring key filters for library $($library) for web :$($url)" $Error 
        }
    }
    catch
    {
        Write-Host "Exception: Fetching library $($library) for web : " $url $Error -ForegroundColor Red
        Write-Output "Exception: Fetching library $($library) for web : " $url $Error 
    }
    Write-Host "Key filter for libraries in web $($url) set successfully" -ForegroundColor Green
    Write-Output "Key filter for libraries in web $($url) set successfully"
}


# ===================================================================================
# FUNC: CreateNavigationHierachyInLibrary
# DESC: Given some xml containing library and keys to set Navigation hierachy on it
# ===================================================================================
function CreateNavigationHierachyInLibrary($library,$url,$web,$keys)
{	
    try
    {
        $Error.Clear()
        $list = $web.Lists[$library]
        try
        {
            $listNavSettings = [Microsoft.Office.DocumentManagement.MetadataNavigation.MetadataNavigationSettings]::GetMetadataNavigationSettings($list)
            $listNavSettings.ClearConfiguredHierarchies()

            #Add folder navigation hierarchi$listes into list settings
            #This is required to enable and show navigation hierarchies in the Tree View
            $folderHierarchy = [Microsoft.Office.DocumentManagement.MetadataNavigation.MetadataNavigationHierarchy]::CreateFolderHierarchy()
            $listnavSettings.AddConfiguredHierarchy($folderHierarchy)

            #Configure Navigation hierachy by adding columns
            foreach($key in $keys)
            {
                if(![string]::IsNullOrEmpty($key))
                {
                    $listNavSettings.AddConfiguredHierarchy($list.Fields.GetFieldByInternalName($key))
                }
            }
            [Microsoft.Office.DocumentManagement.MetadataNavigation.MetadataNavigationSettings]::SetMetadataNavigationSettings($list, $listNavSettings, $true)
            $list.RootFolder.Update()
            Write-Host "Success : Navigation hierachy for library $($library) set."  -ForegroundColor Green
            Write-Output "Success : Navigation hierachy for library $($library) set." 
        }
        catch
        {
            Write-Host "Exception: Configuring Navigation hierachy for library $($library) for web : " $url $Error -ForegroundColor Red
            Write-Output "Exception: Configuring Navigation hierachy for library $($library) for web : " $url $Error 
        }
    }
    catch
    {
        Write-Host "Exception: Fetching library $($library) for web : " $url $Error -ForegroundColor Red
        Write-Output "Exception: Fetching library $($library) for web : " $url $Error 
    }
	Write-Host "Navigation hierachy for libraries in web $($url) set successfully" -ForegroundColor Green
	Write-Output "Navigation hierachy for libraries in web $($url) set successfully"
}

# =================================================================================
#
#Function to set Lists Document Set welcome page view
#
# =================================================================================

function SetListsDocSetWelcomePageView([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $($ConfigPath) file"
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
            foreach($lst in $cfg.Libraries.Library)
            {
                SetListDocSetWelcomePageView $lst
            }
        }
        catch
        {
            Write-Host "`nException in SetListsDocSetWelcomePageView function:" $Error -ForegroundColor Red
            Write-Output "`nException in SetListsDocSetWelcomePageView function:" $Error
        }
	}
	else
	{
		Write-Host "Error: " $Error -ForegroundColor Red
        Write-Output "Error: " $Error
	}
}

# =================================================================================
#
#Function to set List Document Set welcome page view
#
# =================================================================================

function SetListDocSetWelcomePageView([object] $lst)
{
    try
    {
        $error.Clear()
        $site = new-object Microsoft.SharePoint.SPSite $lst.SiteURL
        
        $web = $site.OpenWeb()
       
		$listAdded=$null
        $listCompleteUrl=$null
        $listTemplate=$web.ListTemplates[$lst.Template]       
		
        if($listTemplate.BaseType -eq "DocumentLibrary")
        {
            $listCompleteUrl=$web.Url +"/"+$lst.LibraryUrl
        }
        else
        {
            
            $listCompleteUrl=$web.Url +"/Lists/"+$lst.LibraryUrl
        }
    
		$listAdded=$web.GetList($listCompleteUrl)
        if(![string]::IsNullOrEmpty($lst.ContentType))
        {
            $ct=$web.AvailableContentTypes[$lst.ContentType]
            
            $docSet=$listAdded.ContentTypes[$ct.Name]
            $docSetId=[string]$docSet.Id
			if($docSetId.StartsWith("0x0120D520"))
			{
				
				[Microsoft.Office.DocumentManagement.DocumentSets.DocumentSetTemplate]$docSetCT = [Microsoft.Office.DocumentManagement.DocumentSets.DocumentSetTemplate]::GetDocumentSetTemplate($docSet);
						
				#set  welcome page Views for Document set content type
				if(![string]::IsNullOrEmpty($lst.WelcomePageViewName))
				{
                    $welcomePageView=$listAdded.Views[$lst.WelcomePageViewName]
                    if($welcomePageView)
                    {
				        $docSetCT.WelcomePageView = $listAdded.Views[$lst.WelcomePageViewName]
                        $docSetCT.Update($true)
                        $listAdded.Update()
                        Write-Host "Set View $($lst.WelcomePageViewName) as welcome page view for library $($lst.LibraryName) at web : " $lst.SiteURL -ForegroundColor Green
                        Write-Output "Set View $($lst.WelcomePageViewName) as welcome page view for library $($lst.LibraryName) at web : " $lst.SiteURL
                    }
                    else
                    {
                        Write-Host "View $($lst.WelcomePageViewName) not exist in list $($lst.LibraryName) at web : " $lst.SiteURL -ForegroundColor Yellow
                        Write-Output "View $($lst.WelcomePageViewName) not exist in list $($lst.LibraryName) at web : " $lst.SiteURL
                    }						
				}
				
			}
        }
        
        $web.Dispose()
	}
    catch
    {
        Write-Host "`nException in SetListDocSetWelcomePageView for list :" $lst.LibraryName "`n" $Error -ForegroundColor Red
        Write-Output "`nException in SetListDocSetWelcomePageView for list :" $lst.LibraryName "`n" $Error
    }
}
