<xsl:stylesheet 
  version="1.0" 
  exclude-result-prefixes="x d xsl msxsl cmswrt"
  xmlns:x="http://www.w3.org/2001/XMLSchema" 
  xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" 
  xmlns:cmswrt="http://schemas.microsoft.com/WebParts/v3/Publishing/runtime"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime">
  <xsl:param name="ItemsHaveStreams">
    <xsl:value-of select="'False'" />
  </xsl:param>
  <xsl:variable name="OnClickTargetAttribute" select="string('javascript:this.target=&quot;_blank&quot;')" />
  <xsl:variable name="ImageWidth" />
  <xsl:variable name="ImageHeight" />
  <xsl:template name="Default" match="*" mode="itemstyle">
        <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="SafeImageUrl">
            <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
                <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="DisplayTitle">
            <xsl:call-template name="OuterTemplate.GetTitle">
                <xsl:with-param name="Title" select="@Title"/>
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <div class="item">
            <xsl:if test="string-length($SafeImageUrl) != 0">
                <div class="image-area-left"> 
                    <a href="{$SafeLinkUrl}">
                      <xsl:if test="$ItemsHaveStreams = 'True'">
                        <xsl:attribute name="onclick">
                          <xsl:value-of select="@OnClickForWebRendering"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
                        <xsl:attribute name="onclick">
                          <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
                        </xsl:attribute>
                      </xsl:if>
                      <img class="image" src="{$SafeImageUrl}" title="{@ImageUrlAltText}">
                        <xsl:if test="$ImageWidth != ''">
                          <xsl:attribute name="width">
                            <xsl:value-of select="$ImageWidth" />
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="$ImageHeight != ''">
                          <xsl:attribute name="height">
                            <xsl:value-of select="$ImageHeight" />
                          </xsl:attribute>
                        </xsl:if>
                      </img>
                    </a>
                </div>
            </xsl:if>
            <div class="link-item">
              <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
               <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
                  <xsl:if test="$ItemsHaveStreams = 'True'">
                    <xsl:attribute name="onclick">
                      <xsl:value-of select="@OnClickForWebRendering"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
                    <xsl:attribute name="onclick">
                      <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="$DisplayTitle"/>
                </a>    <!--<a>
    			    			<xsl:attribute name="href">
    				<xsl:value-of select="substring-before(@Link,',')"/>
    			</xsl:attribute>
    			<xsl:attribute name="target">
    			<xsl:choose>
    				<xsl:when test="@NewWindow = 'Yes'">
    					<xsl:value-of select="string('_blank')"/>
    				</xsl:when>
    				<xsl:otherwise>
    					<xsl:value-of select="string('_self')"/>
    				</xsl:otherwise>
    			</xsl:choose>
    			</xsl:attribute>
    				<xsl:value-of select="@Title"/>

    	

    			</a>-->

        

                <div class="description">
                    <xsl:value-of select="@Description" />
                </div>
            </div>
        </div>
    </xsl:template>
    <xsl:template name="NoImage" match="Row[@Style='NoImage']" mode="itemstyle">
        <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="DisplayTitle">
            <xsl:call-template name="OuterTemplate.GetTitle">
                <xsl:with-param name="Title" select="@Title"/>
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <div class="item link-item">
            <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
            <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
              <xsl:if test="$ItemsHaveStreams = 'True'">
                <xsl:attribute name="onclick">
                  <xsl:value-of select="@OnClickForWebRendering"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
                <xsl:attribute name="onclick">
                  <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:value-of select="$DisplayTitle"/>
            </a>
            <div class="description">
                <xsl:value-of select="@Description" />
            </div>
        </div>
    </xsl:template>
    <xsl:template name="TitleOnly" match="Row[@Style='TitleOnly']" mode="itemstyle">
        <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="DisplayTitle">
            <xsl:call-template name="OuterTemplate.GetTitle">
                <xsl:with-param name="Title" select="@Title"/>
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
      <div class="item link-item">
        <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
        <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
          <xsl:if test="$ItemsHaveStreams = 'True'">
            <xsl:attribute name="onclick">
              <xsl:value-of select="@OnClickForWebRendering"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
            <xsl:attribute name="onclick">
              <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:value-of select="$DisplayTitle"/>
        </a>
      </div>
    </xsl:template>
    <xsl:template name="TitleWithBackground" match="Row[@Style='TitleWithBackground']" mode="itemstyle">
        <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="DisplayTitle">
            <xsl:call-template name="OuterTemplate.GetTitle">
                <xsl:with-param name="Title" select="@Title"/>
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <div class="title-With-Background">
            <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
            <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
              <xsl:if test="$ItemsHaveStreams = 'True'">
                <xsl:attribute name="onclick">
                  <xsl:value-of select="@OnClickForWebRendering"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
                <xsl:attribute name="onclick">
                  <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:value-of select="$DisplayTitle"/>
            </a>
        </div>
    </xsl:template>
    <xsl:template name="Bullets" match="Row[@Style='Bullets']" mode="itemstyle">
        <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="DisplayTitle">
            <xsl:call-template name="OuterTemplate.GetTitle">
                <xsl:with-param name="Title" select="@Title"/>
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <div class="item link-item bullet">
            <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
            <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
              <xsl:if test="$ItemsHaveStreams = 'True'">
                <xsl:attribute name="onclick">
                  <xsl:value-of select="@OnClickForWebRendering"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
                <xsl:attribute name="onclick">
                  <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:value-of select="$DisplayTitle"/>
            </a>
        </div>
    </xsl:template>
    <xsl:template name="ImageRight" match="Row[@Style='ImageRight']" mode="itemstyle">
        <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="SafeImageUrl">
            <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
                <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="DisplayTitle">
            <xsl:call-template name="OuterTemplate.GetTitle">
                <xsl:with-param name="Title" select="@Title"/>
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <div class="item">
            <xsl:if test="string-length($SafeImageUrl) != 0">
                <div class="image-area-right">
                    <a href="{$SafeLinkUrl}">
                      <xsl:if test="$ItemsHaveStreams = 'True'">
                        <xsl:attribute name="onclick">
                          <xsl:value-of select="@OnClickForWebRendering"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
                        <xsl:attribute name="onclick">
                          <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
                        </xsl:attribute>
                      </xsl:if>
                      <img class="image" src="{$SafeImageUrl}" title="{@ImageUrlAltText}">
                        <xsl:if test="$ImageWidth != ''">
                          <xsl:attribute name="width">
                            <xsl:value-of select="$ImageWidth" />
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="$ImageHeight != ''">
                          <xsl:attribute name="height">
                            <xsl:value-of select="$ImageHeight" />
                          </xsl:attribute>
                        </xsl:if>
                      </img>
                    </a>
                </div>
            </xsl:if>
            <div class="link-item">
              <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
                <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
                  <xsl:if test="$ItemsHaveStreams = 'True'">
                    <xsl:attribute name="onclick">
                      <xsl:value-of select="@OnClickForWebRendering"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
                    <xsl:attribute name="onclick">
                      <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="$DisplayTitle"/>
                </a>
                <div class="description">
                    <xsl:value-of select="@Description" />
                </div>
            </div>
        </div>
    </xsl:template>
    <xsl:template name="ImageTop" match="Row[@Style='ImageTop']" mode="itemstyle">
        <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="SafeImageUrl">
            <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
                <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="DisplayTitle">
            <xsl:call-template name="OuterTemplate.GetTitle">
                <xsl:with-param name="Title" select="@Title"/>
                <xsl:with-param name="Url" select="@LinkUrl"/>
            </xsl:call-template>
        </xsl:variable>
        <div class="item">
            <xsl:if test="string-length($SafeImageUrl) != 0">
                <div class="image-area-top">
                    <a href="{$SafeLinkUrl}">
                      <xsl:if test="$ItemsHaveStreams = 'True'">
                        <xsl:attribute name="onclick">
                          <xsl:value-of select="@OnClickForWebRendering"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
                        <xsl:attribute name="onclick">
                          <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
                        </xsl:attribute>
                      </xsl:if>
                      <img class="image" src="{$SafeImageUrl}" title="{@ImageUrlAltText}">
                        <xsl:if test="$ImageWidth != ''">
                          <xsl:attribute name="width">
                            <xsl:value-of select="$ImageWidth" />
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="$ImageHeight != ''">
                          <xsl:attribute name="height">
                            <xsl:value-of select="$ImageHeight" />
                          </xsl:attribute>
                        </xsl:if>
                      </img>
                    </a>
                </div>
            </xsl:if>
            <div class="link-item">
                <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
                <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
                  <xsl:if test="$ItemsHaveStreams = 'True'">
                    <xsl:attribute name="onclick">
                      <xsl:value-of select="@OnClickForWebRendering"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
                    <xsl:attribute name="onclick">
                      <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="$DisplayTitle"/>
                </a>
                <div class="description">
                    <xsl:value-of select="@Description" />
                </div>
            </div>
        </div>
    </xsl:template>
    <xsl:template name="ImageTopCentered" match="Row[@Style='ImageTopCentered']" mode="itemstyle">
        <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="SafeImageUrl">
            <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
                <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="DisplayTitle">
            <xsl:call-template name="OuterTemplate.GetTitle">
                <xsl:with-param name="Title" select="@Title"/>
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <div class="item centered">
            <xsl:if test="string-length($SafeImageUrl) != 0">
                <div class="image-area-top">
                    <a href="{$SafeLinkUrl}" >
                      <xsl:if test="$ItemsHaveStreams = 'True'">
                        <xsl:attribute name="onclick">
                          <xsl:value-of select="@OnClickForWebRendering"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
                        <xsl:attribute name="onclick">
                          <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
                        </xsl:attribute>
                      </xsl:if>
                      <img class="image" src="{$SafeImageUrl}" title="{@ImageUrlAltText}">
                        <xsl:if test="$ImageWidth != ''">
                          <xsl:attribute name="width">
                            <xsl:value-of select="$ImageWidth" />
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="$ImageHeight != ''">
                          <xsl:attribute name="height">
                            <xsl:value-of select="$ImageHeight" />
                          </xsl:attribute>
                        </xsl:if>
                      </img>
                    </a>
                </div>
            </xsl:if>
            <div class="link-item">
                <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
                <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
                  <xsl:if test="$ItemsHaveStreams = 'True'">
                    <xsl:attribute name="onclick">
                      <xsl:value-of select="@OnClickForWebRendering"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
                    <xsl:attribute name="onclick">
                      <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="$DisplayTitle"/>
                </a>
                <div class="description">
                    <xsl:value-of select="@Description" />
                </div>
            </div>
        </div>
    </xsl:template>
    <xsl:template name="LargeTitle" match="Row[@Style='LargeTitle']" mode="itemstyle">
        <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="SafeImageUrl">
            <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
                <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="DisplayTitle">
            <xsl:call-template name="OuterTemplate.GetTitle">
                <xsl:with-param name="Title" select="@Title"/>
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <div class="item">
            <xsl:if test="string-length($SafeImageUrl) != 0">
                <div class="image-area-left">
                    <a href="{$SafeLinkUrl}">
                      <xsl:if test="$ItemsHaveStreams = 'True'">
                        <xsl:attribute name="onclick">
                          <xsl:value-of select="@OnClickForWebRendering"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
                        <xsl:attribute name="onclick">
                          <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
                        </xsl:attribute>
                      </xsl:if>
                      <img class="image" src="{$SafeImageUrl}" title="{@ImageUrlAltText}">
                        <xsl:if test="$ImageWidth != ''">
                          <xsl:attribute name="width">
                            <xsl:value-of select="$ImageWidth" />
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="$ImageHeight != ''">
                          <xsl:attribute name="height">
                            <xsl:value-of select="$ImageHeight" />
                          </xsl:attribute>
                        </xsl:if>
                      </img>
                    </a>
                </div>
            </xsl:if>
            <div class="link-item-large">
                <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
                <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
                  <xsl:if test="$ItemsHaveStreams = 'True'">
                    <xsl:attribute name="onclick">
                      <xsl:value-of select="@OnClickForWebRendering"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
                    <xsl:attribute name="onclick">
                      <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="$DisplayTitle"/>
                </a>
                <div class="description">
                    <xsl:value-of select="@Description" />
                </div>
            </div>
        </div>
    </xsl:template>
    <xsl:template name="ClickableImage" match="Row[@Style='ClickableImage']" mode="itemstyle">
        <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="SafeImageUrl">
            <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
                <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <div class="item">
            <xsl:if test="string-length($SafeImageUrl) != 0">
                <div class="image-area-left">
                    <a href="{$SafeLinkUrl}">
                      <xsl:if test="$ItemsHaveStreams = 'True'">
                        <xsl:attribute name="onclick">
                          <xsl:value-of select="@OnClickForWebRendering"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
                        <xsl:attribute name="onclick">
                          <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
                        </xsl:attribute>
                      </xsl:if>
                      <img class="image" src="{$SafeImageUrl}" title="{@ImageUrlAltText}">
                        <xsl:if test="$ImageWidth != ''">
                          <xsl:attribute name="width">
                            <xsl:value-of select="$ImageWidth" />
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="$ImageHeight != ''">
                          <xsl:attribute name="height">
                            <xsl:value-of select="$ImageHeight" />
                          </xsl:attribute>
                        </xsl:if>
                      </img>
                    </a>
                </div>
            </xsl:if>
        </div>
    </xsl:template>
    <xsl:template name="NotClickableImage" match="Row[@Style='NotClickableImage']" mode="itemstyle">
        <xsl:variable name="SafeImageUrl">
            <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
                <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <div class="item">
            <xsl:if test="string-length($SafeImageUrl) != 0">
                <div class="image-area-left">
                  <img class="image" src="{$SafeImageUrl}" title="{@ImageUrlAltText}">
                    <xsl:if test="$ImageWidth != ''">
                      <xsl:attribute name="width">
                        <xsl:value-of select="$ImageWidth" />
                      </xsl:attribute>
                    </xsl:if>
                    <xsl:if test="$ImageHeight != ''">
                      <xsl:attribute name="height">
                        <xsl:value-of select="$ImageHeight" />
                      </xsl:attribute>
                    </xsl:if>
                  </img>
                </div>
            </xsl:if>
        </div>
    </xsl:template>
    <xsl:template name="FixedImageSize" match="Row[@Style='FixedImageSize']" mode="itemstyle">
        <xsl:variable name="SafeImageUrl">
            <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
                <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="DisplayTitle">
            <xsl:call-template name="OuterTemplate.GetTitle">
                <xsl:with-param name="Title" select="@Title"/>
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <div class="item">
            <xsl:if test="string-length($SafeImageUrl) != 0">
                <div class="image-area-left">
                    <a href="{$SafeLinkUrl}">
                      <xsl:if test="$ItemsHaveStreams = 'True'">
                        <xsl:attribute name="onclick">
                          <xsl:value-of select="@OnClickForWebRendering"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
                        <xsl:attribute name="onclick">
                          <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
                        </xsl:attribute>
                      </xsl:if>
                      <img class="image-fixed-width" src="{$SafeImageUrl}" title="{@ImageUrlAltText}"/>
                    </a>
                </div>
            </xsl:if>
            <div class="link-item">
	            <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
                <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
                  <xsl:if test="$ItemsHaveStreams = 'True'">
                    <xsl:attribute name="onclick">
                      <xsl:value-of select="@OnClickForWebRendering"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
                    <xsl:attribute name="onclick">
                      <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="$DisplayTitle"/>
                </a>
                <div class="description">
                    <xsl:value-of select="@Description" />
                </div>
            </div>
        </div>
	</xsl:template>
  <xsl:template name="WithDocIcon" match="Row[@Style='WithDocIcon']" mode="itemstyle">
       <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                 <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
       </xsl:variable>
       <xsl:variable name="DisplayTitle">
            <xsl:call-template name="OuterTemplate.GetTitle">
                <xsl:with-param name="Title" select="''"/>
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
                <xsl:with-param name="UseFileName" select="1"/>
            </xsl:call-template>
       </xsl:variable>
       <div class="item link-item">
           <xsl:if test="string-length(@DocumentIconImageUrl) != 0">
               <div class="image-area-left">
                   <img class="image" src="{@DocumentIconImageUrl}" title="" />
               </div>
           </xsl:if>
           <div class="link-item">
               <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
               <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
                   <xsl:if test="$ItemsHaveStreams = 'True'">
                     <xsl:attribute name="onclick">
                       <xsl:value-of select="@OnClickForWebRendering"/>
                     </xsl:attribute>
                   </xsl:if>
                   <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
                     <xsl:attribute name="onclick">
                       <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
                     </xsl:attribute>
                   </xsl:if>
                   <xsl:value-of select="$DisplayTitle"/>
               </a>
               <div class="description">
                   <xsl:value-of select="@Description" />
               </div>
           </div>
       </div>
  </xsl:template>
  <xsl:template name="HiddenSlots" match="Row[@Style='HiddenSlots']" mode="itemstyle">
    <div class="SipAddress">
      <xsl:value-of select="@SipAddress" />
    </div>
    <div class="LinkToolTip">
      <xsl:value-of select="@LinkToolTip" />
    </div>
    <div class="OpenInNewWindow">
      <xsl:value-of select="@OpenInNewWindow" />
    </div>
    <div class="OnClickForWebRendering">
      <xsl:value-of select="@OnClickForWebRendering" />
    </div>
  </xsl:template>
<xsl:template name="BenefitSelf" match="Row[@Style='BenefitSelf']" mode="itemstyle">

        <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="SafeImageUrl">
            <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
                <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="DisplayTitle">
            <xsl:call-template name="OuterTemplate.GetTitle">
                <xsl:with-param name="Title" select="@Title"/>
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
     
        <div class="greenroom-item">
            <xsl:if test="string-length($SafeImageUrl) != 0">
                <div class="greenroom-selfcontainer"> 
                            <div class="greenroom-row">
                               <a>
                                    

    			    			<xsl:attribute name="href">
    				<xsl:value-of select="substring-before(@Link,',')"/>
    			</xsl:attribute>
    			<xsl:attribute name="target">
    			<xsl:choose>
    				<xsl:when test="@NewWindow = 'Yes'">
    					<xsl:value-of select="string('_blank')"/>
    				</xsl:when>
    				<xsl:otherwise>
    					<xsl:value-of select="string('_self')"/>
    				</xsl:otherwise>
    			</xsl:choose>
    			</xsl:attribute>
    			  <img class="greenroom-selfimage" src="{$SafeImageUrl}" title="{@ImageUrlAltText}">
                        <xsl:if test="$ImageWidth != ''">
                          <xsl:attribute name="width">
                            <xsl:value-of select="$ImageWidth" />
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="$ImageHeight != ''">
                          <xsl:attribute name="height">
                            <xsl:value-of select="$ImageHeight" />
                          </xsl:attribute>
                        </xsl:if>
                      </img>
    				<span class="greenroom-selftitle"><xsl:value-of select="@Title"/></span>


    			</a>
    			</div>        
                </div>
            </xsl:if>
            <div class="link-item">
              <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
             
                <div class="description">
                    <xsl:value-of select="@Description" />
                </div>
            </div>
        </div>
    </xsl:template>
          <xsl:template name="LandingPagecontent" match="Row[@Style='LandingPagecontent']" mode="itemstyle">

        <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="SafeImageUrl">
            <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
                <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="DisplayTitle">
            <xsl:call-template name="OuterTemplate.GetTitle">
                <xsl:with-param name="Title" select="@Title"/>
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
              </xsl:variable>
              


        <div class="greenroom-item">
       
      <!-- <xsl:if test="string-length($SafeImageUrl) != 0">-->


               <div class="twocolumn "> 
        
                
     <!-- <xsl:if test="string-length($SafeImageUrl) != 0">

   <span class="greenroom-col1">
                            
    			  <img src="{$SafeImageUrl}" title="{@ImageUrlAltText}">
                        <xsl:if test="$ImageWidth != ''">
                          <xsl:attribute name="width">
                            <xsl:value-of select="$ImageWidth" />
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="$ImageHeight != ''">
                          <xsl:attribute name="height">
                            <xsl:value-of select="$ImageHeight" />
                          </xsl:attribute>
                         </xsl:if>

                      </img>
        
    			
    				

    			
    			</span> 

    			 </xsl:if>-->
    <xsl:choose>			     
  <xsl:when test="string-length($SafeImageUrl) != 0 and @Link!= ''">
  <span class="greenroom-col1">
                            
    			  <img src="{$SafeImageUrl}" title="{@ImageUrlAltText}" height="43">
                        <xsl:if test="$ImageWidth != ''">
                          <xsl:attribute name="width">
                            <xsl:value-of select="$ImageWidth" />
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="$ImageHeight != ''">
                          <xsl:attribute name="height">
                            <xsl:value-of select="$ImageHeight" />
                          </xsl:attribute>
                         </xsl:if>

                      </img>
        
    			
    				

    			
    			</span> 

                         
    			
    			  <span class="greenroom-col2">
    			  
    			  
    			  <span class="textonhomepage2">
    			    <a>
                                    

    			    			<xsl:attribute name="href">
    				<xsl:value-of select="substring-before(@Link,',')"/>
    			</xsl:attribute>
    			<xsl:attribute name="target">
    			<xsl:choose>
    				<xsl:when test="@NewWindow = 'Yes'">
    					<xsl:value-of select="string('_blank')"/>
    				</xsl:when>
    				<xsl:otherwise>
    					<xsl:value-of select="string('_self')"/>
    				</xsl:otherwise>
    			</xsl:choose>
    			</xsl:attribute>


    			  <xsl:value-of select="@Title" disable-output-escaping="yes"/>
    			 

    			  	</a><br/>
    			  	   <xsl:if test="@Description != ''">

    				  <xsl:value-of select="@Description" disable-output-escaping="yes"/>

    			 </xsl:if>
    			  </span>     
    			</span>
    			
    			
    			</xsl:when>
    			    <xsl:otherwise>
    			     <span class="greenroom-col2noimage">
    			  
    			  
    			  <span class="textonhomepage2">
    			    <a>
                                    

    			    			<xsl:attribute name="href">
    				<xsl:value-of select="substring-before(@Link,',')"/>
    			</xsl:attribute>
    			<xsl:attribute name="target">
    			<xsl:choose>
    				<xsl:when test="@NewWindow = 'Yes'">
    					<xsl:value-of select="string('_blank')"/>
    				</xsl:when>
    				<xsl:otherwise>
    					<xsl:value-of select="string('_self')"/>
    				</xsl:otherwise>
    			</xsl:choose>
    			</xsl:attribute>


    			  <xsl:value-of select="@Title" disable-output-escaping="yes"/>
    			 

    			  	</a>
    			  	<br/>
    			  	   <xsl:if test="@Description != ''">

    				  <xsl:value-of select="@Description" disable-output-escaping="yes"/>

    			 </xsl:if>
    			  </span>     
    			</span>
	<span class='greenroom-col3'>
    		
    		  <a>
                                    

    			    			<xsl:attribute name="href">
    				<xsl:value-of select="substring-before(@Link,',')"/>
    			</xsl:attribute>
    			<xsl:attribute name="target">
    			<xsl:choose>
    				<xsl:when test="@NewWindow = 'Yes'">
    					<xsl:value-of select="string('_blank')"/>
    				</xsl:when>
    				<xsl:otherwise>
    					<xsl:value-of select="string('_self')"/>
    				</xsl:otherwise>
    			</xsl:choose>
    			</xsl:attribute>
    			<img src="/PublishingImages/rightarrow.png"/>
    			</a>
    			  	
    		</span>
    			


    </xsl:otherwise>
    			</xsl:choose>
    			
    	<!--	<xsl:choose>
    			 <xsl:when test="string-length($SafeImageUrl) != 0 and @Intro != 'Yes'">  			

    		<span class='greenroom-col3'>
    		
    		  <a>
                                    

    			    			<xsl:attribute name="href">
    				<xsl:value-of select="substring-before(@Link,',')"/>
    			</xsl:attribute>
    			<xsl:attribute name="target">
    			<xsl:choose>
    				<xsl:when test="@NewWindow = 'Yes'">
    					<xsl:value-of select="string('_blank')"/>
    				</xsl:when>
    				<xsl:otherwise>
    					<xsl:value-of select="string('_self')"/>
    				</xsl:otherwise>
    			</xsl:choose>
    			</xsl:attribute>
    			<img src="/PublishingImages/rightarrow.png"/>
    			</a>
    			  	
    		</span>
    			
     		  </xsl:when>
     		  <xsl:otherwise>
     		  <span class="greenroom-buttonboxes">
					<span class="greenroom-buttontext"> 
				    			    <a>
                                    

    			    			<xsl:attribute name="href">
    				<xsl:value-of select="substring-before(@Link,',')"/>
    			</xsl:attribute>
    			<xsl:attribute name="target">
    			<xsl:choose>
    				<xsl:when test="@NewWindow = 'Yes'">
    					<xsl:value-of select="string('_blank')"/>
    				</xsl:when>
    				<xsl:otherwise>
    					<xsl:value-of select="string('_self')"/>
    				</xsl:otherwise>
    			</xsl:choose>
    			</xsl:attribute>
    		 <xsl:value-of select="substring-after(@Link,',')"/>

    			  	</a>
    			  	</span>
    			</span>  

     		  </xsl:otherwise>
     		  </xsl:choose>-->
                </div>
                   
          <!--  </xsl:if>-->
                <hr/>
        </div>
    </xsl:template>

        <xsl:template name="ServiceHomepage" match="Row[@Style='ServiceHomepage']" mode="itemstyle">

        <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="SafeImageUrl">
            <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
                <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="DisplayTitle">
            <xsl:call-template name="OuterTemplate.GetTitle">
                <xsl:with-param name="Title" select="@Title"/>
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
		<div style="display:none!important">
		
		<xsl:value-of select="@NewWindow "/>
		</div>
        <div class="greenroom-item">
                

            <xsl:if test="string-length($SafeImageUrl) != 0">
                <div class="twocolumn "> 
                            <span class="firsthalf">
             <!--  <a>
                                    

    			    			<xsl:attribute name="href">
    				<xsl:value-of select="substring-before(@Link,',')"/>
    			</xsl:attribute>
    			<xsl:attribute name="target">
    			
    			<xsl:choose>
    				<xsl:when test="@OpenInNewWindow = 'True'">
    					<xsl:value-of select="string('_blank')"/>
    				</xsl:when>
    				<xsl:otherwise>
    					<xsl:value-of select="string('_self')"/>
    				</xsl:otherwise>
    			</xsl:choose>
    			</xsl:attribute>
    			
    			  <img class="greenroom-selfimage" src="{$SafeImageUrl}" title="{@ImageUrlAltText}">
                        <xsl:if test="$ImageWidth != ''">
                          <xsl:attribute name="width">
                            <xsl:value-of select="$ImageWidth" />
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="$ImageHeight != ''">
                          <xsl:attribute name="height">
                            <xsl:value-of select="$ImageHeight" />
                          </xsl:attribute>
                        </xsl:if>
                      </img>
    				<span class="secondhalf"><span class="textonhomepage"><xsl:value-of select="@Title"/></span></span>


    			</a>-->
    		

    			
    			 <a>
                                    

    			<xsl:attribute name="href">
    			<xsl:value-of select="substring-before(@Link,',')"/>
    			</xsl:attribute>
    			<xsl:attribute name="target">
    			
    			<xsl:choose>
    				<xsl:when test="@NewWindow != 'False'">
    					<xsl:value-of select="string('_blank')"/>
    				</xsl:when>
    				<xsl:otherwise>
    					<xsl:value-of select="string('_self')"/>
    				</xsl:otherwise>
    			</xsl:choose>
    			</xsl:attribute>
    			
    			  <img class="greenroom-selfimage" src="{$SafeImageUrl}" title="{@ImageUrlAltText}">
                        <xsl:if test="$ImageWidth != ''">
                          <xsl:attribute name="width">
                            <xsl:value-of select="$ImageWidth" />
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="$ImageHeight != ''">
                          <xsl:attribute name="height">
                            <xsl:value-of select="$ImageHeight" />
                          </xsl:attribute>
                        </xsl:if>
                      </img>
    				<span class="secondhalf"><span class="textonhomepage"><xsl:value-of select="@Title"/></span></span>


    			</a>
    	

    	
            
            
         


                                  
    			</span>        
                </div>
            </xsl:if>
            <div class="link-item">
              <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
             
                
                    <xsl:value-of select="@Description" />
                
            </div>
        </div>
    </xsl:template>
    <xsl:template name="GreenRoomAlert" match="Row[@Style='GreenRoomAlert']" mode="itemstyle">
        <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="DisplayTitle">
            <xsl:call-template name="OuterTemplate.GetTitle">
                <xsl:with-param name="Title" select="@Title"/>
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>

      
        <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
       	<div id="greenroom-homepage-alert-icon"><img src="/SiteCollectionImages/pagelayouts/greenroom-subsite-title-icon-alert.png" /></div>   
       	<div id="greenroom-homepage-alert-title">    
        <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
          <xsl:if test="$ItemsHaveStreams = 'True'">
            <xsl:attribute name="onclick">
              <xsl:value-of select="@OnClickForWebRendering"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
            <xsl:attribute name="onclick">
              <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:value-of select="$DisplayTitle"/>
        </a>
      </div>
    </xsl:template>
    <xsl:template name="GreenRoomHolidaySchedule" match="Row[@Style='GreenRoomHolidaySchedule']" mode="itemstyle">
        <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="DisplayTitle">
            <xsl:call-template name="OuterTemplate.GetTitle">
                <xsl:with-param name="Title" select="@Title"/>
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        

            
        </xsl:variable>
	<div align="left">
   				<table style="width:100%;border-collapse:collapse" >
   						
						<tr>
						<td>
								<table width="100%">
									<tr>
									<td class="WebpartContent">
			
												<table style="width:83%" height="40px" >
												<tr>
													<xsl:attribute name="style">
														<xsl:if test="position() mod 2">background-color: #E6E7E9;</xsl:if>
													</xsl:attribute>
													<td style="width:242px;padding-left:18px;" class="WebpartContent" >
													<xsl:value-of select="substring-before(ddwrt:FormatDate(string(@EventDate), 1033,3),', 20')"/>
													
							
													</td>
													
													<td class="WebpartContent"> <xsl:value-of select="$DisplayTitle"/></td>
												
												</tr>
												
												</table>
											<hr class="hr" style="width:83%; text-align:left;"/>
									</td>
									</tr>
								</table>
						
					</td>
					</tr>
						
				 
					 </table>
		
                   
                </div>
        
    </xsl:template>   
    <xsl:template name="NextHoliday" match="Row[@Style='NextHoliday']" mode="itemstyle">
        <xsl:variable name="DisplayTitle">
            <xsl:call-template name="OuterTemplate.GetTitle">
                <xsl:with-param name="Title" select="@Title"/>
            </xsl:call-template>
        </xsl:variable>
        <div id="greenroom-next-holiday-box">
	        <div id="greenroom-next-holiday-icon"><img src="/SiteCollectionImages/greenroom-icon-next-holiday.png" /></div>
	        <div id="greenroom-next-holiday-occassion">
	              <xsl:value-of select="$DisplayTitle"/>
	            <div id="greenroom-next-holiday-date">
	            <xsl:value-of select="ddwrt:FormatDate(string(@Description), 1033,3)"/>
	            </div>
	        </div>
        </div>
    </xsl:template>  
             
      
      <xsl:template name="CRHomepage" match="Row[@Style='CRHomepage']" mode="itemstyle">
 
        <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="SafeImageUrl">
            <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
                <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
            </xsl:call-template>
        </xsl:variable>
     
       
        <div class="item">
            <xsl:if test="string-length($SafeImageUrl) != 0">
                <div class="image-area-left"> 
                    <a href="{$SafeLinkUrl}">
                      <xsl:if test="$ItemsHaveStreams = 'True'">
                        <xsl:attribute name="onclick">
                          <xsl:value-of select="@OnClickForWebRendering"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
                        <xsl:attribute name="onclick">
                          <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
                        </xsl:attribute>
                      </xsl:if>
                      <img class="image" src="{$SafeImageUrl}" title="{@ImageUrlAltText}">
                        <xsl:if test="$ImageWidth != ''">
                          <xsl:attribute name="width">
                            <xsl:value-of select="$ImageWidth" />
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="$ImageHeight != ''">
                          <xsl:attribute name="height">
                            <xsl:value-of select="$ImageHeight" />
                          </xsl:attribute>
                        </xsl:if>
                      </img>
                    </a>
                   

        

                </div>
            </xsl:if>
            
            <div class="link-item">
           
       
              <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
       <!--      <xsl:for-each select="nodes[position()&lt;=4]">   <xsl:if test="position() mod 1 = 0">-->

        <option value="{@Linktoform}">
         <xsl:value-of select="@Title" />
         </option>
            
      
         

        <!--     <a>
    			    			<xsl:attribute name="href">
    				<xsl:value-of select="substring-before(@Link,',')"/>
    			</xsl:attribute>
    			<xsl:attribute name="target">
    			<xsl:choose>
    				<xsl:when test="@NewWindow = 'Yes'">
    					<xsl:value-of select="string('_blank')"/>
    				</xsl:when>
    				<xsl:otherwise>
    					<xsl:value-of select="string('_self')"/>
    				</xsl:otherwise>
    			</xsl:choose>
    			</xsl:attribute>
    				<xsl:value-of select="@Title"/>

    	

    			</a>
    			

        

                <div class="description">
                    <xsl:value-of select="@Description" />
                </div>-->
                	
            </div>	
        </div>
        
    </xsl:template>
    
    
      <xsl:template name="GetInvolved" match="Row[@Style='GetInvolved']" mode="itemstyle">

        <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="SafeImageUrl">
            <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
                <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="DisplayTitle">
            <xsl:call-template name="OuterTemplate.GetTitle">
                <xsl:with-param name="Title" select="@Title"/>
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
     
     
      
        <div id="getinvolved-cqwp-item" class="greenroom-item">
       
      <!-- <xsl:if test="string-length($SafeImageUrl) != 0">-->


                <div class="twocolumn "> 
        
                
                  <xsl:if test="string-length($SafeImageUrl) != 0">

                        <span class="greenroom-col1">
                             
    			    <a>
    			    			<xsl:attribute name="href">
    				<xsl:value-of select="substring-before(@Link,',')"/>
    			</xsl:attribute>
    			<xsl:attribute name="target">
    			<xsl:choose>
    				<xsl:when test="@NewWindow = 'Yes'">
    					<xsl:value-of select="string('_blank')"/>
    				</xsl:when>
    				<xsl:otherwise>
    					<xsl:value-of select="string('_self')"/>
    				</xsl:otherwise>
    			</xsl:choose>
    			</xsl:attribute>
    			  <img src="{$SafeImageUrl}" title="{@ImageUrlAltText}" height="43">
                        <xsl:if test="$ImageWidth != ''">
                          <xsl:attribute name="width">
                            <xsl:value-of select="$ImageWidth" />
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="$ImageHeight != ''">
                          <xsl:attribute name="height">
                            <xsl:value-of select="$ImageHeight" />
                          </xsl:attribute>
                        </xsl:if>
                      </img>

    			  	</a>
               
    			
    				

    			
    			</span> 
    			    </xsl:if>
    <xsl:choose>			     
	   <xsl:when test="string-length($SafeImageUrl) != 0 and @Intro != 'No' and @Link!= ''">
    			
    		<span class="greenroom-col2">
    			  
    			  
    			  <span class="textonhomepage">
    			    <a>
    			    			<xsl:attribute name="href">
    				<xsl:value-of select="substring-before(@Link,',')"/>
    			</xsl:attribute>
    			<xsl:attribute name="target">
    			<xsl:choose>
    				<xsl:when test="@NewWindow = 'Yes'">
    					<xsl:value-of select="string('_blank')"/>
    				</xsl:when>
    				<xsl:otherwise>
    					<xsl:value-of select="string('_self')"/>
    				</xsl:otherwise>
    			</xsl:choose>
    			</xsl:attribute>
    			  <xsl:value-of select="@Title"/>
    			  	</a>
    			  </span>     
    			</span>
    			</xsl:when>
    			    <xsl:otherwise>
    			  
		

    </xsl:otherwise>
    			</xsl:choose>
    			
    		<xsl:choose>
    			   <xsl:when test="string-length($SafeImageUrl) != 0 and @Intro != 'No' and @Link!= ''">
		
    				<span class='greenroom-col3'>
    	
    		  				<a>
                                    

    			    			<xsl:attribute name="href">
    								<xsl:value-of select="substring-before(@Link,',')"/>
    							</xsl:attribute>
    							<xsl:attribute name="target">
    								<xsl:choose>
    									<xsl:when test="@NewWindow = 'Yes'">
    										<xsl:value-of select="string('_blank')"/>
    									</xsl:when>
		    							<xsl:otherwise>
		    								<xsl:value-of select="string('_self')"/>
		    							</xsl:otherwise>
    								</xsl:choose>
    							</xsl:attribute>
    						<img src="/PublishingImages/rightarrow.png"/>
			    			</a>
			    			  	
			    		</span>
    			
     		  	</xsl:when>
					     		  <xsl:otherwise>
					     		  <span class='greenroom-col2noimage'>
					     		  	  <span class="textonhomepage2">
					     		  	  <a>
                                    

    			    			<xsl:attribute name="href">
    								<xsl:value-of select="substring-before(@Link,',')"/>
    							</xsl:attribute>
    							<xsl:attribute name="target">
    								<xsl:choose>
    									<xsl:when test="@NewWindow = 'Yes'">
    										<xsl:value-of select="string('_blank')"/>
    									</xsl:when>
		    							<xsl:otherwise>
		    								<xsl:value-of select="string('_self')"/>
		    							</xsl:otherwise>
    								</xsl:choose>
    							</xsl:attribute>
    				<xsl:value-of select="@Title" disable-output-escaping="yes"/>			    			</a>

						  				
					    			 </span>
					    			 
					    			 	<span class='greenroom-col3noimg'>
    		
    		  				<a>
                                    

    			    			<xsl:attribute name="href">
    								<xsl:value-of select="substring-before(@Link,',')"/>
    							</xsl:attribute>
    							<xsl:attribute name="target">
    								<xsl:choose>
    									<xsl:when test="@NewWindow = 'Yes'">
    										<xsl:value-of select="string('_blank')"/>
    									</xsl:when>
		    							<xsl:otherwise>
		    								<xsl:value-of select="string('_self')"/>
		    							</xsl:otherwise>
    								</xsl:choose>
    							</xsl:attribute>
    						<img src="/PublishingImages/rightarrow.png"/>
			    			</a>
			    			  	
			    		</span>
										
								</span>
								
								

					
					     		  </xsl:otherwise>
     		  </xsl:choose>
                </div>
       
                <hr/>
        </div>
    </xsl:template>
      <xsl:template name="Campaign" match="Row[@Style='Campaign']" mode="itemstyle">
        <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="SafeImageUrl">
            <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
                <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="DisplayTitle">
            <xsl:call-template name="OuterTemplate.GetTitle">
                <xsl:with-param name="Title" select="@Title"/>
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <div>
            <xsl:if test="string-length($SafeImageUrl) != 0">
                <div id="greenroom-campaign-image"> 
                      <img class="image" src="{$SafeImageUrl}" title="{@ImageUrlAltText}">
                        <xsl:if test="$ImageWidth != ''">
                          <xsl:attribute name="width">
                            <xsl:value-of select="$ImageWidth" />
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="$ImageHeight != ''">
                          <xsl:attribute name="height">
                            <xsl:value-of select="$ImageHeight" />
                          </xsl:attribute>
                        </xsl:if>
                      </img>
                </div>
            </xsl:if>
            
            <div id="greenroom-campaign-title-box">
   				<div id="greenroom-campaign-title"><xsl:value-of select="@Title"/></div>
                <div id="greenroom-campaign-description">
                    <xsl:value-of select="@Description1" />
                </div>
                   
		            <div id="greenroom-campaign-ctalinks">
		            
		            	<xsl:if test="@CTAlink1!= ''">
		            	<div id="greenroom-campaign-ctalink1">
							<a><xsl:attribute name="href">
			    				<xsl:value-of select="substring-before(@CTAlink1,',')"/>
			    			</xsl:attribute>
			    			<xsl:attribute name="target">
			    			<xsl:choose>
			    				<xsl:when test="@NewWindow = 'Yes'">
			    					<xsl:value-of select="string('_blank')"/>
			    				</xsl:when>
			    				<xsl:otherwise>
			    					<xsl:value-of select="string('_self')"/>
			    				</xsl:otherwise>
			    			</xsl:choose>
			    			</xsl:attribute>
								<span class="greenroom-campaign-ctalink1"><xsl:value-of select="substring-after(@CTAlink1,',')"/></span>
			    				</a>
		            	</div>
		            	</xsl:if>
		            	<xsl:if test="@CTAlink2!= ''">		            	
		            	<div id="greenroom-campaign-ctalink2">
							<a><xsl:attribute name="href">
			    				<xsl:value-of select="substring-before(@CTAlink2,',')"/>
			    			</xsl:attribute>
			    			<xsl:attribute name="target">
			    			<xsl:choose>
			    				<xsl:when test="@NewWindow = 'Yes'">
			    					<xsl:value-of select="string('_blank')"/>
			    				</xsl:when>
			    				<xsl:otherwise>
			    					<xsl:value-of select="string('_self')"/>
			    				</xsl:otherwise>
			    			</xsl:choose>
			    			</xsl:attribute>
								<span class="greenroom-campaign-ctalink2"><xsl:value-of select="substring-after(@CTAlink2,',')"/></span>
								</a>
		            	</div>
		            	</xsl:if>		            	
		            </div>

                
            </div>
            
        </div>

    </xsl:template> 
           <xsl:template name="IntroSection" match="Row[@Style='IntroSection']" mode="itemstyle">

        <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="SafeImageUrl">
            <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
                <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="DisplayTitle">
            <xsl:call-template name="OuterTemplate.GetTitle">
                <xsl:with-param name="Title" select="@Title"/>
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
     <div style="display:none!important">
		
		<xsl:value-of select="@NewWindow "/>
		</div>

        <div class="greenroom-item">
            <xsl:if test="string-length($SafeImageUrl) != 0">
                <div class="twocolumn"> 
                            <span class="greenroom-featured">
                            
				<xsl:if test="$SafeImageUrl != ''">	

                             
    			  <img src="{$SafeImageUrl}" title="{@ImageUrlAltText}">
                        <xsl:if test="$ImageWidth != ''">
                          <xsl:attribute name="width">
                            <xsl:value-of select="$ImageWidth" />
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="$ImageHeight != ''">
                          <xsl:attribute name="height">
                            <xsl:value-of select="$ImageHeight" />
                          </xsl:attribute>
                        </xsl:if>
                      </img>
               
    			
    				
</xsl:if>

    			
    			</span> 
    			
    			  <span class="greenroom-featuredtext">
    			  
    			  
    			
    			    <xsl:value-of select="@Title"/>
				</span>
				<br/>
				
				<xsl:if test="@Link!= ''">	
				
				<span>
					<!--<span class="greenroom-buttontext"> -->
					<span> 

				    			    <a class="btn btn-primary btn-lg">
                                    

    			    			<xsl:attribute name="href">
    				<xsl:value-of select="substring-before(@Link,',')"/>
    			</xsl:attribute>
    			<xsl:attribute name="target">
    			<xsl:choose>
    				<xsl:when test="@NewWindow != 'False'">
    					<xsl:value-of select="string('_blank')"/>
    				</xsl:when>
    				<xsl:otherwise>
    					<xsl:value-of select="string('_self')"/>
    				</xsl:otherwise>
    			</xsl:choose>
    			</xsl:attribute>
    		 <xsl:value-of select="substring-after(@Link,',')"/>
				 <span class="glyphicon glyphicon-play"></span>
    			  	</a>
    			  	</span>
    			</span>  
    			
	  
    			    </xsl:if>

    	
    		
       
                </div>
            </xsl:if>
                <hr/>
        </div>
    </xsl:template>
    <xsl:template name="Discounts" match="Row[@Style='Discounts']" mode="itemstyle">
        <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="SafeImageUrl">
            <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
                <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="DisplayTitle">
            <xsl:call-template name="OuterTemplate.GetTitle">
                <xsl:with-param name="Title" select="@Title"/>
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
       		
<div id="greenroom-discounts-container">
	<div id="greenroom-discounts-title"><xsl:value-of select="$DisplayTitle"/></div>
	<div id="greenroom-discounts-image"><xsl:if test="$SafeImageUrl!= ''"><img src="{$SafeImageUrl}" title="{@ImageUrlAltText}" /></xsl:if></div>
	<div id="greenroom-discounts-description-container">
			<div id="greenroom-discounts-desciption-text"><xsl:value-of select="@Description" /></div>
			<xsl:if test="@DiscountWebsite!= ''"><div id="greenroom-discounts-website"><strong>Website: </strong>
	            <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
	              <xsl:if test="$ItemsHaveStreams = 'True'">
	                <xsl:attribute name="onclick">
	                  <xsl:value-of select="@OnClickForWebRendering"/>
	                </xsl:attribute>
	              </xsl:if>
	              <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
	                <xsl:attribute name="onclick">
	                  <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
	                </xsl:attribute>
	              </xsl:if>
					<xsl:value-of select="@DiscountWebsite" />
				</a>	
			</div>
			</xsl:if>	
			<xsl:if test="@DiscountContactInfo!= ''"><div id="greenroom-discounts-contact"><strong>Contact Info: </strong><xsl:value-of select="@DiscountContactInfo" /></div></xsl:if>			
			<xsl:if test="@DiscountCode!= ''"><div id="greenroom-discounts-code"><strong>Code: </strong><xsl:value-of select="@DiscountCode" /></div></xsl:if>				
		</div>
		<xsl:if test="@DiscountWebsite!= ''"><div id="greenroom-discounts-website-go"><xsl:value-of select="@DiscountWebsite" /></div>	</xsl:if>	
	</div>
    </xsl:template>

  <xsl:template name="DiscussionBoard" match="Row[@Style='DiscussionBoard']" mode="itemstyle">
        <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="SafeImageUrl">
            <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
                <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="DisplayTitle">
            <xsl:call-template name="OuterTemplate.GetTitle">
                <xsl:with-param name="Title" select="@Title"/>
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <div class="item">
            <xsl:if test="string-length($SafeImageUrl) != 0">
                <div class="image-area-left"> 
                    <a href="{$SafeLinkUrl}">
                      <xsl:if test="$ItemsHaveStreams = 'True'">
                        <xsl:attribute name="onclick">
                          <xsl:value-of select="@OnClickForWebRendering"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
                        <xsl:attribute name="onclick">
                          <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
                        </xsl:attribute>
                      </xsl:if>
                      <img class="image" src="{$SafeImageUrl}" title="{@ImageUrlAltText}">
                        <xsl:if test="$ImageWidth != ''">
                          <xsl:attribute name="width">
                            <xsl:value-of select="$ImageWidth" />
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="$ImageHeight != ''">
                          <xsl:attribute name="height">
                            <xsl:value-of select="$ImageHeight" />
                          </xsl:attribute>
                        </xsl:if>
                      </img>
                    </a>
                   

        

                </div>
            </xsl:if>
            <div class="link-item">
              <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
              <!--  <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
                  <xsl:if test="$ItemsHaveStreams = 'True'">
                    <xsl:attribute name="onclick">
                      <xsl:value-of select="@OnClickForWebRendering"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
                    <xsl:attribute name="onclick">
                      <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="$DisplayTitle"/>
                </a>-->    <a>
    			    			<xsl:attribute name="href">
    				<xsl:value-of select="substring-before(@Link,',')"/>
    			</xsl:attribute>
    			<xsl:attribute name="target">
    			<xsl:choose>
    				<xsl:when test="@NewWindow = 'Yes'">
    					<xsl:value-of select="string('_blank')"/>
    				</xsl:when>
    				<xsl:otherwise>
    					<xsl:value-of select="string('_self')"/>
    				</xsl:otherwise>
    			</xsl:choose>
    			</xsl:attribute>
    				<xsl:value-of select="@Title"/>

    	

    			</a>

        

                <div class="description">
                  
                        <xsl:value-of select="substring(@Body, 1, 200 + string-length(substring-before(substring(@Body, 250),' ')))" disable-output-escaping="yes" />
                </div>
            </div>
        </div>
    </xsl:template>
    <xsl:template name="DailyClips" match="Row[@Style='DailyClips']" mode="itemstyle">
        <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="DisplayTitle">
            <xsl:call-template name="OuterTemplate.GetTitle">
                <xsl:with-param name="Title" select="@Title"/>
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
      <div class="item link-item">
        <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
          <xsl:value-of select="$DisplayTitle" disable-output-escaping="yes"/>
      </div>
    </xsl:template>
<xsl:template name="MostShared" match="Row[@Style='MostShared']" mode="itemstyle">
        <xsl:variable name="SafeLinkUrl" select="@Url">
        </xsl:variable>
        <xsl:variable name="DisplayTitle" select="@Title">
        </xsl:variable>
		<xsl:variable name="SafeImageLinkUrl" select="@ThumbnailImage">
        </xsl:variable>
		<xsl:variable name="Shares" select="@Shares">
        </xsl:variable>
		
       	<div id="MostShared">
			<div class="page-image">
				<a class="mostshared-image-url" href="{$SafeLinkUrl}">
					<xsl:if test="$SafeImageLinkUrl != ''">
					   <img class="page-image-url" style="width: 185px;height: 110px;" src="{$SafeImageLinkUrl}?RenditionID=6" />
					</xsl:if>
					<xsl:if test="$SafeImageLinkUrl = ''">
					   <img class="page-image-url" style="width: 185px;height: 110px;" src="/SiteCollectionImages/pagelayouts/default-article-thumbnail.png?RenditionID=6" />
					</xsl:if>
				</a>
			</div>
			<div style="margin-top:15px">
				<span id="cswp-footer-number" class="number-of-shares"><xsl:value-of select="$Shares"/></span>
				<span id="cswp-footer-number"> Shares</span>
			</div>
			<div>
				<span class="page-title">
					<a class="page-title-url cbs-picture3LinesLine1Link" href="{$SafeLinkUrl}"><xsl:value-of select="$DisplayTitle"/></a>
				</span>
			</div>
		</div>
    </xsl:template>
	 <xsl:template name="AvailableChannels" match="Row[@Style='AvailableChannels']" mode="itemstyle">
        <xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="DisplayTitle">
            <xsl:call-template name="OuterTemplate.GetTitle">
                <xsl:with-param name="Title" select="@Title"/>
                <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
            </xsl:call-template>
        </xsl:variable>
        <div class="item link-item">
            <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
        
              <xsl:value-of select="$DisplayTitle"/>
 
             <span class="ChannelNo">
                <xsl:value-of select="@ChannelNo" /> 
            </span>

        </div>
    </xsl:template>
	
	<xsl:template name="UpcomingEvents" match="Row[@Style='UpcomingEvents']" mode="itemstyle">
		<xsl:variable name="SafeLinkUrl">
            <xsl:call-template name="OuterTemplate.GetSafeLink">
            <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
        </xsl:call-template>
        </xsl:variable>
		<xsl:variable name="DisplayTitle" select="@Title">
        </xsl:variable>
		<xsl:variable name="StartDate" select="@StartDate">
        </xsl:variable>
		<xsl:variable name="EndDate" select="@EndDate0">
        </xsl:variable>
		<xsl:variable name="Region" select="@Region">
        </xsl:variable>
		<xsl:if test="count(preceding-sibling::*)=0">
			<div id="divHeader">
				<table class="testS">
					<tr>
						<td style="width:100px;text-align:center"><span style="padding:5px">Title</span></td>
						<td style="width:100px;text-align:center"><span style="padding:5px">Start Date</span></td>
						<td style="width:100px;text-align:center"><span style="padding:5px">End Date</span></td>
						<td style="width:100px;text-align:center"><span style="padding:5px">Region</span></td>
					</tr>
				</table>
			</div>	
		</xsl:if>
		<div id="divUpcomingEvents">

			<div class="events">
				<table>
					<tr>
						<td style="width:100px;text-align:center">
							<span style="padding:5px">
								<a href="{$SafeLinkUrl}"><xsl:value-of select="$DisplayTitle"/></a>
							</span>
						</td>
						<td style="width:100px;text-align:center">
							<span style="padding:5px">
								<xsl:value-of select="ddwrt:FormatDate(@StartDate,1033,1)"/>
							</span>
						</td>
						<td style="width:100px;text-align:center"> 
							<span style="padding:5px">
								<xsl:value-of select="ddwrt:FormatDate(@EndDate0,1033,1)"/>
							</span >
						</td>
						<td style="width:100px;text-align:center">
							<span style="padding:5px">
								<xsl:value-of select="$Region"/>
							</span>
						</td>
					</tr>
				</table>
			</div>
		</div>

    </xsl:template>

  <xsl:template name="EventCalendar" match="Row[@Style='EventCalendar']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle" select="@Title">
    </xsl:variable>
    <xsl:variable name="StartDate" select="@StartDate">
    </xsl:variable>
    <xsl:variable name="EndDate" select="@EndDate">
    </xsl:variable>
    <div id="greenroom-next-holiday-box">
      <div id="greenroom-next-holiday-occassion">
        <h3>
        <a href="{$SafeLinkUrl}">
          <xsl:value-of select="$DisplayTitle"/>
        </a>
        </h3>
        <div id="greenroom-next-holiday-date">
          <xsl:value-of select="ddwrt:FormatDate(@StartDate,1033,1)"/>
          <xsl:text>-</xsl:text>
          <xsl:value-of select="ddwrt:FormatDate(@EndDate,1033,1)"/>
        </div>
      </div>
    </div>
  </xsl:template>
  
</xsl:stylesheet>