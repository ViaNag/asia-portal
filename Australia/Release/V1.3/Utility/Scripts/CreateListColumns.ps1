﻿# =================================================================================
#
#Create List Column.#
# =================================================================================
function CreateListColumns([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $($ConfigPath) file"
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
            foreach($libColumn in $cfg.LibraryColumns.LibraryColumn)
            {
                CreateListColumn $libColumn
            }
        }
        catch
        {
            Write-Host "`nException in CreateListColumns :" $Error -ForegroundColor Red
            Write-Output "`nException in CreateListColumns:" $Error
        }
	}
	else
	{
		Write-Host "Error in CreateListColumns:" $Error -ForegroundColor Red
        Write-Output "Error in CreateListColumns:" $Error
	}
}

# =================================================================================
#
# FUNC: CreateListColumn
# DESC: Create, update or delete list Column
#
# =================================================================================
function CreateListColumn([object] $listColumn)
{
    try
    {
        $error.Clear()
        $web =Get-SPWeb $listColumn.SiteUrl
        $field=$null
        $list=$null
        $fieldType=$listColumn.Type
        $fieldExists=$false
        $actionType=$listColumn.ActionType
		
        if($fieldType -eq "UserMulti")
        {
            $fieldType="User"
        }
        if(![string]::IsNullOrEmpty($listColumn.LibraryName))
        {
            $list=$web.Lists.TryGetList($listColumn.LibraryName)
            if($list)
            {
                if($actionType -eq "Add")
                { 
					try
					{				
						$field=$list.Fields.GetFieldByInternalName($listColumn.InternalName)
						if($field)
						{
							$fieldExists=$true
							Write-Host "`nField $($listColumn.InternalName) already exists in Library $($listColumn.LibraryName) for web url: $($web.Url) for action type Add" -ForegroundColor Yellow
							Write-Output "Field $($listColumn.InternalName) already exists in Library $($listColumn.LibraryName) for web url: $($web.Url) for action type Add"
							
						}
					}
					catch
					{
						$error.Clear()
                        $list.Fields.Add($listColumn.InternalName, $fieldType, 0) | Out-Null
                        $list.update()
						$fieldExists=$true
                        Write-Host "`nField $($listColumn.InternalName) added successfully in Library $($listColumn.LibraryName) for web url: $($web.Url)" -ForegroundColor Green
                        Write-Output "Field $($listColumn.InternalName) added successfully in Library $($listColumn.LibraryName) for web url: $($web.Url)"
					}
                                                         
                }
				elseif($actionType -eq "Add from existing")
                {					
					try
					{				
						$sitefield=$web.AvailableFields.GetFieldByInternalName($listColumn.InternalName)
                          
						if(!$list.Fields.Contains($sitefield.Id))
						{
							$list.Fields.Add($sitefield)
							$list.update()
							$fieldExists=$true
							Write-Host "Field $($listColumn.InternalName) added from existing to list $($listColumn.LibraryName) at web : $($listColumn.SiteURL)" -ForegroundColor Green
							Write-Output "Field $($listColumn.InternalName) added from existing to list $($listColumn.LibraryName) at web : $($listColumn.SiteURL)"
						}
						else
						{
							Write-Host "Field $($listColumn.InternalName) already exist in list $($listColumn.LibraryName) at web : $($listColumn.SiteURL)"  -ForegroundColor Yellow
							Write-Output "Field $($listColumn.InternalName) already exist in list $($listColumn.LibraryName) at web :$($listColumn.SiteURL)" 
						}
					}
					catch
					{
						$error.Clear()                        
                        Write-Host "`nField $($listColumn.InternalName) not available for web url: $($web.Url)" -ForegroundColor Yellow
                        Write-Output "Field $($listColumn.InternalName) not available for web url: $($web.Url)"
					}
				}
				elseif($actionType -eq "Delete")
				{
					$field=$list.Fields.GetFieldByInternalName($listColumn.InternalName)
					
					#Reset column properties to allow delete					
					$field.ReadOnlyField = $false
					$field.AllowDeletion = $true
					$field.Update()
			 
					#Delete the column from list
					$list.Fields.Delete($field)
					$fieldExists = $false
					
				}
				elseif($actionType -eq "Update")
				{
					try
					{				
						$field=$list.Fields.GetFieldByInternalName($listColumn.InternalName)
						if($field)
						{
							$fieldExists=$true							
						}
					}
					catch
					{
						$fieldExists=$false
                        Write-Host "`nField $($listColumn.InternalName) with action type Update does not exists in  Library $($listColumn.LibraryName) for web url: $($web.Url)" -ForegroundColor Green
                        Write-Output "Field $($listColumn.InternalName) with action type Update does not exists in Library $($listColumn.LibraryName) for web url: $($web.Url)"
					}
				}
				
				if($fieldExists)
				{
					                
					$field=$list.Fields.GetFieldByInternalName($listColumn.InternalName)
					
					if(($fieldType -eq "TaxonomyFieldType") -or ($fieldType -eq "TaxonomyFieldTypeMulti"))
					{
						# Get the Taxonomy session of your site collection
						$site = new-object Microsoft.SharePoint.SPSite $listColumn.SiteUrl
						$session = new-object Microsoft.SharePoint.Taxonomy.TaxonomySession($site)
						$termStore = $session.TermStores[$listColumn.TermStore]
						# Get the term store group which stores the term sets you want to retrieve.
						$taxGroup=$listColumn.TaxonomyGroup
						#if($listColumn.TaxonomyGroup -eq "M&E Ad Sales")
						#{
						#    $taxGroup="M＆E Ad Sales"
						#}
						if($listColumn.TaxonomyGroup.contains("&"))
						{
							$taxGroup = $listColumn.TaxonomyGroup -replace "&","＆"
						}
						$termStoreGroup = $termStore.Groups[$taxGroup]
						
						# Get the term set you want to associate with this field. 
						if($listColumn.TermSet.contains("&"))
						{
							$listColumn.TermSet = $listColumn.TermSet -replace "&","＆"
						}
						$termSet = $termStoreGroup.TermSets[$listColumn.TermSet]
						# In most cases, the anchor on the managed metadata field is set to a lower level under the root term of the term set. In such cases, specify the term in the spreadsheet and do the following
						$termID=""
						if(($listColumn.TermSet -ne $listColumn.Term) -and ($listColumn.Term -ne "") -and ($listColumn.Term -ne $null))
						{
							#Get all terms under term set
							$terms = $termSet.GetAllTerms()
							$termToMap =$listColumn.Term
							if($termToMap.Contains(";"))
							{
								$multiLevelTaxonomyTerm=$termToMap.Split(";")
								$m=0;
								foreach($termLabel in $multiLevelTaxonomyTerm)
								{
								   $term = $terms | Where-Object {$_.Name -eq $termLabel} | Select-Object -First 1  
								   $terms = $term.Terms    
								}
							}
							else
							{
								 #Get the term to map the column to
								 $term = $terms | Where-Object {$_.Name -eq $listColumn.Term} | Select-Object -First 1
							}                        
						   

							#Get the GUID of the term to map the metadata column anchor to
							$termID = $term.Id
						}
						else # In cases when you want to set the anchor at the root of the term set, leave the  value as blank. Empty guids will error out when you run the script but will accomplish what you need to do i.e. set the anchor at the root of the termset
						{                                
							$termID = [System.GUID]::empty
						} 
					   
						$field.SspId = $termSet.TermStore.ID
						$field.TermSetId = $termSet.Id 
						$field.AnchorId = $termID
						if($type -eq "TaxonomyFieldTypeMulti")
						{
							$field.AllowMultipleValues = $true
						} 
					}
					elseif($fieldType -eq "Choice" -or $fieldType -eq "MultiChoice")
					{
						# Build a string array with the choice values separating the values at ","
						$choiceFieldChoices = @($listColumn.Choices.choice)

						# Declare a new empty String collection
						$stringColl = new-Object System.Collections.Specialized.StringCollection

						# Add the choice fields from array to the string collection
						$stringColl.AddRange($choiceFieldChoices)
						
						$field.choices.clear()
						$field.update()
						$list.update()
						$field=$list.Fields.GetFieldByInternalName($listColumn.InternalName)
						$field.choices.addrange($choiceFieldChoices) 
						if($listColumn.Type -eq "Choice")
						{
							if(![string]::IsNullOrEmpty($listColumn.Format))
							{
								if($listColumn.Format -eq "RadioButtons")
								{
									$field.EditFormat=[Microsoft.SharePoint.SPChoiceFormatType]::RadioButtons
								}
							}          
						}                   
						
					}
					elseif($fieldType -like 'User*')
					{
						$format = $().Format
						if(![string]::IsNullOrEmpty($format))
						{
							$field.SelectionMode=[Microsoft.SharePoint.SPFieldUserSelectionMode]::$format
						}
				
						if($fieldType -eq "UserMulti")
						{
							$field.AllowMultipleValues = $true
						}

						if(![string]::IsNullOrEmpty($listColumn.UserGroupName))
						{
					
							$spGroup=$web.SiteGroups[$listColumn.UserGroupName]
							if($spGroup)
							{
								$field.SelectionGroup=$spGroup.ID
							}
							else
							{
								Write-Host "Group $($listColumn.UserGroupName) does not exists at web:- $($listColumn.SiteUrl) for updating column $($listColumn.InternalName)" -ForegroundColor Yellow
								Write-Output "Group $($listColumn.UserGroupName) does not exists at web:- $($listColumn.SiteUrl) for updating column $($listColumn.InternalName)" 
							}
						}
						else
						{
							$field.SelectionGroup = 0; 
						}

					}
					elseif($fieldType -eq "Number")
					{
						if(![string]::IsNullOrEmpty($listColumn.Max))
						{
							$field.MaximumValue = $listColumn.Max
						}
						if(![string]::IsNullOrEmpty($listColumn.Min ))
						{
							$field.MinimumValue = $listColumn.Min 
						} 
						if(![string]::IsNullOrEmpty($listColumn.Format))
						{
							$field.DisplayFormat=$listColumn.Format;
						}
					}
					elseif($fieldType -eq "Note")
					{
						if($listColumn.Format -eq "PlainText")
						{
							[boolean]$field.RichText = $false
						}
						else
						{
							[boolean]$field.RichText = $true
						}
					}
					elseif($fieldType -eq "Calculated")
					{		         
						$field.Formula = $listColumn.Formula
					}
									
					
					if(![string]::IsNullOrEmpty($listColumn.ColumnName))
					{
						 $field.Title = $listColumn.ColumnName
					}

					if(![string]::IsNullOrEmpty($listColumn.Description))
					{
						$field.Description = $listColumn.Description
					}     
			
					if(![string]::IsNullOrEmpty($listColumn.JSLink))
					{
						$field.JSLink=$listColumn.JSLink
					} 				
					if(![string]::IsNullOrEmpty($listColumn.ColumnValidation))
					{
						$columnValidation=$listColumn.ColumnValidation
						if($columnValidation -eq "Empty")
						{
							$columnValidation = "";    
						} 
						 $field.ValidationFormula= $columnValidation      
					}   
			
					# Boolean values must be converted before they are assigned in PowerShell.
					if(![string]::IsNullOrEmpty($listColumn.ShowInNewForm))
					{
						[boolean]$field.ShowInNewForm = [System.Convert]::ToBoolean($listColumn.ShowInNewForm)
					}
					if(![string]::IsNullOrEmpty($listColumn.ShowInDisplayForm))
					{
						[boolean]$field.ShowInDisplayForm = [System.Convert]::ToBoolean($listColumn.ShowInDisplayForm)
					}
					if(![string]::IsNullOrEmpty($listColumn.ShowInEditForm))
					{
						[boolean]$field.ShowInEditForm = [System.Convert]::ToBoolean($listColumn.ShowInEditForm)
					}
					if(![string]::IsNullOrEmpty($listColumn.Hidden))
					{
						[boolean]$field.Hidden = [System.Convert]::ToBoolean($listColumn.Hidden)
					}
					if(![string]::IsNullOrEmpty($listColumn.Required))
					{
						[boolean]$field.Required = [System.Convert]::ToBoolean($listColumn.Required)
					}
					if(![string]::IsNullOrEmpty($listColumn.ReadOnly))
					{
						[boolean]$field.ReadOnlyField = [System.Convert]::ToBoolean($listColumn.ReadOnly)
					} 
					if(![string]::IsNullOrEmpty($listColumn.DefaultValues))
					{
						SetDefaultValue $field $listColumn $site
					} 
					$field.update()
					$list.update()
					[boolean]$addToAllContentType = [System.Convert]::ToBoolean(@{$true="false";$false=$listColumn.AddToAllContentType}[$listColumn.AddToAllContentType -eq ""])
					if($addToAllContentType)
					{
						foreach($lstCT in $list.ContentTypes)
						{
							if($lstCT.Name -ne  'Folder')
							{
								$fieldLink=New-Object Microsoft.SharePoint.SPFieldLink $field
								$lstCT.FieldLinks.Add($fieldLink)			
								$lstCT.Update();
							}
						}
						$list.update()
					}
				   Write-Host "`nField $($listColumn.InternalName) updated successfully in Library $($listColumn.LibraryName) for web url: $($web.Url)" -ForegroundColor Green
				   Write-Output "Field $($listColumn.InternalName) updated successfully in Library $($listColumn.LibraryName) for web url: $($web.Url)"   
				}				
			}
			else
			{
				Write-Host "`nLibrary $($listColumn.LibraryName) does not exist in web $($web.Url)" -ForegroundColor Yellow
				Write-Output "Library $($listColumn.LibraryName) does not exist in web $($web.Url)"               
			}
		}
        else
        {
           Write-Host "`n Error: LibraryName required field" -ForegroundColor Red
           Write-Output "Error: LibraryName required field" 
        }
    }
    catch
    {
        Write-Host "`nException in CreateListColumns for:" $listColumn.ColumnName "`n" $Error -ForegroundColor Red
        Write-Output "`nException in CreateListColumns for :" $listColumn.ColumnName "`n" $Error 
    }
}


# =================================================================================
#
# FUNC: SetDeafultValue
# DESC: Set default value for column at site collection level
#
# =================================================================================
function SetDefaultValue([object] $listColumn,$PushChanges)
{
    try
    {
        $error.Clear()
        $type=$column.Type
        if(($type -eq "TaxonomyFieldType") -or ($type -eq "TaxonomyFieldTypeMulti"))
            {
                # Get the Taxonomy session of your site collection
                $site = new-object Microsoft.SharePoint.SPSite $listColumn.SiteUrl
                $session = new-object Microsoft.SharePoint.Taxonomy.TaxonomySession($site)
                $termStore = $session.TermStores[$column.TermStore]
                $taxGroup=$column.TaxonomyGroup
				if($taxGroup.contains("&"))
				{
					$taxGroup = $taxGroup -replace "&","＆"
				}
                #if($column.TaxonomyGroup -eq "M&E Ad Sales")
                #{
                #    $taxGroup="M＆E Ad Sales"
                #}
                $termStoreGroup = $termStore.Groups[$taxGroup]
				if($column.TermSet.contains("&"))
				{
					$listColumn.TermSet = $listColumn.TermSet -replace "&","＆"
				}
                $termSet = $termStoreGroup.TermSets[$column.TermSet]
				$web=$site.RootWeb
                $field=$web.Fields.GetFieldByInternalName($column.InternalName)
                $setDefVal=""
                if($type -eq "TaxonomyFieldTypeMulti")
                {
                    $defaultValues = @($column.DefaultValues.Split(","))
                    foreach($value in $defaultValues)
                    {
                        if(($value -ne $null) -and ($value -ne ""))
                        {
                            $term=$termSet.Terms[$value]
							$wssIDToSet = AddTaxonomyHiddenListItem $web $term 
                            $setDefVal=[string]$setDefVal+$wssIDToSet + ";#" + $term.GetPath() + [Microsoft.SharePoint.Taxonomy.TaxonomyField]::TaxonomyGuidLabelDelimiter + $term.Id.ToString()+";#"
                        }
                    }
                    $setDefVal=$setDefVal.TrimEnd(";#")
                }
                elseif($type -eq "TaxonomyFieldType")
                {
                    $defaultValues = $listColumn.DefaultValues
                    if(($defaultValues -ne $null) -and ($defaultValues -ne ""))
                    {
                        $term=$termSet.Terms[$defaultValues]
						$wssIDToSet = AddTaxonomyHiddenListItem $web $term 
                        $setDefVal=[string]$wssIDToSet + ";#" + $term.GetPath() + [Microsoft.SharePoint.Taxonomy.TaxonomyField]::TaxonomyGuidLabelDelimiter + $term.Id.ToString()
                    }
                }
                $field.DefaultValue=$setDefVal
                $field.Update($PushChanges)
            }
            elseif(($type -ne "UserMulti") -and ($type -ne "User") -and ($type -ne "Note"))
            {
                $defaultValues = $listColumn.DefaultValues
                if(($defaultValues -ne $null) -and ($defaultValues -ne ""))
                {
                    $field=$web.Fields.GetFieldByInternalName($column.InternalName)
                    $field.DefaultValue=$defaultValues
                    $field.Update($PushChanges)
                }
            }
    }
    catch
    {
        Write-Host "`nException for site column :" $listColumn.ColumnName "`n" $Error -ForegroundColor Red
        Write-Output "`nException for site column :" $listColumn.ColumnName "`n" $Error 
    }
}





# =================================================================================
#
# FUNC: SetDeafultValue
# DESC: Set default value for column at list level
#
# =================================================================================
function SetDefaultValue($field, $listColumn, $site)
{
    try
    {
        $error.Clear()
		$defaultValues=$listColumn.DefaultValues
		$type=$listColumn.Type
        if(($type -eq "TaxonomyFieldType") -or ($type -eq "TaxonomyFieldTypeMulti"))
            {
                # Get the Taxonomy session of your site collection               
                $session = new-object Microsoft.SharePoint.Taxonomy.TaxonomySession($site)
                $termStore = $session.TermStores[$listColumn.TermStore]
                $taxGroup=$listColumn.TaxonomyGroup
                if($listColumn.TaxonomyGroup -eq "M&E Ad Sales")
                {
                    $taxGroup="M＆E Ad Sales"
                }
                $termStoreGroup = $termStore.Groups[$taxGroup]
                $termSet = $termStoreGroup.TermSets[$listColumn.TermSet]
                
                $setDefVal=""
                if($type -eq "TaxonomyFieldTypeMulti")
                {
                    $defaultValues = @($defaultValues.Split(","))
                    foreach($value in $defaultValues)
                    {
                        if(($value -ne $null) -and ($value -ne ""))
                        {
							$terms=$termSet.Terms
							if($value.Contains(";"))
							{
								$multiLevelTaxonomyTerm=$value.Split(";")
								$m=0;
								foreach($termLabel in $multiLevelTaxonomyTerm)
								{
								   $term = $terms | Where-Object {$_.Name -eq $termLabel} | Select-Object -First 1  
								   $terms = $term.Terms    
								}
							}
							else
							{
								 #Get the term to map the column to
								 $term = $terms | Where-Object {$_.Name -eq $value} | Select-Object -First 1
							}
                            
							$wssIDToSet = AddTaxonomyHiddenListItem $site.RootWeb $term 
                            $setDefVal=[string]$setDefVal+$wssIDToSet + ";#" + $term.GetPath() + [Microsoft.SharePoint.Taxonomy.TaxonomyField]::TaxonomyGuidLabelDelimiter + $term.Id.ToString()+";#"
                        }
                    }
                    $setDefVal=$setDefVal.TrimEnd(";#")
                }
                elseif($type -eq "TaxonomyFieldType")                
				{                    
                    if(($defaultValues -ne $null) -and ($defaultValues -ne ""))
                    {
						$terms=$termSet.Terms
						if($defaultValues.Contains(";"))
                        {
                            $multiLevelTaxonomyTerm=$defaultValues.Split(";")
                            $m=0;
                            foreach($termLabel in $multiLevelTaxonomyTerm)
                            {
                               $term = $terms | Where-Object {$_.Name -eq $termLabel} | Select-Object -First 1  
                               $terms = $term.Terms    
                            }
                        }
                        else
                        {
                             #Get the term to map the column to
                             $term = $terms | Where-Object {$_.Name -eq $defaultValues} | Select-Object -First 1
                        }
                        
						$wssIDToSet = AddTaxonomyHiddenListItem $site.RootWeb $term 
                        $setDefVal=[string]$wssIDToSet + ";#" + $term.GetPath() + [Microsoft.SharePoint.Taxonomy.TaxonomyField]::TaxonomyGuidLabelDelimiter + $term.Id.ToString()
                    }
                }
                $field.DefaultValue=$setDefVal
                $field.Update()
            }
            elseif(($type -ne "UserMulti") -and ($type -ne "User") -and ($type -ne "Note"))
            {
                if(($defaultValues -ne $null) -and ($defaultValues -ne ""))
                {                    
                    $field.DefaultValue=$defaultValues
                    $field.Update()
                }
            }
    }
    catch
    {
        Write-Host "`nException for site column : $($field.Title)`n" $Error -ForegroundColor Red
        Write-Output "`nException for site column : $($field.Title) n" $Error 
    }
}


# =================================================================================
#
# FUNC: AddTaxonomyHiddenListItem
# DESC: Add term to taxonomy hidden list.
#
# =================================================================================
function AddTaxonomyHiddenListItem($w,$termToAdd)
{
      $wssid = $null; #return value
      $count = 0;     
      $l = $w.Lists["TaxonomyHiddenList"]; 
      #check if Hidden List Item already exists
      foreach($item in $l.Items){
        $xml = [xml]$item.xml;#cast the xml TaxonomyHiddenList item values
        $temID = $xml.row.ows_IdForTerm #get the IdForTerm, this is the key that unlocks all the doors
        if($temID -eq $termToAdd.ID){ #compare the IdForTerm in the TaxonomyHiddenList item to the term in the termstore
            Write-Host $item.Name "Taxonomy Hidden List Item already exists" -ForegroundColor Yellow
            $wssid =  $item.ID; #get and return the WSSID needed to set the default clumn value
            return $wssid;
        } 
      }
      $newListItem = $l.Items.ADD();
      $newListItem["Title"] = $termToAdd.Name;
      $newListItem["IdForTermStore"] = $termToAdd.TermStore.ID;
      $newListItem["IdForTerm"] = $termToAdd.ID;
      $newListItem["IdForTermSet"] = $termToAdd.TermSet.ID;
      $newListItem["Term"] = $termToAdd.Name;
      $newListItem["Path"] = $divTerm.Name + ":" + $termToAdd.Name;
      $newListItem["CatchAllDataLabel"] = $termToAdd.Name + "#Љ|";  #"Љ" special char
      $newListItem["Term1033"] = $termToAdd.Name;
      $newListItem["Path1033"] = $divTerm.Name + ":" + $termToAdd.Name;
      $newListItem.Update();
      foreach($item in $l.Items){
        $xml = [xml]$item.xml;
        $temID = $xml.row.ows_IdForTerm
        if($temID -eq $termToAdd.ID){
            $wssid =  $item.ID; #get and return the WSSID needed to set the default clumn value
        } 
      }     
	  return $wssid;      
}

