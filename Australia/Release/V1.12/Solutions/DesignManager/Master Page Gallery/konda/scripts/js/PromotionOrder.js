var j = 0;
var Order = 100;
var SaveFinalOrder = 0;
var finalsaveCount = 0;
var getWebCountBefore = 0;
var getWebCountAfter = 0;
var getArticleCountBefore = 0;
var getArticleCountAfter = 0;
var IsDisplayArticleNotCompleted = true;
var displayloopcount = 0;
var saveloopcount = 0;
var arr = new Array();
var arrStatus = new Array();
var FinalOrder = new Array();
var AllSubwebs = new Array();
var flag = 0;
var PromotionOrderRows;
var sections = [];
var OtherSiteCollections = [];
var Viacom;
Viacom = Viacom || {};
Viacom.Constants = {
    TrueCheckConst: 'YES',
    FalseCheckConst: 'NO',
    approvedArticlesList: 'Cross Portal Approved Articles'
};
Viacom.Constants.CrossPortalApprovedArticles = {
    TaxKeyword: "TaxKeyword",
    IsaFeaturedArticle: "IsaFeaturedArticle",
    PictureURL: "PictureURL",
    ContentType: "ItemContentType",
    ParentSite: "CPS_ParentSite",
    ArticleUrl: "CPS_ArticleUrl",
    Author: "Author",
    SiteTitle: "SiteTitle",
    PromotionEndDate: "PromotionEndDate",
    ShownOnHomepageOrder: "ShownOnHomepageOrder",
    PromotionStartDate: "PromotionStartDate",
    ParentItemId: "CPS_ParentItemId",
    ListId: "CPS_ParentListId",
    PictureURL: "CPS_PictureURL",
    ContentType: "CPS_ParentContentType",
    Title: "Title"
};
Viacom.CrossPortalArticle = {
    ArticlesList: [],
    SavedArticlesList: [],
    LoadSavedArticles: function () {
        var call = $.ajax({
            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/lists/getbytitle('" + Viacom.Constants.approvedArticlesList + "')/items?" +
                        "&$select=ID," + Viacom.Constants.CrossPortalApprovedArticles.ArticleUrl + "," + Viacom.Constants.CrossPortalApprovedArticles.ParentItemId + "," + Viacom.Constants.CrossPortalApprovedArticles.PromotionEndDate + "," + Viacom.Constants.CrossPortalApprovedArticles.PromotionStartDate + "," + Viacom.Constants.CrossPortalApprovedArticles.ShownOnHomepageOrder + "," + Viacom.Constants.CrossPortalApprovedArticles.ParentSite + "," + Viacom.Constants.CrossPortalApprovedArticles.Title,
            type: "GET",
            dataType: "json",
            headers: {
                Accept: "application/json;odata=verbose"
            }
        });
        call.done(function (data, textStatus, jqXHR) {
            var length = data.d.results.length;
            Viacom.CrossPortalArticle.SavedArticlesList = [];
            for (var index = 0; index < length; index++) {
                var item = data.d.results[index];
                var articleUrl = item[Viacom.Constants.CrossPortalApprovedArticles.ArticleUrl];
                if (articleUrl) {
                    Viacom.CrossPortalArticle.SavedArticlesList.push({
                        ArticleUrl: articleUrl,
                        Title: item[Viacom.Constants.CrossPortalApprovedArticles.Title],
                        ShowOrder: item[Viacom.Constants.CrossPortalApprovedArticles.ShownOnHomepageOrder],
                        ParentWeb: item[Viacom.Constants.CrossPortalApprovedArticles.ParentSite],
                        PromotionEndDate: PromotionOrder.getFormatedDate(item[PromotionOrder.searchProperties.PromotionEndDate]),
                        PromotionStartDate: PromotionOrder.getFormatedDate(item[PromotionOrder.searchProperties.PromotionStartDate]),
                        ID: item.ID,
                        ParentItemId: item[Viacom.Constants.CrossPortalApprovedArticles.ParentItemId],
                        URLPath: item[Viacom.Constants.CrossPortalApprovedArticles.ArticleUrl]
                    }
                  );
                }
            }
            PromotionOrder.getArticles();
            Viacom.CrossPortalArticle.ShowApproveBtn();
        });
        call.fail(function (jqXHR, textStatus, errorThrown) {
            console.log("Error getting Site Color" + jqXHR.responseText);
        });
    },
    ShowApproveBtn: function () {
        $("a.approve-article-btn").each(function () {
            var articleUrl = $(this).attr("data");
            var savedArtile = Viacom.CrossPortalArticle.SavedArticlesList.filter(function (itm) {
                return itm.ArticleUrl.toLowerCase() == articleUrl.toLowerCase();
            });
            if (savedArtile.length > 0) {
                $(this).hide();
            }
            else {
                $(this).show();
            }
        });
    },
    ApprovedArticlePopup: function (ArticleUrl, PromoStartDate, PromoEndDate) {
        var htmlElement = document.createElement('div');
        var innerHTML = '<div>' +
   ' <div style="padding-bottom: 10px;"><span style="padding-right: 5px;"> <nobr>Promotion Start Date</nobr></span><span class="PromotionStartDate"><input type="text" value="' + PromoStartDate + '" maxlength="45" id="PromotionStartDateDateTime" title="Promotion Start Date"></span></div>'
    + '<div style="padding-bottom: 10px;"><span style="padding-right: 10px;"> <nobr>Promotion End Date</nobr></span><span class="PromotionEndDate"><input type="text" value="' + PromoEndDate + '" maxlength="45" id="PromotionEndDateDateTime" title="Promotion End Date"></span></div>'
   + '<button style="margin-left: 30%;margin-top: 10px;" onclick="Viacom.CrossPortalArticle.CreateEntryinApprovedArticleList(\'' + encodeURI(ArticleUrl) + '\')">OK</button>'
   + '</div>';
        htmlElement.innerHTML = innerHTML;
        var options = {
            title: " ",
            width: 800,
            height: 300,
            allowMaximize: false,
            showClose: true,
            dialogReturnValueCallback: Viacom.CrossPortalArticle.OnArticlePopupClosed,
            html: htmlElement
        };
        SP.SOD.execute('sp.ui.dialog.js', 'SP.UI.ModalDialog.showModalDialog', options);
        $("#PromotionStartDateDateTime").datepicker({
            showOn: "both",
            buttonImageOnly: true,
            buttonImage: '/sites/Asia/SiteCollectionImages/calendar.gif',
            buttonText: "Calendar"
        });
        $("#PromotionEndDateDateTime").datepicker({
            showOn: 'both',
            buttonText: 'Show Date',
            buttonImageOnly: true,
            buttonImage: '/sites/Asia/SiteCollectionImages/calendar.gif',
            buttonText: "Calendar"
        });
        return false;
    },
    OnArticlePopupClosed: function (ArticleUrl, PromoStartDate, PromoEndDate) {

    },
    UpdateEntryinApprovedArticleList: function (itemId, order) {
        var context = new SP.ClientContext.get_current();
        var web = context.get_web();
        var list = web.get_lists().getByTitle(Viacom.Constants.approvedArticlesList);
        listItem = list.getItemById(itemId);
        listItem.set_item(Viacom.Constants.CrossPortalApprovedArticles.ShownOnHomepageOrder, order);
        listItem.update();
        context.load(listItem);
        context.executeQueryAsync(
          function () {
              SaveFinalOrder++;
          },
          function (sender, args) {
              SaveFinalOrder++;
              console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
          }
      );
    },
    CreateEntryinApprovedArticleList: function (ArticleUrl) {
        var articleDetails = Viacom.CrossPortalArticle.ArticlesList.filter(function (i) {
            return encodeURI(i.URLPath).toLowerCase() == ArticleUrl.toLowerCase();
        });
        if (articleDetails.length > 0) {
            articleDetails = articleDetails[0];
            var contentType = articleDetails.ContentType.split("\n");
            if (contentType.length > 0) {
                contentType = contentType[contentType.length - 1];
            }
            else {
                contentType = [0];
            }
            var startDate = $("#PromotionStartDateDateTime").val();
            var endDate = $("#PromotionEndDateDateTime").val();
            var context = new SP.ClientContext.get_current();
            var web = context.get_web();
            var list = web.get_lists().getByTitle(Viacom.Constants.approvedArticlesList);
            var itemCreateInfo = new SP.ListItemCreationInformation();
            var listItem = list.addItem(itemCreateInfo);
            listItem.set_item(Viacom.Constants.CrossPortalApprovedArticles.Title, articleDetails.Title);
            listItem.set_item(Viacom.Constants.CrossPortalApprovedArticles.ArticleUrl, articleDetails.FileRef);
            listItem.set_item(Viacom.Constants.CrossPortalApprovedArticles.ParentSite, articleDetails.ParentSite);
            listItem.set_item(Viacom.Constants.CrossPortalApprovedArticles.PromotionStartDate, startDate);
            listItem.set_item(Viacom.Constants.CrossPortalApprovedArticles.PromotionEndDate, endDate);
            listItem.set_item(Viacom.Constants.CrossPortalApprovedArticles.ParentItemId, articleDetails.ID);
            listItem.set_item(Viacom.Constants.CrossPortalApprovedArticles.PictureURL, articleDetails.PictureURL);
            listItem.set_item(Viacom.Constants.CrossPortalApprovedArticles.ListId, articleDetails.ListId);
            listItem.set_item(Viacom.Constants.CrossPortalApprovedArticles.ContentType, contentType);
            listItem.update();
            context.load(listItem);
            context.executeQueryAsync(
              function () {
                  SP.UI.ModalDialog.commonModalDialogClose(1, 1);
                  Viacom.CrossPortalArticle.LoadSavedArticles();
              },
              function (sender, args) {
                  console.log("Error Occured");
                  alert('Request failed. ' + args.get_message() +
                  '\n' + args.get_stackTrace());
              }
          );
        }

    },
    OnItemsError: function (sender, args) {
        console.log("Error Occured");
        alert('Request failed. ' + args.get_message() +
        '\n' + args.get_stackTrace());
    }
};
var PromotionOrder = {
    articleSearchResults: null,
    sameSiteCollectQueryText: '((ContentType:"Konda Article Page"*) OR  (ContentType:"Konda Event Article Page"*) OR (ContentType:"Konda Gallery Article Page"*) OR (ContentType:"Konda Video Article Page"*) OR (ContentType:"Konda Survey Article Page"*)) AND (IsaFeaturedArticleOWSBOOL:"true" AND PromotionStartDate<=Today AND PromotionEndDate>=Today) AND ((path:{SiteCollection.URL}) AND PromotionRequestOWSCHCM:"Homepage" AND -PromotionRequestOWSCHCM:"Upcoming Events")',
    searchProperties: { Title: "Title", ID: "ListItemID", SiteTitle: "SiteTitle", SiteUrl: "SPSiteURL", PromotionRequest: "PromotionRequestOWSCHCM", PromotionStartDate: "PromotionStartDate", PromotionEndDate: "PromotionEndDate", IsaFeaturedArticle: "IsaFeaturedArticleOWSBOOL", Created: "CreatedOWSDATE", PageOrder: "ShowOnHomePageOrder", Author: "Author", Path: "Path", Tags: "owstaxidmetadataalltagsinfo" },
    getArticles: function () {
        var querytext = PromotionOrder.sameSiteCollectQueryText.replace("{SiteCollection.URL}", _spPageContextInfo.siteAbsoluteUrl);
        var searchRestURL = _spPageContextInfo.siteAbsoluteUrl + "/_api/search/query?querytext='" + querytext + "'&selectproperties='" + PromotionOrder.searchProperties.Path + "%2c" + PromotionOrder.searchProperties.Title + "%2c" + PromotionOrder.searchProperties.ID + "%2c" + PromotionOrder.searchProperties.PromotionStartDate + "%2c" + PromotionOrder.searchProperties.PromotionEndDate + "%2c" + PromotionOrder.searchProperties.IsaFeaturedArticle + "%2c" + PromotionOrder.searchProperties.PageOrder + "%2c" + PromotionOrder.searchProperties.PromotionRequest + "%2c" + PromotionOrder.searchProperties.Author + "%2c" + PromotionOrder.searchProperties.Tags + "%2c" + PromotionOrder.searchProperties.SiteTitle + "'&rowlimit=500&clienttype='ContentSearchRegular'&trimduplicates=false";
        $.ajax({
            url: searchRestURL,
            type: "GET",
            headers: {
                "Accept": "application/json;odata=verbose"
            },
            success: function (data, textStatus, xhr) {
                arr = new Array();
                var query = data.d.query;
                PromotionOrder.articleSearchResults = PromotionOrder.getResultSet(query.PrimaryQueryResult.RelevantResults.Table.Rows.results);
                var itemsCount = PromotionOrder.articleSearchResults.length
                for (var index = 0; index < itemsCount; index++) {
                    var item = PromotionOrder.articleSearchResults[index];
                    var Path = item[PromotionOrder.searchProperties.Path];
                    var articleUrl = Path;
                    var parentWeb = Path.split('Pages')[0];
                    var urlParts = Path.split('/');
                    var FileRef = "";
                    for (var i = 3; i < urlParts.length ; i++) {
                        FileRef += "/" + urlParts[i];
                    }
                    var SiteCollectionName = "GREENROOM";
                    if (Path.toLowerCase().indexOf("/sites/") > 0) {
                        if (urlParts.length > 3) {
                            SiteCollectionName = urlParts[4];
                        }
                    }
                    Path = parentWeb + 'Pages/Forms/EditForm.aspx?ID=' + item[PromotionOrder.searchProperties.ID];
                    var duplicateArticle = arr.filter(function (i) {
                        return i.URLPath.toLowerCase() == Path.toLowerCase();
                    });
                    if (duplicateArticle.length == 0) {
                        arr.push({
                            ID: item[PromotionOrder.searchProperties.ID],
                            Title: item[PromotionOrder.searchProperties.Title],
                            PSite: SiteCollectionName + " | " + item[PromotionOrder.searchProperties.SiteTitle],
                            ShowOrder: item[PromotionOrder.searchProperties.PageOrder],
                            FileRef: FileRef,
                            ArticleUrl: articleUrl,
                            ParentWeb: parentWeb,
                            IsaFeaturedArticle: item[PromotionOrder.searchProperties.IsaFeaturedArticle],
                            PromotionEndDate: PromotionOrder.getFormatedDate(item[PromotionOrder.searchProperties.PromotionEndDate]),
                            PromotionStartDate: PromotionOrder.getFormatedDate(item[PromotionOrder.searchProperties.PromotionStartDate]),
                            PromotionRequest: item[PromotionOrder.searchProperties.PromotionRequest],
                            Created: PromotionOrder.getFormatedDate(item[PromotionOrder.searchProperties.Created]),
                            CreatedBy: PromotionOrder.getFormatedAuthor(item[PromotionOrder.searchProperties.Author]),
                            CrossSiteArticle: Viacom.Constants.FalseCheckConst,
                            URLPath: Path
                        });
                    }
                }
                var itemsCount = Viacom.CrossPortalArticle.SavedArticlesList.length
                for (var index = 0; index < itemsCount; index++) {
                    var item = Viacom.CrossPortalArticle.SavedArticlesList[index];
                    var Path = item.ArticleUrl;
                    var articleUrl = Path;
                    var parentWeb = Path.split('Pages')[0];
                    var urlParts = Path.split('/');
                    var FileRef = "";
                    for (var i = 3; i < urlParts.length ; i++) {
                        FileRef += "/" + urlParts[i];
                    }
                    var SiteCollectionName = "GREENROOM";
                    if (Path.toLowerCase().indexOf("/sites/") > 0) {
                        if (urlParts.length > 3) {
                            SiteCollectionName = urlParts[4];
                        }
                    }
                    //Get Article author and site title from crossportal article list as we are not saving it
                    var author = "";
                    var siteTitle = "";
                    var articleDetails = Viacom.CrossPortalArticle.ArticlesList.filter(function (i) {
                        return encodeURI(i.URLPath).toLowerCase() == articleUrl.toLowerCase();
                    });
                    if (articleDetails.length > 0) {
                        articleDetails = articleDetails[0];
                        author = articleDetails[Viacom.Constants.CrossPortalApprovedArticles.Author];
                        siteTitle = " | " + articleDetails[Viacom.Constants.CrossPortalApprovedArticles.SiteTitle];
                    }
                    Path = parentWeb + 'Pages/Forms/EditForm.aspx?ID=' + item.ParentItemId;
                    var duplicateArticle = arr.filter(function (i) {
                        return i.URLPath.toLowerCase() == Path.toLowerCase();
                    });
                    if (duplicateArticle.length == 0) {
                        arr.push({
                            ID: item.ID,
                            Title: item.Title,
                            PSite: SiteCollectionName + siteTitle,
                            ShowOrder: item.ShowOrder,
                            FileRef: FileRef,
                            ArticleUrl: articleUrl,
                            ParentWeb: parentWeb,
                            IsaFeaturedArticle: "",
                            PromotionEndDate: item.PromotionEndDate,
                            PromotionStartDate: item.PromotionStartDate,
                            PromotionRequest: "",
                            Created: "",
                            CrossSiteArticle: Viacom.Constants.TrueCheckConst,
                            CreatedBy: author,
                            URLPath: Path
                        });
                    }
                }
                arr.sort(compare);
                display();
            },
            error: function (xhr) {
                console.log(xhr.status + ': ' + xhr.statusText);
            }
        });
    },
    getResultItem: function (resultsItem) {
        var dataitem = {};
        for (var index = 0; index < resultsItem.length; index++) {
            dataitem[resultsItem[index].Key] = resultsItem[index].Value;
        }
        return dataitem;
    },
    getFormatedDate: function (dateString) {
        var formatedDate = ""
        if (dateString) {
            formatedDate = new Date(dateString);
            formatedDate = formatedDate.format('MMM. dd, yyyy');
        }
        return formatedDate;
    },
    getFormatedAuthor: function (userstring) {
        var user = ""
        if (userstring) {
            user = userstring.split(";");
            user = user[0];
        }
        return user;
    },
    getResultSet: function (searchResults) {
        var resultItems = [];
        for (var index = 0; index < searchResults.length; index++) {
            var resultItem = PromotionOrder.getResultItem(searchResults[index]["Cells"].results);
            resultItems.push(resultItem);
        }
        return resultItems;
    }
};
function compare(a, b) {
    if (a.ShowOrder < b.ShowOrder)
        return -1;
    if (a.ShowOrder > b.ShowOrder)
        return 1;
    return 0;
}

function display() {
    //console.log( " getWebCountBefore " +getWebCountBefore +" getWebCountAfter "+ getWebCountAfter+" getArticleCountBefore " +getArticleCountBefore+" getArticleCountAfter "+ getArticleCountAfter);
    $('#table-1').html('<tr id="TableHead"><th style"width":15%">Article</th><th style"width":10%">ParentSite</th><th style"width":10%">Order</th><th style"width":10%">Promo StartDate</th><th style"width":10%">Promo EndDate</th><th style"width":10%">Created By</th></tr>');
    $('#PromotionOrder').css('position', 'relative');
    $('#PromotionOrder').prepend('<div id="loadingAnimation" style="display:none;position: absolute;top: 60%;left: 50%;margin-left: -22px;"><img id="loadingAnimationImg"  src="/SiteAssets/loadingAnimation.gif" /></div>');
    /*$('#table-1').append('<tr id="TableHead"><th style"width":15%">Article</th><th style"width":10%">ParentSite</th><th style"width":10%">Order</th><th  style"width":10%">Promo StartDate</th><th  style"width":10%">Promo EndDate</th><th  style"width":10%">Created By</th></tr>');*/
    for (var i = 0; i < arr.length; i++) {
        var EndDate;
        var StartDate;
        if (arr[i].PromotionStartDate) {
            StartDate = new Date(arr[i].PromotionStartDate);
            StartDate = StartDate.format('MMM, dd yyyy');
			if(StartDate.indexOf('NaN') > -1){
				StartDate = arr[i].PromotionStartDate.replace(',','').replace('.',',')
			}
        }
        else {
            StartDate = "";
        }
        if (arr[i].PromotionEndDate) {
            EndDate = new Date(arr[i].PromotionEndDate);
            EndDate = EndDate.format('MMM, dd yyyy');
			if(EndDate.indexOf('NaN') > -1){
				EndDate = arr[i].PromotionEndDate.replace(',','').replace('.',',')
			}
        }
        else {
            EndDate = "";
        }
        var showOrder;
        if (arr[i].ShowOrder) {
            showOrder = arr[i].ShowOrder;
        }
        else {
            showOrder = "";
        }
        var promoteInputId = arr[i].FileRef.trim().replace(/[^a-z0-9]+/gi, '-');	/* Remove / from url*/
        // promoteInputId=promoteInputId.split(".").join("");	/* Remove / from url*/
        $('#table-1').append('<tr class id="' + arr[i].ID + ":" + arr[i].PSite + ":" + arr[i].FileRef + ":" + arr[i].CrossSiteArticle + '" style="cursor: move;"><td><a target="_blank" href="' + arr[i].FileRef + '">' + arr[i].Title + '</a></td><td>' + arr[i].PSite + '</td><td>' + showOrder + '</td><td>' + StartDate + '</td><td>' + EndDate + '</td><td>' + arr[i].CreatedBy + '</td></tr>');
    }
    $("#table-1").tableDnD({
        onDragClass: "myDragClass",
        onDrop: function (table, row) {
            while (FinalOrder.length > 0) {
                FinalOrder.pop();
            }
            var rows = table.tBodies[0].rows;
            var debugStr = "<br /> Row dropped was " + row.id + "<br /> New order: ";
            for (var i = 0; i < rows.length; i++) {
                debugStr += rows[i].id + "";
                FinalOrder.push(rows[i].id);
                debugStr += breaktag = "<br />";
            }
            $('#debugArea').html(debugStr);
            //   $('#debugArea').hide();
        },
        onDragStart: function (table, row) {
            $('#debugArea').html("Started dragging row " + row.id);
        }
    });

}



function PromoteRequestChanged() {
    if (FinalOrder.length == 0) {
        var rows = $("#table-1")[0].rows;
        for (var i = 0; i < rows.length; i++) {
            FinalOrder.push(rows[i].id);
        }
    }
}

function SavePomotionOrderProperties() {
    $("#loadingAnimation").css("display", "block");
    for (i = 0; i < arr.length; i++) {
        var Contents = FinalOrder[i + 1];
        if (Contents) {
            var Data = Contents.split(":");
            var sitePath = Data[2].split('Pages')[0].replace(/^\/|\/$/g, '');
            var articleUrl = Data[2].split("'").join("''");
            var isCrossPortaSrticle = Data[3];
            if (isCrossPortaSrticle == Viacom.Constants.TrueCheckConst) {
                Viacom.CrossPortalArticle.UpdateEntryinApprovedArticleList(Data[0], Order);
            }
            else {
                SaveItems(sitePath, Data[0], articleUrl);
            }
            finalsaveCount++;
            Order++;
        }
    }
    IsSaveOrderCompleted();
}
function SaveOrder() {
    SavePomotionOrderProperties();
    setTimeout(function () {
        alert("Some error occurred this page will be reloaded");
        location.reload();
    }, 500000);
}

function IsSaveOrderCompleted() {
    saveloopcount++;
    //console.log( " SaveFinalOrder " +SaveFinalOrder +" finalsaveCount " +finalsaveCount +" saveloopcount " +saveloopcount);
    if (SaveFinalOrder == finalsaveCount && saveloopcount < 300) {
        $("#loadingAnimation").css("display", "none");
        location.reload();
    }
    else {
        setTimeout(function () { IsSaveOrderCompleted(); }, 1000);
    }
}


function SaveItems(sitename, ItemID, URL) {
    try {
        var finalPromotionOrder = Order;
        $.ajax({
            url: "/" + sitename + "/_api/web/GetFileByServerRelativeUrl('" + URL + "')/checkOutType",
            headers: { "Accept": "application/json; odata=verbose" },
            success: function (data) {
                if (data.d.CheckOutType == 0) {
                    SaveFinalOrder++;
                    console.log('The file is checked out ' + URL);
                    return;
                }
                var clientContext = new SP.ClientContext("/" + sitename);
                var webSite = clientContext.get_web();
                var list = webSite.get_lists().getByTitle("Pages");
                var pageItem = list.getItemById(ItemID);
                var pageFile = pageItem.get_file();
                pageFile.checkOut();
                clientContext.load(pageFile)
                clientContext.load(pageItem)
                clientContext.executeQueryAsync(function () {
                    pageItem.set_item('ShownOnHomepageOrder', finalPromotionOrder);
                    pageItem.update();
                    clientContext.executeQueryAsync(function () {
                        pageFile.checkIn();
                        clientContext.executeQueryAsync(function () {
                            pageFile.publish();
                            clientContext.executeQueryAsync(function () {
                                SaveFinalOrder++;
                            },
                      function (sender, args) {
                          SaveFinalOrder++;
                          console.log(args.get_message());
                      });
                        },
                     function (sender, args) {
                         SaveFinalOrder++;
                         console.log(args.get_message());
                     });
                    },
                function (sender, args) {
                    SaveFinalOrder++;
                    console.log(args.get_message());
                });
                },
                function (sender, args) {
                    SaveFinalOrder++;
                    console.log(args.get_message());
                });
            }
        });
    }
    catch (e) {
        SaveFinalOrder++;
        console.log(e);
    }
}