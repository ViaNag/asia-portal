# =================================================================================
#
#Main Function to Add publishing pages for given page layouts
#
# =================================================================================
function CreatePublishingPages([string]$ConfigPath = "")
{
    $Error.Clear();
	$cfg = [xml](get-content $ConfigPath) 
	
    if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
	{    
		  Add-PsSnapin Microsoft.SharePoint.PowerShell
	}

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Error: Could not read config file. Exiting ..." -ForegroundColor Red
        Write-Output "Error: Could not read config file. Exiting ..."
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
    Write-Output "Sucessfully read config file $($ConfigPath) file"
	if($Error.Count -eq 0)
	{
        try
        {
            $error.clear()
            foreach($page in $cfg.Pages.Page)
            {
                CreatePublishingPage $page
            }
        }
        catch
        {
            Write-Host "`nException :" $Error -ForegroundColor Red
            Write-Output "`nException :" $Error
        }
	}
	else
	{
		Write-Host "Error: " $Error -ForegroundColor Red
        Write-Output "Error: " $Error
	}
}
# =================================================================================
#
# FUNC: CreatePublishingPage
# DESC: Create publishing page for given page layout .
# =================================================================================
function CreatePublishingPage([object] $pageXmlObj)
{    
	try
    {
        $error.Clear()		

		# Get the SiteURL
		$siteUrl = $pageXmlObj.SiteURL
			
		# Get the PageLayout URL
		$pageLayoutName = $pageXmlObj.PublishingPageLayoutName
		
		# Get the Page URL
		$pageUrl =$pageXmlObj.PageURL
		
		# Get the Title of the Page which is going to get created
		$pageTitle = $pageXmlObj.PageTitle
		
		# Created page set welcome page or not 
		[boolean]$isWelcomePage	=[System.Convert]::ToBoolean(@{$true="false";$false=$pageXmlObj.IsWelcomePage}[$pageXmlObj.IsWelcomePage -eq ""])
		
		# Get the SPWeb Object
		$spWeb = Get-SPWeb $siteUrl
		
		# Get the Publishing Site based on the SPSite
		$pubSite = New-Object Microsoft.SharePoint.Publishing.PublishingSite($spWeb.Site)

		# Initialize the PublishingWeb based on the SPWeb
		$pubWeb = [Microsoft.SharePoint.Publishing.PublishingWeb]::GetPublishingWeb($spWeb)

		# Get the PageLayouts Installed on the Publishing Site
		$layouts = $pubSite.GetPageLayouts($False)

		# Get given PageLayout
		$pageLayout = $layouts | Where-Object {$_.Name -eq $pageLayoutName}

		# Create a new publishing page.
		$page = $pubWeb.AddPublishingPage($pageUrl, $pageLayout)

		# Assign the Title for the Page
		$page.Title = $pageTitle

		# Update the Page
		$page.Update();

		# Check in the Page with Comments
		$page.CheckIn("Page checked in automatically by PowerShell script")

		# Publish the Page With Comments
		$page.ListItem.File.Publish("Page Published")		

        Write-Host "Page $pageTitle created Sucessfully." -ForegroundColor Green
		Write-Output "Page $pageTitle created Sucessfully."
        
		#If newly created page set as welcome page 
		If ($isWelcomePage)
		{
			$folder = $spWeb.RootFolder
			$folder.WelcomePage =$page.Url;  
			$folder.update();	
	        Write-Host "Page $pageTitle set as welcome page." -ForegroundColor Green
		    Write-Output "Page $pageTitle set as welcome page."
		}  
		$spWeb.Dispose()
	}
    catch
    {
        Write-Host "`nException for page :" $pageXmlObj.PageTitle "`n" $Error -ForegroundColor Red
        Write-Output "`nException for page :" $pageXmlObj.PageTitle "`n" $Error
    }
}
