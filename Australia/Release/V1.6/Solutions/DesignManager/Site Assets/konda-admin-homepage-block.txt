﻿
<div id="greenroom-admin-block" class="greenroom-admin-block">
   <ul>
<li><a href="../Lists/campaigns/" target="_blank">View all campaigns</a> | <a href="javascript:void(0)" onclick="javascript:openPopupWindow('../_layouts/15/Upload.aspx?List={' + campaignGuid + '}&RootFolder=&IsDlg=1', 'Add a new campaign')">Add a new campaign</a></li>
<li><a href="../Pages/Manage-Featured-Now-Ordering.aspx" target="_blank">Manage Promotion Order for Featuring Now section</a></li>
<li><a href="../Pages/homepage-preview.aspx">Homepage Preview Page</a></li>
<li><a href="../Pages/Forms/AllItems.aspx" target="_blank">View All Pages</a></li>
<li><a href="../Lists/QuickLinks/" target="_blank">View all Quick Links</a> | <a href="../Lists/QuickLinks/NewForm.aspx">Add a new Quick Link</a></li>
<li><a href="../ReusableContent/" target="_blank">Reusable Content</a></li>
<li><a href="https://greenroom.viacom.com/company/Lists/Business%20Calendar/NewForm.aspx">Add a company holiday</a></li>
<li><br /></li>
</ul>
</div>
 <script type='text/javascript'> 
  var campaignGuid;
  var eventGuid;
     $(document).ready(function(){
           GetCampaignListId();
        });

		function openPopupWindow(listUrl, popupTitle){
		openDialogWindow(listUrl,popupTitle,'613','538');
		}
	  
	function GetCampaignListId() {
    $.ajax({
        url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getByTitle('campaigns')?$select=id",
        type: "GET",
        headers: {
            "accept": "application/json;odata=verbose",
        },
		 success: function(data){
			campaignGuid = data.d.Id;
        },
        error: function(error){
            /*ADFS Alert Issue*/console.log(JSON.stringify(error));
        }
});
}


function openDialogWindow(navUrl,pageTitle,objWidth,objHeight){
var myvar = navUrl;
var options =
{
url: myvar,
width: objWidth,
height: objHeight,
title: pageTitle,
};
SP.UI.ModalDialog.showModalDialog(options);
}
</script>

