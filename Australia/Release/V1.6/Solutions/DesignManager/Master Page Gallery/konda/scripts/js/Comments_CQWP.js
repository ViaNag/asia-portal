﻿var clientContext;
var listID;


function PostComment()
{
   	var relativeURL = window.location.href;
	var urlParts=relativeURL.split('/');
	var parentsite = '';
	for(var i=0;i<4;i++)
	{
		parentsite+=urlParts[i]+'/';
	}
	
	var userComments = $('#CommentArea').val();

	if ( userComments == null || userComments == '' )
	{	
	}
	else
	{
		createComment(parentsite,relativeURL,userComments);
	}
}


function createComment(context,relativeURL,userComments)
{
    clientContext = new SP.ClientContext(context);
    var oList = clientContext.get_web().get_lists().getByTitle('Comments');
    
    var itemCreateInfo = new SP.ListItemCreationInformation();
    this.oListItem = oList.addItem(itemCreateInfo);
        
    oListItem.set_item('Title', relativeURL);
    oListItem.set_item('_Comments', userComments);
        
    oListItem.update();
    clientContext.load(oListItem);
        
    clientContext.executeQueryAsync(Function.createDelegate(this, this.onCommentQuerySucceeded), Function.createDelegate(this, this.onCommentQueryFailed));
}

function onCommentQuerySucceeded()
{
	$('#CommentArea').val('');
	//location.reload();
    //DisplayComments();
}

function onCommentQueryFailed(sender, args)
{
    /*ADFS Alert Issue*/console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
}	

function onDisplayCommentsFailed(sender, args)
{
    /*ADFS Alert Issue*/console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
}	

/*----------------------------------------------------------------------------------------------------------------*/
/*---------------------------DISPLAY TEMPLATE CODE FOR RELATED ARTICLES ( BOTTOM )--------------------------------*/

function CommentsDisplayTemplateGetLikeCount(listItemID)
{	

   	var relativeURL = window.location.href;
	var urlParts=relativeURL.split('/');
	var parentsite = '';
	for(var i=0;i<4;i++)
	{
		parentsite+=urlParts[i]+'/';
	}

    var context = new SP.ClientContext(parentsite);
    var list = context.get_web().get_lists().getByTitle('Comments');
    var item = list.getItemById(listItemID);
	
    context.load(item, "LikedBy", "ID", "LikesCount");
    context.executeQueryAsync(Function.createDelegate(this, function (success) {
        // Check if the user id of the current users is in the collection LikedBy. 
        var likeDisplay = true;
        var $v_0 = item.get_item('LikedBy');
        var itemc = item.get_item('LikesCount');
        if (!SP.ScriptHelpers.isNullOrUndefined($v_0)) {
            for (var $v_1 = 0, $v_2 = $v_0.length; $v_1 < $v_2; $v_1++) {
                var $v_3 = $v_0[$v_1];
                if ($v_3.$1E_1 === _spPageContextInfo.userId) {
                    //cb(true, item.get_item('LikesCount'));
                    //alert("Liked by me");
                    likeDisplay = false;
                }
            }
        }
        
        CDTChangeLikeText(likeDisplay, itemc, listItemID);

    }), Function.createDelegate(this, function (sender, args) {
        /*ADFS Alert Issue*/console.log(args.get_message());
    }));
}

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

function CDTChangeLikeText(like, count, listItemID) 
{
    if (like) {
        $(".CDTLikeButton"+listItemID).html('<span id="cswp-like-text">Like</span>');
    }
    else {
        $(".CDTLikeButton"+listItemID).html('<span id="cswp-like-text">Unlike</span>');
    }
    var htmlstring = String(count) + ' ' +'<img alt="" src="/SiteCollectionImages/pagelayouts/greenroom-tile-like.png" />';
    if (count > 0)
        $(".CDTlikecount"+listItemID).html(htmlstring)
    else
        $(".CDTlikecount"+listItemID).html(String(0) + ' ' + '<img alt="" src="/SiteCollectionImages/pagelayouts/greenroom-tile-like.png" />');        
}

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

function CDTLikePage(listItemID,listID) {

   	var relativeURL = window.location.href;
	var urlParts=relativeURL.split('/');
	var parentsite = '';
	for(var i=0;i<4;i++)
	{
		parentsite+=urlParts[i]+'/';
	}

    var like = false;
    var likeButtonText = $(".CDTLikeButton"+listItemID).text();
    if (likeButtonText != "") {
        if (likeButtonText == "Like")
            like = true;

        var aContextObject = new SP.ClientContext(parentsite);
        EnsureScriptFunc('reputation.js', 'Microsoft.Office.Server.ReputationModel.Reputation', function () {
            Microsoft.Office.Server.ReputationModel.
            Reputation.setLike(aContextObject,
                //listID.substring(1, 37),
                listID,
                listItemID, like);

            aContextObject.executeQueryAsync(
                function () {
                    //alert(String(like));
                    CommentsDisplayTemplateGetLikeCount(listItemID);
                }, function (sender, args) {
                    //alert('F0');
                });
        });
    }
}

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

function ReportAbuse(itemID)
{
   	var relativeURL = window.location.href;
	var urlParts=relativeURL.split('/');
	var parentsite = '';
	for(var i=0;i<4;i++)
	{
		parentsite+=urlParts[i]+'/';
	}

    var clientContext = new SP.ClientContext(parentsite);
    var oList = clientContext.get_web().get_lists().getByTitle('Comments');

    this.oListItem = oList.getItemById(itemID);

    oListItem.set_item('ReportAbuse', '1');

    oListItem.update();

    clientContext.executeQueryAsync(Function.createDelegate(this, this.onReportAbuseSucceeded), Function.createDelegate(this, this.onReportAbuseFailed));
}

function onReportAbuseSucceeded()
{
    alert('This comment has been removed temporarily !');
}

function onReportAbuseFailed(sender, args)
{
    /*ADFS Alert Issue*/console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
}

/*----------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/