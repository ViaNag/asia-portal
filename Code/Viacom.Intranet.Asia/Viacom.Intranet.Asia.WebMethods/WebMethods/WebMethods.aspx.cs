﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.Services;
using System.Web.Script.Services;

namespace Viacom.Intranet.Asia.WebMethods
{
    public class WebMethods : Page
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteURL"></param>
        /// <param name="PollLookupValue"></param>
        /// <param name="listname"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(UseHttpGet = false)]
        public static string GetPollUsersInfo(string siteURL, string PollLookupValue, string listname, string userID, string indexCounter)
        {
            string folderJSON = string.Empty;
            Boolean IsCurrentUserPolled = false;
            string mypollAnswer = string.Empty;
            try
            {
                using (SPSite site = new SPSite(siteURL))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        SPList list = web.Lists[listname];
                        SPQuery currentUserQuery = new SPQuery();
                        currentUserQuery.Query = "<Where><And><Eq><FieldRef Name='PollNameLookUp' /><Value Type='Lookup'>" + PollLookupValue + "</Value></Eq><Eq><FieldRef Name='Author' LookupId='TRUE'/><Value Type='Integer'>" + userID + "</Value></Eq></And></Where>";

                        SPListItemCollection currentUserItems = list.GetItems(currentUserQuery);
                        if (currentUserItems.Count > 0)
                        {
                            IsCurrentUserPolled = true;
                            foreach (SPListItem currentUserItem in currentUserItems)//loop over item collection
                            {
                                mypollAnswer = (string)currentUserItem["Response"];
                            }
                        }


                        SPQuery myQuery = new SPQuery();
                        myQuery.Query = "<Where><Eq><FieldRef Name='PollNameLookUp' /><Value Type='Lookup'>" + PollLookupValue + "</Value></Eq></Where><GroupBy><FieldRef Name='PollUserResponse' /></GroupBy>";
                        SPListItemCollection items = list.GetItems(myQuery);

                        DataTable itemDT = new DataTable();//Create datatable
                        DataColumn itemDC = new DataColumn("MyResponse");
                        itemDT.Columns.Add(itemDC);

                        itemDC = new DataColumn("PollName");
                        itemDT.Columns.Add(itemDC);

                        itemDC = new DataColumn("AnswerChoice");
                        itemDT.Columns.Add(itemDC);

                        itemDC = new DataColumn("Count");
                        itemDT.Columns.Add(itemDC);

                        itemDC = new DataColumn("Index");
                        itemDT.Columns.Add(itemDC);

                        DataRow itemDR = null;

                        if (items != null && items.Count > 0)
                        {

                            string pollAnswer = string.Empty;
                            int i = 0;

                            foreach (SPListItem item in items)//loop over item collection
                            {
                                if (i == 0)
                                {
                                    i++;
                                    pollAnswer = (string)item["Response"]; //Store the item path
                                    itemDR = itemDT.NewRow();//Create row
                                    if (IsCurrentUserPolled == true)
                                    {
                                        itemDR["MyResponse"] = mypollAnswer;    // set the selected value
                                    }
                                    else
                                    {
                                        itemDR["MyResponse"] = "NAN";   // set false when no value selected
                                    }

                                    itemDR["PollName"] = PollLookupValue;
                                    itemDR["AnswerChoice"] = pollAnswer;
                                    itemDR["Count"] = 1;
                                    itemDR["Index"] = Convert.ToInt32(indexCounter);

                                    itemDT.Rows.Add(itemDR);//add row in datatable
                                }
                                else
                                {
                                    if (pollAnswer == (string)item["Response"])
                                    {
                                        itemDT.Rows[i - 1]["Count"] = Convert.ToInt32(itemDT.Rows[i - 1]["Count"]) + 1;
                                    }
                                    else
                                    {
                                        i++;
                                        pollAnswer = (string)item["Response"]; //Store the item path
                                        itemDR = itemDT.NewRow();//Create row
                                        if (IsCurrentUserPolled == true)
                                        {
                                            itemDR["MyResponse"] = mypollAnswer;    // set the selected value
                                        }
                                        else
                                        {
                                            itemDR["MyResponse"] = "NAN";   // set false when no value selected
                                        }
                                        itemDR["PollName"] = PollLookupValue;
                                        itemDR["AnswerChoice"] = pollAnswer;
                                        itemDR["Count"] = 1;
                                        itemDR["Index"] = Convert.ToInt32(indexCounter);
                                        itemDT.Rows.Add(itemDR);//add row in datatable
                                    }
                                }

                            }
                            itemDT.AcceptChanges();

                            folderJSON = GetJson(itemDT);

                        }
                        else
                        {

                            itemDR = itemDT.NewRow();
                            itemDR["MyResponse"] = "No item found.";
                            itemDR["Index"] = Convert.ToInt32(indexCounter);

                            itemDT.Rows.Add(itemDR);

                            folderJSON = GetJson(itemDT);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Research Polls", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                });

            }

            return folderJSON;
        }


        /// <summary>
        /// This method convert datatable into JSON string
        /// </summary>
        /// <param name="dTUsersTable"></param>
        /// <returns></returns>
        private static string GetJson(DataTable dTUsersTable)
        {
            try
            {
                //Declaration for serializer
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();

                try
                {
                    Dictionary<string, object> row = null;
                    //Iterating the Each row in the ADLDS user table
                    foreach (DataRow dr in dTUsersTable.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dTUsersTable.Columns)
                        {
                            //Adding a JSON string
                            row.Add(col.ColumnName.Trim(), dr[col]);
                        }
                        rows.Add(row);
                    }
                }
                catch (Exception ex)
                {
                    //stores the error
                    #region Error Log

                    #endregion
                }
                return serializer.Serialize(rows);
            }
            catch (Exception ex)
            {
                return null;

            }
        }
    }
}
