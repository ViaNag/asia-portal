using Microsoft.Office.Server.Search.WebControls;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Navigation;
using Microsoft.SharePoint.Taxonomy;
using Microsoft.SharePoint.WebPartPages;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.UI.WebControls.WebParts;
using System.Xml;

namespace Viacom.Intranet.Asia.Features.Viacom.Intranet.Asia.Countries
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("59620671-a1be-420b-9590-b3a176a51d2b")]
    public class ViacomIntranetAsiaEventReceiver : SPFeatureReceiver
    {
        #region  CONSTANTS
        private const string NavigationHeader = "Offices";
        private List<KeyValuePair<string, string>> SearchNavigationNodeList = new List<KeyValuePair<string, string>>()
                    {
                       new KeyValuePair<string, string>("Greenroom Asia", "/Search/pages/AsiaResults.aspx"),
                      new KeyValuePair<string, string>("Greenroom", "/Search/pages/GreenroomResults.aspx"),                 
                      new KeyValuePair<string, string>("Everything", "/Search/pages/results.aspx"),
                       new KeyValuePair<string, string>("People", "/Search/pages/peopleresults.aspx"),
                       new KeyValuePair<string, string>("This Site", "/Search/Pages/thisSiteResults.aspx?ud={ContextUrl}")
                    };
        private const string ConfigList = "Config";
        private const string LibrariesKey = "Libraries";
        private const string Title = "Title";
        private const string FetchProfileProperties = "FetchProfileProperties";
        private const string PageName = "pages/landing.aspx";
        private const string WelcomePage = "Pages/landing.aspx";
        private const string WebPartName = "Next Holiday";
        private const string QueryTemplate = "QueryTemplate";
        private const string CheckinMsg = "Next Holiday webpart has been updated";
        private const string AsiaPortal = "\"Asia Portal\"";
        private const string PagesKey = "CountryPages";
        private const string OldLink = "/sites/konda";
        private const string NewLink = "/sites/asia";
        private const string TechnologyPageName = "pages/technology.aspx";
        private const string WebPartTitle = "Videos and Tutorials";
        private const string ZoneID = "xd7ee700c2f8244ae91a39adad0237e41";
        private const int ZoneIndex = 2;
        private const string TechnologyKey = "TechnologyQuery";
        private const string SourceName = "SourceName";
        private const string SourceValue = "Local SharePoint Results";
        private const string ItemTemplateId = "~sitecollection/_catalogs/masterpage/Display Templates/Content Web Parts/Item_VideoAndTutorials.js";
        private const string ControlTemplateId = "~sitecollection/_catalogs/masterpage/Display Templates/Content Web Parts/Control_ListWithPaging.js";
        #endregion
        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            HttpContext oldContext = HttpContext.Current;
            try
            {                
                Guid siteId = ((SPWeb)properties.Feature.Parent).Site.ID;
                Guid webId = ((SPWeb)properties.Feature.Parent).ID;
                oldContext = HttpContext.Current;
                HttpContext.Current = null;
                
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite oSite = new SPSite(siteId))
                    {
                        if (oSite != null)
                        {
                            using (SPWeb currentWeb = oSite.OpenWeb(webId))
                            {
                                if (currentWeb != null)
                                {
                                    using (SPWeb rootWeb = oSite.RootWeb)
                                    {
                                        try
                                        {
                                            //Get Config list from Site Collection
                                            SPList oList = rootWeb.Lists.TryGetList(ConfigList);
                                            if (oList != null)
                                            {
                                                AddListToWeb(oList, currentWeb);
                                                //update existing Next Holiday webpart's query 
                                                UpdateNextHoliday(currentWeb);

                                                //Update links from Konda to Asia
                                                UpdateWebPartLinks(oList, currentWeb);

                                                //Delete existing CEWP & replace it with CSWP
                                                DeleteAndAddWebPart(oList, currentWeb);

                                            }

                                            //Add Country Title to Managed Metadata 
                                            AddWebTitleToMMS(currentWeb, currentWeb.Title);

                                            //Set welcome Page
                                            SetWelcomePage(currentWeb);
                                            CreateSearchNavigationSettings(oSite, currentWeb);
                                            CreateGlobalNavigationSettings(rootWeb, currentWeb);
                                        }
                                        catch (Exception ex)
                                        {
                                            SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Region site updation", TraceSeverity.Verbose, EventSeverity.Verbose), TraceSeverity.Verbose, ex.Message);
                                        }
                                    }

                                }

                            }
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Region site updation", TraceSeverity.Verbose, EventSeverity.Verbose), TraceSeverity.Verbose, ex.Message);
            }
            finally
            {
                HttpContext.Current = oldContext;
            }
        }

        /// <summary>
        /// Set welcome Page
        /// </summary>
        /// <param name="web">Current web</param>
        private void SetWelcomePage(SPWeb web)
        {
            SPFolder rootFolder = web.RootFolder;
            rootFolder.WelcomePage = WelcomePage;
            rootFolder.Update();
        }

        /// <summary>
        /// Update query of existing Next Holiday CSWP
        /// </summary>
        /// <param name="currentWeb"></param>
        private void UpdateNextHoliday(SPWeb currentWeb)
        {
            SPFile file = currentWeb.GetFile(PageName); // or what ever page you are interested in

            // Check out the file, if not checked out
            if (file.CheckOutType == SPFile.SPCheckOutType.None)
                file.CheckOut();
            using (SPLimitedWebPartManager wpm = file.GetLimitedWebPartManager(PersonalizationScope.Shared))
            {
                try
                {
                    wpm.Web.AllowUnsafeUpdates = true;
                    foreach (System.Web.UI.WebControls.WebParts.WebPart wp in wpm.WebParts)
                    {
                        if (wp.Title == WebPartName)
                        {
                            ContentBySearchWebPart cswp = (ContentBySearchWebPart)wp;
                            DataProviderScriptWebPart querySettings = new DataProviderScriptWebPart
                            {
                                PropertiesJson = cswp.DataProviderJSON
                            };
                            querySettings.Properties[QueryTemplate] += " AND PortalOWSCHCS=" + AsiaPortal;
                            cswp.DataProviderJSON = querySettings.PropertiesJson;
                            wpm.SaveChanges(cswp);
                            break;
                        }
                    }
                    // Update the file
                    file.Update();
                    file.CheckIn(CheckinMsg);
                    file.Publish(CheckinMsg);

                    // Disable the Unsafe Update
                    wpm.Web.AllowUnsafeUpdates = false;
                }
                catch (Exception ex)
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Update Next Holiday", TraceSeverity.Verbose, EventSeverity.Verbose), TraceSeverity.Verbose, ex.Message);
                }
            }
        }
        /// <summary>
        /// Update existing WebPart links from Konda to Asia
        /// </summary>
        /// <param name="oList"></param>
        /// <param name="currentWeb"></param>
        private void AddListToWeb(SPList oList, SPWeb currentWeb)
        {
            try
            {
                if (oList != null)
                {
                    //get libraries name to be created in country specific site
                    SPQuery oQuery = new SPQuery();
                    oQuery.Query = @"<Where><Eq><FieldRef Name=" + Title + "/><Value Type='Text'>" + LibrariesKey + "</Value></Eq></Where>";
                    oQuery.ViewFields = "<FieldRef Name=" + Title + "/><FieldRef Name=" + FetchProfileProperties + "/>";
                    SPListItemCollection collListItems = oList.GetItems(oQuery);
                    if (collListItems != null && collListItems.Count > 0)
                    {
                        SPListCollection allLists = currentWeb.Lists;
                        foreach (SPListItem oListItem in collListItems)
                        {
                            string oLibraries = oListItem[FetchProfileProperties].ToString();
                            string[] oLibrary = oLibraries.Split(',');
                            foreach (string listName in oLibrary)
                            {
                                if (!CheckIfListExistsInCollection(allLists, listName))
                                {
                                    allLists.Add(listName, " ", SPListTemplateType.DocumentLibrary);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("AddListToWeb", TraceSeverity.Verbose, EventSeverity.Verbose), TraceSeverity.Verbose, ex.Message);
            }
        }
        /// <summary>
        /// Update existing WebPart links from Konda to Asia
        /// </summary>
        /// <param name="oList"></param>
        /// <param name="currentWeb"></param>
        private void UpdateWebPartLinks(SPList oList, SPWeb currentWeb)
        {
            try
            {
                if (oList != null)
                {
                    SPQuery oQuery = new SPQuery();
                    oQuery.Query = @"<Where><Eq><FieldRef Name=" + Title + "/><Value Type='Text'>" + PagesKey + "</Value></Eq></Where>";
                    oQuery.ViewFields = "<FieldRef Name=" + Title + "/><FieldRef Name=" + FetchProfileProperties + "/>";
                    SPListItemCollection collListItems = oList.GetItems(oQuery);
                    if (collListItems != null && collListItems.Count > 0)
                    {
                        foreach (SPListItem oListItem in collListItems)
                        {
                            string oPages = oListItem[FetchProfileProperties].ToString();
                            string[] oPage = oPages.Split(',');
                            foreach (string page in oPage)
                            {
                                string fileName = "pages/" + page + ".aspx";
                                SPFile file = currentWeb.GetFile(fileName);
                                // Check out the file, if not checked out
                                if (file.CheckOutType == SPFile.SPCheckOutType.None)
                                    file.CheckOut();
                                using (SPLimitedWebPartManager wpm = file.GetLimitedWebPartManager(PersonalizationScope.Shared))
                                {
                                    try
                                    {
                                        wpm.Web.AllowUnsafeUpdates = true;
                                        foreach (System.Web.UI.WebControls.WebParts.WebPart wp in wpm.WebParts)
                                        {
                                            if (wp.GetType().ToString() == ("Microsoft.SharePoint.WebPartPages.ContentEditorWebPart"))
                                            {

                                                ContentEditorWebPart cewp = (ContentEditorWebPart)wp;
                                                string contentLinkVal = cewp.ContentLink;
                                                if (!String.IsNullOrEmpty(contentLinkVal) && contentLinkVal.ToLower().Contains(OldLink.ToLower()))
                                                {
                                                    contentLinkVal = contentLinkVal.ToLower().Replace(OldLink, NewLink);
                                                    cewp.ContentLink = contentLinkVal;
                                                    wpm.SaveChanges(cewp);
                                                }
                                                string htmlcontentl = cewp.Content.InnerText;
                                                if (!String.IsNullOrEmpty(htmlcontentl) && htmlcontentl.ToLower().Contains(OldLink.ToLower()))
                                                {
                                                    htmlcontentl = htmlcontentl.Replace(OldLink, NewLink);
                                                    XmlElement xmlContent = cewp.Content;
                                                    xmlContent.InnerText = htmlcontentl;
                                                    cewp.Content = xmlContent;
                                                    wpm.SaveChanges(cewp);
                                                }

                                            }
                                            string titleUrl = wp.TitleUrl;
                                            if (!String.IsNullOrEmpty(titleUrl))
                                            {
                                                if (titleUrl.ToLower().Contains(OldLink.ToLower()))
                                                {
                                                    titleUrl = titleUrl.ToLower().Replace(OldLink, NewLink);
                                                    wp.TitleUrl = titleUrl;
                                                    wpm.SaveChanges(wp);
                                                }
                                            }

                                        }
                                        // Update the file
                                        file.Update();
                                        file.CheckIn(CheckinMsg);
                                        file.Publish(CheckinMsg);

                                        // Disable the Unsafe Update
                                        wpm.Web.AllowUnsafeUpdates = false;
                                    }
                                    catch (Exception ex)
                                    {
                                        SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("UpdateWebPartLinks", TraceSeverity.Verbose, EventSeverity.Verbose), TraceSeverity.Verbose, ex.Message);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("UpdateWebPartLinks", TraceSeverity.Verbose, EventSeverity.Verbose), TraceSeverity.Verbose, ex.Message);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodes"></param>
        /// <param name="navNode"></param>
        /// <returns></returns>
        private bool CheckIfListExistsInCollection(SPListCollection listCollection, string listTitle)
        {
            foreach (SPList list in listCollection)
            {
                if (list.Title.ToLower() == listTitle.ToLower())
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Delete existing 'video & tutorial' CEWP and add new CSWP
        /// </summary>
        /// <param name="oList"></param>
        /// <param name="currentWeb"></param>
        private void DeleteAndAddWebPart(SPList oList, SPWeb currentWeb)
        {
            SPFile file = currentWeb.GetFile(TechnologyPageName); // or what ever page you are interested in

            // Check out the file, if not checked out
            if (file.CheckOutType == SPFile.SPCheckOutType.None)
                file.CheckOut();
            using (SPLimitedWebPartManager wpm = file.GetLimitedWebPartManager(PersonalizationScope.Shared))
            {
                try
                {
                    wpm.Web.AllowUnsafeUpdates = true;
                    foreach (System.Web.UI.WebControls.WebParts.WebPart wp in wpm.WebParts)
                    {
                        if (wp.Title == WebPartTitle)
                        {
                            wpm.DeleteWebPart(wp);
                            AddWebPart(oList, currentWeb, wpm);
                            break;
                        }
                    }
                    // Update the file
                    file.Update();
                    file.CheckIn(CheckinMsg);
                    file.Publish(CheckinMsg);

                    // Disable the Unsafe Update
                    wpm.Web.AllowUnsafeUpdates = false;
                }
                catch (Exception ex)
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Delete And AddWebPart", TraceSeverity.Verbose, EventSeverity.Verbose), TraceSeverity.Verbose, ex.Message);
                }
            }
        }

        /// <summary>
        /// Add new CSWP part
        /// </summary>
        /// <param name="oList"></param>
        /// <param name="currentWeb"></param>
        /// <param name="wpm"></param>
        private void AddWebPart(SPList oList, SPWeb currentWeb, SPLimitedWebPartManager wpm)
        {
            try
            {
                if (oList != null)
                {
                    SPQuery oQuery = new SPQuery();
                    oQuery.Query = @"<Where><Eq><FieldRef Name=" + Title + "/><Value Type='Text'>" + TechnologyKey + "</Value></Eq></Where>";
                    oQuery.ViewFields = "<FieldRef Name=" + Title + "/><FieldRef Name=" + FetchProfileProperties + "/>";
                    SPListItemCollection collListItems = oList.GetItems(oQuery);
                    if (collListItems != null && collListItems.Count == 1)
                    {
                        string technologyQuery = collListItems[0][FetchProfileProperties].ToString();
                        if (technologyQuery != null)
                        {
                            ContentBySearchWebPart cswp = new ContentBySearchWebPart();
                            cswp.Title = WebPartTitle;
                            cswp.ItemTemplateId = ItemTemplateId;
                            cswp.RenderTemplateId = ControlTemplateId;
                            DataProviderScriptWebPart querySettings = new DataProviderScriptWebPart
                            {
                                PropertiesJson = cswp.DataProviderJSON
                            };
                            querySettings.Properties[SourceName] = SourceValue;
                            querySettings.Properties[QueryTemplate] = technologyQuery;
                            cswp.DataProviderJSON = querySettings.PropertiesJson;
                            wpm.AddWebPart(cswp, ZoneID, ZoneIndex);
                            wpm.SaveChanges(cswp);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("AddWebPart", TraceSeverity.Verbose, EventSeverity.Verbose), TraceSeverity.Verbose, ex.Message);
            }
        }

        /// <summary>
        /// To add office web title to Managed Metadata Term Store
        /// </summary>
        /// <param name="webTitle"></param>
        private void AddWebTitleToMMS(SPWeb oSPWeb, string webTitle)
        {
            try
            {
                SPSite oSPSite = oSPWeb.Site;
                TaxonomySession TaxonomySession = new TaxonomySession(oSPSite);
                string termStore = GetMMDataFromConfig(oSPWeb, "TermStoreName");
                string termGroup = GetMMDataFromConfig(oSPWeb, "MMGroup");
                string termSet = GetMMDataFromConfig(oSPWeb, "TermSetName");
                string term = GetMMDataFromConfig(oSPWeb, "TermName");
                //Get instance of the Term Store
                TermStore oTermStore = TaxonomySession.TermStores[termStore];
                //Check if the Group already exists
                var cTermStore = oTermStore.Groups.Where(s => s.Name == termGroup);
                if (cTermStore.ToList().Count > 0)
                {
                    Group oGroup = oTermStore.Groups[termGroup];
                    var cTermSet = oGroup.TermSets.Where(s => s.Name == termSet).ToList();
                    if (cTermSet.Count > 0)
                    {
                        TermSet oTermSet = oGroup.TermSets[termSet];
                        var cTerm = oTermSet.Terms.Where(s => s.Name == term).ToList();
                        if (cTerm.Count > 0)
                        {
                            Term oTerm = oTermSet.Terms[term];
                            var cChildTerm = oTerm.Terms.Where(s => s.Name == webTitle).ToList();
                            if (cChildTerm == null || cChildTerm.Count == 0)
                            {
                                oTerm.CreateTerm(webTitle, CultureInfo.CurrentCulture.LCID);
                            }
                        }
                        DeleteExistingWebTitlefromMMS(oTermSet, webTitle);
                    }
                    oTermStore.CommitAll();
                    //Add terms to the term set

                }


            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("AddTitleToMMS", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }
        }

        private string GetMMDataFromConfig(SPWeb oSPWeb, string title)
        {
            try
            {
                SPWeb oWeb = oSPWeb.Site.RootWeb;
                SPList oList = oWeb.Lists.TryGetList("Config");
                if (oList != null)
                {
                    SPQuery query = new SPQuery();
                    query.Query = @"<Where><Eq><FieldRef Name='Title'/><Value Type='Text'>" + title + "</Value></Eq></Where>";
                    SPListItemCollection oItems = oList.GetItems(query);
                    if (oItems.Count > 0)
                    {
                        return Convert.ToString(oItems[0]["sitepath"]);
                    }
                }
                return String.Empty;
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("GetMMDataFromConfig", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                return String.Empty;
            }
        }

        private void DeleteExistingWebTitlefromMMS(TermSet oTermSet, string webTitle)
        {
            try
            {
                var cTerm = oTermSet.Terms.Where(s => s.Name == webTitle).ToList();
                if (cTerm != null & cTerm.Count == 1)
                {
                    Term oTerm = oTermSet.Terms[webTitle];
                    oTerm.Delete();
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("DeleteExistingWebTitlefromMMS", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }
        }

        private void CreateSearchNavigationSettings(SPSite site, SPWeb web)
        {
            bool oldUnsafeUpdate = web.AllowUnsafeUpdates;
            web.AllowUnsafeUpdates = true;
            try
            {
                if (web.Navigation.SearchNav != null && web.Navigation.SearchNav.Count > 0)
                {
                    RemoveExistingSearchNavigationNodes(web);
                }
                List<ConfigNavigationNode> tempNodeCollection = GetNodesFromConfig(site.ServerRelativeUrl);

                //Add New Nodes to the search navigation
                foreach (ConfigNavigationNode tempNode in tempNodeCollection)
                {

                    SPNavigationNode node = new SPNavigationNode(tempNode.Title, tempNode.URL, tempNode.isExternal);
                    web.Navigation.AddToSearchNav(node);
                }

            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Region site Create Search Navigation Settings", TraceSeverity.Verbose, EventSeverity.Verbose), TraceSeverity.Verbose, ex.Message);
            }

            web.AllowUnsafeUpdates = oldUnsafeUpdate;
            web.Update();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootWeb"></param>
        /// <param name="spWeb"></param>
        private void CreateGlobalNavigationSettings(SPWeb rootWeb, SPWeb spWeb)
        {
            try
            {
                bool oldUnsafeUpdate = rootWeb.AllowUnsafeUpdates;
                rootWeb.AllowUnsafeUpdates = true;
                string nodeTitle = spWeb.Title;
                string nodeURL = spWeb.Url;
                SPNavigationNodeCollection nodes = rootWeb.Navigation.TopNavigationBar;
                SPNavigationNode navNode = new SPNavigationNode(nodeTitle, nodeURL, true);

                foreach (SPNavigationNode node in nodes)
                {
                    if (node.Title.ToLower() == NavigationHeader.ToLower())
                    {

                        if (!CheckIfNewNodeExists(node.Children, nodeTitle))
                        {
                            node.Children.AddAsLast(navNode);
                        }
                        break;
                    }
                }

                rootWeb.AllowUnsafeUpdates = oldUnsafeUpdate;
                rootWeb.Update();
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Region site Create Global Navigation Settings", TraceSeverity.Verbose, EventSeverity.Verbose), TraceSeverity.Verbose, ex.Message);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodes"></param>
        /// <param name="navNode"></param>
        /// <returns></returns>
        private bool CheckIfNewNodeExists(SPNavigationNodeCollection nodes, string nodeTitle)
        {
            foreach (SPNavigationNode node in nodes)
            {
                if (node.Title.ToLower() == nodeTitle.ToLower())
                {
                    return true;
                }
            }
            return false;
        }
        // Uncomment the method below to handle the event raised before a feature is deactivated.

        public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        {
            try
            {
                Guid siteId = ((SPWeb)properties.Feature.Parent).Site.ID;
                Guid webId = ((SPWeb)properties.Feature.Parent).ID;
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite site = new SPSite(siteId))
                    {
                        using (SPWeb web = site.OpenWeb(webId))
                        {
                            bool oldUnsafeUpdate = web.AllowUnsafeUpdates;
                            web.AllowUnsafeUpdates = true;
                            if (web.Navigation.SearchNav != null)
                            {
                                RemoveExistingSearchNavigationNodes(web);
                            }
                            web.AllowUnsafeUpdates = oldUnsafeUpdate;
                            web.Update();
                            RemoveExistingGlobalhNavigationNodes(site.RootWeb, web);
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Region site ConfigureNavigation Feature Deactivating", TraceSeverity.Verbose, EventSeverity.Verbose), TraceSeverity.Verbose, ex.Message);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteUrl"></param>
        /// <returns></returns>
        private List<ConfigNavigationNode> GetNodesFromConfig(string siteUrl)
        {

            List<ConfigNavigationNode> ConfigNodeCollection = new List<ConfigNavigationNode>();
            siteUrl = siteUrl.TrimEnd("/".ToCharArray());
            foreach (KeyValuePair<string, string> kvp in SearchNavigationNodeList)
            {
                ConfigNodeCollection.Add(new ConfigNavigationNode(kvp.Key, siteUrl + kvp.Value, true));
            }
            return ConfigNodeCollection;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="web"></param>
        private void RemoveExistingSearchNavigationNodes(SPWeb web)
        {
            for (int i = 0; i < web.Navigation.SearchNav.Count; i++)
            {
                web.Navigation.SearchNav[0].Delete();
            }
            web.Update();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="web"></param>
        private void RemoveExistingGlobalhNavigationNodes(SPWeb rootWeb, SPWeb spWeb)
        {
            bool oldUnsafeUpdate = rootWeb.AllowUnsafeUpdates;
            rootWeb.AllowUnsafeUpdates = true;
            string nodeTitle = spWeb.Title;
            string nodeURL = spWeb.Url;
            SPNavigationNodeCollection nodes = rootWeb.Navigation.TopNavigationBar;
            foreach (SPNavigationNode node in nodes)
            {
                if (node.Title.ToLower() == NavigationHeader.ToLower())
                {
                    for (int childIndex = 0; childIndex < node.Children.Count; childIndex++)
                    {
                        SPNavigationNode childNode = node.Children[childIndex];
                        if (childNode.Title.ToLower() == nodeTitle.ToLower())
                        {
                            childNode.Delete();
                            break;
                        }
                    }
                    break;
                }
            }

            rootWeb.AllowUnsafeUpdates = oldUnsafeUpdate;
            rootWeb.Update();
        }
    }

    public class ConfigNavigationNode
    {
        public string Title { get; set; }
        public string URL { get; set; }
        public bool isExternal { get; set; }
        public ConfigNavigationNode(string title, string url, bool isExternalNode)
        {
            this.Title = title;
            this.URL = url;
            this.isExternal = isExternalNode;
        }
    }
}

