﻿var quickLinks={
clientContext:null,
web:null,
oListLinks:null,
currentUser:null,
LoginName:null,
/* Pulls the Quick Links from Enterprise list if the custom user profile property is not null*/
getArticle: function(){	
	quickLinks.createInput();
	quickLinks.clientContext = new SP.ClientContext.get_current();
	quickLinks.web = clientContext.get_web();
	quickLinks.currentUser = quickLinks.web.get_currentUser();
	quickLinks.LoginName = quickLinks.currentUser.get_loginName();
	quickLinks.LoginName = quickLinks.LoginName.split('|')[1];
 	quickLinks.oListLinks= quickLinks.clientContext.get_site().get_rootWeb().get_lists().getByTitle('QuickLinks'); 
	quickLinks.clientContext.load(quickLinks.oListLinks);
	quickLinks.clientContext.load(quickLinks.currentUser);
 	var camlQueryLinks= new SP.CamlQuery();
	collListItemLinks = quickLinks.oListLinks.getItems(camlQueryLinks);
	quickLinks.clientContext.load(collListItemLinks);
	quickLinks.clientContext.executeQueryAsync(quickLinks.successgetArticle , quickLinks.failure);	
},

successgetArticle: function(){
		
	var listItemEnumeratorLinks = collListItemLinks.getEnumerator();	
	var linkID="";
	var linkUrl="";
	var linkName="";
	var propertyValue="";
	while (listItemEnumeratorLinks.moveNext()) 
	{		
		oListItemLinks = listItemEnumeratorLinks.get_current();
		linkID = oListItemLinks.get_item('ID');
		linkName = oListItemLinks.get_item('Title');
		linkUrl = oListItemLinks.get_item('BackgroundImageLocation').get_url();
		var qucikString = '<a href="#"><img src="'+linkUrl+'"><span>'+linkName+'</span></a><input id= "check'+linkID+'" value= "'+linkID+'"type="checkbox" class="chk" style="display:none" /><img src="/SiteCollectionImages/linesepimage.png">';
		$('#QuickLinksContainer').append(qucikString); 				
		propertyValue += linkID +'@#@';			
	}
	propertyValue = propertyValue.substring(0, propertyValue.length -3);
	var propertyName='IntranetQuickLinks';
	quickLinks.updateUserProfile(quickLinks.LoginName,propertyName, propertyValue);
	
},
/*Function to update user profile custom property*/

updateUserProfile: function(LoginName, propertyName, propertyValue){
	var propertyData = "<PropertyData>" +
	"<IsPrivacyChanged>false</IsPrivacyChanged>" +
	"<IsValueChanged>true</IsValueChanged>" +
	"<Name>" + propertyName+ "</Name>" +
	"<Privacy>NotSet</Privacy>" +
	"<Values><ValueData><Value xsi:type=\"xsd:string\">" + propertyValue+ "</Value></ValueData></Values>" +
	"</PropertyData>";	
	$().SPServices({
		    operation: "ModifyUserPropertyByAccountName",
		    async: false,
		    webURL:_spPageContextInfo.siteAbsoluteUrl,
		    accountName: LoginName,
		    newData: propertyData,
		    completefunc: function (xData, Status) 
		    {	
		    	var result = $(xData.responseXML);    	 	
					    	    	
		    }
	  });

},
/* Get quick links will fetch the Quick links if the custom user profile property is not null*/

getQuickLinks: function(){
quickLinks.createInput();
	quickLinks.clientContext = new SP.ClientContext.get_current();
	quickLinks.web = clientContext.get_web();
	quickLinks.currentUser = quickLinks.web.get_currentUser();
	quickLinks.LoginName = quickLinks.currentUser.get_loginName();
	quickLinks.LoginName = quickLinks.LoginName.split('|')[1];
	var temp = intranetQuickLinks.replace(/\|/g, '');
	var selectedLinks = temp.split('@#@');
	
	var imgUrl = "";
        $.ajax({
            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('QuickLinks')/items?$filter=Title eq 'DefaultQuickLinksImagePlaceholder'",
            type: "GET",
            headers: {
                "Accept": "application/json;odata=verbose"
            },
            async: false,
            success: function(data, textStatus, xhr) {
                imgUrl = data.d.results[0].BackgroundImageLocation.Url;
            },
            error: function(xhr, textStatus, errorThrown) {
                // alert("error 1:"+JSON.stringify(xhr));
            }
        });

	
	for(var i=0;i<selectedLinks.length;i++)
	{
	 if (!isNaN(selectedLinks[i])) {
		$.ajax({
	   url: _spPageContextInfo.siteAbsoluteUrl+"/_api/web/lists/getbytitle('QuickLinks')/getitembyid("+selectedLinks[i]+")",
	   type: "GET",
	   headers: { "Accept": "application/json;odata=verbose" },
	   async:false,
	   success: function(data, textStatus, xhr) {	  
	   var linkName=data.d.Title
		var linkID= data.d.ID;
		var linkUrl= data.d.BackgroundImageLocation!=null?data.d.BackgroundImageLocation.Url:"";
		var linkLocation= data.d.LinkLocation!=null?data.d.LinkLocation.Url:"";
		//var externalLink = data.d.ExternalLink.Url;
		var qucikString = '<li style="float: left;margin-right: 10px;"><a href="#"><img src="'+linkUrl+'" style="width:40px;height:40px;display:block"><span>'+linkName+'</span></a></li>';
		$('#QuickLinksContainer ul').append(qucikString);	    
	    },
	   error: function(xhr, textStatus, errorThrown) {
	 //  alert("error 1:"+JSON.stringify(xhr));
	   }
	});
	}
	else{
	var LinkImgUrl = "";
                var title = selectedLinks[i].split(',');
                //console.log(title[0]);
                var nestedSite = title[1].split(_spPageContextInfo.siteAbsoluteUrl);
                var sites = nestedSite[1].split('/');
                for (var k = 0; k < sites.length; k++) {
                    if (sites[k] != "") {
                        $.ajax({
                            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('QuickLinks')/items?$filter=Title eq '" + sites[k] + "'",
                            type: "GET",
                            headers: {
                                "Accept": "application/json;odata=verbose"
                            },
                            async: false,
                            success: function(data, textStatus, xhr) {

                                if (data.d.results.length > 0)
                                    LinkImgUrl = data.d.results[0].BackgroundImageLocation!=null?data.d.results[0].BackgroundImageLocation.Url:"";
                            },
                            error: function(xhr, textStatus, errorThrown) {
                                // alert("error 1:"+JSON.stringify(xhr));
                            }
                        });

                    }
                }
                if (LinkImgUrl == "")
                    LinkImgUrl = imgUrl;
                var linkName = title[0];
                        var qucikString = '<li style="float: left;margin-right: 10px;"><a href="#"><img src="'+LinkImgUrl+'" style="width:40px;height:40px;display:block"><span>'+linkName+'</span></a></li>';
                        $('#QuickLinksContainer ul').append(qucikString);
	
	/*var title = selectedLinks[i].split(',');
                $.ajax({
                    url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('QuickLinks')/items?$filter=Title eq '" + title[0] + "'",
                    type: "GET",
                    headers: {
                        "Accept": "application/json;odata=verbose"
                    },
                    async: false,
                    success: function(data, textStatus, xhr) {
                        var linkName = title[0];
                        if (data.d.results.length > 0)
                           imgUrl= data.d.results[0].BackgroundImageLocation.Url;
                        var qucikString = '<li style="float: left;margin-right: 10px;"><a href="#"><img src="'+imgUrl +'" style="width:40px;height:40px;display:block"><span>'+linkName+'</span></a></li>';
                        $('#QuickLinksContainer ul').append(qucikString);

                    },
                    error: function(xhr, textStatus, errorThrown) {
                        // alert("error 1:"+JSON.stringify(xhr));
                    }
                });*/

	}
	}	
},

createInput:function(){
    var $input = $('<input type="button" value="Edit Links" onclick="quickLinks.editLinks();" />');
    $input.appendTo($("#QuickLinksContainer"));
},
editLinks: function(){
location.href='EditQuickLinks.aspx';	
	
},
failure: function(sender,args){
	//console.log(args.get_message() + '\n' + args.get_stackTrace());
}

}
$(document).ready(function()
{
	$('#RecommendedlinksSection').hide();
	
});
function hideshowQuickLinksPanel()
{
$("#QuickLinksPlaceholder").toggle("slow");
var recomendedLinksLength=$('.RecomendedLinks-homepage ul').find('li').length;
	var quickLinksLength=$('#QuickLinksContainer ul').find('li').length;
	var loopStart=12-quickLinksLength;
	for(var i=recomendedLinksLength;i>loopStart-1;i-- ){
		$('.RecomendedLinks-homepage ul li').eq(i).remove();
	}
}
