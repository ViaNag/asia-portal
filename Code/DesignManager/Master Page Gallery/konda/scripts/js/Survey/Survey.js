﻿        var _votingoptions = new Array();
        var _hasAlreadyVoted = false;
        $(window).load(function() {

            SP.SOD.executeFunc('sp.js', 'SP.ClientContext', CheckifCurrentUserhasVoted);
            //ExecuteOrDelayUntilScriptLoaded(CheckifCurrentUserhasVoted, "sp.js");
        });

        /************** Checks if the current user has already casted his/her vote**************
         ************** if they have then we show the view results panel and **************
         ************** if not the we populate the panel with options  *******************/

        function CheckifCurrentUserhasVoted() {
                
           // articleTagging.pageName = getPageName();


          //  articleTagging.getPageIdValue();
          //  alert(_spPageContextInfo.pageItemId);
            //alert(Number($(".Id").text()));


                    var _userId = _spPageContextInfo.userId;
                    var _url =_spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('Poll')/items?$select=Title,TaggedPage/ID,Id,Author/ID&$expand=TaggedPage/ID,Author/ID&$filter=(Author/ID eq " + _userId+") and (TaggedPage/ID eq '" + _spPageContextInfo.pageItemId + "')";

                    $.ajax({
                        url:_url ,
                        method: "GET",
                        async: false,
                        headers: {
                            "Accept": "application/json; odata=verbose"
                        },
                        success: function (data) {
                            var _countofItemsbyMe = data.d.results.length;
                          
                            if (_countofItemsbyMe > 0)
                                _hasAlreadyVoted = true;
                        },
                        error: function (data) {
                            /*ADFS Alert Issue*/console.log("Error at line 26" + data);
                        }
                    });

                GetVotingOptionsandPoulateinaGlobalArray();
            }
            /*************** This function queries the "Answers" column in "Poll" list and ****************
             **************** populates "_votingoptions" array with choice values in the column ****************/

        function GetVotingOptionsandPoulateinaGlobalArray() {


            clientContext = new SP.ClientContext.get_current();
            web = clientContext.get_web();
            currentUser = web.get_currentUser();
            var oList = clientContext.get_web().get_lists().getByTitle('Pages');
            var camlQuery = new SP.CamlQuery();
            camlQuery.set_viewXml('<View><Query><Where><Eq><FieldRef Name=\'ID\'/><Value Type=\'Text\'>' + _spPageContextInfo.pageItemId + '</Value></Eq></Where></Query></View>');
            collListItem = oList.getItems(camlQuery);
            clientContext.load(collListItem);
            clientContext.load(currentUser);

            clientContext.executeQueryAsync(Function.createDelegate(this, this.PopulateGlobalArraySuccess), Function.createDelegate(this, this.Failure));
        }

        function PopulateGlobalArraySuccess() {

            //_votingoptions = this.AnswersChoicesfield.get_choices().toString().split(',');
            var listItemInfo = '';
            var listItemEnumerator = collListItem.getEnumerator();
            listItemEnumerator.moveNext()
                //while (listItemEnumerator.moveNext())
                //{
            var oListItem = listItemEnumerator.get_current();
            _votingoptions = oListItem.get_item('ArticleSurveyAnswers').toString().split(';');;
            //}

            if (_hasAlreadyVoted) {
            	HideVotingOptions();
                ShowEditVotingOptions();
                ViewVotingResults();                
            } else {
                ShowEditVotingOptions();
                ShowVotingOptions();
            }
        }

        function ShowVotingOptions() {
            for (var i = 0; i < _votingoptions.length; i++) {


                $("#pnlVote").append('<label class="votingOptions ms-emphasis" for="' + _votingoptions[i] + '"><input type="radio" class="radioVotingOptions" name="optionvalues" id="' + _votingoptions[i] + '" value="' + _votingoptions[i] + '" onchange="javascript:{$(\'#btnVote\').removeAttr(\'disabled\')}" /><span id="voteOptions'+i+'" class="spnVoteOptions">' + _votingoptions[i] + '</span></label>');

            }

        }
        
        function HideVotingOptions() {
            for (var i = 0; i < _votingoptions.length; i++) {
                $("#pnlVote").append('<label style="display:none" class="votingOptions ms-emphasis" for="' + _votingoptions[i] + '"><input type="radio" class="radioVotingOptions" style="display:none" name="optionvalues" id="' + _votingoptions[i] + '" value="' + _votingoptions[i] + '" onchange="javascript:{$(\'#btnVote\').removeAttr(\'disabled\')}" /><span style="display:none" id="voteOptions'+i+'" class="spnVoteOptions">' + _votingoptions[i] + '</span></label>');

            }

        }


        function ShowEditVotingOptions() {
            for (var i = 0; i < _votingoptions.length; i++) {

                $("#ChoiceOptions").append("<li><input type='button' value='Remove' onclick='javascript:{$(this).parent().remove();UpdateChoiceColumn()}'/> &nbsp;&nbsp;" + _votingoptions[i] + " </li>");


            }

        }

        function CastYourVote() {

            var votedValue = $('input[name=optionvalues]:checked').val();

            var clientContext = SP.ClientContext.get_current();
            var oList = clientContext.get_web().get_lists().getByTitle('Poll');
            var itemCreateInfo = new SP.ListItemCreationInformation();
            this.oListItem = oList.addItem(itemCreateInfo);
            oListItem.set_item('ArticleSurveyAnswers', votedValue);
            var lookupValue = new SP.FieldLookupValue();
            lookupValue.set_lookupId(_spPageContextInfo.pageItemId);
            oListItem.set_item('TaggedPage', lookupValue);
            oListItem.update();
            clientContext.load(oListItem);
            clientContext.executeQueryAsync(Function.createDelegate(this, this.onVotingSucceeded), Function.createDelegate(this, this.Failure));
        }

        function onVotingSucceeded() {


            ViewVotingResults();
            //console.log('Item created: ' + oListItem.get_id());
        }



        function ViewVotingResults() {
            $("#Vote").hide();
            var _barGraphOptions = {

                scaleOverlay: false,
                scaleOverride: true,
                scaleStepWidth: 10,
                scaleSteps: 10
            }

            var _votingPercentages = new Array();
            $.ajax({
                url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('Poll')/items?$select=Title,ArticleSurveyAnswers,TaggedPage/ID" +
                    "&$expand=TaggedPage/ID&$filter=TaggedPage/ID eq '" + _spPageContextInfo.pageItemId + "'",
                method: "GET",
                async: false,
                headers: {
                    "Accept": "application/json; odata=verbose"
                },
                success: function(data) {

                    var total = data.d.results.length;
                    for (var i = 0; i < _votingoptions.length; i++) {
                        var results = data.d.results.filter(function(elem) {
                            return elem.ArticleSurveyAnswers == _votingoptions[i];
                        });

                        var cent = (results.length / total) * 100;
                        _votingPercentages.push(cent);

                    }
                    var _barGraphData = {
                        labels: _votingoptions,
                        datasets: [{
                            fillColor: "teal",
                            strokeColor: "teal",
                            data: _votingPercentages
                        }]
                    }
                    $("#Results").show();
                    var ctx = document.getElementById("Results").getContext("2d");
                    new Chart(ctx).Bar(_barGraphData, _barGraphOptions);

                },
                error: function(data) {
                    //console.log(data);
                }
            });




        }

        function AddOptions() {
            //console.log("hi");
            if ($("#Options").val() !== '') {

                $("#ChoiceOptions").append("<li> <input type='button' value='Remove' onclick='javascript:{$(this).parent().remove();UpdateChoiceColumn()}'/>" + $("#Options").val() + "</li>");
                $("#pnlVote").append('<label class="votingOptions ms-emphasis" for="' + $("#Options").val() + '"><input type="radio" class="radioVotingOptions" name="optionvalues" id="' + $("#Options").val() + '" value="' + $("#Options").val() + '" onchange="javascript:{$(\'#btnVote\').removeAttr(\'disabled\')}" /><span class="spnVoteOptions">' + $("#Options").val() + '</span></label>');
                UpdateChoiceColumn();
            }

        }

        function UpdateChoiceColumn() {
            this.ctx = new SP.ClientContext.get_current();
            this.list = ctx.get_web().get_lists().getByTitle("Poll");
            this.fields = list.get_fields();
            this.myChoicesfield = ctx.castTo(this.fields.getByInternalNameOrTitle("ArticleSurveyAnswers"), SP.FieldChoice);
            ctx.load(fields);
            ctx.load(this.myChoicesfield);
            ctx.executeQueryAsync(Function.createDelegate(this, this.UpdateChoiceColumnSuccess), Function.createDelegate(this, this.Failure));

        }

        function UpdateChoiceColumnSuccess() {

            var choices = new Array();
            $("#ChoiceOptions").find("li").each(function() {
                choices.push($(this).text());
            });

            this.myChoicesfield.set_choices(choices);
            this.myChoicesfield.update();



            this.ctx.executeQueryAsync(onChoiceFieldUpdateSuccess, Failure);

        }

        function onChoiceFieldUpdateSuccess() {
            //console.log('Field created');
        }

        function Failure(sender, args) {
            //console.log("Failed" + args.get_message());
        }