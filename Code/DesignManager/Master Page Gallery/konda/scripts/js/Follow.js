﻿var follow={
	clientContext :null,
	followingManager :null,
	actorInfo :null,
	isFollowed :null,
	followedCount :null,
	FollowPerson:function(targetUser){
		follow.clientContext = SP.ClientContext.get_current();
		follow.followingManager = new SP.Social.SocialFollowingManager(follow.clientContext);
		follow.actorInfo = new SP.Social.SocialActorInfo();
        follow.actorInfo.set_accountName(targetUser);
        follow.isFollowed = follow.followingManager.isFollowed(follow.actorInfo);        
		follow.clientContext.executeQueryAsync(follow.sucessPersonFollowed, follow.Failure);
	},
	IsFollowed:function(targetUser){
		follow.clientContext = SP.ClientContext.get_current();
		follow.followingManager = new SP.Social.SocialFollowingManager(follow.clientContext);
		follow.actorInfo = new SP.Social.SocialActorInfo();
        follow.actorInfo.set_accountName(targetUser);
        follow.isFollowed = follow.followingManager.isFollowed(follow.actorInfo); 
        follow.clientContext.executeQueryAsync(follow.sucessIsFollowed, follow.Failure);
        var f=follow.isFollowed.get_value();
		if(f===true){
			return true;
		}
		else{
			return false;
		}
	},
	sucessPersonFollowed:function(){
		if (follow.isFollowed.get_value() === false) {
	        follow.followingManager.follow(follow.actorInfo);
	    }
	    else if (follow.isFollowed.get_value() === true) {
	        follow.followingManager.stopFollowing(follow.actorInfo);
	    }
		follow.clientContext.executeQueryAsync(follow.successToggle, follow.Failure);		
	},
	successToggle:function(){
		//console.log('DONE');
	},
	Failure:function(sender,args){
 		//console.log(args.get_message() + '\n' + args.get_stackTrace());
 	}
}


$(document).ready(function() {
	var targetUser=$('.follow').attr('data-username');
	var val =follow.IsFollowed(targetUser);
//console.log(val);
	$('.follow').click(function(){
		var targetUser=$(this).attr('data-username');
		SP.SOD.executeOrDelayUntilScriptLoaded(follow.FollowPerson(targetUser), 'SP.UserProfiles.js');
    });
});
