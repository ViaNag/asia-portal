var unix_timestamp_last;
var day_of_the_week=["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
var timer_counter=0;

function startclock(){unix_timestamp=parseInt($("#unixUTC").val());
unix_timestamp_last=unix_timestamp;
if(timer_counter==0){setInterval("uptick_timestamp()",1000)}timer_counter++}

function uptick_timestamp(){unix_timestamp_last=unix_timestamp_last+1;
var a=0;
for(a=-12;a<=12;a++){tickclocks(a)}}

function time_string(f,a,i){var d;var k=[0,0];if(typeof f=="string"){var k=f.split(":")}else{k[0]=parseInt(f);k[1]=0}var r=parseInt(k[0])*3600;var s=r&&r/Math.abs(r);if(k[1]!="00"){r+=parseInt(k[1])*60*s}var g=f.toString();var o=unix_timestamp_last+r;var e=o*1000;var q=new Date(e);var l=q.getUTCDay();var j=day_of_the_week[l];var p=q.getUTCHours();var m=q.getUTCMinutes();var h=q.getUTCSeconds();var b=p;if(p>=24){p-=24}if(p<1){p=12}if(h<10){h="0"+h}if(m<10){m="0"+m}if(a==true){var c=(b>=12)?"pm":"am";if(p>=13){p-=12}h=h+" "+c}if(i==true){d=j+" "+p+":"+m+":"+h}else{d=p+":"+m+":"+h}return d}


function tickclocks(e){var l=true;var k=true;var b=e+":30";var i=e+":45";var f=time_string(e,l,k);var g=time_string(e,l,false);var h=time_string(b,l,false);var m=time_string(i,l,false);var c="UTC";var a=e.toString();var j=b.toString();var d=i.toString();j=j.replace(":","_");d=d.replace(":","_");if(e>=0){UTC_string_30=c.concat("+"+j);UTC_string_45=c.concat("+"+d);c=c.concat("+"+a)}else{if(e<0){UTC_string_30=c.concat(j);UTC_string_45=c.concat(d);c=c.concat(a)}}UTC_string2=c.concat(":00");c=c.replace("+","");UTC_string_30=UTC_string_30.replace("+","");UTC_string_45=UTC_string_45.replace("+","");$("#"+c).html(f);UTC_string_time=c.concat("-time");$("."+UTC_string_time).html(g);UTC_string_time_30=UTC_string_30.concat("-time");$("."+UTC_string_time_30).html(h);UTC_string_time_45=UTC_string_45.concat("-time");$("."+UTC_string_time_45).html(m)};