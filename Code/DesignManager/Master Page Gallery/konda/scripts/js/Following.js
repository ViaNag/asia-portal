﻿ /********************************************************************************************************************
 ****** Contains ajax calls to REST api of sharepoint.
 ****** Checks if the current content is followed
 ****** Start following and stop following content. 
 ********************************************************************************************************************/

 

  // Constructor class for _followedContent
  // valContentUrl is the url absolute Url of site/document 
  // valActorType is Actor type of followed content "1" indicates sites and "2" indicates documents
  // valSiteUrl is the absolute Url of site collection
  // valDigest is digest value of page grabbed using $("#__REQUESTDIGEST").val()
  
 _followedContent = function (valContentUrl , valActorType , valSiteUrl , valDigest) { 
 	this._contentUrl= valContentUrl;
 	this._actortype = valActorType;
 	this._followingManagerEndpoint = valSiteUrl + "/_api/social.following";
 	this._digest = valDigest;
 }
  

// Check whether the current user is already following the site.
// The request body includes a SocialActorInfo object that represents
// the specified item. 
// The success function reads the response from the REST service and then
// returns user's following status as a boolean value.
 

 _followedContent.prototype._funcIsFollowed = function (){ // start of _funcIsFollowed

 var _isFollowed;
 var _orufusUtility = new _rufusUtility();
 $.ajax( {
        url: this._followingManagerEndpoint + "/isfollowed",
        type: "POST",
        async:false,
        data: JSON.stringify( { 
            "actor": {
                "__metadata": {
                    "type":"SP.Social.SocialActorInfo"
                },
                "ActorType":this._actortype ,
                "ContentUri":this._contentUrl,
                "Id":null
            } 
        }),
        headers: { 
            "accept":"application/json;odata=verbose",
            "content-type":"application/json;odata=verbose",
            "X-RequestDigest":this._digest
        },
        success: function (responseData) { 
            stringData = JSON.stringify(responseData);
            jsonObject = JSON.parse(stringData);
            _isFollowed = (jsonObject.d.IsFollowed === true ) ? true : false ; 
                    
        },
        error: _orufusUtility._funcRequestFailed     
    });
 return _isFollowed;
 }//end of _funcIsFollowed
 
 // Make the current user start following a site.
// The request body includes a SocialActorInfo object that represents
// the site to follow.
// The success function reads the response from the REST service.

_followedContent.prototype._funcStartFollowing = function (){ 

 var _orufusUtility = new _rufusUtility();
    $.ajax({
        url: this._followingManagerEndpoint + "/follow",
        type: "POST",
        async:false,
        data: JSON.stringify({
            "actor": {
                "__metadata": {
                    "type": "SP.Social.SocialActorInfo"
                },
                "ActorType": this._actortype,
                "ContentUri": this._contentUrl,
                "Id": null
            }
        }),
        headers: {
            "accept": "application/json;odata=verbose",
            "content-type": "application/json;odata=verbose",
            "X-RequestDigest": this._digest
        },
        success: function (responseData) {
            stringData = JSON.stringify(responseData);
            jsonObject = JSON.parse(stringData);
            var statusMessage = {
                0: 'Successfully added to your quick links!',
                1: 'This is already added to your quick links! ',
                2: 'Maximum quick links reached!',
                3: 'An internal error occurred.'
            }       
//console.log(stringData);
             _orufusUtility._funcAddStatus(statusMessage[jsonObject.d.Follow], "green")
 
           
        },
        error: _orufusUtility._funcRequestFailed
    });
}

// Make the current user stop following a site.
// The request body includes a SocialActorInfo object that represents
// the site to stop following.
_followedContent.prototype._funcStopFollowing = function (){

 var _orufusUtility= new _rufusUtility(); 
    $.ajax({
        url: this._followingManagerEndpoint + "/stopfollowing",
        type: "POST",
        async:false,
        data: JSON.stringify({
            "actor": {
                "__metadata": {
                    "type": "SP.Social.SocialActorInfo"
                },
                "ActorType": this._actortype,
                "ContentUri": this._contentUrl,
                "Id": null
            }
        }),
        headers: {
            "accept": "application/json;odata=verbose",
            "content-type": "application/json;odata=verbose",
            "X-RequestDigest": this._digest
        },
        success: function () {
         _orufusUtility._funcAddStatus("This has been removed from your quick links!", "green")

        },
        error: _orufusUtility._funcRequestFailed
    });
}

