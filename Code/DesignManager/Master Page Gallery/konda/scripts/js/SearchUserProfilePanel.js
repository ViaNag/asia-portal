﻿//SP.SOD.executeFunc('SP.UserProfiles.js', 'SP.UserProfiles', GetProperties);
/*$(document).ready(function(){	
 //$.noConflict() ;
	 ExecuteOrDelayUntilScriptLoaded(GetProperties, "SP.UserProfiles.js");

	 //MyTeams();

	
});*/


/*Modified by Robin Start*/
$(window).load(function(){
	 ExecuteOrDelayUntilScriptLoaded(GetProperties, "SP.UserProfiles.js");
});

/*Modified by Robin End*/

var clientContext=null;
var web;
var collListItem;
var UserProperties = new Array();
var userProfile;
var currentUser=null;
var peopleManager;
var LoginName=null;
var UserPropertiesValue = new Array();
var _getToKnowIntranet;
var _PictureUrl;
var  percentComplete;
var intranetQuickLinks;
function GetProperties()
{
	clientContext = new SP.ClientContext("/");
 	web = clientContext.get_web();
 	currentUser = web.get_currentUser();  
	var oList = clientContext.get_site().get_rootWeb().get_lists().getByTitle('Config');		
    var camlQuery = new SP.CamlQuery();	    
    camlQuery.set_viewXml('<View><Query><Where><Eq><FieldRef Name=\'Title\'/><Value Type=\'Text\'>Test</Value></Eq></Where></Query><ViewFields><FieldRef Name=\'FetchProfileProperties\'/><FieldRef Name=\'Title\'/></ViewFields></View>');
    collListItem = oList.getItems(camlQuery);
    clientContext.load(collListItem);
    clientContext.load(currentUser);	    
    clientContext.executeQueryAsync(Function.createDelegate(this, this.onProfileSucceeded), Function.createDelegate(this, this.onProfileFailed));       
}
function onProfileFailed(sender,args){
	//console.log(args.get_message() + '\n' + args.get_stackTrace());
}

function onProfileSucceeded(sender, args) 
{
	var listItemInfo = '';
    var listItemEnumerator = collListItem.getEnumerator();
    while (listItemEnumerator.moveNext())
	{
		var oListItem = listItemEnumerator.get_current();
		listItemInfo =  oListItem.get_item('FetchProfileProperties');
	}
	userProfile = listItemInfo;
	//getUserPropertiesFromProfile();
	//GetQuestions();
}

function getUserPropertiesFromProfile()
{
	var userProfileInfo = userProfile.split(',');
	clientContext = new SP.ClientContext("/");
	LoginName = clientContext.get_web().get_currentUser().get_loginName();
	LoginName = LoginName.split('|')[1];
	var peopleManager = new SP.UserProfiles.PeopleManager(clientContext);		
	var userProfilePropertiesForUser = new SP.UserProfiles.UserProfilePropertiesForUser(clientContext,LoginName,userProfileInfo);		
	userProfileProperties = peopleManager.getUserProfilePropertiesFor(userProfilePropertiesForUser);
	clientContext.load(userProfilePropertiesForUser);
	clientContext.executeQueryAsync(onSuccess, onProfileRequestFail);
}

function onSuccess()
{	
	var _userProfilePictureURL="";/* Changes for Defect ID 371 */
	var _Name= userProfileProperties[0]+" "+ userProfileProperties[1];
	_PictureUrl =  userProfileProperties[3]; 
	_userProfilePictureURL=_PictureUrl;/* Changes for Defect ID 371 */
    var _Department =  userProfileProperties[8];
    _getToKnowIntranet =  userProfileProperties[9];
    intranetQuickLinks =  userProfileProperties[10];
     var _employeeType =  userProfileProperties[13];
	 var _newHire =  userProfileProperties[12];
    var _topPages =  userProfileProperties[11];    
   	_topPages = _topPages.replace(/\|/g, '');
	_topPages = _topPages.split('@#@');
	$('#number-of-pages').text("All "+(_topPages.length-1)+" projects >> ");
	var count = 0;
	if((_topPages.length-1)>10)
		count = 10;
	else
		count=_topPages.length-1;

   	for (var k=0;k<count;k++)
   	{   		
   		var topPagesTitle = _topPages[k].split(',');
		var divHtml='<li><a href="'+topPagesTitle[1]+'" title="" target="_blank"><div id="greenroom-panel-top-pages-left"><span class="glyphicon glyphicon-list-alt"></span></div> <div id="greenroom-panel-top-pages-right">'+topPagesTitle[0]+'</div></a></li>';
   		$('#greenroom-panel-top-pages-followed').append(divHtml);   	
   		
   	}

   	if(_newHire=="Yes")
   	{
   		$('#greenroom-panel-onboarding-container').show();
   	}
   	if(_employeeType.indexOf("RF")>-1) /* RF indicates staff, if employeeType contains RF/staff then show employee links */
   	{
   		$('.greenroom-panel-link-employee').show();
   	}
   	if(_employeeType.indexOf("RF")==-1) /* if employeeType does not contain RF show contractor links */
   	{
   		$('#greenroom-panel-contractor-container').show();
   		$('.greenroom-panel-link-contractor').show();
   	}
	if(_PictureUrl == null || _PictureUrl == "")
	{
    	_PictureUrl  =	"_layouts/15/images/Person.gif";  
		_userProfilePictureURL="/SiteCollectionImages/icon_welcome.png";	/* Changes for Defect ID 371 */	
	} 
	else 
	{
		_getToKnowIntranet = _getToKnowIntranet+':'+'2';
	}
	if(_Department== null || _Department== "")
	{
		 _Department="N\A";
	}	
	if (intranetQuickLinks == null || intranetQuickLinks == "")
	{
		quickLinks.getArticle();		
	}
	if(intranetQuickLinks != null || intranetQuickLinks != "")
	{
		quickLinks.getQuickLinks();
	}
	/* Changes for Defect ID 371 */
	$('#DeltaSuiteBarRight').prepend("<img id='WelcomeIcon' src='"+_userProfilePictureURL+"' />");	
	$("#greenroom-panel-default-image").append("<img class='circular'src='"+_PictureUrl+"' alt='"+_Name+"'/>");
	$("#greenroom-panel-default-name").append(_Name);
	$("#viewprofile").append("<a href='http://sp2013-dev-4/Person.aspx'><img src='/SiteCollectionImages/icons/greenroom-panel-icon-profile.png' title='My profile'/> My profile</a>");
	$("#greenroom-panel-default-dept").append(_Department);	

}
 
function onProfileRequestFail(sender, args)
{
	//console.log('Unable to Fetch User Profile Values.' + args.get_message() + '\n' + args.get_stackTrace()); 
}

function GetQuestions()
{
	clientContext = new SP.ClientContext.get_current();	
	web = clientContext.get_web();
	//changed to root web
	//var oListMasterItems = clientContext.get_web().get_lists().getByTitle('MasterItems');
	var oListMasterItems = clientContext.get_site().get_rootWeb().get_lists().getByTitle('MasterItems');
	var camlQuery = new SP.CamlQuery();
    collListItem = oListMasterItems.getItems(camlQuery);
    clientContext.load(collListItem);       
    clientContext.executeQueryAsync(Function.createDelegate(this, this.onQuestionSucceeded), Function.createDelegate(this, this.onQuestionFailed));     		
}

function onQuestionSucceeded()
{ 
    var listItemEnumerator = collListItem.getEnumerator();
    while (listItemEnumerator.moveNext())
	{ 
		var oListItem = listItemEnumerator.get_current();
		listItemInfo =  oListItem.get_item('Title');
		var listItemid =  oListItem.get_item('ID');		
		var listItemURL =  oListItem.get_item('URL');
		var formatValues = _getToKnowIntranet +":"+ listItemid;  
		if ( listItemURL == '' || listItemURL == null )
		{
			listItemURL = '#';
		}				
		if ( _getToKnowIntranet.indexOf(listItemid) > -1 )
		{
			$("#questions").append('<li><a href="#" onclick="updateGetToKnowIntranetStatus(\''+listItemURL+'\',0,\'' + listItemid +'\');">'+listItemInfo+'</a><span><img class="check" src="/SiteCollectionImages/icons/greenroom-panel-icon-circle-check.png" alt="check"/></span></li>');
		}
		else
		{	
			$("#questions").append('<li><a href="#" onclick="updateGetToKnowIntranetStatus(\''+listItemURL+ '\',\'' + formatValues +'\',\'' + listItemid +'\');">'+listItemInfo+'</a><span id="Check'+listItemid+'"></span><img class="uncheck" src="/SiteCollectionImages/icons/greenroom-panel-icon-circle-uncheck.png" alt="check"/></li>');
		}
	}	
	PercentComplete();
}

function onQuestionFailed()
{
	//console.log("Unable to fetch Questions. Please try again");
}

function updateUserProfile(LoginName, propertyName, propertyValue)
{	
	var propertyData = "<PropertyData>" +
	"<IsPrivacyChanged>false</IsPrivacyChanged>" +
	"<IsValueChanged>true</IsValueChanged>" +
	"<Name>" + propertyName + "</Name>" +
	"<Privacy>NotSet</Privacy>" +
	"<Values><ValueData><Value xsi:type=\"xsd:string\">" + propertyValue + "</Value></ValueData></Values>" +
	"</PropertyData>";	
	
	/* To update manually multiple user profile values*/	 
	/*var propertyData1 = "<PropertyData>" +
	"<IsPrivacyChanged>false</IsPrivacyChanged>" +
	"<IsValueChanged>true</IsValueChanged>" +
	"<Name>Fax</Name>" +
	"<Privacy>NotSet</Privacy>" +
	"<Values><ValueData><Value xsi:type=\"xsd:string\">00000000</Value></ValueData></Values>" +
	"</PropertyData>";*/
	
	  $().SPServices({
		    operation: "ModifyUserPropertyByAccountName",
		    async: false,
		    webURL:_spPageContextInfo.siteAbsoluteUrl,
		    accountName: LoginName,
		    newData: propertyData,
		    completefunc: function (xData, Status) 
		    {	
		      	var result = $(xData.responseXML);		    	    	
		    }
	  });
}

function PercentComplete()
{	
	if(_PictureUrl  ==	"_layouts/15/images/Person.gif")
	{
		_getToKnowIntranet = _getToKnowIntranet.split(':');
	}
	else
	{
		_getToKnowIntranet = _getToKnowIntranet.split(':2');
	}
	if(_getToKnowIntranet[0]==null||_getToKnowIntranet[0]=="")
	{
		for(var j=0;j<_getToKnowIntranet.length;j++)
		{
			_getToKnowIntranet[j]=_getToKnowIntranet[j+1];
		}
		_getToKnowIntranet.length = (_getToKnowIntranet.length)-1;
	}	
	
	 percentComplete = 20*(_getToKnowIntranet.length);
	 if(percentComplete<=100)
	 {
	 	$("#bar").append("<div id='complete' style='width:"+percentComplete+"%!important;'>"+percentComplete+"%</div>");
	 }	
	 
	 if(percentComplete==100)
	 {
	 	$("#ifProfileCompleteHtml").hide();
	 	$("#bar").hide();
	 }
} 

function updateGetToKnowIntranetStatus(url,IntranetStatus,listItemid)
{	
	
	var customUserField = 'IntranetGetToKnowStatus';	
	if( IntranetStatus == '0' )
	{
		//updateUserProfile(LoginName, customUserField, '3');

	}
	else
	{
		//IntranetStatus = IntranetStatus.split('2');
		//IntranetStatus = IntranetStatus+':2';
		//alert(IntranetStatus);
		updateUserProfile(LoginName, customUserField, IntranetStatus);		
		percentComplete = percentComplete+20;
		$('#Check'+listItemid+'').html('<img class="check" src="/SiteCollectionImages/check.png" alt="check"/>');
		$('#bar').html('<div style=width:'+percentComplete+'%!important;height:100%!important;background-color:white!important;color:cadetblue;font-size:18px>'+percentComplete +'%</div>');	
	}
	
	window.open(url, '_blank');
}
