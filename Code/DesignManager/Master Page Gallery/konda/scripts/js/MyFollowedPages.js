﻿$(document).ready(function() {

    ExecuteOrDelayUntilScriptLoaded(getFollowedSites.fetchUserProfile("formattedname","unknown","onload"), "SP.UserProfiles.js");
    
   
});

var getFollowedSites = {
    clientContext: null,
    web: null,
    LoginName: null,
    followedSiteName: null,
    followedSiteUri: null,
    userProfile: null,
   /* MySites: function() {
        var size;
        $.ajax({
            url: "/_api/social.following/my/followed(types=4)",
            type: "GET",
            headers: {
                "Accept": "application/json;odata=verbose"
            },
            async: false,
            success: function(data, textStatus, xhr) {

                for (i = 0; i < data.d.Followed.results.length; i++) {
                    getFollowedSites.followedSiteName = data.d.Followed.results[i].Name;
                    getFollowedSites.followedSiteUri = data.d.Followed.results[i].Uri;
                    //li = '<li><div>' + data.d.Followed.results[i].Name + '</div><div><a target="_blank" href="' + data.d.Followed.results[i].Uri + '">' + data.d.Followed.results[i].Uri + '</a></div><div ><input type="button" value="Create Shortcut" onclick="getFollowedSites.fetchUserProfile(\'' + getFollowedSites.followedSiteName + '\',\'' + getFollowedSites.followedSiteUri + '\',\'create\');" /> <input type="button" value="Remove Shortcut" onclick="getFollowedSites.fetchUserProfile(\'' + getFollowedSites.followedSiteName + '\',\'' + getFollowedSites.followedSiteUri + '\',\'remove\');" /></div></li>'
                    //$('#greenroom-followed-sites ul').append(li);                    
                }
				getFollowedSites.fetchUserProfile("formattedname","unknown","onload");
            },
            error: function(xhr, textStatus, errorThrown) {
                alert("error 1:" + JSON.stringify(xhr));
            }
        });
         
    },*/
    fetchUserProfile: function(name, url, action) {
		
        //$.noConflict();
        getFollowedSites.clientContext = new SP.ClientContext.get_current();
        getFollowedSites.web = getFollowedSites.clientContext.get_site().get_rootWeb();
        currentUser = getFollowedSites.web.get_currentUser();
        oList = getFollowedSites.web.get_lists().getByTitle('Landing Page Colors (DO NOT USE)');
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml('<View><Query><Where><Eq><FieldRef Name=\'Title\'/><Value Type=\'Text\'>Test</Value></Eq></Where></Query><ViewFields><FieldRef Name=\'FetchProfileProperties\'/><FieldRef Name=\'Title\'/></ViewFields></View>');
        collListItem = oList.getItems(camlQuery);
        getFollowedSites.clientContext.load(collListItem);
        getFollowedSites.clientContext.load(currentUser);
        getFollowedSites.clientContext.executeQueryAsync(function() {
            getFollowedSites.fetchUserProfileSuccess(name, url, action)
        }, getFollowedSites.failure);
    },
    fetchUserProfileSuccess: function(name, url, action) {
        var listItemInfo = '';
        var listItemEnumerator = collListItem.getEnumerator();
        while (listItemEnumerator.moveNext()) {
            var oListItem = listItemEnumerator.get_current();
            listItemInfo = oListItem.get_item('FetchProfileProperties');
        }
        userProfile = listItemInfo;
        getFollowedSites.getUserPropertiesFromProfile(name, url, action);

    },
    getUserPropertiesFromProfile: function(name, url, action) {



        var userProfileInfo = userProfile.split(",");
        LoginName = currentUser.get_loginName();
        LoginName = LoginName.split('|')[1];
        var TopPagesValue = null;
        $.ajax({
            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/SP.UserProfiles.PeopleManager/GetPropertiesFor(accountName=@v)?@v='" + LoginName + "'",
            async: false,
            headers: {
                Accept: "application/json;odata=verbose"
            },
            success: function(data) {
                for (var i = 0; i < data.d.UserProfileProperties.results.length; i++) {
                    if (data.d.UserProfileProperties.results[i].Key == "TopPages")
                        TopPagesValue = data.d.UserProfileProperties.results[i].Value;
                    //getFollowedSites.TopPagesValue=data.d.UserProfileProperties.results[i].Value;  

                }
            },
            error: function(jQxhr, errorCode, errorThrown) {
                //console.log(errorThrown);
            }
        });
        if (action == "create")
            getFollowedSites.onSuccess(name, url, TopPagesValue);
         if(action == "remove")
            getFollowedSites.RemoveUserProfileValue(name, url, TopPagesValue);
        if (action != "remove"&& action!="create")   
			getFollowedSites.onLoad(TopPagesValue);
    },

    failure: function(sender, args) {
        //console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
    },

    onSuccess: function(name, url, TopPagesValue) {
        var formattedValue = name + ',' + url;
        if (TopPagesValue == null || TopPagesValue == "" || TopPagesValue == "undefined") {
            formattedValue = formattedValue + '@#@';
            getFollowedSites.updateUserProfile(LoginName, "TopPages", formattedValue);
        } else {
            if (TopPagesValue.indexOf(formattedValue) == -1) {
                formattedValue = TopPagesValue + formattedValue + '@#@';
                //getFollowedSites.updateUserProfile(LoginName,"TopPages",formattedValue);
                getFollowedSites.updateUserProfileValue(LoginName, "TopPages", formattedValue);

            }
        }
	
    },
    updateUserProfile: function(LoginName, propertyName, formattedValue) {

        var propertyData = "<PropertyData>" +
            "<IsPrivacyChanged>false</IsPrivacyChanged>" +
            "<IsValueChanged>true</IsValueChanged>" +
            "<Name>" + propertyName + "</Name>" +
            "<Privacy>NotSet</Privacy>" +
            "<Values><ValueData><Value xsi:type=\"xsd:string\"></Value></ValueData></Values>" +
            "</PropertyData>";


        $().SPServices({
            operation: "ModifyUserPropertyByAccountName",
            async: false,
            webURL: _spPageContextInfo.siteAbsoluteUrl,
            accountName: LoginName,
            newData: propertyData,
            completefunc: function(xData, Status) {
                var result = $(xData.responseXML);

            }
        });
        getFollowedSites.updateUserProfileValue(LoginName, "TopPages", formattedValue);
    },
    updateUserProfileValue: function(LoginName, propertyName, propertyValue) {
   
        var propertyArray = propertyValue.split('@#@');
        var multiValues = "";
        for (var i = 0; i < propertyArray.length - 1; i++) {
            multiValues += "<ValueData><Value xsi:type=\"xsd:string\">" + propertyArray[i] + "@#@</Value></ValueData>";
        }
		multiValues = multiValues.replace(/\|/g, '');      
        var propertyData = "<PropertyData>" +
            "<IsPrivacyChanged>false</IsPrivacyChanged>" +
            "<IsValueChanged>true</IsValueChanged>" +
            "<Name>" + propertyName + "</Name>" +
            "<Privacy>NotSet</Privacy>" +
            "<Values>" + multiValues + "</Values>" +
            "</PropertyData>";
        $().SPServices({
            operation: "ModifyUserPropertyByAccountName",
            async: false,
            webURL: _spPageContextInfo.siteAbsoluteUrl,
            accountName: LoginName,
            newData: propertyData,
            completefunc: function(xData, Status) {
                var result = $(xData.responseXML);
            }
        });
      
    },
    RemoveUserProfileValue: function(name, url, TopPagesValue) {
        var formattedValue = name + ',' + url + '@#@';
        if (TopPagesValue.indexOf(formattedValue) > -1) {
            TopPagesValue = TopPagesValue.replace(formattedValue, "");
        }
        getFollowedSites.updateUserProfileValue(LoginName, "TopPages", TopPagesValue);
         getFollowedSites.stopFollowSite(url);
    },
    onLoad:function(TopPagesValue)
    {
    	
    	var size;
    	var formattedName;
        $.ajax({
            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/social.following/my/followed(types=4)",
            type: "GET",
            headers: {
                "Accept": "application/json;odata=verbose"
            },
            async: false,
            success: function(data, textStatus, xhr) {

                for (i = 0; i < data.d.Followed.results.length; i++) {
                    var followedSiteName = data.d.Followed.results[i].Name;
                    var followedSiteUri = data.d.Followed.results[i].Uri;
                    formattedName = followedSiteName +","+followedSiteUri+"@#@";
						if(  TopPagesValue.indexOf(formattedName)>-1)
						{
							
							li = '<li><div>' + data.d.Followed.results[i].Name + '</div><div><a target="_blank" href="' + data.d.Followed.results[i].Uri + '">' + data.d.Followed.results[i].Uri + '</a></div> <input type="button" value="Remove Shortcut" onclick="getFollowedSites.fetchUserProfile(\'' + followedSiteName + '\',\'' + followedSiteUri  + '\',\'remove\');" /></div></li>'
							$('#greenroom-followed-sites ul').append(li);                    
						}  
						else
						{
							li = '<li><div>' + data.d.Followed.results[i].Name + '</div><div><a target="_blank" href="' + data.d.Followed.results[i].Uri + '">' + data.d.Followed.results[i].Uri + '</a></div><div><input class="create" type="button" id ="CreateShortcut" value="Create Shortcut" onclick="getFollowedSites.fetchUserProfile(\'' + followedSiteName  + '\',\'' + followedSiteUri  + '\',\'create\');" /></div></li>'
							$('#greenroom-followed-sites ul').append(li);                    
						}                
                    }
            } 
		});
		
		$('.create').click(function(){
			$(this).parent('div').append('<img src="/SiteCollectionImages/icons/greenroom-panel-icon-circle-check.png"/>');
		});
    },
    stopFollowSite: function(url) { 
        $.ajax( {
            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/social.following/stopfollowing(ActorType=0,AccountName=@v,Id=null)?@v='mtvn\sequeirr'",
        type: "POST",
        data: JSON.stringify( { 
            "actor": {
                "__metadata": {
                    "type":"SP.Social.SocialActorInfo"
                },
                "ActorType":2,
                "ContentUri": url,
                "Id":null
            } 
        } ),
        headers: { 
            "accept":"application/json;odata=verbose",
            "content-type":"application/json;odata=verbose",
            "X-RequestDigest":$("#__REQUESTDIGEST").val()
        },
        success: function (){ 
            alert('The user has stopped following the site.');        

        }
    

    } );
}
}