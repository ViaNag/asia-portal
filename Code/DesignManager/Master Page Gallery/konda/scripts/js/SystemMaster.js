﻿$(document).ready(function() {

    /* display viacom logo in nav bar start */
    $('.ms-core-brandingText').html('<div id="greenroom-logo-container"><a href="/" alt="Green Room"><img id="greenroom-logo" src="/SiteCollectionImages/greenroom_logo.png" /></a></div>');
   /* $('#NavBarContainer').prepend('<div id="greenroom-logo-container-scroll"><a alt="Green Room" href="/"><img src="/SiteCollectionImages/greenroom_logo.png" id="greenroom-logo"></a></div>'); */
    /* display viacom logo in nav bar end */
	
	/* removes suitebar links: OneDrive/SkyDrive, Sites start */
	$("ul.ms-core-suiteLinkList li:contains('Drive')").remove(); //removes li if it contains Drive
	$("div.GreenRoomSites ul.ms-core-suiteLinkList li:contains('Sites')").remove(); //removes li if it contains Sites
	$("ul.ms-core-suiteLinkList").show(); // display remaining links in suitebar
	/* remove suitebar links: OneDrive/SkyDrive, Sites end */

	  $("#globalNavBox").insertBefore("#BreadCrumbNavigation"); //to move editing/share/follow controls below top nav
    $('#ribbonBox').addClass('container'); //adds container class to center share/follow buttons
	   $("#TopBar").insertBefore($("#s4-ribbonrow"));
  
  
  
  /*For Top nav scroll start*/
    jQuery('#s4-workspace').bind('scroll', function() {

        if ($('#s4-workspace').scrollTop() >= 50) {
            //showing the static menu
            $('#TopBar').addClass('fixed');
            $('#TopBar').css({
                top: 0
            });
            $('#s4-workspace').removeClass('s4-workspace-scroll');
            $('#greenroom-logo-container-scroll').css("display", "block");
            $('#ms-designer-ribbon').removeClass('mainRibbonHide');
            $('#ms-designer-ribbon').addClass('mainRibbonDisplay');
            $("#greenroom-logo-container").hide();
            $('#QuickLinks').show();
            $("#QuickLinksPlaceholder").hide();
        }
        if ($('#s4-workspace').scrollTop() < 50) {
            $('#TopBar').removeClass('fixed');
            $('#s4-workspace').addClass('s4-workspace-scroll');
            $('#greenroom-logo-container-scroll').css("display", "none");
            $('#ms-designer-ribbon').addClass('mainRibbonHide');
            $('#ms-designer-ribbon').removeClass('mainRibbonDisplay');
            $('#ms-designer-ribbon').css('visibility', 'visible');
            $("#greenroom-logo-container").show();
            $('#QuickLinks').show();
            $('#s4-workspace').css('margin-top', '25px')
        }


        /*----------------------------------------------------------------------------------------------------------------*/
        /*----------------------- SMOOTH SCROLL ------------------------*/
        /*
        		$('#s4-workspace a[href*=#]').click(
        			function (e) {
        		
        			    // Disable default click event and scrolling
        			    e.preventDefault();
        			    var hash = $(this).attr('href');
        			    hash = hash.slice(hash.indexOf('#') + 1);
        			    
        			    // Scroll to
        			    $("#s4-workspace").scrollTo(hash == 'top' ? 0 : 'a[name='+hash+']', hash == 'top' ? 0 : 'a[name='+hash+']');
        			    
        		    	window.location.hash = '#' + hash;
        			}
        		);
        */
        /*----------------------------------------------------------------------------------------------------------------*/


    });
    /*For Top nav scroll end*/


});

