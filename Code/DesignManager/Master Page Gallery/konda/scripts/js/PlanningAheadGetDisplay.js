var PlanningAheadGetDisplay={
	createHTML : function(subsiteColor)
	{
	var itemCount=PlanningAheadGetData.allCalItems.length;
	var CompleteHtml='';
		if(PlanningAheadGetData.itemLimit>itemCount)
		{
		PlanningAheadGetData.itemLimit=itemCount;
		}
		if(subsiteColor==="")
		{
		subsiteColor="#19a5d4";
		}
		for(var itemIndex=0;itemIndex<PlanningAheadGetData.itemLimit ;itemIndex++ )
		{
			var ArticleType = PlanningAheadGetData.allCalItems[itemIndex].ContentTypeId.substring(0,40);
			if(ArticleType.length>=40)
			{
				if(itemIndex>0)
				{
				CompleteHtml=CompleteHtml+PlanningAheadGetDisplay.createHoriZontalHTML();
				}
				if(ArticleType==="0x0102007ED29E69F64E47DF827B5239039962BE")//GREEN ROOM EVENT
				{
				CompleteHtml=CompleteHtml+PlanningAheadGetDisplay.createEventHTML(itemIndex,subsiteColor);
				}
				if(ArticleType==="0x010200583E76F5E872426E858939A5AC096179")//GREEN ROOM REMINDER
				{
				CompleteHtml=CompleteHtml+PlanningAheadGetDisplay.createReminderHTML(itemIndex);
				}
			}
		}
		return CompleteHtml;
	},

	createEventHTML : function(itemIndex,subsiteColor)
	{
	var linkPage="/"
	var pictureContainerId=PlanningAheadGetData.allCalItems[itemIndex].Guid;
	var calItemID=PlanningAheadGetData.allCalItems[itemIndex].Id;
	var TitleText=PlanningAheadGetData.allCalItems[itemIndex].Title;
	var commentedDate=PlanningAheadGetDisplay.fetchEventDisplayDate(PlanningAheadGetData.allCalItems[itemIndex].StartTime);
	var TaggedPageItem=PlanningAheadGetDisplay.GetTaggedEventLike(PlanningAheadGetData.allCalItems[itemIndex].Id);
	var likeHtml="";
	var likeText="";
	var LikeCount="";
	var LikeClass="";
	var PageListGuid="";
	var PageListItemId="";
	var TaggedImageURL="/SiteCollectionImages/pagelayouts/default-article-thumbnail.png?RenditionID=7";
	if(TaggedPageItem != null)
	{
	LikeCount=TaggedPageItem.LikesCount;
	PageListGuid=TaggedPageItem.ListGuid;
	PageListItemId=TaggedPageItem.ID;
		if(TaggedPageItem.LikeDisplay ==true)
		{
		likeText="Like";
		}
		else{
		likeText="UnLike";
		LikeClass="activestate";
		}
		likeHtml='<span id="cswp-tile-like-tiny" class="SBBGC'+PageListGuid+PageListItemId+calItemID+' '+LikeClass+'" style="background-color: '+subsiteColor+';"> <span><a href="#" onclick="PlanningAheadGetDisplay.CalAgendaEventLikePage(\''+_spPageContextInfo.webAbsoluteUrl+'\',\''+PageListGuid+'\',\''+PageListItemId+'\',\''+calItemID+'\')"  class="SBDTLikeButton'+PageListGuid+PageListItemId+calItemID+'"> <div id="cswp-like-text"> '+likeText+'</div></a><span class="SBDTlikecount'+PageListGuid+PageListItemId+calItemID+'">'+LikeCount+'</span></span></span>';

		linkPage=TaggedPageItem.PageURL;
		if(TaggedPageItem.PublishingPageImage != null)
		{
		TaggedImageURL=PlanningAheadGetDisplay.GetLikeTaggedPublishingImage(TaggedPageItem.PublishingPageImage)+"?RenditionID=7";
		}
	}

	var commonHtmlStart= '<div style="float:left!important;width:90px!important;" class="upcoming-event"><div id="cswp-tile-container-tiny" style="width:90px;margin-right:0px!important;" data-displaytemplate="'+ $htmlEncode(TitleText) +'"> <div class="cbs-picture3LinesImageContainer" style="background-color:transparent!important" id="'+ pictureContainerId +'"><div id="cswp-tile-event">'
	+ commentedDate +'</div><a class="cbs-pictureImgLink" href="'+ linkPage +'" title="'+ $htmlEncode(TitleText) +'" id="'+ pictureContainerId +'"><img style="width:90px!important;height:90px!important;margin:0px 0px 0px 0px;" src="'+TaggedImageURL+'" /></a>';
	var commonHtmlEnd= '</div></div><div id="cswp-tile-title-tiny"><a class="cbs-picture3LinesLine1Link" href="'+ linkPage +'" title="'+ $htmlEncode(TitleText) +'" id="'+ pictureContainerId +'">'+ TitleText +'</a></div> </div>';

	return commonHtmlStart+likeHtml+commonHtmlEnd;

	},
	GetTaggedEventLike: function(id){
	var result=null;
			for(var i = 0; i < PlanningAheadGetData.allEventsTaggedURL.length; i++) {
				if (PlanningAheadGetData.allEventsTaggedURL[i].CalItemID == id) {		
					for(var j = 0; j < PlanningAheadGetData.allEventsImagesAndLikes.length; j++) {
						if (PlanningAheadGetData.allEventsImagesAndLikes[j].FileLeafRef === PlanningAheadGetData.allEventsTaggedURL[i].TaggedPage) {
							PlanningAheadGetData.allEventsImagesAndLikes[j].PageURL=PlanningAheadGetData.allEventsTaggedURL[i].TaggedUrlPage+".aspx";
							result=PlanningAheadGetData.allEventsImagesAndLikes[j];
							break;
						}
					}
					break;
				}
			}
	return result;
	},

	GetLikeTaggedPublishingImage : function(pageImage){
		if(pageImage != null)
		{
		pageImage = pageImage.split('src')[1].split('"')[1].split('"')[0];
		}
	return pageImage;
	},

	createReminderHTML:function(itemIndex)
	{
	var pictureContainerId=PlanningAheadGetData.allCalItems[itemIndex].Guid;
	var TitleText=PlanningAheadGetData.allCalItems[itemIndex].Title;
	var commentedDate=PlanningAheadGetDisplay.fetchEventDisplayDate(PlanningAheadGetData.allCalItems[itemIndex].StartTime);
	var remHTML='<img style="width:90px!important;height:90px!important;border-radius:60px!important;-webkit-border-radius:60px!important;-moz-border-radius: 60px!important;margin:0px 0px 0px 0px;" src="/SiteCollectionImages/pagelayouts/default-article-thumbnail.png" />';
	var commonHtmlStart= '<div style="float:left!important;width:90px!important;" class="upcoming-event"><div id="cswp-tile-container-tiny" style="width:90px;margin-right:0px!important;" data-displaytemplate="'+ $htmlEncode(TitleText) +'"> <div class="cbs-picture3LinesImageContainer" style="background-color:transparent!important" id="'+ pictureContainerId +'"><div id="cswp-tile-event">'
	+ commentedDate +'</div>'+remHTML;
	var commonHtmlEnd= '</div></div><div id="cswp-tile-title-tiny"><span>'+ TitleText +'</span></div> </div>';
	return commonHtmlStart+commonHtmlEnd;
	},
	createHoriZontalHTML:function()
	{
		var horizontalhtml='<div style="float:left;margin-top:50px"><img src="/SiteCollectionImages/HorizoontalLineImage.png" width="56px" height="5px" style="margin-top: 16px; background:transparent;" /></div>';
		return horizontalhtml;
	},
	fetchEventStartDate:function(StartDate)
	{
	var commentedDate='';
	StartDate = StartDate.substring(0,10);
		 try {
				var startDateTime = new Date(StartDate);         
				var today = new Date();  
				if(startDateTime.setHours(0,0,0,0) == today.setHours(0,0,0,0))
				{
					//Date equals today's date
					commentedDate = startDateTime.format('ddd, MMM dd, yyyy' + ' @ ' + 'HH:mm tt');
					var indexOfAt = commentedDate.toString().indexOf("@");
					commentedDate = commentedDate.toString().substring(indexOfAt, commentedDate.toString().length);
					commentedDate = "Today " + commentedDate;
					commentedDate = commentedDate .split('GMT')[0];
					commentedDate = commentedDate .split('@')[0];
				   // alert(commentedDate );
					//console.log('2'+commentedDate);
				} 
				else if(startDateTime.setDate(startDateTime.getDate()-1) == today.setHours(0,0,0,0))
				{
					commentedDate = startDateTime.format('ddd, MMM dd, yyyy' + ' @ ' + 'HH:mm tt');
					var indexOfAt = commentedDate.toString().indexOf("@");
					commentedDate = commentedDate.toString().substring(indexOfAt, commentedDate.toString().length);
					commentedDate = "Tomorrow " + commentedDate;
					commentedDate = commentedDate .split('GMT')[0];
					commentedDate = commentedDate .split('@')[0];
					//console.log('3'+commentedDate);
				   
				}
				else
				{
					commentedDate = StartDate;   
					var newStartDate = new Date(commentedDate);	        		        	
					var one_day=1000*60*60*24;
					// Convert both dates to milliseconds
					var date1_ms = today.getTime();
					var date2_ms = newStartDate.getTime();
					// Calculate the difference in milliseconds
					var difference_ms = date2_ms - date1_ms;
					// Convert back to days and return
					diff = Math.round(difference_ms/one_day); 
					if(diff<=3)
					{
						commentedDate ="In "+ diff +" days";
					}
					else{
					commentedDate=newStartDate.format('MM/dd/yyyy');
					}
				}   
			} catch (e) {
			//console.log(e);
			}
			return commentedDate;
	},
	fetchEventDisplayDate:function(StartDate)
	{
	var commentedDate='';
	StartDate = StartDate.substring(0,10);
	var year=StartDate.substr(0,4);
	var month=StartDate.substr(5,2);
	month = parseInt(month) - 1 ;
	var date=StartDate.substr(8,2);
		 try {
				var startDateTime = new Date();  
				startDateTime.setDate(date);
				startDateTime.setMonth(month);
				startDateTime.setFullYear(year);
				var today = new Date();  
				if(startDateTime.getFullYear()==today.getFullYear() && startDateTime.getMonth()==today.getMonth())
				{
				var startDate=startDateTime.getDate();
				var todayDate=today.getDate();
				var dayDiff=startDate-todayDate;
					switch(dayDiff)
					{
					case 0:
						commentedDate="Today";
						break;
					case 1:
						commentedDate="Tomorrow";
						break;
					case 2:
					case 3:
						commentedDate ="In "+ dayDiff +" days";
						break;
					default:
						commentedDate=startDateTime.format('MM/dd/yyyy');
						break;
					}
				}
			   else{
				commentedDate=startDateTime.format('MM/dd/yyyy');
				}
			} catch (e) {
			//console.log(e);
			}
			return commentedDate;
	},
	CalAgendaEventLikePage:function (contextURL, listID, listItemID,calItemID) {
		var like = false;
		var likeButtonText = $(".SBDTLikeButton" + listID + listItemID +calItemID+ " div").text().trim();
		var likeCountText = $(".SBDTlikecount" + listID + listItemID +calItemID).text().trim();
		if (likeButtonText != "") {
			if (likeButtonText == "Like")
				like = true;

			var aContextObject = new SP.ClientContext(contextURL);
			EnsureScriptFunc('reputation.js', 'Microsoft.Office.Server.ReputationModel.Reputation', function() {
				Microsoft.Office.Server.ReputationModel.
				Reputation.setLike(aContextObject,
					listID,
					listItemID, like);

				aContextObject.executeQueryAsync(
					function() {
						var likeButtonText = $(".SBDTLikeButton" + listID + listItemID +calItemID+ " div").text().trim();
						var likeCountText = $(".SBDTlikecount" + listID + listItemID +calItemID).text().trim();
						if (likeCountText != "" && likeButtonText != "") {
							var likeCount = parseInt(likeCountText);
							if (likeButtonText == "Like")
							{
								$(".SBDTLikeButton" + listID + listItemID +calItemID+ " div").text('UnLike');
								likeCount++;
								$(".SBDTlikecount" + listID + listItemID +calItemID).text(likeCount);
								$(".SBBGC" + listID + listItemID +calItemID).addClass("activestate");
							}
							else{
								$(".SBDTLikeButton" + listID + listItemID+calItemID + " div").text('Like');
								$(".SBBGC" + listID + listItemID +calItemID).removeClass("activestate");
								if(likeCount>0)
								{
								likeCount--;
								$(".SBDTlikecount" + listID + listItemID +calItemID).text(likeCount);
								}
							}
						}
					},
					function(sender, args) {
					//console.log("Error");
					});
			});
		}
		var ev  = (this.event) ? this.event : this.Event;
		(ev.preventDefault) ? ev.preventDefault() : ev.returnValue = false;
	}
}