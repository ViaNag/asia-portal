﻿var url = "";

$(document).ready(function () {
    ExecuteOrDelayUntilScriptLoaded(CreateVideoSnippet, "sp.js");
});

function UploadVid() {

    var ListID = "";

    $.ajax({
        url: _spPageContextInfo.webServerRelativeUrl + "/_api/web/lists/GetByTitle('Images')/",
        type: 'GET',

        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
            "content-type": "application/json;odata=verbose",
            'X-RequestDigest': $('#__REQUESTDIGEST').val()
        },

        success: function (responseData) {
            //console.log(JSON.stringify(responseData.d.Id));
            ListID = responseData.d.Id;
        },
        error: function () {
            //console.log("error");
        }

    });



    var options = SP.UI.$create_DialogOptions();
    options.url = _spPageContextInfo.webAbsoluteUrl + "/_layouts/15/NewVideoSet.aspx?List=" + ListID + "&ContentTypeId=0x0120D520A8080034D0AF1705D7F24994302EA8C70E07D1&";
    options.width = "650";
    options.height = "650";
    options.allowMaximize = false;
    options.dialogReturnValueCallback = Function.createDelegate(null, CloseCallback);
    SP.UI.ModalDialog.showModalDialog(options);
}

function CloseCallback(dialogResult, returnValue) {
    $("input[Title='Video Link']").val(returnValue["newFileUrl"])
    CreateVideoSnippet();


}

function CreateVideoSnippet() {

    if ($("#dvVideoLink").text().trim() == "Video Link") {
        url = $("input[Title='Video Link']").val();
    } else {
        url = $("#dvVideoLink").text();
    }


    var context = new SP.ClientContext.get_current();
    this.Web = context.get_web();
    this.site = context.get_site();


    this.folder = this.Web.getFolderByServerRelativeUrl(url);
    context.load(this.Web);
    context.load(this.site);
    context.load(this.folder, 'Properties');
    context.executeQueryAsync(Function.createDelegate(this, this.onSuccess),
        Function.createDelegate(this, this.onFail));
}

function onSuccess(sender, args) {
    var x = this.folder.get_properties().get_fieldValues()["vti_rtag"].toString().split("rt:")[1].split("@")[0];
    var AlternateThumbnailUrl = encodeURIComponent(this.folder.get_properties().get_fieldValues()["AlternateThumbnailUrl"].split(",")[0]);


    var playerUrl = this.Web.get_url() + "/_layouts/15/videoembedplayer.aspx";
    var siteID = this.site.get_id();
    var webID = this.Web.get_id();

    var embedCode = "<iframe type='text/html' width='640' height='360' src='" + playerUrl + "?site=" + siteID + "&amp;web=" + webID + "&amp;folder=" + x + "&amp;img=" + AlternateThumbnailUrl + "&amp;title=1&amp;lHome=1&amp;lOwner=1'></iframe>";
    /*console.log(siteID);
    console.log(webID);
    console.log(playerUrl);
    console.log(embedCode);*/


    $("#video").html("");
    $("#video").html("<div>" + embedCode + "</div>");

}

function onFail(sender, args) {
    //console.log('Failed:' + args.get_message());
}