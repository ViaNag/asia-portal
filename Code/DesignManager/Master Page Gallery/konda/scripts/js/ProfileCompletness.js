﻿	$(document).ready(function(){
	//getUserPropertiesFromProfile();
	});
function getUserPropertiesFromProfile() {
    // Get the current client context and PeopleManager instance.
    var clientContext = new SP.ClientContext.get_current();
    peopleManager = new SP.UserProfiles.PeopleManager(clientContext);
 
    // Get user profile properties for the current user
    userProfileProperties = peopleManager.getMyProperties();
 
    // Load the UserProfilePropertiesForUser object and send the request.
	clientContext.load(peopleManager );
    clientContext.load(userProfileProperties);
    clientContext.executeQueryAsync(onProfileRequestSuccess, onProfileRequestFail);
}
 
function onProfileRequestSuccess() {
	var _displayName = userProfileProperties.get_displayName();
	//alert(userProfileProperties.get_directReports());
	$("#viewprofile").attr("href",userProfileProperties.get_userUrl());
	$("#completeprofilelink").attr("href",peopleManager.get_editProfileLink());
	//FirstName=1|LastName=1|WorkPhone=2|PictureURL=5
	var _wghtFirstName = 1;
	var _wghtLastName = 1;
	var _wghtWorkPhone = 2;
	var _wghtPictureURL = 5;
 	var _profilePicture = userProfileProperties.get_pictureUrl();
	var _firstName = userProfileProperties.get_userProfileProperties()["FirstName"];
	var _lastName = userProfileProperties.get_userProfileProperties()["LastName"]; 
	var _workPhone = userProfileProperties.get_userProfileProperties()["WorkPhone"]; 
	
	var _valProfilePicture = ( _profilePicture == '' || _profilePicture == null) ? 0 : 1;
	var _valFirstName = ( _firstName == '' || _firstName == null) ? 0 : 1;
	var _valLastName = ( _lastName == '' || _lastName == null) ? 0 : 1;
	var _valWorkPhone = ( _workPhone == '' || _workPhone == null) ? 0 : 1;
	
	var _completePercent = 10 *((_valProfilePicture * _wghtPictureURL) + (_valFirstName * _wghtFirstName ) + (_valLastName * _wghtLastName ) + (_valWorkPhone * _wghtWorkPhone ));
	$("#bar").css("width", _completePercent+"%");
	$("#completePrecentage").html(_completePercent+"%");
	
	
		
	if(_valWorkPhone)	
		$("#AddExtension").addClass("completed");
	if(_valProfilePicture)	
		$("#Profileuplaod").addClass("completed");
		
		
		
	$(".circular").css("background","url("+_profilePicture+") no-repeat");
	$(".Information").html(_displayName);
	//alert(propertyValue);
} 
 
function onProfileRequestFail() {
    // Handle failure
} 
