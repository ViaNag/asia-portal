﻿var mostShared = {
    clientContext: null,
    web: null,
    camlQuery: null,
    mostSharedArticle:null,
    GetUrl: function() {
        mostShared.clientContext = new SP.ClientContext.get_current();
        mostShared.web = mostShared.clientContext.get_site().get_rootWeb();
        oLists = mostShared.web.get_lists().getByTitle('Most Shared');
        mostShared.clientContext.load(oLists);
        mostShared.listRootFolder = oLists.get_rootFolder();
        mostShared.camlQuery = new SP.CamlQuery();
        collListItems = oLists.getItems(mostShared.camlQuery);
        mostShared.clientContext.load(collListItems);
        mostShared.clientContext.executeQueryAsync(mostShared.successGetUrl, mostShared.failure);
    },

    successGetUrl: function() {
    	
        var mainArray = [];
        var Url = '';
         var articleName = '';
         var siteName = '';
         var image = '';
         var article = [];
         var site = [];
         var img = [];

        var listItemEnumerators = collListItems.getEnumerator();
        while (listItemEnumerators.moveNext()) {
            oListItems = listItemEnumerators.get_current();
            Url = oListItems.get_item('Url');
            articleName = oListItems.get_item('Title');
            siteName =  oListItems.get_item('SharedSiteTitle');
            image = oListItems.get_item('ThumbnailImage');
			if(image != null && image != undefined)
			{
				if(image.toLowerCase().indexOf("?renditionid") < 0)
				{
					image = image+"?RenditionID=6";
				}
			}
            mainArray.push(Url);
            article.push(articleName);
            site.push(siteName);
            img.push(image);
          
        }
        var innerArray = [];
        for (var i = 0; i < mainArray.length; i++) {
            var exits = false;
            if (i > 0) {
                for (var j = 0; j < innerArray.length; j++) {
                    if (innerArray[j][0] == mainArray[i]) {
                        var count = innerArray[j][1];
                        count = count + 1;
                        innerArray[j][1] = count;
                        exits = true;
                    }
                }

            } else {
                innerArray.push([mainArray[i], 1,article[i],site[i],img[i]]);
                exits = true;
            }
            if (!exits) {
                innerArray.push([mainArray[i], 1,article[i],site[i],img[i]]);
            }
        }
		innerArray.sort(sortfunc);		
		function sortfunc(a,b){
		  return b[1] - a[1];
		}
        var results = innerArray[0];                

        $('.number-of-shares').html(innerArray[0][1]);         
        $('.page-title-url').html(innerArray[0][2]);
        $('.page-title-url').attr('href',innerArray[0][0]);
		if(innerArray[0][4] != undefined && innerArray[0][4] != null){        
			$('.page-image-url').attr('src',innerArray[0][4]);
		}
		$('.mostshared-image-url').attr('href',innerArray[0][0]);

    },
 failure:function(sender,args){
 	//console.log(args.get_message() + '\n' + args.get_stackTrace());
 },
}

$(window).load(function() {
   mostShared.GetUrl();
    //MostCommented.loadWebs();
    
});

var mostCommentedArticle=[];

var MostCommented={
clientContext: null,
    web: null,
    camlQuery: null,
	relUrl: null,
	webUrl: null,
    
    recursiveAll:function (web){    
	  if(web.get_title()!="Training"){	   	
			MostCommented.GetUrl(web.get_title());						
		}
	},
	onWebsLoaded :function () {		
	   for (var i = 0; i < webs.get_count(); i++) {
           this.subwebs = webs.itemAt(i);
           MostCommented.recursiveAll(this.subwebs)
	  }
		mostCommentedArticle.sort(sortfunc);					
		function sortfunc(a, b) {
		    return b[1] - a[1];
		}
		//Fetch image for most commented article
		if(mostCommentedArticle[0] != undefined && mostCommentedArticle[0] != null && mostCommentedArticle[0][0] != undefined && mostCommentedArticle[0][0] != null){
			var pageUrl = mostCommentedArticle[0][0];
			var url = pageUrl;
			MostCommented.webUrl = pageUrl.substring(0,pageUrl.indexOf("/Pages"));
			//var pageName = pageUrl.substring(pageUrl.lastIndexOf("\\")+1);
			var ctx = new SP.ClientContext(MostCommented.webUrl); //get context
			// context is valid - proceed 
			MostCommented.relUrl = pageUrl.replace(_spPageContextInfo.siteAbsoluteUrl, '');  //convert to relative url
			var file = ctx.get_web().getFileByServerRelativeUrl(MostCommented.relUrl);   //get file
			ctx.load(file, 'ListItemAllFields');
			ctx.executeQueryAsync(
				function () {
					var listItem = file.get_listItemAllFields();
					var listItemId = listItem.get_fieldValues().ID;
					var pageImageSrc;
					var galleryImages="";
					var contentTypeId = listItem.get_fieldValues().ContentTypeId.$c_1;
					//Survey type Article
					if(contentTypeId.indexOf("0x010100C568DB52D9D0A14D9B2FDCC96666E9F2007948130EC3DB064584E219954237AF3900242457EFB8B24247815D688C526CD44D00D82A09EDFC42AA4FBA3089B892E0C3880704")> -1){
						pageImageSrc="/SiteCollectionImages/pagelayouts/tile-bg-survey-sm.png";
					}
					//Gallery Type Article
					else if(contentTypeId.indexOf("0x010100C568DB52D9D0A14D9B2FDCC96666E9F2007948130EC3DB064584E219954237AF3900242457EFB8B24247815D688C526CD44D00D82A09EDFC42AA4FBA3089B892E0C3880703")> -1){
						var imageURLs = "<ul>";		
					   $.ajax({
						url: MostCommented.webUrl+"/_api/web/lists/GetByTitle('Images')/items?$limit=1&$select=FileRef,Title&&expand=TaggedPage/ID&$filter=TaggedPage/ID eq '"+listItemId+"'",
						type: 'GET',
						async:false,
						headers: { 
							"accept":"application/json;odata=verbose",
							"content-type":"application/json;odata=verbose",
							 'X-RequestDigest': $('#__REQUESTDIGEST').val()         
							},
				
						success: function(responseData) {
						 $(responseData.d.results).each(function(){ 
					   pageImageSrc =$(this).attr("FileRef")+"?RenditionID=6";
						});
						},
						error: function(){
							//console.log("error");
						}
					   
					});
					}
					//Other Articles
					else{
						var publishingPageImage = listItem.get_fieldValues().PublishingPageImage;
						pageImageSrc = $(publishingPageImage).attr("src");
					}
					
					if(pageImageSrc != undefined && pageImageSrc != null){
						if(pageImageSrc.toLowerCase().indexOf("?renditionid") > -1)
						{
							pageImageSrc = pageImageSrc+"?RenditionID=6";
						}
						$('.commented-page-image-url').attr('src',pageImageSrc);
					}
				},
				function (sender, args) {
					//console.log(args.get_message()); //errorhandling
				}
			);
		}
		//
		
		
		if(mostCommentedArticle[0] != undefined && mostCommentedArticle[0] != null){
		
			$('.number-of-comments').html(mostCommentedArticle[0][1]);
			
			$('.commented-title-url').html(mostCommentedArticle[0][4]);
			$('.commented-title-url').attr('href',mostCommentedArticle[0][0]);
			$('.mostcommented-image-url').attr('href',mostCommentedArticle[0][0]);
		}
	},
	loadWebs: function () {
		MostCommented.clientContext = new SP.ClientContext.get_current();
		var webTset = MostCommented.clientContext.get_site().get_rootWeb();
        webs = webTset.getSubwebsForCurrentUser(null);
		MostCommented.clientContext.load(webs);
		MostCommented.clientContext.executeQueryAsync(MostCommented.onWebsLoaded, MostCommented.failure);
	},
	GetUrl: function(Web) {	
	$.ajax({
		   url: _spPageContextInfo.siteAbsoluteUrl+"/"+Web+"/_api/lists/getbytitle('Comments')/items?" +"$select=Title,ThumbnailImage,ArticleTitle,ReportAbuse,Deleted,Created,Modified&$filter=(ReportAbuse eq '0') and (Deleted eq '0')",
		   type: "GET",
		   headers: { "Accept": "application/json;odata=verbose" },
		   async:false,
		   success: function(data, textStatus, xhr) {
		   			var mainArray = [];	
		   			var image=[];	
		   			var articleTitle = [];
		   			var created = [];
					for( var k=0;k< data.d.results.length;k++)
					{
						 mainArray.push(data.d.results[k].Title);
						 created.push(data.d.results[k].Created);
						 image.push(data.d.results[k].ThumbnailImage);
						 articleTitle.push(data.d.results[k].ArticleTitle);		 
						// created = data.d.results[k].Created;				 
					}  
					var innerArray = [];
					for (var i = 0; i < mainArray.length; i++) {
					    var exits = false;
					    if (i > 0) {
					        for (var j = 0; j < innerArray.length; j++) {
					            if (innerArray[j][0] == mainArray[i]) {
					                var count = innerArray[j][1];
					                count = count + 1;
					                innerArray[j][1] = count;
					                exits = true;
					            }
					        }					
					    } else {
					        innerArray.push([mainArray[i], 1,created[i],image[i],articleTitle[i]]);
					        exits = true;
					    }
					    if (!exits) {
					        innerArray.push([mainArray[i], 1,created[i],image[i],articleTitle[i]]);
					    }
					}
					innerArray.sort(sortfunc);
					
					function sortfunc(a, b) {
					    return b[1] - a[1];
					}
					var results = innerArray[0];
					//alert(results);	
					mostCommentedArticle.push(innerArray[0]);
					mostCommentedArticle.push(innerArray[1]);
					mostCommentedArticle.push(innerArray[2]);					
					mostCommentedArticle.push(innerArray[3]);
					mostCommentedArticle.push(innerArray[4]);
					
			}, 	
		   error: function(xhr, textStatus, errorThrown) {
		  // alert("error 1:"+JSON.stringify(xhr));
		   }
	   });

 },
	
 failure:function(sender,args){
 	//console.log(args.get_message() + '\n' + args.get_stackTrace());
 }
}



