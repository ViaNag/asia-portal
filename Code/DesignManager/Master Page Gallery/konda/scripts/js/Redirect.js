$(window).load(function(){
	 redirect.RedirectToPageFromPagesLibrary();
	 redirect.RedirectToPageFromCommentsList();
	 redirect.ReplaceHttpToHttps();
	 $("#ctl00_PlaceHolderMain_pageTitleSection_ctl03_urlNameTextBox").attr("onchange","javascript:urlNameTextCkhange();");
	 $("#ctl00_PlaceHolderMain_pageTitleSection_ctl01_titleTextBox").attr("onchange","javascript:titleTextCkhange();");
});

var redirect={
// This method returns true if the passed query string is present in the current URL.
// Else it will return false
getQueryStringParameter:function(urlParameterKey) {
    var queryString = document.URL.split('?');
    if(queryString.length>=2){
		var params = document.URL.split('?')[1].split('&');
		var strParams = '';
		for (var i = 0; i < params.length; i = i + 1) {
			var singleParam = params[i].split('=');
			if (singleParam[0].toLowerCase() == urlParameterKey.toLowerCase())
				return true;
		}
    }
	return false;

},

// This method redirects user to actual page from the Display Form of that page.
// To stop the redirection please use cancelRedirect=true as querystring in the url.
RedirectToPageFromPagesLibrary:function()
{
	var isCancelRedirect = redirect.getQueryStringParameter("cancelRedirect");
	var isPagesDispForm = false ;
	var currentLocation = window.location.href;
	var displayFormUrl = "Pages/Forms/DispForm.aspx";
	if(currentLocation.toLowerCase().indexOf(displayFormUrl.toLowerCase()) > -1)
	{
		isPagesDispForm = true;
	}
	if(!isCancelRedirect && isPagesDispForm)
	{
		if($("#SPFieldFile") != undefined && $("#SPFieldFile") != null && $("#SPFieldFile").find("a").length > 0)
		{
			var redirectLocation = $("#SPFieldFile").find("a")[0].href;
			if(redirectLocation != null && redirectLocation != undefined && redirectLocation != "")
			{
				window.location = redirectLocation;
			}
		}
	}
},

// This method redirects user to actual page from the comments list by reading the url available on the form.
// To stop the redirection please use cancelRedirect=true as querystring in the url.
RedirectToPageFromCommentsList:function()
{
	var isCancelRedirect = redirect.getQueryStringParameter("cancelRedirect");
	var isPagesDispForm = false ;
	var currentLocation = window.location.href;
	var displayFormUrl = "/Lists/Comments/DispForm.aspx";
	if(currentLocation.toLowerCase().indexOf(displayFormUrl.toLowerCase()) > -1)
	{
		isPagesDispForm = true;
	}
	if(!isCancelRedirect && isPagesDispForm)
	{
		var titleFieldLabel = $('a[name="SPBookmark_Title"]');
		if(titleFieldLabel!= undefined && titleFieldLabel != null && titleFieldLabel.parent() != null && titleFieldLabel.parent() != undefined)
		{
			if(titleFieldLabel.parent().parent() != null && titleFieldLabel.parent().parent() != undefined)
			{
				if(titleFieldLabel.parent().parent().parent() != null && titleFieldLabel.parent().parent().parent() != undefined)
				{
					var titleRowChildren = titleFieldLabel.parent().parent().parent().children();
					if(titleRowChildren != null && titleRowChildren != undefined && titleRowChildren.length >= 2)
					{
						var commentsParentPage = titleRowChildren[1];
						var commentsParentPageUrl = commentsParentPage.textContent.trim();
						window.location = commentsParentPageUrl;
					}
				}
			}
		}
	}
},

// This method changes any url from the Comments/Description field that starts with http to /company/pages/businesscalendar.aspx 
ReplaceHttpToHttps:function()
{
	var bCalDisplayFormUrl = "/Lists/Business%20Calendar/DispForm.aspx";
	var currentLocation = window.location.href;
	var isBuissnessCalDispForm = false;
	var newUrlInComments = "/company/pages/businesscalendar.aspx";
	if(currentLocation.toLowerCase().indexOf(bCalDisplayFormUrl.toLowerCase()) > -1)
	{
		isBuissnessCalDispForm = true;
	}
	if(isBuissnessCalDispForm)
	{
		var descriptionLabel = $('a[name="SPBookmark_Description"]');
		if(descriptionLabel != null && descriptionLabel != undefined)
		{
			if(descriptionLabel.length > 0)
			{
				var parentTR = descriptionLabel.parent().parent().parent();
				var TRchildren = parentTR.children();
				if(TRchildren != null && TRchildren != undefined)
				{
					if(TRchildren.length > 1)
					{
						var commentsDescriptionTD = TRchildren[1];
						var anchorInComments = $(commentsDescriptionTD).find("a");
						var isAnchorFound = false;
						if(anchorInComments != null && anchorInComments != undefined)
						{
							if(anchorInComments.length > 0)
							{
								isAnchorFound = true;
								anchorInComments.attr("href",_spPageContextInfo.siteAbsoluteUrl + newUrlInComments);
								anchorInComments[0].textContent = _spPageContextInfo.siteAbsoluteUrl + newUrlInComments;
							}
						}
						if(!isAnchorFound)
						{
							if(commentsDescriptionTD.textContent.trim().toLowerCase().indexOf("http://") > -1)
							{
								commentsDescriptionTD.textContent = _spPageContextInfo.siteAbsoluteUrl + newUrlInComments;
							}
						}
					}
				}
			}
		}
	}
}
}


function titleTextCkhange()
{
UpdateUrl();
var urlNameText=$("#ctl00_PlaceHolderMain_pageTitleSection_ctl01_titleTextBox").val();
RemoveSpecialCharacter(urlNameText);
}
function urlNameTextCkhange()
{
var urlNameText=$("#ctl00_PlaceHolderMain_pageTitleSection_ctl03_urlNameTextBox").val();
RemoveSpecialCharacter(urlNameText);
}
function RemoveSpecialCharacter(urlNameText){
	 urlNameText=urlNameText.trim().replace(/[!@#$%^&*(){}=;:",<>+~`']/g, '');
	 $("#ctl00_PlaceHolderMain_pageTitleSection_ctl03_urlNameTextBox").val(urlNameText);
}