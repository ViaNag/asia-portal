﻿$(document).ready(function() {

    ExecuteOrDelayUntilScriptLoaded(getFollowedSites.fetchUserProfile("formattedname", "unknown", "onload"), "SP.UserProfiles.js");


});

var getFollowedSites = {
    clientContext: null,
    web: null,
    LoginName: null,
    followedSiteName: null,
    followedSiteUri: null,
    userProfile: null,
    fetchUserProfile: function(name, url, action) {

        //$.noConflict();
        getFollowedSites.clientContext = new SP.ClientContext.get_current();
        getFollowedSites.web = getFollowedSites.clientContext.get_site().get_rootWeb();
        currentUser = getFollowedSites.web.get_currentUser();
        oList = getFollowedSites.web.get_lists().getByTitle('Config');
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml('<View><Query><Where><Eq><FieldRef Name=\'Title\'/><Value Type=\'Text\'>Test</Value></Eq></Where></Query><ViewFields><FieldRef Name=\'FetchProfileProperties\'/><FieldRef Name=\'Title\'/></ViewFields></View>');
        collListItem = oList.getItems(camlQuery);
        getFollowedSites.clientContext.load(collListItem);
        getFollowedSites.clientContext.load(currentUser);
        getFollowedSites.clientContext.executeQueryAsync(function() {
            getFollowedSites.fetchUserProfileSuccess(name, url, action)
        }, getFollowedSites.failure);
    },
    fetchUserProfileSuccess: function(name, url, action) {
        var listItemInfo = '';
        var listItemEnumerator = collListItem.getEnumerator();
        while (listItemEnumerator.moveNext()) {
            var oListItem = listItemEnumerator.get_current();
            listItemInfo = oListItem.get_item('FetchProfileProperties');
        }
        userProfile = listItemInfo;
        getFollowedSites.getUserPropertiesFromProfile(name, url, action);

    },
    getUserPropertiesFromProfile: function(name, url, action) {

        var userProfileInfo = userProfile.split(",");
        LoginName = currentUser.get_loginName();
        LoginName = LoginName.split('|')[1];
        var TopPagesValue = null;
        $.ajax({
            url: "/_api/SP.UserProfiles.PeopleManager/GetPropertiesFor(accountName=@v)?@v='" + LoginName + "'",
            async: false,
            headers: {
                Accept: "application/json;odata=verbose"
            },
            success: function(data) {
            
					for (var i = 0; i < data.d.UserProfileProperties.results.length; i++) {
                    if (data.d.UserProfileProperties.results[i].Key == "TopPages"){
                        	TopPagesValue = data.d.UserProfileProperties.results[i].Value; 
                        	var _topPages = TopPagesValue.replace(/\|/g, '');
							_topPages = _topPages.split('@#@');
							var divHtml = '<table id="table-1" border="1" cellspacing="3" cellpadding="3">';
							
							for (var k=0;k<_topPages.length-1;k++)
							{   
						   		var topPagesTitle = _topPages[k].split(',');
						   		divHtml+='<tr id="greenroom-panel-top-pages-right-'+k+'" class="myDragClass" style="cursor: move;"><td style="width:70%"><a target="_blank" href="'+topPagesTitle[1]+'">'+topPagesTitle[0]+'</a></td></tr>';
							}

							divHtml += '</table><div class="site-name">Drag & change the Top-Pages Order.</div><input type="button" class="btn-sm" value="Save Order" onclick="getFollowedSites.saveTopPagesOrder();" />';
							$('#greenroom-top-pages ul').html(divHtml);
							
							/*-------------DRAG & DROP----------------*/
							$("#table-1").tableDnD({
													
						        onDragClass: "myDragClass",
						        onDrop: function(table, row) {	    		        
						       	
					            var rows = table.tBodies[0].rows;
					            var debugStr = "<br /> Row dropped was "+row.id+"<br /> New order: ";
					            for (var i=0; i<rows.length; i++) {
					               debugStr += rows[i].id+"" ;					              
					               debugStr += breaktag="<br />";			               
					            }
					            //$('#debugArea').html(debugStr);
					        },
					        	onDragStart: function(table, row) {
					            //$('#debugArea').html("Started dragging row "+row.id);
					        }
						    });
						    /*-------------DRAG & DROP----------------*/
						}
					}
            },
            
			error: function(jQxhr, errorCode, errorThrown) {
	                //console.log(errorThrown);
	            }
	        });
	        if (action == "create")
	            getFollowedSites.onSuccess(name, url, TopPagesValue);
	        if (action == "remove")
	            getFollowedSites.RemoveUserProfileValue(name, url, TopPagesValue);
	        if (action != "remove" && action != "create")
	            getFollowedSites.onLoad(TopPagesValue);
    },

    failure: function(sender, args) {
        //console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
    },

    onSuccess: function(name, url, TopPagesValue) {
		
		
        TopPagesValue = TopPagesValue.replace(/\|/g, '');
        var TopPagesValueCount = TopPagesValue.split("@#@");
        if (TopPagesValueCount.length <= 10) {

            var formattedValue = name + ',' + url;
            if (formattedValue.indexOf("&") > -1) {
                formattedValue = formattedValue.replace(/\&/g, "and");
                //console.log(formattedValue);
            }
            if (TopPagesValue == null || TopPagesValue == "" || TopPagesValue == "undefined") {
            
                formattedValue = formattedValue + '@#@';
                getFollowedSites.updateUserProfileValue(LoginName, "TopPages", formattedValue);
            } 
            else {
                if (TopPagesValue.indexOf(formattedValue) == -1) {
                
                    formattedValue = TopPagesValue + formattedValue + '@#@';
                    //getFollowedSites.updateUserProfile(LoginName,"TopPages",formattedValue);
                    getFollowedSites.updateUserProfileValue(LoginName, "TopPages", formattedValue);

                }
            }
            //location.reload();
                        
            $('#greenroom-top-pages ul').html('');
            $('#greenroom-top-sites ul').html('');   
            getFollowedSites.fetchUserProfile("formattedname", "unknown", "onload");
            
        } else
            alert("There is a maximum of 10 shortcuts, please remove a shortcut");
        
    },

    saveTopPagesOrder: function(){
    
        var propertyOrder = '';
		var tbl = document.getElementById("table-1");
		
        if (tbl != null) {
            for (var i = 0; i < tbl.rows.length; i++) {
                for (var j = 0; j < tbl.rows[i].cells.length; j++)
                    propertyOrder += $(tbl.rows[i].cells[j]).text() + ',' + $(tbl.rows[i].cells[j]).find('a').attr('href') + '@#@';
            }
        }
        getFollowedSites.updateUserProfileValue(LoginName, "TopPages", propertyOrder);
    },

    updateUserProfileValue: function(LoginName, propertyName, propertyValue) {

        //var propertyArray = propertyValue.split('@#@');
        //var multiValues = "";
        /*for (var i = 0; i < propertyArray.length - 1; i++) {
            multiValues += "<ValueData><Value xsi:type=\"xsd:string\">" + propertyArray[i] + "@#@</Value></ValueData>";
        }
        multiValues = multiValues.replace(/\|/g, '');
        var propertyData = "<PropertyData>" +
            "<IsPrivacyChanged>false</IsPrivacyChanged>" +
            "<IsValueChanged>true</IsValueChanged>" +
            "<Name>" + propertyName + "</Name>" +
            "<Privacy>NotSet</Privacy>" +
            "<Values>" + multiValues + "</Values>" +
            "</PropertyData>";
        $().SPServices({
            operation: "ModifyUserPropertyByAccountName",
            async: false,
            webURL: _spPageContextInfo.siteAbsoluteUrl,
            accountName: LoginName,
            newData: propertyData,
            completefunc: function(xData, Status) {
                var result = $(xData.responseXML);
            }
        }); */
		propertyValue = propertyValue.replace(/\|/g, '');
        propertyValue = propertyValue.replace(/\&/g, '&amp;');
		var domain, userName;
	
		if(LoginName != undefined && LoginName != null && LoginName !="")
		{
			domain = LoginName.split("\\")[0];
			userName = LoginName.split("\\")[1];
		}
		var userprofileServiceURL=GetUserProfileUrlFromLocalStarageJson();
		var siteUrl = GetMySiteUrlFromLocalStarageJson();
		$.support.cors = true;
		var jsonDataType = "json";
		// var myNav = navigator.userAgent.toLowerCase();
		// var browserType = (myNav.indexOf('msie') != -1) ? true : false;
		// if(browserType)
		// {
			// var ieVersion = parseInt(myNav.split('msie')[1]);
			// if(ieVersion < 10)
			// {
				// jsonDataType = "jsonp";
			// }
		// }
		$.ajax({
               url: userprofileServiceURL,
			   dataType: jsonDataType,
               type: "GET",
			   data: { domain : domain, userName: userName, propertyName: propertyName, value: propertyValue, siteUrl: siteUrl },
               success: function (data) {
                   //Grab our data from Ground Control
                   if(data.indexOf("Commit Success") > -1)
				   {
					   location.reload();
				   }
				   else
				   {
					   if(console != undefined && console != null)
					   {
							//console.log(data);
					   }
				   }
               },
               error: function (data) {
                   /* Added code to check the status code and status text in case of IE9
				   as IE couldn't parse the response XML and falls into the error function
				   with all the successful processing at the back end and statusText=success and status=200 */
				   if(data.statusText.toLowerCase() == "success" && data.status == 200)
				   {
					   location.reload();
				   }
				   //If any errors occurred - detail them here
				   else if(console != undefined && console != null)
				   {
						//console.log(data);
				   }
               }
           });

    },
    RemoveUserProfileValue: function(name, url, TopPagesValue) {
        var formattedValue = name + ',' + url + '@#@';
        if (TopPagesValue.indexOf(formattedValue) > -1) {
            TopPagesValue = TopPagesValue.replace(formattedValue, "");
        }
        getFollowedSites.updateUserProfileValue(LoginName, "TopPages", TopPagesValue);
        //location.reload();
    },
    onLoad: function(TopPagesValue) {

        var size;
        var formattedName;
        $.ajax({
            url: "/_api/social.following/my/followed(types=4)",
            type: "GET",
            headers: {
                "Accept": "application/json;odata=verbose"
            },
            async: false,
            success: function(data, textStatus, xhr) {

                for (i = 0; i < data.d.Followed.results.length; i++) {
                    var followedSiteName = data.d.Followed.results[i].Name;
                    var followedSiteUri = data.d.Followed.results[i].Uri;
                    if (followedSiteName.indexOf("&") > -1) {
                        followedSiteName = followedSiteName.replace(/\&/g, "and");
                    }
                    formattedName = followedSiteName + "," + followedSiteUri + "@#@";                    
                    if (TopPagesValue.indexOf(formattedName) > -1) {
                        li = '<li><div class="site-name">' + data.d.Followed.results[i].Name + '</div><div class="site-link"><a target="_blank" href="' + data.d.Followed.results[i].Uri + '">' + data.d.Followed.results[i].Uri + '</a></div> <input type="button" class="btn-sm" value="Stop Followiing" onclick="getFollowedSites.stopFollowSite(\'' + followedSiteName + '\',\'' + followedSiteUri + '\',\'remove\',\'' + LoginName + '\');" /> <input class="create btn-sm" type="button" id ="CreateShortcut" onclick="getFollowedSites.fetchUserProfile(\'' + followedSiteName + '\',\'' + followedSiteUri + '\',\'remove\');"value="Remove Shortcut" style="border: aliceblue;background-color: gainsboro;text-shadow: azure;" /><img src="/SiteCollectionImages/icons/greenroom-panel-icon-circle-check.png"/></div></li>'
                        $('#greenroom-top-sites ul').append(li);
                    } else {
                        li = '<li><div class="site-name">' + data.d.Followed.results[i].Name + '</div><div class="site-link"><a target="_blank" href="' + data.d.Followed.results[i].Uri + '">' + data.d.Followed.results[i].Uri + '</a></div><div><input type="button" class="btn-sm" value="Stop Followiing" onclick="getFollowedSites.stopFollowSite(\'' + followedSiteName + '\',\'' + followedSiteUri + '\',\'remove\');" /> <input class="create btn-sm" type="button" id ="CreateShortcut" value="Create Shortcut" onclick="getFollowedSites.fetchUserProfile(\'' + followedSiteName + '\',\'' + followedSiteUri + '\',\'create\');" /></div></li>'
                        $('#greenroom-top-sites ul').append(li);
                    }
                  }
                }
            
        });

        $('.create').click(function() {
            //$(this).parent('div').append('<img src="/SiteCollectionImages/icons/greenroom-panel-icon-circle-check.png"/>');

        });
    },
    stopFollowSite: function(name, url, action, LoginName) {
        $.ajax({
            url: "/_api/social.following/stopfollowing(ActorType=0,AccountName=@v,Id=null)?@v='" + LoginName + "'",
            type: "POST",
            data: JSON.stringify({
                "actor": {
                    "__metadata": {
                        "type": "SP.Social.SocialActorInfo"
                    },
                    "ActorType": 2,
                    "ContentUri": url,
                    "Id": null
                }
            }),
            headers: {
                "accept": "application/json;odata=verbose",
                "content-type": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
            },
            success: function() {
                alert('The user has stopped following the site.');

            }


        });
        getFollowedSites.fetchUserProfile(name, url, action);
    }
}