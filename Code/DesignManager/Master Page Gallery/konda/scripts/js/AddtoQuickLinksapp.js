﻿//  Add to Quick Links App contains a button to add site/page to quick links
//  Once clicked this app first checks if the site/page is followed
//  If followed a status bar with message "This has been added to your quick links!" is displayed 
//  Else is added to users quick links and a success message is displayed

_addtoQuickLinks = function (){
	this._siteUrl =_spPageContextInfo.siteAbsoluteUrl;
	this._webUrl =_spPageContextInfo.webAbsoluteUrl;	
	this._documentUrl = _spPageContextInfo.siteAbsoluteUrl+_spPageContextInfo.serverRequestPath;
	this._digest = $("#__REQUESTDIGEST").val();
}

_addtoQuickLinks.prototype._funcAddSitetoQuickLinks =  function (){
	var _objFollowedContent = new  _followedContent(this._webUrl , 1 , this._siteUrl ,this._digest);
	var objIsFollowed  = _objFollowedContent._funcIsFollowed();
 	/*if(objIsFollowed == true)
  		{console.log("true");_objFollowedContent._funcStopFollowing(); }
 	else
  		{console.log("false");_objFollowedContent._funcStartFollowing();}*/
  		_objFollowedContent._funcStartFollowing();
}

_addtoQuickLinks.prototype._funcAddPagetoQuickLinks =  function (){
	var _objFollowedContent = new  _followedContent(this._documentUrl , 2 , this._siteUrl ,this._digest);
	var objIsFollowed  = _objFollowedContent._funcIsFollowed();
 	/*if(objIsFollowed == true)
  		{console.log("true");_objFollowedContent._funcStopFollowing(); }
 	else
  		{console.log("false");_objFollowedContent._funcStartFollowing();}*/
  		_objFollowedContent._funcStartFollowing();
  		
}


function AddSitetoQuickLinks()
{
	var oAddtoQuickLinks = new _addtoQuickLinks();
	oAddtoQuickLinks._funcAddSitetoQuickLinks();
	
}

function AddPagetoQuickLinks()
{
	var oAddtoQuickLinks = new _addtoQuickLinks();
	oAddtoQuickLinks._funcAddPagetoQuickLinks ();
}
