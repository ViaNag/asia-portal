var selectedValues=[];
$(document).ready(function() {
GetConfigListDataJson();
loadClockValuesFromUserProfile();
    $('#btn-add').click(function(){
        $('#select-from option:selected').each( function() {
                $('#select-to').append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
            $(this).remove();
        });
    });
    $('#btn-remove').click(function(){
        $('#select-to option:selected').each( function() {
            $('#select-from').append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
            $(this).remove();
        });
    });
 
});
function loadClockValuesFromList()
{

var listName="Master Cities";

$.each($('#select-to option'),function(i,obj){
 selectedValues.push($(obj).val());
});
$.ajax({
    url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('" + listName + "')/GetItems(query=@v1)?@v1={'ViewXml':'<View><Query></Query></View>'}",
    type: "POST",
    headers: {
        "Accept": "application/json;odata=verbose",
        "X-RequestDigest": $("#__REQUESTDIGEST").val()
    },

    success: function (data, textStatus, jqXHR) {
for (var i = 0; i < data.d.results.length; i++) {
                        var item=data.d.results[i];
						if($.inArray(item.LocationClockCode, selectedValues)==-1)
						                      {
						$('#select-from').append("<option value='"+item.LocationClockCode+"'>"+item.CityName.Label+"</option>");
						}
                                 }       
	  
    },

    error: function (jqXHR, textStatus, errorThrown) {
        console.dir(jqXHR);
    }
});
 
}
function loadClockValuesFromUserProfile()
{
 $.ajax({  
            url: _spPageContextInfo.webAbsoluteUrl + "/_api/SP.UserProfiles.PeopleManager/GetMyProperties",  
            headers: { Accept: "application/json;odata=verbose" },  
            success: function (data) { 
                try {  
                      var properties = data.d.UserProfileProperties.results;  
                    for (var i = 0; i < properties.length; i++) {
                        var property=properties[i];
                        if (property.Key == "WorldClockKonda") {  
						$.each(property.Value.split(","),function(i,val){
						var label="";
						if(val!="")
						{
						label=val.split("@#@")[1]
						if (label.indexOf("$")!=-1)
						{
						label= label.replace(/\$/g, ",");
						}
						$('#select-to').append("<option value='"+val.split("@#@")[0]+"'>"+label+"</option>");
						}
						
						
						});
                         
                        }  
                      
                    }
					loadClockValuesFromList();
                } catch (err2) {  
                   console.log(JSON.stringify(err2));  
                }  
            },  
            error: function (jQxhr, errorCode, errorThrown) {  
                console.log(errorThrown);  
            }  
        });
}

function updateClockValue()
{
   var clockValues="";
   
	$.ajax({  
  
            url: _spPageContextInfo.webAbsoluteUrl + "/_api/SP.UserProfiles.PeopleManager/GetMyProperties",  
            headers: { Accept: "application/json;odata=verbose" },  
            success: function (data) { 
 var loginName = data.d.AccountName;
 if($('#select-to option').length>0)
 {
$.each($('#select-to option'),function(i,obj){
	var label=$(obj).text();
	if(label.indexOf(",")!=-1)
	{
	label=$(obj).text().replace(/\,/g, "$")
	}
    clockValues+=$(obj).val()+"@#@"+label+",";
});
}
	console.log(loginName);
	updateUserProfle(loginName,"WorldClockKonda",clockValues); 
			},
			error: function (jQxhr, errorCode, errorThrown) {  
                console.log(errorThrown);  
            }  
        }); 
	
}function updateUserProfle(LoginName, propertyName, propertyValue){
	var domain, userName;
	if(LoginName != undefined && LoginName != null && LoginName !="")
	{
		domain = LoginName.split("\\")[0];
		userName = LoginName.split("\\")[1];
	}
	var userprofileServiceURL=GetUserProfileUrlFromLocalStarageJson();
	var siteUrl = GetMySiteUrlFromLocalStarageJson();
	$.support.cors = true;
	var jsonDataType = "json";
		$.ajax({
               url: userprofileServiceURL,
			   dataType: jsonDataType,
               type: "GET",
			   data: { domain : domain, userName: userName, propertyName: propertyName, value: propertyValue, siteUrl: siteUrl },
               success: function (data) {
               window.frameElement.commitPopup();
               window.frameElement.navigateParent(_spPageContextInfo.webAbsoluteUrl+'/Pages/homepage.aspx');
                   //Grab our data from Ground Control
					   if(console != undefined && console != null)
					   {
							console.log(data);
												   }
               },
               error: function (data) {
				   
                   //If any errors occurred - detail them here
				   if(console != undefined && console != null)
				   {
						console.log(data);
				   }
               }
           });

}
function GetConfigListDataJson(){		
		var retrievedObject = localStorage.getItem('ConfigListDataJson');
		ConfigListData=JSON.parse(retrievedObject);
		var callConfig = $.ajax({
				url: "/_api/lists/getbytitle('config')/items?" +
					"&$select=Title,sitepath,FetchProfileProperties", 
				type: "GET",
				dataType: "json",
				headers: {
					Accept: "application/json;odata=verbose"
				}
			});
			callConfig.done(function(data, textStatus, jqXHR) {
			try 
			{
				ConfigListData=data.d;
				localStorage.setItem('ConfigListDataJson', JSON.stringify(ConfigListData));
			}
			catch (e) {
			//console.log(e);
			}
			});
			callConfig.fail(function(jqXHR, textStatus, errorThrown) {
			localStorage.setItem('ConfigListDataJson', '{"results":[]}');
			//console.log("Error getting Site Color" + jqXHR.responseText);
			});
	}
	
	function GetMySiteUrlFromLocalStarageJson(){
	var retrievedObject = localStorage.getItem('ConfigListDataJson');
	ConfigListData=JSON.parse(retrievedObject);
	if( ConfigListData !=null && ConfigListData.results !=null && ConfigListData.results.length >0)
	{
	try {
			for(var congigItemInex=0;congigItemInex<ConfigListData.results.length;congigItemInex++)
			{
				var ItemTitle = ConfigListData.results[congigItemInex].Title;
				if(ItemTitle.toLowerCase()==="mysiteurl")
				{
					return ConfigListData.results[congigItemInex].sitepath;
				}
						
			}
					
		}
		catch (e) {
		console.log(e);
		}
	}
}
function GetUserProfileUrlFromLocalStarageJson(){
	var retrievedObject = localStorage.getItem('ConfigListDataJson');
	ConfigListData=JSON.parse(retrievedObject);
	if( ConfigListData !=null && ConfigListData.results !=null && ConfigListData.results.length >0)
	{
	try {
			for(var congigItemInex=0;congigItemInex<ConfigListData.results.length;congigItemInex++)
			{
				var ItemTitle = ConfigListData.results[congigItemInex].Title;
				if(ItemTitle.toLowerCase()==="userprofileserviceurl")
				{
					return ConfigListData.results[congigItemInex].sitepath;
				}
						
			}
					
		}
		catch (e) {
		console.log(e);
		}
	}
}