
$(document).ready(function () {

    $('#zz18_RootAspMenu > li > ul').hover(function () {
        if ($(this).parent().hasClass('selected')) {
            $(this).parent().children('a').addClass('selected');
        }
        else {
            $(this).parent().children('a').addClass('selected');
        }
    }, function () {
        if ($(this).parent().hasClass('selected')) {
            $(this).parent().children('a').addClass('selected');
        }
        else {
            $(this).parent().children('a').removeClass('selected');
        }
    });

    $.fn.wpTabify = function () {
        var toReturn = false;
        if ($('.ms-WPAddButton').size() == 0) {
            toReturn = this.each(function (i) {
                var idName = $(this).attr('id');
                var tabList = $('<ul class="wpt-ui-tabs-nav"/>');
                var panels = $('<div class="wpt-ui-tabs-wrapper"/>');

                var centerBottom = $('<div class="centerBottom" />');

                var centerMid = $('<div class="centerMid"  />');

                $(centerBottom).append(centerMid);

                var itemToRemove = false;

                $(panels).append('<div class="centerTop" ><div></div></div>');
                var codeArray = [{ code: "" }];
                var webparts = [{ key: "", valueIn: codeArray, j: 0, t: "", func: "", isCalendar: false, currentElementId: '' }];
                var mergeWebparts = [{ name: "" }];

                $(this).find('div[id^="MSOZoneCell_"]').
                each(function (j) {
                    var isCalendar = false;

                    var currentElementId = $(this).attr('id');

                    if (currentElementId.match("^MSOZoneCell_WebPartWPQ")) {
                        isCalendar = true;
                    }

                    var panelContents = $(this).detach();
                    var nameForId = $(this).find('h2.ms-webpart-titleText').text();

                    var merge = $.grep(mergeWebparts, function (e) {
                        var items = e.name.split(";");
                        var result = $.inArray(nameForId, items)
                        return result != -1;
                    });

                    if (merge.length != 0) {
                        var abc = $.grep(webparts, function (e) {
                            if (e.t != undefined && e.t != '') {
                                var items = e.t.split(";");
                                var result = $.inArray(nameForId, items)
                                return result != -1;
                            }
                            else {
                                return false;
                            }
                        });
                        var panelContentsSecond = $(this).detach();
                        for (var l = 0; l < abc.length; l++) {
                            abc[l].valueIn.push({ code: panelContentsSecond });
                        }
                    }
                    else {
                        var t = "#";
                        var func = "";

                        try {
                            t = $(this).find('input[id*=MergeTo]').val();
                            func = $(this).find('input[id*=LoadFunctionName]').val();

                            if (t != undefined && t != '') {
                                var checkRepeat = $.grep(mergeWebparts, function (e) { return e.name == t; });

                                if (checkRepeat.length == 0) {
                                    var g = { name: t };
                                    mergeWebparts.push(g);
                                }
                            }

                            var it = [{ code: panelContents }];
                            webparts.push({ key: nameForId, valueIn: it, j: j, t: t, func: func, isCalendar: isCalendar, currentElementId: currentElementId });
                        }
                        catch (e) {
                        }
                    }
                });

                for (var count = 1; count < webparts.length; count++) {
                    var thisPanel = '';
                    var calendarDiv = '';
                    var nameForId = webparts[count].key;

                    $(tabList).append('<li><a LoadFunction="' + webparts[count].func + '" href="#ui-tab-panel' +
                        idName + i + webparts[count].j + '"><span>' + nameForId + '</span></a></li>');

                    thisPanel = $('<div id="ui-tab-panel' + idName +
                    i + webparts[count].j + '" class="wpt-ui-tabs-panel" />');

                    for (var item = 0; item < webparts[count].valueIn.length; item++) {
                        var panelContents = webparts[count].valueIn[item].code;

                        if (webparts[count].isCalendar) {
                            var calenderId = webparts[count].currentElementId.split('_');
                            calendarDiv = $('<div id="' + calenderId[1] + '" onkeyup="WpKeyUp(event)" onmouseup="WpClick(event)"/>');

                            $(calendarDiv).append($(panelContents).
                            find('.ms-wpContentDivSpace').html());
                            $(thisPanel).append(calendarDiv);
                        }
                        else {
                            $(thisPanel).append($(panelContents).
                            find('.ms-wpContentDivSpace').html());
                        }
                    }

                    $(centerMid).append(thisPanel);
                }

                
                var simpleDiv = $('<div id="LoadingImage" style="display:none"  />');
                $(centerMid).append('<div class="clear"></div>');
                $(centerMid).append(simpleDiv);
                
                $(panels).append(centerBottom);

                if ($(tabList).find('li').size() > 0) {
                    $(this).prepend(panels);
                    $(this).prepend(tabList);
                    $(this).tabs();
                }
            });
        }
        $('div.ms-PartSpacingVertical').not(':eq(0)').hide();
        $(this).show();
        return toReturn;
    };

    _spBodyOnLoadFunctionNames.push("TabifyFnWrapper");
});
function TabifyFnWrapper() {
try
{

    $('.tabNav').wpTabify();
    $(".tabNav").tabs().addClass("ui-tabs-vertical ui-helper-clearfix");
    //$(".tabNav li").removeClass("ui-corner-top").addClass("ui-corner-left");

    var index;
    var hdnSelectedTabIndex;
    var tabIndex;

    index = 0;
    hdnSelectedTabIndex = $(document).find('input[id*=hdnSelectedTabIndex]');
    tabIndex = hdnSelectedTabIndex.val();
    var querystring = getParameterByName("SelectedTab");
    
    if (tabIndex != '') {
        index = tabIndex;
    }

    if (querystring != '') {
        hdnSelectedTabIndex.val(querystring);
        index = querystring;
    }
    
    $('.tabNav').tabs("option", "active", index);

    if (index == 0 && $('.ms-WPAddButton').size() == 0) {
        var defaultactive = $(".tabNav").tabs("option", "active");
        var tab = $('.tabNav').find(".ui-tabs-nav li:eq('" + defaultactive + "') a");
        var loadfunc = tab[0].attributes["LoadFunction"].value;
        loadFunction(loadfunc);

    }

    $('.tabNav').tabs({
        activate: function (event, ui) {
            var func = ui.newTab[0].childNodes[0].attributes["LoadFunction"].value;
            loadFunction(func);
            var active = $(".tabNav").tabs("option", "active");
            hdnSelectedTabIndex.val(active);

        }
    });
    $('#MainImage').hide();
}
catch(e)
{
    $('#MainImage').hide();
    $('#MainErrorImage').addClass('centerImage');
    var widthMain = $(".containerWidth").width();
    var heigthMain = 500;
    $("#Main").css({ 'display': 'block', 'height': heigthMain, 'width': widthMain });
    $("#MainErrorImage").css({ 'display': 'block', 'height': heigthMain, 'width': widthMain });
    $("#contentDiv").hide();
    $("#errorCenterTab").html("Some error occured.Please contact administrator.");
}

}

function loadFunction(func, index) {
    if (func != "undefined" && func.length > 0) {
        if (func.indexOf(".") > 0) {
            var funcName = func.split('.');
            window[funcName[0]][funcName[1]]();
        }
        else {
            window[func]();
        }
    }
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}