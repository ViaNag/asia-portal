﻿$(document).ready(function() {
	retrieveListItems();
	
});

var siteUrl = 'http://sp2013-dev-4:2222/services/foodservices/';

function retrieveListItems() {

    var clientContext = new SP.ClientContext(siteUrl);
    var oList = clientContext.get_web().get_lists().getByTitle('CateringMenu');
        
    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml('<View><Query><OrderBy><FieldRef Name=\'Category\' Ascending=\'True\'/></OrderBy></Query></View>');
    this.collListItem = oList.getItems(camlQuery);
        
    clientContext.load(collListItem);
        
    clientContext.executeQueryAsync(Function.createDelegate(this, this.onQuerySucceeded), Function.createDelegate(this, this.onQueryFailed));        
        
}

function onQuerySucceeded(sender, args) {

    var listItemInfo = '';

    var listItemEnumerator = collListItem.getEnumerator();
    
	var cateringMenu = '<table width="150%" border="1"><th></th><th>Qty</th><th>Title</th><th>Price</th><th>Total</th>';    
    
    while (listItemEnumerator.moveNext()) {
        var oListItem = listItemEnumerator.get_current();
        var menuId = oListItem.get_fieldValues().ID;        
        var menuTitle = oListItem.get_fieldValues().Title;
        var menuPrice = oListItem.get_fieldValues().Price;
        
		cateringMenu += '<tr><td><input name="Check'+menuId+'" type="checkbox" ></input></td><td style="width:50px"><input style="display:none;width:40px!important" name="Text'+menuId+'" type="text" onchange="calculateTotal('+menuId+','+menuPrice+',this)" /></td><td>'+menuTitle+'</td><td>'+menuPrice+'</td><td style="width:50px" class="calculateSum" id="Total'+menuId+'"></td></tr>';
    }
	cateringMenu += '<tr><td></td><td></td><td></td><td style="text-align:right">Total :</td><td style="width:80px;" id="GrandTotal"></td></tr></table>';
	$('#CateringMenu').append(cateringMenu); 
	
	$('input[type="checkbox"]').click(function()
	{
		if ( $(this).is(':checked') )
		{
			$(this).closest("td").next().find("input").show();
		}
		else
		{
			$(this).closest("td").next().find("input").hide();
			$(this).closest("tr").find("td:last-child").html("");
		}
	});
}

function onQueryFailed(sender, args) {

    /*ADFS Alert Issue*/console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
}

function calculateTotal(itemID, itemPrice, target)
{
	$('#Total'+itemID).html((target.value*itemPrice).toFixed(2));
	
	var total = 0;
	$(".calculateSum").each(function() {
		total += Number($(this).html());
	});
	total = total.toFixed(2);
	$('#GrandTotal').html(total);
}

