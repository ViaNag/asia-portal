﻿$(document).ready(function() {

    articleTagging.pageName = getPageName();

    //articleTagging.getPageIdValue();



    $.ajax({
        url: _spPageContextInfo.webServerRelativeUrl + "/_api/web/lists/GetByTitle('Images')/items?$select=FileLeafRef,OData__Comments,Title,TaggedPage/ID&$expand=TaggedPage/ID&$filter=TaggedPage/ID eq '" + _spPageContextInfo.pageItemId + "'",

        type: 'GET',

        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
            "content-type": "application/json;odata=verbose",
            'X-RequestDigest': $('#__REQUESTDIGEST').val()
        },

        success: function(responseData) {
            $(responseData.d.results).each(function() {
				var dataTitle=$(this).attr("Title");
				dataTitle=htmlEscape(dataTitle);
				var dataDescription=$(this).attr("OData__Comments");
				dataDescription=htmlEscape(dataDescription);
                $(".galleria").append("<img src='" + _spPageContextInfo.webServerRelativeUrl + "/PublishingImages/" + articleTagging.pageName + "/" + $(this).attr("FileLeafRef") + "' data-title='" + dataTitle + "' data-description='" + dataDescription + "' ></img>");

            });
        },
        error: function() {
            //console.log("error");
        }

    });


    $('.galleria').galleria({
        width: 690,
        height: 400
    });

});

function setHeader(xhr) {
    xhr.setRequestHeader("Accept", "application/json; odata=verbose");
}

function htmlEscape(str) {
    return String(str)
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
}
