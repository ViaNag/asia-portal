$(function() {    
   $("#MSOZoneCell_WebPartWPQ8 .dfwp-list li").each(function() {
       $(this).click(openLinkInDialog);
	  $('a', this).click(function(ev) {
       ev.preventDefault();
   });
   });
	function openLinkInDialog()
	{
		var openURL=$('a', this).attr('href');
		var dialogTitle= $(this).text();
		$('a', this).attr('href',"")
		var options = {
			title: dialogTitle,
			width: 1200,
			height: 1000,
			allowMaximize: false,  
            showClose: true
			url: openURL };

		 SP.SOD.execute('sp.ui.dialog.js', 'SP.UI.ModalDialog.showModalDialog', options);
		return false;
	}
});

