<%-- SPG:

This HTML file has been associated with a SharePoint Page Layout (.aspx file) carrying the same name.  While the files remain associated, you will not be allowed to edit the .aspx file, and any rename, move, or deletion operations will be reciprocated.

To build the page layout directly from this HTML file, simply fill in the contents of content placeholders.  Use the Snippet Generator at https://greenroomqa.viacom.com/sites/Asia/_layouts/15/ComponentHome.aspx?Url=https%3A%2F%2Fgreenroomqa%2Eviacom%2Ecom%2Fsites%2FAsia%2F%5Fcatalogs%2Fmasterpage%2Fkonda%2Fkonda%2Darticle%2Dvideo%2Easpx to create and customize additional content placeholders and other useful SharePoint entities, then copy and paste them as HTML snippets into your HTML code.   All updates to this file within content placeholders will automatically sync to the associated page layout.

 --%>
<%@Page language="C#" Inherits="Microsoft.SharePoint.Publishing.Design.ConversionErrorPageLayout, Microsoft.SharePoint.Publishing, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<html xmlns:mso="urn:schemas-microsoft-com:office:office" xmlns:msdt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"><head>
<!--[if gte mso 9]><SharePoint:CTFieldRefs runat=server Prefix="mso:" FieldList="FileLeafRef,MasterPageDescription,PublishingPreviewImage,PublishingHidden,_PublishingMigratedGuid,PublishingAssociatedContentType,PublishingAssociatedVariations"><xml>
<mso:CustomDocumentProperties>
<mso:AssociatedFileMessages msdt:dt="string">&#65279;&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;&lt;ArrayOfFileMessage xmlns:xsd=&quot;http://www.w3.org/2001/XMLSchema&quot; xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot;&gt;&lt;FileMessage xsi:type=&quot;ParseMessage&quot;&gt;&lt;TokenId&gt;HtmlDesignNotXmlCompliant&lt;/TokenId&gt;&lt;MessageType&gt;Error&lt;/MessageType&gt;&lt;Text&gt;Name cannot begin with the '&amp;lt;' character, hexadecimal value 0x3C. Line 320, position 29.&lt;/Text&gt;&lt;Time&gt;2018-04-10T04:31:07.5603569Z&lt;/Time&gt;&lt;/FileMessage&gt;&lt;/ArrayOfFileMessage&gt;</mso:AssociatedFileMessages>
</mso:CustomDocumentProperties>
</xml></SharePoint:CTFieldRefs><![endif]-->
</head>