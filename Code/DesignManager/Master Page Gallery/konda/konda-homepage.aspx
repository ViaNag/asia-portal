<%-- SPG:

This HTML file has been associated with a SharePoint Page Layout (.aspx file) carrying the same name.  While the files remain associated, you will not be allowed to edit the .aspx file, and any rename, move, or deletion operations will be reciprocated.

To build the page layout directly from this HTML file, simply fill in the contents of content placeholders.  Use the Snippet Generator at https://greenroomqa.viacom.com/sites/Asia/_layouts/15/ComponentHome.aspx?Url=https%3A%2F%2Fgreenroomqa%2Eviacom%2Ecom%2Fsites%2FAsia%2F%5Fcatalogs%2Fmasterpage%2Fkonda%2Fkonda%2Dhomepage%2Easpx to create and customize additional content placeholders and other useful SharePoint entities, then copy and paste them as HTML snippets into your HTML code.   All updates to this file within content placeholders will automatically sync to the associated page layout.

 --%>
<%@Page language="C#" Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage, Microsoft.SharePoint.Publishing, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="PageFieldFieldValue" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="Publishing" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<asp:Content runat="server" ContentPlaceHolderID="PlaceHolderAdditionalPageHead">
            
            
            
            <Publishing:EditModePanel runat="server" id="editmodestyles">
                <SharePoint:CssRegistration name="&lt;% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/editmode15.css %&gt;" After="&lt;% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/pagelayouts15.css %&gt;" runat="server">
                </SharePoint:CssRegistration>
            </Publishing:EditModePanel>
            
        </asp:Content><asp:Content runat="server" ContentPlaceHolderID="PlaceHolderPageTitle">
            <SharePoint:ProjectProperty Property="Title" runat="server" />
            
            
            <PageFieldFieldValue:FieldValue FieldName="fa564e0f-0c70-4ab9-b863-0177e6ddd247" runat="server">
            </PageFieldFieldValue:FieldValue>
            
        </asp:Content><asp:Content runat="server" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea">
            
            
            <PageFieldFieldValue:FieldValue FieldName="fa564e0f-0c70-4ab9-b863-0177e6ddd247" runat="server">
            </PageFieldFieldValue:FieldValue>
            
        </asp:Content><asp:Content runat="server" ContentPlaceHolderID="PlaceHolderMain">
            <SharePoint:ScriptLink ID="ScriptLink3" name="~sitecollection/_catalogs/masterpage/konda/scripts/js/pagelayouts.js" OnDemand="false" runat="server" Localizable="false" />
            <link href="/sites/Asia/_catalogs/masterpage/konda/css/homepage.css" rel="stylesheet" type="text/css" />
            <SharePoint:ScriptLink ID="ScriptLink6" name="SP.js" runat="server" ondemand="false" localizable="false" loadafterui="true" />
            <SharePoint:ScriptLink ID="ScriptLink8" name="SP.Core.js" runat="server" ondemand="false" localizable="false" loadafterui="true" />
            <SharePoint:ScriptLink ID="ScriptLink9" name="Reputation.js" runat="server" ondemand="false" localizable="false" loadafterui="true" />
            <div class="container" id="homepage">
                <div class="container" id="greenroom-homepage-content-container">
                    <div class="col-md-12" id="greenroom-homepage-alert">
                        <div data-name="WebPartZone">
                            
                            
                            <div>
                                <WebPartPages:WebPartZone runat="server" ID="x0f216b2285654ec48e74568cbc8ca8a2" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Vertical" Title="Alert Zone">
                                    <ZoneTemplate>
                                        
                                    </ZoneTemplate>
                                </WebPartPages:WebPartZone>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-md-12" id="greenroom-homepage-campaign">
                        <div data-name="WebPartZone">
                            
                            
                            <div>
                                <WebPartPages:WebPartZone runat="server" ID="x77af1708ba5d4f32973b278bd939e5eb" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Vertical" Title="Campaign Zone">
                                    <ZoneTemplate>
                                        
                                    </ZoneTemplate>
                                </WebPartPages:WebPartZone>
                            </div>
                            
                        </div>
                    </div>
                    <div class="homepage-container">
                        <div id="greenroom-anchor-links" class="col-md-12">
                            <div data-name="WebPartZone">
                                
                                
                                <div>
                                    <WebPartPages:WebPartZone runat="server" ID="x38ad711c90c6444e924baeeb1965df58" AllowPersonalization="False" FrameType="None" Orientation="Vertical" Title="Anchor Links Zone" PartChromeType="None">
                                        <ZoneTemplate>
                                            
                                        </ZoneTemplate>
                                    </WebPartPages:WebPartZone>
                                </div>
                                
                            </div>
                        </div>
                        <div id="greenroom-homepage-content" class="col-md-9 col-sm-7">
                            <div data-name="WebPartZone">
                                
                                
                                <div>
                                    <WebPartPages:WebPartZone runat="server" ID="x38ad711c90c6444e924baeeb1965df57" AllowPersonalization="False" FrameType="None" Orientation="Vertical" Title="Left Content Zone" PartChromeType="None">
                                        <ZoneTemplate>
                                            
                                        </ZoneTemplate>
                                    </WebPartPages:WebPartZone>
                                </div>
                                
                            </div>
                        </div>
                        <!-- id="greenroom-homepage-content" end-->
                        <div class="col-md-3 col-sm-5" id="greenroom-homepage-sidebar">
                            <div data-name="WebPartZone">
                                
                                
                                <div>
                                    <WebPartPages:WebPartZone runat="server" ID="xdd6908498dc04faf8396cc5dfd5282fc" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Vertical" Title="Sidebar Zone" PartChromeType="None">
                                        <ZoneTemplate>
                                            
                                        </ZoneTemplate>
                                    </WebPartPages:WebPartZone>
                                </div>
                                
                            </div>
                        </div>
                        <!-- id="greenroom-homepage-sidebar" end-->
                    </div>
                </div>
                <!-- id="greenroom-homepage-content-container" end-->
            </div>
            <SharePoint:ScriptLink ID="ScriptLink4" name="~sitecollection/_catalogs/masterpage/konda/scripts/js/homepage.js" OnDemand="false" runat="server" Localizable="false" />
            <!-- id="homepage" end-->
        </asp:Content>