<%-- SPG:

This HTML file has been associated with a SharePoint Page Layout (.aspx file) carrying the same name.  While the files remain associated, you will not be allowed to edit the .aspx file, and any rename, move, or deletion operations will be reciprocated.

To build the page layout directly from this HTML file, simply fill in the contents of content placeholders.  Use the Snippet Generator at https://greenroomqa.viacom.com/sites/Asia/_layouts/15/ComponentHome.aspx?Url=https%3A%2F%2Fgreenroomqa%2Eviacom%2Ecom%2Fsites%2FAsia%2F%5Fcatalogs%2Fmasterpage%2Fkonda%2Fkonda%2Dpage%2Dtemplate%2Easpx to create and customize additional content placeholders and other useful SharePoint entities, then copy and paste them as HTML snippets into your HTML code.   All updates to this file within content placeholders will automatically sync to the associated page layout.

 --%>
<%@Page language="C#" Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage, Microsoft.SharePoint.Publishing, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="PageFieldFieldValue" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="Publishing" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="PageFieldRichImageField" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="PageFieldRichHtmlField" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@Register TagPrefix="PageFieldTaxonomyFieldControl" Namespace="Microsoft.SharePoint.Taxonomy" Assembly="Microsoft.SharePoint.Taxonomy, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<asp:Content runat="server" ContentPlaceHolderID="PlaceHolderPageTitle">
            <SharePoint:ProjectProperty Property="Title" runat="server" />
            
            
            <PageFieldFieldValue:FieldValue FieldName="fa564e0f-0c70-4ab9-b863-0177e6ddd247" runat="server">
            </PageFieldFieldValue:FieldValue>
            
        </asp:Content><asp:Content runat="server" ContentPlaceHolderID="PlaceHolderAdditionalPageHead">
            
            
            
            <Publishing:EditModePanel runat="server" id="editmodestyles">
                <SharePoint:CssRegistration name="&lt;% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/editmode15.css %&gt;" After="&lt;% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/pagelayouts15.css %&gt;" runat="server">
                </SharePoint:CssRegistration>
            </Publishing:EditModePanel>
            
        </asp:Content><asp:Content runat="server" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea">
            
            
            <PageFieldFieldValue:FieldValue FieldName="fa564e0f-0c70-4ab9-b863-0177e6ddd247" runat="server">
            </PageFieldFieldValue:FieldValue>
            
        </asp:Content><asp:Content runat="server" ContentPlaceHolderID="PlaceHolderMain">
            <script type="text/javascript" src="/sites/Asia/_catalogs/masterpage/konda/scripts/js/LikePages.js">//<![CDATA[
             
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            //]]></script>
            <script src="/sites/Asia/_catalogs/masterpage/konda/scripts/js/pagelayouts.js">//<![CDATA[
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            //]]></script>
            <SharePoint:ScriptLink ID="ScriptLink6" name="SP.js" runat="server" ondemand="false" localizable="false" loadafterui="true" />
            <SharePoint:ScriptLink ID="ScriptLink8" name="SP.Core.js" runat="server" ondemand="false" localizable="false" loadafterui="true" />
            <SharePoint:ScriptLink ID="ScriptLink9" name="Reputation.js" runat="server" ondemand="false" localizable="false" loadafterui="true" />
            <link href="/sites/Asia/_catalogs/masterpage/konda/css/PageLayouts.css" rel="stylesheet" type="text/css" />
            <!-- Begin page content -->
            <div class="container" id="landing-page">
                <div id="greenroom-subsite-title">
                </div>
                <div id="greenroom-subsite-title-follow">
                </div>
                <a href="mailto:AllatSGISAppln@viacom.com?subject=Greenroom Asia Feedback">
                    <div id="greenroom-landing-page-btn-feedback">
                        FEEDBACK 
                        
                        <span>
                        </span>
                    </div>
                </a>
                <div class="col-xs-12" id="greenroom-top-zone-full-width">
                    <div data-name="WebPartZone">
                        
                        
                        <div>
                            <WebPartPages:WebPartZone runat="server" AllowPersonalization="false" ID="xe9d280afd2674276aa5c949e659150e8" FrameType="TitleBarOnly" Orientation="Vertical" Title="greenroom-top-zone-full-width">
                                <ZoneTemplate>
                                    
                                </ZoneTemplate>
                            </WebPartPages:WebPartZone>
                        </div>
                        
                    </div>
                </div>
                <div class="container" id="greenroom-page-content-container">
                    <div class="col-md-9 col-sm-7 col-xs-12">
                        <div class="greenroom-article-info-report">
                            <div id="TimeStampModified">
                            </div>
                        </div>
                        <div id="greenroom-landing-page" class="col-xs-12">
                            <div id="greenroom-landing-page-content">
                                <div data-name="Page Field: Thumbnail Image" class="page-field-image">
                                    
                                    
                                    <PageFieldRichImageField:RichImageField FieldName="3de94b06-4120-41a5-b907-88773e493458" runat="server">
                                        
                                    </PageFieldRichImageField:RichImageField>
                                    
                                </div>
                                <div class="page-field-content">
                                    
                                    
                                    <PageFieldRichHtmlField:RichHtmlField FieldName="f55c4d88-1f2e-4ad9-aaa8-819af4ee7ee8" runat="server">
                                        
                                    </PageFieldRichHtmlField:RichHtmlField>
                                    
                                </div>
                                <div class="row1">
                                    <div id="greenroom-top-zone-left-col8" class="col-md-8">
                                        <div data-name="WebPartZone">
                                            
                                            
                                            <div>
                                                <WebPartPages:WebPartZone runat="server" ID="xa660de4d2908407ab1c5ffad45b9273c1" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Vertical" Title="greenroom-top-zone-left-col8">
                                                    <ZoneTemplate>
                                                        
                                                    </ZoneTemplate>
                                                </WebPartPages:WebPartZone>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div id="greenroom-top-zone-right-col4" class="col-md-4">
                                        <div data-name="WebPartZone">
                                            
                                            
                                            <div>
                                                <WebPartPages:WebPartZone runat="server" ID="xa660de4d2908407ab1c5ffad45b9273c2" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Vertical" Title="greenroom-top-zone-right-col4">
                                                    <ZoneTemplate>
                                                        
                                                    </ZoneTemplate>
                                                </WebPartPages:WebPartZone>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                </div>
                                <div class="row2">
                                    <div id="greenroom-top-zone-left-col6" class="col-md-6 col-xs-12">
                                        <div data-name="WebPartZone">
                                            
                                            
                                            <div>
                                                <WebPartPages:WebPartZone runat="server" ID="xa660de4d2908407ab1c5ffad45b9273c3" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Vertical" Title="greenroom-top-zone-left-col6">
                                                    <ZoneTemplate>
                                                        
                                                    </ZoneTemplate>
                                                </WebPartPages:WebPartZone>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div id="greenroom-top-zone-right-col6" class="col-md-6 col-xs-12">
                                        <div data-name="WebPartZone">
                                            
                                            
                                            <div>
                                                <WebPartPages:WebPartZone runat="server" ID="xa660de4d2908407ab1c5ffad45b9273c4" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Vertical" Title="greenroom-top-zone-right-col6">
                                                    <ZoneTemplate>
                                                        
                                                    </ZoneTemplate>
                                                </WebPartPages:WebPartZone>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                </div>
                                <div class="row1">
                                    <div id="greenroom-top-zone-left-col4" class="col-md-4">
                                        <div data-name="WebPartZone">
                                            
                                            
                                            <div>
                                                <WebPartPages:WebPartZone runat="server" ID="xa660de4d2908407ab1c5ffad45b9273c221" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Vertical" Title="greenroom-top-zone-left-col4">
                                                    <ZoneTemplate>
                                                        
                                                    </ZoneTemplate>
                                                </WebPartPages:WebPartZone>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div id="greenroom-top-zone-right-col8" class="col-md-8">
                                        <div data-name="WebPartZone">
                                            
                                            
                                            <div>
                                                <WebPartPages:WebPartZone runat="server" ID="xa660de4d2908407ab1c5ffad45b9273c122" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Vertical" Title="greenroom-top-zone-right-col8">
                                                    <ZoneTemplate>
                                                        
                                                    </ZoneTemplate>
                                                </WebPartPages:WebPartZone>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                </div>
                                <div class="row3">
                                    <div id="greenroom-top-zone-extreme-left-col4" class="col-md-4">
                                        <div data-name="WebPartZone">
                                            
                                            
                                            <div>
                                                <WebPartPages:WebPartZone runat="server" ID="xa660de4d2908407ab1c5ffad45b9273c5" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Vertical" Title="greenroom-top-zone-extreme-left-col4">
                                                    <ZoneTemplate>
                                                        
                                                    </ZoneTemplate>
                                                </WebPartPages:WebPartZone>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div id="greenroom-top-zone-center-col4" class="col-md-4">
                                        <div data-name="WebPartZone">
                                            
                                            
                                            <div>
                                                <WebPartPages:WebPartZone runat="server" ID="xa660de4d2908407ab1c5ffad45b9273c6" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Vertical" Title="greenroom-top-zone-center-col4">
                                                    <ZoneTemplate>
                                                        
                                                    </ZoneTemplate>
                                                </WebPartPages:WebPartZone>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div id="greenroom-top-zone-extreme-right-col4" class="col-md-4">
                                        <div data-name="WebPartZone">
                                            
                                            
                                            <div>
                                                <WebPartPages:WebPartZone runat="server" ID="xa660de4d2908407ab1c5ffad45b9273c7" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Vertical" Title="greenroom-top-zone-extreme-right-col4">
                                                    <ZoneTemplate>
                                                        
                                                    </ZoneTemplate>
                                                </WebPartPages:WebPartZone>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                </div>
                                <div id="greenroom-top-zone-horizontal">
                                    <div data-name="WebPartZone">
                                        
                                        
                                        <div>
                                            <WebPartPages:WebPartZone runat="server" ID="xa660de4d2908407ab1c5ffad45b9273c" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Vertical" Title="greenroom-top-zone-horizontal">
                                                <ZoneTemplate>
                                                    
                                                </ZoneTemplate>
                                            </WebPartPages:WebPartZone>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div id="greenroom-top-zone-horizontal2">
                                    <div data-name="WebPartZone">
                                        
                                        
                                        <div>
                                            <WebPartPages:WebPartZone runat="server" ID="x8f90f749718c4a66a43577278f3e2c3e" LayoutOrientation="Horizontal" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Horizontal" Title="greenroom-top-zone-horizontal2">
                                                <ZoneTemplate>
                                                    
                                                </ZoneTemplate>
                                            </WebPartPages:WebPartZone>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div id="greenroom-top-zone-vertical">
                                    <div data-name="WebPartZone">
                                        
                                        
                                        <div>
                                            <WebPartPages:WebPartZone runat="server" ID="x23c4d8a186514c6599791c1ab4797146" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Vertical" Title="greenroom-top-zone-vertical">
                                                <ZoneTemplate>
                                                    
                                                </ZoneTemplate>
                                            </WebPartPages:WebPartZone>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <!-- id="greenroom-page-content" end-->
                        </div>
                        <!-- id="greenroom-article-content" end-->
                        <div id="greenroom-bottom-zone-left" class="col-md-6 col-xs-12">
                            <div data-name="WebPartZone">
                                
                                
                                <div>
                                    <WebPartPages:WebPartZone runat="server" ID="xd7ee700c2f8244ae91a39adad0237e41" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Vertical" Title="greenroom-bottom-zone-left">
                                        <ZoneTemplate>
                                        </ZoneTemplate>
                                    </WebPartPages:WebPartZone>
                                </div>
                                
                            </div>
                        </div>
                        <div id="greenroom-bottom-zone-right" class="col-md-6 col-xs-12">
                            <div data-name="WebPartZone">
                                
                                
                                <div>
                                    <WebPartPages:WebPartZone runat="server" ID="x0c81ca42b93c41b8aec63efb5beba7f6" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Vertical" Title="greenroom-bottom-zone-right">
                                        <ZoneTemplate>
                                            
                                        </ZoneTemplate>
                                    </WebPartPages:WebPartZone>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-5 col-xs-12" id="greenroom-sidebar">
                        <div data-name="EditModePanelShowInEdit">
                            
                            
                            <Publishing:EditModePanel runat="server" CssClass="edit-mode-panel">
                                
                                <div class="DefaultContentBlock" style="border:medium black solid; background:yellow; color:black; margin:20px; padding:10px;">
                                    <div data-name="Page Field: Enterprise Keywords">
                                        
                                        
                                        <PageFieldTaxonomyFieldControl:TaxonomyFieldControl FieldName="23f27201-bee3-471e-b2e7-b64fd8b7ca38" runat="server">
                                            
                                        </PageFieldTaxonomyFieldControl:TaxonomyFieldControl>
                                        
                                    </div>
                                </div>
                                
                            </Publishing:EditModePanel>
                            
                        </div>
                        <div data-name="WebPartZone">
                            
                            
                            <div>
                                <WebPartPages:WebPartZone runat="server" ID="x3a0dfd72d75543d3916f18c057f336e5" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Vertical" Title="greenroom-sidebar-zone">
                                    <ZoneTemplate>
                                        
                                    </ZoneTemplate>
                                </WebPartPages:WebPartZone>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-xs-12" id="greenroom-bottom-zone-full-width">
                    <div data-name="WebPartZone">
                        
                        
                        <div>
                            <WebPartPages:WebPartZone runat="server" ID="x5d755603a53347849598d233b05fcfd2" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Vertical" Title="greenroom-bottom-zone-full-width">
                                <ZoneTemplate>
                                    
                                </ZoneTemplate>
                            </WebPartPages:WebPartZone>
                        </div>
                        
                    </div>
                    <div data-name="WebPartZone" id="greenroom-hidden">
                        
                        
                        <div>
                            <WebPartPages:WebPartZone runat="server" ID="xefd92f3301224e1a87c5f515a1d503e8" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Vertical" Title="greenroom-hidden">
                                <ZoneTemplate>
                                    
                                </ZoneTemplate>
                            </WebPartPages:WebPartZone>
                        </div>
                        
                    </div>
                </div>
                <div class="edit-quick-links">
                    <div id="greenroom-bottom-zone-extreme-left-col4" class="col-md-4">
                        <div data-name="WebPartZone">
                            
                            
                            <div>
                                <WebPartPages:WebPartZone runat="server" ID="xa660de4d2908407ab1c5ffad45b9273c5221" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Vertical" Title="greenroom-bottom-zone-extreme-left-col4">
                                    <ZoneTemplate>
                                        
                                    </ZoneTemplate>
                                </WebPartPages:WebPartZone>
                            </div>
                            
                        </div>
                    </div>
                    <div id="greenroom-bottom-zone-center-col4" class="col-md-4">
                        <div data-name="WebPartZone">
                            
                            
                            <div>
                                <WebPartPages:WebPartZone runat="server" ID="xa660de4d2908407ab1c5ffad45b9273c6222" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Vertical" Title="greenroom-bottom-zone-center-col4">
                                    <ZoneTemplate>
                                        
                                    </ZoneTemplate>
                                </WebPartPages:WebPartZone>
                            </div>
                            
                        </div>
                    </div>
                    <div id="greenroom-bottom-zone-extreme-right-col4" class="col-md-4">
                        <div data-name="WebPartZone">
                            
                            
                            <div>
                                <WebPartPages:WebPartZone runat="server" ID="xa660de4d2908407ab1c5ffad45b9273c7223" AllowPersonalization="False" FrameType="TitleBarOnly" Orientation="Vertical" Title="greenroom-bottom-zone-extreme-right-col4">
                                    <ZoneTemplate>
                                        
                                    </ZoneTemplate>
                                </WebPartPages:WebPartZone>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </asp:Content>